
public final  class StructSdtLogins implements Cloneable, java.io.Serializable
{
   public StructSdtLogins( )
   {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      cal.set(1, 0, 1, 0, 0, 0);
      cal.set(java.util.Calendar.MILLISECOND, 0);
      gxTv_SdtLogins_Lgnlogin = "" ;
      gxTv_SdtLogins_Lgnname = "" ;
      gxTv_SdtLogins_Lgnenable = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail = "" ;
      gxTv_SdtLogins_Lgnpwd = "" ;
      gxTv_SdtLogins_Lgnpwdtmp = "" ;
      gxTv_SdtLogins_Lgnlastlogin = cal.getTime() ;
      gxTv_SdtLogins_Lgnpwdlastchanged = cal.getTime() ;
      gxTv_SdtLogins_Lgnpwdforcechange = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser = (byte)(0) ;
      gxTv_SdtLogins_Lastlogininfo = new java.util.Vector();
      gxTv_SdtLogins_Profiles = new java.util.Vector();
      gxTv_SdtLogins_Mode = "" ;
      gxTv_SdtLogins_Lgnlogin_Z = "" ;
      gxTv_SdtLogins_Lgnname_Z = "" ;
      gxTv_SdtLogins_Lgnenable_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail_Z = "" ;
      gxTv_SdtLogins_Lgnpwd_Z = "" ;
      gxTv_SdtLogins_Lgnpwdtmp_Z = "" ;
      gxTv_SdtLogins_Lgnlastlogin_Z = cal.getTime() ;
      gxTv_SdtLogins_Lgnpwdlastchanged_Z = cal.getTime() ;
      gxTv_SdtLogins_Lgnpwdforcechange_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnname_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnenable_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwd_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnlastlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getLgnlogin( )
   {
      return gxTv_SdtLogins_Lgnlogin ;
   }

   public void setLgnlogin( String value )
   {
      gxTv_SdtLogins_Lgnlogin = value ;
      return  ;
   }

   public String getLgnname( )
   {
      return gxTv_SdtLogins_Lgnname ;
   }

   public void setLgnname( String value )
   {
      gxTv_SdtLogins_Lgnname = value ;
      return  ;
   }

   public byte getLgnenable( )
   {
      return gxTv_SdtLogins_Lgnenable ;
   }

   public void setLgnenable( byte value )
   {
      gxTv_SdtLogins_Lgnenable = value ;
      return  ;
   }

   public String getLgnemail( )
   {
      return gxTv_SdtLogins_Lgnemail ;
   }

   public void setLgnemail( String value )
   {
      gxTv_SdtLogins_Lgnemail = value ;
      return  ;
   }

   public String getLgnpwd( )
   {
      return gxTv_SdtLogins_Lgnpwd ;
   }

   public void setLgnpwd( String value )
   {
      gxTv_SdtLogins_Lgnpwd = value ;
      return  ;
   }

   public String getLgnpwdtmp( )
   {
      return gxTv_SdtLogins_Lgnpwdtmp ;
   }

   public void setLgnpwdtmp( String value )
   {
      gxTv_SdtLogins_Lgnpwdtmp = value ;
      return  ;
   }

   public java.util.Date getLgnlastlogin( )
   {
      return gxTv_SdtLogins_Lgnlastlogin ;
   }

   public void setLgnlastlogin( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnlastlogin = value ;
      return  ;
   }

   public java.util.Date getLgnpwdlastchanged( )
   {
      return gxTv_SdtLogins_Lgnpwdlastchanged ;
   }

   public void setLgnpwdlastchanged( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged = value ;
      return  ;
   }

   public byte getLgnpwdforcechange( )
   {
      return gxTv_SdtLogins_Lgnpwdforcechange ;
   }

   public void setLgnpwdforcechange( byte value )
   {
      gxTv_SdtLogins_Lgnpwdforcechange = value ;
      return  ;
   }

   public byte getLgnsuperuser( )
   {
      return gxTv_SdtLogins_Lgnsuperuser ;
   }

   public void setLgnsuperuser( byte value )
   {
      gxTv_SdtLogins_Lgnsuperuser = value ;
      return  ;
   }

   public java.util.Vector getLastlogininfo( )
   {
      return gxTv_SdtLogins_Lastlogininfo ;
   }

   public void setLastlogininfo( java.util.Vector value )
   {
      gxTv_SdtLogins_Lastlogininfo = value ;
      return  ;
   }

   public java.util.Vector getProfiles( )
   {
      return gxTv_SdtLogins_Profiles ;
   }

   public void setProfiles( java.util.Vector value )
   {
      gxTv_SdtLogins_Profiles = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtLogins_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtLogins_Mode = value ;
      return  ;
   }

   public String getLgnlogin_Z( )
   {
      return gxTv_SdtLogins_Lgnlogin_Z ;
   }

   public void setLgnlogin_Z( String value )
   {
      gxTv_SdtLogins_Lgnlogin_Z = value ;
      return  ;
   }

   public String getLgnname_Z( )
   {
      return gxTv_SdtLogins_Lgnname_Z ;
   }

   public void setLgnname_Z( String value )
   {
      gxTv_SdtLogins_Lgnname_Z = value ;
      return  ;
   }

   public byte getLgnenable_Z( )
   {
      return gxTv_SdtLogins_Lgnenable_Z ;
   }

   public void setLgnenable_Z( byte value )
   {
      gxTv_SdtLogins_Lgnenable_Z = value ;
      return  ;
   }

   public String getLgnemail_Z( )
   {
      return gxTv_SdtLogins_Lgnemail_Z ;
   }

   public void setLgnemail_Z( String value )
   {
      gxTv_SdtLogins_Lgnemail_Z = value ;
      return  ;
   }

   public String getLgnpwd_Z( )
   {
      return gxTv_SdtLogins_Lgnpwd_Z ;
   }

   public void setLgnpwd_Z( String value )
   {
      gxTv_SdtLogins_Lgnpwd_Z = value ;
      return  ;
   }

   public String getLgnpwdtmp_Z( )
   {
      return gxTv_SdtLogins_Lgnpwdtmp_Z ;
   }

   public void setLgnpwdtmp_Z( String value )
   {
      gxTv_SdtLogins_Lgnpwdtmp_Z = value ;
      return  ;
   }

   public java.util.Date getLgnlastlogin_Z( )
   {
      return gxTv_SdtLogins_Lgnlastlogin_Z ;
   }

   public void setLgnlastlogin_Z( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnlastlogin_Z = value ;
      return  ;
   }

   public java.util.Date getLgnpwdlastchanged_Z( )
   {
      return gxTv_SdtLogins_Lgnpwdlastchanged_Z ;
   }

   public void setLgnpwdlastchanged_Z( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_Z = value ;
      return  ;
   }

   public byte getLgnpwdforcechange_Z( )
   {
      return gxTv_SdtLogins_Lgnpwdforcechange_Z ;
   }

   public void setLgnpwdforcechange_Z( byte value )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_Z = value ;
      return  ;
   }

   public byte getLgnsuperuser_Z( )
   {
      return gxTv_SdtLogins_Lgnsuperuser_Z ;
   }

   public void setLgnsuperuser_Z( byte value )
   {
      gxTv_SdtLogins_Lgnsuperuser_Z = value ;
      return  ;
   }

   public byte getLgnlogin_N( )
   {
      return gxTv_SdtLogins_Lgnlogin_N ;
   }

   public void setLgnlogin_N( byte value )
   {
      gxTv_SdtLogins_Lgnlogin_N = value ;
      return  ;
   }

   public byte getLgnname_N( )
   {
      return gxTv_SdtLogins_Lgnname_N ;
   }

   public void setLgnname_N( byte value )
   {
      gxTv_SdtLogins_Lgnname_N = value ;
      return  ;
   }

   public byte getLgnenable_N( )
   {
      return gxTv_SdtLogins_Lgnenable_N ;
   }

   public void setLgnenable_N( byte value )
   {
      gxTv_SdtLogins_Lgnenable_N = value ;
      return  ;
   }

   public byte getLgnemail_N( )
   {
      return gxTv_SdtLogins_Lgnemail_N ;
   }

   public void setLgnemail_N( byte value )
   {
      gxTv_SdtLogins_Lgnemail_N = value ;
      return  ;
   }

   public byte getLgnpwd_N( )
   {
      return gxTv_SdtLogins_Lgnpwd_N ;
   }

   public void setLgnpwd_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwd_N = value ;
      return  ;
   }

   public byte getLgnpwdtmp_N( )
   {
      return gxTv_SdtLogins_Lgnpwdtmp_N ;
   }

   public void setLgnpwdtmp_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwdtmp_N = value ;
      return  ;
   }

   public byte getLgnlastlogin_N( )
   {
      return gxTv_SdtLogins_Lgnlastlogin_N ;
   }

   public void setLgnlastlogin_N( byte value )
   {
      gxTv_SdtLogins_Lgnlastlogin_N = value ;
      return  ;
   }

   public byte getLgnpwdlastchanged_N( )
   {
      return gxTv_SdtLogins_Lgnpwdlastchanged_N ;
   }

   public void setLgnpwdlastchanged_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_N = value ;
      return  ;
   }

   public byte getLgnpwdforcechange_N( )
   {
      return gxTv_SdtLogins_Lgnpwdforcechange_N ;
   }

   public void setLgnpwdforcechange_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_N = value ;
      return  ;
   }

   public byte getLgnsuperuser_N( )
   {
      return gxTv_SdtLogins_Lgnsuperuser_N ;
   }

   public void setLgnsuperuser_N( byte value )
   {
      gxTv_SdtLogins_Lgnsuperuser_N = value ;
      return  ;
   }

   protected byte gxTv_SdtLogins_Lgnenable ;
   protected byte gxTv_SdtLogins_Lgnpwdforcechange ;
   protected byte gxTv_SdtLogins_Lgnsuperuser ;
   protected byte gxTv_SdtLogins_Lgnenable_Z ;
   protected byte gxTv_SdtLogins_Lgnpwdforcechange_Z ;
   protected byte gxTv_SdtLogins_Lgnsuperuser_Z ;
   protected byte gxTv_SdtLogins_Lgnlogin_N ;
   protected byte gxTv_SdtLogins_Lgnname_N ;
   protected byte gxTv_SdtLogins_Lgnenable_N ;
   protected byte gxTv_SdtLogins_Lgnemail_N ;
   protected byte gxTv_SdtLogins_Lgnpwd_N ;
   protected byte gxTv_SdtLogins_Lgnpwdtmp_N ;
   protected byte gxTv_SdtLogins_Lgnlastlogin_N ;
   protected byte gxTv_SdtLogins_Lgnpwdlastchanged_N ;
   protected byte gxTv_SdtLogins_Lgnpwdforcechange_N ;
   protected byte gxTv_SdtLogins_Lgnsuperuser_N ;
   protected String gxTv_SdtLogins_Mode ;
   protected String gxTv_SdtLogins_Lgnlogin ;
   protected String gxTv_SdtLogins_Lgnname ;
   protected String gxTv_SdtLogins_Lgnemail ;
   protected String gxTv_SdtLogins_Lgnpwd ;
   protected String gxTv_SdtLogins_Lgnpwdtmp ;
   protected String gxTv_SdtLogins_Lgnlogin_Z ;
   protected String gxTv_SdtLogins_Lgnname_Z ;
   protected String gxTv_SdtLogins_Lgnemail_Z ;
   protected String gxTv_SdtLogins_Lgnpwd_Z ;
   protected String gxTv_SdtLogins_Lgnpwdtmp_Z ;
   protected java.util.Date gxTv_SdtLogins_Lgnlastlogin ;
   protected java.util.Date gxTv_SdtLogins_Lgnpwdlastchanged ;
   protected java.util.Date gxTv_SdtLogins_Lgnlastlogin_Z ;
   protected java.util.Date gxTv_SdtLogins_Lgnpwdlastchanged_Z ;
   protected java.util.Vector gxTv_SdtLogins_Lastlogininfo ;
   protected java.util.Vector gxTv_SdtLogins_Profiles ;
}


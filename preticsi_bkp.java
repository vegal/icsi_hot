/*
               File: RETICSI_BKP
        Description: Stub for RETICSI_BKP
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:18.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class preticsi_bkp extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      preticsi_bkp pgm = new preticsi_bkp (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};
      String[] aP1 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
         aP1[0] = (String) args[1];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0, aP1);
   }

   public preticsi_bkp( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( preticsi_bkp.class ), "" );
   }

   public preticsi_bkp( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      preticsi_bkp.this.AV2FileSou = aP0[0];
      this.aP0 = aP0;
      preticsi_bkp.this.AV3DebugMo = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new areticsi_bkp(remoteHandle, context).execute( aP0, aP1 );
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = preticsi_bkp.this.AV2FileSou;
      this.aP1[0] = preticsi_bkp.this.AV3DebugMo;
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV2FileSou ;
   private String AV3DebugMo ;
   private String[] aP0 ;
   private String[] aP1 ;
}


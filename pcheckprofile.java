/*
               File: CheckProfile
        Description: Verifica o profile ativo de um user
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pcheckprofile extends GXProcedure
{
   public pcheckprofile( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pcheckprofile.class ), "" );
   }

   public pcheckprofile( int remoteHandle ,
                         ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        byte[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             byte[] aP3 )
   {
      pcheckprofile.this.AV8uspCode = aP0[0];
      this.aP0 = aP0;
      pcheckprofile.this.AV12uspDes = aP1[0];
      this.aP1 = aP1;
      pcheckprofile.this.AV21Forced = aP2[0];
      this.aP2 = aP2;
      pcheckprofile.this.AV9ReturnV = aP3[0];
      this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV19Compan = AV11Sessio.getValue("CURRENTCOMPANY") ;
      AV18ISOCod = AV11Sessio.getValue("CURRENTCOUNTRY") ;
      AV15lgnLog = AV11Sessio.getValue("LOGGED") ;
      AV16lgnSup = (byte)(GXutil.val( AV11Sessio.getValue("SUPERUSER"), ".")) ;
      AV8uspCode = GXutil.upper( AV8uspCode) ;
      AV13Profil = AV11Sessio.getValue("ALLPROFILES") ;
      if ( ( GXutil.strSearch( AV13Profil, AV8uspCode, 1) == 0 ) )
      {
         /*
            INSERT RECORD ON TABLE USERFUNCTIONS

         */
         A47uspCode = AV8uspCode ;
         A51uspDesc = AV12uspDes ;
         /* Using cursor P00032 */
         pr_default.execute(0, new Object[] {A47uspCode, A51uspDesc});
         if ( (pr_default.getStatus(0) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         AV13Profil = "" ;
         AV25GXLvl2 = (byte)(0) ;
         /* Using cursor P00033 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A51uspDesc = P00033_A51uspDesc[0] ;
            A47uspCode = P00033_A47uspCode[0] ;
            AV25GXLvl2 = (byte)(1) ;
            AV13Profil = AV13Profil + GXutil.trim( A47uspCode) + "|" ;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( ( AV25GXLvl2 == 0 ) )
         {
         }
         AV11Sessio.setValue("ALLPROFILES", AV13Profil);
      }
      AV13Profil = AV11Sessio.getValue("ACTIVEPROFILES") ;
      if ( ( GXutil.strcmp(AV21Forced, "") != 0 ) )
      {
         AV22s = AV11Sessio.getValue(AV21Forced) ;
      }
      else
      {
         AV22s = "N" ;
      }
      if ( ( ( GXutil.strSearch( AV13Profil, AV8uspCode, 1) == 0 ) && ( AV16lgnSup == 0 ) ) || ( ( GXutil.strcmp(AV22s, "Y") == 0 ) ) )
      {
         AV9ReturnV = (byte)(0) ;
      }
      else
      {
         AV9ReturnV = (byte)(1) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pcheckprofile.this.AV8uspCode;
      this.aP1[0] = pcheckprofile.this.AV12uspDes;
      this.aP2[0] = pcheckprofile.this.AV21Forced;
      this.aP3[0] = pcheckprofile.this.AV9ReturnV;
      Application.commit(context, remoteHandle, "DEFAULT", "pcheckprofile");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV19Compan = "" ;
      AV11Sessio = httpContext.getWebSession();
      AV18ISOCod = "" ;
      AV15lgnLog = "" ;
      AV16lgnSup = (byte)(0) ;
      AV13Profil = "" ;
      GX_INS42 = 0 ;
      A47uspCode = "" ;
      A51uspDesc = "" ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV25GXLvl2 = (byte)(0) ;
      scmdbuf = "" ;
      P00033_A51uspDesc = new String[] {""} ;
      P00033_A47uspCode = new String[] {""} ;
      AV22s = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pcheckprofile__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            P00033_A51uspDesc, P00033_A47uspCode
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV9ReturnV ;
   private byte AV16lgnSup ;
   private byte AV25GXLvl2 ;
   private short Gx_err ;
   private int GX_INS42 ;
   private String AV21Forced ;
   private String AV13Profil ;
   private String Gx_emsg ;
   private String scmdbuf ;
   private String AV22s ;
   private String AV8uspCode ;
   private String AV12uspDes ;
   private String AV19Compan ;
   private String AV18ISOCod ;
   private String AV15lgnLog ;
   private String A47uspCode ;
   private String A51uspDesc ;
   private com.genexus.webpanels.WebSession AV11Sessio ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private byte[] aP3 ;
   private IDataStoreProvider pr_default ;
   private String[] P00033_A51uspDesc ;
   private String[] P00033_A47uspCode ;
}

final  class pcheckprofile__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P00032", "INSERT INTO [USERFUNCTIONS] ([uspCode], [uspDescription]) VALUES (?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00033", "SELECT [uspDescription], [uspCode] FROM [USERFUNCTIONS] WITH (NOLOCK) ORDER BY [uspCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 40, false);
               break;
      }
   }

}


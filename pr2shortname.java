/*
               File: R2ShortName
        Description: Obtem o Nome base do arquivo passado
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2shortname extends GXProcedure
{
   public pr2shortname( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2shortname.class ), "" );
   }

   public pr2shortname( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pr2shortname.this.AV12Comple = aP0[0];
      this.aP0 = aP0;
      pr2shortname.this.AV8FileNam = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV8FileNam = "" ;
      AV12Comple = GXutil.trim( AV12Comple) ;
      AV10i = (short)(GXutil.len( AV12Comple)) ;
      while ( ( AV10i >= 1 ) )
      {
         AV11c = GXutil.substring( AV12Comple, AV10i, 1) ;
         if ( ( GXutil.strcmp(AV11c, ":") == 0 ) || ( GXutil.strcmp(AV11c, "\\") == 0 ) )
         {
            if (true) break;
         }
         else
         {
            AV8FileNam = AV11c + AV8FileNam ;
         }
         AV10i = (short)(AV10i+-1) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2shortname.this.AV12Comple;
      this.aP1[0] = pr2shortname.this.AV8FileNam;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV10i = (short)(0) ;
      AV11c = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short AV10i ;
   private short Gx_err ;
   private String AV12Comple ;
   private String AV8FileNam ;
   private String AV11c ;
   private String[] aP0 ;
   private String[] aP1 ;
}


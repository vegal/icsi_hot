

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;
import com.genexus.PrivateUtilities;
import com.genexus.Application;
import com.genexus.ModelContext;
//import com.genexus.common.interfaces.SpecificImplementation;
import com.genexus.platform.INativeFunctions;
import com.genexus.platform.NativeFunctions;
import com.genexus.util.*;

//import json.org.json.JSONObject;

public final class GXutil
{
    public static boolean Confirmed = false;



/*
    public static void writeLogRaw( String message, Object obj)
    {
        com.genexus.com.genexus.CommonUtil.writeLogRaw(message, obj);
    }
*/


    public static String accessKey(String OCaption)
    {
        return com.genexus.CommonUtil.accessKey(OCaption);
    }

    public static String accessKeyCaption(String OCaption)
    {
        return com.genexus.CommonUtil.accessKeyCaption(OCaption);
    }

    public static Calendar getCalendar()
    {
        return com.genexus.CommonUtil.getCalendar();
    }

    public static String newLine()
    {
        return com.genexus.CommonUtil.newLine();
    }

    public static String getHost(String hostAndPort)
    {
        return com.genexus.CommonUtil.getHost(hostAndPort);
    }

    public static int getPort(String hostAndPort)
    {
        return getPort(hostAndPort, 0);
    }

    public static int getPort(String hostAndPort, int def)
    {
        return com.genexus.CommonUtil.getPort(hostAndPort, def);
    }

/*
    public static String link(String url, int newWindow)
    {
        PrivateUtilities.displayURL(url);
        return "";
    }
*/
    public static byte deleteFile(String fileName)
    {
        return com.genexus.CommonUtil.deleteFile(fileName);
    }

    public static double mod(double value, double div)
    {
        return com.genexus.CommonUtil.mod(value, div);
    }

    public static double random()
    {
        return com.genexus.CommonUtil.random();
    }


    public static int rseed(double[] seed)
    {
        return com.genexus.CommonUtil.rseed(seed);
    }

    public static int rseed(long seed)
    {
        return com.genexus.CommonUtil.rseed(seed);
    }

    public static byte fileExists(String fileName)
    {
        java.io.File file = new java.io.File(fileName);
        if (!fileName.startsWith("http")){

            return (file.isFile() && file.exists())? (byte)1:0;
        }
        else
        {
            return (file.isFile() && file.exists())? (byte)1:0;
        }
    }
    public static void refClasses(Class cls) { ; }
    public static double rand()
    {
        return com.genexus.CommonUtil.rand();
    }

    public static int aleat()
    {
        return	com.genexus.CommonUtil.aleat();
    }

    public static String format(long value, String picture)
    {
        return com.genexus.CommonUtil.format(value, picture);
    }
    public static String format(double value, String picture)
    {
        return com.genexus.CommonUtil.format(value, picture);
    }
    public static String format(String value, String picture)
    {
        return value;
    }
    public static String format(java.util.Date value, String picture)
    {
        return value.toString();
    }

    public static String formatDateTimeParm(Date date)
    {
        return com.genexus.CommonUtil.formatDateTimeParm(date);
    }

    public static String formatDateParm(Date date)
    {
        return com.genexus.CommonUtil.formatDateParm(date);
    }
    public static String formatDateParm(Date[] date)
    {
        return formatDateParm(date[0]);
    }

    public static String delete(String text,char del)
    {
        return (com.genexus.CommonUtil.trimSpaces(text.replace(del,' ')));
    }

    public static boolean isTime(String text)
    {
        return com.genexus.CommonUtil.isTime(text);
    }


    public static String alltrim(String text)
    {
        return com.genexus.CommonUtil.alltrim(text);
    }

    public static String rtrim(String text)
    {
        return com.genexus.CommonUtil.rtrim(text);
    }
    public static String rtrim(String[] text)
    {
        return com.genexus.CommonUtil.rtrim(text[0]);
    }
    public static String rtrim(String[][] text)
    {
        return com.genexus.CommonUtil.rtrim(text[0][0]);
    }
    public static boolean endsWith(String s1, String s2)
    {
        return com.genexus.CommonUtil.endsWith(s1, s2);
    }
    public static boolean startsWith(String s1, String s2)
    {
        return com.genexus.CommonUtil.startsWith(s1, s2);
    }
    public static boolean contains(String s1, String s2)
    {
        return com.genexus.CommonUtil.contains(s1, s2);
    }

    public static String substring (String text, int start, int end)
    {
        return com.genexus.CommonUtil.substring(text, start, end);
    }



    public static Date ymdhmsToT_noYL(String yyyymmddhhmmss)
    {
        return com.genexus.CommonUtil.ymdhmsToT_noYL(yyyymmddhhmmss);
    }

    public static Date resetDate(Date date)
    {
        return com.genexus.CommonUtil.resetDate(date);
    }

    public static Date ymdhmsToT_noYL(int year, int month, int day , int hour , int minute , int second)
    {
        return  ymdhmsToT_noYL(year, month, day, hour, minute, second, 0);
    }
    public static Date ymdhmsToT_noYL(int year, int month, int day , int hour , int minute , int second, int millisecond)
    {
        return com.genexus.CommonUtil.ymdhmsToT_noYL(year, month, day, hour, minute, second, millisecond);
    }




    public static Date nullDate()
    {
        return com.genexus.CommonUtil.nullDate();
    }

    public static Date resetTime(Date dt)
    {
        return com.genexus.CommonUtil.resetTime(dt);
    }

    public static Date resetMillis(Date dt)
    {
        return com.genexus.CommonUtil.resetMillis(dt);
    }

    public static Date now(boolean b, boolean b1)
    {
        return now(true,false);
    }

    public static Date now(boolean useClientTimeZone)
    {
        return now(useClientTimeZone,false);
    }

    public static Date nowms()
    {
        return now(true, true);
    }
/*
    public static Date now(boolean useClientTimeZone, boolean millisecond)
    {
        return SpecificImplementation.GXutil.now(useClientTimeZone, millisecond);
    }
*/
    public static short CurrentTimeOffset()
    {
        return getUTCOffset(now(true, false));
    }

    public static int datecmp( Date left , Date right)
    {
        return com.genexus.CommonUtil.datecmp(left, right);
    }

    public static boolean dateCompare( Date left , Date right)
    {
        return com.genexus.CommonUtil.dateCompare(left, right);
    }

    public static int strcmp( String left ,
                              String right )
    {
        return rtrim(left).compareTo(rtrim(right));
    }

    public static boolean strcmp2( String left ,
                                   String right )
    {
        return left.compareTo(right) == 0;
    }

    public static int guidCompare(java.util.UUID guidA, java.util.UUID guidB, int mode)
    {
        return com.genexus.CommonUtil.guidCompare(guidA, guidB, mode);
    }

    public static Date setTime(Date dt, int time)
    {
        return resetTime(dt);
    }

    public static Date today()
    {
        return (resetTime(now(true, false)));
    }

    public static short getUTCOffset(Date value)
    {
        return com.genexus.CommonUtil.getUTCOffset(value);
    }


 /*   static public Date DateTimefromTimeZone(Date dt, String sTz)
    {
        return DateTimefromTimeZone( dt, sTz, ModelContext.getModelContext());
    }

    static public Date DateTimefromTimeZone(Date dt, String sTz, ModelContext context)
    {
        TimeZone fromTimeZone = TimeZone.getTimeZone( sTz);
        if (fromTimeZone.getID().equals(sTz))
            return ConvertDateTime( dt, fromTimeZone, context.getClientTimeZone());
        return dt;
    }
*/



    public static boolean emptyDate( Date value)
    {
        return com.genexus.CommonUtil.emptyDate(value);
    }



    public static IThreadLocal threadTimeZone = GXThreadLocal.newThreadLocal();

    public static void setThreadTimeZone(TimeZone tz)
    {
        threadTimeZone.set(tz);
    }


    public static String formatLong(long number)
    {
        return com.genexus.CommonUtil.formatLong(number);
    }

    public static String booltostr(boolean val){

        return com.genexus.CommonUtil.booltostr(val);
    }

    public static boolean strtobool(String val)
    {
        return com.genexus.CommonUtil.strtobool(val);
    }

    public static String str(long val, int digits, int decimals)
    {

        return com.genexus.CommonUtil.str(val, digits, decimals);

    }






    // Esto es que hizo str(_, 2, 1) o str(_, 3, 2), todas cosas
    // invalidas que implican que decimals = 0




    // Aca el numero tiene el valor redondeado y tiene los decimales correctos. Ahora
    // hay que empezar a achicarlo si no entra en el espacio indicado.

    public static String str(int[] value, int length, int decimals)
    {
        return str(value[0], length, decimals);
    }

    public static String str(double value, int length, int decimals)
    {
        return com.genexus.CommonUtil.str((long) value, length, decimals);
    }



    public static String strori(double val, int digits, int decimals)
    {
        if	(decimals < 0) decimals = 0;
        if	(digits < 0)   digits   = 0;

        StringBuffer b = new StringBuffer();
        boolean hasSign = (val < 0);

        if	(hasSign)
        {
            b.append('-');
            val = -val;
        }

        val = round(val, decimals);

        int intDigits = 0;
        if	(val < 1)
        {
            intDigits = val == 0?1:(int) log10(val) ;
        }
        else
        {
            intDigits = val == 0?1:(int) log10(val) + 1;
        }

        if	(intDigits + (hasSign?1:0) > digits)
            return com.genexus.CommonUtil.replicate("*", digits);

        if (intDigits <= 0)
        {
            int realDigits = -intDigits;
            realDigits = realDigits > decimals?decimals:realDigits;
            b.append("0." + com.genexus.CommonUtil.replicate("0", realDigits));
        }

        for (int i = intDigits -1; i >= -decimals ; i--)
        {
            double divi =  Math.pow(10, i);
            int cur   = (int) ( (val / divi) + 0.000001);

            //BigDecimal divi2 = new BigDecimal(Math.pow(10, i));
            //int cur = unexponentString(Double.toString(val)).divide(divi2, 0).intValue();


            val = round(val - (divi * cur), decimals);

            if	(b.length() < digits)
            {
                if	(i == 0 && decimals > 0)
                {
                    if (digits > b.length() + 2)
                    {
                        b.append( (int) cur);
                        b.append('.');
                    }
                    else
                    {
                        int cur_tmp = (int) ( (val / Math.pow(10, i-1)) + 0.000001);
                        b.append( (int) cur + (cur_tmp >= 5?1:0) );
                        break;
                    }
                }
                else
                {
                    b.append( (int) cur);
                }
            }
        }

        return padl(b.toString(), digits, " ");
    }

    public static double log10(double val)
    {
        return com.genexus.CommonUtil.log10(val);
    }

    public static int strSearch(String a, String b, int from)
    {
        return com.genexus.CommonUtil.strSearch(a, b, from);
    }

    public static int strSearch(String a, String b)
    {
        return com.genexus.CommonUtil.strSearch(a, b, 0);
    }

    public static int strSearchRev(String a, String b)
    {
        return com.genexus.CommonUtil.strSearchRev(a, b);
    }

    public static int strSearchRev(String a, String b, int from)
    {
        return com.genexus.CommonUtil.strSearchRev(a, b, from);
    }


    public static String strReplace(String s, String subString, String replacement)
    {
        return com.genexus.CommonUtil.strReplace(s, subString, replacement);
    }

    public static String padstr(int number, int length)
    {
        return com.genexus.CommonUtil.padl(com.genexus.CommonUtil.ltrim(str(number, length, 0)), length, "0");
    }

    public static String padr(String text, int size, String fill)
    {
        return com.genexus.CommonUtil.padr(text, size, fill);
    }

    public static String padl(String text, int size, String fill)
    {
        return com.genexus.CommonUtil.padl(text, size, fill);
    }

    public static String right(String text, int size)
    {
        return com.genexus.CommonUtil.right(text, size);
    }

    public static String left(String text, int size)
    {
        return com.genexus.CommonUtil.left(text, size);
    }

    public static String replicate(char character, int size)
    {
        return com.genexus.CommonUtil.replicate(character, size);
    }

    public static String replicate(String character, int size, int a)
    {
        return com.genexus.CommonUtil.replicate(character, size, a);
    }

    public static final String replicate(String character, int size)
    {
        return com.genexus.CommonUtil.replicate(character, size);
    }

    public static String ltrim(String text)
    {
        return com.genexus.CommonUtil.ltrim(text);
    }
    public static String ltrimstr(long val, int digits, int decimals)
    {
        return com.genexus.CommonUtil.ltrimstr(val, digits, decimals);
    }

    public static String ltrimstr(long[] val, int digits, int decimals)
    {
        return ltrimstr(val[0], digits, decimals);
    }


    public static String ltrimstr(double value, int length, int decimals)
    {
        return com.genexus.CommonUtil.ltrimstr(value, length, decimals);
    }
    public static String ltrimstr(double[] value, int length, int decimals)
    {
        return ltrimstr(value[0], length, decimals);
    }

    public static String ltrimstr(int[] value, int length, int decimals)
    {
        return ltrimstr(value[0], length, decimals);
    }
    public static String ltrimstr(short[] value, int length, int decimals)
    {
        return ltrimstr(value[0], length, decimals);
    }
    public static String ltrimstr(byte[] value, int length, int decimals)
    {
        return ltrimstr(value[0], length, decimals);
    }

    public static String time()
    {
        return com.genexus.CommonUtil.time();
    }

    public static Date addmth(Date date, int cnt)
    {
        return com.genexus.CommonUtil.addmth(date, cnt);
    }

    public static long lval(String text)
    {
        return com.genexus.CommonUtil.lval(text);
    }

    public static double val(String text)
    {
        return com.genexus.CommonUtil.val(text);
    }

    public static double val(String text, String sDSep)
    {
        return com.genexus.CommonUtil.val(text, sDSep);
    }

    public static boolean notNumeric(String value)
    {
        return com.genexus.CommonUtil.notNumeric(value);
    }

    public static boolean boolval(String text)
    {
        return com.genexus.CommonUtil.boolval(text);
    }

    public static Date eomdate(Date date)
    {
        return com.genexus.CommonUtil.eomdate(date);
    }

    public static Date eom(Date date)
    {
        return com.genexus.CommonUtil.eom(date);

    }

    public static Date dtadd(Date date, int seconds)
    {
        return com.genexus.CommonUtil.dtadd(date, seconds);
    }
    public static Date dtadd(Date date, double seconds)
    {
        return dtadd(date, (int)seconds);
    }

    public static Date dtaddms(Date date, double seconds)
    {
        return com.genexus.CommonUtil.dtaddms(date, seconds);
    }


    public static Date dadd(Date date, int cnt)
    {
        return com.genexus.CommonUtil.dadd(date, cnt);
    }
    public static Date dadd(Date date, double cnt)
    {
        return dadd(date, (int)cnt);
    }

    public static long dtdiff(Date dateStart, Date dateEnd)
    {
        return com.genexus.CommonUtil.dtdiff(dateStart, dateEnd);
    }

    public static double dtdiffms(Date dateStart, Date dateEnd)
    {
        return com.genexus.CommonUtil.dtdiffms(dateStart, dateEnd);
    }

    public static int ddiff(Date dateStart, Date dateEnd)
    {
        return (int) round(dtdiff(dateStart, dateEnd) / 86400.0, 0);
    }

    public static String concat(String first, String second)
    {
        return com.genexus.CommonUtil.concat(first, second);
    }

    public static String concat(String first, String second, String separator)
    {
        return com.genexus.CommonUtil.concat(first, second, separator);
    }










    static public boolean like( String str, String ptrn)
    {
        return com.genexus.CommonUtil.like(str, ptrn);
    }

    static public boolean like( String str, String ptrn, char escape)
    {
        return com.genexus.CommonUtil.like(str, ptrn, escape);
    }

    public static Date addyr(Date date, int yr)
    {
        return com.genexus.CommonUtil.addyr(date, yr);
    }

    public static int age(Date fn, Date today)
    {
        return com.genexus.CommonUtil.age(fn, today);
    }

    public static int age(Date dateStart)
    {
        return com.genexus.CommonUtil.age(dateStart);
    }

    public static int hour(Date date )
    {
        return com.genexus.CommonUtil.hour(date);
    }

    public static int minute(Date date )
    {
        return com.genexus.CommonUtil.minute(date);
    }

    public static int second(Date date )
    {
        return com.genexus.CommonUtil.second(date);
    }



    public static int day(Date date)
    {
        return com.genexus.CommonUtil.day(date);
    }

    public static int month(Date date)
    {
        return com.genexus.CommonUtil.month(date);
    }

    public static int  year(Date date)
    {
        return com.genexus.CommonUtil.year(date);
    }

    public static String getYYYYMMDD(Date date)
    {
        return com.genexus.CommonUtil.getYYYYMMDD(date);
    }

    public static String getYYYYMMDDHHMMSS_nosep(Date date)
    {
        return com.genexus.CommonUtil.getYYYYMMDDHHMMSS_nosep(date);
    }

    public static String getYYYYMMDDHHMMSSmmm_nosep(Date date)
    {
        return com.genexus.CommonUtil.getYYYYMMDDHHMMSSmmm_nosep(date);
    }

    public static String getYYYYMMDDHHMMSS(Date date)
    {
        return com.genexus.CommonUtil.getYYYYMMDDHHMMSS(date);
    }

    public static String getYYYYMMDDHHMMSSmmm(Date date)
    {
        return com.genexus.CommonUtil.getYYYYMMDDHHMMSSmmm(date);
    }

    public static String getMMDDHHMMSS(Date date)
    {
        return com.genexus.CommonUtil.getMMDDHHMMSS(date);
    }

    public static int len(String text)
    {
        return com.genexus.CommonUtil.len(text);
    }



    public static String space(int n)
    {
        return com.genexus.CommonUtil.space(n);
    }

    public static String trim(String text)
    {
        return com.genexus.CommonUtil.trim(text);
    }

    public static long Int(double num)
    {
        return com.genexus.CommonUtil.Int(num);
    }

    public static BigDecimal roundToEven(BigDecimal in, int decimals)
    {
        return com.genexus.CommonUtil.roundToEven(in, decimals);
    }

    public static BigDecimal roundDecimal(BigDecimal in, int decimals)
    {
        return com.genexus.CommonUtil.roundDecimal(in, decimals);
    }

    public static double round(double in, int decimals)
    {
        return com.genexus.CommonUtil.round(in, decimals);
    }

    public static BigDecimal truncDecimal(BigDecimal num , int decimals)
    {
        return com.genexus.CommonUtil.truncDecimal(num, decimals);
    }


    public static byte dow(Date date)
    {
        return com.genexus.CommonUtil.dow(date);
    }

    public static String getTimeFormat(String time)
    {
        return com.genexus.CommonUtil.getTimeFormat(time);
    }

    public static String chr(int asciiValue)
    {
        return com.genexus.CommonUtil.chr(asciiValue);
    }

    public static int asc(String value)
    {
        return com.genexus.CommonUtil.asc(value);
    }

    public static String wrkst()
    {
        return 	NativeFunctions.getInstance().getWorkstationName();
    }

      /**
     * @deprecated use serverNow(ModelContext context, int handle, com.genexus.db.IDataStoreProvider dataStore);
     */
    public static Date serverNow(ModelContext context, int handle, String dataSource)
    {
        return  now(true,false);
    }

    public static void msg(Object panel, String sText)
    {
        System.out.println(sText);
    }

    /**
     * @deprecated use serverTime(ModelContext context, int handle, com.genexus.db.IDataStoreProvider dataStore);
     * */

       /**
     * @deprecated use serverDate(ModelContext context, int handle, com.genexus.db.IDataStoreProvider dataStore);
     * */



    /**
     * @deprecated use userId(String key, int handle, com.genexus.db.IDataStoreProvider dataStore);
     * */
    public static String userId(String key, ModelContext context, int handle, String dataSource)
    {
        if	(!key.equalsIgnoreCase("server"))
        {
            try
            {
                return upper(System.getProperty("user.name"));
            }
            catch (SecurityException e)
            {
            }
        }

        String user = Application.getDBMSUser(context, handle, dataSource);

        if	(user == null)
        {
            System.err.println("Warning - userId('server') returned null");
            return "";
        }

        return upper(user);
    }

    public static byte openDocument(final String document)
    {
        try
        {
            NativeFunctions.getInstance().executeWithPermissions(
                    new Runnable() {
                        public void run()
                        {
                            int ret = NativeFunctions.getInstance().openDocument(document, 1);
                            if (ret != 0)
                                throw new RuntimeException();

                        }
                    }, INativeFunctions.ALL);
        }
        catch (RuntimeException e)
        {
            return 1;
        }

        return 0;
    }

    public static byte openPrintDocument(String document)
    {
        return (byte) NativeFunctions.getInstance().shellExecute(document, "print");
    }

    /** Hace un Shell modal. Ejecuta en la misma 'consola' que su parent
     * @param command Comando a ejecutar
     * @return true si el comando se pudo ejecutar
     */
    public static boolean shellModal(String command)
    {
        return NativeFunctions.getInstance().executeModal(command, true);
    }

    public static byte shell(String cmd, int modal)
    {
        if	(modal == 1)
            return shellModal(cmd)? 0 : (byte) 1;

        try
        {
            Runtime.getRuntime().exec(cmd);
        }
        catch (Exception e)
        {
            System.err.println("e " + e);
            return 1;
        }

        return 0;
    }

    public static byte shell(String cmd)
    {
        return shell(cmd, 0);
    }

    public static String getClassName(String pgmName)
    {
        // Esta la usa el developerMenu, que saca el package del client.cfg
        String classPackage = Application.getClientContext().getClientPreferences().getPACKAGE();

        if	(!classPackage.equals(""))
            classPackage += ".";

        return classPackage + pgmName.replace('\\', '.').trim();
    }

    public static String getObjectName(String packageName, String objectName)
    {
        return com.genexus.CommonUtil.getObjectName(packageName, objectName);
    }


    public static String classNameNoPackage(Class cls)
    {
        return com.genexus.CommonUtil.classNameNoPackage(cls);
    }

    public static byte sleep(long time)
    {
        return com.genexus.CommonUtil.sleep(time);
    }

    public static void errorHandler(String text, Exception ex)
    {
        PrivateUtilities.errorHandler(text, ex);
    }

    public static int gxmlines(String text, int lineLength)
    {
        return com.genexus.CommonUtil.gxmlines(text, lineLength);
    }

    public static String gxgetmli(String text, int line, int lineLength)
    {
        return com.genexus.CommonUtil.gxgetmli(text, line, lineLength);
    }

    public static String lower(String str)
    {
        return com.genexus.CommonUtil.lower(str);
    }

    public static String upper(String str)
    {
        return com.genexus.CommonUtil.upper(str);
    }

    public static String URLDecode(String s)
    {
        try
        {
            return Codecs.decode(s, "UTF8");
        }
        catch(  UnsupportedEncodingException | IllegalArgumentException e)
        {
            return s;
        }
    }

    public static String URLEncode(String s)
    {
        // Ponemos este wraper porque en JDK1.4+ esta deprecated y no queremos que
        // al compilar el código generado por GX nos de deprecations
        try
        {
            return Codecs.encode(s, "UTF8");
        }
        catch(  UnsupportedEncodingException e)
        {
            return s;
        }
    }

    public static String getDefaultFontName(String language, String defValue)
    {
        return com.genexus.CommonUtil.getDefaultFontName(language, defValue);
    }

    public static String ianaEncodingName(String encoding)
    {
        return com.genexus.CommonUtil.ianaEncodingName(encoding);
    }

    //Transforma un encoding name en nombre canónico (establecido por iana, domain Encoding en genexus) al nombre esperado por jdk
    public static String normalizeEncodingName(String enc)
    {
        return com.genexus.CommonUtil.normalizeEncodingName(enc, enc);
    }

    public static String normalizeEncodingName(String enc, String defaultEncoding)
    {
        return com.genexus.CommonUtil.normalizeEncodingName(enc, defaultEncoding);
    }


    //Transforma un encoding name en nombre canónico (establecido por iana, domain Encoding en genexus) al nombre esperado por jdk
    public static String normalizeSupportedEncodingName(String enc) throws Throwable
    {
        return com.genexus.CommonUtil.normalizeEncodingName(enc, enc);
    }



    public static String toValueList(String DBMS, Object arr, String prefix, String tail)
    {
        return com.genexus.CommonUtil.toValueList(DBMS, arr, prefix, tail);
    }

    private static String toValueList(String DBMS, Vector vec, String prefix, String tail)
    {
        return com.genexus.CommonUtil.toValueList(DBMS, vec, prefix, tail);
    }

    public static String dateToString(Date date, String DBMS)
    {
        return com.genexus.CommonUtil.dateToString(date, DBMS);
    }

    public static boolean contains(BigDecimal []arr, BigDecimal obj)
    {
        return com.genexus.CommonUtil.contains(arr, obj);
    }

    public static boolean contains(byte [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);

    }

    public static boolean contains(char [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(short [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(int [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(long [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(float [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(double [] arr, double item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(String []arr, String item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }

    public static boolean contains(Object []arr, Object item)
    {
        return com.genexus.CommonUtil.contains(arr, item);
    }
    public static String format(String value, String v1, String v2, String v3, String v4, String v5, String v6, String v7, String v8, String v9)
    {
        return com.genexus.CommonUtil.format(value, v1, v2, v3, v4, v5, v6, v7, v8, v9);
    }

    public static String getFileName( String sFullFileName)
    {
        return com.genexus.CommonUtil.getFileName(sFullFileName);
    }

    public static String getFileType( String sFullFileName)
    {
        return com.genexus.CommonUtil.getFileType(sFullFileName);
    }



    public static String getRelativeBlobFile(String path)
    {
        return com.genexus.CommonUtil.getRelativeBlobFile(path);
    }

    public static String getAbsoluteBlobFile(String path)
    {
        if (path.equals(""))
            return "";
        String blobPath = path;
        if (path.lastIndexOf('.') < 0)
            blobPath = blobPath + ".tmp";
        blobPath = blobPath.replace(com.genexus.Preferences.getDefaultPreferences().getProperty("CS_BLOB_PATH", "").trim(), "");
        return com.genexus.Preferences.getDefaultPreferences().getBLOB_PATH() + blobPath;
    }
    public static final String FORMDATA_REFERENCE = "gxformdataref:";
    public static final String UPLOADPREFIX = "gxupload:";
    public static final int UPLOAD_TIMEOUT = 10;


    public static boolean isUploadPrefix(String value)
    {
        return com.genexus.CommonUtil.isUploadPrefix(value);
    }

    public static String dateToCharREST(Date value)
    {
        return com.genexus.CommonUtil.dateToCharREST(value);
    }



    public static Date charToDateREST(String value)
    {
        return com.genexus.CommonUtil.charToDateREST(value);
    }




    public static Object testNumericType(Object obj, int type)
    {
        return com.genexus.CommonUtil.testNumericType(obj, type);
    }



    public static boolean compare(Comparable operand1, String op, Comparable operand2)
    {
        return com.genexus.CommonUtil.compare(operand1, op, operand2);
    }
/*
    public static Object convertObjectTo(Object obj, Class toClass) throws Exception
    {
        return com.genexus.CommonUtil.convertObjectTo( obj, toClass);
    }
*/
/*
    public static Class mapTypeToClass(int type)
    {
        return com.genexus.CommonUtil.mapTypeToClass(type);
    }
    public static Object convertObjectTo(Object obj, int type)
    {
        return com.genexus.CommonUtil.convertObjectTo(obj, type);
    }
*/
    public static boolean toBoolean(int value)
    {
        return com.genexus.CommonUtil.toBoolean(value);
    }

    public static String replaceLast(String string, String toReplace, String replacement) {
        return com.genexus.CommonUtil.replaceLast(string, toReplace, replacement);
    }


    public static java.util.UUID strToGuid(String value)
    {
        return com.genexus.CommonUtil.strToGuid(value);
    }


    public static boolean checkSignature( String sgn, String txt)
    {
        return com.genexus.CommonUtil.checkSignature(sgn, txt);
    }


    public static String getMD5Hash( String s)
    {
        return com.genexus.CommonUtil.getMD5Hash(s);
    }


    public static boolean isAbsoluteURL(String url)
    {
        return com.genexus.CommonUtil.isAbsoluteURL(url);
    }

    public static int getColor(int r, int g, int b)
    {
        return com.genexus.CommonUtil.getColor(r, g, b);
    }

    public static <T> T[] concatArrays(T[] first, T[] second) {
        return com.genexus.CommonUtil.concatArrays(first, second);
    }


    public static String pagingSelect(String select)
    {
        return com.genexus.CommonUtil.pagingSelect(select);
    }
/*
    public static String getEncryptedSignature( String value, String key)
    {
        return Encryption.encrypt64(com.genexus.CommonUtil.getHash( com.genexus.security.web.WebSecurityHelper.StripInvalidChars(value), com.genexus.cryptography.Constants.SECURITY_HASH_ALGORITHM), key);
    }
    public static boolean checkEncryptedSignature( String value, String hash, String key)
    {
        return com.genexus.CommonUtil.getHash( com.genexus.security.web.WebSecurityHelper.StripInvalidChars(value), com.genexus.cryptography.Constants.SECURITY_HASH_ALGORITHM).equals(Encryption.decrypt64(hash, key));
    }

*/

    public static String buildURLFromHttpClient(com.genexus.internet.HttpClient GXSoapHTTPClient, String serviceName, javax.xml.ws.BindingProvider bProvider)
    {
        if (!GXSoapHTTPClient.getProxyServerHost().equals(""))
        {
            bProvider.getRequestContext().put("https.proxyHost", GXSoapHTTPClient.getProxyServerHost());
        }

        if (GXSoapHTTPClient.getProxyServerPort() != 80)
        {
            bProvider.getRequestContext().put("https.proxyPort", GXSoapHTTPClient.getProxyServerPort());
        }

        String scheme = "http";
        if (GXSoapHTTPClient.getSecure() == 1)
        {
            scheme = "https";
        }
        java.net.URI url = null;
        try
        {
            url = new java.net.URI(scheme, null, GXSoapHTTPClient.getHost(), GXSoapHTTPClient.getPort(), GXSoapHTTPClient.getBaseURL() + serviceName, null, null);
        }
        catch(java.net.URISyntaxException e)
        {
            return "";
        }
        return url.toString();
    }

}
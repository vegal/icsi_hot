/*
               File: CompanyProcessor
        Description: Processadora de Cart�o de Cr�dito
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:59.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tcompanyprocessor extends GXTransaction
{
   public tcompanyprocessor( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcompanyprocessor.class ), "" );
   }

   public tcompanyprocessor( int remoteHandle ,
                             ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey2S260( )
   {
      A1420Compa = "" ;
   }

   public void initAll2S260( )
   {
      K1419Compa = A1419Compa ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey2S260( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption2S0( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "CompanyProcessor" ;
   }

   protected String getFrmTitle( )
   {
      return "Processadora de Cart�o de Cr�dito" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TCompanyProcessor.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      tcompanyprocessor.this.A1419Compa = aP0[0];
      this.aP0 = aP0;
      tcompanyprocessor.this.Gx_mode = aP1[0];
      this.aP1 = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtCompanyProcessorCode = new GUIObjectString ( new GXEdit(25, "XXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),196, 89, 185, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 196 , 89 , 185 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1419Compa" );
      ((GXEdit) edtCompanyProcessorCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtCompanyProcessorCode.addFocusListener(this);
      edtCompanyProcessorCode.getGXComponent().setHelpId("HLP_TCompanyProcessor.htm");
      edtCompanyProcessorLayout = new GUIObjectString ( new GXEdit(255, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),196, 113, 430, 87, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 196 , 113 , 430 , 87 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1420Compa" );
      ((GXEdit) edtCompanyProcessorLayout.getGXComponent()).setAlignment(ILabel.LEFT);
      edtCompanyProcessorLayout.addFocusListener(this);
      edtCompanyProcessorLayout.getGXComponent().setHelpId("HLP_TCompanyProcessor.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      bttbtt15 = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  393 ,  36 ,  90 ,  24 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttbtt15.setTooltip("Confirm");
      bttbtt15.addActionListener(this);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "Company Processor Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 145 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "Company Processor Layout", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 154 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtCompanyProcessorCode ,
                edtCompanyProcessorLayout ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttbtt15 ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      valid_Companyprocessorcode();
      setFocus(edtCompanyProcessorLayout, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey2S260( ) ;
   }

   public void disable_std_buttons( )
   {
      bttBtn_first.setGXEnabled( 0 );
      bttBtn_prev.setGXEnabled( 0 );
      bttBtn_next.setGXEnabled( 0 );
      bttBtn_last.setGXEnabled( 0 );
      bttBtn_exit2.setGXEnabled( 0 );
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtCompanyProcessorCode.setEnabled( 0 );
         edtCompanyProcessorLayout.setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll2S260( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttbtt15.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtCompanyProcessorCode.isEventSource(eventSource) ) {
         setGXCursor( edtCompanyProcessorCode.getGXCursor() );
         return;
      }
      if ( edtCompanyProcessorLayout.isEventSource(eventSource) ) {
         setGXCursor( edtCompanyProcessorLayout.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtCompanyProcessorCode.isEventSource(eventSource) ) {
         valid_Companyprocessorcode ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtCompanyProcessorCode.isEventSource(eventSource) ) {
         A1419Compa = edtCompanyProcessorCode.getValue() ;
         if ( ( GXutil.strcmp(A1419Compa, "") != 0 ) )
         {
            n1419Compa = false ;
         }
         return;
      }
      if ( edtCompanyProcessorLayout.isEventSource(eventSource) ) {
         A1420Compa = edtCompanyProcessorLayout.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtCompanyProcessorCode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1419Compa, edtCompanyProcessorCode.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtCompanyProcessorLayout.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1420Compa, edtCompanyProcessorLayout.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption2S0( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtCompanyProcessorLayout.isEventSource(eventSource) )
      {
         edtCompanyProcessorLayout.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void VariablesToControls( )
   {
      edtCompanyProcessorCode.setValue( A1419Compa );
      edtCompanyProcessorLayout.setValue( A1420Compa );
   }

   protected void ControlsToVariables( )
   {
      A1419Compa = edtCompanyProcessorCode.getValue() ;
      n1419Compa = false ;
      if ( ( GXutil.strcmp(A1419Compa, "") != 0 ) )
      {
         n1419Compa = false ;
      }
      A1420Compa = edtCompanyProcessorLayout.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Companyprocessorcode( )
   {
      if ( ( GXutil.strcmp(A1419Compa, K1419Compa) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K1419Compa = A1419Compa ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void e112S2( )
   {
      eventLevelContext();
      /* 'Back' Routine */
      if (canCleanup()) {
         returnInSub = true;
      }
      if (true) return;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
         {
            sMode260 = Gx_mode ;
            Gx_mode = "UPD" ;
            Gx_mode = sMode260 ;
         }
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            getByPrimaryKey( ) ;
            if ( ( RcdFound260 != 1 ) )
            {
               pushError( localUtil.getMessages().getMessage("noinsert") );
               AnyError = (short)(1) ;
               keepFocus();
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm2S260( int GX_JID )
   {
      if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1420Compa = T002S3_A1420Compa[0] ;
         }
         else
         {
            Z1420Compa = A1420Compa ;
         }
      }
      if ( ( GX_JID == -3 ) )
      {
         Z1419Compa = A1419Compa ;
         Z1420Compa = A1420Compa ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
      {
         edtCompanyProcessorCode.setEnabled( 0 );
      }
      else
      {
         edtCompanyProcessorCode.setEnabled( 1 );
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
      }
   }

   public void load2S260( )
   {
      /* Using cursor T002S4 */
      pr_default.execute(2, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound260 = (short)(1) ;
         A1420Compa = T002S4_A1420Compa[0] ;
         zm2S260( -3) ;
      }
      pr_default.close(2);
      onLoadActions2S260( ) ;
   }

   public void onLoadActions2S260( )
   {
   }

   public void checkExtendedTable2S260( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2S260( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey2S260( )
   {
      /* Using cursor T002S5 */
      pr_default.execute(3, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound260 = (short)(1) ;
      }
      else
      {
         RcdFound260 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T002S3 */
      pr_default.execute(1, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(1) != 101) && ( GXutil.strcmp(T002S3_A1419Compa[0], A1419Compa) == 0 ) )
      {
         zm2S260( 3) ;
         RcdFound260 = (short)(1) ;
         A1420Compa = T002S3_A1420Compa[0] ;
         Z1419Compa = A1419Compa ;
         sMode260 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2S260( ) ;
         Gx_mode = sMode260 ;
      }
      else
      {
         RcdFound260 = (short)(0) ;
         initializeNonKey2S260( ) ;
         sMode260 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode260 ;
      }
      K1419Compa = A1419Compa ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey2S260( ) ;
      if ( ( RcdFound260 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound260 = (short)(0) ;
      /* Using cursor T002S6 */
      pr_default.execute(4, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( GXutil.strcmp(T002S6_A1419Compa[0], A1419Compa) == 0 ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( GXutil.strcmp(T002S6_A1419Compa[0], A1419Compa) == 0 ) )
         {
            RcdFound260 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound260 = (short)(0) ;
      /* Using cursor T002S7 */
      pr_default.execute(5, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( GXutil.strcmp(T002S7_A1419Compa[0], A1419Compa) == 0 ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( GXutil.strcmp(T002S7_A1419Compa[0], A1419Compa) == 0 ) )
         {
            RcdFound260 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey2S260( ) ;
      if ( ( RcdFound260 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtCompanyProcessorCode );
         }
         else if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtCompanyProcessorCode );
         }
         else
         {
            /* Update record */
            update2S260( ) ;
            setNextFocus( edtCompanyProcessorCode );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
         {
            /* Insert record */
            insert2S260( ) ;
            setNextFocus( edtCompanyProcessorCode );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtCompanyProcessorCode );
            }
            else
            {
               /* Insert record */
               insert2S260( ) ;
               setNextFocus( edtCompanyProcessorCode );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
      {
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtCompanyProcessorCode );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtCompanyProcessorCode );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void checkOptimisticConcurrency2S260( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002S2 */
         pr_default.execute(0, new Object[] {new Boolean(n1419Compa), A1419Compa});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"COMPANYPROCESSOR"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1420Compa, T002S2_A1420Compa[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"COMPANYPROCESSOR"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2S260( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2S260( 0) ;
         checkOptimisticConcurrency2S260( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2S260( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002S8 */
                  pr_default.execute(6, new Object[] {new Boolean(n1419Compa), A1419Compa, A1420Compa});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2S260( ) ;
         }
         endLevel2S260( ) ;
      }
      closeExtendedTableCursors2S260( ) ;
   }

   public void update2S260( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2S260( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2S260( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002S9 */
                  pr_default.execute(7, new Object[] {A1420Compa, new Boolean(n1419Compa), A1419Compa});
                  deferredUpdate2S260( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2S260( ) ;
      }
      closeExtendedTableCursors2S260( ) ;
   }

   public void deferredUpdate2S260( )
   {
   }

   public void delete( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2S260( ) ;
         /* No cascading delete specified. */
         afterConfirm2S260( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T002S10 */
               pr_default.execute(8, new Object[] {new Boolean(n1419Compa), A1419Compa});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     loopOnce = true;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode260 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2S260( ) ;
      Gx_mode = sMode260 ;
   }

   public void onDeleteControls2S260( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor T002S11 */
         pr_default.execute(9, new Object[] {new Boolean(n1419Compa), A1419Compa});
         if ( (pr_default.getStatus(9) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"Cadastro de maquinetas"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(9);
      }
   }

   public void endLevel2S260( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2S260( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tcompanyprocessor");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tcompanyprocessor");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart2S260( )
   {
      /* Using cursor T002S12 */
      pr_default.execute(10, new Object[] {new Boolean(n1419Compa), A1419Compa});
      RcdFound260 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound260 = (short)(1) ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2S260( )
   {
      pr_default.readNext(10);
      RcdFound260 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound260 = (short)(1) ;
      }
   }

   public void scanEnd2S260( )
   {
      pr_default.close(10);
   }

   public void afterConfirm2S260( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2S260( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2S260( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2S260( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2S260( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2S260( )
   {
      /* Before Validate Rules */
   }

   public void confirm_2S0( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2S260( ) ;
         }
         else
         {
            checkExtendedTable2S260( ) ;
            closeExtendedTableCursors2S260( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_exit1.getBitmap() ;
         PreviousTooltip = bttBtn_exit1.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = tcompanyprocessor.this.A1419Compa;
      this.aP1[0] = tcompanyprocessor.this.Gx_mode;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A1420Compa = "" ;
      lastAnyError = 0 ;
      n1419Compa = false ;
      returnInSub = false ;
      sMode260 = "" ;
      RcdFound260 = (short)(0) ;
      Z1420Compa = "" ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z1419Compa = "" ;
      T002S4_A1419Compa = new String[] {""} ;
      T002S4_n1419Compa = new boolean[] {false} ;
      T002S4_A1420Compa = new String[] {""} ;
      Gx_BScreen = (byte)(0) ;
      T002S5_A1419Compa = new String[] {""} ;
      T002S5_n1419Compa = new boolean[] {false} ;
      T002S3_A1419Compa = new String[] {""} ;
      T002S3_n1419Compa = new boolean[] {false} ;
      T002S3_A1420Compa = new String[] {""} ;
      T002S6_A1419Compa = new String[] {""} ;
      T002S6_n1419Compa = new boolean[] {false} ;
      T002S7_A1419Compa = new String[] {""} ;
      T002S7_n1419Compa = new boolean[] {false} ;
      T002S2_A1419Compa = new String[] {""} ;
      T002S2_n1419Compa = new boolean[] {false} ;
      T002S2_A1420Compa = new String[] {""} ;
      T002S11_A1313CadIA = new String[] {""} ;
      T002S11_A1423POSDa = new java.util.Date[] {GXutil.nullDate()} ;
      T002S11_A1237ICSI_ = new String[] {""} ;
      T002S12_A1419Compa = new String[] {""} ;
      T002S12_n1419Compa = new boolean[] {false} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tcompanyprocessor__default(),
         new Object[] {
             new Object[] {
            T002S2_A1419Compa, T002S2_A1420Compa
            }
            , new Object[] {
            T002S3_A1419Compa, T002S3_A1420Compa
            }
            , new Object[] {
            T002S4_A1419Compa, T002S4_A1420Compa
            }
            , new Object[] {
            T002S5_A1419Compa
            }
            , new Object[] {
            T002S6_A1419Compa
            }
            , new Object[] {
            T002S7_A1419Compa
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002S11_A1313CadIA, T002S11_A1423POSDa, T002S11_A1237ICSI_
            }
            , new Object[] {
            T002S12_A1419Compa
            }
         }
      );
      reloadDynamicLists(0);
      K1419Compa = "" ;
      edtCompanyProcessorCode.setValue(A1419Compa);
      n1419Compa = false ;
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound260 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String sMode260 ;
   protected String scmdbuf ;
   protected boolean n1419Compa ;
   protected boolean returnInSub ;
   protected String A1420Compa ;
   protected String K1419Compa ;
   protected String A1419Compa ;
   protected String Z1420Compa ;
   protected String Z1419Compa ;
   protected String[] aP0 ;
   protected String[] aP1 ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtCompanyProcessorCode ;
   protected GUIObjectString edtCompanyProcessorLayout ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected IGXButton bttbtt15 ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T002S4_A1419Compa ;
   protected boolean[] T002S4_n1419Compa ;
   protected String[] T002S4_A1420Compa ;
   protected String[] T002S5_A1419Compa ;
   protected boolean[] T002S5_n1419Compa ;
   protected String[] T002S3_A1419Compa ;
   protected boolean[] T002S3_n1419Compa ;
   protected String[] T002S3_A1420Compa ;
   protected String[] T002S6_A1419Compa ;
   protected boolean[] T002S6_n1419Compa ;
   protected String[] T002S7_A1419Compa ;
   protected boolean[] T002S7_n1419Compa ;
   protected String[] T002S2_A1419Compa ;
   protected boolean[] T002S2_n1419Compa ;
   protected String[] T002S2_A1420Compa ;
   protected String[] T002S11_A1313CadIA ;
   protected java.util.Date[] T002S11_A1423POSDa ;
   protected String[] T002S11_A1237ICSI_ ;
   protected String[] T002S12_A1419Compa ;
   protected boolean[] T002S12_n1419Compa ;
}

final  class tcompanyprocessor__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T002S2", "SELECT [CompanyProcessorCode], [CompanyProcessorLayout] FROM [COMPANYPROCESSOR] WITH (UPDLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002S3", "SELECT [CompanyProcessorCode], [CompanyProcessorLayout] FROM [COMPANYPROCESSOR] WITH (NOLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002S4", "SELECT TM1.[CompanyProcessorCode], TM1.[CompanyProcessorLayout] FROM [COMPANYPROCESSOR] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[CompanyProcessorCode] = ? ORDER BY TM1.[CompanyProcessorCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002S5", "SELECT [CompanyProcessorCode] FROM [COMPANYPROCESSOR] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002S6", "SELECT TOP 1 [CompanyProcessorCode] FROM [COMPANYPROCESSOR] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyProcessorCode] = ? ORDER BY [CompanyProcessorCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002S7", "SELECT TOP 1 [CompanyProcessorCode] FROM [COMPANYPROCESSOR] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyProcessorCode] = ? ORDER BY [CompanyProcessorCode] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T002S8", "INSERT INTO [COMPANYPROCESSOR] ([CompanyProcessorCode], [CompanyProcessorLayout]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002S9", "UPDATE [COMPANYPROCESSOR] SET [CompanyProcessorLayout]=?  WHERE [CompanyProcessorCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T002S10", "DELETE FROM [COMPANYPROCESSOR]  WHERE [CompanyProcessorCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T002S11", "SELECT TOP 1 [CadIATA], [POSData], [ICSI_CCConfCod] FROM [CADASTROSPOS] WITH (NOLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002S12", "SELECT [CompanyProcessorCode] FROM [COMPANYPROCESSOR] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyProcessorCode] = ? ORDER BY [CompanyProcessorCode] ",true, GX_NOMASK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 3 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 255, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 25, false);
               }
               break;
            case 8 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25);
               }
               break;
            case 10 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
      }
   }

}


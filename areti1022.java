/*
               File: RETi1022
        Description: Gera��o dos Relat�rios i1022/i1023 (Main)
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 18:43:4.15
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class areti1022 extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      areti1022 pgm = new areti1022 (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0);
   }

   public areti1022( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( areti1022.class ), "" );
   }

   public areti1022( int remoteHandle ,
                     ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      areti1022.this.AV8DebugMo = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV9Versao = "00018" ;
      AV8DebugMo = GXutil.trim( GXutil.upper( AV8DebugMo)) ;
      if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "i1022/i1023 - Version "+AV9Versao );
         context.msgStatus( "  Running mode: ["+AV8DebugMo+"] - Started at "+GXutil.time( ) );
      }
      /* Execute user subroutine: S1123 */
      S1123 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "  Ended at "+GXutil.time( ) );
      }
      else
      {
         GXutil.msg( this, "T�rmino do processo" );
      }
      cleanup();
   }

   public void S1123( )
   {
      /* 'MAIN' Routine */
      GXt_char2 = AV10Path ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_4122", "Caminho do R4122", "F", "C:\\Temp\\ICSI\\R4122", GXv_svchar3) ;
      areti1022.this.GXt_char2 = GXv_svchar3[0] ;
      AV10Path = GXt_char2 ;
      GXt_char2 = AV11Path1 ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_4122_BSPLink", "Caminho do R4122", "F", "C:\\Temp\\ICSI\\R4122\\BSPLink", GXv_svchar3) ;
      areti1022.this.GXt_char2 = GXv_svchar3[0] ;
      AV11Path1 = GXt_char2 ;
      AV12DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) ;
      AV12DataB = AV12DataB + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) ;
      AV12DataB = AV12DataB + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      /* Using cursor P006L2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1147lccbE = P006L2_A1147lccbE[0] ;
         n1147lccbE = P006L2_n1147lccbE[0] ;
         A1150lccbE = P006L2_A1150lccbE[0] ;
         AV15lccbEm = GXutil.trim( A1150lccbE) ;
         /* Execute user subroutine: S122 */
         S122 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) > 0 ) )
         {
            context.msgStatus( "    Creating Reports for "+AV15lccbEm+"("+AV16EmpNom+")..." );
         }
         AV13PathPD = AV10Path + "tamlccbgdstrb" + GXutil.substring( GXutil.trim( AV12DataB), 3, 6) + ".pdf" ;
         AV14PathTX = AV10Path + "tamlccbgdstrb" + GXutil.substring( GXutil.trim( AV12DataB), 3, 6) + ".txt" ;
         if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) > 0 ) )
         {
            context.msgStatus( "    "+AV13PathPD );
            context.msgStatus( "    "+AV14PathTX );
         }
         AV25GXLvl5 = (byte)(0) ;
         /* Using cursor P006L3 */
         pr_default.execute(1, new Object[] {AV15lccbEm, AV15lccbEm});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1488ICSI_ = P006L3_A1488ICSI_[0] ;
            A1487ICSI_ = P006L3_A1487ICSI_[0] ;
            A1479ICSI_ = P006L3_A1479ICSI_[0] ;
            n1479ICSI_ = P006L3_n1479ICSI_[0] ;
            AV25GXLvl5 = (byte)(1) ;
            A1479ICSI_ = (int)(A1479ICSI_+1) ;
            n1479ICSI_ = false ;
            if ( ( A1479ICSI_ > 99999 ) )
            {
               A1479ICSI_ = 1 ;
               n1479ICSI_ = false ;
            }
            AV26Icsi_c = A1479ICSI_ ;
            /* Using cursor P006L4 */
            pr_default.execute(2, new Object[] {new Boolean(n1479ICSI_), new Integer(A1479ICSI_), A1487ICSI_, A1488ICSI_});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( ( AV25GXLvl5 == 0 ) )
         {
            context.msgStatus( "  Warning: "+AV15lccbEm+" not configured, assuming file sequence = 1 " );
            AV26Icsi_c = 1 ;
            if ( ( AV27Testmo == 0 ) )
            {
               /*
                  INSERT RECORD ON TABLE ICSI_CCINFO

               */
               A1487ICSI_ = AV15lccbEm ;
               A1479ICSI_ = 1 ;
               n1479ICSI_ = false ;
               A1488ICSI_ = "I1023" ;
               /* Using cursor P006L5 */
               pr_default.execute(3, new Object[] {A1487ICSI_, A1488ICSI_, new Boolean(n1479ICSI_), new Integer(A1479ICSI_)});
               if ( (pr_default.getStatus(3) == 1) )
               {
                  Gx_err = (short)(1) ;
                  Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               }
               else
               {
                  Gx_err = (short)(0) ;
                  Gx_emsg = "" ;
               }
               /* End Insert */
            }
         }
         GXv_svchar3[0] = AV15lccbEm ;
         GXv_char4[0] = AV16EmpNom ;
         GXv_char5[0] = AV13PathPD ;
         GXv_char6[0] = AV14PathTX ;
         GXv_char7[0] = AV8DebugMo ;
         GXv_int8[0] = AV26Icsi_c ;
         new ppreti1022rpt(remoteHandle, context).execute( GXv_svchar3, GXv_char4, GXv_char5, GXv_char6, GXv_char7, GXv_int8) ;
         areti1022.this.AV15lccbEm = GXv_svchar3[0] ;
         areti1022.this.AV16EmpNom = GXv_char4[0] ;
         areti1022.this.AV13PathPD = GXv_char5[0] ;
         areti1022.this.AV14PathTX = GXv_char6[0] ;
         areti1022.this.AV8DebugMo = GXv_char7[0] ;
         areti1022.this.AV26Icsi_c = GXv_int8[0] ;
         if ( ( GXutil.strSearch( AV8DebugMo, "NOMOVE", 1) == 0 ) )
         {
            AV17Comman = "cmd /c move " + AV13PathPD + " " + AV11Path1 ;
            AV18Ret = GXutil.shell( AV17Comman, 1) ;
            AV17Comman = "cmd /c move " + AV14PathTX + " " + AV11Path1 ;
            AV18Ret = GXutil.shell( AV17Comman, 1) ;
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
   }

   public void S122( )
   {
      /* 'GETEMPNOM' Routine */
      AV28GXLvl9 = (byte)(0) ;
      /* Using cursor P006L6 */
      pr_default.execute(4, new Object[] {AV15lccbEm});
      while ( (pr_default.getStatus(4) != 101) )
      {
         A1233EmpCo = P006L6_A1233EmpCo[0] ;
         A1234EmpNo = P006L6_A1234EmpNo[0] ;
         n1234EmpNo = P006L6_n1234EmpNo[0] ;
         AV28GXLvl9 = (byte)(1) ;
         AV16EmpNom = GXutil.trim( A1234EmpNo) ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(4);
      if ( ( AV28GXLvl9 == 0 ) )
      {
         AV16EmpNom = "???" ;
      }
   }

  /* public static Object refClasses( )
   {
      GXutil.refClasses(preti1022.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = areti1022.this.AV8DebugMo;
      Application.commit(context, remoteHandle, "DEFAULT", "areti1022");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9Versao = "" ;
      returnInSub = false ;
      AV10Path = "" ;
      AV11Path1 = "" ;
      AV12DataB = "" ;
      Gx_date = GXutil.nullDate() ;
      scmdbuf = "" ;
      P006L2_A1147lccbE = new String[] {""} ;
      P006L2_n1147lccbE = new boolean[] {false} ;
      P006L2_A1150lccbE = new String[] {""} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      AV15lccbEm = "" ;
      AV16EmpNom = "" ;
      AV13PathPD = "" ;
      AV14PathTX = "" ;
      GXt_char1 = "" ;
      AV25GXLvl5 = (byte)(0) ;
      P006L3_A1488ICSI_ = new String[] {""} ;
      P006L3_A1487ICSI_ = new String[] {""} ;
      P006L3_A1479ICSI_ = new int[1] ;
      P006L3_n1479ICSI_ = new boolean[] {false} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      A1479ICSI_ = 0 ;
      n1479ICSI_ = false ;
      AV26Icsi_c = 0 ;
      GXt_char2 = "" ;
      AV27Testmo = 0 ;
      GX_INS273 = 0 ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      GXv_svchar3 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_char5 = new String [1] ;
      GXv_char6 = new String [1] ;
      GXv_char7 = new String [1] ;
      GXv_int8 = new int [1] ;
      AV17Comman = "" ;
      AV18Ret = 0 ;
      AV28GXLvl9 = (byte)(0) ;
      P006L6_A1233EmpCo = new String[] {""} ;
      P006L6_A1234EmpNo = new String[] {""} ;
      P006L6_n1234EmpNo = new boolean[] {false} ;
      A1233EmpCo = "" ;
      A1234EmpNo = "" ;
      n1234EmpNo = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new areti1022__default(),
         new Object[] {
             new Object[] {
            P006L2_A1147lccbE, P006L2_n1147lccbE, P006L2_A1150lccbE
            }
            , new Object[] {
            P006L3_A1488ICSI_, P006L3_A1487ICSI_, P006L3_A1479ICSI_, P006L3_n1479ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006L6_A1233EmpCo, P006L6_A1234EmpNo, P006L6_n1234EmpNo
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV25GXLvl5 ;
   private byte AV28GXLvl9 ;
   private short Gx_err ;
   private int A1479ICSI_ ;
   private int AV26Icsi_c ;
   private int GX_INS273 ;
   private int GXv_int8[] ;
   private double AV27Testmo ;
   private double AV18Ret ;
   private String AV8DebugMo ;
   private String AV9Versao ;
   private String AV10Path ;
   private String AV11Path1 ;
   private String AV12DataB ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String AV15lccbEm ;
   private String AV16EmpNom ;
   private String AV13PathPD ;
   private String AV14PathTX ;
   private String GXt_char1 ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String GXt_char2 ;
   private String Gx_emsg ;
   private String GXv_char4[] ;
   private String GXv_char5[] ;
   private String GXv_char6[] ;
   private String GXv_char7[] ;
   private String AV17Comman ;
   private String A1233EmpCo ;
   private String A1234EmpNo ;
   private java.util.Date Gx_date ;
   private boolean returnInSub ;
   private boolean n1147lccbE ;
   private boolean n1479ICSI_ ;
   private boolean n1234EmpNo ;
   private String GXv_svchar3[] ;
   private String[] aP0 ;
   private IDataStoreProvider pr_default ;
   private String[] P006L2_A1147lccbE ;
   private boolean[] P006L2_n1147lccbE ;
   private String[] P006L2_A1150lccbE ;
   private String[] P006L3_A1488ICSI_ ;
   private String[] P006L3_A1487ICSI_ ;
   private int[] P006L3_A1479ICSI_ ;
   private boolean[] P006L3_n1479ICSI_ ;
   private String[] P006L6_A1233EmpCo ;
   private String[] P006L6_A1234EmpNo ;
   private boolean[] P006L6_n1234EmpNo ;
}

final  class areti1022__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006L2", "SELECT [lccbEmpEnab], [lccbEmpCod] FROM [LCCBEMP] WITH (NOLOCK) WHERE [lccbEmpEnab] = '1' ORDER BY [lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006L3", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCSeqFile] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = ? AND [ICSI_CCCod] = 'I1023') AND ([ICSI_EmpCod] = ? and [ICSI_CCCod] = 'I1023') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P006L4", "UPDATE [ICSI_CCINFO] SET [ICSI_CCSeqFile]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006L5", "INSERT INTO [ICSI_CCINFO] ([ICSI_EmpCod], [ICSI_CCCod], [ICSI_CCSeqFile], [ICSI_CCLote], [ICSI_CCPOS1], [ICSI_CCPOS2], [ICSI_CCPOS3], [ICSI_SplitPLP], [ICSI_CCEstab], [ICSI_CCNome]) VALUES (?, ?, ?, convert(int, 0), '', '', '', convert(int, 0), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006L6", "SELECT [EmpCod], [EmpNom] FROM [EMPRESAS] WITH (NOLOCK) WHERE [EmpCod] = ? ORDER BY [EmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((int[]) buf[2])[0] = rslt.getInt(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 3);
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 10);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(3, ((Number) parms[3]).intValue());
               }
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 3);
               break;
      }
   }

}


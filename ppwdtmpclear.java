/*
               File: PwdTmpClear
        Description: Apagar a senha temporária
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class ppwdtmpclear extends GXProcedure
{
   public ppwdtmpclear( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( ppwdtmpclear.class ), "" );
   }

   public ppwdtmpclear( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      ppwdtmpclear.this.AV8lgnLogi = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      /* Optimized UPDATE. */
      /* Using cursor P00212 */
      pr_default.execute(0, new Object[] {AV8lgnLogi});
      /* End optimized UPDATE. */
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = ppwdtmpclear.this.AV8lgnLogi;
      Application.commit(context, remoteHandle, "DEFAULT", "ppwdtmpclear");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      pr_default = new DataStoreProvider(context, remoteHandle, new ppwdtmpclear__default(),
         new Object[] {
             new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV8lgnLogi ;
   private String[] aP0 ;
   private IDataStoreProvider pr_default ;
}

final  class ppwdtmpclear__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P00212", "UPDATE [LOGINS] SET [lgnPwdTmp]=''  WHERE [lgnLogin] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20);
               break;
      }
   }

}


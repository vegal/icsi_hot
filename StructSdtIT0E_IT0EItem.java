
public final  class StructSdtIT0E_IT0EItem implements Cloneable, java.io.Serializable
{
   public StructSdtIT0E_IT0EItem( )
   {
      gxTv_SdtIT0E_IT0EItem_Plid_it0e = "" ;
      gxTv_SdtIT0E_IT0EItem_Spin_it0e = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getPlid_it0e( )
   {
      return gxTv_SdtIT0E_IT0EItem_Plid_it0e ;
   }

   public void setPlid_it0e( String value )
   {
      gxTv_SdtIT0E_IT0EItem_Plid_it0e = value ;
      return  ;
   }

   public String getSpin_it0e( )
   {
      return gxTv_SdtIT0E_IT0EItem_Spin_it0e ;
   }

   public void setSpin_it0e( String value )
   {
      gxTv_SdtIT0E_IT0EItem_Spin_it0e = value ;
      return  ;
   }

   protected String gxTv_SdtIT0E_IT0EItem_Plid_it0e ;
   protected String gxTv_SdtIT0E_IT0EItem_Spin_it0e ;
}


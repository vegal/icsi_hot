import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public class tairlines_bc_ws extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new tairlines_bc(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Airline Data";
   }

}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtIT0E_IT0EItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtIT0E_IT0EItem( )
   {
      this(  new ModelContext(SdtIT0E_IT0EItem.class));
   }

   public SdtIT0E_IT0EItem( ModelContext context )
   {
      super( context, "SdtIT0E_IT0EItem");
   }

   public SdtIT0E_IT0EItem( StructSdtIT0E_IT0EItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PLID_IT0E") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0E_IT0EItem_Plid_it0e = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "SPIN_IT0E") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0E_IT0EItem_Spin_it0e = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "IT0E.IT0EItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("PLID_IT0E", GXutil.rtrim( gxTv_SdtIT0E_IT0EItem_Plid_it0e));
      oWriter.writeElement("SPIN_IT0E", GXutil.rtrim( gxTv_SdtIT0E_IT0EItem_Spin_it0e));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtIT0E_IT0EItem_Plid_it0e( )
   {
      return gxTv_SdtIT0E_IT0EItem_Plid_it0e ;
   }

   public void setgxTv_SdtIT0E_IT0EItem_Plid_it0e( String value )
   {
      gxTv_SdtIT0E_IT0EItem_Plid_it0e = value ;
      return  ;
   }

   public void setgxTv_SdtIT0E_IT0EItem_Plid_it0e_SetNull( )
   {
      gxTv_SdtIT0E_IT0EItem_Plid_it0e = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0E_IT0EItem_Spin_it0e( )
   {
      return gxTv_SdtIT0E_IT0EItem_Spin_it0e ;
   }

   public void setgxTv_SdtIT0E_IT0EItem_Spin_it0e( String value )
   {
      gxTv_SdtIT0E_IT0EItem_Spin_it0e = value ;
      return  ;
   }

   public void setgxTv_SdtIT0E_IT0EItem_Spin_it0e_SetNull( )
   {
      gxTv_SdtIT0E_IT0EItem_Spin_it0e = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtIT0E_IT0EItem_Plid_it0e = "" ;
      gxTv_SdtIT0E_IT0EItem_Spin_it0e = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char4 = "" ;
      return  ;
   }

   public SdtIT0E_IT0EItem Clone( )
   {
      return (SdtIT0E_IT0EItem)(clone()) ;
   }

   public void setStruct( StructSdtIT0E_IT0EItem struct )
   {
      setgxTv_SdtIT0E_IT0EItem_Plid_it0e(struct.getPlid_it0e());
      setgxTv_SdtIT0E_IT0EItem_Spin_it0e(struct.getSpin_it0e());
   }

   public StructSdtIT0E_IT0EItem getStruct( )
   {
      StructSdtIT0E_IT0EItem struct = new StructSdtIT0E_IT0EItem ();
      struct.setPlid_it0e(getgxTv_SdtIT0E_IT0EItem_Plid_it0e());
      struct.setSpin_it0e(getgxTv_SdtIT0E_IT0EItem_Spin_it0e());
      return struct ;
   }

   private short nOutParmCount ;
   private short readOk ;
   private String sTagName ;
   private String GXt_char4 ;
   private String gxTv_SdtIT0E_IT0EItem_Plid_it0e ;
   private String gxTv_SdtIT0E_IT0EItem_Spin_it0e ;
}


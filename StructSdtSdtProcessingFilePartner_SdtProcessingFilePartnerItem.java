
public final  class StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem implements Cloneable, java.io.Serializable
{
   public StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem( )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code = "" ;
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename = "" ;
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getCode( )
   {
      return gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code ;
   }

   public void setCode( String value )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code = value ;
      return  ;
   }

   public String getTradename( )
   {
      return gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename ;
   }

   public void setTradename( String value )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename = value ;
      return  ;
   }

   public String getWarning( )
   {
      return gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning ;
   }

   public void setWarning( String value )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning = value ;
      return  ;
   }

   protected String gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code ;
   protected String gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename ;
   protected String gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning ;
}


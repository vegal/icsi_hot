/*
               File: AlteraBaseDados
        Description: Altera Base Dados
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:26.90
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class ualterabasedados extends GXWorkpanel
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      ualterabasedados pgm = new ualterabasedados (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public ualterabasedados( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( ualterabasedados.class ));
   }

   public ualterabasedados( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "AlteraBaseDados" ;
   }

   protected String getFrmTitle( )
   {
      return "Altera Base Dados" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 837 ;
   }

   protected int getFrmHeight( )
   {
      return 94 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WAlteraBaseDados.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return true;
   }

   protected boolean isModal( )
   {
      return false;
   }

   protected boolean hasDBAccess( )
   {
      return false;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      start();
   }

   protected void standAlone( )
   {
      /* Execute user event: e11V0W2 */
      e11V0W2 ();
   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      standAlone();
      VariablesToControls();
      /* End function GeneXus Refresh */
   }

   protected void GXStart( )
   {
      /* Execute user event: e12V0W2 */
      e12V0W2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V0W2( )
   {
      eventNoLevelContext();
      /* Start Routine */
      AV6lccbTab = "L" ;
      cmbavLccbtabela.setValue(AV6lccbTab);
   }

   public void GXEnter( )
   {
      /* Execute user event: e13V0W2 */
      e13V0W2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e13V0W2( )
   {
      eventLevelContext();
      /* Enter Routine */
      if ( ( AV5lccbAno <= 2009 ) && ( AV5lccbAno > 0 ) )
      {
         GXutil.msg( me(), "ATEN��O...O Ano precisa ser maior ou igual a 2010 ou igual a zero!" );
      }
      else
      {
        // GXutil.confirm( me(), "Deseja Executer a Encripta��o ?" , false );
         if ( GXutil.Confirmed )
         {
            if ( ( AV5lccbAno > 0 ) )
            {
               AV9DataCar = "01/01/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV7LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               AV9DataCar = "12/31/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV8LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               GXv_date1[0] = AV7LccbDat ;
               GXv_int2[0] = AV11Count ;
               new palteradadosencriptacao(remoteHandle, context).execute( AV6lccbTab, GXv_date1, AV8LccbDat, GXv_int2) ;
               ualterabasedados.this.AV7LccbDat = GXv_date1[0] ;
               ualterabasedados.this.AV11Count = GXv_int2[0] ;
               Gx_msg = "Opera��o Efetuada com Sucesso. Total Registros Alterados: " + localUtil.format( AV11Count, "ZZZZZZZ9") ;
               GXutil.msg( me(), Gx_msg );
            }
            else
            {
               AV12CountT = 0 ;
               AV5lccbAno = (short)(2010) ;
               edtavLccbano.setValue(AV5lccbAno);
               AV9DataCar = "01/01/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV7LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               AV9DataCar = "12/31/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV8LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               GXv_date1[0] = AV7LccbDat ;
               GXv_int2[0] = AV11Count ;
               new palteradadosencriptacao(remoteHandle, context).execute( AV6lccbTab, GXv_date1, AV8LccbDat, GXv_int2) ;
               ualterabasedados.this.AV7LccbDat = GXv_date1[0] ;
               ualterabasedados.this.AV11Count = GXv_int2[0] ;
               AV12CountT = (long)(AV12CountT+AV11Count) ;
               AV5lccbAno = (short)(2011) ;
               edtavLccbano.setValue(AV5lccbAno);
               AV9DataCar = "01/01/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV7LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               AV9DataCar = "12/31/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV8LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               GXv_date1[0] = AV7LccbDat ;
               GXv_int2[0] = AV11Count ;
               new palteradadosencriptacao(remoteHandle, context).execute( AV6lccbTab, GXv_date1, AV8LccbDat, GXv_int2) ;
               ualterabasedados.this.AV7LccbDat = GXv_date1[0] ;
               ualterabasedados.this.AV11Count = GXv_int2[0] ;
               AV12CountT = (long)(AV12CountT+AV11Count) ;
               AV5lccbAno = (short)(2012) ;
               edtavLccbano.setValue(AV5lccbAno);
               AV9DataCar = "01/01/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV7LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               AV9DataCar = "12/31/" + localUtil.format( AV5lccbAno, "ZZZ9") ;
               AV8LccbDat = localUtil.ctod( AV9DataCar, 2) ;
               GXv_date1[0] = AV7LccbDat ;
               GXv_int2[0] = AV11Count ;
               new palteradadosencriptacao(remoteHandle, context).execute( AV6lccbTab, GXv_date1, AV8LccbDat, GXv_int2) ;
               ualterabasedados.this.AV7LccbDat = GXv_date1[0] ;
               ualterabasedados.this.AV11Count = GXv_int2[0] ;
               AV12CountT = (long)(AV12CountT+AV11Count) ;
               Gx_msg = "Opera��o Efetuada com Sucesso. Total Registros Alterados: " + localUtil.format( AV12CountT, "ZZZZZZZZZZZZZ9") ;
               GXutil.msg( me(), Gx_msg );
               AV5lccbAno = (short)(0) ;
               edtavLccbano.setValue(AV5lccbAno);
            }
         }
      }
      eventLevelResetContext();
   }

   protected void nextLoad( )
   {
   }

   protected void e11V0W2( )
   {
      /* Load Routine */
      nextLoad();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 24 , 837 , 94 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtavLccbano = new GUIObjectShort ( new GXEdit(4, "ZZZ9", UIFactory.getFont( "Courier New", 0, 9),74, 20, 38, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 74 , 20 , 38 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5lccbAno" );
      ((GXEdit) edtavLccbano.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtavLccbano.addFocusListener(this);
      edtavLccbano.getGXComponent().setHelpId("HLP_WAlteraBaseDados.htm");
      cmbavLccbtabela = new GUIObjectString ( new GXComboBox(GXPanel1) , GXPanel1 , 74 , 48 , 82 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6lccbTab" );
      ((GXComboBox) cmbavLccbtabela.getGXComponent()).addItem( "L","LCCBPLP");
      ((GXComboBox) cmbavLccbtabela.getGXComponent()).addItem( "L1","LCCBPLP1");
      ((GXComboBox) cmbavLccbtabela.getGXComponent()).addItem( "L2","LCCBPLP2");
      cmbavLccbtabela.addFocusListener(this);
      cmbavLccbtabela.addItemListener(this);
      cmbavLccbtabela.getGXComponent().setHelpId("HLP_WAlteraBaseDados.htm");
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  717 ,  21 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  717 ,  49 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      lbllbl5 = UIFactory.getLabel(GXPanel1, "Ano:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 18 , 24 , 27 , 13 );
      lbllbl6 = UIFactory.getLabel(GXPanel1, "Tabela:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 18 , 52 , 44 , 13 );
      rctrct4 = UIFactory.getGXRectangle( GXPanel1 , 1 , 3 , 8 , 828 , 81 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      focusManager.setControlList(new IFocusableControl[] {
                edtavLccbano ,
                cmbavLccbtabela ,
                bttBtn_enter ,
                bttBtn_cancel
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtavLccbano, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void VariablesToControls( )
   {
      edtavLccbano.setValue( AV5lccbAno );
      cmbavLccbtabela.setValue( AV6lccbTab );
   }

   protected void ControlsToVariables( )
   {
      AV5lccbAno = edtavLccbano.getValue() ;
      AV6lccbTab = cmbavLccbtabela.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e13V0W2 */
         e13V0W2 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavLccbano.isEventSource(eventSource) ) {
         setGXCursor( edtavLccbano.getGXCursor() );
         return;
      }
      if ( cmbavLccbtabela.isEventSource(eventSource) ) {
         setGXCursor( cmbavLccbtabela.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavLccbano.isEventSource(eventSource) ) {
         AV5lccbAno = edtavLccbano.getValue() ;
         return;
      }
      if ( cmbavLccbtabela.isEventSource(eventSource) ) {
         AV6lccbTab = cmbavLccbtabela.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e13V0W2 */
         e13V0W2 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(walterabasedados.class);
      return new GXcfg();
   }
*/
   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
      if  (array.equals("GXv_int2"))  {
      }
      if  (array.equals("GXv_date1"))  {
      }
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      returnInSub = false ;
      AV6lccbTab = "" ;
      AV5lccbAno = (short)(0) ;
      AV9DataCar = "" ;
      AV7LccbDat = GXutil.nullDate() ;
      AV8LccbDat = GXutil.nullDate() ;
      AV11Count = 0 ;
      Gx_msg = "" ;
      AV12CountT = 0 ;
      GXv_date1 = new java.util.Date [1] ;
      GXv_int2 = new int [1] ;
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short AV5lccbAno ;
   protected short Gx_err ;
   protected int AV11Count ;
   protected int GXv_int2[] ;
   protected long AV12CountT ;
   protected String AV6lccbTab ;
   protected String AV9DataCar ;
   protected String Gx_msg ;
   protected java.util.Date AV7LccbDat ;
   protected java.util.Date AV8LccbDat ;
   protected java.util.Date GXv_date1[] ;
   protected boolean returnInSub ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectShort edtavLccbano ;
   protected GUIObjectString cmbavLccbtabela ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected ILabel lbllbl5 ;
   protected ILabel lbllbl6 ;
   protected IGXRectangle rctrct4 ;
}


/*
               File: ICSI_CCConf
        Description: Configura��o das Empresas de cart�o no ICSI
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:5.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class ticsi_ccconf extends GXTransaction
{
   public ticsi_ccconf( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( ticsi_ccconf.class ), "" );
   }

   public ticsi_ccconf( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey2I238( )
   {
      A1421ICSI_ = "" ;
      n1421ICSI_ = false ;
      A1235ICSI_ = (byte)(0) ;
      n1235ICSI_ = false ;
   }

   public void initAll2I238( )
   {
      A1237ICSI_ = "" ;
      K1237ICSI_ = A1237ICSI_ ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey2I238( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void initializeNonKey2I239( )
   {
      A1236ICSI_ = "" ;
      n1236ICSI_ = false ;
   }

   public void initAll2I239( )
   {
      A1238ICSI_ = 0 ;
      K1238ICSI_ = A1238ICSI_ ;
      geteqAfterKey239 = (byte)(1) ;
      initializeNonKey2I239( ) ;
   }

   public void standaloneModalInsert2I239( )
   {
   }

   public void resetCaption2I0( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "ICSI_CCConf" ;
   }

   protected String getFrmTitle( )
   {
      return "Configura��o das Empresas de cart�o no ICSI" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return new GXMenuBarDefaultTRN(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TICSI_CCConf.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String aP0 ,
                        String aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String aP1 )
   {
      ticsi_ccconf.this.AV11ICSI_C = aP0;
      ticsi_ccconf.this.Gx_mode = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtICSI_CCConfCod = new GUIObjectString ( new GXEdit(2, "XX", UIFactory.getFont( "Courier New", 0, 9),259, 89, 24, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 259 , 89 , 24 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1237ICSI_" );
      ((GXEdit) edtICSI_CCConfCod.getGXComponent()).setAlignment(ILabel.LEFT);
      edtICSI_CCConfCod.addFocusListener(this);
      edtICSI_CCConfCod.getGXComponent().setHelpId("HLP_TICSI_CCConf.htm");
      edtICSI_CCConfName = new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),259, 113, 290, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 259 , 113 , 290 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1421ICSI_" );
      ((GXEdit) edtICSI_CCConfName.getGXComponent()).setAlignment(ILabel.LEFT);
      edtICSI_CCConfName.addFocusListener(this);
      edtICSI_CCConfName.getGXComponent().setHelpId("HLP_TICSI_CCConf.htm");
      edtICSI_CCConfEnab = new GUIObjectByte ( new GXEdit(1, "9", UIFactory.getFont( "Courier New", 0, 9),259, 137, 17, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 259 , 137 , 17 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1235ICSI_" );
      ((GXEdit) edtICSI_CCConfEnab.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtICSI_CCConfEnab.addFocusListener(this);
      edtICSI_CCConfEnab.getGXComponent().setHelpId("HLP_TICSI_CCConf.htm");
      addSubfile ( subGrd_1  = new GXSubfileTRN ( new ICSI_CCConf_flow17(this) , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectInt ( new GXEdit(6, "ZZZZZ9", UIFactory.getFont( "Courier New", 0, 9),0, 0, 232, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.NUMERIC, false, false, 0, false) , null ,  0 , 0 , 231 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1238ICSI_" ), "C�digo do erro de submiss�o ao Cart�o"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 231 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 288, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.CHAR, false, false, 0, false) , null ,  0 , 0 , 287 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1236ICSI_" ), "Descri��o do erro de submiss�o ao Cart�o"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 287 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      }, 9 , 18 , GXPanel1 , 21 , 161 , 575 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(false);
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "Company Credit Card", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 119 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "Processor Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 93 , 13 );
      lbllbl16 = UIFactory.getLabel(GXPanel1, "Indicador de habilitado (1) ou n�o (0)", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 137 , 212 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtICSI_CCConfCod ,
                edtICSI_CCConfName ,
                edtICSI_CCConfEnab ,
                subGrd_1 ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtICSI_CCConfCod, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey2I238( ) ;
      subGrd_1.startLoad();
      subticsi_ccconf17 = new subticsi_ccconf17 ();
      subGrd_1.endLoad();
   }

   public void disable_std_buttons( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_first.setGXEnabled( 0 );
         bttBtn_prev.setGXEnabled( 0 );
         bttBtn_next.setGXEnabled( 0 );
         bttBtn_last.setGXEnabled( 0 );
         bttBtn_exit2.setGXEnabled( 0 );
      }
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtICSI_CCConfCod.setEnabled( 0 );
         edtICSI_CCConfName.setEnabled( 0 );
         edtICSI_CCConfEnab.setEnabled( 0 );
         subGrd_1.getColumn(0).setEnabled( 0 );
         subGrd_1.getColumn(1).setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll2I238( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   public void loadToBuffer17( )
   {
      subticsi_ccconf17 oAux = subticsi_ccconf17 ;
      subticsi_ccconf17 = new subticsi_ccconf17 ();
      variablesToSubfile17 ();
      subGrd_1.addElement(subticsi_ccconf17);
      subticsi_ccconf17 = oAux;
   }

   public boolean isLoadAtStartup_flow17( )
   {
      return false;
   }

   public void autoRefresh_flow17( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
   }

   public boolean getSearch_flow17( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow17( )
   {
   }

   public void resetSearchConditions_flow17( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow17( )
   {
      if ( subGrd_1.getItemCount() > 0 )
      {
         subticsi_ccconf17 = ( subticsi_ccconf17 ) subGrd_1.getElementAt(subGrd_1.getItemCount() -1);
         subfileToVariables17 ();
         /* Save values for previous() function. */
      }
      subticsi_ccconf17 = new subticsi_ccconf17 ();
      initAll2I239( ) ;
      sMode239 = Gx_mode ;
      Gx_mode = "INS" ;
      standaloneModal2I239( ) ;
      Gx_mode = sMode239 ;
      variablesToSubfile17 ();
      return subticsi_ccconf17 ;
   }

   public boolean getSearch_flow17( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow17( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow17( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      String Gx_mode = element.getMode();
      subticsi_ccconf17 subticsi_ccconf17  = ( subticsi_ccconf17 ) element;
      if ( col == 0 )
      {
         return ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  );
      }
      return !enabled;
   }

   public void refresh_flow17( )
   {
   }

   public final  class ICSI_CCConf_flow17 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      ticsi_ccconf _sf ;

      public ICSI_CCConf_flow17( ticsi_ccconf uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow17();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow17(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow17();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow17();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow17(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow17();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow17(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow17(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow17(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow17();
      }

   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_first.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_first( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_prev.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_previous( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_next.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_next( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_last.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_last( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_exit2.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_select( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         GXEnter( );
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtICSI_CCConfCod.isEventSource(eventSource) ) {
         setGXCursor( edtICSI_CCConfCod.getGXCursor() );
         return;
      }
      if ( edtICSI_CCConfName.isEventSource(eventSource) ) {
         setGXCursor( edtICSI_CCConfName.getGXCursor() );
         return;
      }
      if ( edtICSI_CCConfEnab.isEventSource(eventSource) ) {
         setGXCursor( edtICSI_CCConfEnab.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtICSI_CCConfCod.isEventSource(eventSource) ) {
         valid_Icsi_ccconfcod ();
         return;
      }
      if ( subGrd_1.getColumn(0).isEventSource(eventSource) ) {
         subticsi_ccconf17 = (subticsi_ccconf17)subGrd_1.cloneCurrentElement();
         if ( ( subticsi_ccconf17.isDeleted() != 1 ) )
         {
            subfileToVariables17 ();
            sMode239 = Gx_mode ;
            Gx_mode = subticsi_ccconf17.getTrnMode() ;
            valid_Icsi_ccerror ();
            Gx_mode = sMode239 ;
         }
         return;
      }
      if ( subGrd_1.getColumn(1).isEventSource(eventSource) ) {
         subticsi_ccconf17 = (subticsi_ccconf17)subGrd_1.cloneCurrentElement();
         if ( ( subticsi_ccconf17.isDeleted() != 1 ) )
         {
            subfileToVariables17 ();
            sMode239 = Gx_mode ;
            Gx_mode = subticsi_ccconf17.getTrnMode() ;
            valid_Icsi_ccerrordsc ();
            Gx_mode = sMode239 ;
         }
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtICSI_CCConfCod.isEventSource(eventSource) ) {
         A1237ICSI_ = edtICSI_CCConfCod.getValue() ;
         return;
      }
      if ( edtICSI_CCConfName.isEventSource(eventSource) ) {
         A1421ICSI_ = edtICSI_CCConfName.getValue() ;
         if ( ( GXutil.strcmp(A1421ICSI_, "") != 0 ) )
         {
            n1421ICSI_ = false ;
         }
         return;
      }
      if ( edtICSI_CCConfEnab.isEventSource(eventSource) ) {
         A1235ICSI_ = edtICSI_CCConfEnab.getValue() ;
         if ( ( A1235ICSI_ != 0 ) )
         {
            n1235ICSI_ = false ;
         }
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtICSI_CCConfCod.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1237ICSI_, edtICSI_CCConfCod.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtICSI_CCConfName.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1421ICSI_, edtICSI_CCConfName.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtICSI_CCConfEnab.isEventSource(eventSource) ) && ( ( A1235ICSI_ != edtICSI_CCConfEnab.getValue() ) ) )
      {
         return true;
      }
      if (subGrd_1.elementsEventSource(eventSource))
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption2I0( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
      if ( subGrd_1 .isEventSource(eventSource)) {
         if ( row < 0 ) {
            subticsi_ccconf17 = ( subticsi_ccconf17 ) subGrd_1.getCurrentElement() ;
         }
         else
         {
            subticsi_ccconf17 = ( subticsi_ccconf17 ) subGrd_1.getElementAt(row) ;
         }
         subfileToVariables17 ();
         if ( ( subticsi_ccconf17.isDeleted() == 1 ) )
         {
            sMode239 = Gx_mode ;
            Gx_mode = "DLT" ;
         }
         else
         {
            sMode239 = Gx_mode ;
            Gx_mode = "INS" ;
         }
         validate_on_delete239 ();
         Gx_mode = sMode239 ;
      }
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtICSI_CCConfName.isEventSource(eventSource) )
      {
         edtICSI_CCConfName.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtICSI_CCConfEnab.isEventSource(eventSource) )
      {
         edtICSI_CCConfEnab.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void variablesToSubfile17( )
   {
      subticsi_ccconf17.setICSI_CCError(A1238ICSI_);
      subticsi_ccconf17.setICSI_CCErrorDsc(A1236ICSI_);
      subticsi_ccconf17.setZICSI_CCErrorDsc(Z1236ICSI_);
   }

   protected void subfileToVariables17( )
   {
      A1238ICSI_ = subticsi_ccconf17.getICSI_CCError();
      A1236ICSI_ = subticsi_ccconf17.getICSI_CCErrorDsc();
      if ( ( GXutil.strcmp(A1236ICSI_, "") != 0 ) )
      {
         n1236ICSI_ = false ;
      }
      Z1236ICSI_ = subticsi_ccconf17.getZICSI_CCErrorDsc();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      edtICSI_CCConfCod.setValue( A1237ICSI_ );
      edtICSI_CCConfName.setValue( A1421ICSI_ );
      edtICSI_CCConfEnab.setValue( A1235ICSI_ );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      A1237ICSI_ = edtICSI_CCConfCod.getValue() ;
      A1421ICSI_ = edtICSI_CCConfName.getValue() ;
      n1421ICSI_ = false ;
      if ( ( GXutil.strcmp(A1421ICSI_, "") != 0 ) )
      {
         n1421ICSI_ = false ;
      }
      A1235ICSI_ = edtICSI_CCConfEnab.getValue() ;
      n1235ICSI_ = false ;
      if ( ( A1235ICSI_ != 0 ) )
      {
         n1235ICSI_ = false ;
      }
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subticsi_ccconf17 = ( subticsi_ccconf17 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subticsi_ccconf17 = new subticsi_ccconf17 ();
      }
      subfileToVariables17 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile17 ();
      subGrd_1.refreshLineValue(subticsi_ccconf17);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subticsi_ccconf17 = ( subticsi_ccconf17 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subticsi_ccconf17 = new subticsi_ccconf17 ();
      }
      subfileToVariables17 ();
   }

   public void valid_Icsi_ccconfcod( )
   {
      if ( ( GXutil.strcmp(A1237ICSI_, K1237ICSI_) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K1237ICSI_ = A1237ICSI_ ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void valid_Icsi_ccerror( )
   {
      if ( ( A1238ICSI_ != K1238ICSI_ ) || ( geteqAfterKey239 == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey239 = (byte)(0) ;
         }
         else
         {
            geteqAfterKey239 = (byte)(1) ;
         }
      }
   }

   public void valid_Icsi_ccerrordsc( )
   {
      reloadGridRow();
   }

   public void validate_on_delete239( )
   {
      /* No delete mode formulas found. */
   }

   public void e112I2( )
   {
      eventLevelContext();
      /* 'Back' Routine */
      if (canCleanup()) {
         returnInSub = true;
      }
      if (true) return;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm2I238( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1421ICSI_ = T002I5_A1421ICSI_[0] ;
            Z1235ICSI_ = T002I5_A1235ICSI_[0] ;
         }
         else
         {
            Z1421ICSI_ = A1421ICSI_ ;
            Z1235ICSI_ = A1235ICSI_ ;
         }
      }
      if ( ( GX_JID == -1 ) )
      {
         Z1237ICSI_ = A1237ICSI_ ;
         Z1421ICSI_ = A1421ICSI_ ;
         Z1235ICSI_ = A1235ICSI_ ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         subGrd_1.setEnabled( 0 );
      }
   }

   public void load2I238( )
   {
      /* Using cursor T002I6 */
      pr_default.execute(4, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound238 = (short)(1) ;
         A1421ICSI_ = T002I6_A1421ICSI_[0] ;
         n1421ICSI_ = T002I6_n1421ICSI_[0] ;
         A1235ICSI_ = T002I6_A1235ICSI_[0] ;
         n1235ICSI_ = T002I6_n1235ICSI_[0] ;
         zm2I238( -1) ;
      }
      pr_default.close(4);
      onLoadActions2I238( ) ;
   }

   public void onLoadActions2I238( )
   {
   }

   public void checkExtendedTable2I238( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2I238( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey2I238( )
   {
      /* Using cursor T002I7 */
      pr_default.execute(5, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound238 = (short)(1) ;
      }
      else
      {
         RcdFound238 = (short)(0) ;
      }
      pr_default.close(5);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T002I5 */
      pr_default.execute(3, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(3) != 101) )
      {
         zm2I238( 1) ;
         RcdFound238 = (short)(1) ;
         A1237ICSI_ = T002I5_A1237ICSI_[0] ;
         A1421ICSI_ = T002I5_A1421ICSI_[0] ;
         n1421ICSI_ = T002I5_n1421ICSI_[0] ;
         A1235ICSI_ = T002I5_A1235ICSI_[0] ;
         n1235ICSI_ = T002I5_n1235ICSI_[0] ;
         Z1237ICSI_ = A1237ICSI_ ;
         sMode238 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2I238( ) ;
         Gx_mode = sMode238 ;
      }
      else
      {
         RcdFound238 = (short)(0) ;
         initializeNonKey2I238( ) ;
         sMode238 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode238 ;
      }
      K1237ICSI_ = A1237ICSI_ ;
      pr_default.close(3);
      subGrd_1.startLoad();
      subticsi_ccconf17 = new subticsi_ccconf17 ();
      if ( ( RcdFound238 == 1 ) )
      {
         scanStart2I239( ) ;
         while ( ( RcdFound239 != 0 ) )
         {
            getByPrimaryKey2I239( ) ;
            addRow2I239( ) ;
            scanNext2I239( ) ;
         }
         scanEnd2I239( ) ;
      }
      subGrd_1.endLoad(new subticsi_ccconf17());
   }

   public void getEqualNoModal( )
   {
      getKey2I238( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound238 = (short)(0) ;
      /* Using cursor T002I8 */
      pr_default.execute(6, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(6) != 101) )
      {
         while ( (pr_default.getStatus(6) != 101) && ( ( GXutil.strcmp(T002I8_A1237ICSI_[0], A1237ICSI_) < 0 ) ) )
         {
            pr_default.readNext(6);
         }
         if ( (pr_default.getStatus(6) != 101) && ( ( GXutil.strcmp(T002I8_A1237ICSI_[0], A1237ICSI_) > 0 ) ) )
         {
            A1237ICSI_ = T002I8_A1237ICSI_[0] ;
            RcdFound238 = (short)(1) ;
         }
      }
      pr_default.close(6);
   }

   public void move_previous( )
   {
      RcdFound238 = (short)(0) ;
      /* Using cursor T002I9 */
      pr_default.execute(7, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(7) != 101) )
      {
         while ( (pr_default.getStatus(7) != 101) && ( ( GXutil.strcmp(T002I9_A1237ICSI_[0], A1237ICSI_) > 0 ) ) )
         {
            pr_default.readNext(7);
         }
         if ( (pr_default.getStatus(7) != 101) && ( ( GXutil.strcmp(T002I9_A1237ICSI_[0], A1237ICSI_) < 0 ) ) )
         {
            A1237ICSI_ = T002I9_A1237ICSI_[0] ;
            RcdFound238 = (short)(1) ;
         }
      }
      pr_default.close(7);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey2I238( ) ;
      if ( ( RcdFound238 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtICSI_CCConfCod );
         }
         else if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
         {
            A1237ICSI_ = Z1237ICSI_ ;
            edtICSI_CCConfCod.setValue(A1237ICSI_);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtICSI_CCConfCod );
         }
         else
         {
            /* Update record */
            update2I238( ) ;
            setNextFocus( edtICSI_CCConfCod );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
         {
            /* Insert record */
            insert2I238( ) ;
            setNextFocus( edtICSI_CCConfCod );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtICSI_CCConfCod );
            }
            else
            {
               /* Insert record */
               insert2I238( ) ;
               setNextFocus( edtICSI_CCConfCod );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
      {
         A1237ICSI_ = Z1237ICSI_ ;
         edtICSI_CCConfCod.setValue(A1237ICSI_);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtICSI_CCConfCod );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtICSI_CCConfCod );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      getByPrimaryKey( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         pushError( localUtil.getMessages().getMessage("keynfound") );
         AnyError = (short)(1) ;
         keepFocus();
      }
      setNextFocus( edtICSI_CCConfName );
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart2I238( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
      }
      setNextFocus( edtICSI_CCConfName );
      scanEnd2I238( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
      }
      setNextFocus( edtICSI_CCConfName );
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
      }
      setNextFocus( edtICSI_CCConfName );
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart2I238( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
         while ( ( RcdFound238 != 0 ) )
         {
            scanNext2I238( ) ;
         }
      }
      setNextFocus( edtICSI_CCConfName );
      scanEnd2I238( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_select( )
   {
      GXv_char1[0] = A1237ICSI_ ;
      new wgx06m0(remoteHandle, context).execute( GXv_char1) ;
      ticsi_ccconf.this.A1237ICSI_ = GXv_char1[0] ;
      edtICSI_CCConfCod.setValue(A1237ICSI_);
      edtICSI_CCConfCod.setValue( A1237ICSI_ );
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency2I238( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002I4 */
         pr_default.execute(2, new Object[] {A1237ICSI_});
         if ( ! (pr_default.getStatus(2) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"ICSI_CCCONF"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(2) == 101) || ( GXutil.strcmp(Z1421ICSI_, T002I4_A1421ICSI_[0]) != 0 ) || ( Z1235ICSI_ != T002I4_A1235ICSI_[0] ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"ICSI_CCCONF"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2I238( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2I238( 0) ;
         checkOptimisticConcurrency2I238( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2I238( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002I10 */
                  pr_default.execute(8, new Object[] {A1237ICSI_, new Boolean(n1421ICSI_), A1421ICSI_, new Boolean(n1235ICSI_), new Byte(A1235ICSI_)});
                  if ( (pr_default.getStatus(8) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2I238( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                           resetCaption2I0( ) ;
                           subGrd_1.startLoad();
                           subticsi_ccconf17 = new subticsi_ccconf17 ();
                           subGrd_1.endLoad();
                        }
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2I238( ) ;
         }
         endLevel2I238( ) ;
      }
      closeExtendedTableCursors2I238( ) ;
   }

   public void update2I238( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I238( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2I238( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002I11 */
                  pr_default.execute(9, new Object[] {new Boolean(n1421ICSI_), A1421ICSI_, new Boolean(n1235ICSI_), new Byte(A1235ICSI_), A1237ICSI_});
                  deferredUpdate2I238( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2I238( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           getByPrimaryKey( ) ;
                           context.msgStatus( localUtil.getMessages().getMessage("sucupdated") );
                           resetCaption2I0( ) ;
                        }
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2I238( ) ;
      }
      closeExtendedTableCursors2I238( ) ;
   }

   public void deferredUpdate2I238( )
   {
   }

   public void delete( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2I238( ) ;
         scanStart2I239( ) ;
         while ( ( RcdFound239 != 0 ) )
         {
            getByPrimaryKey2I239( ) ;
            delete2I239( ) ;
            scanNext2I239( ) ;
         }
         scanEnd2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete2I238( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002I12 */
                  pr_default.execute(10, new Object[] {A1237ICSI_});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        move_next( ) ;
                        if ( ( RcdFound238 == 0 ) )
                        {
                           initAll2I238( ) ;
                           subGrd_1.startLoad();
                           subticsi_ccconf17 = new subticsi_ccconf17 ();
                           subGrd_1.endLoad();
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                        }
                        context.msgStatus( localUtil.getMessages().getMessage("sucdeleted") );
                        resetCaption2I0( ) ;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
      }
      sMode238 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2I238( ) ;
      Gx_mode = sMode238 ;
   }

   public void onDeleteControls2I238( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor T002I13 */
         pr_default.execute(11, new Object[] {A1237ICSI_});
         if ( (pr_default.getStatus(11) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"Cadastro de maquinetas"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(11);
      }
   }

   public void processNestedLevel2I239( )
   {
      nGXsfl_17_idx = (short)(0) ;
      while ( ( nGXsfl_17_idx < subGrd_1.getItemCount() ) )
      {
         readRow2I239( ) ;
         if ( ( subticsi_ccconf17.isLoaded() != 0 ) || ( subticsi_ccconf17.isChanged() != 0 ) )
         {
            standaloneNotModal2I239( ) ;
            getKey2I239( ) ;
            if ( ( subticsi_ccconf17.isLoaded() == 0 ) && ( subticsi_ccconf17.isDeleted() == 0 ) )
            {
               if ( ( RcdFound239 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  insert2I239( ) ;
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("noupdate") );
                  AnyError = (short)(1) ;
                  setNextFocus( edtICSI_CCConfCod );
               }
            }
            else
            {
               if ( ( RcdFound239 != 0 ) )
               {
                  if ( ( subticsi_ccconf17.isDeleted() != 0 ) && ( subticsi_ccconf17.isLoaded() != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     delete2I239( ) ;
                  }
                  else
                  {
                     if ( ( subticsi_ccconf17.isChanged() != 0 ) && ( subticsi_ccconf17.isLoaded() != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        update2I239( ) ;
                     }
                  }
               }
               else
               {
                  if ( ( subticsi_ccconf17.isDeleted() == 0 ) )
                  {
                     pushError( localUtil.getMessages().getMessage("recdeleted") );
                     AnyError = (short)(1) ;
                     setNextFocus( edtICSI_CCConfCod );
                  }
               }
            }
         }
      }
      subGrd_1.endLoad(new subticsi_ccconf17());
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void processLevel2I238( )
   {
      /* Save parent mode. */
      sMode238 = Gx_mode ;
      processNestedLevel2I239( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode238 ;
      /* ' Update level parameters */
   }

   public void endLevel2I238( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(2);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2I238( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         pr_default.close(1);
         pr_default.close(0);
         Application.commit(context, remoteHandle, "DEFAULT", "ticsi_ccconf");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         pr_default.close(1);
         pr_default.close(0);
         Application.rollback(context, remoteHandle, "DEFAULT", "ticsi_ccconf");
         nGXsfl_17_idx = (short)(0) ;
         while ( ( nGXsfl_17_idx < subGrd_1.getItemCount() ) )
         {
            readRow2I239( ) ;
            /* Using cursor T002I3 */
            pr_default.execute(1, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
            if ( (pr_default.getStatus(1) != 101) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
               {
                  Z1236ICSI_ = T002I3_A1236ICSI_[0] ;
               }
               else
               {
                  Z1236ICSI_ = A1236ICSI_ ;
               }
               variablesToSubfile17 ();
            }
            pr_default.close(1);
         }
      }
      IsModified = (short)(0) ;
   }

   public void scanStart2I238( )
   {
      /* Using cursor T002I14 */
      pr_default.execute(12);
      RcdFound238 = (short)(0) ;
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound238 = (short)(1) ;
         A1237ICSI_ = T002I14_A1237ICSI_[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2I238( )
   {
      pr_default.readNext(12);
      RcdFound238 = (short)(0) ;
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound238 = (short)(1) ;
         A1237ICSI_ = T002I14_A1237ICSI_[0] ;
      }
   }

   public void scanEnd2I238( )
   {
      pr_default.close(12);
   }

   public void afterConfirm2I238( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2I238( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2I238( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2I238( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2I238( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2I238( )
   {
      /* Before Validate Rules */
   }

   public void zm2I239( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1236ICSI_ = T002I3_A1236ICSI_[0] ;
         }
         else
         {
            Z1236ICSI_ = A1236ICSI_ ;
         }
      }
      if ( ( GX_JID == -2 ) )
      {
         Z1237ICSI_ = A1237ICSI_ ;
         Z1238ICSI_ = A1238ICSI_ ;
         Z1236ICSI_ = A1236ICSI_ ;
      }
   }

   public void standaloneNotModal2I239( )
   {
   }

   public void standaloneModal2I239( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         subGrd_1.setEnabled( 0 );
      }
   }

   public void load2I239( )
   {
      /* Using cursor T002I15 */
      pr_default.execute(13, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound239 = (short)(1) ;
         A1236ICSI_ = T002I15_A1236ICSI_[0] ;
         n1236ICSI_ = T002I15_n1236ICSI_[0] ;
         zm2I239( -2) ;
      }
      pr_default.close(13);
      onLoadActions2I239( ) ;
   }

   public void onLoadActions2I239( )
   {
   }

   public void checkExtendedTable2I239( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal2I239( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2I239( )
   {
   }

   public void enableDisable2I239( )
   {
   }

   public void getKey2I239( )
   {
      /* Using cursor T002I16 */
      pr_default.execute(14, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound239 = (short)(1) ;
      }
      else
      {
         RcdFound239 = (short)(0) ;
      }
      pr_default.close(14);
   }

   public void getByPrimaryKey2I239( )
   {
      /* Using cursor T002I3 */
      pr_default.execute(1, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm2I239( 2) ;
         RcdFound239 = (short)(1) ;
         initializeNonKey2I239( ) ;
         A1238ICSI_ = T002I3_A1238ICSI_[0] ;
         A1236ICSI_ = T002I3_A1236ICSI_[0] ;
         n1236ICSI_ = T002I3_n1236ICSI_[0] ;
         Z1237ICSI_ = A1237ICSI_ ;
         Z1238ICSI_ = A1238ICSI_ ;
         sMode239 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2I239( ) ;
         Gx_mode = sMode239 ;
      }
      else
      {
         RcdFound239 = (short)(0) ;
         initializeNonKey2I239( ) ;
         sMode239 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2I239( ) ;
         Gx_mode = sMode239 ;
      }
      K1238ICSI_ = A1238ICSI_ ;
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency2I239( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002I2 */
         pr_default.execute(0, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"ICSI_CCCONF1"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1236ICSI_, T002I2_A1236ICSI_[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"ICSI_CCCONF1"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2I239( )
   {
      beforeValidate2I239( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I239( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2I239( 0) ;
         checkOptimisticConcurrency2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I239( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2I239( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002I17 */
                  pr_default.execute(15, new Object[] {A1237ICSI_, new Integer(A1238ICSI_), new Boolean(n1236ICSI_), A1236ICSI_});
                  if ( (pr_default.getStatus(15) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2I239( ) ;
         }
         endLevel2I239( ) ;
      }
      closeExtendedTableCursors2I239( ) ;
   }

   public void update2I239( )
   {
      beforeValidate2I239( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I239( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I239( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2I239( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002I18 */
                  pr_default.execute(16, new Object[] {new Boolean(n1236ICSI_), A1236ICSI_, A1237ICSI_, new Integer(A1238ICSI_)});
                  deferredUpdate2I239( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey2I239( ) ;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2I239( ) ;
      }
      closeExtendedTableCursors2I239( ) ;
   }

   public void deferredUpdate2I239( )
   {
   }

   public void delete2I239( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2I239( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I239( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2I239( ) ;
         /* No cascading delete specified. */
         afterConfirm2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2I239( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T002I19 */
               pr_default.execute(17, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode239 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2I239( ) ;
      Gx_mode = sMode239 ;
   }

   public void onDeleteControls2I239( )
   {
      standaloneModal2I239( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel2I239( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
   }

   public void scanStart2I239( )
   {
      /* Using cursor T002I20 */
      pr_default.execute(18, new Object[] {A1237ICSI_});
      RcdFound239 = (short)(0) ;
      if ( (pr_default.getStatus(18) != 101) )
      {
         RcdFound239 = (short)(1) ;
         A1238ICSI_ = T002I20_A1238ICSI_[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2I239( )
   {
      pr_default.readNext(18);
      RcdFound239 = (short)(0) ;
      if ( (pr_default.getStatus(18) != 101) )
      {
         RcdFound239 = (short)(1) ;
         A1238ICSI_ = T002I20_A1238ICSI_[0] ;
      }
   }

   public void scanEnd2I239( )
   {
      pr_default.close(18);
   }

   public void afterConfirm2I239( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2I239( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2I239( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2I239( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2I239( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2I239( )
   {
      /* Before Validate Rules */
   }

   public void addRow2I239( )
   {
      loadToBuffer17();
   }

   public void readRow2I239( )
   {
      subticsi_ccconf17 = ( subticsi_ccconf17 ) subGrd_1.getElementAt(nGXsfl_17_idx) ;
      subfileToVariables17 ();
      nGXsfl_17_idx = (short)(nGXsfl_17_idx+1) ;
   }

   public void confirm_2I0( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2I238( ) ;
         }
         else
         {
            checkExtendedTable2I238( ) ;
            closeExtendedTableCursors2I238( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         /* Save parent mode. */
         sMode238 = Gx_mode ;
         confirm_2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            /* Restore parent mode. */
            Gx_mode = sMode238 ;
            PreviousBitmap = bttBtn_exit1.getBitmap() ;
            PreviousTooltip = bttBtn_exit1.getTooltip() ;
            IsConfirmed = (short)(1) ;
         }
         /* Restore parent mode. */
         Gx_mode = sMode238 ;
      }
   }

   public void confirm_2I239( )
   {
      nGXsfl_17_idx = (short)(0) ;
      while ( ( nGXsfl_17_idx < subGrd_1.getItemCount() ) )
      {
         readRow2I239( ) ;
         if ( ( subticsi_ccconf17.isLoaded() != 0 ) || ( subticsi_ccconf17.isChanged() != 0 ) )
         {
            getKey2I239( ) ;
            if ( ( subticsi_ccconf17.isLoaded() == 0 ) && ( subticsi_ccconf17.isDeleted() == 0 ) )
            {
               if ( ( RcdFound239 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate2I239( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable2I239( ) ;
                     closeExtendedTableCursors2I239( ) ;
                     if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
                     {
                        PreviousBitmap = bttBtn_exit1.getBitmap() ;
                        PreviousTooltip = bttBtn_exit1.getTooltip() ;
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("noupdate") );
                  AnyError = (short)(1) ;
                  setNextFocus( edtICSI_CCConfCod );
               }
            }
            else
            {
               if ( ( RcdFound239 != 0 ) )
               {
                  if ( ( subticsi_ccconf17.isDeleted() != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey2I239( ) ;
                     load2I239( ) ;
                     beforeValidate2I239( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls2I239( ) ;
                     }
                  }
                  else
                  {
                     if ( ( subticsi_ccconf17.isChanged() != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate2I239( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable2I239( ) ;
                           closeExtendedTableCursors2I239( ) ;
                           if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
                           {
                              PreviousBitmap = bttBtn_exit1.getBitmap() ;
                              PreviousTooltip = bttBtn_exit1.getTooltip() ;
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( subticsi_ccconf17.isDeleted() == 0 ) )
                  {
                     pushError( localUtil.getMessages().getMessage("recdeleted") );
                     AnyError = (short)(1) ;
                     setNextFocus( edtICSI_CCConfCod );
                  }
               }
            }
         }
      }
      subGrd_1.endLoad();
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A1421ICSI_ = "" ;
      n1421ICSI_ = false ;
      A1235ICSI_ = (byte)(0) ;
      n1235ICSI_ = false ;
      A1237ICSI_ = "" ;
      K1237ICSI_ = "" ;
      A1236ICSI_ = "" ;
      n1236ICSI_ = false ;
      A1238ICSI_ = 0 ;
      K1238ICSI_ = 0 ;
      subticsi_ccconf17 = new subticsi_ccconf17();
      sMode239 = "" ;
      lastAnyError = 0 ;
      Z1236ICSI_ = "" ;
      returnInSub = false ;
      Z1421ICSI_ = "" ;
      Z1235ICSI_ = (byte)(0) ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z1237ICSI_ = "" ;
      T002I6_A1237ICSI_ = new String[] {""} ;
      T002I6_A1421ICSI_ = new String[] {""} ;
      T002I6_n1421ICSI_ = new boolean[] {false} ;
      T002I6_A1235ICSI_ = new byte[1] ;
      T002I6_n1235ICSI_ = new boolean[] {false} ;
      RcdFound238 = (short)(0) ;
      Gx_BScreen = (byte)(0) ;
      T002I7_A1237ICSI_ = new String[] {""} ;
      T002I5_A1237ICSI_ = new String[] {""} ;
      T002I5_A1421ICSI_ = new String[] {""} ;
      T002I5_n1421ICSI_ = new boolean[] {false} ;
      T002I5_A1235ICSI_ = new byte[1] ;
      T002I5_n1235ICSI_ = new boolean[] {false} ;
      sMode238 = "" ;
      RcdFound239 = (short)(0) ;
      T002I8_A1237ICSI_ = new String[] {""} ;
      T002I9_A1237ICSI_ = new String[] {""} ;
      GXv_char1 = new String [1] ;
      T002I4_A1237ICSI_ = new String[] {""} ;
      T002I4_A1421ICSI_ = new String[] {""} ;
      T002I4_n1421ICSI_ = new boolean[] {false} ;
      T002I4_A1235ICSI_ = new byte[1] ;
      T002I4_n1235ICSI_ = new boolean[] {false} ;
      T002I13_A1313CadIA = new String[] {""} ;
      T002I13_A1423POSDa = new java.util.Date[] {GXutil.nullDate()} ;
      T002I13_A1237ICSI_ = new String[] {""} ;
      T002I3_A1237ICSI_ = new String[] {""} ;
      T002I3_A1238ICSI_ = new int[1] ;
      T002I3_A1236ICSI_ = new String[] {""} ;
      T002I3_n1236ICSI_ = new boolean[] {false} ;
      T002I14_A1237ICSI_ = new String[] {""} ;
      Z1238ICSI_ = 0 ;
      T002I15_A1237ICSI_ = new String[] {""} ;
      T002I15_A1238ICSI_ = new int[1] ;
      T002I15_A1236ICSI_ = new String[] {""} ;
      T002I15_n1236ICSI_ = new boolean[] {false} ;
      T002I16_A1237ICSI_ = new String[] {""} ;
      T002I16_A1238ICSI_ = new int[1] ;
      T002I2_A1237ICSI_ = new String[] {""} ;
      T002I2_A1238ICSI_ = new int[1] ;
      T002I2_A1236ICSI_ = new String[] {""} ;
      T002I2_n1236ICSI_ = new boolean[] {false} ;
      T002I20_A1237ICSI_ = new String[] {""} ;
      T002I20_A1238ICSI_ = new int[1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new ticsi_ccconf__default(),
         new Object[] {
             new Object[] {
            T002I2_A1237ICSI_, T002I2_A1238ICSI_, T002I2_A1236ICSI_, T002I2_n1236ICSI_
            }
            , new Object[] {
            T002I3_A1237ICSI_, T002I3_A1238ICSI_, T002I3_A1236ICSI_, T002I3_n1236ICSI_
            }
            , new Object[] {
            T002I4_A1237ICSI_, T002I4_A1421ICSI_, T002I4_n1421ICSI_, T002I4_A1235ICSI_, T002I4_n1235ICSI_
            }
            , new Object[] {
            T002I5_A1237ICSI_, T002I5_A1421ICSI_, T002I5_n1421ICSI_, T002I5_A1235ICSI_, T002I5_n1235ICSI_
            }
            , new Object[] {
            T002I6_A1237ICSI_, T002I6_A1421ICSI_, T002I6_n1421ICSI_, T002I6_A1235ICSI_, T002I6_n1235ICSI_
            }
            , new Object[] {
            T002I7_A1237ICSI_
            }
            , new Object[] {
            T002I8_A1237ICSI_
            }
            , new Object[] {
            T002I9_A1237ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002I13_A1313CadIA, T002I13_A1423POSDa, T002I13_A1237ICSI_
            }
            , new Object[] {
            T002I14_A1237ICSI_
            }
            , new Object[] {
            T002I15_A1237ICSI_, T002I15_A1238ICSI_, T002I15_A1236ICSI_, T002I15_n1236ICSI_
            }
            , new Object[] {
            T002I16_A1237ICSI_, T002I16_A1238ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002I20_A1237ICSI_, T002I20_A1238ICSI_
            }
         }
      );
      reloadDynamicLists(0);
   }

   protected byte nKeyPressed ;
   protected byte A1235ICSI_ ;
   protected byte geteqAfterKey= 1 ;
   protected byte geteqAfterKey239= 1 ;
   protected byte Z1235ICSI_ ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound238 ;
   protected short RcdFound239 ;
   protected short nGXsfl_17_idx=1 ;
   protected int trnEnded ;
   protected int A1238ICSI_ ;
   protected int K1238ICSI_ ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected int Z1238ICSI_ ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String A1237ICSI_ ;
   protected String K1237ICSI_ ;
   protected String A1236ICSI_ ;
   protected String AV11ICSI_C ;
   protected String sMode239 ;
   protected String Z1236ICSI_ ;
   protected String scmdbuf ;
   protected String Z1237ICSI_ ;
   protected String sMode238 ;
   protected String GXv_char1[] ;
   protected boolean n1421ICSI_ ;
   protected boolean n1235ICSI_ ;
   protected boolean n1236ICSI_ ;
   protected boolean returnInSub ;
   protected String A1421ICSI_ ;
   protected String Z1421ICSI_ ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtICSI_CCConfCod ;
   protected GUIObjectString edtICSI_CCConfName ;
   protected GUIObjectByte edtICSI_CCConfEnab ;
   protected GXSubfileTRN subGrd_1 ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected ILabel lbllbl16 ;
   protected IGXImage imgimg7 ;
   protected subticsi_ccconf17 subticsi_ccconf17 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T002I6_A1237ICSI_ ;
   protected String[] T002I6_A1421ICSI_ ;
   protected boolean[] T002I6_n1421ICSI_ ;
   protected byte[] T002I6_A1235ICSI_ ;
   protected boolean[] T002I6_n1235ICSI_ ;
   protected String[] T002I7_A1237ICSI_ ;
   protected String[] T002I5_A1237ICSI_ ;
   protected String[] T002I5_A1421ICSI_ ;
   protected boolean[] T002I5_n1421ICSI_ ;
   protected byte[] T002I5_A1235ICSI_ ;
   protected boolean[] T002I5_n1235ICSI_ ;
   protected String[] T002I8_A1237ICSI_ ;
   protected String[] T002I9_A1237ICSI_ ;
   protected String[] T002I4_A1237ICSI_ ;
   protected String[] T002I4_A1421ICSI_ ;
   protected boolean[] T002I4_n1421ICSI_ ;
   protected byte[] T002I4_A1235ICSI_ ;
   protected boolean[] T002I4_n1235ICSI_ ;
   protected String[] T002I13_A1313CadIA ;
   protected java.util.Date[] T002I13_A1423POSDa ;
   protected String[] T002I13_A1237ICSI_ ;
   protected String[] T002I3_A1237ICSI_ ;
   protected int[] T002I3_A1238ICSI_ ;
   protected String[] T002I3_A1236ICSI_ ;
   protected boolean[] T002I3_n1236ICSI_ ;
   protected String[] T002I14_A1237ICSI_ ;
   protected String[] T002I15_A1237ICSI_ ;
   protected int[] T002I15_A1238ICSI_ ;
   protected String[] T002I15_A1236ICSI_ ;
   protected boolean[] T002I15_n1236ICSI_ ;
   protected String[] T002I16_A1237ICSI_ ;
   protected int[] T002I16_A1238ICSI_ ;
   protected String[] T002I2_A1237ICSI_ ;
   protected int[] T002I2_A1238ICSI_ ;
   protected String[] T002I2_A1236ICSI_ ;
   protected boolean[] T002I2_n1236ICSI_ ;
   protected String[] T002I20_A1237ICSI_ ;
   protected int[] T002I20_A1238ICSI_ ;
}

final  class ticsi_ccconf__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T002I2", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (UPDLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I3", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I4", "SELECT [ICSI_CCConfCod], [ICSI_CCConfName], [ICSI_CCConfEnab] FROM [ICSI_CCCONF] WITH (UPDLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I5", "SELECT [ICSI_CCConfCod], [ICSI_CCConfName], [ICSI_CCConfEnab] FROM [ICSI_CCCONF] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I6", "SELECT TM1.[ICSI_CCConfCod], TM1.[ICSI_CCConfName], TM1.[ICSI_CCConfEnab] FROM [ICSI_CCCONF] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ICSI_CCConfCod] = ? ORDER BY TM1.[ICSI_CCConfCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I7", "SELECT [ICSI_CCConfCod] FROM [ICSI_CCCONF] WITH (FASTFIRSTROW NOLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I8", "SELECT TOP 1 [ICSI_CCConfCod] FROM [ICSI_CCCONF] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ICSI_CCConfCod] > ?) ORDER BY [ICSI_CCConfCod] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002I9", "SELECT TOP 1 [ICSI_CCConfCod] FROM [ICSI_CCCONF] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ICSI_CCConfCod] < ?) ORDER BY [ICSI_CCConfCod] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T002I10", "INSERT INTO [ICSI_CCCONF] ([ICSI_CCConfCod], [ICSI_CCConfName], [ICSI_CCConfEnab]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002I11", "UPDATE [ICSI_CCCONF] SET [ICSI_CCConfName]=?, [ICSI_CCConfEnab]=?  WHERE [ICSI_CCConfCod] = ?", GX_NOMASK)
         ,new UpdateCursor("T002I12", "DELETE FROM [ICSI_CCCONF]  WHERE [ICSI_CCConfCod] = ?", GX_NOMASK)
         ,new ForEachCursor("T002I13", "SELECT TOP 1 [CadIATA], [POSData], [ICSI_CCConfCod] FROM [CADASTROSPOS] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002I14", "SELECT [ICSI_CCConfCod] FROM [ICSI_CCCONF] WITH (FASTFIRSTROW NOLOCK) ORDER BY [ICSI_CCConfCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I15", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? and [ICSI_CCError] = ? ORDER BY [ICSI_CCConfCod], [ICSI_CCError] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002I16", "SELECT [ICSI_CCConfCod], [ICSI_CCError] FROM [ICSI_CCCONF1] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("T002I17", "INSERT INTO [ICSI_CCCONF1] ([ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002I18", "UPDATE [ICSI_CCCONF1] SET [ICSI_CCErrorDsc]=?  WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ?", GX_NOMASK)
         ,new UpdateCursor("T002I19", "DELETE FROM [ICSI_CCCONF1]  WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ?", GX_NOMASK)
         ,new ForEachCursor("T002I20", "SELECT [ICSI_CCConfCod], [ICSI_CCError] FROM [ICSI_CCCONF1] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? ORDER BY [ICSI_CCConfCod], [ICSI_CCError] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               break;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               break;
            case 18 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 6 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 7 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 40);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(3, ((Number) parms[4]).byteValue());
               }
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 40);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(2, ((Number) parms[3]).byteValue());
               }
               stmt.setString(3, (String)parms[4], 2);
               break;
            case 10 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 11 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 13 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 14 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 15 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 40);
               }
               break;
            case 16 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 40);
               }
               stmt.setString(2, (String)parms[2], 2);
               stmt.setInt(3, ((Number) parms[3]).intValue());
               break;
            case 17 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 18 :
               stmt.setString(1, (String)parms[0], 2);
               break;
      }
   }

}


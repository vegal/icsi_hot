/*
               File: DistribuicaoTransacoes
        Description: Distribui��o de Transac�es
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:57.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pdistribuicaotransacoes extends GXProcedure
{
   public pdistribuicaotransacoes( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pdistribuicaotransacoes.class ), "" );
   }

   public pdistribuicaotransacoes( int remoteHandle ,
                                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV14Cartao ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_CARTAOCIELO", "C�digo Cart�o Cielo", "S", "VI", GXv_svchar2) ;
      pdistribuicaotransacoes.this.GXt_char1 = GXv_svchar2[0] ;
      AV14Cartao = GXt_char1 ;
      GXt_char1 = AV15Cartao ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ISCI_CARTAOMASTER", "C�digo Cart�o Master", "S", "CA", GXv_svchar2) ;
      pdistribuicaotransacoes.this.GXt_char1 = GXv_svchar2[0] ;
      AV15Cartao = GXt_char1 ;
      GXt_char1 = AV23x ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ISCI_QTDCARTOES", "Quantidade Cart�es Distribui��o", "S", "2", GXv_svchar2) ;
      pdistribuicaotransacoes.this.GXt_char1 = GXv_svchar2[0] ;
      AV23x = GXt_char1 ;
      AV22QtdCar = (byte)(GXutil.val( AV23x, ".")) ;
      AV9Erro = "N" ;
      AV12TotalC = 0 ;
      AV13TotaLM = 0 ;
      AV29GXLvl1 = (byte)(0) ;
      /* Using cursor P006U2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         brk6U2 = false ;
         A1491Distr = P006U2_A1491Distr[0] ;
         A1498Distr = P006U2_A1498Distr[0] ;
         n1498Distr = P006U2_n1498Distr[0] ;
         A1496Distr = P006U2_A1496Distr[0] ;
         A1490Distr = P006U2_A1490Distr[0] ;
         n1490Distr = P006U2_n1490Distr[0] ;
         AV29GXLvl1 = (byte)(1) ;
         AV8TotalPo = 0 ;
         while ( (pr_default.getStatus(0) != 101) && ( GXutil.strcmp(P006U2_A1496Distr[0], A1496Distr) == 0 ) && ( GXutil.strcmp(P006U2_A1498Distr[0], A1498Distr) == 0 ) )
         {
            brk6U2 = false ;
            A1491Distr = P006U2_A1491Distr[0] ;
            A1490Distr = P006U2_A1490Distr[0] ;
            n1490Distr = P006U2_n1490Distr[0] ;
            AV8TotalPo = (double)(AV8TotalPo+A1491Distr) ;
            brk6U2 = true ;
            pr_default.readNext(0);
         }
         if ( ( AV8TotalPo != 100 ) )
         {
            AV9Erro = "S" ;
         }
         if ( ! brk6U2 )
         {
            brk6U2 = true ;
            pr_default.readNext(0);
         }
      }
      pr_default.close(0);
      if ( ( AV29GXLvl1 == 0 ) )
      {
         context.msgStatus( "A tabela de cpnfigura��o esta vazia! Favor verificar" );
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      if ( ( GXutil.strcmp(AV9Erro, "S") == 0 ) )
      {
         context.msgStatus( "Processamento Abortado! A tabela de configura��o de distribui��o de transa��es esta incorreta !" );
      }
      AV24Sequen = (byte)(0) ;
      /* Using cursor P006U3 */
      pr_default.execute(1);
      while ( (pr_default.getStatus(1) != 101) )
      {
         A1490Distr = P006U3_A1490Distr[0] ;
         n1490Distr = P006U3_n1490Distr[0] ;
         A1491Distr = P006U3_A1491Distr[0] ;
         A1496Distr = P006U3_A1496Distr[0] ;
         A1498Distr = P006U3_A1498Distr[0] ;
         n1498Distr = P006U3_n1498Distr[0] ;
         AV24Sequen = (byte)(AV24Sequen+1) ;
         AV18Distri = A1498Distr ;
         AV10lccbCC = A1496Distr ;
         AV11Distri = A1490Distr ;
         AV20Distri = A1491Distr ;
         if ( ( AV20Distri > 0 ) )
         {
            if ( ( AV20Distri != 100 ) )
            {
               /* Execute user subroutine: S1170 */
               S1170 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
            }
            /* Execute user subroutine: S1287 */
            S1287 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               cleanup();
               if (true) return;
            }
         }
         if ( ( AV20Distri == 100 ) || ( AV24Sequen == 2 ) || ( AV20Distri == 0 ) )
         {
            AV24Sequen = (byte)(0) ;
         }
         pr_default.readNext(1);
      }
      pr_default.close(1);
      cleanup();
   }

   public void S1170( )
   {
      /* 'BUSCA_QUANTIDADE_TOTAL' Routine */
      AV19Valor_ = 0 ;
      AV25lccbCr = GXutil.resetTime(GXutil.serverNow( context, remoteHandle, "DEFAULT")) ;
      /* Optimized group. */
      /* Using cursor P006U4 */
      pr_default.execute(2, new Object[] {AV25lccbCr, AV18Distri, AV10lccbCC});
      c1172lccbS = P006U4_A1172lccbS[0] ;
      n1172lccbS = P006U4_n1172lccbS[0] ;
      pr_default.close(2);
      AV19Valor_ = (double)(AV19Valor_+c1172lccbS) ;
      /* End optimized group. */
   }

   public void S1287( )
   {
      /* 'DISTRIBUI_TRANSACOES' Routine */
      AV21SubTot = 0 ;
      AV25lccbCr = GXutil.resetTime(GXutil.serverNow( context, remoteHandle, "DEFAULT")) ;
      AV26Valor_ = (double)(AV19Valor_*(AV20Distri/ (double) (100))) ;
      /* Using cursor P006U5 */
      pr_default.execute(3, new Object[] {AV25lccbCr, AV18Distri, AV10lccbCC, AV25lccbCr, AV18Distri, AV10lccbCC});
      while ( (pr_default.getStatus(3) != 101) )
      {
         A1490Distr = P006U5_A1490Distr[0] ;
         n1490Distr = P006U5_n1490Distr[0] ;
         A1224lccbC = P006U5_A1224lccbC[0] ;
         A1150lccbE = P006U5_A1150lccbE[0] ;
         A1495lccbC = P006U5_A1495lccbC[0] ;
         n1495lccbC = P006U5_n1495lccbC[0] ;
         A1498Distr = P006U5_A1498Distr[0] ;
         n1498Distr = P006U5_n1498Distr[0] ;
         A1172lccbS = P006U5_A1172lccbS[0] ;
         n1172lccbS = P006U5_n1172lccbS[0] ;
         A1222lccbI = P006U5_A1222lccbI[0] ;
         A1223lccbD = P006U5_A1223lccbD[0] ;
         A1225lccbC = P006U5_A1225lccbC[0] ;
         A1226lccbA = P006U5_A1226lccbA[0] ;
         A1227lccbO = P006U5_A1227lccbO[0] ;
         A1228lccbF = P006U5_A1228lccbF[0] ;
         if ( ( GXutil.strcmp(A1490Distr, "") == 0 ) )
         {
            AV21SubTot = (double)(AV21SubTot+A1172lccbS) ;
            if ( ( AV21SubTot > AV26Valor_ ) && ( AV20Distri != 100 ) && ( AV24Sequen == 1 ) )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P006U6 */
               pr_default.execute(4, new Object[] {new Boolean(n1490Distr), A1490Distr, new Boolean(n1498Distr), A1498Distr, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               if (true) break;
            }
            A1498Distr = AV18Distri ;
            n1498Distr = false ;
            A1490Distr = AV11Distri ;
            n1490Distr = false ;
            /* Using cursor P006U7 */
            pr_default.execute(5, new Object[] {new Boolean(n1490Distr), A1490Distr, new Boolean(n1498Distr), A1498Distr, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         }
         pr_default.readNext(3);
      }
      pr_default.close(3);
   }

   protected void cleanup( )
   {
      Application.commit(context, remoteHandle, "DEFAULT", "pdistribuicaotransacoes");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV14Cartao = "" ;
      AV15Cartao = "" ;
      AV23x = "" ;
      GXt_char1 = "" ;
      GXv_svchar2 = new String [1] ;
      AV22QtdCar = (byte)(0) ;
      AV9Erro = "" ;
      AV12TotalC = 0 ;
      AV13TotaLM = 0 ;
      AV29GXLvl1 = (byte)(0) ;
      scmdbuf = "" ;
      P006U2_A1491Distr = new double[1] ;
      P006U2_A1498Distr = new String[] {""} ;
      P006U2_n1498Distr = new boolean[] {false} ;
      P006U2_A1496Distr = new String[] {""} ;
      P006U2_A1490Distr = new String[] {""} ;
      P006U2_n1490Distr = new boolean[] {false} ;
      brk6U2 = false ;
      A1491Distr = 0 ;
      A1498Distr = "" ;
      n1498Distr = false ;
      A1496Distr = "" ;
      A1490Distr = "" ;
      n1490Distr = false ;
      AV8TotalPo = 0 ;
      returnInSub = false ;
      AV24Sequen = (byte)(0) ;
      P006U3_A1490Distr = new String[] {""} ;
      P006U3_n1490Distr = new boolean[] {false} ;
      P006U3_A1491Distr = new double[1] ;
      P006U3_A1496Distr = new String[] {""} ;
      P006U3_A1498Distr = new String[] {""} ;
      P006U3_n1498Distr = new boolean[] {false} ;
      AV18Distri = "" ;
      AV10lccbCC = "" ;
      AV11Distri = "" ;
      AV20Distri = 0 ;
      AV19Valor_ = 0 ;
      AV25lccbCr = GXutil.nullDate() ;
      c1172lccbS = 0 ;
      P006U4_A1172lccbS = new double[1] ;
      P006U4_n1172lccbS = new boolean[] {false} ;
      n1172lccbS = false ;
      AV21SubTot = 0 ;
      AV26Valor_ = 0 ;
      P006U5_A1490Distr = new String[] {""} ;
      P006U5_n1490Distr = new boolean[] {false} ;
      P006U5_A1224lccbC = new String[] {""} ;
      P006U5_A1150lccbE = new String[] {""} ;
      P006U5_A1495lccbC = new java.util.Date[] {GXutil.nullDate()} ;
      P006U5_n1495lccbC = new boolean[] {false} ;
      P006U5_A1498Distr = new String[] {""} ;
      P006U5_n1498Distr = new boolean[] {false} ;
      P006U5_A1172lccbS = new double[1] ;
      P006U5_n1172lccbS = new boolean[] {false} ;
      P006U5_A1222lccbI = new String[] {""} ;
      P006U5_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006U5_A1225lccbC = new String[] {""} ;
      P006U5_A1226lccbA = new String[] {""} ;
      P006U5_A1227lccbO = new String[] {""} ;
      P006U5_A1228lccbF = new String[] {""} ;
      A1224lccbC = "" ;
      A1150lccbE = "" ;
      A1495lccbC = GXutil.nullDate() ;
      n1495lccbC = false ;
      A1172lccbS = 0 ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pdistribuicaotransacoes__default(),
         new Object[] {
             new Object[] {
            P006U2_A1491Distr, P006U2_A1498Distr, P006U2_A1496Distr, P006U2_A1490Distr
            }
            , new Object[] {
            P006U3_A1490Distr, P006U3_A1491Distr, P006U3_A1496Distr, P006U3_A1498Distr
            }
            , new Object[] {
            P006U4_A1172lccbS, P006U4_n1172lccbS
            }
            , new Object[] {
            P006U5_A1490Distr, P006U5_n1490Distr, P006U5_A1224lccbC, P006U5_A1150lccbE, P006U5_A1495lccbC, P006U5_n1495lccbC, P006U5_A1498Distr, P006U5_n1498Distr, P006U5_A1172lccbS, P006U5_n1172lccbS,
            P006U5_A1222lccbI, P006U5_A1223lccbD, P006U5_A1225lccbC, P006U5_A1226lccbA, P006U5_A1227lccbO, P006U5_A1228lccbF
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV22QtdCar ;
   private byte AV29GXLvl1 ;
   private byte AV24Sequen ;
   private short Gx_err ;
   private double AV12TotalC ;
   private double AV13TotaLM ;
   private double A1491Distr ;
   private double AV8TotalPo ;
   private double AV20Distri ;
   private double AV19Valor_ ;
   private double c1172lccbS ;
   private double AV21SubTot ;
   private double AV26Valor_ ;
   private double A1172lccbS ;
   private String AV14Cartao ;
   private String AV15Cartao ;
   private String AV23x ;
   private String GXt_char1 ;
   private String AV9Erro ;
   private String scmdbuf ;
   private String A1498Distr ;
   private String A1496Distr ;
   private String A1490Distr ;
   private String AV18Distri ;
   private String AV10lccbCC ;
   private String AV11Distri ;
   private String A1224lccbC ;
   private String A1150lccbE ;
   private String A1222lccbI ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private java.util.Date AV25lccbCr ;
   private java.util.Date A1495lccbC ;
   private java.util.Date A1223lccbD ;
   private boolean brk6U2 ;
   private boolean n1498Distr ;
   private boolean n1490Distr ;
   private boolean returnInSub ;
   private boolean n1172lccbS ;
   private boolean n1495lccbC ;
   private String GXv_svchar2[] ;
   private IDataStoreProvider pr_default ;
   private double[] P006U2_A1491Distr ;
   private String[] P006U2_A1498Distr ;
   private boolean[] P006U2_n1498Distr ;
   private String[] P006U2_A1496Distr ;
   private String[] P006U2_A1490Distr ;
   private boolean[] P006U2_n1490Distr ;
   private String[] P006U3_A1490Distr ;
   private boolean[] P006U3_n1490Distr ;
   private double[] P006U3_A1491Distr ;
   private String[] P006U3_A1496Distr ;
   private String[] P006U3_A1498Distr ;
   private boolean[] P006U3_n1498Distr ;
   private double[] P006U4_A1172lccbS ;
   private boolean[] P006U4_n1172lccbS ;
   private String[] P006U5_A1490Distr ;
   private boolean[] P006U5_n1490Distr ;
   private String[] P006U5_A1224lccbC ;
   private String[] P006U5_A1150lccbE ;
   private java.util.Date[] P006U5_A1495lccbC ;
   private boolean[] P006U5_n1495lccbC ;
   private String[] P006U5_A1498Distr ;
   private boolean[] P006U5_n1498Distr ;
   private double[] P006U5_A1172lccbS ;
   private boolean[] P006U5_n1172lccbS ;
   private String[] P006U5_A1222lccbI ;
   private java.util.Date[] P006U5_A1223lccbD ;
   private String[] P006U5_A1225lccbC ;
   private String[] P006U5_A1226lccbA ;
   private String[] P006U5_A1227lccbO ;
   private String[] P006U5_A1228lccbF ;
}

final  class pdistribuicaotransacoes__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006U2", "SELECT [DistribuicaoTransacoesPorcenta], [DistribuicaoTransacoesEmpresa], [DistribuicaoTransacoesCartao], [DistribuicaoTransacoesTipo] FROM [DISTRIBUICAOTRANSACOES] WITH (NOLOCK) ORDER BY [DistribuicaoTransacoesCartao], [DistribuicaoTransacoesEmpresa] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006U3", "SELECT [DistribuicaoTransacoesTipo], [DistribuicaoTransacoesPorcenta], [DistribuicaoTransacoesCartao], [DistribuicaoTransacoesEmpresa] FROM [DISTRIBUICAOTRANSACOES] WITH (NOLOCK) ORDER BY [DistribuicaoTransacoesEmpresa], [DistribuicaoTransacoesCartao], [DistribuicaoTransacoesPorcenta] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006U4", "SELECT SUM([lccbSaleAmount]) FROM [LCCBPLP] WITH (NOLOCK) WHERE [lccbCreationDate] = ? and [lccbEmpCod] = ? and [lccbCCard] = ? ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006U5", "SELECT [DistribuicaoTransacoesTipo], [lccbCCard], [lccbEmpCod], [lccbCreationDate], [DistribuicaoTransacoesEmpresa], [lccbSaleAmount], [lccbIATA], [lccbDate], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbCreationDate] = ? AND [lccbEmpCod] = ? AND [lccbCCard] = ?) AND (([lccbCreationDate] = ? and [lccbEmpCod] = ? and [lccbCCard] = ?) AND ([DistribuicaoTransacoesTipo] = '')) ORDER BY [lccbCreationDate], [lccbEmpCod], [lccbCCard], [lccbSaleAmount] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006U6", "UPDATE [LCCBPLP] SET [DistribuicaoTransacoesTipo]=?, [DistribuicaoTransacoesEmpresa]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006U7", "UPDATE [LCCBPLP] SET [DistribuicaoTransacoesTipo]=?, [DistribuicaoTransacoesEmpresa]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((double[]) buf[0])[0] = rslt.getDouble(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 4) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((double[]) buf[1])[0] = rslt.getDouble(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
               break;
            case 2 :
               ((double[]) buf[0])[0] = rslt.getDouble(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 3) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((double[]) buf[8])[0] = rslt.getDouble(6) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(7, 7) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[12])[0] = rslt.getString(9, 44) ;
               ((String[]) buf[13])[0] = rslt.getString(10, 20) ;
               ((String[]) buf[14])[0] = rslt.getString(11, 1) ;
               ((String[]) buf[15])[0] = rslt.getString(12, 19) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 2 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               stmt.setString(2, (String)parms[1], 3);
               stmt.setString(3, (String)parms[2], 2);
               break;
            case 3 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               stmt.setString(2, (String)parms[1], 3);
               stmt.setString(3, (String)parms[2], 2);
               stmt.setDate(4, (java.util.Date)parms[3]);
               stmt.setString(5, (String)parms[4], 3);
               stmt.setString(6, (String)parms[5], 2);
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 4);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 3);
               }
               stmt.setString(3, (String)parms[4], 3);
               stmt.setString(4, (String)parms[5], 7);
               stmt.setDate(5, (java.util.Date)parms[6]);
               stmt.setString(6, (String)parms[7], 2);
               stmt.setString(7, (String)parms[8], 44);
               stmt.setString(8, (String)parms[9], 20);
               stmt.setString(9, (String)parms[10], 1);
               stmt.setString(10, (String)parms[11], 19);
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 4);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 3);
               }
               stmt.setString(3, (String)parms[4], 3);
               stmt.setString(4, (String)parms[5], 7);
               stmt.setDate(5, (java.util.Date)parms[6]);
               stmt.setString(6, (String)parms[7], 2);
               stmt.setString(7, (String)parms[8], 44);
               stmt.setString(8, (String)parms[9], 20);
               stmt.setString(9, (String)parms[10], 1);
               stmt.setString(10, (String)parms[11], 19);
               break;
      }
   }

}


import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx06m012 extends GXSubfileElement
{
   private String ICSI_CCConfCod ;
   private byte ICSI_CCConfEnab ;
   public String getICSI_CCConfCod( )
   {
      return ICSI_CCConfCod ;
   }

   public void setICSI_CCConfCod( String value )
   {
      ICSI_CCConfCod = value;
   }

   public byte getICSI_CCConfEnab( )
   {
      return ICSI_CCConfEnab ;
   }

   public void setICSI_CCConfEnab( byte value )
   {
      ICSI_CCConfEnab = value;
   }

   public void clear( )
   {
      ICSI_CCConfCod = "" ;
      ICSI_CCConfEnab = 0 ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getICSI_CCConfCod().compareTo(((subwgx06m012) element).getICSI_CCConfCod()) ;
            case 1 :
               if ( getICSI_CCConfEnab() > ((subwgx06m012) element).getICSI_CCConfEnab() ) return 1;
               if ( getICSI_CCConfEnab() < ((subwgx06m012) element).getICSI_CCConfEnab() ) return -1;
               return 0;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getICSI_CCConfCod(), "") == 0 ) && ( getICSI_CCConfEnab() == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getICSI_CCConfCod() );
            break;
         case 1 :
            cell.setValue( getICSI_CCConfEnab() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getICSI_CCConfCod()) == 0) );
         case 1 :
            return ( (((GUIObjectByte) cell).getValue() == getICSI_CCConfEnab()) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setICSI_CCConfCod(value.getStringValue());
               break;
            case 1 :
               setICSI_CCConfEnab(value.getByteValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setICSI_CCConfCod(((subwgx06m012) element).getICSI_CCConfCod());
               return;
            case 1 :
               setICSI_CCConfEnab(((subwgx06m012) element).getICSI_CCConfEnab());
               return;
      }
   }

}


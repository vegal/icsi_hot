
public final  class StructSdtCompanyProcessor implements Cloneable, java.io.Serializable
{
   public StructSdtCompanyProcessor( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorlayout = "" ;
      gxTv_SdtCompanyProcessor_Mode = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getCompanyprocessorcode( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorcode ;
   }

   public void setCompanyprocessorcode( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode = value ;
      return  ;
   }

   public String getCompanyprocessorlayout( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorlayout ;
   }

   public void setCompanyprocessorlayout( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorlayout = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtCompanyProcessor_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCompanyProcessor_Mode = value ;
      return  ;
   }

   public String getCompanyprocessorcode_Z( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorcode_Z ;
   }

   public void setCompanyprocessorcode_Z( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = value ;
      return  ;
   }

   public String getCompanyprocessorlayout_Z( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z ;
   }

   public void setCompanyprocessorlayout_Z( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = value ;
      return  ;
   }

   public byte getCompanyprocessorcode_N( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorcode_N ;
   }

   public void setCompanyprocessorcode_N( byte value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = value ;
      return  ;
   }

   protected byte gxTv_SdtCompanyProcessor_Companyprocessorcode_N ;
   protected String gxTv_SdtCompanyProcessor_Mode ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorcode ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorlayout ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorcode_Z ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z ;
}


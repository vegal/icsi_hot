import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtUserProfiles_Level1Item extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtUserProfiles_Level1Item( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtUserProfiles_Level1Item.class));
   }

   public SdtUserProfiles_Level1Item( int remoteHandle ,
                                      ModelContext context )
   {
      super( context, "SdtUserProfiles_Level1Item");
      initialize( remoteHandle) ;
   }

   public SdtUserProfiles_Level1Item( int remoteHandle ,
                                      StructSdtUserProfiles_Level1Item struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtUserProfiles_Level1Item( )
   {
      super( new ModelContext(SdtUserProfiles_Level1Item.class), "SdtUserProfiles_Level1Item");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Level1Item_Uspcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Level1Item_Uspdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Level1Item_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Level1Item_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "UserProfiles.Level1Item" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("uspCode", GXutil.rtrim( gxTv_SdtUserProfiles_Level1Item_Uspcode));
      oWriter.writeElement("uspDescription", GXutil.rtrim( gxTv_SdtUserProfiles_Level1Item_Uspdescription));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtUserProfiles_Level1Item_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtUserProfiles_Level1Item_Modified, 4, 0)));
      oWriter.writeElement("uspCode_Z", GXutil.rtrim( gxTv_SdtUserProfiles_Level1Item_Uspcode_Z));
      oWriter.writeElement("uspDescription_Z", GXutil.rtrim( gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Level1Item_Uspcode( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspcode ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspcode( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspcode_SetNull( )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode = "" ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Level1Item_Uspdescription( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspdescription ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspdescription( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspdescription = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspdescription_SetNull( )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Level1Item_Mode( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Mode ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Mode( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Mode_SetNull( )
   {
      gxTv_SdtUserProfiles_Level1Item_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtUserProfiles_Level1Item_Modified( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Modified ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Modified( short value )
   {
      gxTv_SdtUserProfiles_Level1Item_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Modified_SetNull( )
   {
      gxTv_SdtUserProfiles_Level1Item_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Level1Item_Uspcode_Z( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspcode_Z ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspcode_Z( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspcode_Z_SetNull( )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z_SetNull( )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode = "" ;
      gxTv_SdtUserProfiles_Level1Item_Uspdescription = "" ;
      gxTv_SdtUserProfiles_Level1Item_Mode = "" ;
      gxTv_SdtUserProfiles_Level1Item_Modified = (short)(0) ;
      gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = "" ;
      gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      return  ;
   }

   public SdtUserProfiles_Level1Item Clone( )
   {
      return (SdtUserProfiles_Level1Item)(clone()) ;
   }

   public void setStruct( StructSdtUserProfiles_Level1Item struct )
   {
      setgxTv_SdtUserProfiles_Level1Item_Uspcode(struct.getUspcode());
      setgxTv_SdtUserProfiles_Level1Item_Uspdescription(struct.getUspdescription());
      setgxTv_SdtUserProfiles_Level1Item_Mode(struct.getMode());
      setgxTv_SdtUserProfiles_Level1Item_Modified(struct.getModified());
      setgxTv_SdtUserProfiles_Level1Item_Uspcode_Z(struct.getUspcode_Z());
      setgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z(struct.getUspdescription_Z());
   }

   public StructSdtUserProfiles_Level1Item getStruct( )
   {
      StructSdtUserProfiles_Level1Item struct = new StructSdtUserProfiles_Level1Item ();
      struct.setUspcode(getgxTv_SdtUserProfiles_Level1Item_Uspcode());
      struct.setUspdescription(getgxTv_SdtUserProfiles_Level1Item_Uspdescription());
      struct.setMode(getgxTv_SdtUserProfiles_Level1Item_Mode());
      struct.setModified(getgxTv_SdtUserProfiles_Level1Item_Modified());
      struct.setUspcode_Z(getgxTv_SdtUserProfiles_Level1Item_Uspcode_Z());
      struct.setUspdescription_Z(getgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z());
      return struct ;
   }

   protected short gxTv_SdtUserProfiles_Level1Item_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtUserProfiles_Level1Item_Mode ;
   protected String sTagName ;
   protected String GXt_char2 ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspcode ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspdescription ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspcode_Z ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtAirlines_Contacts extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtAirlines_Contacts( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtAirlines_Contacts.class));
   }

   public SdtAirlines_Contacts( int remoteHandle ,
                                ModelContext context )
   {
      super( context, "SdtAirlines_Contacts");
      initialize( remoteHandle) ;
   }

   public SdtAirlines_Contacts( int remoteHandle ,
                                StructSdtAirlines_Contacts struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtAirlines_Contacts( )
   {
      super( new ModelContext(SdtAirlines_Contacts.class), "SdtAirlines_Contacts");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Contacttypescode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttSeq") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttseq = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttName") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttname = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttPhone") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttphone = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttEmail") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttemail = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Contacttypesdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Contacttypescode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttSeq_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttName_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttname_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttPhone_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCttEmail_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Airlines.Contacts" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ContactTypesCode", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Contacttypescode));
      oWriter.writeElement("AirLineCttSeq", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Contacts_Airlinecttseq, 4, 0)));
      oWriter.writeElement("AirLineCttName", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Airlinecttname));
      oWriter.writeElement("AirLineCttPhone", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Airlinecttphone));
      oWriter.writeElement("AirLineCttEmail", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Airlinecttemail));
      oWriter.writeElement("ContactTypesDescription", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Contacttypesdescription));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Contacts_Modified, 4, 0)));
      oWriter.writeElement("ContactTypesCode_Z", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Contacttypescode_Z));
      oWriter.writeElement("AirLineCttSeq_Z", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Contacts_Airlinecttseq_Z, 4, 0)));
      oWriter.writeElement("AirLineCttName_Z", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Airlinecttname_Z));
      oWriter.writeElement("AirLineCttPhone_Z", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Airlinecttphone_Z));
      oWriter.writeElement("AirLineCttEmail_Z", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Airlinecttemail_Z));
      oWriter.writeElement("ContactTypesDescription_Z", GXutil.rtrim( gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Contacttypescode( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypescode ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypescode( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypescode_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode = "" ;
      return  ;
   }

   public short getgxTv_SdtAirlines_Contacts_Airlinecttseq( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttseq ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttseq( short value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttseq = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttseq_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttseq = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Airlinecttname( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttname ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttname( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttname = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttname_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttname = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Airlinecttphone( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttphone ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttphone( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttphone = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttphone_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttphone = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Airlinecttemail( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttemail ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttemail( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttemail = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttemail_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttemail = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Contacttypesdescription( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypesdescription ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypesdescription( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypesdescription = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypesdescription_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Contacttypesdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Mode( )
   {
      return gxTv_SdtAirlines_Contacts_Mode ;
   }

   public void setgxTv_SdtAirlines_Contacts_Mode( String value )
   {
      gxTv_SdtAirlines_Contacts_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Mode_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtAirlines_Contacts_Modified( )
   {
      return gxTv_SdtAirlines_Contacts_Modified ;
   }

   public void setgxTv_SdtAirlines_Contacts_Modified( short value )
   {
      gxTv_SdtAirlines_Contacts_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Modified_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Contacttypescode_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypescode_Z ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypescode_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypescode_Z_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode_Z = "" ;
      return  ;
   }

   public short getgxTv_SdtAirlines_Contacts_Airlinecttseq_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttseq_Z ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttseq_Z( short value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttseq_Z_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Airlinecttname_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttname_Z ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttname_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttname_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttname_Z_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttname_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Airlinecttphone_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttphone_Z ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttphone_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttphone_Z_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Airlinecttemail_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttemail_Z ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttemail_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Airlinecttemail_Z_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z_SetNull( )
   {
      gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttseq = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Airlinecttname = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttphone = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttemail = "" ;
      gxTv_SdtAirlines_Contacts_Contacttypesdescription = "" ;
      gxTv_SdtAirlines_Contacts_Mode = "" ;
      gxTv_SdtAirlines_Contacts_Modified = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Contacttypescode_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Airlinecttname_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = "" ;
      gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char4 = "" ;
      return  ;
   }

   public SdtAirlines_Contacts Clone( )
   {
      return (SdtAirlines_Contacts)(clone()) ;
   }

   public void setStruct( StructSdtAirlines_Contacts struct )
   {
      setgxTv_SdtAirlines_Contacts_Contacttypescode(struct.getContacttypescode());
      setgxTv_SdtAirlines_Contacts_Airlinecttseq(struct.getAirlinecttseq());
      setgxTv_SdtAirlines_Contacts_Airlinecttname(struct.getAirlinecttname());
      setgxTv_SdtAirlines_Contacts_Airlinecttphone(struct.getAirlinecttphone());
      setgxTv_SdtAirlines_Contacts_Airlinecttemail(struct.getAirlinecttemail());
      setgxTv_SdtAirlines_Contacts_Contacttypesdescription(struct.getContacttypesdescription());
      setgxTv_SdtAirlines_Contacts_Mode(struct.getMode());
      setgxTv_SdtAirlines_Contacts_Modified(struct.getModified());
      setgxTv_SdtAirlines_Contacts_Contacttypescode_Z(struct.getContacttypescode_Z());
      setgxTv_SdtAirlines_Contacts_Airlinecttseq_Z(struct.getAirlinecttseq_Z());
      setgxTv_SdtAirlines_Contacts_Airlinecttname_Z(struct.getAirlinecttname_Z());
      setgxTv_SdtAirlines_Contacts_Airlinecttphone_Z(struct.getAirlinecttphone_Z());
      setgxTv_SdtAirlines_Contacts_Airlinecttemail_Z(struct.getAirlinecttemail_Z());
      setgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z(struct.getContacttypesdescription_Z());
   }

   public StructSdtAirlines_Contacts getStruct( )
   {
      StructSdtAirlines_Contacts struct = new StructSdtAirlines_Contacts ();
      struct.setContacttypescode(getgxTv_SdtAirlines_Contacts_Contacttypescode());
      struct.setAirlinecttseq(getgxTv_SdtAirlines_Contacts_Airlinecttseq());
      struct.setAirlinecttname(getgxTv_SdtAirlines_Contacts_Airlinecttname());
      struct.setAirlinecttphone(getgxTv_SdtAirlines_Contacts_Airlinecttphone());
      struct.setAirlinecttemail(getgxTv_SdtAirlines_Contacts_Airlinecttemail());
      struct.setContacttypesdescription(getgxTv_SdtAirlines_Contacts_Contacttypesdescription());
      struct.setMode(getgxTv_SdtAirlines_Contacts_Mode());
      struct.setModified(getgxTv_SdtAirlines_Contacts_Modified());
      struct.setContacttypescode_Z(getgxTv_SdtAirlines_Contacts_Contacttypescode_Z());
      struct.setAirlinecttseq_Z(getgxTv_SdtAirlines_Contacts_Airlinecttseq_Z());
      struct.setAirlinecttname_Z(getgxTv_SdtAirlines_Contacts_Airlinecttname_Z());
      struct.setAirlinecttphone_Z(getgxTv_SdtAirlines_Contacts_Airlinecttphone_Z());
      struct.setAirlinecttemail_Z(getgxTv_SdtAirlines_Contacts_Airlinecttemail_Z());
      struct.setContacttypesdescription_Z(getgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z());
      return struct ;
   }

   protected short gxTv_SdtAirlines_Contacts_Airlinecttseq ;
   protected short gxTv_SdtAirlines_Contacts_Modified ;
   protected short gxTv_SdtAirlines_Contacts_Airlinecttseq_Z ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtAirlines_Contacts_Mode ;
   protected String sTagName ;
   protected String GXt_char4 ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypescode ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttname ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttphone ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttemail ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypesdescription ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypescode_Z ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttname_Z ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttphone_Z ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttemail_Z ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z ;
}


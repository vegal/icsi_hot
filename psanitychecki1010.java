/*
               File: sanitychecki1010
        Description: sanitychecki1010
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:23.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class psanitychecki1010 extends GXProcedure
{
   public psanitychecki1010( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psanitychecki1010.class ), "" );
   }

   public psanitychecki1010( int remoteHandle ,
                             ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String[] aP2 )
   {
      psanitychecki1010.this.AV24FileNa = aP0;
      psanitychecki1010.this.AV68lccbEm = aP1;
      psanitychecki1010.this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV26Caminh = AV24FileNa ;
      AV18File.setSource( AV26Caminh );
      if ( AV18File.exists() )
      {
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfropen( AV26Caminh, 255, "%", "\"", "") ;
         AV28RegCou = 0 ;
         AV51RegCou = 0 ;
         AV10ErroHe = "Header Not Found." ;
         AV11ErroTr = "Trailer Not Found." ;
         while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
         {
            GXv_char1[0] = AV9linha ;
            GXt_int2 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char1, (short)(255)) ;
            AV9linha = GXv_char1[0] ;
            AV27RetVal = GXt_int2 ;
            AV28RegCou = (long)(AV28RegCou+1) ;
            if ( ( AV28RegCou == 1 ) )
            {
               /* Execute user subroutine: S1182 */
               S1182 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               AV10ErroHe = AV15Erro ;
            }
            if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "DE") == 0 ) )
            {
               AV51RegCou = (long)(AV51RegCou+1) ;
            }
         }
         if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV9linha))==0)) )
         {
            /* Execute user subroutine: S12130 */
            S12130 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            AV11ErroTr = AV15Erro ;
         }
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         AV25msgErr = "" ;
         if ( ! ( ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV11ErroTr))==0)) ) )
         {
            AV25msgErr = AV10ErroHe + "=ErrHeader / ErrTrailer=" + AV11ErroTr ;
            GXt_svchar3 = AV29ShortN ;
            GXv_char1[0] = AV26Caminh ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2shortname(remoteHandle, context).execute( GXv_char1, GXv_char4) ;
            psanitychecki1010.this.AV26Caminh = GXv_char1[0] ;
            psanitychecki1010.this.GXt_svchar3 = GXv_char4[0] ;
            AV29ShortN = GXt_svchar3 ;
            GXt_svchar3 = AV12BasePa ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getpath(remoteHandle, context).execute( AV26Caminh, GXv_char4) ;
            psanitychecki1010.this.GXt_svchar3 = GXv_char4[0] ;
            AV12BasePa = GXt_svchar3 ;
            AV30SubFol = "Error\\" ;
            AV12BasePa = AV12BasePa + AV30SubFol ;
            AV19NewFil = GXutil.trim( AV12BasePa) + GXutil.trim( AV29ShortN) ;
            AV18File.setSource( AV26Caminh );
            AV18File.rename(GXutil.trim( AV19NewFil));
            AV14ShortN = GXutil.trim( AV29ShortN) + ".err" ;
            AV31Arquiv = GXutil.trim( AV12BasePa) + GXutil.trim( AV14ShortN) ;
            AV13LOGFil.openURL(AV31Arquiv);
            AV10ErroHe = AV10ErroHe + GXutil.newLine( ) ;
            AV11ErroTr = AV11ErroTr + GXutil.newLine( ) ;
            AV13LOGFil.writeRawText(AV10ErroHe);
            AV13LOGFil.writeRawText(AV11ErroTr);
            AV13LOGFil.close();
            GXt_svchar3 = AV23Subjec ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_SUBJ_10", "Mensagem no subject do e-mail do tipo Sanity i1010", "S", "Problem file i1010 generation", GXv_char4) ;
            psanitychecki1010.this.GXt_svchar3 = GXv_char4[0] ;
            AV23Subjec = GXt_svchar3 ;
            GXt_svchar3 = AV21Body ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_BODY_10", "Mensagem no corpo do e-mail do tipo Sanity i1010", "S", "<br>The file [FILE] type i1010 was rejected in the Sanity Check.<br><br>Please check the file.<br><br>Thanks", GXv_char4) ;
            psanitychecki1010.this.GXt_svchar3 = GXv_char4[0] ;
            AV21Body = GXt_svchar3 ;
            AV21Body = GXutil.strReplace( AV21Body, "[FILE]", AV29ShortN) ;
            GXt_svchar3 = AV40To ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "", "S", "", GXv_char4) ;
            psanitychecki1010.this.GXt_svchar3 = GXv_char4[0] ;
            AV40To = GXutil.trim( GXt_svchar3) ;
            GXt_svchar3 = AV22CC ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "", "S", "", GXv_char4) ;
            psanitychecki1010.this.GXt_svchar3 = GXv_char4[0] ;
            AV22CC = GXutil.trim( GXt_svchar3) ;
            AV20BCC = "" ;
            GX_I = 1 ;
            while ( ( GX_I <= 5 ) )
            {
               AV8Anexos[GX_I-1] = "" ;
               GX_I = (int)(GX_I+1) ;
            }
            new penviaemail(remoteHandle, context).execute( AV23Subjec, AV21Body, AV40To, AV22CC, AV20BCC, AV8Anexos) ;
         }
      }
      cleanup();
   }

   public void S1182( )
   {
      /* 'CHECKHEADER' Routine */
      AV15Erro = "" ;
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "HC") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Identifier Not equal HC /" ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 3, 6), "000001") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Sequencial Not equal 000001 / " ;
      }
      AV50yyyy = GXutil.substring( AV9linha, 17, 4) ;
      AV33mm = GXutil.substring( AV9linha, 21, 2) ;
      AV34dd = GXutil.substring( AV9linha, 23, 2) ;
      AV42hh = GXutil.substring( AV9linha, 25, 2) ;
      AV43mi = GXutil.substring( AV9linha, 27, 2) ;
      AV44ss = GXutil.substring( AV9linha, 29, 2) ;
      AV35Nyy = (int)(GXutil.val( GXutil.trim( AV50yyyy), ".")) ;
      AV36Nmm = (int)(GXutil.val( AV33mm, ".")) ;
      AV37Ndd = (int)(GXutil.val( AV34dd, ".")) ;
      AV45Nhh = (int)(GXutil.val( AV42hh, ".")) ;
      AV46Nmi = (int)(GXutil.val( AV43mi, ".")) ;
      AV47Nss = (int)(GXutil.val( AV44ss, ".")) ;
      AV16DataGe = localUtil.ymdhmsToT( (short)(AV35Nyy), (byte)(AV36Nmm), (byte)(AV37Ndd), (byte)(AV45Nhh), (byte)(AV46Nmi), (byte)(AV47Nss)) ;
      if ( (GXutil.nullDate().equals(AV16DataGe)) )
      {
         AV15Erro = AV15Erro + "Header Processing Date Not Valid / " ;
      }
      AV55Datax = localUtil.ymdtod( AV35Nyy, AV36Nmm, AV37Ndd) ;
      AV53Data1 = GXutil.resetTime( AV55Datax );
      AV54Data2 = GXutil.resetTime( GXutil.dadd( AV55Datax , + ( 1 )) );
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 31, 3), "000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Information 31 Not equal 000 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 34, 3), "957") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Company Not equal 957 / " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 50, 5) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Invoice Not Valid / " ;
      }
   }

   public void S12130( )
   {
      /* 'CHECKTRAILER' Routine */
      AV15Erro = "" ;
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "TC") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Identifier Not equal TC " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 3, 6) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Sequencial Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 9, 8), "00000000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Information 9 Not equal 00000000 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 77, 3), "000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Information 77 Not equal 000 / " ;
      }
      AV52TotalR = (long)(GXutil.val( GXutil.substring( AV9linha, 80, 5), ".")) ;
      if ( ( AV52TotalR != AV51RegCou ) )
      {
         AV15Erro = AV15Erro + "Trailer Total Lines DE Not OK: " + GXutil.trim( GXutil.str( AV52TotalR, 10, 0)) + " <> " + GXutil.trim( GXutil.str( AV51RegCou, 10, 0)) + " / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 85, 5), "00000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Information Not 85 equal 00000 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 105, 15), "000000000000000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Information Not 105 equal 000000000000000 / " ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( AV15Erro))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) )
      {
         AV56vTotal = (double)(GXutil.val( GXutil.substring( AV9linha, 17, 15), ".")/ (double) (100)) ;
         AV57vTotal = (double)(GXutil.val( GXutil.substring( AV9linha, 32, 15), ".")/ (double) (100)) ;
         AV58vTotal = (double)(GXutil.val( GXutil.substring( AV9linha, 47, 15), ".")/ (double) (100)) ;
         AV59vTotal = (double)(GXutil.val( GXutil.substring( AV9linha, 62, 15), ".")/ (double) (100)) ;
         /* Using cursor P007R2 */
         pr_default.execute(0, new Object[] {AV68lccbEm, AV53Data1, AV54Data2});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1227lccbO = P007R2_A1227lccbO[0] ;
            A1184lccbS = P007R2_A1184lccbS[0] ;
            n1184lccbS = P007R2_n1184lccbS[0] ;
            A1191lccbS = P007R2_A1191lccbS[0] ;
            n1191lccbS = P007R2_n1191lccbS[0] ;
            A1150lccbE = P007R2_A1150lccbE[0] ;
            A1190lccbS = P007R2_A1190lccbS[0] ;
            n1190lccbS = P007R2_n1190lccbS[0] ;
            A1172lccbS = P007R2_A1172lccbS[0] ;
            n1172lccbS = P007R2_n1172lccbS[0] ;
            A1515lccbC = P007R2_A1515lccbC[0] ;
            n1515lccbC = P007R2_n1515lccbC[0] ;
            A1171lccbT = P007R2_A1171lccbT[0] ;
            n1171lccbT = P007R2_n1171lccbT[0] ;
            A1170lccbI = P007R2_A1170lccbI[0] ;
            n1170lccbI = P007R2_n1170lccbI[0] ;
            A1194lccbS = P007R2_A1194lccbS[0] ;
            n1194lccbS = P007R2_n1194lccbS[0] ;
            A1224lccbC = P007R2_A1224lccbC[0] ;
            A1222lccbI = P007R2_A1222lccbI[0] ;
            A1223lccbD = P007R2_A1223lccbD[0] ;
            A1225lccbC = P007R2_A1225lccbC[0] ;
            A1226lccbA = P007R2_A1226lccbA[0] ;
            A1228lccbF = P007R2_A1228lccbF[0] ;
            AV60TotSal = (double)(AV60TotSal+((A1172lccbS))) ;
            AV61TotDow = (double)(AV61TotDow+A1515lccbC) ;
            AV62TotTip = (double)(AV62TotTip+A1171lccbT) ;
            AV63TotIns = (double)(AV63TotIns+A1170lccbI) ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV64dif1 = (double)(AV56vTotal-AV60TotSal) ;
         if ( ( AV64dif1 <= -0.005 ) || ( AV64dif1 >= 0.005 ) )
         {
            AV15Erro = AV15Erro + "Total Sales Amount not OK: " + GXutil.trim( GXutil.str( AV56vTotal, 14, 2)) + " <> " + GXutil.trim( GXutil.str( AV60TotSal, 14, 2)) ;
         }
         AV65dif2 = (double)(AV57vTotal-AV61TotDow) ;
         if ( ( AV65dif2 <= -0.005 ) || ( AV65dif2 >= 0.005 ) )
         {
            AV15Erro = AV15Erro + "Total DownPayment Not Ok: " + GXutil.trim( GXutil.str( AV57vTotal, 14, 2)) + " <> " + GXutil.trim( GXutil.str( AV61TotDow, 14, 2)) + " : " + GXutil.trim( GXutil.str( AV65dif2, 14, 2)) + " / " ;
         }
         AV66dif3 = (double)(AV58vTotal-AV62TotTip) ;
         if ( ( AV66dif3 <= -0.005 ) || ( AV66dif3 >= 0.005 ) )
         {
            AV15Erro = AV15Erro + "Total Taxes Not Ok: " + GXutil.trim( GXutil.str( AV58vTotal, 14, 2)) + " <> " + GXutil.trim( GXutil.str( AV62TotTip, 14, 2)) + " : " + GXutil.trim( GXutil.str( AV66dif3, 14, 2)) + " / " ;
         }
         AV67dif4 = (double)(AV59vTotal-AV63TotIns) ;
         if ( ( AV67dif4 <= -0.005 ) || ( AV67dif4 >= 0.005 ) )
         {
            AV15Erro = AV15Erro + "Total Installments Not Ok: " + GXutil.trim( GXutil.str( AV59vTotal, 14, 2)) + " <> " + GXutil.trim( GXutil.str( AV63TotIns, 14, 2)) + " : " + GXutil.trim( GXutil.str( AV67dif4, 14, 2)) + " / " ;
         }
      }
   }

   protected void cleanup( )
   {
      this.aP2[0] = psanitychecki1010.this.AV25msgErr;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25msgErr = "" ;
      AV26Caminh = "" ;
      AV18File = new com.genexus.util.GXFile();
      AV27RetVal = 0 ;
      AV28RegCou = 0 ;
      AV51RegCou = 0 ;
      AV10ErroHe = "" ;
      AV11ErroTr = "" ;
      AV9linha = "" ;
      GXt_int2 = (short)(0) ;
      returnInSub = false ;
      AV15Erro = "" ;
      AV29ShortN = "" ;
      GXv_char1 = new String [1] ;
      AV12BasePa = "" ;
      AV30SubFol = "" ;
      AV19NewFil = "" ;
      AV14ShortN = "" ;
      AV31Arquiv = "" ;
      AV13LOGFil = new com.genexus.xml.XMLWriter();
      AV23Subjec = "" ;
      AV21Body = "" ;
      AV40To = "" ;
      AV22CC = "" ;
      GXt_svchar3 = "" ;
      GXv_char4 = new String [1] ;
      AV20BCC = "" ;
      GX_I = 0 ;
      AV8Anexos = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV8Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV50yyyy = "" ;
      AV33mm = "" ;
      AV34dd = "" ;
      AV42hh = "" ;
      AV43mi = "" ;
      AV44ss = "" ;
      AV35Nyy = 0 ;
      AV36Nmm = 0 ;
      AV37Ndd = 0 ;
      AV45Nhh = 0 ;
      AV46Nmi = 0 ;
      AV47Nss = 0 ;
      AV16DataGe = GXutil.resetTime( GXutil.nullDate() );
      AV55Datax = GXutil.nullDate() ;
      AV53Data1 = GXutil.resetTime( GXutil.nullDate() );
      AV54Data2 = GXutil.resetTime( GXutil.nullDate() );
      AV39Sequen = "" ;
      AV52TotalR = 0 ;
      AV56vTotal = 0 ;
      AV57vTotal = 0 ;
      AV58vTotal = 0 ;
      AV59vTotal = 0 ;
      scmdbuf = "" ;
      P007R2_A1227lccbO = new String[] {""} ;
      P007R2_A1184lccbS = new String[] {""} ;
      P007R2_n1184lccbS = new boolean[] {false} ;
      P007R2_A1191lccbS = new String[] {""} ;
      P007R2_n1191lccbS = new boolean[] {false} ;
      P007R2_A1150lccbE = new String[] {""} ;
      P007R2_A1190lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P007R2_n1190lccbS = new boolean[] {false} ;
      P007R2_A1172lccbS = new double[1] ;
      P007R2_n1172lccbS = new boolean[] {false} ;
      P007R2_A1515lccbC = new double[1] ;
      P007R2_n1515lccbC = new boolean[] {false} ;
      P007R2_A1171lccbT = new double[1] ;
      P007R2_n1171lccbT = new boolean[] {false} ;
      P007R2_A1170lccbI = new double[1] ;
      P007R2_n1170lccbI = new boolean[] {false} ;
      P007R2_A1194lccbS = new String[] {""} ;
      P007R2_n1194lccbS = new boolean[] {false} ;
      P007R2_A1224lccbC = new String[] {""} ;
      P007R2_A1222lccbI = new String[] {""} ;
      P007R2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007R2_A1225lccbC = new String[] {""} ;
      P007R2_A1226lccbA = new String[] {""} ;
      P007R2_A1228lccbF = new String[] {""} ;
      A1227lccbO = "" ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1191lccbS = "" ;
      n1191lccbS = false ;
      A1150lccbE = "" ;
      A1190lccbS = GXutil.resetTime( GXutil.nullDate() );
      n1190lccbS = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1515lccbC = 0 ;
      n1515lccbC = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1224lccbC = "" ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1228lccbF = "" ;
      AV60TotSal = 0 ;
      AV61TotDow = 0 ;
      AV62TotTip = 0 ;
      AV63TotIns = 0 ;
      AV64dif1 = 0 ;
      AV65dif2 = 0 ;
      AV66dif3 = 0 ;
      AV67dif4 = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new psanitychecki1010__default(),
         new Object[] {
             new Object[] {
            P007R2_A1227lccbO, P007R2_A1184lccbS, P007R2_n1184lccbS, P007R2_A1191lccbS, P007R2_n1191lccbS, P007R2_A1150lccbE, P007R2_A1190lccbS, P007R2_n1190lccbS, P007R2_A1172lccbS, P007R2_n1172lccbS,
            P007R2_A1515lccbC, P007R2_n1515lccbC, P007R2_A1171lccbT, P007R2_n1171lccbT, P007R2_A1170lccbI, P007R2_n1170lccbI, P007R2_A1194lccbS, P007R2_n1194lccbS, P007R2_A1224lccbC, P007R2_A1222lccbI,
            P007R2_A1223lccbD, P007R2_A1225lccbC, P007R2_A1226lccbA, P007R2_A1228lccbF
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short GXt_int2 ;
   private short Gx_err ;
   private int GX_I ;
   private int AV35Nyy ;
   private int AV36Nmm ;
   private int AV37Ndd ;
   private int AV45Nhh ;
   private int AV46Nmi ;
   private int AV47Nss ;
   private long AV27RetVal ;
   private long AV28RegCou ;
   private long AV51RegCou ;
   private long AV52TotalR ;
   private double AV56vTotal ;
   private double AV57vTotal ;
   private double AV58vTotal ;
   private double AV59vTotal ;
   private double A1172lccbS ;
   private double A1515lccbC ;
   private double A1171lccbT ;
   private double A1170lccbI ;
   private double AV60TotSal ;
   private double AV61TotDow ;
   private double AV62TotTip ;
   private double AV63TotIns ;
   private double AV64dif1 ;
   private double AV65dif2 ;
   private double AV66dif3 ;
   private double AV67dif4 ;
   private String AV68lccbEm ;
   private String GXv_char1[] ;
   private String AV12BasePa ;
   private String GXv_char4[] ;
   private String AV50yyyy ;
   private String AV33mm ;
   private String AV34dd ;
   private String AV42hh ;
   private String AV43mi ;
   private String AV44ss ;
   private String scmdbuf ;
   private String A1227lccbO ;
   private String A1184lccbS ;
   private String A1191lccbS ;
   private String A1150lccbE ;
   private String A1194lccbS ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1228lccbF ;
   private java.util.Date AV16DataGe ;
   private java.util.Date AV53Data1 ;
   private java.util.Date AV54Data2 ;
   private java.util.Date A1190lccbS ;
   private java.util.Date AV55Datax ;
   private java.util.Date A1223lccbD ;
   private boolean returnInSub ;
   private boolean n1184lccbS ;
   private boolean n1191lccbS ;
   private boolean n1190lccbS ;
   private boolean n1172lccbS ;
   private boolean n1515lccbC ;
   private boolean n1171lccbT ;
   private boolean n1170lccbI ;
   private boolean n1194lccbS ;
   private String AV24FileNa ;
   private String AV25msgErr ;
   private String AV26Caminh ;
   private String AV10ErroHe ;
   private String AV11ErroTr ;
   private String AV9linha ;
   private String AV15Erro ;
   private String AV29ShortN ;
   private String AV30SubFol ;
   private String AV19NewFil ;
   private String AV14ShortN ;
   private String AV31Arquiv ;
   private String AV23Subjec ;
   private String AV21Body ;
   private String AV40To ;
   private String AV22CC ;
   private String GXt_svchar3 ;
   private String AV20BCC ;
   private String AV8Anexos[] ;
   private String AV39Sequen ;
   private com.genexus.xml.XMLWriter AV13LOGFil ;
   private com.genexus.util.GXFile AV18File ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P007R2_A1227lccbO ;
   private String[] P007R2_A1184lccbS ;
   private boolean[] P007R2_n1184lccbS ;
   private String[] P007R2_A1191lccbS ;
   private boolean[] P007R2_n1191lccbS ;
   private String[] P007R2_A1150lccbE ;
   private java.util.Date[] P007R2_A1190lccbS ;
   private boolean[] P007R2_n1190lccbS ;
   private double[] P007R2_A1172lccbS ;
   private boolean[] P007R2_n1172lccbS ;
   private double[] P007R2_A1515lccbC ;
   private boolean[] P007R2_n1515lccbC ;
   private double[] P007R2_A1171lccbT ;
   private boolean[] P007R2_n1171lccbT ;
   private double[] P007R2_A1170lccbI ;
   private boolean[] P007R2_n1170lccbI ;
   private String[] P007R2_A1194lccbS ;
   private boolean[] P007R2_n1194lccbS ;
   private String[] P007R2_A1224lccbC ;
   private String[] P007R2_A1222lccbI ;
   private java.util.Date[] P007R2_A1223lccbD ;
   private String[] P007R2_A1225lccbC ;
   private String[] P007R2_A1226lccbA ;
   private String[] P007R2_A1228lccbF ;
}

final  class psanitychecki1010__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007R2", "SELECT [lccbOpCode], [lccbStatus], [lccbSubType], [lccbEmpCod], [lccbSubTime], [lccbSaleAmount], [lccbCashAmount], [lccbTip], [lccbInstAmount], [lccbSubRO], [lccbCCard], [lccbIATA], [lccbDate], [lccbCCNum], [lccbAppCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (NOLOCK) WHERE ([lccbOpCode] = 'S' and [lccbStatus] = 'OKI1010' and [lccbEmpCod] = ?) AND ([lccbSubTime] >= ?) AND ([lccbSubTime] <= ?) AND ([lccbSubType] = 'F') ORDER BY [lccbOpCode], [lccbStatus], [lccbEmpCod], [lccbCCard], [lccbSubRO] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 8) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDateTime(5) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((double[]) buf[8])[0] = rslt.getDouble(6) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((double[]) buf[10])[0] = rslt.getDouble(7) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(8) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((double[]) buf[14])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(10, 10) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(11, 2) ;
               ((String[]) buf[19])[0] = rslt.getString(12, 7) ;
               ((java.util.Date[]) buf[20])[0] = rslt.getGXDate(13) ;
               ((String[]) buf[21])[0] = rslt.getString(14, 44) ;
               ((String[]) buf[22])[0] = rslt.getString(15, 20) ;
               ((String[]) buf[23])[0] = rslt.getString(16, 19) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               stmt.setDateTime(3, (java.util.Date)parms[2], false);
               break;
      }
   }

}


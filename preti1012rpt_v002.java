/*
               File: RETi1012rpt_V002
        Description: RETi1012rpt_ V002
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:11.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.reports.*;

public final  class preti1012rpt_v002 extends GXReport
{
   public preti1012rpt_v002( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( preti1012rpt_v002.class ), "" );
   }

   public preti1012rpt_v002( int remoteHandle ,
                             ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 ,
                        String[] aP5 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 ,
                             String[] aP5 )
   {
      preti1012rpt_v002.this.AV10lccbEm = aP0[0];
      this.aP0 = aP0;
      preti1012rpt_v002.this.AV86EmpNom = aP1[0];
      this.aP1 = aP1;
      preti1012rpt_v002.this.AV80Pathpd = aP2[0];
      this.aP2 = aP2;
      preti1012rpt_v002.this.AV82PathTx = aP3[0];
      this.aP3 = aP3;
      preti1012rpt_v002.this.AV44DataB = aP4[0];
      this.aP4 = aP4;
      preti1012rpt_v002.this.AV41DebugM = aP5[0];
      this.aP5 = aP5;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      M_top = 0 ;
      M_bot = 5 ;
      P_lines = (int)(66-M_bot) ;
      getPrinter().GxClearAttris() ;
      add_metrics( ) ;
      lineHeight = 10 ;
      PrtOffset = 0 ;
      gxXPage = 96 ;
      gxYPage = 96 ;
      getPrinter().GxSetDocName(AV80Pathpd) ;
      getPrinter().GxSetDocFormat("PDF") ;
      try
      {
         Gx_out = "FIL" ;
         if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 9, 16838, 11906, 0, 1, 1, 0, 1, 1) )
         {
            cleanup();
            return;
         }
         getPrinter().setModal(true) ;
         P_lines = (int)(gxYPage-(lineHeight*5)) ;
         Gx_line = (int)(P_lines+1) ;
         getPrinter().setPageLines(P_lines);
         getPrinter().setLineHeight(lineHeight);
         getPrinter().setM_top(M_top);
         getPrinter().setM_bot(M_bot);
         GXt_char1 = AV107Utili ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "UTILIZA_FIM_LINHA", "Utiliza Fim de Linha", "S", "N", GXv_svchar2) ;
         preti1012rpt_v002.this.GXt_char1 = GXv_svchar2[0] ;
         AV107Utili = GXt_char1 ;
         GXt_char1 = AV132Bande ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER", "C�digo do cart�o Hipercard", "S", "HC", GXv_svchar2) ;
         preti1012rpt_v002.this.GXt_char1 = GXv_svchar2[0] ;
         AV132Bande = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV133NovaB ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER_NEW", "C�digo do cart�o Hipercard para arquivos output", "S", "HP", GXv_svchar2) ;
         preti1012rpt_v002.this.GXt_char1 = GXv_svchar2[0] ;
         AV133NovaB = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV137Bande ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO", "C�digo do cart�o ELO", "S", "EL", GXv_svchar2) ;
         preti1012rpt_v002.this.GXt_char1 = GXv_svchar2[0] ;
         AV137Bande = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV138NovaB ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO_NEW", "C�digo do cart�o ELO para arquivos output", "S", "EL", GXv_svchar2) ;
         preti1012rpt_v002.this.GXt_char1 = GXv_svchar2[0] ;
         AV138NovaB = GXutil.trim( GXt_char1) ;
         AV54Versao = "00007" ;
         AV41DebugM = GXutil.trim( GXutil.upper( AV41DebugM)) ;
         if ( ( GXutil.strSearch( AV41DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( "i1011/i1012 - Version "+AV54Versao );
            context.msgStatus( "  Running mode: ["+AV41DebugM+"] - Started at "+GXutil.time( ) );
            context.msgStatus( "  Creating i1011/i1012 file for "+AV10lccbEm );
            context.msgStatus( "    "+AV80Pathpd );
            context.msgStatus( "    "+AV82PathTx );
         }
         if ( ( GXutil.strSearch( AV41DebugM, "TESTMODE", 1) > 0 ) )
         {
            AV85TestMo = (byte)(1) ;
         }
         else
         {
            AV85TestMo = (byte)(0) ;
         }
         if ( ( GXutil.strSearch( AV41DebugM, "SEPPIPE", 1) > 0 ) )
         {
            AV53Sep = "|" ;
         }
         else
         {
            AV53Sep = "" ;
         }
         AV45HoraA = GXutil.time( ) ;
         AV46HoraB = GXutil.substring( AV45HoraA, 1, 2) ;
         AV46HoraB = AV46HoraB + GXutil.substring( AV45HoraA, 4, 2) ;
         AV143GXLvl = (byte)(0) ;
         /* Using cursor P00712 */
         pr_default.execute(0, new Object[] {AV10lccbEm, AV10lccbEm});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1488ICSI_ = P00712_A1488ICSI_[0] ;
            A1487ICSI_ = P00712_A1487ICSI_[0] ;
            A1479ICSI_ = P00712_A1479ICSI_[0] ;
            n1479ICSI_ = P00712_n1479ICSI_[0] ;
            AV143GXLvl = (byte)(1) ;
            A1479ICSI_ = (int)(A1479ICSI_+1) ;
            n1479ICSI_ = false ;
            if ( ( A1479ICSI_ > 99999 ) )
            {
               A1479ICSI_ = 1 ;
               n1479ICSI_ = false ;
            }
            AV49ICSI_C = A1479ICSI_ ;
            /* Using cursor P00713 */
            pr_default.execute(1, new Object[] {new Boolean(n1479ICSI_), new Integer(A1479ICSI_), A1487ICSI_, A1488ICSI_});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ( AV143GXLvl == 0 ) )
         {
            context.msgStatus( "  Warning: "+AV10lccbEm+" not configured, assuming file sequence = 1" );
            AV49ICSI_C = 1 ;
            if ( ( AV85TestMo == 0 ) )
            {
               /*
                  INSERT RECORD ON TABLE ICSI_CCINFO

               */
               A1487ICSI_ = AV10lccbEm ;
               A1479ICSI_ = 1 ;
               n1479ICSI_ = false ;
               /* Using cursor P00714 */
               pr_default.execute(2, new Object[] {A1487ICSI_, A1488ICSI_, new Boolean(n1479ICSI_), new Integer(A1479ICSI_)});
               if ( (pr_default.getStatus(2) == 1) )
               {
                  Gx_err = (short)(1) ;
                  Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               }
               else
               {
                  Gx_err = (short)(0) ;
                  Gx_emsg = "" ;
               }
               /* End Insert */
            }
         }
         /* Execute user subroutine: S1181 */
         S1181 ();
         if ( returnInSub )
         {
            getPrinter().GxEndPage() ;
            /* Close printer file */
            getPrinter().GxEndDocument() ;
            endPrinter();
            returnInSub = true;
            cleanup();
            if (true) return;
         }
         if ( ( GXutil.strSearch( AV41DebugM, "NOBATCH", 1) > 0 ) )
         {
            GXutil.msg( this, "T�rmino do processamento" );
         }
         /* Print footer for last page */
         ToSkip = (int)(P_lines+1) ;
         h710( true, 0) ;
         /* Close printer file */
         getPrinter().GxEndDocument() ;
         endPrinter();
      }
      catch ( ProcessInterruptedException e )
      {
      }
      cleanup();
   }

   public void S1181( ) throws ProcessInterruptedException
   {
      /* 'MAIN' Routine */
      AV36SeqReg = 0 ;
      if ( ( GXutil.strcmp(AV107Utili, "S") == 0 ) )
      {
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwopen( AV82PathTx, AV53Sep, "", (byte)(0), "") ;
      }
      else
      {
         AV108XML10.openURL(AV82PathTx);
      }
      AV74iPos = GXutil.strSearch( AV41DebugM, "DATE=", 1) ;
      if ( ( AV74iPos > 0 ) )
      {
         AV74iPos = (int)(AV74iPos+5) ;
         AV73sTemp = GXutil.substring( AV41DebugM, AV74iPos, 10) ;
         AV77Ano1 = (short)(GXutil.val( GXutil.substring( AV73sTemp, 1, 4), ".")) ;
         AV78Mes1 = (byte)(GXutil.val( GXutil.substring( AV73sTemp, 6, 2), ".")) ;
         AV79Dia1 = (byte)(GXutil.val( GXutil.substring( AV73sTemp, 9, 2), ".")) ;
         AV76OneDay = localUtil.ymdtod( AV77Ano1, AV78Mes1, AV79Dia1) ;
      }
      else
      {
         AV76OneDay = GXutil.nullDate() ;
      }
      if ( ( GXutil.strcmp(AV107Utili, "S") == 0 ) )
      {
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "C", (short)(1)) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV10lccbEm, (short)(3)) ;
         AV73sTemp = GXutil.substring( AV44DataB, 3, 6) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(6)) ;
         GXt_char6 = AV73sTemp ;
         GXv_int7[0] = AV49ICSI_C ;
         GXv_int8[0] = (byte)(3) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_svchar2[0] = GXt_char6 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int8, GXv_int9, GXv_svchar2) ;
         preti1012rpt_v002.this.AV49ICSI_C = (int)((int)(GXv_int7[0])) ;
         preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
         AV73sTemp = GXt_char6 ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(3)) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV44DataB), (short)(8)) ;
         AV104Num04 = (short)(GXutil.val( AV46HoraB, ".")) ;
         AV105Txt = localUtil.format( AV104Num04, "9999") ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV105Txt, (short)(4)) ;
         GXt_char6 = AV73sTemp ;
         GXv_int7[0] = AV49ICSI_C ;
         GXv_int9[0] = (byte)(5) ;
         GXv_int8[0] = (byte)(0) ;
         GXv_svchar2[0] = GXt_char6 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_svchar2) ;
         preti1012rpt_v002.this.AV49ICSI_C = (int)((int)(GXv_int7[0])) ;
         preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
         AV73sTemp = GXt_char6 ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(5)) ;
         AV73sTemp = GXutil.space( (short)(210)) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(210)) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      else
      {
         GXt_char6 = AV109Linha ;
         GXv_int7[0] = AV49ICSI_C ;
         GXv_int9[0] = (byte)(5) ;
         GXv_int8[0] = (byte)(0) ;
         GXv_svchar2[0] = GXt_char6 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_svchar2) ;
         preti1012rpt_v002.this.AV49ICSI_C = (int)((int)(GXv_int7[0])) ;
         preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
         AV109Linha = "C" + GXutil.padr( AV10lccbEm, (short)(3), " ") + GXutil.substring( AV44DataB, 3, 6) + GXutil.padl( GXutil.left( GXutil.trim( GXutil.str( AV49ICSI_C, 8, 0)), 3), (short)(3), "0") + GXutil.trim( AV44DataB) + GXutil.padl( GXutil.trim( AV46HoraB), (short)(4), "0") + GXutil.padl( GXt_char6, (short)(5), "0") + GXutil.space( (short)(210)) ;
         AV109Linha = AV109Linha + GXutil.newLine( ) ;
         AV108XML10.writeRawText(AV109Linha);
      }
      AV90Inic = "S" ;
      AV144GXLvl = (byte)(0) ;
      pr_default.dynParam(3, new Object[]{ new Object[]{
                                           AV76OneDay ,
                                           A1196lccbR ,
                                           AV10lccbEm ,
                                           A1227lccbO ,
                                           A1184lccbS ,
                                           A1150lccbE },
                                           new int[] {
                                           TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                           }
      });
      /* Using cursor P00715 */
      pr_default.execute(3, new Object[] {AV10lccbEm, AV10lccbEm});
      while ( (pr_default.getStatus(3) != 101) )
      {
         A1228lccbF = P00715_A1228lccbF[0] ;
         A1227lccbO = P00715_A1227lccbO[0] ;
         A1226lccbA = P00715_A1226lccbA[0] ;
         A1225lccbC = P00715_A1225lccbC[0] ;
         A1224lccbC = P00715_A1224lccbC[0] ;
         A1223lccbD = P00715_A1223lccbD[0] ;
         A1222lccbI = P00715_A1222lccbI[0] ;
         A1150lccbE = P00715_A1150lccbE[0] ;
         A1184lccbS = P00715_A1184lccbS[0] ;
         n1184lccbS = P00715_n1184lccbS[0] ;
         A1196lccbR = P00715_A1196lccbR[0] ;
         n1196lccbR = P00715_n1196lccbR[0] ;
         A1167lccbG = P00715_A1167lccbG[0] ;
         n1167lccbG = P00715_n1167lccbG[0] ;
         A1490Distr = P00715_A1490Distr[0] ;
         n1490Distr = P00715_n1490Distr[0] ;
         A1163lccbP = P00715_A1163lccbP[0] ;
         n1163lccbP = P00715_n1163lccbP[0] ;
         A1168lccbI = P00715_A1168lccbI[0] ;
         n1168lccbI = P00715_n1168lccbI[0] ;
         A1170lccbI = P00715_A1170lccbI[0] ;
         n1170lccbI = P00715_n1170lccbI[0] ;
         A1515lccbC = P00715_A1515lccbC[0] ;
         n1515lccbC = P00715_n1515lccbC[0] ;
         A1172lccbS = P00715_A1172lccbS[0] ;
         n1172lccbS = P00715_n1172lccbS[0] ;
         A1171lccbT = P00715_A1171lccbT[0] ;
         n1171lccbT = P00715_n1171lccbT[0] ;
         A1201lccbR = P00715_A1201lccbR[0] ;
         n1201lccbR = P00715_n1201lccbR[0] ;
         A1514lccbF = P00715_A1514lccbF[0] ;
         n1514lccbF = P00715_n1514lccbF[0] ;
         A1194lccbS = P00715_A1194lccbS[0] ;
         n1194lccbS = P00715_n1194lccbS[0] ;
         A1193lccbS = P00715_A1193lccbS[0] ;
         n1193lccbS = P00715_n1193lccbS[0] ;
         A1189lccbS = P00715_A1189lccbS[0] ;
         n1189lccbS = P00715_n1189lccbS[0] ;
         A1202lccbR = getlccbRSubErrorDSC0( A1201lccbR, A1224lccbC) ;
         AV144GXLvl = (byte)(1) ;
         AV10lccbEm = A1150lccbE ;
         AV19lccbIA = A1222lccbI ;
         GXt_char6 = AV96LccbIa ;
         GXv_svchar2[0] = GXt_char6 ;
         new pdigitoverificador(remoteHandle, context).execute( A1222lccbI, GXv_svchar2) ;
         preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
         AV96LccbIa = A1222lccbI + GXt_char6 ;
         AV14lccbDa = A1223lccbD ;
         AV12lccbCC = A1224lccbC ;
         if ( ( GXutil.strcmp(A1224lccbC, AV132Bande) == 0 ) )
         {
            AV136LccbC = AV133NovaB ;
         }
         else if ( ( GXutil.strcmp(A1224lccbC, AV137Bande) == 0 ) )
         {
            AV136LccbC = AV138NovaB ;
         }
         else
         {
            AV136LccbC = A1224lccbC ;
         }
         AV124Distr = A1490Distr ;
         AV120Atrib = A1225lccbC ;
         GXv_svchar2[0] = AV122Resul ;
         new pcrypto(remoteHandle, context).execute( AV120Atrib, "D", GXv_svchar2) ;
         preti1012rpt_v002.this.AV122Resul = GXv_svchar2[0] ;
         AV13lccbCC = AV122Resul ;
         AV11lccbAp = A1226lccbA ;
         AV26lccbOp = A1227lccbO ;
         AV130lccbp = A1163lccbP ;
         AV18lccbFP = A1228lccbF ;
         AV21lccbIn = A1168lccbI ;
         AV23lccbIn = A1170lccbI ;
         AV15lccbDo = A1515lccbC ;
         AV28lccbSa = A1172lccbS ;
         AV129lccbS = GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( AV28lccbSa, 14, 2)), ".", ""), ",", "") ;
         AV129lccbS = GXutil.padl( AV129lccbS, (short)(11), "0") ;
         AV88LccbSa = A1172lccbS ;
         AV33lccbTi = A1171lccbT ;
         AV131lccbt = GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( AV33lccbTi, 14, 2)), ".", ""), ",", "") ;
         AV131lccbt = GXutil.padl( GXutil.trim( AV131lccbt), (short)(11), "0") ;
         AV21lccbIn = A1168lccbI ;
         AV62lccbRS = A1201lccbR ;
         AV91TotalA = (double)(AV91TotalA+AV88LccbSa) ;
         AV92TotLcc = (double)(AV92TotLcc+A1171lccbT) ;
         AV93TotLcc = (double)(AV93TotLcc+A1515lccbC) ;
         AV127lccbF = A1514lccbF ;
         AV128lccbF = GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( A1514lccbF, 14, 2)), ".", ""), ",", "") ;
         AV128lccbF = GXutil.padl( AV128lccbF, (short)(11), "0") ;
         if ( ( GXutil.strcmp(GXutil.trim( A1194lccbS), "") == 0 ) )
         {
            if ( ( GXutil.strcmp(AV12lccbCC, "CA") == 0 ) || ( GXutil.strcmp(AV12lccbCC, "DC") == 0 ) )
            {
               AV83LccbSu = "000" + GXutil.substring( A1193lccbS, 6, 5) ;
            }
            else if ( ( GXutil.strcmp(AV12lccbCC, "AX") == 0 ) )
            {
               AV83LccbSu = GXutil.right( A1193lccbS, 8) ;
            }
            else if ( ( GXutil.strcmp(AV12lccbCC, "VI") == 0 ) )
            {
               AV83LccbSu = GXutil.substring( A1193lccbS, 3, 8) ;
            }
            else if ( ( GXutil.strcmp(AV12lccbCC, "HC") == 0 ) )
            {
               AV83LccbSu = GXutil.right( A1193lccbS, 8) ;
            }
         }
         else
         {
            AV83LccbSu = A1194lccbS ;
         }
         AV36SeqReg = (int)(AV36SeqReg+1) ;
         /* Using cursor P00716 */
         pr_default.execute(4, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1231lccbT = P00716_A1231lccbT[0] ;
            A1232lccbT = P00716_A1232lccbT[0] ;
            AV31lccbTD = A1231lccbT ;
            GXt_char6 = AV97LccbTd ;
            GXv_svchar2[0] = GXt_char6 ;
            new pdigitoverificador(remoteHandle, context).execute( A1231lccbT, GXv_svchar2) ;
            preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
            AV97LccbTd = GXutil.trim( A1231lccbT) + GXt_char6 ;
            AV69lccbTR = A1232lccbT ;
            /* Execute user subroutine: S126 */
            S126 ();
            if ( returnInSub )
            {
               pr_default.close(4);
               pr_default.close(3);
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               if (true) return;
            }
            if ( ( GXutil.strcmp(AV67PER_NA, "00000") != 0 ) )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( ( GXutil.strcmp(AV107Utili, "S") == 0 ) )
         {
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "6", (short)(1)) ;
            GXt_char6 = AV43SeqReg ;
            GXv_int7[0] = AV36SeqReg ;
            GXv_int9[0] = (byte)(7) ;
            GXv_int8[0] = (byte)(0) ;
            GXv_svchar2[0] = GXt_char6 ;
            new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_svchar2) ;
            preti1012rpt_v002.this.AV36SeqReg = (int)((int)(GXv_int7[0])) ;
            preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
            AV43SeqReg = GXt_char6 ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV43SeqReg, (short)(7)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV10lccbEm, (short)(3)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV19lccbIA, (short)(7)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "0000000000", (short)(10)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "0000000", (short)(7)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "BRL", (short)(3)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV128lccbF, (short)(11)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00000000000", (short)(11)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "        ", (short)(8)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV131lccbt, (short)(11)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV31lccbTD, (short)(10)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "0000", (short)(4)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "Y", (short)(3)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV83LccbSu, 1, 8), (short)(8)) ;
            AV98DiaVen = (byte)(GXutil.day( A1223lccbD)) ;
            AV99MesVen = (byte)(GXutil.month( A1223lccbD)) ;
            AV100AnoVe = (short)(GXutil.year( A1223lccbD)) ;
            AV73sTemp = localUtil.format( AV100AnoVe, "9999") + localUtil.format( AV99MesVen, "99") + localUtil.format( AV98DiaVen, "99") ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(8)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV136LccbC, (short)(2)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV129lccbS, (short)(0)) ;
            AV73sTemp = GXutil.padl( GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( AV15lccbDo, 14, 2)), ".", ""), ",", ""), (short)(11), "0") ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(11)) ;
            GXt_char6 = AV73sTemp ;
            GXv_int7[0] = AV21lccbIn ;
            GXv_int9[0] = (byte)(2) ;
            GXv_int8[0] = (byte)(0) ;
            GXv_svchar2[0] = GXt_char6 ;
            new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_svchar2) ;
            preti1012rpt_v002.this.AV21lccbIn = (short)((short)(GXv_int7[0])) ;
            preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
            AV73sTemp = GXt_char6 ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(2)) ;
            AV73sTemp = GXutil.padl( GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( AV23lccbIn, 14, 2)), ".", ""), ",", ""), (short)(11), "0") ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(11)) ;
            AV94Num03P = (short)(AV62lccbRS) ;
            AV73sTemp = localUtil.format( AV94Num03P, "999") ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(3)) ;
            AV106lccbR = GXutil.substring( GXutil.trim( A1202lccbR), 1, 40) ;
            GXt_char6 = AV73sTemp ;
            GXv_svchar2[0] = AV106lccbR ;
            GXv_char10[0] = " " ;
            GXv_int11[0] = (short)(40) ;
            GXv_char12[0] = "R" ;
            GXv_char13[0] = GXt_char6 ;
            new pr2strset(remoteHandle, context).execute( GXv_svchar2, GXv_char10, GXv_int11, GXv_char12, GXv_char13) ;
            preti1012rpt_v002.this.AV106lccbR = GXv_svchar2[0] ;
            preti1012rpt_v002.this.GXt_char6 = GXv_char13[0] ;
            AV73sTemp = GXt_char6 ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(40)) ;
            GXt_char6 = AV73sTemp ;
            GXv_char13[0] = AV13lccbCC ;
            GXv_char12[0] = " " ;
            GXv_int11[0] = (short)(20) ;
            GXv_char10[0] = "R" ;
            GXv_svchar2[0] = GXt_char6 ;
            new pr2strset(remoteHandle, context).execute( GXv_char13, GXv_char12, GXv_int11, GXv_char10, GXv_svchar2) ;
            preti1012rpt_v002.this.AV13lccbCC = GXv_char13[0] ;
            preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
            AV73sTemp = GXt_char6 ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(20)) ;
            AV101Txt1 = GXutil.padr( GXutil.trim( AV11lccbAp), (short)(6), " ") ;
            AV102Tam = (short)(GXutil.len( AV101Txt1)) ;
            if ( ( AV102Tam < 6 ) )
            {
               AV103DifTa = (short)(6-AV102Tam) ;
               AV73sTemp = AV101Txt1 + GXutil.space( AV103DifTa) ;
            }
            else
            {
               AV73sTemp = AV101Txt1 ;
            }
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(6)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.right( GXutil.trim( A1163lccbP), 6), (short)(6)) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "DB", (short)(2)) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV124Distr, 1, 1), "C") == 0 ) )
            {
               AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "01", (short)(2)) ;
               AV125Proce = "01" ;
               AV123Proce = "Cielo" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( AV124Distr, 1, 1), "R") == 0 ) )
            {
               AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "02", (short)(2)) ;
               AV125Proce = "02" ;
               AV123Proce = "Rede" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( AV124Distr, 1, 1), "A") == 0 ) )
            {
               AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "03", (short)(2)) ;
               AV125Proce = "03" ;
               AV123Proce = "Amex" ;
            }
            else
            {
               AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00", (short)(2)) ;
               AV125Proce = "00" ;
               AV123Proce = "" ;
            }
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(12)), (short)(12)) ;
            AV55Traile = (int)(AV36SeqReg+2) ;
            AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         }
         else
         {
            GXt_char6 = AV43SeqReg ;
            GXv_int7[0] = AV36SeqReg ;
            GXv_int9[0] = (byte)(7) ;
            GXv_int8[0] = (byte)(0) ;
            GXv_char13[0] = GXt_char6 ;
            new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_char13) ;
            preti1012rpt_v002.this.AV36SeqReg = (int)((int)(GXv_int7[0])) ;
            preti1012rpt_v002.this.GXt_char6 = GXv_char13[0] ;
            AV43SeqReg = GXt_char6 ;
            AV110Valor = AV128lccbF ;
            AV111Valor = AV131lccbt ;
            AV98DiaVen = (byte)(GXutil.day( A1223lccbD)) ;
            AV99MesVen = (byte)(GXutil.month( A1223lccbD)) ;
            AV100AnoVe = (short)(GXutil.year( A1223lccbD)) ;
            AV112DataT = localUtil.format( AV100AnoVe, "9999") + localUtil.format( AV99MesVen, "99") + localUtil.format( AV98DiaVen, "99") ;
            AV113Valor = AV129lccbS ;
            AV114Valor = GXutil.padl( GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( AV15lccbDo, 14, 2)), ".", ""), ",", ""), (short)(11), "0") ;
            GXt_char6 = AV116NumPa ;
            GXv_int7[0] = AV21lccbIn ;
            GXv_int9[0] = (byte)(2) ;
            GXv_int8[0] = (byte)(0) ;
            GXv_char13[0] = GXt_char6 ;
            new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_char13) ;
            preti1012rpt_v002.this.AV21lccbIn = (short)((short)(GXv_int7[0])) ;
            preti1012rpt_v002.this.GXt_char6 = GXv_char13[0] ;
            AV116NumPa = GXt_char6 ;
            AV115Valor = GXutil.padl( GXutil.strReplace( GXutil.strReplace( GXutil.trim( GXutil.str( AV23lccbIn, 14, 2)), ".", ""), ",", ""), (short)(11), "0") ;
            AV106lccbR = GXutil.substring( GXutil.trim( A1202lccbR), 1, 40) ;
            GXt_char6 = AV117DescE ;
            GXv_char13[0] = AV106lccbR ;
            GXv_char12[0] = " " ;
            GXv_int11[0] = (short)(40) ;
            GXv_char10[0] = "R" ;
            GXv_svchar2[0] = GXt_char6 ;
            new pr2strset(remoteHandle, context).execute( GXv_char13, GXv_char12, GXv_int11, GXv_char10, GXv_svchar2) ;
            preti1012rpt_v002.this.AV106lccbR = GXv_char13[0] ;
            preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
            AV117DescE = GXt_char6 ;
            GXt_char6 = AV118Carta ;
            GXv_char13[0] = AV13lccbCC ;
            GXv_char12[0] = " " ;
            GXv_int11[0] = (short)(20) ;
            GXv_char10[0] = "R" ;
            GXv_svchar2[0] = GXt_char6 ;
            new pr2strset(remoteHandle, context).execute( GXv_char13, GXv_char12, GXv_int11, GXv_char10, GXv_svchar2) ;
            preti1012rpt_v002.this.AV13lccbCC = GXv_char13[0] ;
            preti1012rpt_v002.this.GXt_char6 = GXv_svchar2[0] ;
            AV118Carta = GXt_char6 ;
            AV94Num03P = (short)(AV62lccbRS) ;
            AV101Txt1 = GXutil.trim( GXutil.substring( AV11lccbAp, 1, 6)) ;
            AV102Tam = (short)(GXutil.len( AV101Txt1)) ;
            if ( ( AV102Tam < 6 ) )
            {
               AV103DifTa = (short)(6-AV102Tam) ;
               AV119Auth = AV101Txt1 + GXutil.space( AV103DifTa) ;
            }
            else
            {
               AV119Auth = AV101Txt1 ;
            }
            if ( ( GXutil.strcmp(GXutil.substring( AV124Distr, 1, 1), "C") == 0 ) )
            {
               AV125Proce = "01" ;
               AV123Proce = "Cielo" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( AV124Distr, 1, 1), "R") == 0 ) )
            {
               AV125Proce = "02" ;
               AV123Proce = "Rede" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( AV124Distr, 1, 1), "A") == 0 ) )
            {
               AV125Proce = "03" ;
               AV123Proce = "Amex" ;
            }
            else
            {
               AV125Proce = "00" ;
               AV123Proce = "" ;
            }
            AV109Linha = "6" + AV43SeqReg + AV10lccbEm + AV19lccbIA + "0000000000" + "0000000" + "BRL" + GXutil.trim( AV128lccbF) + "00000000000" + GXutil.space( (short)(8)) + GXutil.padl( GXutil.trim( AV111Valor), (short)(11), "0") + AV31lccbTD + "0000" + "Y  " + GXutil.padl( GXutil.substring( AV83LccbSu, 1, 8), (short)(8), " ") + AV112DataT + AV136LccbC + AV129lccbS + GXutil.padl( GXutil.trim( AV114Valor), (short)(11), "0") + GXutil.padl( GXutil.trim( AV116NumPa), (short)(2), "0") + GXutil.padl( GXutil.trim( AV115Valor), (short)(11), "0") + localUtil.format( AV94Num03P, "999") + GXutil.padr( GXutil.trim( AV117DescE), (short)(40), " ") + GXutil.padr( GXutil.trim( AV118Carta), (short)(20), " ") + AV119Auth + "      " + "DB" + AV125Proce + GXutil.space( (short)(12)) ;
            AV109Linha = AV109Linha + GXutil.newLine( ) ;
            AV108XML10.writeRawText(AV109Linha);
         }
         h710( false, 13) ;
         getPrinter().GxAttris("MS Serif", 6, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV13lccbCC, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 200, Gx_line+0, 296, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV12lccbCC, "XX")), 0, Gx_line+0, 13, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV88LccbSa, "ZZ,ZZZ,ZZZ,ZZ9.99")), 517, Gx_line+0, 597, Gx_line+11, 2) ;
         getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV21lccbIn, "ZZZ9")), 613, Gx_line+0, 632, Gx_line+11, 2) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV83LccbSu, "XXXXXXXXXX")), 87, Gx_line+0, 131, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1202lccbR, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 685, Gx_line+0, 792, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( A1201lccbR, "ZZZZZ9")), 667, Gx_line+0, 692, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV96LccbIa, "XXXXXXXX")), 143, Gx_line+0, 189, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV123Proce, "XXXXXXXXXXXXXXX")), 17, Gx_line+0, 76, Gx_line+10, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV97LccbTd, "XXXXXXXXXXX")), 309, Gx_line+0, 359, Gx_line+11, 0) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV11lccbAp, "XXXXXXXXXXXXXXXXXXXX")), 434, Gx_line+1, 513, Gx_line+11, 0+256) ;
         getPrinter().GxDrawText(localUtil.format( AV14lccbDa, "99/99/99"), 374, Gx_line+0, 409, Gx_line+10, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+13) ;
         if ( ( AV85TestMo == 0 ) )
         {
            A1184lccbS = "I1012" ;
            n1184lccbS = false ;
            /*
               INSERT RECORD ON TABLE LCCBPLP1

            */
            A1229lccbS = GXutil.now(true, false) ;
            A1186lccbS = "I1012" ;
            n1186lccbS = false ;
            A1187lccbS = "Gerado Arquivo de Rejei��o i1012" ;
            n1187lccbS = false ;
            /* Using cursor P00717 */
            pr_default.execute(5, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
            if ( (pr_default.getStatus(5) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
         }
         /* Using cursor P00718 */
         pr_default.execute(6, new Object[] {new Boolean(n1184lccbS), A1184lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         pr_default.readNext(3);
      }
      pr_default.close(3);
      if ( ( AV144GXLvl == 0 ) )
      {
      }
      h710( false, 20) ;
      getPrinter().GxAttris("MS Serif", 6, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
      getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV91TotalA, "ZZ,ZZZ,ZZZ,ZZ9.99")), 513, Gx_line+4, 597, Gx_line+15, 2) ;
      getPrinter().GxAttris("MS Serif", 6, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
      getPrinter().GxDrawText("Totais", 132, Gx_line+5, 160, Gx_line+15, 0+256) ;
      Gx_OldLine = Gx_line ;
      Gx_line = (int)(Gx_line+20) ;
      if ( ( GXutil.strcmp(AV107Utili, "S") == 0 ) )
      {
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( "F", (short)(1)) ;
         GXt_char6 = AV73sTemp ;
         GXv_int7[0] = AV36SeqReg ;
         GXv_int9[0] = (byte)(7) ;
         GXv_int8[0] = (byte)(0) ;
         GXv_char13[0] = GXt_char6 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_char13) ;
         preti1012rpt_v002.this.AV36SeqReg = (int)((int)(GXv_int7[0])) ;
         preti1012rpt_v002.this.GXt_char6 = GXv_char13[0] ;
         AV73sTemp = GXt_char6 ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV73sTemp, (short)(1)) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(232)), (short)(232)) ;
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      else
      {
         GXt_char6 = AV43SeqReg ;
         GXv_int7[0] = AV36SeqReg ;
         GXv_int9[0] = (byte)(7) ;
         GXv_int8[0] = (byte)(0) ;
         GXv_char13[0] = GXt_char6 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int7, GXv_int9, GXv_int8, GXv_char13) ;
         preti1012rpt_v002.this.AV36SeqReg = (int)((int)(GXv_int7[0])) ;
         preti1012rpt_v002.this.GXt_char6 = GXv_char13[0] ;
         AV43SeqReg = GXt_char6 ;
         AV109Linha = "F" + AV43SeqReg + GXutil.space( (short)(232)) ;
         AV109Linha = AV109Linha + GXutil.newLine( ) ;
         AV108XML10.writeRawText(AV109Linha);
      }
      if ( ( GXutil.strcmp(AV107Utili, "S") == 0 ) )
      {
         AV9FileNoO = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      }
      else
      {
         AV108XML10.close();
      }
      GXt_char6 = AV134Proce ;
      GXv_char13[0] = GXt_char6 ;
      new pr2getparm(remoteHandle, context).execute( "PROCESSA_SCHECK", "Verifica se processa o sanity check", "S", "N", GXv_char13) ;
      preti1012rpt_v002.this.GXt_char6 = GXv_char13[0] ;
      AV134Proce = GXt_char6 ;
      if ( ( GXutil.strcmp(AV134Proce, "Y") == 0 ) )
      {
         context.msgStatus( "Sanity Check " );
         GXv_char13[0] = AV135msgEr ;
         new psanitychecki1012_23(remoteHandle, context).execute( AV82PathTx, "i1012", GXv_char13) ;
         preti1012rpt_v002.this.AV135msgEr = GXv_char13[0] ;
         if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV135msgEr))==0)) )
         {
            context.msgStatus( AV135msgEr );
         }
      }
   }

   public void S126( ) throws ProcessInterruptedException
   {
      /* 'DADOS HOT' Routine */
      AV84NUM_BI = (long)(GXutil.val( AV31lccbTD, ".")) ;
      AV146GXLvl = (byte)(0) ;
      /* Using cursor P00719 */
      pr_default.execute(7, new Object[] {new Long(AV84NUM_BI), AV69lccbTR});
      while ( (pr_default.getStatus(7) != 101) )
      {
         A966CODE = P00719_A966CODE[0] ;
         A902NUM_BI = P00719_A902NUM_BI[0] ;
         n902NUM_BI = P00719_n902NUM_BI[0] ;
         A965PER_NA = P00719_A965PER_NA[0] ;
         A963ISOC = P00719_A963ISOC[0] ;
         A964CiaCod = P00719_A964CiaCod[0] ;
         A967IATA = P00719_A967IATA[0] ;
         A968NUM_BI = P00719_A968NUM_BI[0] ;
         A969TIPO_V = P00719_A969TIPO_V[0] ;
         A970DATA = P00719_A970DATA[0] ;
         AV146GXLvl = (byte)(1) ;
         AV67PER_NA = A965PER_NA ;
         pr_default.readNext(7);
      }
      pr_default.close(7);
      if ( ( AV146GXLvl == 0 ) )
      {
         AV67PER_NA = "00000" ;
      }
   }

   public void h710( boolean bFoot ,
                     int Inc )
   {
      /* Skip the required number of lines */
      while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
      {
         if ( ( Gx_line + Inc >= P_lines ) )
         {
            if ( ( Gx_page > 0 ) )
            {
               /* Print footers */
               Gx_line = P_lines ;
               if ( ! bFoot )
               {
                  getPrinter().GxEndPage() ;
               }
               if ( bFoot )
               {
                  return  ;
               }
            }
            ToSkip = 0 ;
            Gx_line = 0 ;
            Gx_page = (int)(Gx_page+1) ;
            /* Skip Margin Top Lines */
            Gx_line = (int)(Gx_line+(M_top*lineHeight)) ;
            /* Print headers */
            getPrinter().GxStartPage() ;
            getPrinter().setPage(Gx_page);
            getPrinter().GxDrawLine(0, Gx_line+77, 790, Gx_line+77, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(2, Gx_line+99, 792, Gx_line+99, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(localUtil.format( Gx_date, "99/99/99"), 727, Gx_line+27, 768, Gx_line+41, 0+256) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( Gx_page, "ZZZZZ9")), 727, Gx_line+7, 762, Gx_line+21, 2+256) ;
            getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("P�GINA:", 668, Gx_line+7, 711, Gx_line+21, 0+256) ;
            getPrinter().GxDrawText("DATA:", 668, Gx_line+25, 698, Gx_line+39, 0+256) ;
            getPrinter().GxAttris("Arial", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("EMPRESA A�REA:", 19, Gx_line+58, 118, Gx_line+73, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("TIESS TAM", 19, Gx_line+11, 87, Gx_line+27, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV10lccbEm, "@!")), 129, Gx_line+57, 167, Gx_line+73, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV86EmpNom, "@!")), 192, Gx_line+57, 371, Gx_line+73, 0+256) ;
            getPrinter().GxAttris("MS Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("RELAT�RIO DE VENDAS A CR�DITO REJEITADAS", 178, Gx_line+19, 605, Gx_line+39, 0+256) ;
            getPrinter().GxAttris("MS Serif", 6, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("i1011", 19, Gx_line+32, 40, Gx_line+42, 0+256) ;
            getPrinter().GxDrawText("N� Cart�o", 200, Gx_line+86, 245, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Tp", 0, Gx_line+86, 11, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Valor CC", 554, Gx_line+86, 597, Gx_line+96, 2+256) ;
            getPrinter().GxDrawText("Par(s)", 611, Gx_line+86, 638, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Erro", 659, Gx_line+86, 679, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Descri��o do erro", 685, Gx_line+86, 767, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("N� Ro", 87, Gx_line+86, 113, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Iata", 143, Gx_line+86, 161, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Bilhete", 309, Gx_line+86, 341, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Processadora", 17, Gx_line+86, 78, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Emiss�o", 374, Gx_line+86, 411, Gx_line+96, 0+256) ;
            getPrinter().GxDrawText("Autoriza��o", 434, Gx_line+86, 490, Gx_line+96, 0+256) ;
            Gx_OldLine = Gx_line ;
            Gx_line = (int)(Gx_line+103) ;
            if (true) break;
         }
         else
         {
            PrtOffset = 0 ;
            Gx_line = (int)(Gx_line+1) ;
         }
         ToSkip = (int)(ToSkip-1) ;
      }
      getPrinter().setPage(Gx_page);
   }

   public void add_metrics( )
   {
      add_metrics0( ) ;
      add_metrics1( ) ;
      add_metrics2( ) ;
      add_metrics3( ) ;
      add_metrics4( ) ;
   }

   public void add_metrics0( )
   {
      getPrinter().setMetrics("MS Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 36, 48, 14, 36, 21, 64, 36, 36, 21, 64, 43, 21, 64, 48, 39, 48, 48, 14, 14, 21, 21, 22, 36, 64, 20, 64, 32, 21, 60, 48, 31, 43, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
   }

   public void add_metrics1( )
   {
      getPrinter().setMetrics("MS Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics2( )
   {
      getPrinter().setMetrics("Arial", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 36, 48, 14, 36, 21, 64, 36, 36, 21, 64, 43, 21, 64, 48, 39, 48, 48, 14, 14, 21, 21, 22, 36, 64, 20, 64, 32, 21, 60, 48, 31, 43, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
   }

   public void add_metrics3( )
   {
      getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics4( )
   {
      getPrinter().setMetrics("MS Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   protected int getOutputType( )
   {
      return OUTPUT_PDF;
   }

   protected void cleanup( )
   {
      this.aP0[0] = preti1012rpt_v002.this.AV10lccbEm;
      this.aP1[0] = preti1012rpt_v002.this.AV86EmpNom;
      this.aP2[0] = preti1012rpt_v002.this.AV80Pathpd;
      this.aP3[0] = preti1012rpt_v002.this.AV82PathTx;
      this.aP4[0] = preti1012rpt_v002.this.AV44DataB;
      this.aP5[0] = preti1012rpt_v002.this.AV41DebugM;
      Application.commit(context, remoteHandle, "DEFAULT", "preti1012rpt_v002");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public String getlccbRSubErrorDSC0( int E1201lccbR ,
                                       String E1224lccbC )
   {
      X1236ICSI_ = "ERRO DESCONHECIDO" ;
      nX1236ICSI = false ;
      Gx_first = true ;
      /* Using cursor P007110 */
      pr_default.execute(8, new Object[] {E1224lccbC, new Boolean(nA1201lccb), new Integer(E1201lccbR)});
      while ( (pr_default.getStatus(8) != 101) )
      {
         if ( ( E1201lccbR == P007110_A1238ICSI_[0] ) && ( GXutil.strcmp(E1224lccbC, P007110_A1237ICSI_[0]) == 0 ) )
         {
            X1236ICSI_ = P007110_A1236ICSI_[0] ;
            nX1236ICSI = false ;
            if (true) break;
         }
         pr_default.readNext(8);
      }
      pr_default.close(8);
      return X1236ICSI_ ;
   }

   public void initialize( )
   {
      M_top = 0 ;
      M_bot = 0 ;
      Gx_line = 0 ;
      ToSkip = 0 ;
      PrtOffset = 0 ;
      AV107Utili = "" ;
      AV132Bande = "" ;
      AV133NovaB = "" ;
      AV137Bande = "" ;
      AV138NovaB = "" ;
      AV54Versao = "" ;
      GXt_char1 = "" ;
      GXt_char3 = "" ;
      GXt_char4 = "" ;
      GXt_char5 = "" ;
      AV85TestMo = (byte)(0) ;
      AV53Sep = "" ;
      AV45HoraA = "" ;
      AV46HoraB = "" ;
      AV143GXLvl = (byte)(0) ;
      scmdbuf = "" ;
      P00712_A1488ICSI_ = new String[] {""} ;
      P00712_A1487ICSI_ = new String[] {""} ;
      P00712_A1479ICSI_ = new int[1] ;
      P00712_n1479ICSI_ = new boolean[] {false} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      A1479ICSI_ = 0 ;
      n1479ICSI_ = false ;
      AV49ICSI_C = 0 ;
      GX_INS273 = 0 ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      returnInSub = false ;
      AV36SeqReg = 0 ;
      AV9FileNoO = 0 ;
      AV108XML10 = new com.genexus.xml.XMLWriter();
      AV74iPos = 0 ;
      AV73sTemp = "" ;
      AV77Ano1 = (short)(0) ;
      AV78Mes1 = (byte)(0) ;
      AV79Dia1 = (byte)(0) ;
      AV76OneDay = GXutil.nullDate() ;
      Gx_date = GXutil.nullDate() ;
      AV104Num04 = (short)(0) ;
      AV105Txt = "" ;
      AV109Linha = "" ;
      AV90Inic = "" ;
      AV144GXLvl = (byte)(0) ;
      A1196lccbR = GXutil.nullDate() ;
      A1227lccbO = "" ;
      A1184lccbS = "" ;
      A1150lccbE = "" ;
      P00715_A1228lccbF = new String[] {""} ;
      P00715_A1227lccbO = new String[] {""} ;
      P00715_A1226lccbA = new String[] {""} ;
      P00715_A1225lccbC = new String[] {""} ;
      P00715_A1224lccbC = new String[] {""} ;
      P00715_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00715_A1222lccbI = new String[] {""} ;
      P00715_A1150lccbE = new String[] {""} ;
      P00715_A1184lccbS = new String[] {""} ;
      P00715_n1184lccbS = new boolean[] {false} ;
      P00715_A1196lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P00715_n1196lccbR = new boolean[] {false} ;
      P00715_A1167lccbG = new String[] {""} ;
      P00715_n1167lccbG = new boolean[] {false} ;
      P00715_A1490Distr = new String[] {""} ;
      P00715_n1490Distr = new boolean[] {false} ;
      P00715_A1163lccbP = new String[] {""} ;
      P00715_n1163lccbP = new boolean[] {false} ;
      P00715_A1168lccbI = new short[1] ;
      P00715_n1168lccbI = new boolean[] {false} ;
      P00715_A1170lccbI = new double[1] ;
      P00715_n1170lccbI = new boolean[] {false} ;
      P00715_A1515lccbC = new double[1] ;
      P00715_n1515lccbC = new boolean[] {false} ;
      P00715_A1172lccbS = new double[1] ;
      P00715_n1172lccbS = new boolean[] {false} ;
      P00715_A1171lccbT = new double[1] ;
      P00715_n1171lccbT = new boolean[] {false} ;
      P00715_A1201lccbR = new int[1] ;
      P00715_n1201lccbR = new boolean[] {false} ;
      P00715_A1514lccbF = new double[1] ;
      P00715_n1514lccbF = new boolean[] {false} ;
      P00715_A1194lccbS = new String[] {""} ;
      P00715_n1194lccbS = new boolean[] {false} ;
      P00715_A1193lccbS = new String[] {""} ;
      P00715_n1193lccbS = new boolean[] {false} ;
      P00715_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P00715_n1189lccbS = new boolean[] {false} ;
      A1228lccbF = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      n1184lccbS = false ;
      n1196lccbR = false ;
      A1167lccbG = "" ;
      n1167lccbG = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1163lccbP = "" ;
      n1163lccbP = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1515lccbC = 0 ;
      n1515lccbC = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1201lccbR = 0 ;
      n1201lccbR = false ;
      A1514lccbF = 0 ;
      n1514lccbF = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1189lccbS = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1202lccbR = "" ;
      AV19lccbIA = "" ;
      AV96LccbIa = "" ;
      AV14lccbDa = GXutil.nullDate() ;
      AV12lccbCC = "" ;
      AV136LccbC = "" ;
      AV124Distr = "" ;
      AV120Atrib = "" ;
      AV122Resul = "" ;
      AV13lccbCC = "" ;
      AV11lccbAp = "" ;
      AV26lccbOp = "" ;
      AV130lccbp = "" ;
      AV18lccbFP = "" ;
      AV21lccbIn = (short)(0) ;
      AV23lccbIn = 0 ;
      AV15lccbDo = 0 ;
      AV28lccbSa = 0 ;
      AV129lccbS = "" ;
      AV88LccbSa = 0 ;
      AV33lccbTi = 0 ;
      AV131lccbt = "" ;
      AV62lccbRS = 0 ;
      AV91TotalA = 0 ;
      AV92TotLcc = 0 ;
      AV93TotLcc = 0 ;
      AV127lccbF = 0 ;
      AV128lccbF = "" ;
      AV83LccbSu = "" ;
      P00716_A1150lccbE = new String[] {""} ;
      P00716_A1222lccbI = new String[] {""} ;
      P00716_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00716_A1224lccbC = new String[] {""} ;
      P00716_A1225lccbC = new String[] {""} ;
      P00716_A1226lccbA = new String[] {""} ;
      P00716_A1227lccbO = new String[] {""} ;
      P00716_A1228lccbF = new String[] {""} ;
      P00716_A1231lccbT = new String[] {""} ;
      P00716_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1232lccbT = "" ;
      AV31lccbTD = "" ;
      AV97LccbTd = "" ;
      AV69lccbTR = "" ;
      AV67PER_NA = "" ;
      AV43SeqReg = "" ;
      AV98DiaVen = (byte)(0) ;
      AV99MesVen = (byte)(0) ;
      AV100AnoVe = (short)(0) ;
      AV94Num03P = (short)(0) ;
      AV106lccbR = "" ;
      AV101Txt1 = "" ;
      AV102Tam = (short)(0) ;
      AV103DifTa = (short)(0) ;
      AV125Proce = "" ;
      AV123Proce = "" ;
      AV55Traile = 0 ;
      AV110Valor = "" ;
      AV111Valor = "" ;
      AV112DataT = "" ;
      AV113Valor = "" ;
      AV114Valor = "" ;
      AV116NumPa = "" ;
      AV115Valor = "" ;
      AV117DescE = "" ;
      AV118Carta = "" ;
      GXv_char12 = new String [1] ;
      GXv_int11 = new short [1] ;
      GXv_char10 = new String [1] ;
      GXv_svchar2 = new String [1] ;
      AV119Auth = "" ;
      Gx_OldLine = 0 ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      GXv_int7 = new double [1] ;
      GXv_int9 = new byte [1] ;
      GXv_int8 = new byte [1] ;
      AV134Proce = "" ;
      GXt_char6 = "" ;
      AV135msgEr = "" ;
      GXv_char13 = new String [1] ;
      AV84NUM_BI = 0 ;
      AV146GXLvl = (byte)(0) ;
      P00719_A966CODE = new String[] {""} ;
      P00719_A902NUM_BI = new long[1] ;
      P00719_n902NUM_BI = new boolean[] {false} ;
      P00719_A965PER_NA = new String[] {""} ;
      P00719_A963ISOC = new String[] {""} ;
      P00719_A964CiaCod = new String[] {""} ;
      P00719_A967IATA = new String[] {""} ;
      P00719_A968NUM_BI = new String[] {""} ;
      P00719_A969TIPO_V = new String[] {""} ;
      P00719_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      A966CODE = "" ;
      A902NUM_BI = 0 ;
      n902NUM_BI = false ;
      A965PER_NA = "" ;
      A963ISOC = "" ;
      A964CiaCod = "" ;
      A967IATA = "" ;
      A968NUM_BI = "" ;
      A969TIPO_V = "" ;
      A970DATA = GXutil.nullDate() ;
      X1236ICSI_ = "" ;
      nX1236ICSI = false ;
      Gx_first = false ;
      nA1201lccb = false ;
      E1224lccbC = "" ;
      E1201lccbR = 0 ;
      P007110_A1237ICSI_ = new String[] {""} ;
      P007110_A1238ICSI_ = new int[1] ;
      P007110_A1236ICSI_ = new String[] {""} ;
      P007110_n1236ICSI_ = new boolean[] {false} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new preti1012rpt_v002__default(),
         new Object[] {
             new Object[] {
            P00712_A1488ICSI_, P00712_A1487ICSI_, P00712_A1479ICSI_, P00712_n1479ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P00715_A1228lccbF, P00715_A1227lccbO, P00715_A1226lccbA, P00715_A1225lccbC, P00715_A1224lccbC, P00715_A1223lccbD, P00715_A1222lccbI, P00715_A1150lccbE, P00715_A1184lccbS, P00715_n1184lccbS,
            P00715_A1196lccbR, P00715_n1196lccbR, P00715_A1167lccbG, P00715_n1167lccbG, P00715_A1490Distr, P00715_n1490Distr, P00715_A1163lccbP, P00715_n1163lccbP, P00715_A1168lccbI, P00715_n1168lccbI,
            P00715_A1170lccbI, P00715_n1170lccbI, P00715_A1515lccbC, P00715_n1515lccbC, P00715_A1172lccbS, P00715_n1172lccbS, P00715_A1171lccbT, P00715_n1171lccbT, P00715_A1201lccbR, P00715_n1201lccbR,
            P00715_A1514lccbF, P00715_n1514lccbF, P00715_A1194lccbS, P00715_n1194lccbS, P00715_A1193lccbS, P00715_n1193lccbS, P00715_A1189lccbS, P00715_n1189lccbS
            }
            , new Object[] {
            P00716_A1150lccbE, P00716_A1222lccbI, P00716_A1223lccbD, P00716_A1224lccbC, P00716_A1225lccbC, P00716_A1226lccbA, P00716_A1227lccbO, P00716_A1228lccbF, P00716_A1231lccbT, P00716_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P00719_A966CODE, P00719_A902NUM_BI, P00719_n902NUM_BI, P00719_A965PER_NA, P00719_A963ISOC, P00719_A964CiaCod, P00719_A967IATA, P00719_A968NUM_BI, P00719_A969TIPO_V, P00719_A970DATA
            }
            , new Object[] {
            P007110_A1237ICSI_, P007110_A1238ICSI_, P007110_A1236ICSI_, P007110_n1236ICSI_
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_line = 0 ;
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV85TestMo ;
   private byte AV143GXLvl ;
   private byte AV78Mes1 ;
   private byte AV79Dia1 ;
   private byte AV144GXLvl ;
   private byte AV98DiaVen ;
   private byte AV99MesVen ;
   private byte GXv_int9[] ;
   private byte GXv_int8[] ;
   private byte AV146GXLvl ;
   private short Gx_err ;
   private short AV77Ano1 ;
   private short AV104Num04 ;
   private short A1168lccbI ;
   private short AV21lccbIn ;
   private short AV100AnoVe ;
   private short AV94Num03P ;
   private short AV102Tam ;
   private short AV103DifTa ;
   private short GXv_int11[] ;
   private short A1230lccbS ;
   private int M_top ;
   private int M_bot ;
   private int Line ;
   private int ToSkip ;
   private int PrtOffset ;
   private int A1479ICSI_ ;
   private int AV49ICSI_C ;
   private int GX_INS273 ;
   private int AV36SeqReg ;
   private int AV74iPos ;
   private int A1201lccbR ;
   private int AV62lccbRS ;
   private int AV55Traile ;
   private int Gx_OldLine ;
   private int GX_INS236 ;
   private int E1201lccbR ;
   private long AV9FileNoO ;
   private long AV84NUM_BI ;
   private long A902NUM_BI ;
   private double A1170lccbI ;
   private double A1515lccbC ;
   private double A1172lccbS ;
   private double A1171lccbT ;
   private double A1514lccbF ;
   private double AV23lccbIn ;
   private double AV15lccbDo ;
   private double AV28lccbSa ;
   private double AV88LccbSa ;
   private double AV33lccbTi ;
   private double AV91TotalA ;
   private double AV92TotLcc ;
   private double AV93TotLcc ;
   private double AV127lccbF ;
   private double GXv_int7[] ;
   private String AV10lccbEm ;
   private String AV86EmpNom ;
   private String AV80Pathpd ;
   private String AV82PathTx ;
   private String AV44DataB ;
   private String AV41DebugM ;
   private String AV107Utili ;
   private String AV54Versao ;
   private String GXt_char1 ;
   private String GXt_char3 ;
   private String GXt_char4 ;
   private String GXt_char5 ;
   private String AV53Sep ;
   private String AV45HoraA ;
   private String AV46HoraB ;
   private String scmdbuf ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String Gx_emsg ;
   private String AV73sTemp ;
   private String AV105Txt ;
   private String AV109Linha ;
   private String AV90Inic ;
   private String A1227lccbO ;
   private String A1184lccbS ;
   private String A1150lccbE ;
   private String A1228lccbF ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1167lccbG ;
   private String A1490Distr ;
   private String A1163lccbP ;
   private String A1194lccbS ;
   private String A1193lccbS ;
   private String A1202lccbR ;
   private String AV19lccbIA ;
   private String AV96LccbIa ;
   private String AV12lccbCC ;
   private String AV136LccbC ;
   private String AV124Distr ;
   private String AV120Atrib ;
   private String AV122Resul ;
   private String AV13lccbCC ;
   private String AV11lccbAp ;
   private String AV26lccbOp ;
   private String AV130lccbp ;
   private String AV18lccbFP ;
   private String AV83LccbSu ;
   private String A1231lccbT ;
   private String A1232lccbT ;
   private String AV31lccbTD ;
   private String AV97LccbTd ;
   private String AV69lccbTR ;
   private String AV67PER_NA ;
   private String AV43SeqReg ;
   private String AV106lccbR ;
   private String AV101Txt1 ;
   private String AV125Proce ;
   private String AV110Valor ;
   private String AV111Valor ;
   private String AV112DataT ;
   private String AV113Valor ;
   private String AV114Valor ;
   private String AV116NumPa ;
   private String AV115Valor ;
   private String AV117DescE ;
   private String AV118Carta ;
   private String GXv_char12[] ;
   private String GXv_char10[] ;
   private String AV119Auth ;
   private String A1186lccbS ;
   private String GXt_char6 ;
   private String GXv_char13[] ;
   private String A966CODE ;
   private String A965PER_NA ;
   private String A963ISOC ;
   private String A964CiaCod ;
   private String A967IATA ;
   private String A968NUM_BI ;
   private String A969TIPO_V ;
   private String X1236ICSI_ ;
   private String E1224lccbC ;
   private java.util.Date A1229lccbS ;
   private java.util.Date AV76OneDay ;
   private java.util.Date Gx_date ;
   private java.util.Date A1196lccbR ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1189lccbS ;
   private java.util.Date AV14lccbDa ;
   private java.util.Date A970DATA ;
   private boolean n1479ICSI_ ;
   private boolean returnInSub ;
   private boolean n1184lccbS ;
   private boolean n1196lccbR ;
   private boolean n1167lccbG ;
   private boolean n1490Distr ;
   private boolean n1163lccbP ;
   private boolean n1168lccbI ;
   private boolean n1170lccbI ;
   private boolean n1515lccbC ;
   private boolean n1172lccbS ;
   private boolean n1171lccbT ;
   private boolean n1201lccbR ;
   private boolean n1514lccbF ;
   private boolean n1194lccbS ;
   private boolean n1193lccbS ;
   private boolean n1189lccbS ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n902NUM_BI ;
   private boolean nX1236ICSI ;
   private boolean Gx_first ;
   private boolean nA1201lccb ;
   private String AV132Bande ;
   private String AV133NovaB ;
   private String AV137Bande ;
   private String AV138NovaB ;
   private String AV129lccbS ;
   private String AV131lccbt ;
   private String AV128lccbF ;
   private String AV123Proce ;
   private String GXv_svchar2[] ;
   private String A1187lccbS ;
   private String AV134Proce ;
   private String AV135msgEr ;
   private com.genexus.xml.XMLWriter AV108XML10 ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
   private String[] aP5 ;
   private IDataStoreProvider pr_default ;
   private String[] P00712_A1488ICSI_ ;
   private String[] P00712_A1487ICSI_ ;
   private int[] P00712_A1479ICSI_ ;
   private boolean[] P00712_n1479ICSI_ ;
   private String[] P00715_A1228lccbF ;
   private String[] P00715_A1227lccbO ;
   private String[] P00715_A1226lccbA ;
   private String[] P00715_A1225lccbC ;
   private String[] P00715_A1224lccbC ;
   private java.util.Date[] P00715_A1223lccbD ;
   private String[] P00715_A1222lccbI ;
   private String[] P00715_A1150lccbE ;
   private String[] P00715_A1184lccbS ;
   private boolean[] P00715_n1184lccbS ;
   private java.util.Date[] P00715_A1196lccbR ;
   private boolean[] P00715_n1196lccbR ;
   private String[] P00715_A1167lccbG ;
   private boolean[] P00715_n1167lccbG ;
   private String[] P00715_A1490Distr ;
   private boolean[] P00715_n1490Distr ;
   private String[] P00715_A1163lccbP ;
   private boolean[] P00715_n1163lccbP ;
   private short[] P00715_A1168lccbI ;
   private boolean[] P00715_n1168lccbI ;
   private double[] P00715_A1170lccbI ;
   private boolean[] P00715_n1170lccbI ;
   private double[] P00715_A1515lccbC ;
   private boolean[] P00715_n1515lccbC ;
   private double[] P00715_A1172lccbS ;
   private boolean[] P00715_n1172lccbS ;
   private double[] P00715_A1171lccbT ;
   private boolean[] P00715_n1171lccbT ;
   private int[] P00715_A1201lccbR ;
   private boolean[] P00715_n1201lccbR ;
   private double[] P00715_A1514lccbF ;
   private boolean[] P00715_n1514lccbF ;
   private String[] P00715_A1194lccbS ;
   private boolean[] P00715_n1194lccbS ;
   private String[] P00715_A1193lccbS ;
   private boolean[] P00715_n1193lccbS ;
   private java.util.Date[] P00715_A1189lccbS ;
   private boolean[] P00715_n1189lccbS ;
   private String[] P00716_A1150lccbE ;
   private String[] P00716_A1222lccbI ;
   private java.util.Date[] P00716_A1223lccbD ;
   private String[] P00716_A1224lccbC ;
   private String[] P00716_A1225lccbC ;
   private String[] P00716_A1226lccbA ;
   private String[] P00716_A1227lccbO ;
   private String[] P00716_A1228lccbF ;
   private String[] P00716_A1231lccbT ;
   private String[] P00716_A1232lccbT ;
   private String[] P00719_A966CODE ;
   private long[] P00719_A902NUM_BI ;
   private boolean[] P00719_n902NUM_BI ;
   private String[] P00719_A965PER_NA ;
   private String[] P00719_A963ISOC ;
   private String[] P00719_A964CiaCod ;
   private String[] P00719_A967IATA ;
   private String[] P00719_A968NUM_BI ;
   private String[] P00719_A969TIPO_V ;
   private java.util.Date[] P00719_A970DATA ;
   private String[] P007110_A1237ICSI_ ;
   private int[] P007110_A1238ICSI_ ;
   private String[] P007110_A1236ICSI_ ;
   private boolean[] P007110_n1236ICSI_ ;
}

final  class preti1012rpt_v002__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P00715( java.util.Date AV76OneDay ,
                                        java.util.Date A1196lccbR ,
                                        String AV10lccbEm ,
                                        String A1227lccbO ,
                                        String A1184lccbS ,
                                        String A1150lccbE )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate]," ;
      scmdbuf = scmdbuf + " [lccbIATA], [lccbEmpCod], [lccbStatus], [lccbRSubDate], [lccbGDS], [DistribuicaoTransacoesTipo]," ;
      scmdbuf = scmdbuf + " [lccbPlanCode], [lccbInstallments], [lccbInstAmount], [lccbCashAmount], [lccbSaleAmount]," ;
      scmdbuf = scmdbuf + " [lccbTip], [lccbRSubError], [lccbFareAmount], [lccbSubRO], [lccbSubTrn], [lccbSubDate]" ;
      scmdbuf = scmdbuf + " FROM [LCCBPLP] WITH (NOLOCK)" ;
      scmdbuf = scmdbuf + " WHERE ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and ([lccbStatus] = 'RETNOK')" ;
      scmdbuf = scmdbuf + " and ([lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV10lccbEm, "'", "''")) + "')" ;
      scmdbuf = scmdbuf + " and ([lccbOpCode] = 'S' and [lccbStatus] = 'RETNOK' and [lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV10lccbEm, "'", "''")) + "')" ;
      sWhereString = "" ;
      if ( ! (GXutil.nullDate().equals(AV76OneDay)) )
      {
         sWhereString = sWhereString + " and ([lccbRSubDate] = " + (AV76OneDay.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.dtoc( AV76OneDay, 0, "-")+"',102)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY [lccbOpCode], [lccbStatus], [lccbEmpCod], [lccbSubDate], [lccbCCard]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 3 :
                  return conditional_P00715( (java.util.Date)dynConstraints[0] , (java.util.Date)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00712", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCSeqFile] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = ? AND [ICSI_CCCod] = 'I1012') AND ([ICSI_EmpCod] = ? and [ICSI_CCCod] = 'I1012') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P00713", "UPDATE [ICSI_CCINFO] SET [ICSI_CCSeqFile]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P00714", "INSERT INTO [ICSI_CCINFO] ([ICSI_EmpCod], [ICSI_CCCod], [ICSI_CCSeqFile], [ICSI_CCLote], [ICSI_CCPOS1], [ICSI_CCPOS2], [ICSI_CCPOS3], [ICSI_SplitPLP], [ICSI_CCEstab], [ICSI_CCNome]) VALUES (?, ?, ?, convert(int, 0), '', '', '', convert(int, 0), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00715", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00716", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P00717", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P00718", "UPDATE [LCCBPLP] SET [lccbStatus]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00719", "SELECT [CODE], [NUM_BIL2], [PER_NAME], [ISOC], [CiaCod], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([CODE] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007110", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((int[]) buf[2])[0] = rslt.getInt(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 8) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 4) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 4) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 20) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((short[]) buf[18])[0] = rslt.getShort(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((double[]) buf[24])[0] = rslt.getDouble(17) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((double[]) buf[26])[0] = rslt.getDouble(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((int[]) buf[28])[0] = rslt.getInt(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((double[]) buf[30])[0] = rslt.getDouble(20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 10) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((String[]) buf[34])[0] = rslt.getString(22, 20) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[36])[0] = rslt.getGXDate(23) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((long[]) buf[1])[0] = rslt.getLong(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 20) ;
               ((java.util.Date[]) buf[9])[0] = rslt.getGXDate(9) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 3);
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 10);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(3, ((Number) parms[3]).intValue());
               }
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               break;
            case 7 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 4);
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(2, ((Number) parms[2]).intValue());
               }
               break;
      }
   }

}


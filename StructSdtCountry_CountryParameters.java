
public final  class StructSdtCountry_CountryParameters implements Cloneable, java.io.Serializable
{
   public StructSdtCountry_CountryParameters( )
   {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      cal.set(1, 0, 1, 0, 0, 0);
      cal.set(java.util.Calendar.MILLISECOND, 0);
      gxTv_SdtCountry_CountryParameters_Isoconfigid = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigdes = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue = "" ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin = "" ;
      gxTv_SdtCountry_CountryParameters_Dateupd = cal.getTime() ;
      gxTv_SdtCountry_CountryParameters_Mode = "" ;
      gxTv_SdtCountry_CountryParameters_Modified = (short)(0) ;
      gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Dateupd_Z = cal.getTime() ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIsoconfigid( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigid ;
   }

   public void setIsoconfigid( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid = value ;
      return  ;
   }

   public String getIsoconfigdes( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigdes ;
   }

   public void setIsoconfigdes( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigdes = value ;
      return  ;
   }

   public String getIsoconfigvalue( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigvalue ;
   }

   public void setIsoconfigvalue( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue = value ;
      return  ;
   }

   public String getLgnlogin( )
   {
      return gxTv_SdtCountry_CountryParameters_Lgnlogin ;
   }

   public void setLgnlogin( String value )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin = value ;
      return  ;
   }

   public java.util.Date getDateupd( )
   {
      return gxTv_SdtCountry_CountryParameters_Dateupd ;
   }

   public void setDateupd( java.util.Date value )
   {
      gxTv_SdtCountry_CountryParameters_Dateupd = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtCountry_CountryParameters_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCountry_CountryParameters_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtCountry_CountryParameters_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtCountry_CountryParameters_Modified = value ;
      return  ;
   }

   public String getIsoconfigid_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigid_Z ;
   }

   public void setIsoconfigid_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = value ;
      return  ;
   }

   public String getIsoconfigdes_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z ;
   }

   public void setIsoconfigdes_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = value ;
      return  ;
   }

   public String getIsoconfigvalue_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z ;
   }

   public void setIsoconfigvalue_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = value ;
      return  ;
   }

   public String getLgnlogin_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Lgnlogin_Z ;
   }

   public void setLgnlogin_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = value ;
      return  ;
   }

   public java.util.Date getDateupd_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Dateupd_Z ;
   }

   public void setDateupd_Z( java.util.Date value )
   {
      gxTv_SdtCountry_CountryParameters_Dateupd_Z = value ;
      return  ;
   }

   public byte getLgnlogin_N( )
   {
      return gxTv_SdtCountry_CountryParameters_Lgnlogin_N ;
   }

   public void setLgnlogin_N( byte value )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = value ;
      return  ;
   }

   protected byte gxTv_SdtCountry_CountryParameters_Lgnlogin_N ;
   protected short gxTv_SdtCountry_CountryParameters_Modified ;
   protected String gxTv_SdtCountry_CountryParameters_Mode ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigid ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigdes ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigvalue ;
   protected String gxTv_SdtCountry_CountryParameters_Lgnlogin ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigid_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Lgnlogin_Z ;
   protected java.util.Date gxTv_SdtCountry_CountryParameters_Dateupd ;
   protected java.util.Date gxTv_SdtCountry_CountryParameters_Dateupd_Z ;
}


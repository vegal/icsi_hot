/*
               File: Debug
        Description: Debug
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 16:12:48.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pdebug extends GXProcedure
{
   public pdebug( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pdebug.class ), "" );
   }

   public pdebug( int remoteHandle ,
                  ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String aP2 )
   {
      pdebug.this.AV8LinhaDe = aP0;
      pdebug.this.AV10Caminh = aP1;
      pdebug.this.AV9NomeArq = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV12Caminh = "C:\\temp\\" + GXutil.trim( AV9NomeArq) ;
      AV11RetVal = context.getSessionInstances().getDelimitedFiles().dfwopen( AV12Caminh, "", "", (byte)(1), "") ;
      AV11RetVal = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV8LinhaDe, (short)(0)) ;
      AV11RetVal = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      AV11RetVal = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV12Caminh = "" ;
      AV11RetVal = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private double AV11RetVal ;
   private String AV8LinhaDe ;
   private String AV10Caminh ;
   private String AV9NomeArq ;
   private String AV12Caminh ;
}


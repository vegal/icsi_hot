/*
               File: ManipulaHOT10
        Description: Procedimento para leitura ou grava��o da HOT10
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: March 27, 2014 11:43:43.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pmanipulahot10 extends GXProcedure
{
   public pmanipulahot10( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pmanipulahot10.class ), "" );
   }

   public pmanipulahot10( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( SdtSDT_HOT10 aP0 ,
                        String aP1 ,
                        String aP2 ,
                        String aP3 ,
                        String aP4 ,
                        String[] aP5 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5);
   }

   private void execute_int( SdtSDT_HOT10 aP0 ,
                             String aP1 ,
                             String aP2 ,
                             String aP3 ,
                             String aP4 ,
                             String[] aP5 )
   {
      pmanipulahot10.this.AV8Hot = aP0;
      pmanipulahot10.this.AV11H10Rec = aP1;
      pmanipulahot10.this.AV12H10Fie = aP2;
      pmanipulahot10.this.AV13H10Seq = aP3;
      pmanipulahot10.this.AV10Tipo = aP4;
      pmanipulahot10.this.aP5 = aP5;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( GXutil.strcmp(AV10Tipo, "INSERT") == 0 ) )
      {
         /*
            INSERT RECORD ON TABLE HOT10

         */
         A963ISOC = AV8Hot.getgxTv_SdtSDT_HOT10_Isoc() ;
         A964CiaCod = AV8Hot.getgxTv_SdtSDT_HOT10_Ciacod() ;
         A965PER_NA = AV8Hot.getgxTv_SdtSDT_HOT10_Per_name() ;
         A966CODE = AV8Hot.getgxTv_SdtSDT_HOT10_Code() ;
         A967IATA = AV8Hot.getgxTv_SdtSDT_HOT10_Iata() ;
         A968NUM_BI = AV8Hot.getgxTv_SdtSDT_HOT10_Num_bil() ;
         A969TIPO_V = AV8Hot.getgxTv_SdtSDT_HOT10_Tipo_vend() ;
         A970DATA = AV8Hot.getgxTv_SdtSDT_HOT10_Data() ;
         A1508H10Re = AV8Hot.getgxTv_SdtSDT_HOT10_H10recid() ;
         A1509H10Fi = AV8Hot.getgxTv_SdtSDT_HOT10_H10field() ;
         A1510H10Se = AV8Hot.getgxTv_SdtSDT_HOT10_H10seq() ;
         A1511H10Ty = AV8Hot.getgxTv_SdtSDT_HOT10_H10type() ;
         A1512H10Co = AV8Hot.getgxTv_SdtSDT_HOT10_H10content() ;
         /* Using cursor P007J2 */
         pr_default.execute(0, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A1508H10Re, A1509H10Fi, A1510H10Se, A1511H10Ty, A1512H10Co});
         if ( (pr_default.getStatus(0) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         Application.commit(context, remoteHandle, "DEFAULT", "pmanipulahot10");
      }
      else if ( ( GXutil.strcmp(AV10Tipo, "READ") == 0 ) )
      {
         AV17GXLvl2 = (byte)(0) ;
         /* Using cursor P007J3 */
         pr_default.execute(1, new Object[] {AV8Hot.getgxTv_SdtSDT_HOT10_Isoc(), AV8Hot.getgxTv_SdtSDT_HOT10_Ciacod(), AV8Hot.getgxTv_SdtSDT_HOT10_Per_name(), AV8Hot.getgxTv_SdtSDT_HOT10_Code(), AV8Hot.getgxTv_SdtSDT_HOT10_Iata(), AV8Hot.getgxTv_SdtSDT_HOT10_Num_bil(), AV8Hot.getgxTv_SdtSDT_HOT10_Tipo_vend(), AV8Hot.getgxTv_SdtSDT_HOT10_Data(), AV11H10Rec, AV12H10Fie, AV13H10Seq});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1510H10Se = P007J3_A1510H10Se[0] ;
            A1509H10Fi = P007J3_A1509H10Fi[0] ;
            A1508H10Re = P007J3_A1508H10Re[0] ;
            A970DATA = P007J3_A970DATA[0] ;
            A969TIPO_V = P007J3_A969TIPO_V[0] ;
            A968NUM_BI = P007J3_A968NUM_BI[0] ;
            A967IATA = P007J3_A967IATA[0] ;
            A966CODE = P007J3_A966CODE[0] ;
            A965PER_NA = P007J3_A965PER_NA[0] ;
            A964CiaCod = P007J3_A964CiaCod[0] ;
            A963ISOC = P007J3_A963ISOC[0] ;
            A1512H10Co = P007J3_A1512H10Co[0] ;
            AV17GXLvl2 = (byte)(1) ;
            AV9H10Cont = A1512H10Co ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( ( AV17GXLvl2 == 0 ) )
         {
            AV9H10Cont = "" ;
         }
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP5[0] = pmanipulahot10.this.AV9H10Cont;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9H10Cont = "" ;
      GX_INS278 = 0 ;
      A963ISOC = "" ;
      A964CiaCod = "" ;
      A965PER_NA = "" ;
      A966CODE = "" ;
      A967IATA = "" ;
      A968NUM_BI = "" ;
      A969TIPO_V = "" ;
      A970DATA = GXutil.nullDate() ;
      A1508H10Re = "" ;
      A1509H10Fi = "" ;
      A1510H10Se = "" ;
      A1511H10Ty = "" ;
      A1512H10Co = "" ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV17GXLvl2 = (byte)(0) ;
      scmdbuf = "" ;
      P007J3_A1510H10Se = new String[] {""} ;
      P007J3_A1509H10Fi = new String[] {""} ;
      P007J3_A1508H10Re = new String[] {""} ;
      P007J3_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P007J3_A969TIPO_V = new String[] {""} ;
      P007J3_A968NUM_BI = new String[] {""} ;
      P007J3_A967IATA = new String[] {""} ;
      P007J3_A966CODE = new String[] {""} ;
      P007J3_A965PER_NA = new String[] {""} ;
      P007J3_A964CiaCod = new String[] {""} ;
      P007J3_A963ISOC = new String[] {""} ;
      P007J3_A1512H10Co = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pmanipulahot10__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            P007J3_A1510H10Se, P007J3_A1509H10Fi, P007J3_A1508H10Re, P007J3_A970DATA, P007J3_A969TIPO_V, P007J3_A968NUM_BI, P007J3_A967IATA, P007J3_A966CODE, P007J3_A965PER_NA, P007J3_A964CiaCod,
            P007J3_A963ISOC, P007J3_A1512H10Co
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV17GXLvl2 ;
   private short Gx_err ;
   private int GX_INS278 ;
   private String AV11H10Rec ;
   private String AV12H10Fie ;
   private String AV13H10Seq ;
   private String A963ISOC ;
   private String A964CiaCod ;
   private String A965PER_NA ;
   private String A966CODE ;
   private String A967IATA ;
   private String A968NUM_BI ;
   private String A969TIPO_V ;
   private String A1508H10Re ;
   private String A1509H10Fi ;
   private String A1510H10Se ;
   private String A1511H10Ty ;
   private String Gx_emsg ;
   private String scmdbuf ;
   private java.util.Date A970DATA ;
   private String AV10Tipo ;
   private String AV9H10Cont ;
   private String A1512H10Co ;
   private String[] aP5 ;
   private IDataStoreProvider pr_default ;
   private String[] P007J3_A1510H10Se ;
   private String[] P007J3_A1509H10Fi ;
   private String[] P007J3_A1508H10Re ;
   private java.util.Date[] P007J3_A970DATA ;
   private String[] P007J3_A969TIPO_V ;
   private String[] P007J3_A968NUM_BI ;
   private String[] P007J3_A967IATA ;
   private String[] P007J3_A966CODE ;
   private String[] P007J3_A965PER_NA ;
   private String[] P007J3_A964CiaCod ;
   private String[] P007J3_A963ISOC ;
   private String[] P007J3_A1512H10Co ;
   private SdtSDT_HOT10 AV8Hot ;
}

final  class pmanipulahot10__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P007J2", "INSERT INTO [HOT10] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [H10RecID], [H10Field], [H10Seq], [H10Type], [H10Content]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007J3", "SELECT [H10Seq], [H10Field], [H10RecID], [DATA], [TIPO_VEND], [NUM_BIL], [IATA], [CODE], [PER_NAME], [CiaCod], [ISOC], [H10Content] FROM [HOT10] WITH (NOLOCK) WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [H10RecID] = ? and [H10Field] = ? and [H10Seq] = ? ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [H10RecID], [H10Field], [H10Seq] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(11, 2) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(12) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 5);
               stmt.setString(10, (String)parms[9], 20);
               stmt.setString(11, (String)parms[10], 3);
               stmt.setString(12, (String)parms[11], 5);
               stmt.setVarchar(13, (String)parms[12], 1024, false);
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setVarchar(2, (String)parms[1], 20);
               stmt.setVarchar(3, (String)parms[2], 20);
               stmt.setVarchar(4, (String)parms[3], 20);
               stmt.setVarchar(5, (String)parms[4], 20);
               stmt.setVarchar(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 5);
               stmt.setString(10, (String)parms[9], 20);
               stmt.setString(11, (String)parms[10], 3);
               break;
      }
   }

}


/*
               File: AjustaSaleAmount
        Description: Ajusta Sale Amount
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:57.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pajustasaleamount extends GXProcedure
{
   public pajustasaleamount( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pajustasaleamount.class ), "" );
   }

   public pajustasaleamount( int remoteHandle ,
                             ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      /* Using cursor P007V2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1184lccbS = P007V2_A1184lccbS[0] ;
         n1184lccbS = P007V2_n1184lccbS[0] ;
         A1172lccbS = P007V2_A1172lccbS[0] ;
         n1172lccbS = P007V2_n1172lccbS[0] ;
         A1171lccbT = P007V2_A1171lccbT[0] ;
         n1171lccbT = P007V2_n1171lccbT[0] ;
         A1168lccbI = P007V2_A1168lccbI[0] ;
         n1168lccbI = P007V2_n1168lccbI[0] ;
         A1170lccbI = P007V2_A1170lccbI[0] ;
         n1170lccbI = P007V2_n1170lccbI[0] ;
         A1150lccbE = P007V2_A1150lccbE[0] ;
         A1222lccbI = P007V2_A1222lccbI[0] ;
         A1223lccbD = P007V2_A1223lccbD[0] ;
         A1224lccbC = P007V2_A1224lccbC[0] ;
         A1225lccbC = P007V2_A1225lccbC[0] ;
         A1226lccbA = P007V2_A1226lccbA[0] ;
         A1227lccbO = P007V2_A1227lccbO[0] ;
         A1228lccbF = P007V2_A1228lccbF[0] ;
         if ( ( ( A1170lccbI * A1168lccbI + A1171lccbT != A1172lccbS ) ) && ( A1170lccbI > 1 ) )
         {
            A1172lccbS = (double)(A1170lccbI*A1168lccbI+A1171lccbT) ;
            n1172lccbS = false ;
         }
         /* Using cursor P007V3 */
         pr_default.execute(1, new Object[] {new Boolean(n1172lccbS), new Double(A1172lccbS), A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         pr_default.readNext(0);
      }
      pr_default.close(0);
      cleanup();
   }

   protected void cleanup( )
   {
      Application.commit(context, remoteHandle, "DEFAULT", "pajustasaleamount");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      P007V2_A1184lccbS = new String[] {""} ;
      P007V2_n1184lccbS = new boolean[] {false} ;
      P007V2_A1172lccbS = new double[1] ;
      P007V2_n1172lccbS = new boolean[] {false} ;
      P007V2_A1171lccbT = new double[1] ;
      P007V2_n1171lccbT = new boolean[] {false} ;
      P007V2_A1168lccbI = new short[1] ;
      P007V2_n1168lccbI = new boolean[] {false} ;
      P007V2_A1170lccbI = new double[1] ;
      P007V2_n1170lccbI = new boolean[] {false} ;
      P007V2_A1150lccbE = new String[] {""} ;
      P007V2_A1222lccbI = new String[] {""} ;
      P007V2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007V2_A1224lccbC = new String[] {""} ;
      P007V2_A1225lccbC = new String[] {""} ;
      P007V2_A1226lccbA = new String[] {""} ;
      P007V2_A1227lccbO = new String[] {""} ;
      P007V2_A1228lccbF = new String[] {""} ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1150lccbE = "" ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1224lccbC = "" ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pajustasaleamount__default(),
         new Object[] {
             new Object[] {
            P007V2_A1184lccbS, P007V2_n1184lccbS, P007V2_A1172lccbS, P007V2_n1172lccbS, P007V2_A1171lccbT, P007V2_n1171lccbT, P007V2_A1168lccbI, P007V2_n1168lccbI, P007V2_A1170lccbI, P007V2_n1170lccbI,
            P007V2_A1150lccbE, P007V2_A1222lccbI, P007V2_A1223lccbD, P007V2_A1224lccbC, P007V2_A1225lccbC, P007V2_A1226lccbA, P007V2_A1227lccbO, P007V2_A1228lccbF
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short A1168lccbI ;
   private short Gx_err ;
   private double A1172lccbS ;
   private double A1171lccbT ;
   private double A1170lccbI ;
   private String scmdbuf ;
   private String A1184lccbS ;
   private String A1150lccbE ;
   private String A1222lccbI ;
   private String A1224lccbC ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private java.util.Date A1223lccbD ;
   private boolean n1184lccbS ;
   private boolean n1172lccbS ;
   private boolean n1171lccbT ;
   private boolean n1168lccbI ;
   private boolean n1170lccbI ;
   private IDataStoreProvider pr_default ;
   private String[] P007V2_A1184lccbS ;
   private boolean[] P007V2_n1184lccbS ;
   private double[] P007V2_A1172lccbS ;
   private boolean[] P007V2_n1172lccbS ;
   private double[] P007V2_A1171lccbT ;
   private boolean[] P007V2_n1171lccbT ;
   private short[] P007V2_A1168lccbI ;
   private boolean[] P007V2_n1168lccbI ;
   private double[] P007V2_A1170lccbI ;
   private boolean[] P007V2_n1170lccbI ;
   private String[] P007V2_A1150lccbE ;
   private String[] P007V2_A1222lccbI ;
   private java.util.Date[] P007V2_A1223lccbD ;
   private String[] P007V2_A1224lccbC ;
   private String[] P007V2_A1225lccbC ;
   private String[] P007V2_A1226lccbA ;
   private String[] P007V2_A1227lccbO ;
   private String[] P007V2_A1228lccbF ;
}

final  class pajustasaleamount__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007V2", "SELECT [lccbStatus], [lccbSaleAmount], [lccbTip], [lccbInstallments], [lccbInstAmount], [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbStatus] = 'TOSUB') AND ([lccbStatus] = 'TOSUB') ORDER BY [lccbStatus] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007V3", "UPDATE [LCCBPLP] SET [lccbSaleAmount]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 8) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((double[]) buf[2])[0] = rslt.getDouble(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(3) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((short[]) buf[6])[0] = rslt.getShort(4) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((double[]) buf[8])[0] = rslt.getDouble(5) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(6, 3) ;
               ((String[]) buf[11])[0] = rslt.getString(7, 7) ;
               ((java.util.Date[]) buf[12])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[13])[0] = rslt.getString(9, 2) ;
               ((String[]) buf[14])[0] = rslt.getString(10, 44) ;
               ((String[]) buf[15])[0] = rslt.getString(11, 20) ;
               ((String[]) buf[16])[0] = rslt.getString(12, 1) ;
               ((String[]) buf[17])[0] = rslt.getString(13, 19) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(1, ((Number) parms[1]).doubleValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               break;
      }
   }

}


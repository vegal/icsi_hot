import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtCompany extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCompany( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCompany.class));
   }

   public SdtCompany( int remoteHandle ,
                      ModelContext context )
   {
      super( context, "SdtCompany");
      initialize( remoteHandle) ;
   }

   public SdtCompany( int remoteHandle ,
                      StructSdtCompany struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV865CompanyCod )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV865CompanyCod});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyCod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompany_Companycod = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyDes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompany_Companydes = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompany_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyCod_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompany_Companycod_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyDes_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompany_Companydes_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Company" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("CompanyCod", GXutil.rtrim( gxTv_SdtCompany_Companycod));
      oWriter.writeElement("CompanyDes", GXutil.rtrim( gxTv_SdtCompany_Companydes));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCompany_Mode));
      oWriter.writeElement("CompanyCod_Z", GXutil.rtrim( gxTv_SdtCompany_Companycod_Z));
      oWriter.writeElement("CompanyDes_Z", GXutil.rtrim( gxTv_SdtCompany_Companydes_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtCompany_Companycod( )
   {
      return gxTv_SdtCompany_Companycod ;
   }

   public void setgxTv_SdtCompany_Companycod( String value )
   {
      gxTv_SdtCompany_Companycod = value ;
      return  ;
   }

   public void setgxTv_SdtCompany_Companycod_SetNull( )
   {
      gxTv_SdtCompany_Companycod = "" ;
      return  ;
   }

   public String getgxTv_SdtCompany_Companydes( )
   {
      return gxTv_SdtCompany_Companydes ;
   }

   public void setgxTv_SdtCompany_Companydes( String value )
   {
      gxTv_SdtCompany_Companydes = value ;
      return  ;
   }

   public void setgxTv_SdtCompany_Companydes_SetNull( )
   {
      gxTv_SdtCompany_Companydes = "" ;
      return  ;
   }

   public String getgxTv_SdtCompany_Mode( )
   {
      return gxTv_SdtCompany_Mode ;
   }

   public void setgxTv_SdtCompany_Mode( String value )
   {
      gxTv_SdtCompany_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtCompany_Mode_SetNull( )
   {
      gxTv_SdtCompany_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtCompany_Companycod_Z( )
   {
      return gxTv_SdtCompany_Companycod_Z ;
   }

   public void setgxTv_SdtCompany_Companycod_Z( String value )
   {
      gxTv_SdtCompany_Companycod_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCompany_Companycod_Z_SetNull( )
   {
      gxTv_SdtCompany_Companycod_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCompany_Companydes_Z( )
   {
      return gxTv_SdtCompany_Companydes_Z ;
   }

   public void setgxTv_SdtCompany_Companydes_Z( String value )
   {
      gxTv_SdtCompany_Companydes_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCompany_Companydes_Z_SetNull( )
   {
      gxTv_SdtCompany_Companydes_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tcompany_bc obj ;
      obj = new tcompany_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtCompany_Companycod = "" ;
      gxTv_SdtCompany_Companydes = "" ;
      gxTv_SdtCompany_Mode = "" ;
      gxTv_SdtCompany_Companycod_Z = "" ;
      gxTv_SdtCompany_Companydes_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtCompany Clone( )
   {
      SdtCompany sdt ;
      tcompany_bc obj ;
      sdt = (SdtCompany)(clone()) ;
      obj = (tcompany_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtCompany struct )
   {
      setgxTv_SdtCompany_Companycod(struct.getCompanycod());
      setgxTv_SdtCompany_Companydes(struct.getCompanydes());
      setgxTv_SdtCompany_Mode(struct.getMode());
      setgxTv_SdtCompany_Companycod_Z(struct.getCompanycod_Z());
      setgxTv_SdtCompany_Companydes_Z(struct.getCompanydes_Z());
   }

   public StructSdtCompany getStruct( )
   {
      StructSdtCompany struct = new StructSdtCompany ();
      struct.setCompanycod(getgxTv_SdtCompany_Companycod());
      struct.setCompanydes(getgxTv_SdtCompany_Companydes());
      struct.setMode(getgxTv_SdtCompany_Mode());
      struct.setCompanycod_Z(getgxTv_SdtCompany_Companycod_Z());
      struct.setCompanydes_Z(getgxTv_SdtCompany_Companydes_Z());
      return struct ;
   }

   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtCompany_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtCompany_Companycod ;
   protected String gxTv_SdtCompany_Companydes ;
   protected String gxTv_SdtCompany_Companycod_Z ;
   protected String gxTv_SdtCompany_Companydes_Z ;
}


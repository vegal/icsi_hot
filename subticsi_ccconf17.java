import com.genexus.*;
import com.genexus.ui.*;

public final  class subticsi_ccconf17 extends GXSubfileElement
{
   private int ICSI_CCError ;
   private String ICSI_CCErrorDsc ;
   private String ZICSI_CCErrorDsc ;
   public int getICSI_CCError( )
   {
      return ICSI_CCError ;
   }

   public void setICSI_CCError( int value )
   {
      ICSI_CCError = value;
   }

   public String getICSI_CCErrorDsc( )
   {
      return ICSI_CCErrorDsc ;
   }

   public void setICSI_CCErrorDsc( String value )
   {
      ICSI_CCErrorDsc = value;
   }

   public String getZICSI_CCErrorDsc( )
   {
      return ZICSI_CCErrorDsc ;
   }

   public void setZICSI_CCErrorDsc( String value )
   {
      ZICSI_CCErrorDsc = value;
   }

   public void clear( )
   {
      ICSI_CCError = 0 ;
      ICSI_CCErrorDsc = "" ;
      ZICSI_CCErrorDsc = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               if ( getICSI_CCError() > ((subticsi_ccconf17) element).getICSI_CCError() ) return 1;
               if ( getICSI_CCError() < ((subticsi_ccconf17) element).getICSI_CCError() ) return -1;
               return 0;
            case 1 :
               return  getICSI_CCErrorDsc().compareTo(((subticsi_ccconf17) element).getICSI_CCErrorDsc()) ;
      }
      return 0;
   }

   public int isChanged( )
   {
      return (!userModified && (!inserted
      && ( GXutil.strcmp(ZICSI_CCErrorDsc,ICSI_CCErrorDsc) == 0)
      ))?0:1;
   }

   public boolean isEmpty( )
   {
      return ( ( getICSI_CCError() == 0 ) && ( GXutil.strcmp(getICSI_CCErrorDsc(), "") == 0 ) || insertedNotUserModified() )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getICSI_CCError() );
            break;
         case 1 :
            cell.setValue( getICSI_CCErrorDsc() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( (((GUIObjectInt) cell).getValue() == getICSI_CCError()) );
         case 1 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getICSI_CCErrorDsc()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setICSI_CCError(value.getIntValue());
               break;
            case 1 :
               setICSI_CCErrorDsc(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setICSI_CCError(((subticsi_ccconf17) element).getICSI_CCError());
               return;
            case 1 :
               setICSI_CCErrorDsc(((subticsi_ccconf17) element).getICSI_CCErrorDsc());
               return;
      }
   }

}


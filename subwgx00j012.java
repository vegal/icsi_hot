import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx00j012 extends GXSubfileElement
{
   private String MsgCode ;
   private byte MsgStatus ;
   public String getMsgCode( )
   {
      return MsgCode ;
   }

   public void setMsgCode( String value )
   {
      MsgCode = value;
   }

   public byte getMsgStatus( )
   {
      return MsgStatus ;
   }

   public void setMsgStatus( byte value )
   {
      MsgStatus = value;
   }

   public void clear( )
   {
      MsgCode = "" ;
      MsgStatus = 0 ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getMsgCode().compareTo(((subwgx00j012) element).getMsgCode()) ;
            case 1 :
               if ( getMsgStatus() > ((subwgx00j012) element).getMsgStatus() ) return 1;
               if ( getMsgStatus() < ((subwgx00j012) element).getMsgStatus() ) return -1;
               return 0;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getMsgCode(), "") == 0 ) && ( getMsgStatus() == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getMsgCode() );
            break;
         case 1 :
            cell.setValue( getMsgStatus() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getMsgCode()) == 0) );
         case 1 :
            return ( (((GUIObjectByte) cell).getValue() == getMsgStatus()) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setMsgCode(value.getStringValue());
               break;
            case 1 :
               setMsgStatus(value.getByteValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setMsgCode(((subwgx00j012) element).getMsgCode());
               return;
            case 1 :
               setMsgStatus(((subwgx00j012) element).getMsgStatus());
               return;
      }
   }

}


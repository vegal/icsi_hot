
public final  class StructSdtIT0G_IT0GItem implements Cloneable, java.io.Serializable
{
   public StructSdtIT0G_IT0GItem( )
   {
      gxTv_SdtIT0G_IT0GItem_Emcp_it0g = (byte)(0) ;
      gxTv_SdtIT0G_IT0GItem_Emcv_it0g = 0 ;
      gxTv_SdtIT0G_IT0GItem_Cutp_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emrt_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emrc_it0g = (byte)(0) ;
      gxTv_SdtIT0G_IT0GItem_Emsc_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emoc_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xboa_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xbru_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xbne_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emrm_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emci_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xbct_it0g = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public byte getEmcp_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emcp_it0g ;
   }

   public void setEmcp_it0g( byte value )
   {
      gxTv_SdtIT0G_IT0GItem_Emcp_it0g = value ;
      return  ;
   }

   public long getEmcv_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emcv_it0g ;
   }

   public void setEmcv_it0g( long value )
   {
      gxTv_SdtIT0G_IT0GItem_Emcv_it0g = value ;
      return  ;
   }

   public String getCutp_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Cutp_it0g ;
   }

   public void setCutp_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Cutp_it0g = value ;
      return  ;
   }

   public String getEmrt_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emrt_it0g ;
   }

   public void setEmrt_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emrt_it0g = value ;
      return  ;
   }

   public byte getEmrc_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emrc_it0g ;
   }

   public void setEmrc_it0g( byte value )
   {
      gxTv_SdtIT0G_IT0GItem_Emrc_it0g = value ;
      return  ;
   }

   public String getEmsc_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emsc_it0g ;
   }

   public void setEmsc_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emsc_it0g = value ;
      return  ;
   }

   public String getEmoc_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emoc_it0g ;
   }

   public void setEmoc_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emoc_it0g = value ;
      return  ;
   }

   public String getXboa_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xboa_it0g ;
   }

   public void setXboa_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xboa_it0g = value ;
      return  ;
   }

   public String getXbru_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xbru_it0g ;
   }

   public void setXbru_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xbru_it0g = value ;
      return  ;
   }

   public String getXbne_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xbne_it0g ;
   }

   public void setXbne_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xbne_it0g = value ;
      return  ;
   }

   public String getEmrm_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emrm_it0g ;
   }

   public void setEmrm_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emrm_it0g = value ;
      return  ;
   }

   public String getEmci_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emci_it0g ;
   }

   public void setEmci_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emci_it0g = value ;
      return  ;
   }

   public String getXbct_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xbct_it0g ;
   }

   public void setXbct_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xbct_it0g = value ;
      return  ;
   }

   protected byte gxTv_SdtIT0G_IT0GItem_Emcp_it0g ;
   protected byte gxTv_SdtIT0G_IT0GItem_Emrc_it0g ;
   protected long gxTv_SdtIT0G_IT0GItem_Emcv_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Cutp_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Emrt_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Emsc_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Emoc_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Xboa_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Xbru_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Xbne_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Emrm_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Emci_it0g ;
   protected String gxTv_SdtIT0G_IT0GItem_Xbct_it0g ;
}


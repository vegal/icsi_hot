import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtCompanyProcessor extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCompanyProcessor( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCompanyProcessor.class));
   }

   public SdtCompanyProcessor( int remoteHandle ,
                               ModelContext context )
   {
      super( context, "SdtCompanyProcessor");
      initialize( remoteHandle) ;
   }

   public SdtCompanyProcessor( int remoteHandle ,
                               StructSdtCompanyProcessor struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV1419CompanyProcessorCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV1419CompanyProcessorCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyProcessorCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompanyProcessor_Companyprocessorcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyProcessorLayout") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompanyProcessor_Companyprocessorlayout = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompanyProcessor_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyProcessorCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyProcessorLayout_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CompanyProcessorCode_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "CompanyProcessor" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("CompanyProcessorCode", GXutil.rtrim( gxTv_SdtCompanyProcessor_Companyprocessorcode));
      oWriter.writeElement("CompanyProcessorLayout", GXutil.rtrim( gxTv_SdtCompanyProcessor_Companyprocessorlayout));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCompanyProcessor_Mode));
      oWriter.writeElement("CompanyProcessorCode_Z", GXutil.rtrim( gxTv_SdtCompanyProcessor_Companyprocessorcode_Z));
      oWriter.writeElement("CompanyProcessorLayout_Z", GXutil.rtrim( gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z));
      oWriter.writeElement("CompanyProcessorCode_N", GXutil.trim( GXutil.str( gxTv_SdtCompanyProcessor_Companyprocessorcode_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtCompanyProcessor_Companyprocessorcode( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorcode ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorcode( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(0) ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode = value ;
      return  ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorcode_SetNull( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(1) ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode = "" ;
      return  ;
   }

   public String getgxTv_SdtCompanyProcessor_Companyprocessorlayout( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorlayout ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorlayout( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorlayout = value ;
      return  ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorlayout_SetNull( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorlayout = "" ;
      return  ;
   }

   public String getgxTv_SdtCompanyProcessor_Mode( )
   {
      return gxTv_SdtCompanyProcessor_Mode ;
   }

   public void setgxTv_SdtCompanyProcessor_Mode( String value )
   {
      gxTv_SdtCompanyProcessor_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtCompanyProcessor_Mode_SetNull( )
   {
      gxTv_SdtCompanyProcessor_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtCompanyProcessor_Companyprocessorcode_Z( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorcode_Z ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorcode_Z( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorcode_Z_SetNull( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z( String value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z_SetNull( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtCompanyProcessor_Companyprocessorcode_N( )
   {
      return gxTv_SdtCompanyProcessor_Companyprocessorcode_N ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorcode_N( byte value )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = value ;
      return  ;
   }

   public void setgxTv_SdtCompanyProcessor_Companyprocessorcode_N_SetNull( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tcompanyprocessor_bc obj ;
      obj = new tcompanyprocessor_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtCompanyProcessor_Companyprocessorcode = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorlayout = "" ;
      gxTv_SdtCompanyProcessor_Mode = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtCompanyProcessor Clone( )
   {
      SdtCompanyProcessor sdt ;
      tcompanyprocessor_bc obj ;
      sdt = (SdtCompanyProcessor)(clone()) ;
      obj = (tcompanyprocessor_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtCompanyProcessor struct )
   {
      setgxTv_SdtCompanyProcessor_Companyprocessorcode(struct.getCompanyprocessorcode());
      setgxTv_SdtCompanyProcessor_Companyprocessorlayout(struct.getCompanyprocessorlayout());
      setgxTv_SdtCompanyProcessor_Mode(struct.getMode());
      setgxTv_SdtCompanyProcessor_Companyprocessorcode_Z(struct.getCompanyprocessorcode_Z());
      setgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z(struct.getCompanyprocessorlayout_Z());
      setgxTv_SdtCompanyProcessor_Companyprocessorcode_N(struct.getCompanyprocessorcode_N());
   }

   public StructSdtCompanyProcessor getStruct( )
   {
      StructSdtCompanyProcessor struct = new StructSdtCompanyProcessor ();
      struct.setCompanyprocessorcode(getgxTv_SdtCompanyProcessor_Companyprocessorcode());
      struct.setCompanyprocessorlayout(getgxTv_SdtCompanyProcessor_Companyprocessorlayout());
      struct.setMode(getgxTv_SdtCompanyProcessor_Mode());
      struct.setCompanyprocessorcode_Z(getgxTv_SdtCompanyProcessor_Companyprocessorcode_Z());
      struct.setCompanyprocessorlayout_Z(getgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z());
      struct.setCompanyprocessorcode_N(getgxTv_SdtCompanyProcessor_Companyprocessorcode_N());
      return struct ;
   }

   protected byte gxTv_SdtCompanyProcessor_Companyprocessorcode_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtCompanyProcessor_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorcode ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorlayout ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorcode_Z ;
   protected String gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z ;
}


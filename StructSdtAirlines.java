
public final  class StructSdtAirlines implements Cloneable, java.io.Serializable
{
   public StructSdtAirlines( )
   {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      cal.set(1, 0, 1, 0, 0, 0);
      cal.set(java.util.Calendar.MILLISECOND, 0);
      gxTv_SdtAirlines_Isocod = "" ;
      gxTv_SdtAirlines_Airlinecode = "" ;
      gxTv_SdtAirlines_Airlinedescription = "" ;
      gxTv_SdtAirlines_Isodes = "" ;
      gxTv_SdtAirlines_Airlineabbreviation = "" ;
      gxTv_SdtAirlines_Airlineaddress = "" ;
      gxTv_SdtAirlines_Airlineaddresscompl = "" ;
      gxTv_SdtAirlines_Airlinecity = "" ;
      gxTv_SdtAirlines_Airlinestate = "" ;
      gxTv_SdtAirlines_Airlinezip = "" ;
      gxTv_SdtAirlines_Airlinedatins = cal.getTime() ;
      gxTv_SdtAirlines_Airlinedatmod = cal.getTime() ;
      gxTv_SdtAirlines_Airlinecompleteaddress = "" ;
      gxTv_SdtAirlines_Airlinetypesettlement = "" ;
      gxTv_SdtAirlines_Airlinesta = "" ;
      gxTv_SdtAirlines_Airlinebanktransfermode = "" ;
      gxTv_SdtAirlines_Level1 = new java.util.Vector();
      gxTv_SdtAirlines_Contacts = new java.util.Vector();
      gxTv_SdtAirlines_Mode = "" ;
      gxTv_SdtAirlines_Isocod_Z = "" ;
      gxTv_SdtAirlines_Airlinecode_Z = "" ;
      gxTv_SdtAirlines_Airlinedescription_Z = "" ;
      gxTv_SdtAirlines_Isodes_Z = "" ;
      gxTv_SdtAirlines_Airlineabbreviation_Z = "" ;
      gxTv_SdtAirlines_Airlineaddress_Z = "" ;
      gxTv_SdtAirlines_Airlineaddresscompl_Z = "" ;
      gxTv_SdtAirlines_Airlinecity_Z = "" ;
      gxTv_SdtAirlines_Airlinestate_Z = "" ;
      gxTv_SdtAirlines_Airlinezip_Z = "" ;
      gxTv_SdtAirlines_Airlinedatins_Z = cal.getTime() ;
      gxTv_SdtAirlines_Airlinedatmod_Z = cal.getTime() ;
      gxTv_SdtAirlines_Airlinecompleteaddress_Z = "" ;
      gxTv_SdtAirlines_Airlinetypesettlement_Z = "" ;
      gxTv_SdtAirlines_Airlinesta_Z = "" ;
      gxTv_SdtAirlines_Airlinebanktransfermode_Z = "" ;
      gxTv_SdtAirlines_Isodes_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinesta_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIsocod( )
   {
      return gxTv_SdtAirlines_Isocod ;
   }

   public void setIsocod( String value )
   {
      gxTv_SdtAirlines_Isocod = value ;
      return  ;
   }

   public String getAirlinecode( )
   {
      return gxTv_SdtAirlines_Airlinecode ;
   }

   public void setAirlinecode( String value )
   {
      gxTv_SdtAirlines_Airlinecode = value ;
      return  ;
   }

   public String getAirlinedescription( )
   {
      return gxTv_SdtAirlines_Airlinedescription ;
   }

   public void setAirlinedescription( String value )
   {
      gxTv_SdtAirlines_Airlinedescription = value ;
      return  ;
   }

   public String getIsodes( )
   {
      return gxTv_SdtAirlines_Isodes ;
   }

   public void setIsodes( String value )
   {
      gxTv_SdtAirlines_Isodes = value ;
      return  ;
   }

   public String getAirlineabbreviation( )
   {
      return gxTv_SdtAirlines_Airlineabbreviation ;
   }

   public void setAirlineabbreviation( String value )
   {
      gxTv_SdtAirlines_Airlineabbreviation = value ;
      return  ;
   }

   public String getAirlineaddress( )
   {
      return gxTv_SdtAirlines_Airlineaddress ;
   }

   public void setAirlineaddress( String value )
   {
      gxTv_SdtAirlines_Airlineaddress = value ;
      return  ;
   }

   public String getAirlineaddresscompl( )
   {
      return gxTv_SdtAirlines_Airlineaddresscompl ;
   }

   public void setAirlineaddresscompl( String value )
   {
      gxTv_SdtAirlines_Airlineaddresscompl = value ;
      return  ;
   }

   public String getAirlinecity( )
   {
      return gxTv_SdtAirlines_Airlinecity ;
   }

   public void setAirlinecity( String value )
   {
      gxTv_SdtAirlines_Airlinecity = value ;
      return  ;
   }

   public String getAirlinestate( )
   {
      return gxTv_SdtAirlines_Airlinestate ;
   }

   public void setAirlinestate( String value )
   {
      gxTv_SdtAirlines_Airlinestate = value ;
      return  ;
   }

   public String getAirlinezip( )
   {
      return gxTv_SdtAirlines_Airlinezip ;
   }

   public void setAirlinezip( String value )
   {
      gxTv_SdtAirlines_Airlinezip = value ;
      return  ;
   }

   public java.util.Date getAirlinedatins( )
   {
      return gxTv_SdtAirlines_Airlinedatins ;
   }

   public void setAirlinedatins( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatins = value ;
      return  ;
   }

   public java.util.Date getAirlinedatmod( )
   {
      return gxTv_SdtAirlines_Airlinedatmod ;
   }

   public void setAirlinedatmod( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatmod = value ;
      return  ;
   }

   public String getAirlinecompleteaddress( )
   {
      return gxTv_SdtAirlines_Airlinecompleteaddress ;
   }

   public void setAirlinecompleteaddress( String value )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress = value ;
      return  ;
   }

   public String getAirlinetypesettlement( )
   {
      return gxTv_SdtAirlines_Airlinetypesettlement ;
   }

   public void setAirlinetypesettlement( String value )
   {
      gxTv_SdtAirlines_Airlinetypesettlement = value ;
      return  ;
   }

   public String getAirlinesta( )
   {
      return gxTv_SdtAirlines_Airlinesta ;
   }

   public void setAirlinesta( String value )
   {
      gxTv_SdtAirlines_Airlinesta = value ;
      return  ;
   }

   public String getAirlinebanktransfermode( )
   {
      return gxTv_SdtAirlines_Airlinebanktransfermode ;
   }

   public void setAirlinebanktransfermode( String value )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode = value ;
      return  ;
   }

   public java.util.Vector getLevel1( )
   {
      return gxTv_SdtAirlines_Level1 ;
   }

   public void setLevel1( java.util.Vector value )
   {
      gxTv_SdtAirlines_Level1 = value ;
      return  ;
   }

   public java.util.Vector getContacts( )
   {
      return gxTv_SdtAirlines_Contacts ;
   }

   public void setContacts( java.util.Vector value )
   {
      gxTv_SdtAirlines_Contacts = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtAirlines_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtAirlines_Mode = value ;
      return  ;
   }

   public String getIsocod_Z( )
   {
      return gxTv_SdtAirlines_Isocod_Z ;
   }

   public void setIsocod_Z( String value )
   {
      gxTv_SdtAirlines_Isocod_Z = value ;
      return  ;
   }

   public String getAirlinecode_Z( )
   {
      return gxTv_SdtAirlines_Airlinecode_Z ;
   }

   public void setAirlinecode_Z( String value )
   {
      gxTv_SdtAirlines_Airlinecode_Z = value ;
      return  ;
   }

   public String getAirlinedescription_Z( )
   {
      return gxTv_SdtAirlines_Airlinedescription_Z ;
   }

   public void setAirlinedescription_Z( String value )
   {
      gxTv_SdtAirlines_Airlinedescription_Z = value ;
      return  ;
   }

   public String getIsodes_Z( )
   {
      return gxTv_SdtAirlines_Isodes_Z ;
   }

   public void setIsodes_Z( String value )
   {
      gxTv_SdtAirlines_Isodes_Z = value ;
      return  ;
   }

   public String getAirlineabbreviation_Z( )
   {
      return gxTv_SdtAirlines_Airlineabbreviation_Z ;
   }

   public void setAirlineabbreviation_Z( String value )
   {
      gxTv_SdtAirlines_Airlineabbreviation_Z = value ;
      return  ;
   }

   public String getAirlineaddress_Z( )
   {
      return gxTv_SdtAirlines_Airlineaddress_Z ;
   }

   public void setAirlineaddress_Z( String value )
   {
      gxTv_SdtAirlines_Airlineaddress_Z = value ;
      return  ;
   }

   public String getAirlineaddresscompl_Z( )
   {
      return gxTv_SdtAirlines_Airlineaddresscompl_Z ;
   }

   public void setAirlineaddresscompl_Z( String value )
   {
      gxTv_SdtAirlines_Airlineaddresscompl_Z = value ;
      return  ;
   }

   public String getAirlinecity_Z( )
   {
      return gxTv_SdtAirlines_Airlinecity_Z ;
   }

   public void setAirlinecity_Z( String value )
   {
      gxTv_SdtAirlines_Airlinecity_Z = value ;
      return  ;
   }

   public String getAirlinestate_Z( )
   {
      return gxTv_SdtAirlines_Airlinestate_Z ;
   }

   public void setAirlinestate_Z( String value )
   {
      gxTv_SdtAirlines_Airlinestate_Z = value ;
      return  ;
   }

   public String getAirlinezip_Z( )
   {
      return gxTv_SdtAirlines_Airlinezip_Z ;
   }

   public void setAirlinezip_Z( String value )
   {
      gxTv_SdtAirlines_Airlinezip_Z = value ;
      return  ;
   }

   public java.util.Date getAirlinedatins_Z( )
   {
      return gxTv_SdtAirlines_Airlinedatins_Z ;
   }

   public void setAirlinedatins_Z( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatins_Z = value ;
      return  ;
   }

   public java.util.Date getAirlinedatmod_Z( )
   {
      return gxTv_SdtAirlines_Airlinedatmod_Z ;
   }

   public void setAirlinedatmod_Z( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatmod_Z = value ;
      return  ;
   }

   public String getAirlinecompleteaddress_Z( )
   {
      return gxTv_SdtAirlines_Airlinecompleteaddress_Z ;
   }

   public void setAirlinecompleteaddress_Z( String value )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_Z = value ;
      return  ;
   }

   public String getAirlinetypesettlement_Z( )
   {
      return gxTv_SdtAirlines_Airlinetypesettlement_Z ;
   }

   public void setAirlinetypesettlement_Z( String value )
   {
      gxTv_SdtAirlines_Airlinetypesettlement_Z = value ;
      return  ;
   }

   public String getAirlinesta_Z( )
   {
      return gxTv_SdtAirlines_Airlinesta_Z ;
   }

   public void setAirlinesta_Z( String value )
   {
      gxTv_SdtAirlines_Airlinesta_Z = value ;
      return  ;
   }

   public String getAirlinebanktransfermode_Z( )
   {
      return gxTv_SdtAirlines_Airlinebanktransfermode_Z ;
   }

   public void setAirlinebanktransfermode_Z( String value )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_Z = value ;
      return  ;
   }

   public byte getIsodes_N( )
   {
      return gxTv_SdtAirlines_Isodes_N ;
   }

   public void setIsodes_N( byte value )
   {
      gxTv_SdtAirlines_Isodes_N = value ;
      return  ;
   }

   public byte getAirlinecompleteaddress_N( )
   {
      return gxTv_SdtAirlines_Airlinecompleteaddress_N ;
   }

   public void setAirlinecompleteaddress_N( byte value )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_N = value ;
      return  ;
   }

   public byte getAirlinesta_N( )
   {
      return gxTv_SdtAirlines_Airlinesta_N ;
   }

   public void setAirlinesta_N( byte value )
   {
      gxTv_SdtAirlines_Airlinesta_N = value ;
      return  ;
   }

   public byte getAirlinebanktransfermode_N( )
   {
      return gxTv_SdtAirlines_Airlinebanktransfermode_N ;
   }

   public void setAirlinebanktransfermode_N( byte value )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_N = value ;
      return  ;
   }

   protected byte gxTv_SdtAirlines_Isodes_N ;
   protected byte gxTv_SdtAirlines_Airlinecompleteaddress_N ;
   protected byte gxTv_SdtAirlines_Airlinesta_N ;
   protected byte gxTv_SdtAirlines_Airlinebanktransfermode_N ;
   protected String gxTv_SdtAirlines_Airlinetypesettlement ;
   protected String gxTv_SdtAirlines_Airlinesta ;
   protected String gxTv_SdtAirlines_Airlinebanktransfermode ;
   protected String gxTv_SdtAirlines_Mode ;
   protected String gxTv_SdtAirlines_Airlinetypesettlement_Z ;
   protected String gxTv_SdtAirlines_Airlinesta_Z ;
   protected String gxTv_SdtAirlines_Airlinebanktransfermode_Z ;
   protected String gxTv_SdtAirlines_Isocod ;
   protected String gxTv_SdtAirlines_Airlinecode ;
   protected String gxTv_SdtAirlines_Airlinedescription ;
   protected String gxTv_SdtAirlines_Isodes ;
   protected String gxTv_SdtAirlines_Airlineabbreviation ;
   protected String gxTv_SdtAirlines_Airlineaddress ;
   protected String gxTv_SdtAirlines_Airlineaddresscompl ;
   protected String gxTv_SdtAirlines_Airlinecity ;
   protected String gxTv_SdtAirlines_Airlinestate ;
   protected String gxTv_SdtAirlines_Airlinezip ;
   protected String gxTv_SdtAirlines_Airlinecompleteaddress ;
   protected String gxTv_SdtAirlines_Isocod_Z ;
   protected String gxTv_SdtAirlines_Airlinecode_Z ;
   protected String gxTv_SdtAirlines_Airlinedescription_Z ;
   protected String gxTv_SdtAirlines_Isodes_Z ;
   protected String gxTv_SdtAirlines_Airlineabbreviation_Z ;
   protected String gxTv_SdtAirlines_Airlineaddress_Z ;
   protected String gxTv_SdtAirlines_Airlineaddresscompl_Z ;
   protected String gxTv_SdtAirlines_Airlinecity_Z ;
   protected String gxTv_SdtAirlines_Airlinestate_Z ;
   protected String gxTv_SdtAirlines_Airlinezip_Z ;
   protected String gxTv_SdtAirlines_Airlinecompleteaddress_Z ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatins ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatmod ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatins_Z ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatmod_Z ;
   protected java.util.Vector gxTv_SdtAirlines_Level1 ;
   protected java.util.Vector gxTv_SdtAirlines_Contacts ;
}


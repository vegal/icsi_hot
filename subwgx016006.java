import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx016006 extends GXSubfileElement
{
   private String uspDescription ;
   private String uspCode ;
   public String getuspDescription( )
   {
      return uspDescription ;
   }

   public void setuspDescription( String value )
   {
      uspDescription = value;
   }

   public String getuspCode( )
   {
      return uspCode ;
   }

   public void setuspCode( String value )
   {
      uspCode = value;
   }

   public void clear( )
   {
      uspDescription = "" ;
      uspCode = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getuspDescription().compareTo(((subwgx016006) element).getuspDescription()) ;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getuspDescription(), "") == 0 ) && ( GXutil.strcmp(getuspCode(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getuspDescription() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getuspDescription()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setuspDescription(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setuspDescription(((subwgx016006) element).getuspDescription());
               return;
      }
   }

}


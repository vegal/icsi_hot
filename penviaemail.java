/*
               File: EnviaEmail
        Description: Envia Email
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:57.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class penviaemail extends GXProcedure
{
   public penviaemail( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( penviaemail.class ), "" );
   }

   public penviaemail( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String aP2 ,
                        String aP3 ,
                        String aP4 ,
                        String[] AV8Attachm )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, AV8Attachm);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String aP2 ,
                             String aP3 ,
                             String aP4 ,
                             String[] AV8Attachm )
   {
      penviaemail.this.AV25Subjec = aP0;
      penviaemail.this.AV11Body = aP1;
      penviaemail.this.AV27To = aP2;
      penviaemail.this.AV12CC = aP3;
      penviaemail.this.AV10BCC = aP4;
      penviaemail.this.AV8Attachm = AV8Attachm;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV14emailM.clear();
      AV14emailM.getAttachments().removeAllItems();
      AV16Entrad = AV27To ;
      AV26Tipo = "TO" ;
      /* Execute user subroutine: S1158 */
      S1158 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      AV16Entrad = AV12CC ;
      AV26Tipo = "CC" ;
      /* Execute user subroutine: S1158 */
      S1158 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      AV16Entrad = AV10BCC ;
      AV26Tipo = "BCC" ;
      /* Execute user subroutine: S1158 */
      S1158 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      AV14emailM.setSubject( AV25Subjec );
      AV28Body2 = "<html><body>  <center><img src =" + "https://ww6.iata.org.br/iaportaltiespro/images/TIESSLOGO.jpg" + "></center>  <br><br> <br><br>  **** ATTENTION ****<br> [MESSAGE]  <br><br>  Best Regards,  <br><br>  TIESS TEAM  </body></html>" ;
      AV28Body2 = GXutil.strReplace( AV28Body2, "[MESSAGE]", AV11Body) ;
      AV14emailM.setHtmltext( AV28Body2 );
      GXt_svchar1 = AV9Auth ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailAuth", "S", "1", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV9Auth = GXt_svchar1 ;
      GXt_svchar1 = AV21Port ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailPort", "S", "25", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV21Port = GXt_svchar1 ;
      AV24smtp.setAuthentication( (short)(GXutil.val( GXutil.trim( AV9Auth), ".")) );
      AV24smtp.setPort( (int)(GXutil.val( GXutil.trim( AV21Port), ".")) );
      GXt_svchar1 = AV24smtp.getHost() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailServ", "S", "mailer01.acl.com.br", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV24smtp.setHost( GXt_svchar1 );
      GXt_svchar1 = AV24smtp.getUserName() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailUser", "S", "infra@r2tec.com", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV24smtp.setUserName( GXt_svchar1 );
      GXt_svchar1 = AV24smtp.getPassword() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailPwd", "S", "", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV24smtp.setPassword( GXt_svchar1 );
      GXt_svchar1 = AV14emailM.getFrom().getName() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailName", "S", "IATA TIESS GSA", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV14emailM.getFrom().setName( GXt_svchar1 );
      GXt_svchar1 = AV14emailM.getFrom().getAddress() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailAddress", "S", "infra@r2tec.com", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV14emailM.getFrom().setAddress( GXt_svchar1 );
      GXt_svchar1 = AV24smtp.getSender().getName() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailName", "S", "IATA TIESS GSAl", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV24smtp.getSender().setName( GXt_svchar1 );
      GXt_svchar1 = AV24smtp.getSender().getAddress() ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "EmailAddress", "S", "infra@r2tec.com", "", GXv_svchar2) ;
      penviaemail.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV24smtp.getSender().setAddress( GXt_svchar1 );
      AV19i = 1 ;
      while ( ( AV19i <= 5 ) )
      {
         if ( ( GXutil.strcmp(AV8Attachm[AV19i-1], "") != 0 ) )
         {
            AV14emailM.getAttachments().add(AV8Attachm[AV19i-1]);
         }
         AV19i = (int)(AV19i+1) ;
      }
      AV23Ret = (byte)(AV24smtp.login()) ;
      if ( ( AV23Ret == 0 ) )
      {
         AV23Ret = (byte)(AV24smtp.send(AV14emailM)) ;
         AV17error = (byte)(AV24smtp.logout()) ;
         if ( ( AV23Ret != 0 ) )
         {
            context.msgStatus( "Error Login:"+AV24smtp.getErrDescription() );
         }
      }
      cleanup();
   }

   public void S1158( )
   {
      /* 'PREPARADESTINATARIOS' Routine */
      if ( ( GXutil.strcmp(GXutil.right( GXutil.trim( AV16Entrad), 1), ";") != 0 ) )
      {
         AV16Entrad = GXutil.trim( AV16Entrad) + ";" ;
      }
      else
      {
         AV16Entrad = GXutil.trim( AV16Entrad) ;
      }
      AV22Pos = 1 ;
      while ( ( GXutil.len( AV16Entrad) > AV22Pos ) )
      {
         AV19i = GXutil.strSearch( AV16Entrad, ";", AV22Pos) ;
         AV20Inicio = AV22Pos ;
         AV18Final = (int)((AV19i-AV22Pos)) ;
         AV13Email = GXutil.substring( AV16Entrad, AV20Inicio, AV18Final) ;
         if ( ( GXutil.strcmp(AV26Tipo, "TO") == 0 ) )
         {
            AV15emailT.setAddress( AV13Email );
            AV15emailT.setName( AV13Email );
            AV14emailM.getTo().add(AV15emailT);
         }
         else if ( ( GXutil.strcmp(AV26Tipo, "CC") == 0 ) )
         {
            AV15emailT.setAddress( AV13Email );
            AV15emailT.setName( AV13Email );
            AV14emailM.getCc().add(AV15emailT);
         }
         else if ( ( GXutil.strcmp(AV26Tipo, "BCC") == 0 ) )
         {
            AV15emailT.setAddress( AV13Email );
            AV15emailT.setName( AV13Email );
            AV14emailM.getBcc().add(AV15emailT);
         }
         AV22Pos = (int)(AV19i+1) ;
      }
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV14emailM = new com.genexus.internet.GXMailMessage();
      AV16Entrad = "" ;
      AV26Tipo = "" ;
      returnInSub = false ;
      AV28Body2 = "" ;
      AV9Auth = "" ;
      AV21Port = "" ;
      AV24smtp = new com.genexus.internet.GXSMTPSession();
      GXv_svchar2 = new String [1] ;
      AV19i = 0 ;
      AV23Ret = (byte)(0) ;
      AV17error = (byte)(0) ;
      GXt_svchar1 = "" ;
      AV22Pos = 0 ;
      AV20Inicio = 0 ;
      AV18Final = 0 ;
      AV13Email = "" ;
      AV15emailT = new com.genexus.internet.MailRecipient();
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV23Ret ;
   private byte AV17error ;
   private short Gx_err ;
   private int AV19i ;
   private int AV22Pos ;
   private int AV20Inicio ;
   private int AV18Final ;
   private String AV26Tipo ;
   private boolean returnInSub ;
   private String AV28Body2 ;
   private String AV25Subjec ;
   private String AV11Body ;
   private String AV27To ;
   private String AV12CC ;
   private String AV10BCC ;
   private String AV8Attachm[] ;
   private String AV16Entrad ;
   private String AV9Auth ;
   private String AV21Port ;
   private String GXv_svchar2[] ;
   private String GXt_svchar1 ;
   private String AV13Email ;
   private com.genexus.internet.MailRecipient AV15emailT ;
   private com.genexus.internet.GXMailMessage AV14emailM ;
   private com.genexus.internet.GXSMTPSession AV24smtp ;
}


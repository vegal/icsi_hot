/*
               File: AlteraBaseDados
        Description: Stub for AlteraBaseDados
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:27.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class walterabasedados extends GXApplet
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      walterabasedados pgm = new walterabasedados (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void init( )
   {
      Application.setApplet(this);
      ApplicationContext.getInstance().setCurrentLocation( "" );
      ClientPreferences.resetPreferences();
      Application.init(GXcfg.class);
      ModelContext context = new ModelContext( walterabasedados.class );
      remoteHandle = Application.getNewRemoteHandle(context);
      execute();
   }

   public  walterabasedados( )
   {
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public walterabasedados( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( walterabasedados.class ), "" );
   }

   public walterabasedados( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new ualterabasedados(remoteHandle, context).execute(  );
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
}


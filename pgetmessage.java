/*
               File: GetMessage
        Description: Obt�m uma Mensagem do Sistema
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pgetmessage extends GXProcedure
{
   public pgetmessage( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pgetmessage.class ), "" );
   }

   public pgetmessage( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      pgetmessage.this.AV18MsgCod = aP0[0];
      this.aP0 = aP0;
      pgetmessage.this.AV19MsgBod = aP1[0];
      this.aP1 = aP1;
      pgetmessage.this.AV23MsgSub = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV19MsgBod = "<Empty>" ;
      AV23MsgSub = "<Empty>" ;
      /*
         INSERT RECORD ON TABLE MESSAGETYPES

      */
      A100MsgCod = AV18MsgCod ;
      A97MsgBody = AV19MsgBod ;
      n97MsgBody = false ;
      A96MsgDesc = "<Pending Description>" ;
      n96MsgDesc = false ;
      A868MSGSub = AV23MsgSub ;
      n868MSGSub = false ;
      A98MsgStat = (byte)(1) ;
      n98MsgStat = false ;
      /* Using cursor P00082 */
      pr_default.execute(0, new Object[] {A100MsgCod, new Boolean(n96MsgDesc), A96MsgDesc, new Boolean(n97MsgBody), A97MsgBody, new Boolean(n98MsgStat), new Byte(A98MsgStat), new Boolean(n868MSGSub), A868MSGSub});
      if ( (pr_default.getStatus(0) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P00083 */
         pr_default.execute(1, new Object[] {A100MsgCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A100MsgCod = P00083_A100MsgCod[0] ;
            A97MsgBody = P00083_A97MsgBody[0] ;
            n97MsgBody = P00083_n97MsgBody[0] ;
            A868MSGSub = P00083_A868MSGSub[0] ;
            n868MSGSub = P00083_n868MSGSub[0] ;
            AV19MsgBod = A97MsgBody ;
            AV23MsgSub = A868MSGSub ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pgetmessage.this.AV18MsgCod;
      this.aP1[0] = pgetmessage.this.AV19MsgBod;
      this.aP2[0] = pgetmessage.this.AV23MsgSub;
      Application.commit(context, remoteHandle, "DEFAULT", "pgetmessage");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      GX_INS19 = 0 ;
      A100MsgCod = "" ;
      A97MsgBody = "" ;
      n97MsgBody = false ;
      A96MsgDesc = "" ;
      n96MsgDesc = false ;
      A868MSGSub = "" ;
      n868MSGSub = false ;
      A98MsgStat = (byte)(0) ;
      n98MsgStat = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      scmdbuf = "" ;
      P00083_A100MsgCod = new String[] {""} ;
      P00083_A97MsgBody = new String[] {""} ;
      P00083_n97MsgBody = new boolean[] {false} ;
      P00083_A868MSGSub = new String[] {""} ;
      P00083_n868MSGSub = new boolean[] {false} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pgetmessage__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            P00083_A100MsgCod, P00083_A97MsgBody, P00083_n97MsgBody, P00083_A868MSGSub, P00083_n868MSGSub
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte A98MsgStat ;
   private short Gx_err ;
   private int GX_INS19 ;
   private String Gx_emsg ;
   private String scmdbuf ;
   private boolean n97MsgBody ;
   private boolean n96MsgDesc ;
   private boolean n868MSGSub ;
   private boolean n98MsgStat ;
   private String AV19MsgBod ;
   private String A97MsgBody ;
   private String AV18MsgCod ;
   private String AV23MsgSub ;
   private String A100MsgCod ;
   private String A96MsgDesc ;
   private String A868MSGSub ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P00083_A100MsgCod ;
   private String[] P00083_A97MsgBody ;
   private boolean[] P00083_n97MsgBody ;
   private String[] P00083_A868MSGSub ;
   private boolean[] P00083_n868MSGSub ;
}

final  class pgetmessage__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P00082", "INSERT INTO [MESSAGETYPES] ([MsgCode], [MsgDescription], [MsgBody], [MsgStatus], [MSGSubject]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00083", "SELECT [MsgCode], [MsgBody], [MSGSubject] FROM [MESSAGETYPES] WITH (NOLOCK) WHERE [MsgCode] = ? ORDER BY [MsgCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 50);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(3, (String)parms[4]);
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(4, ((Number) parms[6]).byteValue());
               }
               if ( ((Boolean) parms[7]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[8], 50);
               }
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


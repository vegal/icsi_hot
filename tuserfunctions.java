/*
               File: UserFunctions
        Description: User Functions
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:13.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tuserfunctions extends GXTransaction
{
   public tuserfunctions( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tuserfunctions.class ), "" );
   }

   public tuserfunctions( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey0D42( )
   {
      A51uspDesc = "" ;
   }

   public void initAll0D42( )
   {
      A47uspCode = "" ;
      K47uspCode = A47uspCode ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey0D42( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption0D0( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "UserFunctions" ;
   }

   protected String getFrmTitle( )
   {
      return "User Functions" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TUserFunctions.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      tuserfunctions.this.AV16uspCod = aP0[0];
      this.aP0 = aP0;
      tuserfunctions.this.Gx_mode = aP1[0];
      this.aP1 = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtuspCode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 89, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 89 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A47uspCode" );
      ((GXEdit) edtuspCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtuspCode.addFocusListener(this);
      edtuspCode.getGXComponent().setHelpId("HLP_TUserFunctions.htm");
      edtuspDescription = new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 113, 290, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 113 , 290 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A51uspDesc" );
      ((GXEdit) edtuspDescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtuspDescription.addFocusListener(this);
      edtuspDescription.getGXComponent().setHelpId("HLP_TUserFunctions.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "User Function Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 113 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "User Function Description", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 148 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtuspCode ,
                edtuspDescription ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtuspCode, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey0D42( ) ;
   }

   public void disable_std_buttons( )
   {
      bttBtn_first.setGXEnabled( 0 );
      bttBtn_prev.setGXEnabled( 0 );
      bttBtn_next.setGXEnabled( 0 );
      bttBtn_last.setGXEnabled( 0 );
      bttBtn_exit2.setGXEnabled( 0 );
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtuspCode.setEnabled( 0 );
         edtuspDescription.setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll0D42( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtuspCode.isEventSource(eventSource) ) {
         setGXCursor( edtuspCode.getGXCursor() );
         return;
      }
      if ( edtuspDescription.isEventSource(eventSource) ) {
         setGXCursor( edtuspDescription.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtuspCode.isEventSource(eventSource) ) {
         valid_Uspcode ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtuspCode.isEventSource(eventSource) ) {
         A47uspCode = edtuspCode.getValue() ;
         return;
      }
      if ( edtuspDescription.isEventSource(eventSource) ) {
         A51uspDesc = edtuspDescription.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtuspCode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A47uspCode, edtuspCode.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtuspDescription.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A51uspDesc, edtuspDescription.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption0D0( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtuspCode.isEventSource(eventSource) )
      {
         edtuspCode.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtuspDescription.isEventSource(eventSource) )
      {
         edtuspDescription.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void VariablesToControls( )
   {
      edtuspCode.setValue( A47uspCode );
      edtuspDescription.setValue( A51uspDesc );
   }

   protected void ControlsToVariables( )
   {
      A47uspCode = edtuspCode.getValue() ;
      A51uspDesc = edtuspDescription.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Uspcode( )
   {
      if ( ( GXutil.strcmp(A47uspCode, K47uspCode) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K47uspCode = A47uspCode ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
         {
            sMode42 = Gx_mode ;
            Gx_mode = "UPD" ;
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
            {
               A47uspCode = AV16uspCod ;
               edtuspCode.setValue(A47uspCode);
            }
            Gx_mode = sMode42 ;
         }
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            getByPrimaryKey( ) ;
            if ( ( RcdFound42 != 1 ) )
            {
               pushError( localUtil.getMessages().getMessage("noinsert") );
               AnyError = (short)(1) ;
               keepFocus();
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm0D42( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z51uspDesc = T000D3_A51uspDesc[0] ;
         }
         else
         {
            Z51uspDesc = A51uspDesc ;
         }
      }
      if ( ( GX_JID == -2 ) )
      {
         Z47uspCode = A47uspCode ;
         Z51uspDesc = A51uspDesc ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
      {
         A47uspCode = AV16uspCod ;
         edtuspCode.setValue(A47uspCode);
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
      }
   }

   public void load0D42( )
   {
      /* Using cursor T000D4 */
      pr_default.execute(2, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound42 = (short)(1) ;
         A51uspDesc = T000D4_A51uspDesc[0] ;
         zm0D42( -2) ;
      }
      pr_default.close(2);
      onLoadActions0D42( ) ;
   }

   public void onLoadActions0D42( )
   {
   }

   public void checkExtendedTable0D42( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors0D42( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey0D42( )
   {
      /* Using cursor T000D5 */
      pr_default.execute(3, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound42 = (short)(1) ;
      }
      else
      {
         RcdFound42 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T000D3 */
      pr_default.execute(1, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0D42( 2) ;
         RcdFound42 = (short)(1) ;
         A47uspCode = T000D3_A47uspCode[0] ;
         A51uspDesc = T000D3_A51uspDesc[0] ;
         Z47uspCode = A47uspCode ;
         sMode42 = Gx_mode ;
         Gx_mode = "DSP" ;
         load0D42( ) ;
         Gx_mode = sMode42 ;
      }
      else
      {
         RcdFound42 = (short)(0) ;
         initializeNonKey0D42( ) ;
         sMode42 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode42 ;
      }
      K47uspCode = A47uspCode ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey0D42( ) ;
      if ( ( RcdFound42 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound42 = (short)(0) ;
      /* Using cursor T000D6 */
      pr_default.execute(4, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T000D6_A47uspCode[0], A47uspCode) < 0 ) ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T000D6_A47uspCode[0], A47uspCode) > 0 ) ) )
         {
            A47uspCode = T000D6_A47uspCode[0] ;
            RcdFound42 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound42 = (short)(0) ;
      /* Using cursor T000D7 */
      pr_default.execute(5, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T000D7_A47uspCode[0], A47uspCode) > 0 ) ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T000D7_A47uspCode[0], A47uspCode) < 0 ) ) )
         {
            A47uspCode = T000D7_A47uspCode[0] ;
            RcdFound42 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey0D42( ) ;
      if ( ( RcdFound42 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtuspCode );
         }
         else if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
         {
            A47uspCode = Z47uspCode ;
            edtuspCode.setValue(A47uspCode);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtuspCode );
         }
         else
         {
            /* Update record */
            update0D42( ) ;
            setNextFocus( edtuspCode );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
         {
            /* Insert record */
            insert0D42( ) ;
            setNextFocus( edtuspCode );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtuspCode );
            }
            else
            {
               /* Insert record */
               insert0D42( ) ;
               setNextFocus( edtuspCode );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
      {
         A47uspCode = Z47uspCode ;
         edtuspCode.setValue(A47uspCode);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtuspCode );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtuspCode );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void checkOptimisticConcurrency0D42( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T000D2 */
         pr_default.execute(0, new Object[] {A47uspCode});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"USERFUNCTIONS"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z51uspDesc, T000D2_A51uspDesc[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"USERFUNCTIONS"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert0D42( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0D42( 0) ;
         checkOptimisticConcurrency0D42( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0D42( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T000D8 */
                  pr_default.execute(6, new Object[] {A47uspCode, A51uspDesc});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                        resetCaption0D0( ) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load0D42( ) ;
         }
         endLevel0D42( ) ;
      }
      closeExtendedTableCursors0D42( ) ;
   }

   public void update0D42( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0D42( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0D42( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T000D9 */
                  pr_default.execute(7, new Object[] {A51uspDesc, A47uspCode});
                  deferredUpdate0D42( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel0D42( ) ;
      }
      closeExtendedTableCursors0D42( ) ;
   }

   public void deferredUpdate0D42( )
   {
   }

   public void delete( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0D42( ) ;
         /* No cascading delete specified. */
         afterConfirm0D42( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T000D10 */
               pr_default.execute(8, new Object[] {A47uspCode});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     loopOnce = true;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode42 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0D42( ) ;
      Gx_mode = sMode42 ;
   }

   public void onDeleteControls0D42( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor T000D11 */
         pr_default.execute(9, new Object[] {A47uspCode});
         if ( (pr_default.getStatus(9) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"Level1"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(9);
      }
   }

   public void endLevel0D42( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete0D42( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tuserfunctions");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tuserfunctions");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart0D42( )
   {
      /* Using cursor T000D12 */
      pr_default.execute(10);
      RcdFound42 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound42 = (short)(1) ;
         A47uspCode = T000D12_A47uspCode[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0D42( )
   {
      pr_default.readNext(10);
      RcdFound42 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound42 = (short)(1) ;
         A47uspCode = T000D12_A47uspCode[0] ;
      }
   }

   public void scanEnd0D42( )
   {
      pr_default.close(10);
   }

   public void afterConfirm0D42( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0D42( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0D42( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0D42( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0D42( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0D42( )
   {
      /* Before Validate Rules */
   }

   public void confirm_0D0( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls0D42( ) ;
         }
         else
         {
            checkExtendedTable0D42( ) ;
            closeExtendedTableCursors0D42( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_exit1.getBitmap() ;
         PreviousTooltip = bttBtn_exit1.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = tuserfunctions.this.AV16uspCod;
      this.aP1[0] = tuserfunctions.this.Gx_mode;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A51uspDesc = "" ;
      A47uspCode = "" ;
      K47uspCode = "" ;
      lastAnyError = 0 ;
      sMode42 = "" ;
      RcdFound42 = (short)(0) ;
      Z51uspDesc = "" ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z47uspCode = "" ;
      T000D4_A47uspCode = new String[] {""} ;
      T000D4_A51uspDesc = new String[] {""} ;
      Gx_BScreen = (byte)(0) ;
      T000D5_A47uspCode = new String[] {""} ;
      T000D3_A47uspCode = new String[] {""} ;
      T000D3_A51uspDesc = new String[] {""} ;
      T000D6_A47uspCode = new String[] {""} ;
      T000D7_A47uspCode = new String[] {""} ;
      T000D2_A47uspCode = new String[] {""} ;
      T000D2_A51uspDesc = new String[] {""} ;
      T000D11_A64ustCode = new String[] {""} ;
      T000D11_A47uspCode = new String[] {""} ;
      T000D12_A47uspCode = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tuserfunctions__default(),
         new Object[] {
             new Object[] {
            T000D2_A47uspCode, T000D2_A51uspDesc
            }
            , new Object[] {
            T000D3_A47uspCode, T000D3_A51uspDesc
            }
            , new Object[] {
            T000D4_A47uspCode, T000D4_A51uspDesc
            }
            , new Object[] {
            T000D5_A47uspCode
            }
            , new Object[] {
            T000D6_A47uspCode
            }
            , new Object[] {
            T000D7_A47uspCode
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000D11_A64ustCode, T000D11_A47uspCode
            }
            , new Object[] {
            T000D12_A47uspCode
            }
         }
      );
      reloadDynamicLists(0);
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound42 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String sMode42 ;
   protected String scmdbuf ;
   protected String A51uspDesc ;
   protected String A47uspCode ;
   protected String K47uspCode ;
   protected String AV16uspCod ;
   protected String Z51uspDesc ;
   protected String Z47uspCode ;
   protected String[] aP0 ;
   protected String[] aP1 ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtuspCode ;
   protected GUIObjectString edtuspDescription ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T000D4_A47uspCode ;
   protected String[] T000D4_A51uspDesc ;
   protected String[] T000D5_A47uspCode ;
   protected String[] T000D3_A47uspCode ;
   protected String[] T000D3_A51uspDesc ;
   protected String[] T000D6_A47uspCode ;
   protected String[] T000D7_A47uspCode ;
   protected String[] T000D2_A47uspCode ;
   protected String[] T000D2_A51uspDesc ;
   protected String[] T000D11_A64ustCode ;
   protected String[] T000D11_A47uspCode ;
   protected String[] T000D12_A47uspCode ;
}

final  class tuserfunctions__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T000D2", "SELECT [uspCode], [uspDescription] FROM [USERFUNCTIONS] WITH (UPDLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000D3", "SELECT [uspCode], [uspDescription] FROM [USERFUNCTIONS] WITH (NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000D4", "SELECT TM1.[uspCode], TM1.[uspDescription] FROM [USERFUNCTIONS] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[uspCode] = ? ORDER BY TM1.[uspCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000D5", "SELECT [uspCode] FROM [USERFUNCTIONS] WITH (FASTFIRSTROW NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000D6", "SELECT TOP 1 [uspCode] FROM [USERFUNCTIONS] WITH (FASTFIRSTROW NOLOCK) WHERE ( [uspCode] > ?) ORDER BY [uspCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T000D7", "SELECT TOP 1 [uspCode] FROM [USERFUNCTIONS] WITH (FASTFIRSTROW NOLOCK) WHERE ( [uspCode] < ?) ORDER BY [uspCode] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T000D8", "INSERT INTO [USERFUNCTIONS] ([uspCode], [uspDescription]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000D9", "UPDATE [USERFUNCTIONS] SET [uspDescription]=?  WHERE [uspCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T000D10", "DELETE FROM [USERFUNCTIONS]  WHERE [uspCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T000D11", "SELECT TOP 1 [ustCode], [uspCode] FROM [USERPROFILESLEVEL1] WITH (NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T000D12", "SELECT [uspCode] FROM [USERFUNCTIONS] WITH (FASTFIRSTROW NOLOCK) ORDER BY [uspCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 40, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 40, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


/*
               File: CarregaHot
        Description: Carrega Hot
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:55:32.31
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class acarregahot extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      acarregahot pgm = new acarregahot (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public acarregahot( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( acarregahot.class ), "" );
   }

   public acarregahot( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV25Path ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PATH_HOT", "Tasf Loader Path", "F", "C:\\R2Tech\\WebApps\\iatatasfdev", GXv_svchar2) ;
      acarregahot.this.GXt_char1 = GXv_svchar2[0] ;
      AV25Path = GXt_char1 ;
      GXt_char1 = AV31PathPr ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PATH_HOT_PROCESSED", "Path where the files are copied", "F", "C:\\R2Tech\\WebApps\\iatatasfdev\\Processed", GXv_svchar2) ;
      acarregahot.this.GXt_char1 = GXv_svchar2[0] ;
      AV31PathPr = GXt_char1 ;
      AV28Direto.setSource( GXutil.trim( AV25Path) );
      /* Execute user subroutine: S1163 */
      S1163 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      /* Optimized DELETE. */
      /* Using cursor P005H2 */
      pr_default.execute(0);
      /* End optimized DELETE. */
      AV11Barra = "Loading Temporary Table..." ;
      AV12RetVal = (byte)(context.getSessionInstances().getDelimitedFiles().dfropen( AV26Fileso, 255, "%", "\"", "")) ;
      AV16HotCou = 0 ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_svchar2[0] = AV17Linha ;
         GXt_int3 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_svchar2, (short)(255)) ;
         AV17Linha = GXv_svchar2[0] ;
         AV12RetVal = (byte)(GXt_int3) ;
         AV20sHntSe = GXutil.padl( GXutil.trim( GXutil.str( AV16HotCou, 10, 0)), (short)(8), "0") ;
         /*
            INSERT RECORD ON TABLE RETSPECTEMP

         */
         A997HntSeq = GXutil.trim( AV20sHntSe) ;
         A989HntRec = GXutil.substring( AV17Linha, 1, 3) ;
         n989HntRec = false ;
         A994HntLin = AV17Linha ;
         n994HntLin = false ;
         A993HntUsu = "TASFHOT" ;
         n993HntUsu = false ;
         /* Using cursor P005H3 */
         pr_default.execute(1, new Object[] {A997HntSeq, new Byte(A998HntSub), new Boolean(n989HntRec), A989HntRec, new Boolean(n993HntUsu), A993HntUsu, new Boolean(n994HntLin), A994HntLin});
         if ( (pr_default.getStatus(1) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         Application.commit(context, remoteHandle, "DEFAULT", "acarregahot");
         AV16HotCou = (int)(AV16HotCou+1) ;
      }
      AV13Tam = (short)(AV13Tam+1) ;
      AV18Tamanh = (int)(GXutil.len( AV26Fileso)-AV13Tam) ;
      AV19Arquiv = GXutil.substring( AV26Fileso, AV13Tam, AV18Tamanh) ;
      AV12RetVal = (byte)(context.getSessionInstances().getDelimitedFiles().dfrclose( )) ;
      GXv_svchar2[0] = AV9CiaCod ;
      GXv_char4[0] = "" ;
      GXv_char5[0] = "M" ;
      GXv_char6[0] = "" ;
      GXv_char7[0] = "" ;
      GXv_char8[0] = AV10Type ;
      GXv_char9[0] = AV26Fileso ;
      new pverper(remoteHandle, context).execute( GXv_svchar2, GXv_char4, GXv_char5, GXv_char6, GXv_char7, GXv_char8, GXv_char9) ;
      acarregahot.this.AV9CiaCod = GXv_svchar2[0] ;
      acarregahot.this.AV10Type = GXv_char8[0] ;
      acarregahot.this.AV26Fileso = GXv_char9[0] ;
      /* Optimized DELETE. */
      /* Using cursor P005H4 */
      pr_default.execute(2);
      /* End optimized DELETE. */
      GXt_char1 = AV32ShortN ;
      GXv_char9[0] = AV26Fileso ;
      GXv_char8[0] = GXt_char1 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char9, GXv_char8) ;
      acarregahot.this.AV26Fileso = GXv_char9[0] ;
      acarregahot.this.GXt_char1 = GXv_char8[0] ;
      AV32ShortN = GXt_char1 ;
      AV31PathPr = GXutil.trim( AV31PathPr) + GXutil.trim( AV32ShortN) ;
      AV27File.setSource( AV26Fileso );
      AV27File.copy(AV31PathPr);
      AV12RetVal = GXutil.deleteFile( AV26Fileso) ;
      cleanup();
   }

   public void S1163( )
   {
      /* 'BUSCA_ARQUIVOS' Routine */
      AV38GXV2 = 1 ;
      AV37GXV1 = AV28Direto.getFiles("txt") ;
      while ( ( AV38GXV2 <= AV37GXV1.getItemCount() ) )
      {
         AV27File = AV37GXV1.item((short)(AV38GXV2)) ;
         AV26Fileso = AV27File.getAbsoluteName() ;
         AV38GXV2 = (int)(AV38GXV2+1) ;
      }
   }



   protected void cleanup( )
   {
      Application.commit(context, remoteHandle, "DEFAULT", "acarregahot");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25Path = "" ;
      AV31PathPr = "" ;
      AV28Direto = new com.genexus.util.GXDirectory();
      returnInSub = false ;
      AV11Barra = "" ;
      AV12RetVal = (byte)(0) ;
      AV26Fileso = "" ;
      AV16HotCou = 0 ;
      AV17Linha = "" ;
      GXt_int3 = (short)(0) ;
      AV20sHntSe = "" ;
      GX_INS207 = 0 ;
      A997HntSeq = "" ;
      A989HntRec = "" ;
      n989HntRec = false ;
      A994HntLin = "" ;
      n994HntLin = false ;
      A993HntUsu = "" ;
      n993HntUsu = false ;
      A998HntSub = (byte)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV13Tam = (short)(0) ;
      AV18Tamanh = 0 ;
      AV19Arquiv = "" ;
      AV9CiaCod = "" ;
      GXv_svchar2 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_char5 = new String [1] ;
      GXv_char6 = new String [1] ;
      GXv_char7 = new String [1] ;
      AV10Type = "" ;
      AV32ShortN = "" ;
      GXt_char1 = "" ;
      GXv_char9 = new String [1] ;
      GXv_char8 = new String [1] ;
      AV27File = new com.genexus.util.GXFile();
      AV38GXV2 = 0 ;
      AV37GXV1 = new com.genexus.util.GXFileCollection();
      pr_default = new DataStoreProvider(context, remoteHandle, new acarregahot__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV12RetVal ;
   private byte A998HntSub ;
   private short GXt_int3 ;
   private short Gx_err ;
   private short AV13Tam ;
   private int AV16HotCou ;
   private int GX_INS207 ;
   private int AV18Tamanh ;
   private int AV38GXV2 ;
   private String AV25Path ;
   private String AV11Barra ;
   private String AV17Linha ;
   private String AV20sHntSe ;
   private String A997HntSeq ;
   private String A989HntRec ;
   private String Gx_emsg ;
   private String AV19Arquiv ;
   private String AV9CiaCod ;
   private String GXv_char4[] ;
   private String GXv_char5[] ;
   private String GXv_char6[] ;
   private String GXv_char7[] ;
   private String AV10Type ;
   private String GXt_char1 ;
   private String GXv_char9[] ;
   private String GXv_char8[] ;
   private boolean returnInSub ;
   private boolean n989HntRec ;
   private boolean n994HntLin ;
   private boolean n993HntUsu ;
   private String A994HntLin ;
   private String AV31PathPr ;
   private String AV26Fileso ;
   private String A993HntUsu ;
   private String GXv_svchar2[] ;
   private String AV32ShortN ;
   private com.genexus.util.GXFile AV27File ;
   private com.genexus.util.GXDirectory AV28Direto ;
   private IDataStoreProvider pr_default ;
   private com.genexus.util.GXFileCollection AV37GXV1 ;
}

final  class acarregahot__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P005H2", "DELETE FROM [RETSPECTEMP]  WHERE [HntUsuCod] like 'TASFHOT%'", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005H3", "INSERT INTO [RETSPECTEMP] ([HntSeq], [HntSub], [HntRecId], [HntUsuCod], [HntLine], [HntTypId], [HntIata], [HntStd], [HntTDNR], [HntTRNC]) VALUES (?, ?, ?, ?, ?, '', '', '', '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005H4", "DELETE FROM [RETSPECTEMP]  WHERE [HntUsuCod] like 'TASFHOT%'", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setByte(2, ((Number) parms[1]).byteValue());
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 3);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(5, (String)parms[7]);
               }
               break;
      }
   }

}


/*
               File: RETRSubCielo
        Description: RETRSub Cielo
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:14.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pretrsubcielo extends GXProcedure
{
   public pretrsubcielo( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretrsubcielo.class ), "" );
   }

   public pretrsubcielo( int remoteHandle ,
                         ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pretrsubcielo.this.AV11DebugM = aP0[0];
      this.aP0 = aP0;
      pretrsubcielo.this.AV100FileS = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_svchar1 = AV154Bande ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER", "C�digo do cart�o Hipercard", "S", "HC", GXv_svchar2) ;
      pretrsubcielo.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV154Bande = GXutil.trim( GXt_svchar1) ;
      GXt_svchar1 = AV155NovaB ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER_NEW", "C�digo do cart�o Hipercard para arquivos output", "S", "HP", GXv_svchar2) ;
      pretrsubcielo.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV155NovaB = GXutil.trim( GXt_svchar1) ;
      GXt_svchar1 = AV156Bande ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO", "C�digo do cart�o ELO", "S", "EL", GXv_svchar2) ;
      pretrsubcielo.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV156Bande = GXutil.trim( GXt_svchar1) ;
      GXt_svchar1 = AV157NovaB ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO_NEW", "C�digo do cart�o ELO para arquivos output", "S", "EL", GXv_svchar2) ;
      pretrsubcielo.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV157NovaB = GXutil.trim( GXt_svchar1) ;
      if ( ( GXutil.strSearch( AV11DebugM, "TESTMODE", 1) > 0 ) )
      {
         AV122TestM = (byte)(1) ;
      }
      else
      {
         AV122TestM = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV11DebugM, "SAYALL", 1) > 0 ) )
      {
         AV124Verbo = (byte)(1) ;
      }
      else
      {
         AV124Verbo = (byte)(0) ;
      }
      AV9DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      AV10DataC = GXutil.substring( AV9DataB, 3, 6) ;
      AV28HoraA = GXutil.time( ) ;
      AV29HoraB = GXutil.substring( AV28HoraA, 1, 2) + GXutil.substring( AV28HoraA, 4, 2) + GXutil.substring( AV28HoraA, 7, 2) ;
      /* Execute user subroutine: S1133 */
      S1133 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S1133( )
   {
      /* 'MAIN' Routine */
      AV85Versao = "0.00.02" ;
      AV11DebugM = GXutil.trim( GXutil.upper( AV11DebugM)) ;
      GXt_svchar1 = AV14FileNa ;
      GXv_svchar2[0] = AV100FileS ;
      GXv_char3[0] = GXt_svchar1 ;
      new pr2shortname(remoteHandle, context).execute( GXv_svchar2, GXv_char3) ;
      pretrsubcielo.this.AV100FileS = GXv_svchar2[0] ;
      pretrsubcielo.this.GXt_svchar1 = GXv_char3[0] ;
      AV14FileNa = GXt_svchar1 ;
      if ( ( GXutil.strSearch( AV11DebugM, "SAYALL", 1) > 0 ) )
      {
         context.msgStatus( "Cielo Submission Feedback - Version "+AV85Versao );
         context.msgStatus( "  Running mode: ["+AV11DebugM+"] - Started at "+GXutil.time( ) );
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV100FileS), "") != 0 ) )
      {
         context.msgStatus( "  Reading feedback file "+AV14FileNa );
         /* Execute user subroutine: S121 */
         S121 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( AV110Error == 0 ) )
         {
            AV143Aceit = "N" ;
            /* Execute user subroutine: S131 */
            S131 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.msgStatus( "  Record Statistics:" );
            AV104n = (int)(AV128cntCC+AV129cntCC) ;
            context.msgStatus( "    Total CC      = "+GXutil.trim( GXutil.str( AV104n, 10, 0)) );
            AV104n = (int)(AV130cntPL+AV131cntPL) ;
            context.msgStatus( "    Total PLP     = "+GXutil.trim( GXutil.str( AV104n, 10, 0)) );
            context.msgStatus( "  Breakdown:" );
            context.msgStatus( "    Accepted      = "+GXutil.trim( GXutil.str( AV132cntAc, 10, 0)) );
            context.msgStatus( "       Normal CC  = "+GXutil.trim( GXutil.str( AV128cntCC, 10, 0)) );
            context.msgStatus( "       PLP        = "+GXutil.trim( GXutil.str( AV130cntPL, 10, 0)) );
            context.msgStatus( "    Rejected      = "+GXutil.trim( GXutil.str( AV133cntRe, 10, 0)) );
            context.msgStatus( "       Normal CC  = "+GXutil.trim( GXutil.str( AV129cntCC, 10, 0)) );
            context.msgStatus( "       PLP        = "+GXutil.trim( GXutil.str( AV131cntPL, 10, 0)) );
            context.msgStatus( "    Wrong status  = "+GXutil.trim( GXutil.str( AV116cntWr, 10, 0)) );
            context.msgStatus( "    Not found     = "+GXutil.trim( GXutil.str( AV115cntNo, 10, 0)) );
         }
      }
      else
      {
         if ( ( GXutil.strSearch( AV11DebugM, "SAYALL", 1) > 0 ) )
         {
            context.msgStatus( "  ERROR: No filename suplied!" );
         }
      }
   }

   public void S121( )
   {
      /* 'CHECKFILE' Routine */
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfropen( AV100FileS, 250, "", "\"", "") ;
      AV110Error = (byte)(0) ;
      AV106LineC = 0 ;
      AV134Openb = 0 ;
      AV107OpenB = 0 ;
      AV144Erro_ = "N" ;
      AV145Erro_ = "N" ;
      AV146Linha = "" ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char3[0] = AV102Linha ;
         GXt_int12 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char3, (short)(255)) ;
         AV102Linha = GXv_char3[0] ;
         AV15FileNo = GXt_int12 ;
         AV106LineC = (int)(AV106LineC+1) ;
         AV105LineC = GXutil.trim( GXutil.str( AV106LineC, 10, 0)) ;
         Gx_msg = "  ERROR at line " + AV105LineC + ": " ;
         AV103RecTy = GXutil.upper( GXutil.substring( AV102Linha, 1, 2)) ;
         AV153RecTy = GXutil.upper( GXutil.substring( AV102Linha, 41, 5)) ;
         AV104n = GXutil.len( AV102Linha) ;
         AV146Linha = AV102Linha ;
         if ( ( ( GXutil.strcmp(AV103RecTy, "00") != 0 ) || ( GXutil.strcmp(AV153RecTy, "986PV") != 0 ) ) && ( AV106LineC == 1 ) )
         {
            Gx_msg = Gx_msg + "File Header Invalid" ;
            context.msgStatus( Gx_msg );
            AV145Erro_ = "S" ;
            AV110Error = (byte)(AV110Error+1) ;
            if (true) break;
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "00") == 0 ) && ( GXutil.strcmp(AV153RecTy, "986PV") != 0 ) )
         {
            Gx_msg = Gx_msg + "File Header Invalid" ;
            context.msgStatus( Gx_msg );
            AV145Erro_ = "S" ;
            AV110Error = (byte)(AV110Error+1) ;
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "00") == 0 ) )
         {
            if ( ( AV134Openb > 0 ) )
            {
               Gx_msg = Gx_msg + "File Header duplicated" ;
               context.msgStatus( Gx_msg );
               AV110Error = (byte)(AV110Error+1) ;
            }
            AV134Openb = (int)(AV134Openb+1) ;
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "01") == 0 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV102Linha, 172, 2), " ") == 0 ) )
            {
               if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) > 0 ) )
               {
                  Gx_msg = Gx_msg + "File is the Submission file, ignored" ;
                  context.msgStatus( Gx_msg );
                  AV110Error = (byte)(AV110Error+1) ;
               }
            }
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "99") == 0 ) )
         {
            if ( ( AV134Openb < 0 ) )
            {
               Gx_msg = Gx_msg + "File Trailer duplicated" ;
               if ( ( GXutil.strSearch( AV11DebugM, "SAYALL", 1) > 0 ) )
               {
                  context.msgStatus( Gx_msg );
               }
               AV110Error = (byte)(AV110Error+1) ;
            }
            AV134Openb = (int)(AV134Openb-1) ;
         }
         else
         {
            context.msgStatus( "  Warning at line"+AV105LineC+": record type ["+AV103RecTy+"] unknown" );
         }
         if ( ( AV110Error >= 30 ) )
         {
            Gx_msg = Gx_msg + "Too Many errors, aborting analisys" ;
            context.msgStatus( Gx_msg );
            if (true) break;
         }
      }
      if ( ( GXutil.strcmp(GXutil.substring( GXutil.trim( AV146Linha), 1, 2), "99") != 0 ) && ( AV106LineC > 0 ) )
      {
         Gx_msg = Gx_msg + "File Trailer Invalid" ;
         context.msgStatus( Gx_msg );
         AV145Erro_ = "S" ;
         AV110Error = (byte)(AV110Error+1) ;
      }
      if ( ( AV134Openb > 0 ) )
      {
         Gx_msg = Gx_msg + "File Trailer missing" ;
         context.msgStatus( Gx_msg );
         AV110Error = (byte)(AV110Error+1) ;
      }
      if ( ( AV134Openb < 0 ) )
      {
         Gx_msg = Gx_msg + "File Header missing" ;
         context.msgStatus( Gx_msg );
         AV110Error = (byte)(AV110Error+1) ;
      }
      if ( ( GXutil.strcmp(AV145Erro_, "S") == 0 ) )
      {
         /* Execute user subroutine: S141 */
         S141 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
   }

   public void S131( )
   {
      /* 'LOADRECORDS' Routine */
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfropen( AV100FileS, 250, "", "\"", "") ;
      AV110Error = (byte)(0) ;
      AV106LineC = 0 ;
      AV136TotRe = 0 ;
      AV137TotRe = 0 ;
      AV139TotRe = 0 ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char3[0] = AV102Linha ;
         GXt_int12 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char3, (short)(255)) ;
         AV102Linha = GXv_char3[0] ;
         AV15FileNo = GXt_int12 ;
         AV103RecTy = GXutil.trim( GXutil.upper( GXutil.substring( AV102Linha, 1, 2))) ;
         if ( ( GXutil.strcmp(AV103RecTy, "00") == 0 ) )
         {
            AV125Resum = GXutil.substring( AV102Linha, 11, 7) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV102Linha, 249, 1), "R") == 0 ) )
            {
               AV144Erro_ = "S" ;
            }
            else
            {
               AV144Erro_ = "N" ;
            }
         }
         else if ( ( GXutil.strcmp(GXutil.substring( AV102Linha, 1, 2), "01") == 0 ) )
         {
            AV118RetSt = GXutil.substring( AV102Linha, 172, 2) ;
            if ( ( GXutil.strcmp(AV118RetSt, "99") == 0 ) )
            {
               AV120ErrCo = GXutil.substring( AV102Linha, 212, 3) ;
            }
            else
            {
               AV120ErrCo = AV118RetSt ;
            }
            if ( ( GXutil.strcmp(AV118RetSt, "00") == 0 ) )
            {
               AV143Aceit = "Y" ;
               AV123lccbS = "RETOK" ;
            }
            else
            {
               AV143Aceit = "N" ;
               if ( ( GXutil.strcmp(AV144Erro_, "S") == 0 ) )
               {
               }
               else
               {
                  AV123lccbS = "RETNOK" ;
               }
            }
            AV114Refer = GXutil.trim( GXutil.substring( AV102Linha, 142, 30)) ;
            AV163GXLvl = (byte)(0) ;
            /* Using cursor P006R2 */
            pr_default.execute(0, new Object[] {AV114Refer});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1228lccbF = P006R2_A1228lccbF[0] ;
               A1227lccbO = P006R2_A1227lccbO[0] ;
               A1226lccbA = P006R2_A1226lccbA[0] ;
               A1225lccbC = P006R2_A1225lccbC[0] ;
               A1224lccbC = P006R2_A1224lccbC[0] ;
               A1223lccbD = P006R2_A1223lccbD[0] ;
               A1222lccbI = P006R2_A1222lccbI[0] ;
               A1150lccbE = P006R2_A1150lccbE[0] ;
               A1193lccbS = P006R2_A1193lccbS[0] ;
               n1193lccbS = P006R2_n1193lccbS[0] ;
               A1490Distr = P006R2_A1490Distr[0] ;
               n1490Distr = P006R2_n1490Distr[0] ;
               A1184lccbS = P006R2_A1184lccbS[0] ;
               n1184lccbS = P006R2_n1184lccbS[0] ;
               A1194lccbS = P006R2_A1194lccbS[0] ;
               n1194lccbS = P006R2_n1194lccbS[0] ;
               A1204lccbR = P006R2_A1204lccbR[0] ;
               n1204lccbR = P006R2_n1204lccbR[0] ;
               A1205lccbR = P006R2_A1205lccbR[0] ;
               n1205lccbR = P006R2_n1205lccbR[0] ;
               A1203lccbR = P006R2_A1203lccbR[0] ;
               n1203lccbR = P006R2_n1203lccbR[0] ;
               A1196lccbR = P006R2_A1196lccbR[0] ;
               n1196lccbR = P006R2_n1196lccbR[0] ;
               A1201lccbR = P006R2_A1201lccbR[0] ;
               n1201lccbR = P006R2_n1201lccbR[0] ;
               A1199lccbR = P006R2_A1199lccbR[0] ;
               n1199lccbR = P006R2_n1199lccbR[0] ;
               A1197lccbR = P006R2_A1197lccbR[0] ;
               n1197lccbR = P006R2_n1197lccbR[0] ;
               A1200lccbR = P006R2_A1200lccbR[0] ;
               n1200lccbR = P006R2_n1200lccbR[0] ;
               A1198lccbR = P006R2_A1198lccbR[0] ;
               n1198lccbR = P006R2_n1198lccbR[0] ;
               if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "C") == 0 ) )
               {
                  if ( ( GXutil.strcmp(A1193lccbS, AV114Refer) == 0 ) )
                  {
                     AV163GXLvl = (byte)(1) ;
                     if ( ( GXutil.strcmp(A1184lccbS, "PROCPLP") == 0 ) )
                     {
                        GXt_char11 = AV121ErrDe ;
                        GXv_char3[0] = AV118RetSt ;
                        GXv_svchar2[0] = AV120ErrCo ;
                        GXv_char13[0] = GXt_char11 ;
                        new pgeterrorcielo(remoteHandle, context).execute( GXv_char3, GXv_svchar2, GXv_char13) ;
                        pretrsubcielo.this.AV118RetSt = GXv_char3[0] ;
                        pretrsubcielo.this.AV120ErrCo = GXv_svchar2[0] ;
                        pretrsubcielo.this.GXt_char11 = GXv_char13[0] ;
                        AV121ErrDe = GXt_char11 ;
                        AV158Linha = (long)(AV158Linha+1) ;
                        context.msgStatus( "    "+GXutil.str( AV158Linha, 10, 0)+" "+AV121ErrDe );
                        if ( ( AV122TestM == 0 ) )
                        {
                           A1184lccbS = AV123lccbS ;
                           n1184lccbS = false ;
                           A1204lccbR = GXutil.substring( A1194lccbS, 1, 10) ;
                           n1204lccbR = false ;
                           /*
                              INSERT RECORD ON TABLE LCCBPLP1

                           */
                           A1229lccbS = GXutil.now(true, false) ;
                           A1186lccbS = GXutil.substring( AV123lccbS, 1, 8) ;
                           n1186lccbS = false ;
                           A1187lccbS = "Cielo - Retorno realizado - " + AV121ErrDe ;
                           n1187lccbS = false ;
                           /* Using cursor P006R3 */
                           pr_default.execute(1, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                           if ( (pr_default.getStatus(1) == 1) )
                           {
                              Gx_err = (short)(1) ;
                              Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                           }
                           else
                           {
                              Gx_err = (short)(0) ;
                              Gx_emsg = "" ;
                           }
                           /* End Insert */
                        }
                        if ( ( GXutil.strcmp(AV143Aceit, "N") == 0 ) )
                        {
                           if ( ( GXutil.strcmp(AV144Erro_, "S") == 0 ) )
                           {
                              A1205lccbR = "" ;
                              n1205lccbR = false ;
                              A1203lccbR = 0 ;
                              n1203lccbR = false ;
                              A1196lccbR = GXutil.nullDate() ;
                              n1196lccbR = false ;
                              A1201lccbR = 0 ;
                              n1201lccbR = false ;
                              A1199lccbR = "" ;
                              n1199lccbR = false ;
                              A1197lccbR = GXutil.resetTime( GXutil.nullDate() );
                              n1197lccbR = false ;
                              A1200lccbR = "" ;
                              n1200lccbR = false ;
                              A1198lccbR = "" ;
                              n1198lccbR = false ;
                           }
                           else
                           {
                              A1205lccbR = "" ;
                              n1205lccbR = false ;
                              A1203lccbR = 0 ;
                              n1203lccbR = false ;
                              A1196lccbR = Gx_date ;
                              n1196lccbR = false ;
                              A1201lccbR = (int)(GXutil.val( AV120ErrCo, ".")) ;
                              n1201lccbR = false ;
                              A1199lccbR = GXutil.substring( AV14FileNa, 1, 20) ;
                              n1199lccbR = false ;
                              A1197lccbR = GXutil.now(true, false) ;
                              n1197lccbR = false ;
                              A1200lccbR = GXutil.substring( AV114Refer, 1, 20) ;
                              n1200lccbR = false ;
                              A1198lccbR = "F" ;
                              n1198lccbR = false ;
                              AV133cntRe = (int)(AV133cntRe+1) ;
                           }
                        }
                        else
                        {
                           A1205lccbR = GXutil.substring( A1226lccbA, 1, 10) ;
                           n1205lccbR = false ;
                           A1203lccbR = (long)(GXutil.val( A1226lccbA, ".")) ;
                           n1203lccbR = false ;
                           A1196lccbR = Gx_date ;
                           n1196lccbR = false ;
                           A1201lccbR = 0 ;
                           n1201lccbR = false ;
                           A1199lccbR = GXutil.substring( AV14FileNa, 1, 20) ;
                           n1199lccbR = false ;
                           A1197lccbR = GXutil.now(true, false) ;
                           n1197lccbR = false ;
                           A1200lccbR = GXutil.substring( A1193lccbS, 1, 20) ;
                           n1200lccbR = false ;
                           A1198lccbR = "F" ;
                           n1198lccbR = false ;
                           AV132cntAc = (int)(AV132cntAc+1) ;
                        }
                        AV136TotRe = (int)(AV136TotRe+1) ;
                        if ( ( GXutil.strcmp(GXutil.substring( AV102Linha, 43, 1), "0") == 0 ) )
                        {
                           AV129cntCC = (int)(AV129cntCC+1) ;
                           AV128cntCC = (int)(AV128cntCC+1) ;
                        }
                        else
                        {
                           AV131cntPL = (int)(AV131cntPL+1) ;
                           AV130cntPL = (int)(AV130cntPL+1) ;
                        }
                     }
                     else
                     {
                        context.msgStatus( "  WARNING: Reference number ["+AV114Refer+"] at Wrong Status: "+A1184lccbS );
                        AV116cntWr = (int)(AV116cntWr+1) ;
                     }
                     /* Using cursor P006R4 */
                     pr_default.execute(2, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1204lccbR), A1204lccbR, new Boolean(n1205lccbR), A1205lccbR, new Boolean(n1203lccbR), new Long(A1203lccbR), new Boolean(n1196lccbR), A1196lccbR, new Boolean(n1201lccbR), new Integer(A1201lccbR), new Boolean(n1199lccbR), A1199lccbR, new Boolean(n1197lccbR), A1197lccbR, new Boolean(n1200lccbR), A1200lccbR, new Boolean(n1198lccbR), A1198lccbR, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                  }
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( ( AV163GXLvl == 0 ) )
            {
               context.msgStatus( "  WARNING: Reference number not found: ["+AV114Refer+"]" );
               AV115cntNo = (int)(AV115cntNo+1) ;
            }
         }
         else
         {
         }
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
      AV139TotRe = (int)(AV137TotRe+AV136TotRe) ;
   }

   public void S141( )
   {
      /* 'ENVIAREMAIL' Routine */
      GXt_char11 = AV152To ;
      GXv_char13[0] = GXt_char11 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_TO_ICSI", "S", "", "", GXv_char13) ;
      pretrsubcielo.this.GXt_char11 = GXv_char13[0] ;
      AV152To = GXt_char11 ;
      GXt_char11 = AV150CC ;
      GXv_char13[0] = GXt_char11 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_CC_ICSI", "S", "", "", GXv_char13) ;
      pretrsubcielo.this.GXt_char11 = GXv_char13[0] ;
      AV150CC = GXt_char11 ;
      AV148BCC = "" ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV147Anexo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GXt_char11 = AV151Subje ;
      GXv_char13[0] = GXt_char11 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_RET_INVALIDO", "S", "TIESS ICSI - RETORNO INVALIDO", "", GXv_char13) ;
      pretrsubcielo.this.GXt_char11 = GXv_char13[0] ;
      AV151Subje = GXt_char11 ;
      GXt_char11 = AV149Body ;
      GXv_char13[0] = GXt_char11 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_RET_BODY", "S", "<br>The return file [FILE] related to [ADM] is uncompleted.", "", GXv_char13) ;
      pretrsubcielo.this.GXt_char11 = GXv_char13[0] ;
      AV149Body = GXt_char11 ;
      AV149Body = GXutil.strReplace( AV149Body, "[FILE]", AV14FileNa) ;
      AV149Body = GXutil.strReplace( AV149Body, "[ADM]", "VI") ;
      context.msgStatus( "The return file "+GXutil.trim( AV14FileNa)+" related to VI is uncompleted " );
      new penviaemail(remoteHandle, context).execute( AV151Subje, AV149Body, AV152To, AV150CC, AV148BCC, AV147Anexo) ;
   }

   protected void cleanup( )
   {
      this.aP0[0] = pretrsubcielo.this.AV11DebugM;
      this.aP1[0] = pretrsubcielo.this.AV100FileS;
      Application.commit(context, remoteHandle, "DEFAULT", "pretrsubcielo");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV154Bande = "" ;
      AV155NovaB = "" ;
      AV156Bande = "" ;
      AV157NovaB = "" ;
      AV122TestM = (byte)(0) ;
      AV124Verbo = (byte)(0) ;
      AV9DataB = "" ;
      Gx_date = GXutil.nullDate() ;
      AV10DataC = "" ;
      AV28HoraA = "" ;
      AV29HoraB = "" ;
      returnInSub = false ;
      AV85Versao = "" ;
      AV14FileNa = "" ;
      AV110Error = (byte)(0) ;
      AV143Aceit = "" ;
      AV104n = 0 ;
      AV128cntCC = 0 ;
      AV129cntCC = 0 ;
      AV130cntPL = 0 ;
      AV131cntPL = 0 ;
      GXt_char4 = "" ;
      AV132cntAc = 0 ;
      GXt_svchar1 = "" ;
      GXt_char5 = "" ;
      GXt_char6 = "" ;
      AV133cntRe = 0 ;
      GXt_char7 = "" ;
      GXt_char8 = "" ;
      GXt_char9 = "" ;
      AV116cntWr = 0 ;
      GXt_char10 = "" ;
      AV115cntNo = 0 ;
      AV15FileNo = 0 ;
      AV106LineC = 0 ;
      AV134Openb = 0 ;
      AV107OpenB = 0 ;
      AV144Erro_ = "" ;
      AV145Erro_ = "" ;
      AV146Linha = "" ;
      AV102Linha = "" ;
      AV105LineC = "" ;
      Gx_msg = "" ;
      AV103RecTy = "" ;
      AV153RecTy = "" ;
      AV136TotRe = 0 ;
      AV137TotRe = 0 ;
      AV139TotRe = 0 ;
      GXt_int12 = (short)(0) ;
      AV125Resum = "" ;
      AV118RetSt = "" ;
      AV120ErrCo = "" ;
      AV123lccbS = "" ;
      AV114Refer = "" ;
      AV163GXLvl = (byte)(0) ;
      scmdbuf = "" ;
      P006R2_A1228lccbF = new String[] {""} ;
      P006R2_A1227lccbO = new String[] {""} ;
      P006R2_A1226lccbA = new String[] {""} ;
      P006R2_A1225lccbC = new String[] {""} ;
      P006R2_A1224lccbC = new String[] {""} ;
      P006R2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006R2_A1222lccbI = new String[] {""} ;
      P006R2_A1150lccbE = new String[] {""} ;
      P006R2_A1193lccbS = new String[] {""} ;
      P006R2_n1193lccbS = new boolean[] {false} ;
      P006R2_A1490Distr = new String[] {""} ;
      P006R2_n1490Distr = new boolean[] {false} ;
      P006R2_A1184lccbS = new String[] {""} ;
      P006R2_n1184lccbS = new boolean[] {false} ;
      P006R2_A1194lccbS = new String[] {""} ;
      P006R2_n1194lccbS = new boolean[] {false} ;
      P006R2_A1204lccbR = new String[] {""} ;
      P006R2_n1204lccbR = new boolean[] {false} ;
      P006R2_A1205lccbR = new String[] {""} ;
      P006R2_n1205lccbR = new boolean[] {false} ;
      P006R2_A1203lccbR = new long[1] ;
      P006R2_n1203lccbR = new boolean[] {false} ;
      P006R2_A1196lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006R2_n1196lccbR = new boolean[] {false} ;
      P006R2_A1201lccbR = new int[1] ;
      P006R2_n1201lccbR = new boolean[] {false} ;
      P006R2_A1199lccbR = new String[] {""} ;
      P006R2_n1199lccbR = new boolean[] {false} ;
      P006R2_A1197lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006R2_n1197lccbR = new boolean[] {false} ;
      P006R2_A1200lccbR = new String[] {""} ;
      P006R2_n1200lccbR = new boolean[] {false} ;
      P006R2_A1198lccbR = new String[] {""} ;
      P006R2_n1198lccbR = new boolean[] {false} ;
      A1228lccbF = "" ;
      A1227lccbO = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1150lccbE = "" ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1204lccbR = "" ;
      n1204lccbR = false ;
      A1205lccbR = "" ;
      n1205lccbR = false ;
      A1203lccbR = 0 ;
      n1203lccbR = false ;
      A1196lccbR = GXutil.nullDate() ;
      n1196lccbR = false ;
      A1201lccbR = 0 ;
      n1201lccbR = false ;
      A1199lccbR = "" ;
      n1199lccbR = false ;
      A1197lccbR = GXutil.resetTime( GXutil.nullDate() );
      n1197lccbR = false ;
      A1200lccbR = "" ;
      n1200lccbR = false ;
      A1198lccbR = "" ;
      n1198lccbR = false ;
      AV121ErrDe = "" ;
      GXv_char3 = new String [1] ;
      GXv_svchar2 = new String [1] ;
      AV158Linha = 0 ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV152To = "" ;
      AV150CC = "" ;
      AV148BCC = "" ;
      GX_I = 0 ;
      AV147Anexo = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV147Anexo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV151Subje = "" ;
      AV149Body = "" ;
      GXv_char13 = new String [1] ;
      GXt_char11 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretrsubcielo__default(),
         new Object[] {
             new Object[] {
            P006R2_A1228lccbF, P006R2_A1227lccbO, P006R2_A1226lccbA, P006R2_A1225lccbC, P006R2_A1224lccbC, P006R2_A1223lccbD, P006R2_A1222lccbI, P006R2_A1150lccbE, P006R2_A1193lccbS, P006R2_n1193lccbS,
            P006R2_A1490Distr, P006R2_n1490Distr, P006R2_A1184lccbS, P006R2_n1184lccbS, P006R2_A1194lccbS, P006R2_n1194lccbS, P006R2_A1204lccbR, P006R2_n1204lccbR, P006R2_A1205lccbR, P006R2_n1205lccbR,
            P006R2_A1203lccbR, P006R2_n1203lccbR, P006R2_A1196lccbR, P006R2_n1196lccbR, P006R2_A1201lccbR, P006R2_n1201lccbR, P006R2_A1199lccbR, P006R2_n1199lccbR, P006R2_A1197lccbR, P006R2_n1197lccbR,
            P006R2_A1200lccbR, P006R2_n1200lccbR, P006R2_A1198lccbR, P006R2_n1198lccbR
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV122TestM ;
   private byte AV124Verbo ;
   private byte AV110Error ;
   private byte AV163GXLvl ;
   private short GXt_int12 ;
   private short A1230lccbS ;
   private short Gx_err ;
   private int AV104n ;
   private int AV128cntCC ;
   private int AV129cntCC ;
   private int AV130cntPL ;
   private int AV131cntPL ;
   private int AV132cntAc ;
   private int AV133cntRe ;
   private int AV116cntWr ;
   private int AV115cntNo ;
   private int AV106LineC ;
   private int AV134Openb ;
   private int AV107OpenB ;
   private int AV136TotRe ;
   private int AV137TotRe ;
   private int AV139TotRe ;
   private int A1201lccbR ;
   private int GX_INS236 ;
   private int GX_I ;
   private long AV15FileNo ;
   private long A1203lccbR ;
   private long AV158Linha ;
   private String AV11DebugM ;
   private String AV100FileS ;
   private String AV9DataB ;
   private String AV10DataC ;
   private String AV28HoraA ;
   private String AV29HoraB ;
   private String AV85Versao ;
   private String AV14FileNa ;
   private String AV143Aceit ;
   private String GXt_char4 ;
   private String GXt_char5 ;
   private String GXt_char6 ;
   private String GXt_char7 ;
   private String GXt_char8 ;
   private String GXt_char9 ;
   private String GXt_char10 ;
   private String AV144Erro_ ;
   private String AV145Erro_ ;
   private String AV146Linha ;
   private String AV102Linha ;
   private String AV105LineC ;
   private String Gx_msg ;
   private String AV103RecTy ;
   private String AV153RecTy ;
   private String AV125Resum ;
   private String AV118RetSt ;
   private String AV120ErrCo ;
   private String AV123lccbS ;
   private String AV114Refer ;
   private String scmdbuf ;
   private String A1228lccbF ;
   private String A1227lccbO ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1150lccbE ;
   private String A1193lccbS ;
   private String A1490Distr ;
   private String A1184lccbS ;
   private String A1194lccbS ;
   private String A1204lccbR ;
   private String A1205lccbR ;
   private String A1199lccbR ;
   private String A1200lccbR ;
   private String A1198lccbR ;
   private String AV121ErrDe ;
   private String GXv_char3[] ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String GXv_char13[] ;
   private String GXt_char11 ;
   private java.util.Date A1197lccbR ;
   private java.util.Date A1229lccbS ;
   private java.util.Date Gx_date ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1196lccbR ;
   private boolean returnInSub ;
   private boolean n1193lccbS ;
   private boolean n1490Distr ;
   private boolean n1184lccbS ;
   private boolean n1194lccbS ;
   private boolean n1204lccbR ;
   private boolean n1205lccbR ;
   private boolean n1203lccbR ;
   private boolean n1196lccbR ;
   private boolean n1201lccbR ;
   private boolean n1199lccbR ;
   private boolean n1197lccbR ;
   private boolean n1200lccbR ;
   private boolean n1198lccbR ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private String AV154Bande ;
   private String AV155NovaB ;
   private String AV156Bande ;
   private String AV157NovaB ;
   private String GXt_svchar1 ;
   private String GXv_svchar2[] ;
   private String A1187lccbS ;
   private String AV152To ;
   private String AV150CC ;
   private String AV148BCC ;
   private String AV147Anexo[] ;
   private String AV151Subje ;
   private String AV149Body ;
   private String[] aP0 ;
   private String[] aP1 ;
   private IDataStoreProvider pr_default ;
   private String[] P006R2_A1228lccbF ;
   private String[] P006R2_A1227lccbO ;
   private String[] P006R2_A1226lccbA ;
   private String[] P006R2_A1225lccbC ;
   private String[] P006R2_A1224lccbC ;
   private java.util.Date[] P006R2_A1223lccbD ;
   private String[] P006R2_A1222lccbI ;
   private String[] P006R2_A1150lccbE ;
   private String[] P006R2_A1193lccbS ;
   private boolean[] P006R2_n1193lccbS ;
   private String[] P006R2_A1490Distr ;
   private boolean[] P006R2_n1490Distr ;
   private String[] P006R2_A1184lccbS ;
   private boolean[] P006R2_n1184lccbS ;
   private String[] P006R2_A1194lccbS ;
   private boolean[] P006R2_n1194lccbS ;
   private String[] P006R2_A1204lccbR ;
   private boolean[] P006R2_n1204lccbR ;
   private String[] P006R2_A1205lccbR ;
   private boolean[] P006R2_n1205lccbR ;
   private long[] P006R2_A1203lccbR ;
   private boolean[] P006R2_n1203lccbR ;
   private java.util.Date[] P006R2_A1196lccbR ;
   private boolean[] P006R2_n1196lccbR ;
   private int[] P006R2_A1201lccbR ;
   private boolean[] P006R2_n1201lccbR ;
   private String[] P006R2_A1199lccbR ;
   private boolean[] P006R2_n1199lccbR ;
   private java.util.Date[] P006R2_A1197lccbR ;
   private boolean[] P006R2_n1197lccbR ;
   private String[] P006R2_A1200lccbR ;
   private boolean[] P006R2_n1200lccbR ;
   private String[] P006R2_A1198lccbR ;
   private boolean[] P006R2_n1198lccbR ;
}

final  class pretrsubcielo__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006R2", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbSubTrn], [DistribuicaoTransacoesTipo], [lccbStatus], [lccbSubRO], [lccbRSubRO], [lccbRSubAppCode], [lccbRSubCCCF], [lccbRSubDate], [lccbRSubError], [lccbRSubFile], [lccbRSubTime], [lccbRSubTrn], [lccbRSubType] FROM [LCCBPLP] WITH (UPDLOCK) WHERE (SUBSTRING([DistribuicaoTransacoesTipo], 1, 1) = 'C') AND ([lccbSubTrn] = ?) ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006R3", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006R4", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbRSubRO]=?, [lccbRSubAppCode]=?, [lccbRSubCCCF]=?, [lccbRSubDate]=?, [lccbRSubError]=?, [lccbRSubFile]=?, [lccbRSubTime]=?, [lccbRSubTrn]=?, [lccbRSubType]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 20) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 8) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 10) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 10) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 10) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((long[]) buf[20])[0] = rslt.getLong(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[22])[0] = rslt.getGXDate(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((int[]) buf[24])[0] = rslt.getInt(17) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(18, 20) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[28])[0] = rslt.getGXDateTime(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 1) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 20);
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 10);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 10);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(4, ((Number) parms[7]).longValue());
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(5, (java.util.Date)parms[9]);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(6, ((Number) parms[11]).intValue());
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(8, (java.util.Date)parms[15], false);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 20);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 1);
               }
               stmt.setString(11, (String)parms[20], 3);
               stmt.setString(12, (String)parms[21], 7);
               stmt.setDate(13, (java.util.Date)parms[22]);
               stmt.setString(14, (String)parms[23], 2);
               stmt.setString(15, (String)parms[24], 44);
               stmt.setString(16, (String)parms[25], 20);
               stmt.setString(17, (String)parms[26], 1);
               stmt.setString(18, (String)parms[27], 19);
               break;
      }
   }

}


/*
               File: Company
        Description: Company
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:59.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tcompany extends GXTransaction
{
   public tcompany( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcompany.class ), "" );
   }

   public tcompany( int remoteHandle ,
                    ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey02181( )
   {
      A866Compan = "" ;
   }

   public void initAll02181( )
   {
      K865Compan = A865Compan ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey02181( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption020( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "Company" ;
   }

   protected String getFrmTitle( )
   {
      return "Company" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TCompany.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      tcompany.this.A865Compan = aP0[0];
      this.aP0 = aP0;
      tcompany.this.Gx_mode = aP1[0];
      this.aP1 = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtCompanyCod = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),161, 89, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 161 , 89 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A865Compan" );
      ((GXEdit) edtCompanyCod.getGXComponent()).setAlignment(ILabel.LEFT);
      edtCompanyCod.addFocusListener(this);
      edtCompanyCod.getGXComponent().setHelpId("HLP_TCompany.htm");
      edtCompanyDes = new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),161, 113, 290, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 161 , 113 , 290 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A866Compan" );
      ((GXEdit) edtCompanyDes.getGXComponent()).setAlignment(ILabel.LEFT);
      edtCompanyDes.addFocusListener(this);
      edtCompanyDes.getGXComponent().setHelpId("HLP_TCompany.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      bttbtt15 = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  393 ,  36 ,  90 ,  24 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttbtt15.setTooltip("Confirm");
      bttbtt15.addActionListener(this);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "Company", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 52 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "Company Description", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 120 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtCompanyCod ,
                edtCompanyDes ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttbtt15 ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      valid_Companycod();
      setFocus(edtCompanyDes, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey02181( ) ;
   }

   public void disable_std_buttons( )
   {
      bttBtn_first.setGXEnabled( 0 );
      bttBtn_prev.setGXEnabled( 0 );
      bttBtn_next.setGXEnabled( 0 );
      bttBtn_last.setGXEnabled( 0 );
      bttBtn_exit2.setGXEnabled( 0 );
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtCompanyCod.setEnabled( 0 );
         edtCompanyDes.setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll02181( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttbtt15.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtCompanyCod.isEventSource(eventSource) ) {
         setGXCursor( edtCompanyCod.getGXCursor() );
         return;
      }
      if ( edtCompanyDes.isEventSource(eventSource) ) {
         setGXCursor( edtCompanyDes.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtCompanyCod.isEventSource(eventSource) ) {
         valid_Companycod ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtCompanyCod.isEventSource(eventSource) ) {
         A865Compan = edtCompanyCod.getValue() ;
         return;
      }
      if ( edtCompanyDes.isEventSource(eventSource) ) {
         A866Compan = edtCompanyDes.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtCompanyCod.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A865Compan, edtCompanyCod.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtCompanyDes.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A866Compan, edtCompanyDes.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption020( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtCompanyDes.isEventSource(eventSource) )
      {
         edtCompanyDes.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void VariablesToControls( )
   {
      edtCompanyCod.setValue( A865Compan );
      edtCompanyDes.setValue( A866Compan );
   }

   protected void ControlsToVariables( )
   {
      A865Compan = edtCompanyCod.getValue() ;
      A866Compan = edtCompanyDes.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Companycod( )
   {
      if ( ( GXutil.strcmp(A865Compan, K865Compan) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K865Compan = A865Compan ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void e11022( )
   {
      eventLevelContext();
      /* 'Back' Routine */
      if (canCleanup()) {
         returnInSub = true;
      }
      if (true) return;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
         {
            sMode181 = Gx_mode ;
            Gx_mode = "UPD" ;
            Gx_mode = sMode181 ;
         }
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            getByPrimaryKey( ) ;
            if ( ( RcdFound181 != 1 ) )
            {
               pushError( localUtil.getMessages().getMessage("noinsert") );
               AnyError = (short)(1) ;
               keepFocus();
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm02181( int GX_JID )
   {
      if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z866Compan = T00023_A866Compan[0] ;
         }
         else
         {
            Z866Compan = A866Compan ;
         }
      }
      if ( ( GX_JID == -3 ) )
      {
         Z865Compan = A865Compan ;
         Z866Compan = A866Compan ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
      {
         edtCompanyCod.setEnabled( 0 );
      }
      else
      {
         edtCompanyCod.setEnabled( 1 );
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
      }
   }

   public void load02181( )
   {
      /* Using cursor T00024 */
      pr_default.execute(2, new Object[] {A865Compan});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound181 = (short)(1) ;
         A866Compan = T00024_A866Compan[0] ;
         zm02181( -3) ;
      }
      pr_default.close(2);
      onLoadActions02181( ) ;
   }

   public void onLoadActions02181( )
   {
   }

   public void checkExtendedTable02181( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors02181( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey02181( )
   {
      /* Using cursor T00025 */
      pr_default.execute(3, new Object[] {A865Compan});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound181 = (short)(1) ;
      }
      else
      {
         RcdFound181 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00023 */
      pr_default.execute(1, new Object[] {A865Compan});
      if ( (pr_default.getStatus(1) != 101) && ( GXutil.strcmp(T00023_A865Compan[0], A865Compan) == 0 ) )
      {
         zm02181( 3) ;
         RcdFound181 = (short)(1) ;
         A866Compan = T00023_A866Compan[0] ;
         Z865Compan = A865Compan ;
         sMode181 = Gx_mode ;
         Gx_mode = "DSP" ;
         load02181( ) ;
         Gx_mode = sMode181 ;
      }
      else
      {
         RcdFound181 = (short)(0) ;
         initializeNonKey02181( ) ;
         sMode181 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode181 ;
      }
      K865Compan = A865Compan ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey02181( ) ;
      if ( ( RcdFound181 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound181 = (short)(0) ;
      /* Using cursor T00026 */
      pr_default.execute(4, new Object[] {A865Compan});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( GXutil.strcmp(T00026_A865Compan[0], A865Compan) == 0 ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( GXutil.strcmp(T00026_A865Compan[0], A865Compan) == 0 ) )
         {
            RcdFound181 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound181 = (short)(0) ;
      /* Using cursor T00027 */
      pr_default.execute(5, new Object[] {A865Compan});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( GXutil.strcmp(T00027_A865Compan[0], A865Compan) == 0 ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( GXutil.strcmp(T00027_A865Compan[0], A865Compan) == 0 ) )
         {
            RcdFound181 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey02181( ) ;
      if ( ( RcdFound181 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtCompanyCod );
         }
         else if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtCompanyCod );
         }
         else
         {
            /* Update record */
            update02181( ) ;
            setNextFocus( edtCompanyCod );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
         {
            /* Insert record */
            insert02181( ) ;
            setNextFocus( edtCompanyCod );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtCompanyCod );
            }
            else
            {
               /* Insert record */
               insert02181( ) ;
               setNextFocus( edtCompanyCod );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
      {
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtCompanyCod );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtCompanyCod );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void checkOptimisticConcurrency02181( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T00022 */
         pr_default.execute(0, new Object[] {A865Compan});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"COMPANY"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z866Compan, T00022_A866Compan[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"COMPANY"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert02181( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm02181( 0) ;
         checkOptimisticConcurrency02181( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm02181( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert02181( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T00028 */
                  pr_default.execute(6, new Object[] {A865Compan, A866Compan});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load02181( ) ;
         }
         endLevel02181( ) ;
      }
      closeExtendedTableCursors02181( ) ;
   }

   public void update02181( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency02181( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm02181( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate02181( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T00029 */
                  pr_default.execute(7, new Object[] {A866Compan, A865Compan});
                  deferredUpdate02181( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel02181( ) ;
      }
      closeExtendedTableCursors02181( ) ;
   }

   public void deferredUpdate02181( )
   {
   }

   public void delete( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls02181( ) ;
         /* No cascading delete specified. */
         afterConfirm02181( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete02181( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T000210 */
               pr_default.execute(8, new Object[] {A865Compan});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     loopOnce = true;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode181 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel02181( ) ;
      Gx_mode = sMode181 ;
   }

   public void onDeleteControls02181( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor T000211 */
         pr_default.execute(9, new Object[] {A865Compan});
         if ( (pr_default.getStatus(9) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"File Downloads"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(9);
      }
   }

   public void endLevel02181( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete02181( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tcompany");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tcompany");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart02181( )
   {
      /* Using cursor T000212 */
      pr_default.execute(10, new Object[] {A865Compan});
      RcdFound181 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound181 = (short)(1) ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext02181( )
   {
      pr_default.readNext(10);
      RcdFound181 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound181 = (short)(1) ;
      }
   }

   public void scanEnd02181( )
   {
      pr_default.close(10);
   }

   public void afterConfirm02181( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert02181( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate02181( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete02181( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete02181( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate02181( )
   {
      /* Before Validate Rules */
   }

   public void confirm_020( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls02181( ) ;
         }
         else
         {
            checkExtendedTable02181( ) ;
            closeExtendedTableCursors02181( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_exit1.getBitmap() ;
         PreviousTooltip = bttBtn_exit1.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = tcompany.this.A865Compan;
      this.aP1[0] = tcompany.this.Gx_mode;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A866Compan = "" ;
      lastAnyError = 0 ;
      returnInSub = false ;
      sMode181 = "" ;
      RcdFound181 = (short)(0) ;
      Z866Compan = "" ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z865Compan = "" ;
      T00024_A865Compan = new String[] {""} ;
      T00024_A866Compan = new String[] {""} ;
      Gx_BScreen = (byte)(0) ;
      T00025_A865Compan = new String[] {""} ;
      T00023_A865Compan = new String[] {""} ;
      T00023_A866Compan = new String[] {""} ;
      T00026_A865Compan = new String[] {""} ;
      T00027_A865Compan = new String[] {""} ;
      T00022_A865Compan = new String[] {""} ;
      T00022_A866Compan = new String[] {""} ;
      T000211_A42Downloa = new long[1] ;
      T000212_A865Compan = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tcompany__default(),
         new Object[] {
             new Object[] {
            T00022_A865Compan, T00022_A866Compan
            }
            , new Object[] {
            T00023_A865Compan, T00023_A866Compan
            }
            , new Object[] {
            T00024_A865Compan, T00024_A866Compan
            }
            , new Object[] {
            T00025_A865Compan
            }
            , new Object[] {
            T00026_A865Compan
            }
            , new Object[] {
            T00027_A865Compan
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000211_A42Downloa
            }
            , new Object[] {
            T000212_A865Compan
            }
         }
      );
      reloadDynamicLists(0);
      K865Compan = "" ;
      edtCompanyCod.setValue(A865Compan);
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound181 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String sMode181 ;
   protected String scmdbuf ;
   protected boolean returnInSub ;
   protected String A866Compan ;
   protected String K865Compan ;
   protected String A865Compan ;
   protected String Z866Compan ;
   protected String Z865Compan ;
   protected String[] aP0 ;
   protected String[] aP1 ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtCompanyCod ;
   protected GUIObjectString edtCompanyDes ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected IGXButton bttbtt15 ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T00024_A865Compan ;
   protected String[] T00024_A866Compan ;
   protected String[] T00025_A865Compan ;
   protected String[] T00023_A865Compan ;
   protected String[] T00023_A866Compan ;
   protected String[] T00026_A865Compan ;
   protected String[] T00027_A865Compan ;
   protected String[] T00022_A865Compan ;
   protected String[] T00022_A866Compan ;
   protected long[] T000211_A42Downloa ;
   protected String[] T000212_A865Compan ;
}

final  class tcompany__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00022", "SELECT [CompanyCod], [CompanyDes] FROM [COMPANY] WITH (UPDLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00023", "SELECT [CompanyCod], [CompanyDes] FROM [COMPANY] WITH (NOLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00024", "SELECT TM1.[CompanyCod], TM1.[CompanyDes] FROM [COMPANY] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[CompanyCod] = ? ORDER BY TM1.[CompanyCod] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00025", "SELECT [CompanyCod] FROM [COMPANY] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00026", "SELECT TOP 1 [CompanyCod] FROM [COMPANY] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyCod] = ? ORDER BY [CompanyCod] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00027", "SELECT TOP 1 [CompanyCod] FROM [COMPANY] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyCod] = ? ORDER BY [CompanyCod] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T00028", "INSERT INTO [COMPANY] ([CompanyCod], [CompanyDes]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("T00029", "UPDATE [COMPANY] SET [CompanyDes]=?  WHERE [CompanyCod] = ?", GX_NOMASK)
         ,new UpdateCursor("T000210", "DELETE FROM [COMPANY]  WHERE [CompanyCod] = ?", GX_NOMASK)
         ,new ForEachCursor("T000211", "SELECT TOP 1 [DownloadID] FROM [DOWNLOAD] WITH (NOLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T000212", "SELECT [CompanyCod] FROM [COMPANY] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyCod] = ? ORDER BY [CompanyCod] ",true, GX_NOMASK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 40, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 40, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


/*
               File: RETi1022
        Description: Stub for RETi1022
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 18:43:4.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class preti1022 extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      preti1022 pgm = new preti1022 (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0);
   }

   public preti1022( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( preti1022.class ), "" );
   }

   public preti1022( int remoteHandle ,
                     ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      preti1022.this.AV2DebugMo = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new areti1022(remoteHandle, context).execute( aP0 );
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = preti1022.this.AV2DebugMo;
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV2DebugMo ;
   private String[] aP0 ;
}


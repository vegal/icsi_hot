/*
               File: Crypto
        Description: Crypto
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:57.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pcrypto extends GXProcedure
{
   public pcrypto( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pcrypto.class ), "" );
   }

   public pcrypto( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String[] aP2 )
   {
      pcrypto.this.AV9Atribut = aP0;
      pcrypto.this.AV8Acao = aP1;
      pcrypto.this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV14ParmKe ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PCI_KEY_CARD", "Chave de Encriptação", "S", "A", GXv_svchar2) ;
      pcrypto.this.GXt_char1 = GXv_svchar2[0] ;
      AV14ParmKe = GXt_char1 ;
      if ( ( GXutil.strcmp(AV14ParmKe, "A") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "B") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "C") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "D") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "E") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "F") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "G") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "H") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "I") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else if ( ( GXutil.strcmp(AV14ParmKe, "J") == 0 ) )
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      else
      {
         AV10Chave = "1a00f902f2df1b38968877e8514bcb56" ;
      }
      if ( ( GXutil.strcmp(AV8Acao, "E") == 0 ) )
      {
         AV13Result = AV9Atribut ;
         AV13Result = com.genexus.util.Encryption.encrypt64( AV13Result, AV10Chave) ;
      }
      if ( ( GXutil.strcmp(AV8Acao, "D") == 0 ) )
      {
         AV13Result = com.genexus.util.Encryption.decrypt64( AV9Atribut, AV10Chave) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP2[0] = pcrypto.this.AV13Result;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV13Result = "" ;
      AV14ParmKe = "" ;
      GXt_char1 = "" ;
      GXv_svchar2 = new String [1] ;
      AV10Chave = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV9Atribut ;
   private String AV8Acao ;
   private String AV13Result ;
   private String AV14ParmKe ;
   private String GXt_char1 ;
   private String AV10Chave ;
   private String GXv_svchar2[] ;
   private String[] aP2 ;
}


/*
               File: Mask
        Description: Mask
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pmask extends GXProcedure
{
   public pmask( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pmask.class ), "" );
   }

   public pmask( int remoteHandle ,
                 ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pmask.this.AV10CCnumO = aP0[0];
      this.aP0 = aP0;
      pmask.this.AV8CCmsk = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV9CCnum = AV10CCnumO ;
      /* Execute user subroutine: S113 */
      S113 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S113( )
   {
      /* 'MASCARA' Routine */
      AV11cont = 0 ;
      if ( ( GXutil.len( GXutil.trim( AV9CCnum)) > 12 ) )
      {
         AV9CCnum = GXutil.trim( AV9CCnum) ;
         while ( ( GXutil.strcmp(GXutil.substring( AV9CCnum, 1, 1), "0") == 0 ) )
         {
            AV11cont = GXutil.len( AV9CCnum) ;
            AV11cont = (int)(AV11cont-1) ;
            AV9CCnum = GXutil.right( GXutil.trim( AV9CCnum), AV11cont) ;
            AV9CCnum = GXutil.trim( AV9CCnum) ;
         }
         if ( ( AV11cont > 0 ) )
         {
            /* Execute user subroutine: S113 */
            S113 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV11cont = GXutil.len( AV9CCnum) ;
            if ( ( AV11cont < 17 ) )
            {
               AV8CCmsk = GXutil.substring( AV9CCnum, 1, 6) ;
               AV11cont = (int)(AV11cont-6) ;
               while ( ( AV11cont > 4 ) )
               {
                  AV8CCmsk = AV8CCmsk + "*" ;
                  AV11cont = (int)(AV11cont-1) ;
               }
               AV9CCnum = GXutil.strReplace( AV9CCnum, " ", "") ;
               AV8CCmsk = AV8CCmsk + GXutil.right( GXutil.trim( AV9CCnum), 4) ;
            }
            else
            {
               AV8CCmsk = "" ;
            }
         }
      }
      else
      {
         AV8CCmsk = "" ;
      }
   }

   protected void cleanup( )
   {
      this.aP0[0] = pmask.this.AV10CCnumO;
      this.aP1[0] = pmask.this.AV8CCmsk;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9CCnum = "" ;
      returnInSub = false ;
      AV11cont = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private int AV11cont ;
   private String AV10CCnumO ;
   private String AV8CCmsk ;
   private String AV9CCnum ;
   private boolean returnInSub ;
   private String[] aP0 ;
   private String[] aP1 ;
}


/*
               File: Gx0160
        Description: Selection List User Functions
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:15.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx0160 extends GXWorkpanel
{
   public wgx0160( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx0160.class ));
   }

   public wgx0160( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx0160" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List User Functions" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 490 ;
   }

   protected int getFrmHeight( )
   {
      return 234 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx0160.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      wgx0160.this.aP0 = aP0;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load06( ) throws GXLoadInterruptException
   {
      subwgx016006 = new subwgx016006 ();
      /* Using cursor W00102 */
      pr_default.execute(0);
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A47uspCode = W00102_A47uspCode[0] ;
         A51uspDesc = W00102_A51uspDesc[0] ;
         /* Execute user event: e11V102 */
         e11V102 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx0160_load06 extends GXLoadProducer
   {
      wgx0160 _sf ;

      public Gx0160_load06( wgx0160 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer06();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load06();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors06();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow06( )
   {
      return true;
   }

   public void autoRefresh_flow06( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( false ) || (!loadedFirstTime && ! isLoadAtStartup_flow06() )) {
         subfile.refresh();
         resetSubfileConditions_flow06() ;
      }
   }

   public boolean getSearch_flow06( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow06( )
   {
   }

   public void resetSearchConditions_flow06( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow06( )
   {
      return new subwgx016006 ();
   }

   public boolean getSearch_flow06( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow06( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow06( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow06( )
   {
      GXRefreshCommand06 ();
   }

   public final  class Gx0160_flow06 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx0160 _sf ;

      public Gx0160_flow06( wgx0160 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow06();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow06(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow06();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow06();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow06(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow06();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow06(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow06(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow06(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow06();
      }

   }

   protected void GXRefreshCommand06( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V102 */
      e12V102 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V102( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV5puspCod = A47uspCode ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer06( )
   {
      subwgx016006 oAux = subwgx016006 ;
      subwgx016006 = new subwgx016006 ();
      variablesToSubfile06 ();
      subGrd_1.addElement(subwgx016006);
      subwgx016006 = oAux;
   }

   private void e11V102( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors06( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 490 , 234 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx0160_load06(this), new Gx0160_flow06(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 288, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 287 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A51uspDesc" ), "User Function Description"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 287 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 24 , 343 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  382 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  382 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  382 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  382 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      focusManager.setControlList(new IFocusableControl[] {
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void variablesToSubfile06( )
   {
      subwgx016006.setuspDescription(A51uspDesc);
      subwgx016006.setuspCode(A47uspCode);
   }

   protected void subfileToVariables06( )
   {
      A51uspDesc = subwgx016006.getuspDescription();
      A47uspCode = subwgx016006.getuspCode();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx016006 = ( subwgx016006 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx016006 = new subwgx016006 ();
      }
      subfileToVariables06 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile06 ();
      subGrd_1.refreshLineValue(subwgx016006);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx016006 = ( subwgx016006 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx016006 = new subwgx016006 ();
      }
      subfileToVariables06 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V102 */
         e12V102 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V102 */
         e12V102 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V102 */
         e12V102 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = wgx0160.this.AV5puspCod;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV5puspCod = "" ;
      subwgx016006 = new subwgx016006();
      scmdbuf = "" ;
      W00102_A47uspCode = new String[] {""} ;
      W00102_A51uspDesc = new String[] {""} ;
      A47uspCode = "" ;
      A51uspDesc = "" ;
      gxIsRefreshing = false ;
      returnInSub = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx0160__default(),
         new Object[] {
             new Object[] {
            W00102_A47uspCode, W00102_A51uspDesc
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String AV5puspCod ;
   protected String A47uspCode ;
   protected String A51uspDesc ;
   protected String[] aP0 ;
   protected subwgx016006 subwgx016006 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00102_A47uspCode ;
   protected String[] W00102_A51uspDesc ;
   protected GXPanel GXPanel1 ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
}

final  class wgx0160__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00102", "SELECT [uspCode], [uspDescription] FROM [USERFUNCTIONS] WITH (FASTFIRSTROW NOLOCK) ORDER BY [uspCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

}


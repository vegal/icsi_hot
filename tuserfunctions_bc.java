/*
               File: tuserfunctions_bc
        Description: User Functions
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:14.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tuserfunctions_bc extends GXWebPanel implements IGxSilentTrn
{
   public tuserfunctions_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tuserfunctions_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tuserfunctions_bc.class ));
   }

   public tuserfunctions_bc( int remoteHandle ,
                             ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z47uspCode = A47uspCode ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_0D0( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls0D42( ) ;
         }
         else
         {
            checkExtendedTable0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors0D42( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues0D0( ) ;
      }
   }

   public void zm0D42( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         Z51uspDesc = A51uspDesc ;
      }
      if ( ( GX_JID == -1 ) )
      {
         Z47uspCode = A47uspCode ;
         Z51uspDesc = A51uspDesc ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load0D42( )
   {
      /* Using cursor BC000D4 */
      pr_default.execute(2, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound42 = (short)(1) ;
         A51uspDesc = BC000D4_A51uspDesc[0] ;
         zm0D42( -1) ;
      }
      pr_default.close(2);
      onLoadActions0D42( ) ;
   }

   public void onLoadActions0D42( )
   {
   }

   public void checkExtendedTable0D42( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors0D42( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey0D42( )
   {
      /* Using cursor BC000D5 */
      pr_default.execute(3, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound42 = (short)(1) ;
      }
      else
      {
         RcdFound42 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC000D3 */
      pr_default.execute(1, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0D42( 1) ;
         RcdFound42 = (short)(1) ;
         A47uspCode = BC000D3_A47uspCode[0] ;
         A51uspDesc = BC000D3_A51uspDesc[0] ;
         Z47uspCode = A47uspCode ;
         sMode42 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load0D42( ) ;
         Gx_mode = sMode42 ;
      }
      else
      {
         RcdFound42 = (short)(0) ;
         initializeNonKey0D42( ) ;
         sMode42 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode42 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey0D42( ) ;
      if ( ( RcdFound42 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_0D0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency0D42( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC000D2 */
         pr_default.execute(0, new Object[] {A47uspCode});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"USERFUNCTIONS"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z51uspDesc, BC000D2_A51uspDesc[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"USERFUNCTIONS"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0D42( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0D42( 0) ;
         checkOptimisticConcurrency0D42( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0D42( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000D6 */
                  pr_default.execute(4, new Object[] {A47uspCode, A51uspDesc});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0D42( ) ;
         }
         endLevel0D42( ) ;
      }
      closeExtendedTableCursors0D42( ) ;
   }

   public void update0D42( )
   {
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0D42( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0D42( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000D7 */
                  pr_default.execute(5, new Object[] {A51uspDesc, A47uspCode});
                  deferredUpdate0D42( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0D42( ) ;
      }
      closeExtendedTableCursors0D42( ) ;
   }

   public void deferredUpdate0D42( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0D42( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0D42( ) ;
         /* No cascading delete specified. */
         afterConfirm0D42( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0D42( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000D8 */
               pr_default.execute(6, new Object[] {A47uspCode});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode42 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0D42( ) ;
      Gx_mode = sMode42 ;
   }

   public void onDeleteControls0D42( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC000D9 */
         pr_default.execute(7, new Object[] {A47uspCode});
         if ( (pr_default.getStatus(7) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Level1"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(7);
      }
   }

   public void endLevel0D42( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete0D42( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues0D0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0D42( )
   {
      /* Using cursor BC000D10 */
      pr_default.execute(8, new Object[] {A47uspCode});
      RcdFound42 = (short)(0) ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound42 = (short)(1) ;
         A47uspCode = BC000D10_A47uspCode[0] ;
         A51uspDesc = BC000D10_A51uspDesc[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0D42( )
   {
      pr_default.readNext(8);
      RcdFound42 = (short)(0) ;
      scanLoad0D42( ) ;
   }

   public void scanLoad0D42( )
   {
      sMode42 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound42 = (short)(1) ;
         A47uspCode = BC000D10_A47uspCode[0] ;
         A51uspDesc = BC000D10_A51uspDesc[0] ;
      }
      Gx_mode = sMode42 ;
   }

   public void scanEnd0D42( )
   {
      pr_default.close(8);
   }

   public void afterConfirm0D42( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0D42( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0D42( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0D42( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0D42( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0D42( )
   {
      /* Before Validate Rules */
   }

   public void addRow0D42( )
   {
      VarsToRow42( bcUserFunctions) ;
   }

   public void sendRow0D42( )
   {
   }

   public void readRow0D42( )
   {
      RowToVars42( bcUserFunctions, 0) ;
   }

   public void confirmValues0D0( )
   {
   }

   public void initializeNonKey0D42( )
   {
      A51uspDesc = "" ;
   }

   public void initAll0D42( )
   {
      A47uspCode = "" ;
      initializeNonKey0D42( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow42( SdtUserFunctions obj42 )
   {
      obj42.setgxTv_SdtUserFunctions_Mode( Gx_mode );
      obj42.setgxTv_SdtUserFunctions_Uspdescription( A51uspDesc );
      obj42.setgxTv_SdtUserFunctions_Uspcode( A47uspCode );
      obj42.setgxTv_SdtUserFunctions_Uspcode_Z( Z47uspCode );
      obj42.setgxTv_SdtUserFunctions_Uspdescription_Z( Z51uspDesc );
      obj42.setgxTv_SdtUserFunctions_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars42( SdtUserFunctions obj42 ,
                            int forceLoad )
   {
      Gx_mode = obj42.getgxTv_SdtUserFunctions_Mode() ;
      A51uspDesc = obj42.getgxTv_SdtUserFunctions_Uspdescription() ;
      A47uspCode = obj42.getgxTv_SdtUserFunctions_Uspcode() ;
      Z47uspCode = obj42.getgxTv_SdtUserFunctions_Uspcode_Z() ;
      Z51uspDesc = obj42.getgxTv_SdtUserFunctions_Uspdescription_Z() ;
      Gx_mode = obj42.getgxTv_SdtUserFunctions_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A47uspCode = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey0D42( ) ;
      scanStart0D42( ) ;
      if ( ( RcdFound42 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z47uspCode = A47uspCode ;
      }
      onLoadActions0D42( ) ;
      zm0D42( 0) ;
      addRow0D42( ) ;
      scanEnd0D42( ) ;
      if ( ( RcdFound42 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars42( bcUserFunctions, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey0D42( ) ;
      if ( ( RcdFound42 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
         {
            A47uspCode = Z47uspCode ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update0D42( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert0D42( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert0D42( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow42( bcUserFunctions) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars42( bcUserFunctions, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey0D42( ) ;
      if ( ( RcdFound42 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
         {
            A47uspCode = Z47uspCode ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A47uspCode, Z47uspCode) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tuserfunctions_bc");
      VarsToRow42( bcUserFunctions) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcUserFunctions.getgxTv_SdtUserFunctions_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcUserFunctions.setgxTv_SdtUserFunctions_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtUserFunctions sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcUserFunctions ) )
      {
         bcUserFunctions = sdt ;
         if ( ( GXutil.strcmp(bcUserFunctions.getgxTv_SdtUserFunctions_Mode(), "") == 0 ) )
         {
            bcUserFunctions.setgxTv_SdtUserFunctions_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow42( bcUserFunctions) ;
         }
         else
         {
            RowToVars42( bcUserFunctions, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcUserFunctions.getgxTv_SdtUserFunctions_Mode(), "") == 0 ) )
         {
            bcUserFunctions.setgxTv_SdtUserFunctions_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars42( bcUserFunctions, 1) ;
      return  ;
   }

   public SdtUserFunctions getUserFunctions_BC( )
   {
      return bcUserFunctions ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z47uspCode = "" ;
      A47uspCode = "" ;
      gxTv_SdtUserFunctions_Uspcode_Z = "" ;
      gxTv_SdtUserFunctions_Uspdescription_Z = "" ;
      GX_JID = 0 ;
      Z51uspDesc = "" ;
      A51uspDesc = "" ;
      BC000D4_A47uspCode = new String[] {""} ;
      BC000D4_A51uspDesc = new String[] {""} ;
      RcdFound42 = (short)(0) ;
      BC000D5_A47uspCode = new String[] {""} ;
      BC000D3_A47uspCode = new String[] {""} ;
      BC000D3_A51uspDesc = new String[] {""} ;
      sMode42 = "" ;
      BC000D2_A47uspCode = new String[] {""} ;
      BC000D2_A51uspDesc = new String[] {""} ;
      BC000D9_A64ustCode = new String[] {""} ;
      BC000D9_A47uspCode = new String[] {""} ;
      BC000D10_A47uspCode = new String[] {""} ;
      BC000D10_A51uspDesc = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tuserfunctions_bc__default(),
         new Object[] {
             new Object[] {
            BC000D2_A47uspCode, BC000D2_A51uspDesc
            }
            , new Object[] {
            BC000D3_A47uspCode, BC000D3_A51uspDesc
            }
            , new Object[] {
            BC000D4_A47uspCode, BC000D4_A51uspDesc
            }
            , new Object[] {
            BC000D5_A47uspCode
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000D9_A64ustCode, BC000D9_A47uspCode
            }
            , new Object[] {
            BC000D10_A47uspCode, BC000D10_A51uspDesc
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound42 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode42 ;
   private String Z47uspCode ;
   private String A47uspCode ;
   private String gxTv_SdtUserFunctions_Uspcode_Z ;
   private String gxTv_SdtUserFunctions_Uspdescription_Z ;
   private String Z51uspDesc ;
   private String A51uspDesc ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtUserFunctions bcUserFunctions ;
   private IDataStoreProvider pr_default ;
   private String[] BC000D4_A47uspCode ;
   private String[] BC000D4_A51uspDesc ;
   private String[] BC000D5_A47uspCode ;
   private String[] BC000D3_A47uspCode ;
   private String[] BC000D3_A51uspDesc ;
   private String[] BC000D2_A47uspCode ;
   private String[] BC000D2_A51uspDesc ;
   private String[] BC000D9_A64ustCode ;
   private String[] BC000D9_A47uspCode ;
   private String[] BC000D10_A47uspCode ;
   private String[] BC000D10_A51uspDesc ;
}

final  class tuserfunctions_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC000D2", "SELECT [uspCode], [uspDescription] FROM [USERFUNCTIONS] WITH (UPDLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000D3", "SELECT [uspCode], [uspDescription] FROM [USERFUNCTIONS] WITH (NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000D4", "SELECT TM1.[uspCode], TM1.[uspDescription] FROM [USERFUNCTIONS] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[uspCode] = ? ORDER BY TM1.[uspCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000D5", "SELECT [uspCode] FROM [USERFUNCTIONS] WITH (FASTFIRSTROW NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000D6", "INSERT INTO [USERFUNCTIONS] ([uspCode], [uspDescription]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000D7", "UPDATE [USERFUNCTIONS] SET [uspDescription]=?  WHERE [uspCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000D8", "DELETE FROM [USERFUNCTIONS]  WHERE [uspCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000D9", "SELECT TOP 1 [ustCode], [uspCode] FROM [USERPROFILESLEVEL1] WITH (NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000D10", "SELECT TM1.[uspCode], TM1.[uspDescription] FROM [USERFUNCTIONS] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[uspCode] = ? ORDER BY TM1.[uspCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 40, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 40, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


/*
               File: Wkp01
        Description: Wkp01
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:27.18
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class uwkp01 extends GXWorkpanel
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      uwkp01 pgm = new uwkp01 (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public uwkp01( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( uwkp01.class ));
   }

   public uwkp01( int remoteHandle ,
                  ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Wkp01" ;
   }

   protected String getFrmTitle( )
   {
      return "Wkp01" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 572 ;
   }

   protected int getFrmHeight( )
   {
      return 158 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WWkp01.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return true;
   }

   protected boolean isModal( )
   {
      return false;
   }

   protected boolean hasDBAccess( )
   {
      return false;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      start();
   }

   protected void standAlone( )
   {
      /* Execute user event: e11V0V2 */
      e11V0V2 ();
   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      standAlone();
      VariablesToControls();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V0V2 */
      e12V0V2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V0V2( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV6Atribut = GXutil.trim( AV5Cartao) ;
      GXv_char1[0] = AV7Resulta ;
      new pcrypto(remoteHandle, context).execute( AV6Atribut, "D", GXv_char1) ;
      uwkp01.this.AV7Resulta = GXv_char1[0] ;
      edtavResultado.setValue(AV7Resulta);
      eventLevelResetContext();
   }

   protected void nextLoad( )
   {
   }

   protected void e11V0V2( )
   {
      /* Load Routine */
      nextLoad();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 24 , 572 , 158 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtavCartao = new GUIObjectString ( new GXEdit(44, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),7, 21, 318, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 7 , 21 , 318 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5Cartao" );
      ((GXEdit) edtavCartao.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCartao.addFocusListener(this);
      edtavCartao.getGXComponent().setHelpId("HLP_WWkp01.htm");
      edtavResultado = new GUIObjectString ( new GXEdit(44, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),13, 110, 318, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 13 , 110 , 318 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV7Resulta" );
      ((GXEdit) edtavResultado.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavResultado.addFocusListener(this);
      edtavResultado.getGXComponent().setHelpId("HLP_WWkp01.htm");
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  392 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  392 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  392 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  392 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      focusManager.setControlList(new IFocusableControl[] {
                edtavCartao ,
                edtavResultado ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtavCartao, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void VariablesToControls( )
   {
      edtavCartao.setValue( AV5Cartao );
      edtavResultado.setValue( AV7Resulta );
   }

   protected void ControlsToVariables( )
   {
      AV5Cartao = edtavCartao.getValue() ;
      AV7Resulta = edtavResultado.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V0V2 */
         e12V0V2 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavCartao.isEventSource(eventSource) ) {
         setGXCursor( edtavCartao.getGXCursor() );
         return;
      }
      if ( edtavResultado.isEventSource(eventSource) ) {
         setGXCursor( edtavResultado.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavCartao.isEventSource(eventSource) ) {
         AV5Cartao = edtavCartao.getValue() ;
         return;
      }
      if ( edtavResultado.isEventSource(eventSource) ) {
         AV7Resulta = edtavResultado.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V0V2 */
         e12V0V2 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(wwkp01.class);
      return new GXcfg();
   }
*/
   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
      if  (array.equals("GXv_char1"))  {
      }
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      returnInSub = false ;
      AV6Atribut = "" ;
      AV5Cartao = "" ;
      AV7Resulta = "" ;
      GXv_char1 = new String [1] ;
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String AV6Atribut ;
   protected String AV5Cartao ;
   protected String AV7Resulta ;
   protected String GXv_char1[] ;
   protected boolean returnInSub ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtavCartao ;
   protected GUIObjectString edtavResultado ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtCountry_ContactTypes extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCountry_ContactTypes( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCountry_ContactTypes.class));
   }

   public SdtCountry_ContactTypes( int remoteHandle ,
                                   ModelContext context )
   {
      super( context, "SdtCountry_ContactTypes");
      initialize( remoteHandle) ;
   }

   public SdtCountry_ContactTypes( int remoteHandle ,
                                   StructSdtCountry_ContactTypes struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtCountry_ContactTypes( )
   {
      super( new ModelContext(SdtCountry_ContactTypes.class), "SdtCountry_ContactTypes");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Contacttypescode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttSeq") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttseq = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttName") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttname = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttPhone") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttphone = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttEmail") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttemail = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Contacttypesdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttSeq_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttseq_Z = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttName_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttname_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttPhone_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttphone_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCttEmail_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Isocttemail_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Country.ContactTypes" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ContactTypesCode", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Contacttypescode));
      oWriter.writeElement("ISOCttSeq", GXutil.trim( GXutil.str( gxTv_SdtCountry_ContactTypes_Isocttseq, 4, 0)));
      oWriter.writeElement("ISOCttName", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Isocttname));
      oWriter.writeElement("ISOCttPhone", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Isocttphone));
      oWriter.writeElement("ISOCttEmail", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Isocttemail));
      oWriter.writeElement("ContactTypesDescription", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Contacttypesdescription));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtCountry_ContactTypes_Modified, 4, 0)));
      oWriter.writeElement("ContactTypesCode_Z", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Contacttypescode_Z));
      oWriter.writeElement("ISOCttSeq_Z", GXutil.trim( GXutil.str( gxTv_SdtCountry_ContactTypes_Isocttseq_Z, 4, 0)));
      oWriter.writeElement("ISOCttName_Z", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Isocttname_Z));
      oWriter.writeElement("ISOCttPhone_Z", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Isocttphone_Z));
      oWriter.writeElement("ISOCttEmail_Z", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Isocttemail_Z));
      oWriter.writeElement("ContactTypesDescription_Z", GXutil.rtrim( gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Contacttypescode( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypescode ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypescode( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypescode_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode = "" ;
      return  ;
   }

   public short getgxTv_SdtCountry_ContactTypes_Isocttseq( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttseq ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttseq( short value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttseq = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttseq_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttseq = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Isocttname( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttname ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttname( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttname = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttname_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttname = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Isocttphone( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttphone ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttphone( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttphone = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttphone_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttphone = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Isocttemail( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttemail ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttemail( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttemail = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttemail_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttemail = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Contacttypesdescription( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypesdescription ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypesdescription( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypesdescription_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Mode( )
   {
      return gxTv_SdtCountry_ContactTypes_Mode ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Mode( String value )
   {
      gxTv_SdtCountry_ContactTypes_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Mode_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtCountry_ContactTypes_Modified( )
   {
      return gxTv_SdtCountry_ContactTypes_Modified ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Modified( short value )
   {
      gxTv_SdtCountry_ContactTypes_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Modified_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Contacttypescode_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypescode_Z ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypescode_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypescode_Z_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = "" ;
      return  ;
   }

   public short getgxTv_SdtCountry_ContactTypes_Isocttseq_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttseq_Z ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttseq_Z( short value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttseq_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttseq_Z_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttseq_Z = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Isocttname_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttname_Z ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttname_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttname_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttname_Z_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttname_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Isocttphone_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttphone_Z ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttphone_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttphone_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttphone_Z_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttphone_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Isocttemail_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttemail_Z ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttemail_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttemail_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Isocttemail_Z_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Isocttemail_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z_SetNull( )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttseq = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Isocttname = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttphone = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttemail = "" ;
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription = "" ;
      gxTv_SdtCountry_ContactTypes_Mode = "" ;
      gxTv_SdtCountry_ContactTypes_Modified = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttseq_Z = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Isocttname_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttphone_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttemail_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char3 = "" ;
      return  ;
   }

   public SdtCountry_ContactTypes Clone( )
   {
      return (SdtCountry_ContactTypes)(clone()) ;
   }

   public void setStruct( StructSdtCountry_ContactTypes struct )
   {
      setgxTv_SdtCountry_ContactTypes_Contacttypescode(struct.getContacttypescode());
      setgxTv_SdtCountry_ContactTypes_Isocttseq(struct.getIsocttseq());
      setgxTv_SdtCountry_ContactTypes_Isocttname(struct.getIsocttname());
      setgxTv_SdtCountry_ContactTypes_Isocttphone(struct.getIsocttphone());
      setgxTv_SdtCountry_ContactTypes_Isocttemail(struct.getIsocttemail());
      setgxTv_SdtCountry_ContactTypes_Contacttypesdescription(struct.getContacttypesdescription());
      setgxTv_SdtCountry_ContactTypes_Mode(struct.getMode());
      setgxTv_SdtCountry_ContactTypes_Modified(struct.getModified());
      setgxTv_SdtCountry_ContactTypes_Contacttypescode_Z(struct.getContacttypescode_Z());
      setgxTv_SdtCountry_ContactTypes_Isocttseq_Z(struct.getIsocttseq_Z());
      setgxTv_SdtCountry_ContactTypes_Isocttname_Z(struct.getIsocttname_Z());
      setgxTv_SdtCountry_ContactTypes_Isocttphone_Z(struct.getIsocttphone_Z());
      setgxTv_SdtCountry_ContactTypes_Isocttemail_Z(struct.getIsocttemail_Z());
      setgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z(struct.getContacttypesdescription_Z());
   }

   public StructSdtCountry_ContactTypes getStruct( )
   {
      StructSdtCountry_ContactTypes struct = new StructSdtCountry_ContactTypes ();
      struct.setContacttypescode(getgxTv_SdtCountry_ContactTypes_Contacttypescode());
      struct.setIsocttseq(getgxTv_SdtCountry_ContactTypes_Isocttseq());
      struct.setIsocttname(getgxTv_SdtCountry_ContactTypes_Isocttname());
      struct.setIsocttphone(getgxTv_SdtCountry_ContactTypes_Isocttphone());
      struct.setIsocttemail(getgxTv_SdtCountry_ContactTypes_Isocttemail());
      struct.setContacttypesdescription(getgxTv_SdtCountry_ContactTypes_Contacttypesdescription());
      struct.setMode(getgxTv_SdtCountry_ContactTypes_Mode());
      struct.setModified(getgxTv_SdtCountry_ContactTypes_Modified());
      struct.setContacttypescode_Z(getgxTv_SdtCountry_ContactTypes_Contacttypescode_Z());
      struct.setIsocttseq_Z(getgxTv_SdtCountry_ContactTypes_Isocttseq_Z());
      struct.setIsocttname_Z(getgxTv_SdtCountry_ContactTypes_Isocttname_Z());
      struct.setIsocttphone_Z(getgxTv_SdtCountry_ContactTypes_Isocttphone_Z());
      struct.setIsocttemail_Z(getgxTv_SdtCountry_ContactTypes_Isocttemail_Z());
      struct.setContacttypesdescription_Z(getgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z());
      return struct ;
   }

   protected short gxTv_SdtCountry_ContactTypes_Isocttseq ;
   protected short gxTv_SdtCountry_ContactTypes_Modified ;
   protected short gxTv_SdtCountry_ContactTypes_Isocttseq_Z ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtCountry_ContactTypes_Mode ;
   protected String sTagName ;
   protected String GXt_char3 ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypescode ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttname ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttphone ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttemail ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypesdescription ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypescode_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttname_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttphone_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttemail_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z ;
}


/*
               File: RETi1012
        Description: Chama Relat�rio I1011 e I1012 (Main)
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 18:37:29.88
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class preti1012 extends GXProcedure
{
   public preti1012( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( preti1012.class ), "" );
   }

   public preti1012( int remoteHandle ,
                     ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      preti1012.this.AV41DebugM = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV54Versao = "00003" ;
      AV41DebugM = GXutil.trim( GXutil.upper( AV41DebugM)) ;
      GXt_char1 = AV81Path ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_1092", "", "F", "C:\\Temp\\ICSI\\R1092", GXv_svchar2) ;
      preti1012.this.GXt_char1 = GXv_svchar2[0] ;
      AV81Path = GXt_char1 ;
      if ( ( GXutil.strSearch( AV41DebugM, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "i1011/i1012 - Version "+AV54Versao );
         context.msgStatus( "  Running mode: ["+AV41DebugM+"] - Started at "+GXutil.time( ) );
         context.msgStatus( "  Creating i1011/i1012 files for "+AV10lccbEm );
         context.msgStatus( "    "+AV80Pathpd );
         context.msgStatus( "    "+AV82PathTx );
      }
      if ( ( GXutil.strSearch( AV41DebugM, "TESTMODE", 1) > 0 ) )
      {
         AV85TestMo = (byte)(1) ;
      }
      else
      {
         AV85TestMo = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV41DebugM, "SEPPIPE", 1) > 0 ) )
      {
         AV53Sep = "|" ;
      }
      else
      {
         AV53Sep = "" ;
      }
      AV45HoraA = GXutil.time( ) ;
      AV46HoraB = GXutil.substring( AV45HoraA, 1, 2) ;
      AV46HoraB = AV46HoraB + GXutil.substring( AV45HoraA, 4, 2) ;
      AV44DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) ;
      AV44DataB = AV44DataB + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) ;
      AV44DataB = AV44DataB + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      /* Using cursor P006J2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1147lccbE = P006J2_A1147lccbE[0] ;
         n1147lccbE = P006J2_n1147lccbE[0] ;
         A1150lccbE = P006J2_A1150lccbE[0] ;
         AV10lccbEm = A1150lccbE ;
         AV80Pathpd = AV81Path + "tamlccbgdsrj" + GXutil.substring( GXutil.trim( AV44DataB), 3, 6) + ".pdf" ;
         AV82PathTx = AV81Path + "tamlccbgdsrj" + GXutil.substring( GXutil.trim( AV44DataB), 3, 6) + ".txt" ;
         /* Execute user subroutine: S1167 */
         S1167 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            cleanup();
            if (true) return;
         }
         GXv_svchar2[0] = AV10lccbEm ;
         GXv_char7[0] = AV86EmpNom ;
         GXv_char8[0] = AV80Pathpd ;
         GXv_char9[0] = AV82PathTx ;
         GXv_char10[0] = AV44DataB ;
         GXv_char11[0] = AV41DebugM ;
         new preti1012rpt_v002(remoteHandle, context).execute( GXv_svchar2, GXv_char7, GXv_char8, GXv_char9, GXv_char10, GXv_char11) ;
         preti1012.this.AV10lccbEm = GXv_svchar2[0] ;
         preti1012.this.AV86EmpNom = GXv_char7[0] ;
         preti1012.this.AV80Pathpd = GXv_char8[0] ;
         preti1012.this.AV82PathTx = GXv_char9[0] ;
         preti1012.this.AV44DataB = GXv_char10[0] ;
         preti1012.this.AV41DebugM = GXv_char11[0] ;
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( ( GXutil.strSearch( AV41DebugM, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "T�rmino do processamento - Relat�rio I1011/i1012" );
      }
      cleanup();
   }

   public void S1167( )
   {
      /* 'GETEMPNOM' Routine */
      AV96GXLvl6 = (byte)(0) ;
      /* Using cursor P006J3 */
      pr_default.execute(1, new Object[] {AV10lccbEm});
      while ( (pr_default.getStatus(1) != 101) )
      {
         A1233EmpCo = P006J3_A1233EmpCo[0] ;
         A1234EmpNo = P006J3_A1234EmpNo[0] ;
         n1234EmpNo = P006J3_n1234EmpNo[0] ;
         AV96GXLvl6 = (byte)(1) ;
         AV86EmpNom = GXutil.trim( A1234EmpNo) ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(1);
      if ( ( AV96GXLvl6 == 0 ) )
      {
         AV86EmpNom = "???" ;
      }
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(preti1012.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = preti1012.this.AV41DebugM;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV54Versao = "" ;
      AV81Path = "" ;
      GXt_char1 = "" ;
      GXt_char3 = "" ;
      AV10lccbEm = "" ;
      GXt_char4 = "" ;
      AV80Pathpd = "" ;
      GXt_char5 = "" ;
      AV82PathTx = "" ;
      GXt_char6 = "" ;
      AV85TestMo = (byte)(0) ;
      AV53Sep = "" ;
      AV45HoraA = "" ;
      AV46HoraB = "" ;
      AV44DataB = "" ;
      Gx_date = GXutil.nullDate() ;
      scmdbuf = "" ;
      P006J2_A1147lccbE = new String[] {""} ;
      P006J2_n1147lccbE = new boolean[] {false} ;
      P006J2_A1150lccbE = new String[] {""} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      returnInSub = false ;
      GXv_svchar2 = new String [1] ;
      AV86EmpNom = "" ;
      GXv_char7 = new String [1] ;
      GXv_char8 = new String [1] ;
      GXv_char9 = new String [1] ;
      GXv_char10 = new String [1] ;
      GXv_char11 = new String [1] ;
      AV96GXLvl6 = (byte)(0) ;
      P006J3_A1233EmpCo = new String[] {""} ;
      P006J3_A1234EmpNo = new String[] {""} ;
      P006J3_n1234EmpNo = new boolean[] {false} ;
      A1233EmpCo = "" ;
      A1234EmpNo = "" ;
      n1234EmpNo = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new preti1012__default(),
         new Object[] {
             new Object[] {
            P006J2_A1147lccbE, P006J2_n1147lccbE, P006J2_A1150lccbE
            }
            , new Object[] {
            P006J3_A1233EmpCo, P006J3_A1234EmpNo, P006J3_n1234EmpNo
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV85TestMo ;
   private byte AV96GXLvl6 ;
   private short Gx_err ;
   private String AV41DebugM ;
   private String AV54Versao ;
   private String AV81Path ;
   private String GXt_char1 ;
   private String GXt_char3 ;
   private String AV10lccbEm ;
   private String GXt_char4 ;
   private String AV80Pathpd ;
   private String GXt_char5 ;
   private String AV82PathTx ;
   private String GXt_char6 ;
   private String AV53Sep ;
   private String AV45HoraA ;
   private String AV46HoraB ;
   private String AV44DataB ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String AV86EmpNom ;
   private String GXv_char7[] ;
   private String GXv_char8[] ;
   private String GXv_char9[] ;
   private String GXv_char10[] ;
   private String GXv_char11[] ;
   private String A1233EmpCo ;
   private String A1234EmpNo ;
   private java.util.Date Gx_date ;
   private boolean n1147lccbE ;
   private boolean returnInSub ;
   private boolean n1234EmpNo ;
   private String GXv_svchar2[] ;
   private String[] aP0 ;
   private IDataStoreProvider pr_default ;
   private String[] P006J2_A1147lccbE ;
   private boolean[] P006J2_n1147lccbE ;
   private String[] P006J2_A1150lccbE ;
   private String[] P006J3_A1233EmpCo ;
   private String[] P006J3_A1234EmpNo ;
   private boolean[] P006J3_n1234EmpNo ;
}

final  class preti1012__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006J2", "SELECT [lccbEmpEnab], [lccbEmpCod] FROM [LCCBEMP] WITH (NOLOCK) WHERE [lccbEmpEnab] = '1' ORDER BY [lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006J3", "SELECT [EmpCod], [EmpNom] FROM [EMPRESAS] WITH (NOLOCK) WHERE [EmpCod] = ? ORDER BY [EmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               break;
      }
   }

}


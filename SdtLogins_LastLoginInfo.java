import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtLogins_LastLoginInfo extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtLogins_LastLoginInfo( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtLogins_LastLoginInfo.class));
   }

   public SdtLogins_LastLoginInfo( int remoteHandle ,
                                   ModelContext context )
   {
      super( context, "SdtLogins_LastLoginInfo");
      initialize( remoteHandle) ;
   }

   public SdtLogins_LastLoginInfo( int remoteHandle ,
                                   StructSdtLogins_LastLoginInfo struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtLogins_LastLoginInfo( )
   {
      super( new ModelContext(SdtLogins_LastLoginInfo.class), "SdtLogins_LastLoginInfo");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastPwd") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastPwdDate") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_LastLoginInfo_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_LastLoginInfo_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastPwd_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastPwdDate_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastPwdDate_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Logins.LastLoginInfo" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("lgnLastPwd", GXutil.rtrim( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd));
      if ( (GXutil.nullDate().equals(gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate)) )
      {
         oWriter.writeElement("lgnLastPwdDate", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("lgnLastPwdDate", sDateCnv);
      }
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtLogins_LastLoginInfo_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtLogins_LastLoginInfo_Modified, 4, 0)));
      oWriter.writeElement("lgnLastPwd_Z", GXutil.rtrim( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z));
      if ( (GXutil.nullDate().equals(gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z)) )
      {
         oWriter.writeElement("lgnLastPwdDate_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("lgnLastPwdDate_Z", sDateCnv);
      }
      oWriter.writeElement("lgnLastPwdDate_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd( String value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate( java.util.Date value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(0) ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(1) ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = GXutil.nullDate() ;
      return  ;
   }

   public String getgxTv_SdtLogins_LastLoginInfo_Mode( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Mode ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Mode( String value )
   {
      gxTv_SdtLogins_LastLoginInfo_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Mode_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtLogins_LastLoginInfo_Modified( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Modified ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Modified( short value )
   {
      gxTv_SdtLogins_LastLoginInfo_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Modified_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z( String value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z( java.util.Date value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = GXutil.nullDate() ;
      return  ;
   }

   public byte getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N( byte value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N_SetNull( )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd = "" ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = GXutil.nullDate() ;
      gxTv_SdtLogins_LastLoginInfo_Mode = "" ;
      gxTv_SdtLogins_LastLoginInfo_Modified = (short)(0) ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = "" ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = GXutil.nullDate() ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      sDateCnv = "" ;
      sNumToPad = "" ;
      return  ;
   }

   public SdtLogins_LastLoginInfo Clone( )
   {
      return (SdtLogins_LastLoginInfo)(clone()) ;
   }

   public void setStruct( StructSdtLogins_LastLoginInfo struct )
   {
      setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd(struct.getLgnlastpwd());
      setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate(struct.getLgnlastpwddate());
      setgxTv_SdtLogins_LastLoginInfo_Mode(struct.getMode());
      setgxTv_SdtLogins_LastLoginInfo_Modified(struct.getModified());
      setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z(struct.getLgnlastpwd_Z());
      setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z(struct.getLgnlastpwddate_Z());
      setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N(struct.getLgnlastpwddate_N());
   }

   public StructSdtLogins_LastLoginInfo getStruct( )
   {
      StructSdtLogins_LastLoginInfo struct = new StructSdtLogins_LastLoginInfo ();
      struct.setLgnlastpwd(getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd());
      struct.setLgnlastpwddate(getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate());
      struct.setMode(getgxTv_SdtLogins_LastLoginInfo_Mode());
      struct.setModified(getgxTv_SdtLogins_LastLoginInfo_Modified());
      struct.setLgnlastpwd_Z(getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z());
      struct.setLgnlastpwddate_Z(getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z());
      struct.setLgnlastpwddate_N(getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N());
      return struct ;
   }

   protected byte gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N ;
   protected short gxTv_SdtLogins_LastLoginInfo_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtLogins_LastLoginInfo_Mode ;
   protected String sTagName ;
   protected String GXt_char2 ;
   protected String sDateCnv ;
   protected String sNumToPad ;
   protected java.util.Date gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate ;
   protected java.util.Date gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z ;
   protected String gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd ;
   protected String gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z ;
}


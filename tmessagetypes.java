/*
               File: MessageTypes
        Description: Message Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:9.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tmessagetypes extends GXTransaction
{
   public tmessagetypes( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tmessagetypes.class ), "" );
   }

   public tmessagetypes( int remoteHandle ,
                         ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey0C19( )
   {
      A96MsgDesc = "" ;
      n96MsgDesc = false ;
      A868MSGSub = "" ;
      n868MSGSub = false ;
      A97MsgBody = "" ;
      n97MsgBody = false ;
      A98MsgStat = (byte)(0) ;
      n98MsgStat = false ;
   }

   public void initAll0C19( )
   {
      A100MsgCod = "" ;
      K100MsgCod = A100MsgCod ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey0C19( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption0C0( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "MessageTypes" ;
   }

   protected String getFrmTitle( )
   {
      return "Message Types" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return new GXMenuBarDefaultTRN(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TMessageTypes.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      tmessagetypes.this.AV11MsgCod = aP0[0];
      this.aP0 = aP0;
      tmessagetypes.this.Gx_mode = aP1[0];
      this.aP1 = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtMsgCode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),217, 89, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 217 , 89 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A100MsgCod" );
      ((GXEdit) edtMsgCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtMsgCode.addFocusListener(this);
      edtMsgCode.getGXComponent().setHelpId("HLP_TMessageTypes.htm");
      edtMsgDescription = new GUIObjectString ( new GXEdit(50, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),217, 113, 360, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 217 , 113 , 360 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A96MsgDesc" );
      ((GXEdit) edtMsgDescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtMsgDescription.addFocusListener(this);
      edtMsgDescription.getGXComponent().setHelpId("HLP_TMessageTypes.htm");
      edtMSGSubject = new GUIObjectString ( new GXEdit(50, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),217, 137, 360, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 217 , 137 , 360 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A868MSGSub" );
      ((GXEdit) edtMSGSubject.getGXComponent()).setAlignment(ILabel.LEFT);
      edtMSGSubject.addFocusListener(this);
      edtMSGSubject.getGXComponent().setHelpId("HLP_TMessageTypes.htm");
      edtMsgBody = new GUIObjectString ( new GXEdit(20480, "", UIFactory.getFont( "Courier New", 0, 9),217, 161, 430, 87, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.LONGVARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 217 , 161 , 430 , 87 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A97MsgBody" );
      ((GXEdit) edtMsgBody.getGXComponent()).setAlignment(ILabel.LEFT);
      edtMsgBody.addFocusListener(this);
      edtMsgBody.getGXComponent().setHelpId("HLP_TMessageTypes.htm");
      edtMsgStatus = new GUIObjectByte ( new GXEdit(1, "9", UIFactory.getFont( "Courier New", 0, 9),217, 257, 17, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 217 , 257 , 17 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A98MsgStat" );
      ((GXEdit) edtMsgStatus.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtMsgStatus.addFocusListener(this);
      edtMsgStatus.getGXComponent().setHelpId("HLP_TMessageTypes.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "C�digo da Mensagem", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 122 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "Descri��o da Mensagem", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 140 , 13 );
      lbllbl16 = UIFactory.getLabel(GXPanel1, "MSGSubject", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 137 , 71 , 13 );
      lbllbl18 = UIFactory.getLabel(GXPanel1, "Corpo da Mensagem (at� 20K)", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 161 , 172 , 13 );
      lbllbl20 = UIFactory.getLabel(GXPanel1, "Status (1=Enabled)", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 257 , 109 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtMsgCode ,
                edtMsgDescription ,
                edtMSGSubject ,
                edtMsgBody ,
                edtMsgStatus ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtMsgCode, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey0C19( ) ;
   }

   public void disable_std_buttons( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_first.setGXEnabled( 0 );
         bttBtn_prev.setGXEnabled( 0 );
         bttBtn_next.setGXEnabled( 0 );
         bttBtn_last.setGXEnabled( 0 );
         bttBtn_exit2.setGXEnabled( 0 );
      }
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtMsgCode.setEnabled( 0 );
         edtMsgDescription.setEnabled( 0 );
         edtMSGSubject.setEnabled( 0 );
         edtMsgBody.setEnabled( 0 );
         edtMsgStatus.setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll0C19( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_first.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_first( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_prev.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_previous( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_next.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_next( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_last.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_last( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_exit2.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_select( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtMsgCode.isEventSource(eventSource) ) {
         setGXCursor( edtMsgCode.getGXCursor() );
         return;
      }
      if ( edtMsgDescription.isEventSource(eventSource) ) {
         setGXCursor( edtMsgDescription.getGXCursor() );
         return;
      }
      if ( edtMSGSubject.isEventSource(eventSource) ) {
         setGXCursor( edtMSGSubject.getGXCursor() );
         return;
      }
      if ( edtMsgBody.isEventSource(eventSource) ) {
         setGXCursor( edtMsgBody.getGXCursor() );
         return;
      }
      if ( edtMsgStatus.isEventSource(eventSource) ) {
         setGXCursor( edtMsgStatus.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtMsgCode.isEventSource(eventSource) ) {
         valid_Msgcode ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtMsgCode.isEventSource(eventSource) ) {
         A100MsgCod = edtMsgCode.getValue() ;
         return;
      }
      if ( edtMsgDescription.isEventSource(eventSource) ) {
         A96MsgDesc = edtMsgDescription.getValue() ;
         if ( ( GXutil.strcmp(A96MsgDesc, "") != 0 ) )
         {
            n96MsgDesc = false ;
         }
         return;
      }
      if ( edtMSGSubject.isEventSource(eventSource) ) {
         A868MSGSub = edtMSGSubject.getValue() ;
         if ( ( GXutil.strcmp(A868MSGSub, "") != 0 ) )
         {
            n868MSGSub = false ;
         }
         return;
      }
      if ( edtMsgBody.isEventSource(eventSource) ) {
         A97MsgBody = edtMsgBody.getValue() ;
         if ( ( GXutil.strcmp(A97MsgBody, "") != 0 ) )
         {
            n97MsgBody = false ;
         }
         return;
      }
      if ( edtMsgStatus.isEventSource(eventSource) ) {
         A98MsgStat = edtMsgStatus.getValue() ;
         if ( ( A98MsgStat != 0 ) )
         {
            n98MsgStat = false ;
         }
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtMsgCode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A100MsgCod, edtMsgCode.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtMsgDescription.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A96MsgDesc, edtMsgDescription.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtMSGSubject.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A868MSGSub, edtMSGSubject.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtMsgBody.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A97MsgBody, edtMsgBody.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtMsgStatus.isEventSource(eventSource) ) && ( ( A98MsgStat != edtMsgStatus.getValue() ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption0C0( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtMsgDescription.isEventSource(eventSource) )
      {
         edtMsgDescription.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtMSGSubject.isEventSource(eventSource) )
      {
         edtMSGSubject.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtMsgBody.isEventSource(eventSource) )
      {
         edtMsgBody.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtMsgStatus.isEventSource(eventSource) )
      {
         edtMsgStatus.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void VariablesToControls( )
   {
      edtMsgCode.setValue( A100MsgCod );
      edtMsgDescription.setValue( A96MsgDesc );
      edtMSGSubject.setValue( A868MSGSub );
      edtMsgBody.setValue( A97MsgBody );
      edtMsgStatus.setValue( A98MsgStat );
   }

   protected void ControlsToVariables( )
   {
      A100MsgCod = edtMsgCode.getValue() ;
      A96MsgDesc = edtMsgDescription.getValue() ;
      n96MsgDesc = false ;
      if ( ( GXutil.strcmp(A96MsgDesc, "") != 0 ) )
      {
         n96MsgDesc = false ;
      }
      A868MSGSub = edtMSGSubject.getValue() ;
      n868MSGSub = false ;
      if ( ( GXutil.strcmp(A868MSGSub, "") != 0 ) )
      {
         n868MSGSub = false ;
      }
      A97MsgBody = edtMsgBody.getValue() ;
      n97MsgBody = false ;
      if ( ( GXutil.strcmp(A97MsgBody, "") != 0 ) )
      {
         n97MsgBody = false ;
      }
      A98MsgStat = edtMsgStatus.getValue() ;
      n98MsgStat = false ;
      if ( ( A98MsgStat != 0 ) )
      {
         n98MsgStat = false ;
      }
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Msgcode( )
   {
      if ( ( GXutil.strcmp(A100MsgCod, K100MsgCod) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K100MsgCod = A100MsgCod ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void e110C2( )
   {
      eventLevelContext();
      /* 'Back' Routine */
      if (canCleanup()) {
         returnInSub = true;
      }
      if (true) return;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm0C19( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z96MsgDesc = T000C3_A96MsgDesc[0] ;
            Z868MSGSub = T000C3_A868MSGSub[0] ;
            Z98MsgStat = T000C3_A98MsgStat[0] ;
         }
         else
         {
            Z96MsgDesc = A96MsgDesc ;
            Z868MSGSub = A868MSGSub ;
            Z98MsgStat = A98MsgStat ;
         }
      }
      if ( ( GX_JID == -1 ) )
      {
         Z100MsgCod = A100MsgCod ;
         Z96MsgDesc = A96MsgDesc ;
         Z868MSGSub = A868MSGSub ;
         Z97MsgBody = A97MsgBody ;
         Z98MsgStat = A98MsgStat ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
      }
   }

   public void load0C19( )
   {
      /* Using cursor T000C4 */
      pr_default.execute(2, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound19 = (short)(1) ;
         A96MsgDesc = T000C4_A96MsgDesc[0] ;
         n96MsgDesc = T000C4_n96MsgDesc[0] ;
         A868MSGSub = T000C4_A868MSGSub[0] ;
         n868MSGSub = T000C4_n868MSGSub[0] ;
         A97MsgBody = T000C4_A97MsgBody[0] ;
         n97MsgBody = T000C4_n97MsgBody[0] ;
         A98MsgStat = T000C4_A98MsgStat[0] ;
         n98MsgStat = T000C4_n98MsgStat[0] ;
         zm0C19( -1) ;
      }
      pr_default.close(2);
      onLoadActions0C19( ) ;
   }

   public void onLoadActions0C19( )
   {
   }

   public void checkExtendedTable0C19( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors0C19( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey0C19( )
   {
      /* Using cursor T000C5 */
      pr_default.execute(3, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound19 = (short)(1) ;
      }
      else
      {
         RcdFound19 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T000C3 */
      pr_default.execute(1, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0C19( 1) ;
         RcdFound19 = (short)(1) ;
         A100MsgCod = T000C3_A100MsgCod[0] ;
         A96MsgDesc = T000C3_A96MsgDesc[0] ;
         n96MsgDesc = T000C3_n96MsgDesc[0] ;
         A868MSGSub = T000C3_A868MSGSub[0] ;
         n868MSGSub = T000C3_n868MSGSub[0] ;
         A97MsgBody = T000C3_A97MsgBody[0] ;
         n97MsgBody = T000C3_n97MsgBody[0] ;
         A98MsgStat = T000C3_A98MsgStat[0] ;
         n98MsgStat = T000C3_n98MsgStat[0] ;
         Z100MsgCod = A100MsgCod ;
         sMode19 = Gx_mode ;
         Gx_mode = "DSP" ;
         load0C19( ) ;
         Gx_mode = sMode19 ;
      }
      else
      {
         RcdFound19 = (short)(0) ;
         initializeNonKey0C19( ) ;
         sMode19 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode19 ;
      }
      K100MsgCod = A100MsgCod ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey0C19( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound19 = (short)(0) ;
      /* Using cursor T000C6 */
      pr_default.execute(4, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T000C6_A100MsgCod[0], A100MsgCod) < 0 ) ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T000C6_A100MsgCod[0], A100MsgCod) > 0 ) ) )
         {
            A100MsgCod = T000C6_A100MsgCod[0] ;
            RcdFound19 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound19 = (short)(0) ;
      /* Using cursor T000C7 */
      pr_default.execute(5, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T000C7_A100MsgCod[0], A100MsgCod) > 0 ) ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T000C7_A100MsgCod[0], A100MsgCod) < 0 ) ) )
         {
            A100MsgCod = T000C7_A100MsgCod[0] ;
            RcdFound19 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey0C19( ) ;
      if ( ( RcdFound19 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtMsgCode );
         }
         else if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
         {
            A100MsgCod = Z100MsgCod ;
            edtMsgCode.setValue(A100MsgCod);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtMsgCode );
         }
         else
         {
            /* Update record */
            update0C19( ) ;
            setNextFocus( edtMsgCode );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
         {
            /* Insert record */
            insert0C19( ) ;
            setNextFocus( edtMsgCode );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtMsgCode );
            }
            else
            {
               /* Insert record */
               insert0C19( ) ;
               setNextFocus( edtMsgCode );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
      {
         A100MsgCod = Z100MsgCod ;
         edtMsgCode.setValue(A100MsgCod);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtMsgCode );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtMsgCode );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      getByPrimaryKey( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         pushError( localUtil.getMessages().getMessage("keynfound") );
         AnyError = (short)(1) ;
         keepFocus();
      }
      setNextFocus( edtMsgDescription );
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart0C19( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
      }
      setNextFocus( edtMsgDescription );
      scanEnd0C19( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
      }
      setNextFocus( edtMsgDescription );
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
      }
      setNextFocus( edtMsgDescription );
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart0C19( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
         while ( ( RcdFound19 != 0 ) )
         {
            scanNext0C19( ) ;
         }
      }
      setNextFocus( edtMsgDescription );
      scanEnd0C19( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_select( )
   {
      GXv_svchar1[0] = A100MsgCod ;
      new wgx00j0(remoteHandle, context).execute( GXv_svchar1) ;
      tmessagetypes.this.A100MsgCod = GXv_svchar1[0] ;
      edtMsgCode.setValue(A100MsgCod);
      edtMsgCode.setValue( A100MsgCod );
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency0C19( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T000C2 */
         pr_default.execute(0, new Object[] {A100MsgCod});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"MESSAGETYPES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z96MsgDesc, T000C2_A96MsgDesc[0]) != 0 ) || ( GXutil.strcmp(Z868MSGSub, T000C2_A868MSGSub[0]) != 0 ) || ( Z98MsgStat != T000C2_A98MsgStat[0] ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"MESSAGETYPES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert0C19( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0C19( 0) ;
         checkOptimisticConcurrency0C19( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0C19( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T000C8 */
                  pr_default.execute(6, new Object[] {A100MsgCod, new Boolean(n96MsgDesc), A96MsgDesc, new Boolean(n868MSGSub), A868MSGSub, new Boolean(n97MsgBody), A97MsgBody, new Boolean(n98MsgStat), new Byte(A98MsgStat)});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                        resetCaption0C0( ) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load0C19( ) ;
         }
         endLevel0C19( ) ;
      }
      closeExtendedTableCursors0C19( ) ;
   }

   public void update0C19( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0C19( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0C19( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T000C9 */
                  pr_default.execute(7, new Object[] {new Boolean(n96MsgDesc), A96MsgDesc, new Boolean(n868MSGSub), A868MSGSub, new Boolean(n97MsgBody), A97MsgBody, new Boolean(n98MsgStat), new Byte(A98MsgStat), A100MsgCod});
                  deferredUpdate0C19( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        context.msgStatus( localUtil.getMessages().getMessage("sucupdated") );
                        resetCaption0C0( ) ;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel0C19( ) ;
      }
      closeExtendedTableCursors0C19( ) ;
   }

   public void deferredUpdate0C19( )
   {
   }

   public void delete( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0C19( ) ;
         /* No cascading delete specified. */
         afterConfirm0C19( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T000C10 */
               pr_default.execute(8, new Object[] {A100MsgCod});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     move_next( ) ;
                     if ( ( RcdFound19 == 0 ) )
                     {
                        initAll0C19( ) ;
                     }
                     else
                     {
                        getByPrimaryKey( ) ;
                     }
                     context.msgStatus( localUtil.getMessages().getMessage("sucdeleted") );
                     resetCaption0C0( ) ;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode19 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0C19( ) ;
      Gx_mode = sMode19 ;
   }

   public void onDeleteControls0C19( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel0C19( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete0C19( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tmessagetypes");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tmessagetypes");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart0C19( )
   {
      /* Using cursor T000C11 */
      pr_default.execute(9);
      RcdFound19 = (short)(0) ;
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound19 = (short)(1) ;
         A100MsgCod = T000C11_A100MsgCod[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0C19( )
   {
      pr_default.readNext(9);
      RcdFound19 = (short)(0) ;
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound19 = (short)(1) ;
         A100MsgCod = T000C11_A100MsgCod[0] ;
      }
   }

   public void scanEnd0C19( )
   {
      pr_default.close(9);
   }

   public void afterConfirm0C19( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0C19( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0C19( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0C19( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0C19( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0C19( )
   {
      /* Before Validate Rules */
   }

   public void confirm_0C0( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls0C19( ) ;
         }
         else
         {
            checkExtendedTable0C19( ) ;
            closeExtendedTableCursors0C19( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_exit1.getBitmap() ;
         PreviousTooltip = bttBtn_exit1.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = tmessagetypes.this.AV11MsgCod;
      this.aP1[0] = tmessagetypes.this.Gx_mode;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A96MsgDesc = "" ;
      n96MsgDesc = false ;
      A868MSGSub = "" ;
      n868MSGSub = false ;
      A97MsgBody = "" ;
      n97MsgBody = false ;
      A98MsgStat = (byte)(0) ;
      n98MsgStat = false ;
      A100MsgCod = "" ;
      K100MsgCod = "" ;
      lastAnyError = 0 ;
      returnInSub = false ;
      Z96MsgDesc = "" ;
      Z868MSGSub = "" ;
      Z98MsgStat = (byte)(0) ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z100MsgCod = "" ;
      Z97MsgBody = "" ;
      T000C4_A100MsgCod = new String[] {""} ;
      T000C4_A96MsgDesc = new String[] {""} ;
      T000C4_n96MsgDesc = new boolean[] {false} ;
      T000C4_A868MSGSub = new String[] {""} ;
      T000C4_n868MSGSub = new boolean[] {false} ;
      T000C4_A97MsgBody = new String[] {""} ;
      T000C4_n97MsgBody = new boolean[] {false} ;
      T000C4_A98MsgStat = new byte[1] ;
      T000C4_n98MsgStat = new boolean[] {false} ;
      RcdFound19 = (short)(0) ;
      Gx_BScreen = (byte)(0) ;
      T000C5_A100MsgCod = new String[] {""} ;
      T000C3_A100MsgCod = new String[] {""} ;
      T000C3_A96MsgDesc = new String[] {""} ;
      T000C3_n96MsgDesc = new boolean[] {false} ;
      T000C3_A868MSGSub = new String[] {""} ;
      T000C3_n868MSGSub = new boolean[] {false} ;
      T000C3_A97MsgBody = new String[] {""} ;
      T000C3_n97MsgBody = new boolean[] {false} ;
      T000C3_A98MsgStat = new byte[1] ;
      T000C3_n98MsgStat = new boolean[] {false} ;
      sMode19 = "" ;
      T000C6_A100MsgCod = new String[] {""} ;
      T000C7_A100MsgCod = new String[] {""} ;
      GXv_svchar1 = new String [1] ;
      T000C2_A100MsgCod = new String[] {""} ;
      T000C2_A96MsgDesc = new String[] {""} ;
      T000C2_n96MsgDesc = new boolean[] {false} ;
      T000C2_A868MSGSub = new String[] {""} ;
      T000C2_n868MSGSub = new boolean[] {false} ;
      T000C2_A97MsgBody = new String[] {""} ;
      T000C2_n97MsgBody = new boolean[] {false} ;
      T000C2_A98MsgStat = new byte[1] ;
      T000C2_n98MsgStat = new boolean[] {false} ;
      T000C11_A100MsgCod = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tmessagetypes__default(),
         new Object[] {
             new Object[] {
            T000C2_A100MsgCod, T000C2_A96MsgDesc, T000C2_n96MsgDesc, T000C2_A868MSGSub, T000C2_n868MSGSub, T000C2_A97MsgBody, T000C2_n97MsgBody, T000C2_A98MsgStat, T000C2_n98MsgStat
            }
            , new Object[] {
            T000C3_A100MsgCod, T000C3_A96MsgDesc, T000C3_n96MsgDesc, T000C3_A868MSGSub, T000C3_n868MSGSub, T000C3_A97MsgBody, T000C3_n97MsgBody, T000C3_A98MsgStat, T000C3_n98MsgStat
            }
            , new Object[] {
            T000C4_A100MsgCod, T000C4_A96MsgDesc, T000C4_n96MsgDesc, T000C4_A868MSGSub, T000C4_n868MSGSub, T000C4_A97MsgBody, T000C4_n97MsgBody, T000C4_A98MsgStat, T000C4_n98MsgStat
            }
            , new Object[] {
            T000C5_A100MsgCod
            }
            , new Object[] {
            T000C6_A100MsgCod
            }
            , new Object[] {
            T000C7_A100MsgCod
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000C11_A100MsgCod
            }
         }
      );
      reloadDynamicLists(0);
   }

   protected byte nKeyPressed ;
   protected byte A98MsgStat ;
   protected byte geteqAfterKey= 1 ;
   protected byte Z98MsgStat ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound19 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String scmdbuf ;
   protected String sMode19 ;
   protected boolean n96MsgDesc ;
   protected boolean n868MSGSub ;
   protected boolean n97MsgBody ;
   protected boolean n98MsgStat ;
   protected boolean returnInSub ;
   protected String A97MsgBody ;
   protected String Z97MsgBody ;
   protected String A96MsgDesc ;
   protected String A868MSGSub ;
   protected String A100MsgCod ;
   protected String K100MsgCod ;
   protected String AV11MsgCod ;
   protected String Z96MsgDesc ;
   protected String Z868MSGSub ;
   protected String Z100MsgCod ;
   protected String GXv_svchar1[] ;
   protected String[] aP0 ;
   protected String[] aP1 ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtMsgCode ;
   protected GUIObjectString edtMsgDescription ;
   protected GUIObjectString edtMSGSubject ;
   protected GUIObjectString edtMsgBody ;
   protected GUIObjectByte edtMsgStatus ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected ILabel lbllbl16 ;
   protected ILabel lbllbl18 ;
   protected ILabel lbllbl20 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T000C4_A100MsgCod ;
   protected String[] T000C4_A96MsgDesc ;
   protected boolean[] T000C4_n96MsgDesc ;
   protected String[] T000C4_A868MSGSub ;
   protected boolean[] T000C4_n868MSGSub ;
   protected String[] T000C4_A97MsgBody ;
   protected boolean[] T000C4_n97MsgBody ;
   protected byte[] T000C4_A98MsgStat ;
   protected boolean[] T000C4_n98MsgStat ;
   protected String[] T000C5_A100MsgCod ;
   protected String[] T000C3_A100MsgCod ;
   protected String[] T000C3_A96MsgDesc ;
   protected boolean[] T000C3_n96MsgDesc ;
   protected String[] T000C3_A868MSGSub ;
   protected boolean[] T000C3_n868MSGSub ;
   protected String[] T000C3_A97MsgBody ;
   protected boolean[] T000C3_n97MsgBody ;
   protected byte[] T000C3_A98MsgStat ;
   protected boolean[] T000C3_n98MsgStat ;
   protected String[] T000C6_A100MsgCod ;
   protected String[] T000C7_A100MsgCod ;
   protected String[] T000C2_A100MsgCod ;
   protected String[] T000C2_A96MsgDesc ;
   protected boolean[] T000C2_n96MsgDesc ;
   protected String[] T000C2_A868MSGSub ;
   protected boolean[] T000C2_n868MSGSub ;
   protected String[] T000C2_A97MsgBody ;
   protected boolean[] T000C2_n97MsgBody ;
   protected byte[] T000C2_A98MsgStat ;
   protected boolean[] T000C2_n98MsgStat ;
   protected String[] T000C11_A100MsgCod ;
}

final  class tmessagetypes__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T000C2", "SELECT [MsgCode], [MsgDescription], [MSGSubject], [MsgBody], [MsgStatus] FROM [MESSAGETYPES] WITH (UPDLOCK) WHERE [MsgCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000C3", "SELECT [MsgCode], [MsgDescription], [MSGSubject], [MsgBody], [MsgStatus] FROM [MESSAGETYPES] WITH (NOLOCK) WHERE [MsgCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000C4", "SELECT TM1.[MsgCode], TM1.[MsgDescription], TM1.[MSGSubject], TM1.[MsgBody], TM1.[MsgStatus] FROM [MESSAGETYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[MsgCode] = ? ORDER BY TM1.[MsgCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000C5", "SELECT [MsgCode] FROM [MESSAGETYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [MsgCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T000C6", "SELECT TOP 1 [MsgCode] FROM [MESSAGETYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [MsgCode] > ?) ORDER BY [MsgCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T000C7", "SELECT TOP 1 [MsgCode] FROM [MESSAGETYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [MsgCode] < ?) ORDER BY [MsgCode] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T000C8", "INSERT INTO [MESSAGETYPES] ([MsgCode], [MsgDescription], [MSGSubject], [MsgBody], [MsgStatus]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000C9", "UPDATE [MESSAGETYPES] SET [MsgDescription]=?, [MSGSubject]=?, [MsgBody]=?, [MsgStatus]=?  WHERE [MsgCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T000C10", "DELETE FROM [MESSAGETYPES]  WHERE [MsgCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T000C11", "SELECT [MsgCode] FROM [MESSAGETYPES] WITH (FASTFIRSTROW NOLOCK) ORDER BY [MsgCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 50);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[4], 50);
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(4, (String)parms[6]);
               }
               if ( ((Boolean) parms[7]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(5, ((Number) parms[8]).byteValue());
               }
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 50);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 50);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(3, (String)parms[5]);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(4, ((Number) parms[7]).byteValue());
               }
               stmt.setVarchar(5, (String)parms[8], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


/*
               File: PeriodTypes
        Description: Period Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:12.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tperiodtypes extends GXTransaction
{
   public tperiodtypes( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tperiodtypes.class ), "" );
   }

   public tperiodtypes( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey2A225( )
   {
      A1116Perio = "" ;
      A1117Perio = "N" ;
   }

   public void initAll2A225( )
   {
      A387Period = "" ;
      K387Period = A387Period ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey2A225( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption2A0( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "PeriodTypes" ;
   }

   protected String getFrmTitle( )
   {
      return "Period Types" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TPeriodTypes.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String aP0 ,
                        String aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String aP1 )
   {
      tperiodtypes.this.AV12Period = aP0;
      tperiodtypes.this.Gx_mode = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtPeriodTypesCode = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 89, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 89 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A387Period" );
      ((GXEdit) edtPeriodTypesCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtPeriodTypesCode.addFocusListener(this);
      edtPeriodTypesCode.getGXComponent().setHelpId("HLP_TPeriodTypes.htm");
      edtPeriodTypesDescription = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 113, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 113 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1116Perio" );
      ((GXEdit) edtPeriodTypesDescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtPeriodTypesDescription.addFocusListener(this);
      edtPeriodTypesDescription.getGXComponent().setHelpId("HLP_TPeriodTypes.htm");
      chkPeriodTypesStatus = new GUIObjectString ( new GXCheckBox(GXPanel1, "Enabled ?" , "Y", "N") , GXPanel1 , 189 , 137 , 84 , 16 , Integer.MAX_VALUE , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1117Perio" );
      chkPeriodTypesStatus.addFocusListener(this);
      chkPeriodTypesStatus.addItemListener(this);
      chkPeriodTypesStatus.getGXComponent().setHelpId("HLP_TPeriodTypes.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "Period Types Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 108 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "Period Types Description", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 143 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtPeriodTypesCode ,
                edtPeriodTypesDescription ,
                chkPeriodTypesStatus ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtPeriodTypesCode, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey2A225( ) ;
   }

   public void disable_std_buttons( )
   {
      bttBtn_first.setGXEnabled( 0 );
      bttBtn_prev.setGXEnabled( 0 );
      bttBtn_next.setGXEnabled( 0 );
      bttBtn_last.setGXEnabled( 0 );
      bttBtn_exit2.setGXEnabled( 0 );
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtPeriodTypesCode.setEnabled( 0 );
         edtPeriodTypesDescription.setEnabled( 0 );
         chkPeriodTypesStatus.setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         /* Execute user event: e112A2 */
         e112A2 ();
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll2A225( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
      /* Execute user event: e122A2 */
      e122A2 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtPeriodTypesCode.isEventSource(eventSource) ) {
         setGXCursor( edtPeriodTypesCode.getGXCursor() );
         return;
      }
      if ( edtPeriodTypesDescription.isEventSource(eventSource) ) {
         setGXCursor( edtPeriodTypesDescription.getGXCursor() );
         return;
      }
      if ( chkPeriodTypesStatus.isEventSource(eventSource) ) {
         setGXCursor( chkPeriodTypesStatus.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtPeriodTypesCode.isEventSource(eventSource) ) {
         valid_Periodtypescode ();
         return;
      }
      if ( edtPeriodTypesDescription.isEventSource(eventSource) ) {
         valid_Periodtypesdescription ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtPeriodTypesCode.isEventSource(eventSource) ) {
         A387Period = edtPeriodTypesCode.getValue() ;
         return;
      }
      if ( edtPeriodTypesDescription.isEventSource(eventSource) ) {
         A1116Perio = edtPeriodTypesDescription.getValue() ;
         return;
      }
      if ( chkPeriodTypesStatus.isEventSource(eventSource) ) {
         A1117Perio = chkPeriodTypesStatus.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtPeriodTypesCode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A387Period, edtPeriodTypesCode.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtPeriodTypesDescription.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1116Perio, edtPeriodTypesDescription.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( chkPeriodTypesStatus.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1117Perio, chkPeriodTypesStatus.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption2A0( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtPeriodTypesCode.isEventSource(eventSource) )
      {
         edtPeriodTypesCode.setEnabled(!( ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ) ));
      }
      if ( edtPeriodTypesDescription.isEventSource(eventSource) )
      {
         edtPeriodTypesDescription.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( chkPeriodTypesStatus.isEventSource(eventSource) )
      {
         chkPeriodTypesStatus.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void VariablesToControls( )
   {
      edtPeriodTypesCode.setValue( A387Period );
      edtPeriodTypesDescription.setValue( A1116Perio );
      chkPeriodTypesStatus.setValue( A1117Perio );
   }

   protected void ControlsToVariables( )
   {
      A387Period = edtPeriodTypesCode.getValue() ;
      A1116Perio = edtPeriodTypesDescription.getValue() ;
      A1117Perio = chkPeriodTypesStatus.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Periodtypescode( )
   {
      if ( ( GXutil.strcmp(A387Period, K387Period) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K387Period = A387Period ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A387Period), "") == 0 ) )
         {
            GXutil.msg( me(), "Code can not be null" );
            AnyError = (short)(1) ;
            setNextFocus( edtPeriodTypesCode );
            setFocusNext();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void valid_Periodtypesdescription( )
   {
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A1116Perio), "") == 0 ) )
      {
         GXutil.msg( me(), "Description can not be null" );
         AnyError = (short)(1) ;
         setNextFocus( edtPeriodTypesDescription );
         setFocusNext();
      }
   }

   public void e122A2( )
   {
      eventNoLevelContext();
      /* Start Routine */
      AV14LgnLog = AV11Sessio.getValue("LOGGED") ;
      if ( ( GXutil.strcmp(AV14LgnLog, "") == 0 ) )
      {
      }
   }

   public void e132A2( )
   {
      eventLevelContext();
      /* 'Back' Routine */
   }

   public void e112A2( )
   {
      /* After Trn Routine */
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
      }
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
         {
            sMode225 = Gx_mode ;
            Gx_mode = "UPD" ;
            Gx_mode = sMode225 ;
         }
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            getByPrimaryKey( ) ;
            if ( ( RcdFound225 != 1 ) )
            {
               pushError( localUtil.getMessages().getMessage("noinsert") );
               AnyError = (short)(1) ;
               keepFocus();
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm2A225( int GX_JID )
   {
      if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1116Perio = T002A3_A1116Perio[0] ;
            Z1117Perio = T002A3_A1117Perio[0] ;
         }
         else
         {
            Z1116Perio = A1116Perio ;
            Z1117Perio = A1117Perio ;
         }
      }
      if ( ( GX_JID == -6 ) )
      {
         Z387Period = A387Period ;
         Z1116Perio = A1116Perio ;
         Z1117Perio = A1117Perio ;
      }
   }

   public void standaloneNotModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         A387Period = AV12Period ;
         edtPeriodTypesCode.setValue(A387Period);
      }
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
      {
         edtPeriodTypesCode.setEnabled( 0 );
      }
      else
      {
         edtPeriodTypesCode.setEnabled( 1 );
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
      {
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
      }
   }

   public void load2A225( )
   {
      /* Using cursor T002A4 */
      pr_default.execute(2, new Object[] {A387Period});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound225 = (short)(1) ;
         A1116Perio = T002A4_A1116Perio[0] ;
         A1117Perio = T002A4_A1117Perio[0] ;
         zm2A225( -6) ;
      }
      pr_default.close(2);
      onLoadActions2A225( ) ;
   }

   public void onLoadActions2A225( )
   {
   }

   public void checkExtendedTable2A225( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A387Period), "") == 0 ) )
      {
         pushError( "Code can not be null" );
         AnyError = (short)(1) ;
         keepFocus();
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A1116Perio), "") == 0 ) )
      {
         pushError( "Description can not be null" );
         AnyError = (short)(1) ;
         keepFocus();
      }
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2A225( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey2A225( )
   {
      /* Using cursor T002A5 */
      pr_default.execute(3, new Object[] {A387Period});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound225 = (short)(1) ;
      }
      else
      {
         RcdFound225 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T002A3 */
      pr_default.execute(1, new Object[] {A387Period});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm2A225( 6) ;
         RcdFound225 = (short)(1) ;
         A387Period = T002A3_A387Period[0] ;
         A1116Perio = T002A3_A1116Perio[0] ;
         A1117Perio = T002A3_A1117Perio[0] ;
         Z387Period = A387Period ;
         sMode225 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2A225( ) ;
         Gx_mode = sMode225 ;
      }
      else
      {
         RcdFound225 = (short)(0) ;
         initializeNonKey2A225( ) ;
         sMode225 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode225 ;
      }
      K387Period = A387Period ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey2A225( ) ;
      if ( ( RcdFound225 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound225 = (short)(0) ;
      /* Using cursor T002A6 */
      pr_default.execute(4, new Object[] {A387Period});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T002A6_A387Period[0], A387Period) < 0 ) ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T002A6_A387Period[0], A387Period) > 0 ) ) )
         {
            A387Period = T002A6_A387Period[0] ;
            RcdFound225 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound225 = (short)(0) ;
      /* Using cursor T002A7 */
      pr_default.execute(5, new Object[] {A387Period});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T002A7_A387Period[0], A387Period) > 0 ) ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T002A7_A387Period[0], A387Period) < 0 ) ) )
         {
            A387Period = T002A7_A387Period[0] ;
            RcdFound225 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey2A225( ) ;
      if ( ( RcdFound225 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtPeriodTypesCode );
         }
         else if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
         {
            A387Period = Z387Period ;
            edtPeriodTypesCode.setValue(A387Period);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtPeriodTypesCode );
         }
         else
         {
            /* Update record */
            update2A225( ) ;
            setNextFocus( edtPeriodTypesCode );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
         {
            /* Insert record */
            insert2A225( ) ;
            setNextFocus( edtPeriodTypesCode );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtPeriodTypesCode );
            }
            else
            {
               /* Insert record */
               insert2A225( ) ;
               setNextFocus( edtPeriodTypesCode );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
      {
         A387Period = Z387Period ;
         edtPeriodTypesCode.setValue(A387Period);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtPeriodTypesCode );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtPeriodTypesCode );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void checkOptimisticConcurrency2A225( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002A2 */
         pr_default.execute(0, new Object[] {A387Period});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"PERIODTYPES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1116Perio, T002A2_A1116Perio[0]) != 0 ) || ( GXutil.strcmp(Z1117Perio, T002A2_A1117Perio[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"PERIODTYPES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2A225( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2A225( 0) ;
         checkOptimisticConcurrency2A225( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2A225( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002A8 */
                  pr_default.execute(6, new Object[] {A387Period, A1116Perio, A1117Perio});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                        resetCaption2A0( ) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2A225( ) ;
         }
         endLevel2A225( ) ;
      }
      closeExtendedTableCursors2A225( ) ;
   }

   public void update2A225( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2A225( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2A225( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002A9 */
                  pr_default.execute(7, new Object[] {A1116Perio, A1117Perio, A387Period});
                  deferredUpdate2A225( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2A225( ) ;
      }
      closeExtendedTableCursors2A225( ) ;
   }

   public void deferredUpdate2A225( )
   {
   }

   public void delete( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2A225( ) ;
         /* No cascading delete specified. */
         afterConfirm2A225( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T002A10 */
               pr_default.execute(8, new Object[] {A387Period});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     loopOnce = true;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode225 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2A225( ) ;
      Gx_mode = sMode225 ;
   }

   public void onDeleteControls2A225( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor T002A11 */
         pr_default.execute(9, new Object[] {A387Period});
         if ( (pr_default.getStatus(9) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"Period Configuration"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(9);
      }
   }

   public void endLevel2A225( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2A225( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tperiodtypes");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tperiodtypes");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart2A225( )
   {
      /* Using cursor T002A12 */
      pr_default.execute(10);
      RcdFound225 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound225 = (short)(1) ;
         A387Period = T002A12_A387Period[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2A225( )
   {
      pr_default.readNext(10);
      RcdFound225 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound225 = (short)(1) ;
         A387Period = T002A12_A387Period[0] ;
      }
   }

   public void scanEnd2A225( )
   {
      pr_default.close(10);
   }

   public void afterConfirm2A225( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2A225( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2A225( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2A225( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2A225( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2A225( )
   {
      /* Before Validate Rules */
   }

   public void confirm_2A0( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2A225( ) ;
         }
         else
         {
            checkExtendedTable2A225( ) ;
            closeExtendedTableCursors2A225( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_exit1.getBitmap() ;
         PreviousTooltip = bttBtn_exit1.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A1116Perio = "" ;
      A1117Perio = "" ;
      A387Period = "" ;
      K387Period = "" ;
      lastAnyError = 0 ;
      AV14LgnLog = "" ;
      AV11Sessio = null; //httpContext.getWebSession();
      sMode225 = "" ;
      RcdFound225 = (short)(0) ;
      Z1116Perio = "" ;
      Z1117Perio = "" ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z387Period = "" ;
      Gx_BScreen = (byte)(0) ;
      T002A4_A387Period = new String[] {""} ;
      T002A4_A1116Perio = new String[] {""} ;
      T002A4_A1117Perio = new String[] {""} ;
      T002A5_A387Period = new String[] {""} ;
      T002A3_A387Period = new String[] {""} ;
      T002A3_A1116Perio = new String[] {""} ;
      T002A3_A1117Perio = new String[] {""} ;
      T002A6_A387Period = new String[] {""} ;
      T002A7_A387Period = new String[] {""} ;
      T002A2_A387Period = new String[] {""} ;
      T002A2_A1116Perio = new String[] {""} ;
      T002A2_A1117Perio = new String[] {""} ;
      T002A11_A23ISOCod = new String[] {""} ;
      T002A11_A349CurCod = new String[] {""} ;
      T002A11_A387Period = new String[] {""} ;
      T002A11_A263PerID = new String[] {""} ;
      T002A12_A387Period = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tperiodtypes__default(),
         new Object[] {
             new Object[] {
            T002A2_A387Period, T002A2_A1116Perio, T002A2_A1117Perio
            }
            , new Object[] {
            T002A3_A387Period, T002A3_A1116Perio, T002A3_A1117Perio
            }
            , new Object[] {
            T002A4_A387Period, T002A4_A1116Perio, T002A4_A1117Perio
            }
            , new Object[] {
            T002A5_A387Period
            }
            , new Object[] {
            T002A6_A387Period
            }
            , new Object[] {
            T002A7_A387Period
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002A11_A23ISOCod, T002A11_A349CurCod, T002A11_A387Period, T002A11_A263PerID
            }
            , new Object[] {
            T002A12_A387Period
            }
         }
      );
      reloadDynamicLists(0);
      A1117Perio = "N" ;
      chkPeriodTypesStatus.setValue(A1117Perio);
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound225 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String A1117Perio ;
   protected String sMode225 ;
   protected String Z1117Perio ;
   protected String scmdbuf ;
   protected String A1116Perio ;
   protected String A387Period ;
   protected String K387Period ;
   protected String AV12Period ;
   protected String AV14LgnLog ;
   protected String Z1116Perio ;
   protected String Z387Period ;
   protected com.genexus.webpanels.WebSession AV11Sessio ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtPeriodTypesCode ;
   protected GUIObjectString edtPeriodTypesDescription ;
   protected GUIObjectString chkPeriodTypesStatus ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T002A4_A387Period ;
   protected String[] T002A4_A1116Perio ;
   protected String[] T002A4_A1117Perio ;
   protected String[] T002A5_A387Period ;
   protected String[] T002A3_A387Period ;
   protected String[] T002A3_A1116Perio ;
   protected String[] T002A3_A1117Perio ;
   protected String[] T002A6_A387Period ;
   protected String[] T002A7_A387Period ;
   protected String[] T002A2_A387Period ;
   protected String[] T002A2_A1116Perio ;
   protected String[] T002A2_A1117Perio ;
   protected String[] T002A11_A23ISOCod ;
   protected String[] T002A11_A349CurCod ;
   protected String[] T002A11_A387Period ;
   protected String[] T002A11_A263PerID ;
   protected String[] T002A12_A387Period ;
}

final  class tperiodtypes__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T002A2", "SELECT [PeriodTypesCode], [PeriodTypesDescription], [PeriodTypesStatus] FROM [PERIODTYPES] WITH (UPDLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002A3", "SELECT [PeriodTypesCode], [PeriodTypesDescription], [PeriodTypesStatus] FROM [PERIODTYPES] WITH (NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002A4", "SELECT TM1.[PeriodTypesCode], TM1.[PeriodTypesDescription], TM1.[PeriodTypesStatus] FROM [PERIODTYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[PeriodTypesCode] = ? ORDER BY TM1.[PeriodTypesCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002A5", "SELECT [PeriodTypesCode] FROM [PERIODTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002A6", "SELECT TOP 1 [PeriodTypesCode] FROM [PERIODTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [PeriodTypesCode] > ?) ORDER BY [PeriodTypesCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002A7", "SELECT TOP 1 [PeriodTypesCode] FROM [PERIODTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [PeriodTypesCode] < ?) ORDER BY [PeriodTypesCode] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T002A8", "INSERT INTO [PERIODTYPES] ([PeriodTypesCode], [PeriodTypesDescription], [PeriodTypesStatus]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002A9", "UPDATE [PERIODTYPES] SET [PeriodTypesDescription]=?, [PeriodTypesStatus]=?  WHERE [PeriodTypesCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T002A10", "DELETE FROM [PERIODTYPES]  WHERE [PeriodTypesCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T002A11", "SELECT TOP 1 [ISOCod], [CurCode], [PeriodTypesCode], [PerID] FROM [PERIODS] WITH (NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002A12", "SELECT [PeriodTypesCode] FROM [PERIODTYPES] WITH (FASTFIRSTROW NOLOCK) ORDER BY [PeriodTypesCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               stmt.setVarchar(2, (String)parms[1], 30, false);
               stmt.setString(3, (String)parms[2], 1);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setString(2, (String)parms[1], 1);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
      }
   }

}


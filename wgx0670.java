/*
               File: Gx0670
        Description: Selection List Contact Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:16.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx0670 extends GXWorkpanel
{
   public wgx0670( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx0670.class ));
   }

   public wgx0670( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx0670" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List Contact Types" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 539 ;
   }

   protected int getFrmHeight( )
   {
      return 282 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx0670.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      wgx0670.this.aP0 = aP0;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load10( ) throws GXLoadInterruptException
   {
      subwgx067010 = new subwgx067010 ();
      lV5cContac = GXutil.padr( GXutil.rtrim( AV5cContac), (short)(5), "%") ;
      lV6cContac = GXutil.padr( GXutil.rtrim( AV6cContac), (short)(30), "%") ;
      /* Using cursor W00112 */
      pr_default.execute(0, new Object[] {lV5cContac, lV6cContac});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A366Contac = W00112_A366Contac[0] ;
         A365Contac = W00112_A365Contac[0] ;
         /* Execute user event: e11V112 */
         e11V112 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx0670_load10 extends GXLoadProducer
   {
      wgx0670 _sf ;

      public Gx0670_load10( wgx0670 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer10();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load10();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors10();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow10( )
   {
      return true;
   }

   public void autoRefresh_flow10( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cContac, cV5cContac)!=0)||(GXutil.strcmp(AV6cContac, cV6cContac)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow10() )) {
         subfile.refresh();
         resetSubfileConditions_flow10() ;
      }
   }

   public boolean getSearch_flow10( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow10( )
   {
      cV5cContac = AV5cContac ;
      cV6cContac = AV6cContac ;
   }

   public void resetSearchConditions_flow10( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow10( )
   {
      return new subwgx067010 ();
   }

   public boolean getSearch_flow10( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow10( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow10( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow10( )
   {
      GXRefreshCommand10 ();
   }

   public final  class Gx0670_flow10 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx0670 _sf ;

      public Gx0670_flow10( wgx0670 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow10();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow10(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow10();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow10();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow10(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow10();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow10(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow10(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow10(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow10();
      }

   }

   protected void GXRefreshCommand10( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V112 */
      e12V112 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V112( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV7pContac = A365Contac ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer10( )
   {
      subwgx067010 oAux = subwgx067010 ;
      subwgx067010 = new subwgx067010 ();
      variablesToSubfile10 ();
      subGrd_1.addElement(subwgx067010);
      subwgx067010 = oAux;
   }

   private void e11V112( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors10( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 539 , 282 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtavCcontacttypescode = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 24, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 24 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cContac" );
      ((GXEdit) edtavCcontacttypescode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCcontacttypescode.addFocusListener(this);
      edtavCcontacttypescode.getGXComponent().setHelpId("HLP_WGx0670.htm");
      edtavCcontacttypesdescription = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 48, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 48 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6cContac" );
      ((GXEdit) edtavCcontacttypesdescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCcontacttypesdescription.addFocusListener(this);
      edtavCcontacttypesdescription.getGXComponent().setHelpId("HLP_WGx0670.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx0670_load10(this), new Gx0670_flow10(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 119, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 118 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A365Contac" ), "Contact Type Code"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 118 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 218, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 217 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A366Contac" ), "Contact Type Description"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 217 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 72 , 392 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  431 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  431 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  431 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  431 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Contact Type Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 110 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "Contact Type Description", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 145 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                edtavCcontacttypescode ,
                edtavCcontacttypesdescription ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void variablesToSubfile10( )
   {
      subwgx067010.setContactTypesCode(A365Contac);
      subwgx067010.setContactTypesDescription(A366Contac);
   }

   protected void subfileToVariables10( )
   {
      A365Contac = subwgx067010.getContactTypesCode();
      A366Contac = subwgx067010.getContactTypesDescription();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      edtavCcontacttypescode.setValue( AV5cContac );
      edtavCcontacttypesdescription.setValue( AV6cContac );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      AV5cContac = edtavCcontacttypescode.getValue() ;
      AV6cContac = edtavCcontacttypesdescription.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx067010 = ( subwgx067010 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx067010 = new subwgx067010 ();
      }
      subfileToVariables10 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile10 ();
      subGrd_1.refreshLineValue(subwgx067010);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx067010 = ( subwgx067010 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx067010 = new subwgx067010 ();
      }
      subfileToVariables10 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V112 */
         e12V112 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V112 */
         e12V112 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavCcontacttypescode.isEventSource(eventSource) ) {
         setGXCursor( edtavCcontacttypescode.getGXCursor() );
         return;
      }
      if ( edtavCcontacttypesdescription.isEventSource(eventSource) ) {
         setGXCursor( edtavCcontacttypesdescription.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavCcontacttypescode.isEventSource(eventSource) ) {
         AV5cContac = edtavCcontacttypescode.getValue() ;
         return;
      }
      if ( edtavCcontacttypesdescription.isEventSource(eventSource) ) {
         AV6cContac = edtavCcontacttypesdescription.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V112 */
         e12V112 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = wgx0670.this.AV7pContac;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV7pContac = "" ;
      subwgx067010 = new subwgx067010();
      scmdbuf = "" ;
      lV5cContac = "" ;
      AV5cContac = "" ;
      lV6cContac = "" ;
      AV6cContac = "" ;
      W00112_A366Contac = new String[] {""} ;
      W00112_A365Contac = new String[] {""} ;
      A366Contac = "" ;
      A365Contac = "" ;
      gxIsRefreshing = false ;
      cV5cContac = "" ;
      cV6cContac = "" ;
      returnInSub = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx0670__default(),
         new Object[] {
             new Object[] {
            W00112_A366Contac, W00112_A365Contac
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String AV7pContac ;
   protected String lV5cContac ;
   protected String AV5cContac ;
   protected String lV6cContac ;
   protected String AV6cContac ;
   protected String A366Contac ;
   protected String A365Contac ;
   protected String cV5cContac ;
   protected String cV6cContac ;
   protected String[] aP0 ;
   protected subwgx067010 subwgx067010 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00112_A366Contac ;
   protected String[] W00112_A365Contac ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtavCcontacttypescode ;
   protected GUIObjectString edtavCcontacttypesdescription ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
}

final  class wgx0670__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00112", "SELECT [ContactTypesDescription], [ContactTypesCode] FROM [CONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ([ContactTypesCode] like ?) AND ([ContactTypesDescription] like ?) ORDER BY [ContactTypesCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 5);
               stmt.setVarchar(2, (String)parms[1], 30);
               break;
      }
   }

}


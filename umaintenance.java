/*
               File: MAINTENANCE
        Description: MAINTENANCE
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:27.0
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class umaintenance extends GXWorkpanel
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      umaintenance pgm = new umaintenance (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public umaintenance( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( umaintenance.class ));
   }

   public umaintenance( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "MAINTENANCE" ;
   }

   protected String getFrmTitle( )
   {
      return "Parameters - Processing ICSI" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 789 ;
   }

   protected int getFrmHeight( )
   {
      return 451 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WMAINTENANCE.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return true;
   }

   protected boolean isModal( )
   {
      return false;
   }

   protected boolean hasDBAccess( )
   {
      return false;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      start();
   }

   protected void standAlone( )
   {
      /* Execute user event: e11V0X2 */
      e11V0X2 ();
   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      standAlone();
      VariablesToControls();
      /* End function GeneXus Refresh */
   }

   protected void GXStart( )
   {
      /* Execute user event: e12V0X2 */
      e12V0X2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V0X2( )
   {
      eventNoLevelContext();
      /* Start Routine */
      GXt_svchar1 = AV8PATHRET ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILESAB", "Path RET files upload folder Sabre", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV8PATHRET = GXt_svchar1 ;
      edtavPathretfilesab.setValue(AV8PATHRET);
      GXt_svchar1 = AV9PATHRET ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILEAMA", "Path RET files upload folder Amadeus", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV9PATHRET = GXt_svchar1 ;
      edtavPathretfileama.setValue(AV9PATHRET);
      GXt_svchar1 = AV10PATHRE ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILEGAL", "Path RET files upload folder Galileo", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV10PATHRE = GXt_svchar1 ;
      edtavPathretfilegal.setValue(AV10PATHRE);
      GXt_svchar1 = AV11PATHBA ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATENCSAB", "Path Bat file to execute encrypt Sabre", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV11PATHBA = GXt_svchar1 ;
      edtavPathbatencsab.setValue(AV11PATHBA);
      GXt_svchar1 = AV12PATHBA ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATENCGAL", "Path Bat file to execute encrypt Galileo", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV12PATHBA = GXt_svchar1 ;
      edtavPathbatencgal.setValue(AV12PATHBA);
      GXt_svchar1 = AV34PATHBA ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATENCAMA", "Path Bat file to execute encrypt Amadeus", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV34PATHBA = GXt_svchar1 ;
      edtavPathbatencama.setValue(AV34PATHBA);
      GXt_svchar1 = AV32PATHBA ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATENCWSPN", "Path Bat file to execute encrypt Worldspan", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV32PATHBA = GXt_svchar1 ;
      edtavPathbatencwspn.setValue(AV32PATHBA);
      GXt_svchar1 = AV13PATHRE ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILVIS", "Path Visa return file", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV13PATHRE = GXt_svchar1 ;
      edtavPathretfilvis.setValue(AV13PATHRE);
      GXt_svchar1 = AV14PATHRE ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILRED", "Path Redecard return file", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV14PATHRE = GXt_svchar1 ;
      edtavPathretfilred.setValue(AV14PATHRE);
      GXt_svchar1 = AV15PATHRE ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILAX", "Path Amex return file", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV15PATHRE = GXt_svchar1 ;
      edtavPathretfilax.setValue(AV15PATHRE);
      GXt_svchar1 = AV31PATHRE ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILEWSPN", "Path Wordspan return file", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV31PATHRE = GXt_svchar1 ;
      edtavPathretfilewspn.setValue(AV31PATHRE);
      GXt_svchar1 = AV16PATHCO ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHCOPFILVIS", "Path Visa return file processing", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV16PATHCO = GXt_svchar1 ;
      edtavPathcopfilvis.setValue(AV16PATHCO);
      GXt_svchar1 = AV17PATHCO ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHCOPFILRED", "Path Redecard return file processing", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV17PATHCO = GXt_svchar1 ;
      edtavPathcopfilred.setValue(AV17PATHCO);
      GXt_svchar1 = AV18PATHCO ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHCOPFILAX", "Path Amex return file processing", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV18PATHCO = GXt_svchar1 ;
      edtavPathcopfilax.setValue(AV18PATHCO);
      GXt_svchar1 = AV19PATHSF ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHSFTPTAM", "Path SFTP TAM folder", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV19PATHSF = GXt_svchar1 ;
      edtavPathsftptam.setValue(AV19PATHSF);
      GXt_svchar1 = AV20HOSTSF ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "HOSTSFTPTAM", "Host access", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV20HOSTSF = GXt_svchar1 ;
      edtavHostsftptam.setValue(AV20HOSTSF);
      GXt_svchar1 = AV21PORTSF ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PORTSFTPTAM", "Port SFTP", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV21PORTSF = GXt_svchar1 ;
      edtavPortsftptam.setValue(AV21PORTSF);
      GXt_svchar1 = AV22KEYSFT ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "KEYSFTPTAM", "Key access SFTP", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV22KEYSFT = GXt_svchar1 ;
      edtavKeysftptam.setValue(AV22KEYSFT);
      GXt_svchar1 = AV23USERSF ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "USERSFTPTAM", "Key user SFTP", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV23USERSF = GXt_svchar1 ;
      edtavUsersftptam.setValue(AV23USERSF);
      GXt_svchar1 = AV24PASSSF ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PASSSFTPTAM", "Key password SFTP ( data saved encrypt )", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV24PASSSF = GXt_svchar1 ;
      edtavPasssftptam.setValue(AV24PASSSF);
      GXt_svchar1 = AV25NAMESF ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "NAMESFTPTAM", "Name file SFTP", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV25NAMESF = GXt_svchar1 ;
      edtavNamesftptam.setValue(AV25NAMESF);
      GXt_svchar1 = AV30PathBa ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATHSENDEMAIL", "Path Bat to send files TAM", "S", "", GXv_svchar2) ;
      umaintenance.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV30PathBa = GXt_svchar1 ;
      edtavPathbatsftp.setValue(AV30PathBa);
   }

   public void GXEnter( )
   {
      /* Execute user event: e13V0X2 */
      e13V0X2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e13V0X2( )
   {
      eventLevelContext();
      /* Enter Routine */
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILESAB", AV8PATHRET) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILEAMA", AV9PATHRET) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILEGAL", AV10PATHRE) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHBATENCSAB", AV11PATHBA) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHBATENCGAL", AV12PATHBA) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHBATENCAMA", AV34PATHBA) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHBATENCWSPN", AV32PATHBA) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILVIS", AV13PATHRE) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILRED", AV14PATHRE) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILAX", AV15PATHRE) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHRETFILEWSPN", AV31PATHRE) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHCOPFILVIS", AV16PATHCO) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHCOPFILRED", AV17PATHCO) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHCOPFILAX", AV18PATHCO) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHSFTPTAM", AV19PATHSF) ;
      new patualizaconfig(remoteHandle, context).execute( "HOSTSFTPTAM", AV20HOSTSF) ;
      new patualizaconfig(remoteHandle, context).execute( "PORTSFTPTAM", AV21PORTSF) ;
      new patualizaconfig(remoteHandle, context).execute( "KEYSFTPTAM", AV22KEYSFT) ;
      new patualizaconfig(remoteHandle, context).execute( "USERSFTPTAM", AV23USERSF) ;
      new patualizaconfig(remoteHandle, context).execute( "NAMESFTPTAM", AV25NAMESF) ;
      new patualizaconfig(remoteHandle, context).execute( "PASSSFTPTAM", AV24PASSSF) ;
      new patualizaconfig(remoteHandle, context).execute( "PATHBATHSENDEMAIL", AV30PathBa) ;
      GXutil.msg( me(), "The data was updated !" );
      AV29AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " Mudan�a nos parametros de configura��o de processamento." ;
      /* Execute user subroutine: S112 */
      S112 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
      eventLevelResetContext();
   }

   public void S112( )
   {
      /* 'AUDITTRAIL' Routine */
      AV26Auditl = "Localhost" ;
      AV27AuditT = "TIES_ICSI" ;
      AV28AuditT = "PCI" ;
      GXv_svchar2[0] = AV26Auditl ;
      GXv_svchar3[0] = AV27AuditT ;
      GXv_svchar4[0] = AV28AuditT ;
      GXv_svchar5[0] = AV29AuditT ;
      new pnewaudit(remoteHandle, context).execute( GXv_svchar2, GXv_svchar3, GXv_svchar4, GXv_svchar5) ;
      umaintenance.this.AV26Auditl = GXv_svchar2[0] ;
      umaintenance.this.AV27AuditT = GXv_svchar3[0] ;
      umaintenance.this.AV28AuditT = GXv_svchar4[0] ;
      umaintenance.this.AV29AuditT = GXv_svchar5[0] ;
   }

   protected void nextLoad( )
   {
   }

   protected void e11V0X2( )
   {
      /* Load Routine */
      nextLoad();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 24 , 789 , 451 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      bttBtnatualizaparametro = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  554 ,  17 ,  91 ,  26 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtnatualizaparametro.setTooltip("Confirm");
      bttBtnatualizaparametro.addActionListener(this);
      bttBtnsair = UIFactory.getGXButton( GXPanel1 , "Close" ,  660 ,  17 ,  91 ,  26 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtnsair.setTooltip("Close");
      bttBtnsair.addActionListener(this);
      bttBtnsair.setFiresEvents(false);
      rctrct59 = UIFactory.getGXRectangle( GXPanel1 , 1 , 5 , 60 , 779 , 384 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      rctrct58 = UIFactory.getGXRectangle( GXPanel1 , 1 , 4 , 9 , 779 , 42 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      tctrltctrl2 = new GXTabControl (this,  GXPanel1 , 20 , 74 , 741 , 362 , UIFactory.getColor(5) , UIFactory.getColor(8) , 0, 0, 0, false );
      tpagetpage3 = new GXTabPage ( tctrltctrl2 , "Upload RET Folder" , 5 , 29 , 736 , 333 , UIFactory.getColor(15) , UIFactory.getColor(18) , 0, 0);
      edtavPathretfilesab = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),97, 25, 618, 21, tpagetpage3.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage3.getGXPanel() , 97 , 25 , 618 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV8PATHRET" );
      ((GXEdit) edtavPathretfilesab.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfilesab.addFocusListener(this);
      edtavPathretfilesab.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathretfileama = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),97, 64, 618, 21, tpagetpage3.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage3.getGXPanel() , 97 , 64 , 618 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV9PATHRET" );
      ((GXEdit) edtavPathretfileama.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfileama.addFocusListener(this);
      edtavPathretfileama.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathretfilegal = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),97, 103, 618, 21, tpagetpage3.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage3.getGXPanel() , 97 , 103 , 618 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV10PATHRE" );
      ((GXEdit) edtavPathretfilegal.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfilegal.addFocusListener(this);
      edtavPathretfilegal.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathretfilewspn = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),97, 139, 618, 20, tpagetpage3.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage3.getGXPanel() , 97 , 139 , 618 , 20 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV31PATHRE" );
      ((GXEdit) edtavPathretfilewspn.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfilewspn.addFocusListener(this);
      edtavPathretfilewspn.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      lbllbl5 = UIFactory.getLabel(tpagetpage3.getGXPanel(), "Sabre:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 32 , 29 , 38 , 13 );
      lbllbl6 = UIFactory.getLabel(tpagetpage3.getGXPanel(), "Amadeus:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 32 , 68 , 56 , 13 );
      lbllbl7 = UIFactory.getLabel(tpagetpage3.getGXPanel(), "Galileo:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 32 , 107 , 44 , 13 );
      lbllbl11 = UIFactory.getLabel(tpagetpage3.getGXPanel(), "Worldspan:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 32 , 143 , 65 , 13 );
      rctrct4 = UIFactory.getGXRectangle( tpagetpage3.getGXPanel() , 1 , 6 , 5 , 722 , 322 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      tpagetpage13 = new GXTabPage ( tctrltctrl2 , "Bat Encrypt" , 5 , 29 , 736 , 333 , UIFactory.getColor(15) , UIFactory.getColor(18) , 0, 0);
      edtavPathbatencsab = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),90, 22, 628, 21, tpagetpage13.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage13.getGXPanel() , 90 , 22 , 628 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV11PATHBA" );
      ((GXEdit) edtavPathbatencsab.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathbatencsab.addFocusListener(this);
      edtavPathbatencsab.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathbatencama = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),90, 49, 628, 20, tpagetpage13.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage13.getGXPanel() , 90 , 49 , 628 , 20 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV34PATHBA" );
      ((GXEdit) edtavPathbatencama.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathbatencama.addFocusListener(this);
      edtavPathbatencama.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathbatencgal = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),90, 84, 628, 21, tpagetpage13.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage13.getGXPanel() , 90 , 84 , 628 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV12PATHBA" );
      ((GXEdit) edtavPathbatencgal.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathbatencgal.addFocusListener(this);
      edtavPathbatencgal.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathbatencwspn = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),91, 122, 628, 20, tpagetpage13.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage13.getGXPanel() , 91 , 122 , 628 , 20 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV32PATHBA" );
      ((GXEdit) edtavPathbatencwspn.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathbatencwspn.addFocusListener(this);
      edtavPathbatencwspn.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      lbllbl16 = UIFactory.getLabel(tpagetpage13.getGXPanel(), "Sabre:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 26 , 38 , 13 );
      lbllbl21 = UIFactory.getLabel(tpagetpage13.getGXPanel(), "Amadeus:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 24 , 53 , 56 , 13 );
      lbllbl15 = UIFactory.getLabel(tpagetpage13.getGXPanel(), "Galileo:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 37 , 88 , 44 , 13 );
      lbllbl19 = UIFactory.getLabel(tpagetpage13.getGXPanel(), "Worldspan:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 17 , 126 , 65 , 13 );
      rctrct14 = UIFactory.getGXRectangle( tpagetpage13.getGXPanel() , 1 , 10 , 6 , 722 , 317 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      tpagetpage23 = new GXTabPage ( tctrltctrl2 , "Return Folder" , 5 , 29 , 736 , 333 , UIFactory.getColor(15) , UIFactory.getColor(18) , 0, 0);
      edtavPathretfilvis = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),105, 25, 616, 21, tpagetpage23.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage23.getGXPanel() , 105 , 25 , 616 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV13PATHRE" );
      ((GXEdit) edtavPathretfilvis.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfilvis.addFocusListener(this);
      edtavPathretfilvis.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathretfilred = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),105, 64, 616, 21, tpagetpage23.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage23.getGXPanel() , 105 , 64 , 616 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV14PATHRE" );
      ((GXEdit) edtavPathretfilred.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfilred.addFocusListener(this);
      edtavPathretfilred.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathretfilax = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),105, 103, 616, 21, tpagetpage23.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage23.getGXPanel() , 105 , 103 , 616 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV15PATHRE" );
      ((GXEdit) edtavPathretfilax.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathretfilax.addFocusListener(this);
      edtavPathretfilax.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      lbllbl24 = UIFactory.getLabel(tpagetpage23.getGXPanel(), "CIELO:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 24 , 29 , 41 , 13 );
      lbllbl25 = UIFactory.getLabel(tpagetpage23.getGXPanel(), "REDECARD:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 24 , 68 , 73 , 13 );
      lbllbl26 = UIFactory.getLabel(tpagetpage23.getGXPanel(), "AMEX:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 24 , 107 , 39 , 13 );
      rctrct30 = UIFactory.getGXRectangle( tpagetpage23.getGXPanel() , 1 , 7 , 7 , 724 , 315 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      tpagetpage31 = new GXTabPage ( tctrltctrl2 , "Return File Processing" , 5 , 29 , 736 , 333 , UIFactory.getColor(15) , UIFactory.getColor(18) , 0, 0);
      edtavPathcopfilred = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),108, 23, 602, 21, tpagetpage31.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage31.getGXPanel() , 108 , 23 , 602 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV17PATHCO" );
      ((GXEdit) edtavPathcopfilred.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathcopfilred.addFocusListener(this);
      edtavPathcopfilred.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathcopfilax = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),108, 62, 602, 21, tpagetpage31.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage31.getGXPanel() , 108 , 62 , 602 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV18PATHCO" );
      ((GXEdit) edtavPathcopfilax.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathcopfilax.addFocusListener(this);
      edtavPathcopfilax.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathcopfilvis = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),108, 101, 602, 21, tpagetpage31.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage31.getGXPanel() , 108 , 101 , 602 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV16PATHCO" );
      ((GXEdit) edtavPathcopfilvis.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathcopfilvis.addFocusListener(this);
      edtavPathcopfilvis.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      lbllbl34 = UIFactory.getLabel(tpagetpage31.getGXPanel(), "REDECARD:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 27 , 27 , 73 , 13 );
      lbllbl35 = UIFactory.getLabel(tpagetpage31.getGXPanel(), "AMEX:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 27 , 66 , 39 , 13 );
      lbllbl33 = UIFactory.getLabel(tpagetpage31.getGXPanel(), "CIELO:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 27 , 105 , 41 , 13 );
      rctrct32 = UIFactory.getGXRectangle( tpagetpage31.getGXPanel() , 1 , 5 , 6 , 724 , 317 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      tpagetpage39 = new GXTabPage ( tctrltctrl2 , "SFTP Data" , 5 , 29 , 736 , 333 , UIFactory.getColor(15) , UIFactory.getColor(18) , 0, 0);
      edtavPathsftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 23, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 23 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV19PATHSF" );
      ((GXEdit) edtavPathsftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathsftptam.addFocusListener(this);
      edtavPathsftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavHostsftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 61, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 61 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV20HOSTSF" );
      ((GXEdit) edtavHostsftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavHostsftptam.addFocusListener(this);
      edtavHostsftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPortsftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 98, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 98 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV21PORTSF" );
      ((GXEdit) edtavPortsftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPortsftptam.addFocusListener(this);
      edtavPortsftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavKeysftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 136, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 136 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV22KEYSFT" );
      ((GXEdit) edtavKeysftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavKeysftptam.addFocusListener(this);
      edtavKeysftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavUsersftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 173, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 173 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV23USERSF" );
      ((GXEdit) edtavUsersftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavUsersftptam.addFocusListener(this);
      edtavUsersftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPasssftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 211, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 211 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV24PASSSF" );
      ((GXEdit) edtavPasssftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPasssftptam.addFocusListener(this);
      edtavPasssftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavNamesftptam = new GUIObjectString ( new GXEdit(256, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 248, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 248 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV25NAMESF" );
      ((GXEdit) edtavNamesftptam.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavNamesftptam.addFocusListener(this);
      edtavNamesftptam.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      edtavPathbatsftp = new GUIObjectString ( new GXEdit(255, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 8),180, 285, 525, 21, tpagetpage39.getGXPanel(), false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , tpagetpage39.getGXPanel() , 180 , 285 , 525 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 8) , true , "AV30PathBa" );
      ((GXEdit) edtavPathbatsftp.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavPathbatsftp.addFocusListener(this);
      edtavPathbatsftp.getGXComponent().setHelpId("HLP_WMAINTENANCE.htm");
      lbllbl40 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Path SFTP TAM folder:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 27 , 132 , 13 );
      lbllbl41 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "HOST:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 65 , 39 , 13 );
      lbllbl42 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Port SFTP:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 102 , 63 , 13 );
      lbllbl43 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Key access:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 140 , 70 , 13 );
      lbllbl44 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Key User:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 177 , 56 , 13 );
      lbllbl45 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Key password SFTP:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 215 , 118 , 13 );
      lbllbl46 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Name file SFTP:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 252 , 93 , 13 );
      lbllbl55 = UIFactory.getLabel(tpagetpage39.getGXPanel(), "Path Bat SFTP:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 40 , 289 , 89 , 13 );
      rctrct54 = UIFactory.getGXRectangle( tpagetpage39.getGXPanel() , 1 , 7 , 9 , 718 , 313 , Integer.MAX_VALUE , UIFactory.getColor(8) , ILabel.BORDER_3D );
      focusManager.setControlList(new IFocusableControl[] {
                edtavPathretfilesab ,
                edtavPathretfileama ,
                edtavPathretfilegal ,
                edtavPathretfilewspn ,
                edtavPathbatencsab ,
                edtavPathbatencama ,
                edtavPathbatencgal ,
                edtavPathbatencwspn ,
                edtavPathretfilvis ,
                edtavPathretfilred ,
                edtavPathretfilax ,
                edtavPathcopfilred ,
                edtavPathcopfilax ,
                edtavPathcopfilvis ,
                edtavPathsftptam ,
                edtavHostsftptam ,
                edtavPortsftptam ,
                edtavKeysftptam ,
                edtavUsersftptam ,
                edtavPasssftptam ,
                edtavNamesftptam ,
                edtavPathbatsftp ,
                bttBtnatualizaparametro ,
                bttBtnsair
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtavPathretfilesab, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void VariablesToControls( )
   {
      edtavPathretfilesab.setValue( AV8PATHRET );
      edtavPathretfileama.setValue( AV9PATHRET );
      edtavPathretfilegal.setValue( AV10PATHRE );
      edtavPathretfilewspn.setValue( AV31PATHRE );
      edtavPathbatencsab.setValue( AV11PATHBA );
      edtavPathbatencgal.setValue( AV12PATHBA );
      edtavPathbatencwspn.setValue( AV32PATHBA );
      edtavPathbatencama.setValue( AV34PATHBA );
      edtavPathretfilvis.setValue( AV13PATHRE );
      edtavPathretfilred.setValue( AV14PATHRE );
      edtavPathretfilax.setValue( AV15PATHRE );
      edtavPathcopfilvis.setValue( AV16PATHCO );
      edtavPathcopfilred.setValue( AV17PATHCO );
      edtavPathcopfilax.setValue( AV18PATHCO );
      edtavPathsftptam.setValue( AV19PATHSF );
      edtavHostsftptam.setValue( AV20HOSTSF );
      edtavPortsftptam.setValue( AV21PORTSF );
      edtavKeysftptam.setValue( AV22KEYSFT );
      edtavUsersftptam.setValue( AV23USERSF );
      edtavPasssftptam.setValue( AV24PASSSF );
      edtavNamesftptam.setValue( AV25NAMESF );
      edtavPathbatsftp.setValue( AV30PathBa );
   }

   protected void ControlsToVariables( )
   {
      AV8PATHRET = edtavPathretfilesab.getValue() ;
      AV9PATHRET = edtavPathretfileama.getValue() ;
      AV10PATHRE = edtavPathretfilegal.getValue() ;
      AV31PATHRE = edtavPathretfilewspn.getValue() ;
      AV11PATHBA = edtavPathbatencsab.getValue() ;
      AV12PATHBA = edtavPathbatencgal.getValue() ;
      AV32PATHBA = edtavPathbatencwspn.getValue() ;
      AV34PATHBA = edtavPathbatencama.getValue() ;
      AV13PATHRE = edtavPathretfilvis.getValue() ;
      AV14PATHRE = edtavPathretfilred.getValue() ;
      AV15PATHRE = edtavPathretfilax.getValue() ;
      AV16PATHCO = edtavPathcopfilvis.getValue() ;
      AV17PATHCO = edtavPathcopfilred.getValue() ;
      AV18PATHCO = edtavPathcopfilax.getValue() ;
      AV19PATHSF = edtavPathsftptam.getValue() ;
      AV20HOSTSF = edtavHostsftptam.getValue() ;
      AV21PORTSF = edtavPortsftptam.getValue() ;
      AV22KEYSFT = edtavKeysftptam.getValue() ;
      AV23USERSF = edtavUsersftptam.getValue() ;
      AV24PASSSF = edtavPasssftptam.getValue() ;
      AV25NAMESF = edtavNamesftptam.getValue() ;
      AV30PathBa = edtavPathbatsftp.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtnsair.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtnatualizaparametro.isEventSource(eventSource) ) {
         /* Execute user event: e13V0X2 */
         e13V0X2 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavPathretfilesab.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfilesab.getGXCursor() );
         return;
      }
      if ( edtavPathretfileama.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfileama.getGXCursor() );
         return;
      }
      if ( edtavPathretfilegal.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfilegal.getGXCursor() );
         return;
      }
      if ( edtavPathretfilewspn.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfilewspn.getGXCursor() );
         return;
      }
      if ( edtavPathbatencsab.isEventSource(eventSource) ) {
         setGXCursor( edtavPathbatencsab.getGXCursor() );
         return;
      }
      if ( edtavPathbatencgal.isEventSource(eventSource) ) {
         setGXCursor( edtavPathbatencgal.getGXCursor() );
         return;
      }
      if ( edtavPathbatencwspn.isEventSource(eventSource) ) {
         setGXCursor( edtavPathbatencwspn.getGXCursor() );
         return;
      }
      if ( edtavPathbatencama.isEventSource(eventSource) ) {
         setGXCursor( edtavPathbatencama.getGXCursor() );
         return;
      }
      if ( edtavPathretfilvis.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfilvis.getGXCursor() );
         return;
      }
      if ( edtavPathretfilred.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfilred.getGXCursor() );
         return;
      }
      if ( edtavPathretfilax.isEventSource(eventSource) ) {
         setGXCursor( edtavPathretfilax.getGXCursor() );
         return;
      }
      if ( edtavPathcopfilvis.isEventSource(eventSource) ) {
         setGXCursor( edtavPathcopfilvis.getGXCursor() );
         return;
      }
      if ( edtavPathcopfilred.isEventSource(eventSource) ) {
         setGXCursor( edtavPathcopfilred.getGXCursor() );
         return;
      }
      if ( edtavPathcopfilax.isEventSource(eventSource) ) {
         setGXCursor( edtavPathcopfilax.getGXCursor() );
         return;
      }
      if ( edtavPathsftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavPathsftptam.getGXCursor() );
         return;
      }
      if ( edtavHostsftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavHostsftptam.getGXCursor() );
         return;
      }
      if ( edtavPortsftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavPortsftptam.getGXCursor() );
         return;
      }
      if ( edtavKeysftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavKeysftptam.getGXCursor() );
         return;
      }
      if ( edtavUsersftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavUsersftptam.getGXCursor() );
         return;
      }
      if ( edtavPasssftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavPasssftptam.getGXCursor() );
         return;
      }
      if ( edtavNamesftptam.isEventSource(eventSource) ) {
         setGXCursor( edtavNamesftptam.getGXCursor() );
         return;
      }
      if ( edtavPathbatsftp.isEventSource(eventSource) ) {
         setGXCursor( edtavPathbatsftp.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavPathretfilesab.isEventSource(eventSource) ) {
         AV8PATHRET = edtavPathretfilesab.getValue() ;
         return;
      }
      if ( edtavPathretfileama.isEventSource(eventSource) ) {
         AV9PATHRET = edtavPathretfileama.getValue() ;
         return;
      }
      if ( edtavPathretfilegal.isEventSource(eventSource) ) {
         AV10PATHRE = edtavPathretfilegal.getValue() ;
         return;
      }
      if ( edtavPathretfilewspn.isEventSource(eventSource) ) {
         AV31PATHRE = edtavPathretfilewspn.getValue() ;
         return;
      }
      if ( edtavPathbatencsab.isEventSource(eventSource) ) {
         AV11PATHBA = edtavPathbatencsab.getValue() ;
         return;
      }
      if ( edtavPathbatencgal.isEventSource(eventSource) ) {
         AV12PATHBA = edtavPathbatencgal.getValue() ;
         return;
      }
      if ( edtavPathbatencwspn.isEventSource(eventSource) ) {
         AV32PATHBA = edtavPathbatencwspn.getValue() ;
         return;
      }
      if ( edtavPathbatencama.isEventSource(eventSource) ) {
         AV34PATHBA = edtavPathbatencama.getValue() ;
         return;
      }
      if ( edtavPathretfilvis.isEventSource(eventSource) ) {
         AV13PATHRE = edtavPathretfilvis.getValue() ;
         return;
      }
      if ( edtavPathretfilred.isEventSource(eventSource) ) {
         AV14PATHRE = edtavPathretfilred.getValue() ;
         return;
      }
      if ( edtavPathretfilax.isEventSource(eventSource) ) {
         AV15PATHRE = edtavPathretfilax.getValue() ;
         return;
      }
      if ( edtavPathcopfilvis.isEventSource(eventSource) ) {
         AV16PATHCO = edtavPathcopfilvis.getValue() ;
         return;
      }
      if ( edtavPathcopfilred.isEventSource(eventSource) ) {
         AV17PATHCO = edtavPathcopfilred.getValue() ;
         return;
      }
      if ( edtavPathcopfilax.isEventSource(eventSource) ) {
         AV18PATHCO = edtavPathcopfilax.getValue() ;
         return;
      }
      if ( edtavPathsftptam.isEventSource(eventSource) ) {
         AV19PATHSF = edtavPathsftptam.getValue() ;
         return;
      }
      if ( edtavHostsftptam.isEventSource(eventSource) ) {
         AV20HOSTSF = edtavHostsftptam.getValue() ;
         return;
      }
      if ( edtavPortsftptam.isEventSource(eventSource) ) {
         AV21PORTSF = edtavPortsftptam.getValue() ;
         return;
      }
      if ( edtavKeysftptam.isEventSource(eventSource) ) {
         AV22KEYSFT = edtavKeysftptam.getValue() ;
         return;
      }
      if ( edtavUsersftptam.isEventSource(eventSource) ) {
         AV23USERSF = edtavUsersftptam.getValue() ;
         return;
      }
      if ( edtavPasssftptam.isEventSource(eventSource) ) {
         AV24PASSSF = edtavPasssftptam.getValue() ;
         return;
      }
      if ( edtavNamesftptam.isEventSource(eventSource) ) {
         AV25NAMESF = edtavNamesftptam.getValue() ;
         return;
      }
      if ( edtavPathbatsftp.isEventSource(eventSource) ) {
         AV30PathBa = edtavPathbatsftp.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e13V0X2 */
         e13V0X2 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(wmaintenance.class);
      return new GXcfg();
   }
*/
   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtnsair ;
   }

   public void refreshArray( String array )
   {
      if  (array.equals("GXv_svchar5"))  {
      }
      if  (array.equals("GXv_svchar4"))  {
      }
      if  (array.equals("GXv_svchar3"))  {
      }
      if  (array.equals("GXv_svchar2"))  {
      }
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      returnInSub = false ;
      AV8PATHRET = "" ;
      AV9PATHRET = "" ;
      AV10PATHRE = "" ;
      AV11PATHBA = "" ;
      AV12PATHBA = "" ;
      AV34PATHBA = "" ;
      AV32PATHBA = "" ;
      AV13PATHRE = "" ;
      AV14PATHRE = "" ;
      AV15PATHRE = "" ;
      AV31PATHRE = "" ;
      AV16PATHCO = "" ;
      AV17PATHCO = "" ;
      AV18PATHCO = "" ;
      AV19PATHSF = "" ;
      AV20HOSTSF = "" ;
      AV21PORTSF = "" ;
      AV22KEYSFT = "" ;
      AV23USERSF = "" ;
      AV24PASSSF = "" ;
      AV25NAMESF = "" ;
      AV30PathBa = "" ;
      GXt_svchar1 = "" ;
      AV29AuditT = "" ;
      AV26Auditl = "" ;
      AV27AuditT = "" ;
      AV28AuditT = "" ;
      GXv_svchar2 = new String [1] ;
      GXv_svchar3 = new String [1] ;
      GXv_svchar4 = new String [1] ;
      GXv_svchar5 = new String [1] ;
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String AV30PathBa ;
   protected boolean returnInSub ;
   protected String AV8PATHRET ;
   protected String AV9PATHRET ;
   protected String AV10PATHRE ;
   protected String AV11PATHBA ;
   protected String AV12PATHBA ;
   protected String AV34PATHBA ;
   protected String AV32PATHBA ;
   protected String AV13PATHRE ;
   protected String AV14PATHRE ;
   protected String AV15PATHRE ;
   protected String AV31PATHRE ;
   protected String AV16PATHCO ;
   protected String AV17PATHCO ;
   protected String AV18PATHCO ;
   protected String AV19PATHSF ;
   protected String AV20HOSTSF ;
   protected String AV21PORTSF ;
   protected String AV22KEYSFT ;
   protected String AV23USERSF ;
   protected String AV24PASSSF ;
   protected String AV25NAMESF ;
   protected String GXt_svchar1 ;
   protected String AV29AuditT ;
   protected String AV26Auditl ;
   protected String AV27AuditT ;
   protected String AV28AuditT ;
   protected String GXv_svchar2[] ;
   protected String GXv_svchar3[] ;
   protected String GXv_svchar4[] ;
   protected String GXv_svchar5[] ;
   protected GXPanel GXPanel1 ;
   protected IGXButton bttBtnatualizaparametro ;
   protected IGXButton bttBtnsair ;
   protected IGXRectangle rctrct59 ;
   protected IGXRectangle rctrct58 ;
   protected GXTabControl tctrltctrl2 ;
   protected GXTabPage tpagetpage3 ;
   protected GUIObjectString edtavPathretfilesab ;
   protected GUIObjectString edtavPathretfileama ;
   protected GUIObjectString edtavPathretfilegal ;
   protected GUIObjectString edtavPathretfilewspn ;
   protected ILabel lbllbl5 ;
   protected ILabel lbllbl6 ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl11 ;
   protected IGXRectangle rctrct4 ;
   protected GXTabPage tpagetpage13 ;
   protected GUIObjectString edtavPathbatencsab ;
   protected GUIObjectString edtavPathbatencama ;
   protected GUIObjectString edtavPathbatencgal ;
   protected GUIObjectString edtavPathbatencwspn ;
   protected ILabel lbllbl16 ;
   protected ILabel lbllbl21 ;
   protected ILabel lbllbl15 ;
   protected ILabel lbllbl19 ;
   protected IGXRectangle rctrct14 ;
   protected GXTabPage tpagetpage23 ;
   protected GUIObjectString edtavPathretfilvis ;
   protected GUIObjectString edtavPathretfilred ;
   protected GUIObjectString edtavPathretfilax ;
   protected ILabel lbllbl24 ;
   protected ILabel lbllbl25 ;
   protected ILabel lbllbl26 ;
   protected IGXRectangle rctrct30 ;
   protected GXTabPage tpagetpage31 ;
   protected GUIObjectString edtavPathcopfilred ;
   protected GUIObjectString edtavPathcopfilax ;
   protected GUIObjectString edtavPathcopfilvis ;
   protected ILabel lbllbl34 ;
   protected ILabel lbllbl35 ;
   protected ILabel lbllbl33 ;
   protected IGXRectangle rctrct32 ;
   protected GXTabPage tpagetpage39 ;
   protected GUIObjectString edtavPathsftptam ;
   protected GUIObjectString edtavHostsftptam ;
   protected GUIObjectString edtavPortsftptam ;
   protected GUIObjectString edtavKeysftptam ;
   protected GUIObjectString edtavUsersftptam ;
   protected GUIObjectString edtavPasssftptam ;
   protected GUIObjectString edtavNamesftptam ;
   protected GUIObjectString edtavPathbatsftp ;
   protected ILabel lbllbl40 ;
   protected ILabel lbllbl41 ;
   protected ILabel lbllbl42 ;
   protected ILabel lbllbl43 ;
   protected ILabel lbllbl44 ;
   protected ILabel lbllbl45 ;
   protected ILabel lbllbl46 ;
   protected ILabel lbllbl55 ;
   protected IGXRectangle rctrct54 ;
}


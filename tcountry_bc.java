/*
               File: tcountry_bc
        Description: Country
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:2.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tcountry_bc extends GXWebPanel implements IGxSilentTrn
{
   public tcountry_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tcountry_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcountry_bc.class ));
   }

   public tcountry_bc( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z23ISOCod = A23ISOCod ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_040( )
   {
      beforeValidate044( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls044( ) ;
         }
         else
         {
            checkExtendedTable044( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors044( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         /* Save parent mode. */
         sMode4 = Gx_mode ;
         confirm_0488( ) ;
         if ( ( AnyError == 0 ) )
         {
            confirm_0478( ) ;
            if ( ( AnyError == 0 ) )
            {
               confirm_0465( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Restore parent mode. */
                  Gx_mode = sMode4 ;
                  IsConfirmed = (short)(1) ;
               }
            }
         }
         /* Restore parent mode. */
         Gx_mode = sMode4 ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues040( ) ;
      }
   }

   public void confirm_0465( )
   {
      nGXsfl_65_idx = (short)(0) ;
      while ( ( nGXsfl_65_idx < bcCountry.getgxTv_SdtCountry_Currencies().size() ) )
      {
         readRow0465( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound65 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_65 != 0 ) )
         {
            getKey0465( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound65 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate0465( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable0465( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                     }
                     closeExtendedTableCursors0465( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound65 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey0465( ) ;
                     load0465( ) ;
                     beforeValidate0465( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls0465( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_65 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate0465( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable0465( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                           }
                           closeExtendedTableCursors0465( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void confirm_0478( )
   {
      nGXsfl_78_idx = (short)(0) ;
      while ( ( nGXsfl_78_idx < bcCountry.getgxTv_SdtCountry_Contacttypes().size() ) )
      {
         readRow0478( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound78 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_78 != 0 ) )
         {
            getKey0478( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound78 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate0478( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable0478( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        zm0478( 20) ;
                     }
                     closeExtendedTableCursors0478( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound78 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey0478( ) ;
                     load0478( ) ;
                     beforeValidate0478( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls0478( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_78 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate0478( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable0478( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              zm0478( 20) ;
                           }
                           closeExtendedTableCursors0478( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void confirm_0488( )
   {
      nGXsfl_88_idx = (short)(0) ;
      while ( ( nGXsfl_88_idx < bcCountry.getgxTv_SdtCountry_Level1().size() ) )
      {
         readRow0488( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound88 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_88 != 0 ) )
         {
            getKey0488( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound88 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate0488( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable0488( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        zm0488( 18) ;
                     }
                     closeExtendedTableCursors0488( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound88 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey0488( ) ;
                     load0488( ) ;
                     beforeValidate0488( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls0488( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_88 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate0488( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable0488( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              zm0488( 18) ;
                           }
                           closeExtendedTableCursors0488( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void e11042( )
   {
      /* Start Routine */
      AV13LgnLog = AV11Sessio.getValue("LOGGED") ;
      if ( ( GXutil.strcmp(AV13LgnLog, "") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hlogin") );
      }
      GXt_int1 = AV16AlterB ;
      GXv_svchar2[0] = "ALTERDATABANK" ;
      GXv_svchar3[0] = "User Can Alter Data Bank" ;
      GXv_char4[0] = "" ;
      GXv_int5[0] = GXt_int1 ;
      new pcheckprofile(remoteHandle, context).execute( GXv_svchar2, GXv_svchar3, GXv_char4, GXv_int5) ;
      tcountry_bc.this.GXt_int1 = GXv_int5[0] ;
      AV16AlterB = GXt_int1 ;
   }

   public void e12042( )
   {
      /* 'Back' Routine */
   }

   public void zm044( int GX_JID )
   {
      if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
      {
         Z20ISODes = A20ISODes ;
         Z448ISOCon = A448ISOCon ;
         Z449ISOCon = A449ISOCon ;
         Z450ISOCon = A450ISOCon ;
         Z30lgnLogi = A30lgnLogi ;
         Z846DateUp = A846DateUp ;
         Z365Contac = A365Contac ;
         Z406ISOCtt = A406ISOCtt ;
         Z407ISOCtt = A407ISOCtt ;
         Z408ISOCtt = A408ISOCtt ;
         Z409ISOCtt = A409ISOCtt ;
         Z366Contac = A366Contac ;
         Z349CurCod = A349CurCod ;
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
      }
      if ( ( GX_JID == -16 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z20ISODes = A20ISODes ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load044( )
   {
      /* Using cursor BC000412 */
      pr_default.execute(10, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound4 = (short)(1) ;
         A20ISODes = BC000412_A20ISODes[0] ;
         n20ISODes = BC000412_n20ISODes[0] ;
         zm044( -16) ;
      }
      pr_default.close(10);
      onLoadActions044( ) ;
   }

   public void onLoadActions044( )
   {
   }

   public void checkExtendedTable044( )
   {
      standaloneModal( ) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A23ISOCod), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Code can not be empty", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A20ISODes), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Description can not be empty", 1);
         AnyError = (short)(1) ;
      }
   }

   public void closeExtendedTableCursors044( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey044( )
   {
      /* Using cursor BC000413 */
      pr_default.execute(11, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound4 = (short)(1) ;
      }
      else
      {
         RcdFound4 = (short)(0) ;
      }
      pr_default.close(11);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC000411 */
      pr_default.execute(9, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      if ( (pr_default.getStatus(9) != 101) )
      {
         zm044( 16) ;
         RcdFound4 = (short)(1) ;
         A23ISOCod = BC000411_A23ISOCod[0] ;
         n23ISOCod = BC000411_n23ISOCod[0] ;
         A20ISODes = BC000411_A20ISODes[0] ;
         n20ISODes = BC000411_n20ISODes[0] ;
         Z23ISOCod = A23ISOCod ;
         sMode4 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load044( ) ;
         Gx_mode = sMode4 ;
      }
      else
      {
         RcdFound4 = (short)(0) ;
         initializeNonKey044( ) ;
         sMode4 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode4 ;
      }
      pr_default.close(9);
   }

   public void getEqualNoModal( )
   {
      getKey044( ) ;
      if ( ( RcdFound4 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_040( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency044( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC000410 */
         pr_default.execute(8, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
         if ( ! (pr_default.getStatus(8) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"COUNTRY"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(8) == 101) || ( GXutil.strcmp(Z20ISODes, BC000410_A20ISODes[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"COUNTRY"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert044( )
   {
      beforeValidate044( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable044( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm044( 0) ;
         checkOptimisticConcurrency044( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm044( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert044( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000414 */
                  pr_default.execute(12, new Object[] {new Boolean(n23ISOCod), A23ISOCod, new Boolean(n20ISODes), A20ISODes});
                  if ( (pr_default.getStatus(12) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel044( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load044( ) ;
         }
         endLevel044( ) ;
      }
      closeExtendedTableCursors044( ) ;
   }

   public void update044( )
   {
      beforeValidate044( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable044( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency044( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm044( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate044( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000415 */
                  pr_default.execute(13, new Object[] {new Boolean(n20ISODes), A20ISODes, new Boolean(n23ISOCod), A23ISOCod});
                  deferredUpdate044( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel044( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           getByPrimaryKey( ) ;
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel044( ) ;
      }
      closeExtendedTableCursors044( ) ;
   }

   public void deferredUpdate044( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate044( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency044( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls044( ) ;
         scanStart0488( ) ;
         while ( ( RcdFound88 != 0 ) )
         {
            getByPrimaryKey0488( ) ;
            delete0488( ) ;
            scanNext0488( ) ;
         }
         scanEnd0488( ) ;
         scanStart0478( ) ;
         while ( ( RcdFound78 != 0 ) )
         {
            getByPrimaryKey0478( ) ;
            delete0478( ) ;
            scanNext0478( ) ;
         }
         scanEnd0478( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm044( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete044( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000416 */
                  pr_default.execute(14, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      sMode4 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel044( ) ;
      Gx_mode = sMode4 ;
   }

   public void onDeleteControls044( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC000417 */
         pr_default.execute(15, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
         if ( (pr_default.getStatus(15) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"File Downloads"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(15);
         /* Using cursor BC000418 */
         pr_default.execute(16, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
         if ( (pr_default.getStatus(16) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Airline Data"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(16);
         /* Using cursor BC000419 */
         pr_default.execute(17, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
         if ( (pr_default.getStatus(17) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Currencies"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(17);
      }
   }

   public void processNestedLevel0488( )
   {
      nGXsfl_88_idx = (short)(0) ;
      while ( ( nGXsfl_88_idx < bcCountry.getgxTv_SdtCountry_Level1().size() ) )
      {
         readRow0488( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound88 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_88 != 0 ) )
         {
            standaloneNotModal0488( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert0488( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete0488( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update0488( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_88 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_88 = (short)(1) ;
               Gxremove88 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_88 = (short)(0) ;
               Gxremove88 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcCountry.getgxTv_SdtCountry_Level1().removeElement(nGXsfl_88_idx);
            nGXsfl_88_idx = (short)(nGXsfl_88_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow88( ((SdtCountry_CountryParameters)bcCountry.getgxTv_SdtCountry_Level1().elementAt(-1+nGXsfl_88_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll0488( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processNestedLevel0478( )
   {
      nGXsfl_78_idx = (short)(0) ;
      while ( ( nGXsfl_78_idx < bcCountry.getgxTv_SdtCountry_Contacttypes().size() ) )
      {
         readRow0478( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound78 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_78 != 0 ) )
         {
            standaloneNotModal0478( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert0478( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete0478( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update0478( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_78 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_78 = (short)(1) ;
               Gxremove78 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_78 = (short)(0) ;
               Gxremove78 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcCountry.getgxTv_SdtCountry_Contacttypes().removeElement(nGXsfl_78_idx);
            nGXsfl_78_idx = (short)(nGXsfl_78_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow78( ((SdtCountry_ContactTypes)bcCountry.getgxTv_SdtCountry_Contacttypes().elementAt(-1+nGXsfl_78_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll0478( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processNestedLevel0465( )
   {
      nGXsfl_65_idx = (short)(0) ;
      while ( ( nGXsfl_65_idx < bcCountry.getgxTv_SdtCountry_Currencies().size() ) )
      {
         readRow0465( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound65 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_65 != 0 ) )
         {
            standaloneNotModal0465( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert0465( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete0465( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update0465( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_65 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_65 = (short)(1) ;
               Gxremove65 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_65 = (short)(0) ;
               Gxremove65 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcCountry.getgxTv_SdtCountry_Currencies().removeElement(nGXsfl_65_idx);
            nGXsfl_65_idx = (short)(nGXsfl_65_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow65( ((SdtCountry_Currencies)bcCountry.getgxTv_SdtCountry_Currencies().elementAt(-1+nGXsfl_65_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll0465( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processLevel044( )
   {
      /* Save parent mode. */
      sMode4 = Gx_mode ;
      processNestedLevel0488( ) ;
      processNestedLevel0478( ) ;
      processNestedLevel0465( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode4 ;
      /* ' Update level parameters */
   }

   public void endLevel044( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(8);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete044( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues040( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart044( )
   {
      /* Using cursor BC000420 */
      pr_default.execute(18, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      RcdFound4 = (short)(0) ;
      if ( (pr_default.getStatus(18) != 101) )
      {
         RcdFound4 = (short)(1) ;
         A23ISOCod = BC000420_A23ISOCod[0] ;
         n23ISOCod = BC000420_n23ISOCod[0] ;
         A20ISODes = BC000420_A20ISODes[0] ;
         n20ISODes = BC000420_n20ISODes[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext044( )
   {
      pr_default.readNext(18);
      RcdFound4 = (short)(0) ;
      scanLoad044( ) ;
   }

   public void scanLoad044( )
   {
      sMode4 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(18) != 101) )
      {
         RcdFound4 = (short)(1) ;
         A23ISOCod = BC000420_A23ISOCod[0] ;
         n23ISOCod = BC000420_n23ISOCod[0] ;
         A20ISODes = BC000420_A20ISODes[0] ;
         n20ISODes = BC000420_n20ISODes[0] ;
      }
      Gx_mode = sMode4 ;
   }

   public void scanEnd044( )
   {
      pr_default.close(18);
   }

   public void afterConfirm044( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert044( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate044( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete044( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete044( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate044( )
   {
      /* Before Validate Rules */
   }

   public void addRow044( )
   {
      VarsToRow4( bcCountry) ;
   }

   public void sendRow044( )
   {
   }

   public void readRow044( )
   {
      RowToVars4( bcCountry, 0) ;
   }

   public void zm0488( int GX_JID )
   {
      if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
      {
         Z846DateUp = A846DateUp ;
         Z449ISOCon = A449ISOCon ;
         Z450ISOCon = A450ISOCon ;
         Z30lgnLogi = A30lgnLogi ;
         Z20ISODes = A20ISODes ;
         Z365Contac = A365Contac ;
         Z406ISOCtt = A406ISOCtt ;
         Z407ISOCtt = A407ISOCtt ;
         Z408ISOCtt = A408ISOCtt ;
         Z409ISOCtt = A409ISOCtt ;
         Z366Contac = A366Contac ;
         Z349CurCod = A349CurCod ;
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
      }
      if ( ( GX_JID == 18 ) || ( GX_JID == 0 ) )
      {
         Z20ISODes = A20ISODes ;
         Z365Contac = A365Contac ;
         Z406ISOCtt = A406ISOCtt ;
         Z407ISOCtt = A407ISOCtt ;
         Z408ISOCtt = A408ISOCtt ;
         Z409ISOCtt = A409ISOCtt ;
         Z366Contac = A366Contac ;
         Z349CurCod = A349CurCod ;
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
      }
      if ( ( GX_JID == -17 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z448ISOCon = A448ISOCon ;
         Z846DateUp = A846DateUp ;
         Z449ISOCon = A449ISOCon ;
         Z450ISOCon = A450ISOCon ;
         Z30lgnLogi = A30lgnLogi ;
      }
   }

   public void standaloneNotModal0488( )
   {
   }

   public void standaloneModal0488( )
   {
      /* Using cursor BC00049 */
      pr_default.execute(7, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      if ( (pr_default.getStatus(7) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Login Control'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      pr_default.close(7);
   }

   public void load0488( )
   {
      /* Using cursor BC000421 */
      pr_default.execute(19, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A448ISOCon});
      if ( (pr_default.getStatus(19) != 101) )
      {
         RcdFound88 = (short)(1) ;
         A846DateUp = BC000421_A846DateUp[0] ;
         A449ISOCon = BC000421_A449ISOCon[0] ;
         A450ISOCon = BC000421_A450ISOCon[0] ;
         A30lgnLogi = BC000421_A30lgnLogi[0] ;
         n30lgnLogi = BC000421_n30lgnLogi[0] ;
         zm0488( -17) ;
      }
      pr_default.close(19);
      onLoadActions0488( ) ;
   }

   public void onLoadActions0488( )
   {
   }

   public void checkExtendedTable0488( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal0488( ) ;
      Gx_BScreen = (byte)(0) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A448ISOCon), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("ID Config can not be empty", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A449ISOCon), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Description Config can not be empty", 1);
         AnyError = (short)(1) ;
      }
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors0488( )
   {
   }

   public void enableDisable0488( )
   {
   }

   public void getKey0488( )
   {
      /* Using cursor BC000422 */
      pr_default.execute(20, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A448ISOCon});
      if ( (pr_default.getStatus(20) != 101) )
      {
         RcdFound88 = (short)(1) ;
      }
      else
      {
         RcdFound88 = (short)(0) ;
      }
      pr_default.close(20);
   }

   public void getByPrimaryKey0488( )
   {
      /* Using cursor BC00048 */
      pr_default.execute(6, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A448ISOCon});
      if ( (pr_default.getStatus(6) != 101) )
      {
         zm0488( 17) ;
         RcdFound88 = (short)(1) ;
         initializeNonKey0488( ) ;
         A448ISOCon = BC00048_A448ISOCon[0] ;
         A846DateUp = BC00048_A846DateUp[0] ;
         A449ISOCon = BC00048_A449ISOCon[0] ;
         A450ISOCon = BC00048_A450ISOCon[0] ;
         A30lgnLogi = BC00048_A30lgnLogi[0] ;
         n30lgnLogi = BC00048_n30lgnLogi[0] ;
         Z23ISOCod = A23ISOCod ;
         Z448ISOCon = A448ISOCon ;
         sMode88 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0488( ) ;
         load0488( ) ;
         Gx_mode = sMode88 ;
      }
      else
      {
         RcdFound88 = (short)(0) ;
         initializeNonKey0488( ) ;
         sMode88 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0488( ) ;
         Gx_mode = sMode88 ;
      }
      pr_default.close(6);
   }

   public void checkOptimisticConcurrency0488( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00047 */
         pr_default.execute(5, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A448ISOCon});
         if ( ! (pr_default.getStatus(5) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"COUNTRYPARAMETER"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(5) == 101) || !( Z846DateUp.equals( BC00047_A846DateUp[0] ) ) || ( GXutil.strcmp(Z449ISOCon, BC00047_A449ISOCon[0]) != 0 ) || ( GXutil.strcmp(Z450ISOCon, BC00047_A450ISOCon[0]) != 0 ) || ( GXutil.strcmp(Z30lgnLogi, BC00047_A30lgnLogi[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"COUNTRYPARAMETER"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0488( )
   {
      beforeValidate0488( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0488( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0488( 0) ;
         checkOptimisticConcurrency0488( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0488( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0488( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000423 */
                  pr_default.execute(21, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A448ISOCon, A846DateUp, A449ISOCon, A450ISOCon, new Boolean(n30lgnLogi), A30lgnLogi});
                  if ( (pr_default.getStatus(21) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0488( ) ;
         }
         endLevel0488( ) ;
      }
      closeExtendedTableCursors0488( ) ;
   }

   public void update0488( )
   {
      beforeValidate0488( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0488( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0488( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0488( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0488( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000424 */
                  pr_default.execute(22, new Object[] {A846DateUp, A449ISOCon, A450ISOCon, new Boolean(n30lgnLogi), A30lgnLogi, new Boolean(n23ISOCod), A23ISOCod, A448ISOCon});
                  deferredUpdate0488( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey0488( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0488( ) ;
      }
      closeExtendedTableCursors0488( ) ;
   }

   public void deferredUpdate0488( )
   {
   }

   public void delete0488( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0488( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0488( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0488( ) ;
         /* No cascading delete specified. */
         afterConfirm0488( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0488( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000425 */
               pr_default.execute(23, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A448ISOCon});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode88 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0488( ) ;
      Gx_mode = sMode88 ;
   }

   public void onDeleteControls0488( )
   {
      standaloneModal0488( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel0488( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(5);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0488( )
   {
      /* Using cursor BC000426 */
      pr_default.execute(24, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      RcdFound88 = (short)(0) ;
      if ( (pr_default.getStatus(24) != 101) )
      {
         RcdFound88 = (short)(1) ;
         A448ISOCon = BC000426_A448ISOCon[0] ;
         A846DateUp = BC000426_A846DateUp[0] ;
         A449ISOCon = BC000426_A449ISOCon[0] ;
         A450ISOCon = BC000426_A450ISOCon[0] ;
         A30lgnLogi = BC000426_A30lgnLogi[0] ;
         n30lgnLogi = BC000426_n30lgnLogi[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0488( )
   {
      pr_default.readNext(24);
      RcdFound88 = (short)(0) ;
      scanLoad0488( ) ;
   }

   public void scanLoad0488( )
   {
      sMode88 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(24) != 101) )
      {
         RcdFound88 = (short)(1) ;
         A448ISOCon = BC000426_A448ISOCon[0] ;
         A846DateUp = BC000426_A846DateUp[0] ;
         A449ISOCon = BC000426_A449ISOCon[0] ;
         A450ISOCon = BC000426_A450ISOCon[0] ;
         A30lgnLogi = BC000426_A30lgnLogi[0] ;
         n30lgnLogi = BC000426_n30lgnLogi[0] ;
      }
      Gx_mode = sMode88 ;
   }

   public void scanEnd0488( )
   {
      pr_default.close(24);
   }

   public void afterConfirm0488( )
   {
      /* After Confirm Rules */
      if ( ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "INS") == 0 )  ) && true /* Level */ )
      {
         A846DateUp = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      }
      if ( ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "INS") == 0 )  ) && true /* Level */ )
      {
         A30lgnLogi = AV13LgnLog ;
         n30lgnLogi = false ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A450ISOCon))==0)) && true /* Level */ )
      {
         httpContext.GX_msglist.addItem("Value Config can not be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  && true /* Level */ && ( AV16AlterB == 0 ) )
      {
         httpContext.GX_msglist.addItem("User cannot delete data bank!", 1);
         AnyError = (short)(1) ;
         return  ;
      }
   }

   public void beforeInsert0488( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0488( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0488( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0488( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0488( )
   {
      /* Before Validate Rules */
   }

   public void addRow0488( )
   {
      SdtCountry_CountryParameters obj88 ;
      obj88 = new SdtCountry_CountryParameters(remoteHandle);
      VarsToRow88( obj88) ;
      bcCountry.getgxTv_SdtCountry_Level1().add(obj88, 0);
      obj88.setgxTv_SdtCountry_CountryParameters_Mode( "UPD" );
      obj88.setgxTv_SdtCountry_CountryParameters_Modified( (short)(0) );
   }

   public void sendRow0488( )
   {
   }

   public void readRow0488( )
   {
      nGXsfl_88_idx = (short)(nGXsfl_88_idx+1) ;
      RowToVars88( ((SdtCountry_CountryParameters)bcCountry.getgxTv_SdtCountry_Level1().elementAt(-1+nGXsfl_88_idx)), 0) ;
   }

   public void zm0478( int GX_JID )
   {
      if ( ( GX_JID == 19 ) || ( GX_JID == 0 ) )
      {
         Z407ISOCtt = A407ISOCtt ;
         Z408ISOCtt = A408ISOCtt ;
         Z409ISOCtt = A409ISOCtt ;
         Z20ISODes = A20ISODes ;
         Z448ISOCon = A448ISOCon ;
         Z449ISOCon = A449ISOCon ;
         Z450ISOCon = A450ISOCon ;
         Z30lgnLogi = A30lgnLogi ;
         Z846DateUp = A846DateUp ;
         Z349CurCod = A349CurCod ;
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
      }
      if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
      {
         Z366Contac = A366Contac ;
         Z20ISODes = A20ISODes ;
         Z448ISOCon = A448ISOCon ;
         Z449ISOCon = A449ISOCon ;
         Z450ISOCon = A450ISOCon ;
         Z30lgnLogi = A30lgnLogi ;
         Z846DateUp = A846DateUp ;
         Z349CurCod = A349CurCod ;
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
      }
      if ( ( GX_JID == -19 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z406ISOCtt = A406ISOCtt ;
         Z407ISOCtt = A407ISOCtt ;
         Z408ISOCtt = A408ISOCtt ;
         Z409ISOCtt = A409ISOCtt ;
         Z365Contac = A365Contac ;
      }
   }

   public void standaloneNotModal0478( )
   {
   }

   public void standaloneModal0478( )
   {
   }

   public void load0478( )
   {
      /* Using cursor BC000427 */
      pr_default.execute(25, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A365Contac, new Short(A406ISOCtt)});
      if ( (pr_default.getStatus(25) != 101) )
      {
         RcdFound78 = (short)(1) ;
         A407ISOCtt = BC000427_A407ISOCtt[0] ;
         A408ISOCtt = BC000427_A408ISOCtt[0] ;
         A409ISOCtt = BC000427_A409ISOCtt[0] ;
         A366Contac = BC000427_A366Contac[0] ;
         zm0478( -19) ;
      }
      pr_default.close(25);
      onLoadActions0478( ) ;
   }

   public void onLoadActions0478( )
   {
   }

   public void checkExtendedTable0478( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal0478( ) ;
      Gx_BScreen = (byte)(0) ;
      /* Using cursor BC00046 */
      pr_default.execute(4, new Object[] {A365Contac});
      if ( (pr_default.getStatus(4) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Contact Types'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A366Contac = BC00046_A366Contac[0] ;
      pr_default.close(4);
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors0478( )
   {
      pr_default.close(4);
   }

   public void enableDisable0478( )
   {
   }

   public void getKey0478( )
   {
      /* Using cursor BC000428 */
      pr_default.execute(26, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A365Contac, new Short(A406ISOCtt)});
      if ( (pr_default.getStatus(26) != 101) )
      {
         RcdFound78 = (short)(1) ;
      }
      else
      {
         RcdFound78 = (short)(0) ;
      }
      pr_default.close(26);
   }

   public void getByPrimaryKey0478( )
   {
      /* Using cursor BC00045 */
      pr_default.execute(3, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A365Contac, new Short(A406ISOCtt)});
      if ( (pr_default.getStatus(3) != 101) )
      {
         zm0478( 19) ;
         RcdFound78 = (short)(1) ;
         initializeNonKey0478( ) ;
         A406ISOCtt = BC00045_A406ISOCtt[0] ;
         A407ISOCtt = BC00045_A407ISOCtt[0] ;
         A408ISOCtt = BC00045_A408ISOCtt[0] ;
         A409ISOCtt = BC00045_A409ISOCtt[0] ;
         A365Contac = BC00045_A365Contac[0] ;
         Z23ISOCod = A23ISOCod ;
         Z365Contac = A365Contac ;
         Z406ISOCtt = A406ISOCtt ;
         sMode78 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0478( ) ;
         load0478( ) ;
         Gx_mode = sMode78 ;
      }
      else
      {
         RcdFound78 = (short)(0) ;
         initializeNonKey0478( ) ;
         sMode78 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0478( ) ;
         Gx_mode = sMode78 ;
      }
      pr_default.close(3);
   }

   public void checkOptimisticConcurrency0478( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00044 */
         pr_default.execute(2, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A365Contac, new Short(A406ISOCtt)});
         if ( ! (pr_default.getStatus(2) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"COUNTRYCONTACTTYPES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(2) == 101) || ( GXutil.strcmp(Z407ISOCtt, BC00044_A407ISOCtt[0]) != 0 ) || ( GXutil.strcmp(Z408ISOCtt, BC00044_A408ISOCtt[0]) != 0 ) || ( GXutil.strcmp(Z409ISOCtt, BC00044_A409ISOCtt[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"COUNTRYCONTACTTYPES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0478( )
   {
      beforeValidate0478( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0478( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0478( 0) ;
         checkOptimisticConcurrency0478( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0478( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0478( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000429 */
                  pr_default.execute(27, new Object[] {new Boolean(n23ISOCod), A23ISOCod, new Short(A406ISOCtt), A407ISOCtt, A408ISOCtt, A409ISOCtt, A365Contac});
                  if ( (pr_default.getStatus(27) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0478( ) ;
         }
         endLevel0478( ) ;
      }
      closeExtendedTableCursors0478( ) ;
   }

   public void update0478( )
   {
      beforeValidate0478( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0478( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0478( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0478( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0478( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000430 */
                  pr_default.execute(28, new Object[] {A407ISOCtt, A408ISOCtt, A409ISOCtt, new Boolean(n23ISOCod), A23ISOCod, A365Contac, new Short(A406ISOCtt)});
                  deferredUpdate0478( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey0478( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0478( ) ;
      }
      closeExtendedTableCursors0478( ) ;
   }

   public void deferredUpdate0478( )
   {
   }

   public void delete0478( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0478( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0478( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0478( ) ;
         /* No cascading delete specified. */
         afterConfirm0478( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0478( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000431 */
               pr_default.execute(29, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A365Contac, new Short(A406ISOCtt)});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode78 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0478( ) ;
      Gx_mode = sMode78 ;
   }

   public void onDeleteControls0478( )
   {
      standaloneModal0478( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor BC000432 */
         pr_default.execute(30, new Object[] {A365Contac});
         A366Contac = BC000432_A366Contac[0] ;
         pr_default.close(30);
      }
   }

   public void endLevel0478( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(2);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0478( )
   {
      /* Using cursor BC000433 */
      pr_default.execute(31, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      RcdFound78 = (short)(0) ;
      if ( (pr_default.getStatus(31) != 101) )
      {
         RcdFound78 = (short)(1) ;
         A406ISOCtt = BC000433_A406ISOCtt[0] ;
         A407ISOCtt = BC000433_A407ISOCtt[0] ;
         A408ISOCtt = BC000433_A408ISOCtt[0] ;
         A409ISOCtt = BC000433_A409ISOCtt[0] ;
         A366Contac = BC000433_A366Contac[0] ;
         A365Contac = BC000433_A365Contac[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0478( )
   {
      pr_default.readNext(31);
      RcdFound78 = (short)(0) ;
      scanLoad0478( ) ;
   }

   public void scanLoad0478( )
   {
      sMode78 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(31) != 101) )
      {
         RcdFound78 = (short)(1) ;
         A406ISOCtt = BC000433_A406ISOCtt[0] ;
         A407ISOCtt = BC000433_A407ISOCtt[0] ;
         A408ISOCtt = BC000433_A408ISOCtt[0] ;
         A409ISOCtt = BC000433_A409ISOCtt[0] ;
         A366Contac = BC000433_A366Contac[0] ;
         A365Contac = BC000433_A365Contac[0] ;
      }
      Gx_mode = sMode78 ;
   }

   public void scanEnd0478( )
   {
      pr_default.close(31);
   }

   public void afterConfirm0478( )
   {
      /* After Confirm Rules */
      if ( true /* Level */ && ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         GXt_int6 = A406ISOCtt ;
         GXv_int7[0] = GXt_int6 ;
         new pcountrycttseq(remoteHandle, context).execute( A23ISOCod, A365Contac, GXv_int7) ;
         tcountry_bc.this.GXt_int6 = GXv_int7[0] ;
         A406ISOCtt = GXt_int6 ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A407ISOCtt))==0)) && true /* Level */ )
      {
         httpContext.GX_msglist.addItem("Contact Name can not be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A409ISOCtt))==0)) && true /* Level */ )
      {
         httpContext.GX_msglist.addItem("E-mail can not be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
   }

   public void beforeInsert0478( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0478( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0478( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0478( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0478( )
   {
      /* Before Validate Rules */
   }

   public void addRow0478( )
   {
      SdtCountry_ContactTypes obj78 ;
      obj78 = new SdtCountry_ContactTypes(remoteHandle);
      VarsToRow78( obj78) ;
      bcCountry.getgxTv_SdtCountry_Contacttypes().add(obj78, 0);
      obj78.setgxTv_SdtCountry_ContactTypes_Mode( "UPD" );
      obj78.setgxTv_SdtCountry_ContactTypes_Modified( (short)(0) );
   }

   public void sendRow0478( )
   {
   }

   public void readRow0478( )
   {
      nGXsfl_78_idx = (short)(nGXsfl_78_idx+1) ;
      RowToVars78( ((SdtCountry_ContactTypes)bcCountry.getgxTv_SdtCountry_Contacttypes().elementAt(-1+nGXsfl_78_idx)), 0) ;
   }

   public void zm0465( int GX_JID )
   {
      if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
      {
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
         Z20ISODes = A20ISODes ;
         Z448ISOCon = A448ISOCon ;
         Z449ISOCon = A449ISOCon ;
         Z450ISOCon = A450ISOCon ;
         Z30lgnLogi = A30lgnLogi ;
         Z846DateUp = A846DateUp ;
         Z365Contac = A365Contac ;
         Z406ISOCtt = A406ISOCtt ;
         Z407ISOCtt = A407ISOCtt ;
         Z408ISOCtt = A408ISOCtt ;
         Z409ISOCtt = A409ISOCtt ;
         Z366Contac = A366Contac ;
      }
      if ( ( GX_JID == -21 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z349CurCod = A349CurCod ;
         Z350CurPla = A350CurPla ;
         Z351CurDes = A351CurDes ;
      }
   }

   public void standaloneNotModal0465( )
   {
   }

   public void standaloneModal0465( )
   {
   }

   public void load0465( )
   {
      /* Using cursor BC000434 */
      pr_default.execute(32, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
      if ( (pr_default.getStatus(32) != 101) )
      {
         RcdFound65 = (short)(1) ;
         A350CurPla = BC000434_A350CurPla[0] ;
         n350CurPla = BC000434_n350CurPla[0] ;
         A351CurDes = BC000434_A351CurDes[0] ;
         n351CurDes = BC000434_n351CurDes[0] ;
         zm0465( -21) ;
      }
      pr_default.close(32);
      onLoadActions0465( ) ;
   }

   public void onLoadActions0465( )
   {
   }

   public void checkExtendedTable0465( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal0465( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors0465( )
   {
   }

   public void enableDisable0465( )
   {
   }

   public void getKey0465( )
   {
      /* Using cursor BC000435 */
      pr_default.execute(33, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
      if ( (pr_default.getStatus(33) != 101) )
      {
         RcdFound65 = (short)(1) ;
      }
      else
      {
         RcdFound65 = (short)(0) ;
      }
      pr_default.close(33);
   }

   public void getByPrimaryKey0465( )
   {
      /* Using cursor BC00043 */
      pr_default.execute(1, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0465( 21) ;
         RcdFound65 = (short)(1) ;
         initializeNonKey0465( ) ;
         A349CurCod = BC00043_A349CurCod[0] ;
         A350CurPla = BC00043_A350CurPla[0] ;
         n350CurPla = BC00043_n350CurPla[0] ;
         A351CurDes = BC00043_A351CurDes[0] ;
         n351CurDes = BC00043_n351CurDes[0] ;
         Z23ISOCod = A23ISOCod ;
         Z349CurCod = A349CurCod ;
         sMode65 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0465( ) ;
         load0465( ) ;
         Gx_mode = sMode65 ;
      }
      else
      {
         RcdFound65 = (short)(0) ;
         initializeNonKey0465( ) ;
         sMode65 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0465( ) ;
         Gx_mode = sMode65 ;
      }
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency0465( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00042 */
         pr_default.execute(0, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"COUNTRYCURRENCIES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( Z350CurPla != BC00042_A350CurPla[0] ) || ( GXutil.strcmp(Z351CurDes, BC00042_A351CurDes[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"COUNTRYCURRENCIES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0465( )
   {
      beforeValidate0465( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0465( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0465( 0) ;
         checkOptimisticConcurrency0465( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0465( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0465( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000436 */
                  pr_default.execute(34, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod, new Boolean(n350CurPla), new Byte(A350CurPla), new Boolean(n351CurDes), A351CurDes});
                  if ( (pr_default.getStatus(34) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0465( ) ;
         }
         endLevel0465( ) ;
      }
      closeExtendedTableCursors0465( ) ;
   }

   public void update0465( )
   {
      beforeValidate0465( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0465( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0465( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0465( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0465( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000437 */
                  pr_default.execute(35, new Object[] {new Boolean(n350CurPla), new Byte(A350CurPla), new Boolean(n351CurDes), A351CurDes, new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
                  deferredUpdate0465( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey0465( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0465( ) ;
      }
      closeExtendedTableCursors0465( ) ;
   }

   public void deferredUpdate0465( )
   {
   }

   public void delete0465( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0465( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0465( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0465( ) ;
         /* No cascading delete specified. */
         afterConfirm0465( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0465( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000438 */
               pr_default.execute(36, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode65 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0465( ) ;
      Gx_mode = sMode65 ;
   }

   public void onDeleteControls0465( )
   {
      standaloneModal0465( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC000439 */
         pr_default.execute(37, new Object[] {new Boolean(n23ISOCod), A23ISOCod, A349CurCod});
         if ( (pr_default.getStatus(37) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Period Configuration"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(37);
      }
   }

   public void endLevel0465( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0465( )
   {
      /* Using cursor BC000440 */
      pr_default.execute(38, new Object[] {new Boolean(n23ISOCod), A23ISOCod});
      RcdFound65 = (short)(0) ;
      if ( (pr_default.getStatus(38) != 101) )
      {
         RcdFound65 = (short)(1) ;
         A349CurCod = BC000440_A349CurCod[0] ;
         A350CurPla = BC000440_A350CurPla[0] ;
         n350CurPla = BC000440_n350CurPla[0] ;
         A351CurDes = BC000440_A351CurDes[0] ;
         n351CurDes = BC000440_n351CurDes[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0465( )
   {
      pr_default.readNext(38);
      RcdFound65 = (short)(0) ;
      scanLoad0465( ) ;
   }

   public void scanLoad0465( )
   {
      sMode65 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(38) != 101) )
      {
         RcdFound65 = (short)(1) ;
         A349CurCod = BC000440_A349CurCod[0] ;
         A350CurPla = BC000440_A350CurPla[0] ;
         n350CurPla = BC000440_n350CurPla[0] ;
         A351CurDes = BC000440_A351CurDes[0] ;
         n351CurDes = BC000440_n351CurDes[0] ;
      }
      Gx_mode = sMode65 ;
   }

   public void scanEnd0465( )
   {
      pr_default.close(38);
   }

   public void afterConfirm0465( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0465( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0465( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0465( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0465( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0465( )
   {
      /* Before Validate Rules */
   }

   public void addRow0465( )
   {
      SdtCountry_Currencies obj65 ;
      obj65 = new SdtCountry_Currencies(remoteHandle);
      VarsToRow65( obj65) ;
      bcCountry.getgxTv_SdtCountry_Currencies().add(obj65, 0);
      obj65.setgxTv_SdtCountry_Currencies_Mode( "UPD" );
      obj65.setgxTv_SdtCountry_Currencies_Modified( (short)(0) );
   }

   public void sendRow0465( )
   {
   }

   public void readRow0465( )
   {
      nGXsfl_65_idx = (short)(nGXsfl_65_idx+1) ;
      RowToVars65( ((SdtCountry_Currencies)bcCountry.getgxTv_SdtCountry_Currencies().elementAt(-1+nGXsfl_65_idx)), 0) ;
   }

   public void confirmValues040( )
   {
   }

   public void initializeNonKey044( )
   {
      A20ISODes = "" ;
      n20ISODes = false ;
   }

   public void initAll044( )
   {
      A23ISOCod = "" ;
      n23ISOCod = false ;
      initializeNonKey044( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void initializeNonKey0488( )
   {
      A30lgnLogi = "" ;
      n30lgnLogi = false ;
      A846DateUp = GXutil.resetTime( GXutil.nullDate() );
      A449ISOCon = "" ;
      A450ISOCon = "" ;
   }

   public void initAll0488( )
   {
      A448ISOCon = "" ;
      initializeNonKey0488( ) ;
   }

   public void standaloneModalInsert0488( )
   {
   }

   public void initializeNonKey0478( )
   {
      A407ISOCtt = "" ;
      A408ISOCtt = "" ;
      A409ISOCtt = "" ;
      A366Contac = "" ;
   }

   public void initAll0478( )
   {
      A365Contac = "" ;
      A406ISOCtt = (short)(0) ;
      initializeNonKey0478( ) ;
   }

   public void standaloneModalInsert0478( )
   {
   }

   public void initializeNonKey0465( )
   {
      A350CurPla = (byte)(0) ;
      n350CurPla = false ;
      A351CurDes = "" ;
      n351CurDes = false ;
   }

   public void initAll0465( )
   {
      A349CurCod = "" ;
      initializeNonKey0465( ) ;
   }

   public void standaloneModalInsert0465( )
   {
   }

   public void VarsToRow4( SdtCountry obj4 )
   {
      obj4.setgxTv_SdtCountry_Mode( Gx_mode );
      obj4.setgxTv_SdtCountry_Isodes( A20ISODes );
      obj4.setgxTv_SdtCountry_Isocod( A23ISOCod );
      obj4.setgxTv_SdtCountry_Isocod_Z( Z23ISOCod );
      obj4.setgxTv_SdtCountry_Isodes_Z( Z20ISODes );
      obj4.setgxTv_SdtCountry_Isocod_N( (byte)((byte)((n23ISOCod)?1:0)) );
      obj4.setgxTv_SdtCountry_Isodes_N( (byte)((byte)((n20ISODes)?1:0)) );
      obj4.setgxTv_SdtCountry_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars4( SdtCountry obj4 ,
                           int forceLoad )
   {
      Gx_mode = obj4.getgxTv_SdtCountry_Mode() ;
      A20ISODes = obj4.getgxTv_SdtCountry_Isodes() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A23ISOCod = obj4.getgxTv_SdtCountry_Isocod() ;
      }
      Z23ISOCod = obj4.getgxTv_SdtCountry_Isocod_Z() ;
      Z20ISODes = obj4.getgxTv_SdtCountry_Isodes_Z() ;
      n23ISOCod = (boolean)((obj4.getgxTv_SdtCountry_Isocod_N()==0)?false:true) ;
      n20ISODes = (boolean)((obj4.getgxTv_SdtCountry_Isodes_N()==0)?false:true) ;
      Gx_mode = obj4.getgxTv_SdtCountry_Mode() ;
      return  ;
   }

   public void VarsToRow88( SdtCountry_CountryParameters obj88 )
   {
      obj88.setgxTv_SdtCountry_CountryParameters_Mode( Gx_mode );
      obj88.setgxTv_SdtCountry_CountryParameters_Lgnlogin( A30lgnLogi );
      obj88.setgxTv_SdtCountry_CountryParameters_Dateupd( A846DateUp );
      obj88.setgxTv_SdtCountry_CountryParameters_Isoconfigdes( A449ISOCon );
      obj88.setgxTv_SdtCountry_CountryParameters_Isoconfigvalue( A450ISOCon );
      obj88.setgxTv_SdtCountry_CountryParameters_Isoconfigid( A448ISOCon );
      obj88.setgxTv_SdtCountry_CountryParameters_Isoconfigid_Z( Z448ISOCon );
      obj88.setgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z( Z449ISOCon );
      obj88.setgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z( Z450ISOCon );
      obj88.setgxTv_SdtCountry_CountryParameters_Lgnlogin_Z( Z30lgnLogi );
      obj88.setgxTv_SdtCountry_CountryParameters_Dateupd_Z( Z846DateUp );
      obj88.setgxTv_SdtCountry_CountryParameters_Lgnlogin_N( (byte)((byte)((n30lgnLogi)?1:0)) );
      obj88.setgxTv_SdtCountry_CountryParameters_Modified( nIsMod_88 );
      return  ;
   }

   public void RowToVars88( SdtCountry_CountryParameters obj88 ,
                            int forceLoad )
   {
      Gx_mode = obj88.getgxTv_SdtCountry_CountryParameters_Mode() ;
      if ( ( forceLoad == 1 ) )
      {
         A30lgnLogi = obj88.getgxTv_SdtCountry_CountryParameters_Lgnlogin() ;
      }
      if ( ( forceLoad == 1 ) )
      {
         A846DateUp = obj88.getgxTv_SdtCountry_CountryParameters_Dateupd() ;
      }
      if ( ! ( ( AV16AlterB == 0 ) ) || ( forceLoad == 1 ) )
      {
         A449ISOCon = obj88.getgxTv_SdtCountry_CountryParameters_Isoconfigdes() ;
      }
      if ( ! ( ( AV16AlterB == 0 ) ) || ( forceLoad == 1 ) )
      {
         A450ISOCon = obj88.getgxTv_SdtCountry_CountryParameters_Isoconfigvalue() ;
      }
      if ( ! ( ( AV16AlterB == 0 ) ) || ( forceLoad == 1 ) )
      {
         A448ISOCon = obj88.getgxTv_SdtCountry_CountryParameters_Isoconfigid() ;
      }
      Z448ISOCon = obj88.getgxTv_SdtCountry_CountryParameters_Isoconfigid_Z() ;
      Z449ISOCon = obj88.getgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z() ;
      Z450ISOCon = obj88.getgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z() ;
      Z30lgnLogi = obj88.getgxTv_SdtCountry_CountryParameters_Lgnlogin_Z() ;
      Z846DateUp = obj88.getgxTv_SdtCountry_CountryParameters_Dateupd_Z() ;
      n30lgnLogi = (boolean)((obj88.getgxTv_SdtCountry_CountryParameters_Lgnlogin_N()==0)?false:true) ;
      nIsMod_88 = obj88.getgxTv_SdtCountry_CountryParameters_Modified() ;
      return  ;
   }

   public void VarsToRow78( SdtCountry_ContactTypes obj78 )
   {
      obj78.setgxTv_SdtCountry_ContactTypes_Mode( Gx_mode );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttname( A407ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttphone( A408ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttemail( A409ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Contacttypesdescription( A366Contac );
      obj78.setgxTv_SdtCountry_ContactTypes_Contacttypescode( A365Contac );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttseq( A406ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Contacttypescode_Z( Z365Contac );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttseq_Z( Z406ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttname_Z( Z407ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttphone_Z( Z408ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Isocttemail_Z( Z409ISOCtt );
      obj78.setgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z( Z366Contac );
      obj78.setgxTv_SdtCountry_ContactTypes_Modified( nIsMod_78 );
      return  ;
   }

   public void RowToVars78( SdtCountry_ContactTypes obj78 ,
                            int forceLoad )
   {
      Gx_mode = obj78.getgxTv_SdtCountry_ContactTypes_Mode() ;
      A407ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttname() ;
      A408ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttphone() ;
      A409ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttemail() ;
      A366Contac = obj78.getgxTv_SdtCountry_ContactTypes_Contacttypesdescription() ;
      A365Contac = obj78.getgxTv_SdtCountry_ContactTypes_Contacttypescode() ;
      A406ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttseq() ;
      Z365Contac = obj78.getgxTv_SdtCountry_ContactTypes_Contacttypescode_Z() ;
      Z406ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttseq_Z() ;
      Z407ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttname_Z() ;
      Z408ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttphone_Z() ;
      Z409ISOCtt = obj78.getgxTv_SdtCountry_ContactTypes_Isocttemail_Z() ;
      Z366Contac = obj78.getgxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z() ;
      nIsMod_78 = obj78.getgxTv_SdtCountry_ContactTypes_Modified() ;
      return  ;
   }

   public void VarsToRow65( SdtCountry_Currencies obj65 )
   {
      obj65.setgxTv_SdtCountry_Currencies_Mode( Gx_mode );
      obj65.setgxTv_SdtCountry_Currencies_Curplaces( A350CurPla );
      obj65.setgxTv_SdtCountry_Currencies_Curdescription( A351CurDes );
      obj65.setgxTv_SdtCountry_Currencies_Curcode( A349CurCod );
      obj65.setgxTv_SdtCountry_Currencies_Curcode_Z( Z349CurCod );
      obj65.setgxTv_SdtCountry_Currencies_Curplaces_Z( Z350CurPla );
      obj65.setgxTv_SdtCountry_Currencies_Curdescription_Z( Z351CurDes );
      obj65.setgxTv_SdtCountry_Currencies_Curplaces_N( (byte)((byte)((n350CurPla)?1:0)) );
      obj65.setgxTv_SdtCountry_Currencies_Curdescription_N( (byte)((byte)((n351CurDes)?1:0)) );
      obj65.setgxTv_SdtCountry_Currencies_Modified( nIsMod_65 );
      return  ;
   }

   public void RowToVars65( SdtCountry_Currencies obj65 ,
                            int forceLoad )
   {
      Gx_mode = obj65.getgxTv_SdtCountry_Currencies_Mode() ;
      A350CurPla = obj65.getgxTv_SdtCountry_Currencies_Curplaces() ;
      A351CurDes = obj65.getgxTv_SdtCountry_Currencies_Curdescription() ;
      A349CurCod = obj65.getgxTv_SdtCountry_Currencies_Curcode() ;
      Z349CurCod = obj65.getgxTv_SdtCountry_Currencies_Curcode_Z() ;
      Z350CurPla = obj65.getgxTv_SdtCountry_Currencies_Curplaces_Z() ;
      Z351CurDes = obj65.getgxTv_SdtCountry_Currencies_Curdescription_Z() ;
      n350CurPla = (boolean)((obj65.getgxTv_SdtCountry_Currencies_Curplaces_N()==0)?false:true) ;
      n351CurDes = (boolean)((obj65.getgxTv_SdtCountry_Currencies_Curdescription_N()==0)?false:true) ;
      nIsMod_65 = obj65.getgxTv_SdtCountry_Currencies_Modified() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A23ISOCod = (String)obj[0] ;
      n23ISOCod = false ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey044( ) ;
      scanStart044( ) ;
      if ( ( RcdFound4 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z23ISOCod = A23ISOCod ;
      }
      onLoadActions044( ) ;
      zm044( 0) ;
      addRow044( ) ;
      bcCountry.getgxTv_SdtCountry_Level1().clearCollection();
      if ( ( RcdFound4 == 1 ) )
      {
         scanStart0488( ) ;
         nGXsfl_88_idx = (short)(1) ;
         while ( ( RcdFound88 != 0 ) )
         {
            onLoadActions0488( ) ;
            Z23ISOCod = A23ISOCod ;
            Z448ISOCon = A448ISOCon ;
            zm0488( 0) ;
            nRcdExists_88 = (short)(1) ;
            nIsMod_88 = (short)(0) ;
            addRow0488( ) ;
            nGXsfl_88_idx = (short)(nGXsfl_88_idx+1) ;
            scanNext0488( ) ;
         }
         scanEnd0488( ) ;
      }
      bcCountry.getgxTv_SdtCountry_Contacttypes().clearCollection();
      if ( ( RcdFound4 == 1 ) )
      {
         scanStart0478( ) ;
         nGXsfl_78_idx = (short)(1) ;
         while ( ( RcdFound78 != 0 ) )
         {
            onLoadActions0478( ) ;
            Z23ISOCod = A23ISOCod ;
            Z365Contac = A365Contac ;
            Z406ISOCtt = A406ISOCtt ;
            zm0478( 0) ;
            nRcdExists_78 = (short)(1) ;
            nIsMod_78 = (short)(0) ;
            addRow0478( ) ;
            nGXsfl_78_idx = (short)(nGXsfl_78_idx+1) ;
            scanNext0478( ) ;
         }
         scanEnd0478( ) ;
      }
      bcCountry.getgxTv_SdtCountry_Currencies().clearCollection();
      if ( ( RcdFound4 == 1 ) )
      {
         scanStart0465( ) ;
         nGXsfl_65_idx = (short)(1) ;
         while ( ( RcdFound65 != 0 ) )
         {
            onLoadActions0465( ) ;
            Z23ISOCod = A23ISOCod ;
            Z349CurCod = A349CurCod ;
            zm0465( 0) ;
            nRcdExists_65 = (short)(1) ;
            nIsMod_65 = (short)(0) ;
            addRow0465( ) ;
            nGXsfl_65_idx = (short)(nGXsfl_65_idx+1) ;
            scanNext0465( ) ;
         }
         scanEnd0465( ) ;
      }
      scanEnd044( ) ;
      if ( ( RcdFound4 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars4( bcCountry, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey044( ) ;
      if ( ( RcdFound4 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            n23ISOCod = false ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update044( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert044( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert044( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow4( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars4( bcCountry, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey044( ) ;
      if ( ( RcdFound4 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            n23ISOCod = false ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      pr_default.close(30);
      Application.rollback(context, remoteHandle, "DEFAULT", "tcountry_bc");
      VarsToRow4( bcCountry) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcCountry.getgxTv_SdtCountry_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcCountry.setgxTv_SdtCountry_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtCountry sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcCountry ) )
      {
         bcCountry = sdt ;
         if ( ( GXutil.strcmp(bcCountry.getgxTv_SdtCountry_Mode(), "") == 0 ) )
         {
            bcCountry.setgxTv_SdtCountry_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow4( bcCountry) ;
         }
         else
         {
            RowToVars4( bcCountry, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcCountry.getgxTv_SdtCountry_Mode(), "") == 0 ) )
         {
            bcCountry.setgxTv_SdtCountry_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars4( bcCountry, 1) ;
      return  ;
   }

   public SdtCountry getCountry_BC( )
   {
      return bcCountry ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(30);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z23ISOCod = "" ;
      A23ISOCod = "" ;
      sMode4 = "" ;
      nIsMod_65 = (short)(0) ;
      RcdFound65 = (short)(0) ;
      nIsMod_78 = (short)(0) ;
      RcdFound78 = (short)(0) ;
      nIsMod_88 = (short)(0) ;
      RcdFound88 = (short)(0) ;
      AV13LgnLog = "" ;
      AV11Sessio = httpContext.getWebSession();
      AV16AlterB = (byte)(0) ;
      GXt_int1 = (byte)(0) ;
      GXv_svchar2 = new String [1] ;
      GXv_svchar3 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_int5 = new byte [1] ;
      gxTv_SdtCountry_Isocod_Z = "" ;
      gxTv_SdtCountry_Isodes_Z = "" ;
      gxTv_SdtCountry_Isocod_N = (byte)(0) ;
      gxTv_SdtCountry_Isodes_N = (byte)(0) ;
      gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Dateupd_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(0) ;
      gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttseq_Z = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Isocttname_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttphone_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttemail_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = "" ;
      gxTv_SdtCountry_Currencies_Curcode_Z = "" ;
      gxTv_SdtCountry_Currencies_Curplaces_Z = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription_Z = "" ;
      gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(0) ;
      GX_JID = 0 ;
      Z20ISODes = "" ;
      A20ISODes = "" ;
      Z448ISOCon = "" ;
      A448ISOCon = "" ;
      Z449ISOCon = "" ;
      A449ISOCon = "" ;
      Z450ISOCon = "" ;
      A450ISOCon = "" ;
      Z30lgnLogi = "" ;
      A30lgnLogi = "" ;
      Z846DateUp = GXutil.resetTime( GXutil.nullDate() );
      A846DateUp = GXutil.resetTime( GXutil.nullDate() );
      Z365Contac = "" ;
      A365Contac = "" ;
      Z406ISOCtt = (short)(0) ;
      A406ISOCtt = (short)(0) ;
      Z407ISOCtt = "" ;
      A407ISOCtt = "" ;
      Z408ISOCtt = "" ;
      A408ISOCtt = "" ;
      Z409ISOCtt = "" ;
      A409ISOCtt = "" ;
      Z366Contac = "" ;
      A366Contac = "" ;
      Z349CurCod = "" ;
      A349CurCod = "" ;
      Z350CurPla = (byte)(0) ;
      A350CurPla = (byte)(0) ;
      Z351CurDes = "" ;
      A351CurDes = "" ;
      n23ISOCod = false ;
      BC000412_A23ISOCod = new String[] {""} ;
      BC000412_n23ISOCod = new boolean[] {false} ;
      BC000412_A20ISODes = new String[] {""} ;
      BC000412_n20ISODes = new boolean[] {false} ;
      RcdFound4 = (short)(0) ;
      n20ISODes = false ;
      BC000413_A23ISOCod = new String[] {""} ;
      BC000413_n23ISOCod = new boolean[] {false} ;
      BC000411_A23ISOCod = new String[] {""} ;
      BC000411_n23ISOCod = new boolean[] {false} ;
      BC000411_A20ISODes = new String[] {""} ;
      BC000411_n20ISODes = new boolean[] {false} ;
      BC000410_A23ISOCod = new String[] {""} ;
      BC000410_n23ISOCod = new boolean[] {false} ;
      BC000410_A20ISODes = new String[] {""} ;
      BC000410_n20ISODes = new boolean[] {false} ;
      BC000417_A42Downloa = new long[1] ;
      BC000418_A23ISOCod = new String[] {""} ;
      BC000418_n23ISOCod = new boolean[] {false} ;
      BC000418_A1136AirLi = new String[] {""} ;
      BC000419_A23ISOCod = new String[] {""} ;
      BC000419_n23ISOCod = new boolean[] {false} ;
      BC000419_A349CurCod = new String[] {""} ;
      nRcdExists_88 = (short)(0) ;
      Gxremove88 = (byte)(0) ;
      nRcdExists_78 = (short)(0) ;
      Gxremove78 = (byte)(0) ;
      nRcdExists_65 = (short)(0) ;
      Gxremove65 = (byte)(0) ;
      BC000420_A23ISOCod = new String[] {""} ;
      BC000420_n23ISOCod = new boolean[] {false} ;
      BC000420_A20ISODes = new String[] {""} ;
      BC000420_n20ISODes = new boolean[] {false} ;
      n30lgnLogi = false ;
      BC00049_A30lgnLogi = new String[] {""} ;
      BC00049_n30lgnLogi = new boolean[] {false} ;
      BC000421_A23ISOCod = new String[] {""} ;
      BC000421_n23ISOCod = new boolean[] {false} ;
      BC000421_A448ISOCon = new String[] {""} ;
      BC000421_A846DateUp = new java.util.Date[] {GXutil.nullDate()} ;
      BC000421_A449ISOCon = new String[] {""} ;
      BC000421_A450ISOCon = new String[] {""} ;
      BC000421_A30lgnLogi = new String[] {""} ;
      BC000421_n30lgnLogi = new boolean[] {false} ;
      Gx_BScreen = (byte)(0) ;
      BC000422_A23ISOCod = new String[] {""} ;
      BC000422_n23ISOCod = new boolean[] {false} ;
      BC000422_A448ISOCon = new String[] {""} ;
      BC00048_A23ISOCod = new String[] {""} ;
      BC00048_n23ISOCod = new boolean[] {false} ;
      BC00048_A448ISOCon = new String[] {""} ;
      BC00048_A846DateUp = new java.util.Date[] {GXutil.nullDate()} ;
      BC00048_A449ISOCon = new String[] {""} ;
      BC00048_A450ISOCon = new String[] {""} ;
      BC00048_A30lgnLogi = new String[] {""} ;
      BC00048_n30lgnLogi = new boolean[] {false} ;
      sMode88 = "" ;
      BC00047_A23ISOCod = new String[] {""} ;
      BC00047_n23ISOCod = new boolean[] {false} ;
      BC00047_A448ISOCon = new String[] {""} ;
      BC00047_A846DateUp = new java.util.Date[] {GXutil.nullDate()} ;
      BC00047_A449ISOCon = new String[] {""} ;
      BC00047_A450ISOCon = new String[] {""} ;
      BC00047_A30lgnLogi = new String[] {""} ;
      BC00047_n30lgnLogi = new boolean[] {false} ;
      BC000426_A23ISOCod = new String[] {""} ;
      BC000426_n23ISOCod = new boolean[] {false} ;
      BC000426_A448ISOCon = new String[] {""} ;
      BC000426_A846DateUp = new java.util.Date[] {GXutil.nullDate()} ;
      BC000426_A449ISOCon = new String[] {""} ;
      BC000426_A450ISOCon = new String[] {""} ;
      BC000426_A30lgnLogi = new String[] {""} ;
      BC000426_n30lgnLogi = new boolean[] {false} ;
      BC000427_A23ISOCod = new String[] {""} ;
      BC000427_n23ISOCod = new boolean[] {false} ;
      BC000427_A406ISOCtt = new short[1] ;
      BC000427_A407ISOCtt = new String[] {""} ;
      BC000427_A408ISOCtt = new String[] {""} ;
      BC000427_A409ISOCtt = new String[] {""} ;
      BC000427_A366Contac = new String[] {""} ;
      BC000427_A365Contac = new String[] {""} ;
      BC00046_A366Contac = new String[] {""} ;
      BC000428_A23ISOCod = new String[] {""} ;
      BC000428_n23ISOCod = new boolean[] {false} ;
      BC000428_A365Contac = new String[] {""} ;
      BC000428_A406ISOCtt = new short[1] ;
      BC00045_A23ISOCod = new String[] {""} ;
      BC00045_n23ISOCod = new boolean[] {false} ;
      BC00045_A406ISOCtt = new short[1] ;
      BC00045_A407ISOCtt = new String[] {""} ;
      BC00045_A408ISOCtt = new String[] {""} ;
      BC00045_A409ISOCtt = new String[] {""} ;
      BC00045_A365Contac = new String[] {""} ;
      sMode78 = "" ;
      BC00044_A23ISOCod = new String[] {""} ;
      BC00044_n23ISOCod = new boolean[] {false} ;
      BC00044_A406ISOCtt = new short[1] ;
      BC00044_A407ISOCtt = new String[] {""} ;
      BC00044_A408ISOCtt = new String[] {""} ;
      BC00044_A409ISOCtt = new String[] {""} ;
      BC00044_A365Contac = new String[] {""} ;
      BC000432_A366Contac = new String[] {""} ;
      BC000433_A23ISOCod = new String[] {""} ;
      BC000433_n23ISOCod = new boolean[] {false} ;
      BC000433_A406ISOCtt = new short[1] ;
      BC000433_A407ISOCtt = new String[] {""} ;
      BC000433_A408ISOCtt = new String[] {""} ;
      BC000433_A409ISOCtt = new String[] {""} ;
      BC000433_A366Contac = new String[] {""} ;
      BC000433_A365Contac = new String[] {""} ;
      GXt_int6 = (short)(0) ;
      GXv_int7 = new short [1] ;
      BC000434_A23ISOCod = new String[] {""} ;
      BC000434_n23ISOCod = new boolean[] {false} ;
      BC000434_A349CurCod = new String[] {""} ;
      BC000434_A350CurPla = new byte[1] ;
      BC000434_n350CurPla = new boolean[] {false} ;
      BC000434_A351CurDes = new String[] {""} ;
      BC000434_n351CurDes = new boolean[] {false} ;
      n350CurPla = false ;
      n351CurDes = false ;
      BC000435_A23ISOCod = new String[] {""} ;
      BC000435_n23ISOCod = new boolean[] {false} ;
      BC000435_A349CurCod = new String[] {""} ;
      BC00043_A23ISOCod = new String[] {""} ;
      BC00043_n23ISOCod = new boolean[] {false} ;
      BC00043_A349CurCod = new String[] {""} ;
      BC00043_A350CurPla = new byte[1] ;
      BC00043_n350CurPla = new boolean[] {false} ;
      BC00043_A351CurDes = new String[] {""} ;
      BC00043_n351CurDes = new boolean[] {false} ;
      sMode65 = "" ;
      BC00042_A23ISOCod = new String[] {""} ;
      BC00042_n23ISOCod = new boolean[] {false} ;
      BC00042_A349CurCod = new String[] {""} ;
      BC00042_A350CurPla = new byte[1] ;
      BC00042_n350CurPla = new boolean[] {false} ;
      BC00042_A351CurDes = new String[] {""} ;
      BC00042_n351CurDes = new boolean[] {false} ;
      BC000439_A23ISOCod = new String[] {""} ;
      BC000439_n23ISOCod = new boolean[] {false} ;
      BC000439_A349CurCod = new String[] {""} ;
      BC000439_A387Period = new String[] {""} ;
      BC000439_A263PerID = new String[] {""} ;
      BC000440_A23ISOCod = new String[] {""} ;
      BC000440_n23ISOCod = new boolean[] {false} ;
      BC000440_A349CurCod = new String[] {""} ;
      BC000440_A350CurPla = new byte[1] ;
      BC000440_n350CurPla = new boolean[] {false} ;
      BC000440_A351CurDes = new String[] {""} ;
      BC000440_n351CurDes = new boolean[] {false} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tcountry_bc__default(),
         new Object[] {
             new Object[] {
            BC00042_A23ISOCod, BC00042_A349CurCod, BC00042_A350CurPla, BC00042_n350CurPla, BC00042_A351CurDes, BC00042_n351CurDes
            }
            , new Object[] {
            BC00043_A23ISOCod, BC00043_A349CurCod, BC00043_A350CurPla, BC00043_n350CurPla, BC00043_A351CurDes, BC00043_n351CurDes
            }
            , new Object[] {
            BC00044_A23ISOCod, BC00044_A406ISOCtt, BC00044_A407ISOCtt, BC00044_A408ISOCtt, BC00044_A409ISOCtt, BC00044_A365Contac
            }
            , new Object[] {
            BC00045_A23ISOCod, BC00045_A406ISOCtt, BC00045_A407ISOCtt, BC00045_A408ISOCtt, BC00045_A409ISOCtt, BC00045_A365Contac
            }
            , new Object[] {
            BC00046_A366Contac
            }
            , new Object[] {
            BC00047_A23ISOCod, BC00047_A448ISOCon, BC00047_A846DateUp, BC00047_A449ISOCon, BC00047_A450ISOCon, BC00047_A30lgnLogi, BC00047_n30lgnLogi
            }
            , new Object[] {
            BC00048_A23ISOCod, BC00048_A448ISOCon, BC00048_A846DateUp, BC00048_A449ISOCon, BC00048_A450ISOCon, BC00048_A30lgnLogi, BC00048_n30lgnLogi
            }
            , new Object[] {
            BC00049_A30lgnLogi
            }
            , new Object[] {
            BC000410_A23ISOCod, BC000410_A20ISODes, BC000410_n20ISODes
            }
            , new Object[] {
            BC000411_A23ISOCod, BC000411_A20ISODes, BC000411_n20ISODes
            }
            , new Object[] {
            BC000412_A23ISOCod, BC000412_A20ISODes, BC000412_n20ISODes
            }
            , new Object[] {
            BC000413_A23ISOCod
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000417_A42Downloa
            }
            , new Object[] {
            BC000418_A23ISOCod, BC000418_A1136AirLi
            }
            , new Object[] {
            BC000419_A23ISOCod, BC000419_A349CurCod
            }
            , new Object[] {
            BC000420_A23ISOCod, BC000420_A20ISODes, BC000420_n20ISODes
            }
            , new Object[] {
            BC000421_A23ISOCod, BC000421_A448ISOCon, BC000421_A846DateUp, BC000421_A449ISOCon, BC000421_A450ISOCon, BC000421_A30lgnLogi, BC000421_n30lgnLogi
            }
            , new Object[] {
            BC000422_A23ISOCod, BC000422_A448ISOCon
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000426_A23ISOCod, BC000426_A448ISOCon, BC000426_A846DateUp, BC000426_A449ISOCon, BC000426_A450ISOCon, BC000426_A30lgnLogi, BC000426_n30lgnLogi
            }
            , new Object[] {
            BC000427_A23ISOCod, BC000427_A406ISOCtt, BC000427_A407ISOCtt, BC000427_A408ISOCtt, BC000427_A409ISOCtt, BC000427_A366Contac, BC000427_A365Contac
            }
            , new Object[] {
            BC000428_A23ISOCod, BC000428_A365Contac, BC000428_A406ISOCtt
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000432_A366Contac
            }
            , new Object[] {
            BC000433_A23ISOCod, BC000433_A406ISOCtt, BC000433_A407ISOCtt, BC000433_A408ISOCtt, BC000433_A409ISOCtt, BC000433_A366Contac, BC000433_A365Contac
            }
            , new Object[] {
            BC000434_A23ISOCod, BC000434_A349CurCod, BC000434_A350CurPla, BC000434_n350CurPla, BC000434_A351CurDes, BC000434_n351CurDes
            }
            , new Object[] {
            BC000435_A23ISOCod, BC000435_A349CurCod
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000439_A23ISOCod, BC000439_A349CurCod, BC000439_A387Period, BC000439_A263PerID
            }
            , new Object[] {
            BC000440_A23ISOCod, BC000440_A349CurCod, BC000440_A350CurPla, BC000440_n350CurPla, BC000440_A351CurDes, BC000440_n351CurDes
            }
         }
      );
      /* Execute Start event if defined. */
      /* Execute user event: e11042 */
      e11042 ();
   }

   private byte nKeyPressed ;
   private byte AV16AlterB ;
   private byte GXt_int1 ;
   private byte GXv_int5[] ;
   private byte gxTv_SdtCountry_Isocod_N ;
   private byte gxTv_SdtCountry_Isodes_N ;
   private byte gxTv_SdtCountry_CountryParameters_Lgnlogin_N ;
   private byte gxTv_SdtCountry_Currencies_Curplaces_Z ;
   private byte gxTv_SdtCountry_Currencies_Curplaces_N ;
   private byte gxTv_SdtCountry_Currencies_Curdescription_N ;
   private byte Z350CurPla ;
   private byte A350CurPla ;
   private byte Gxremove88 ;
   private byte Gxremove78 ;
   private byte Gxremove65 ;
   private byte Gx_BScreen ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short nGXsfl_65_idx=1 ;
   private short nIsMod_65 ;
   private short RcdFound65 ;
   private short nGXsfl_78_idx=1 ;
   private short nIsMod_78 ;
   private short RcdFound78 ;
   private short nGXsfl_88_idx=1 ;
   private short nIsMod_88 ;
   private short RcdFound88 ;
   private short gxTv_SdtCountry_ContactTypes_Isocttseq_Z ;
   private short Z406ISOCtt ;
   private short A406ISOCtt ;
   private short RcdFound4 ;
   private short nRcdExists_88 ;
   private short nRcdExists_78 ;
   private short nRcdExists_65 ;
   private short GXt_int6 ;
   private short GXv_int7[] ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode4 ;
   private String GXv_char4[] ;
   private String sMode88 ;
   private String sMode78 ;
   private String sMode65 ;
   private java.util.Date gxTv_SdtCountry_CountryParameters_Dateupd_Z ;
   private java.util.Date Z846DateUp ;
   private java.util.Date A846DateUp ;
   private boolean n23ISOCod ;
   private boolean n20ISODes ;
   private boolean n30lgnLogi ;
   private boolean n350CurPla ;
   private boolean n351CurDes ;
   private String Z23ISOCod ;
   private String A23ISOCod ;
   private String AV13LgnLog ;
   private String GXv_svchar2[] ;
   private String GXv_svchar3[] ;
   private String gxTv_SdtCountry_Isocod_Z ;
   private String gxTv_SdtCountry_Isodes_Z ;
   private String gxTv_SdtCountry_CountryParameters_Isoconfigid_Z ;
   private String gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z ;
   private String gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z ;
   private String gxTv_SdtCountry_CountryParameters_Lgnlogin_Z ;
   private String gxTv_SdtCountry_ContactTypes_Contacttypescode_Z ;
   private String gxTv_SdtCountry_ContactTypes_Isocttname_Z ;
   private String gxTv_SdtCountry_ContactTypes_Isocttphone_Z ;
   private String gxTv_SdtCountry_ContactTypes_Isocttemail_Z ;
   private String gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z ;
   private String gxTv_SdtCountry_Currencies_Curcode_Z ;
   private String gxTv_SdtCountry_Currencies_Curdescription_Z ;
   private String Z20ISODes ;
   private String A20ISODes ;
   private String Z448ISOCon ;
   private String A448ISOCon ;
   private String Z449ISOCon ;
   private String A449ISOCon ;
   private String Z450ISOCon ;
   private String A450ISOCon ;
   private String Z30lgnLogi ;
   private String A30lgnLogi ;
   private String Z365Contac ;
   private String A365Contac ;
   private String Z407ISOCtt ;
   private String A407ISOCtt ;
   private String Z408ISOCtt ;
   private String A408ISOCtt ;
   private String Z409ISOCtt ;
   private String A409ISOCtt ;
   private String Z366Contac ;
   private String A366Contac ;
   private String Z349CurCod ;
   private String A349CurCod ;
   private String Z351CurDes ;
   private String A351CurDes ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.WebSession AV11Sessio ;
   private SdtCountry bcCountry ;
   private IDataStoreProvider pr_default ;
   private String[] BC000412_A23ISOCod ;
   private boolean[] BC000412_n23ISOCod ;
   private String[] BC000412_A20ISODes ;
   private boolean[] BC000412_n20ISODes ;
   private String[] BC000413_A23ISOCod ;
   private boolean[] BC000413_n23ISOCod ;
   private String[] BC000411_A23ISOCod ;
   private boolean[] BC000411_n23ISOCod ;
   private String[] BC000411_A20ISODes ;
   private boolean[] BC000411_n20ISODes ;
   private String[] BC000410_A23ISOCod ;
   private boolean[] BC000410_n23ISOCod ;
   private String[] BC000410_A20ISODes ;
   private boolean[] BC000410_n20ISODes ;
   private long[] BC000417_A42Downloa ;
   private String[] BC000418_A23ISOCod ;
   private boolean[] BC000418_n23ISOCod ;
   private String[] BC000418_A1136AirLi ;
   private String[] BC000419_A23ISOCod ;
   private boolean[] BC000419_n23ISOCod ;
   private String[] BC000419_A349CurCod ;
   private String[] BC000420_A23ISOCod ;
   private boolean[] BC000420_n23ISOCod ;
   private String[] BC000420_A20ISODes ;
   private boolean[] BC000420_n20ISODes ;
   private String[] BC00049_A30lgnLogi ;
   private boolean[] BC00049_n30lgnLogi ;
   private String[] BC000421_A23ISOCod ;
   private boolean[] BC000421_n23ISOCod ;
   private String[] BC000421_A448ISOCon ;
   private java.util.Date[] BC000421_A846DateUp ;
   private String[] BC000421_A449ISOCon ;
   private String[] BC000421_A450ISOCon ;
   private String[] BC000421_A30lgnLogi ;
   private boolean[] BC000421_n30lgnLogi ;
   private String[] BC000422_A23ISOCod ;
   private boolean[] BC000422_n23ISOCod ;
   private String[] BC000422_A448ISOCon ;
   private String[] BC00048_A23ISOCod ;
   private boolean[] BC00048_n23ISOCod ;
   private String[] BC00048_A448ISOCon ;
   private java.util.Date[] BC00048_A846DateUp ;
   private String[] BC00048_A449ISOCon ;
   private String[] BC00048_A450ISOCon ;
   private String[] BC00048_A30lgnLogi ;
   private boolean[] BC00048_n30lgnLogi ;
   private String[] BC00047_A23ISOCod ;
   private boolean[] BC00047_n23ISOCod ;
   private String[] BC00047_A448ISOCon ;
   private java.util.Date[] BC00047_A846DateUp ;
   private String[] BC00047_A449ISOCon ;
   private String[] BC00047_A450ISOCon ;
   private String[] BC00047_A30lgnLogi ;
   private boolean[] BC00047_n30lgnLogi ;
   private String[] BC000426_A23ISOCod ;
   private boolean[] BC000426_n23ISOCod ;
   private String[] BC000426_A448ISOCon ;
   private java.util.Date[] BC000426_A846DateUp ;
   private String[] BC000426_A449ISOCon ;
   private String[] BC000426_A450ISOCon ;
   private String[] BC000426_A30lgnLogi ;
   private boolean[] BC000426_n30lgnLogi ;
   private String[] BC000427_A23ISOCod ;
   private boolean[] BC000427_n23ISOCod ;
   private short[] BC000427_A406ISOCtt ;
   private String[] BC000427_A407ISOCtt ;
   private String[] BC000427_A408ISOCtt ;
   private String[] BC000427_A409ISOCtt ;
   private String[] BC000427_A366Contac ;
   private String[] BC000427_A365Contac ;
   private String[] BC00046_A366Contac ;
   private String[] BC000428_A23ISOCod ;
   private boolean[] BC000428_n23ISOCod ;
   private String[] BC000428_A365Contac ;
   private short[] BC000428_A406ISOCtt ;
   private String[] BC00045_A23ISOCod ;
   private boolean[] BC00045_n23ISOCod ;
   private short[] BC00045_A406ISOCtt ;
   private String[] BC00045_A407ISOCtt ;
   private String[] BC00045_A408ISOCtt ;
   private String[] BC00045_A409ISOCtt ;
   private String[] BC00045_A365Contac ;
   private String[] BC00044_A23ISOCod ;
   private boolean[] BC00044_n23ISOCod ;
   private short[] BC00044_A406ISOCtt ;
   private String[] BC00044_A407ISOCtt ;
   private String[] BC00044_A408ISOCtt ;
   private String[] BC00044_A409ISOCtt ;
   private String[] BC00044_A365Contac ;
   private String[] BC000432_A366Contac ;
   private String[] BC000433_A23ISOCod ;
   private boolean[] BC000433_n23ISOCod ;
   private short[] BC000433_A406ISOCtt ;
   private String[] BC000433_A407ISOCtt ;
   private String[] BC000433_A408ISOCtt ;
   private String[] BC000433_A409ISOCtt ;
   private String[] BC000433_A366Contac ;
   private String[] BC000433_A365Contac ;
   private String[] BC000434_A23ISOCod ;
   private boolean[] BC000434_n23ISOCod ;
   private String[] BC000434_A349CurCod ;
   private byte[] BC000434_A350CurPla ;
   private boolean[] BC000434_n350CurPla ;
   private String[] BC000434_A351CurDes ;
   private boolean[] BC000434_n351CurDes ;
   private String[] BC000435_A23ISOCod ;
   private boolean[] BC000435_n23ISOCod ;
   private String[] BC000435_A349CurCod ;
   private String[] BC00043_A23ISOCod ;
   private boolean[] BC00043_n23ISOCod ;
   private String[] BC00043_A349CurCod ;
   private byte[] BC00043_A350CurPla ;
   private boolean[] BC00043_n350CurPla ;
   private String[] BC00043_A351CurDes ;
   private boolean[] BC00043_n351CurDes ;
   private String[] BC00042_A23ISOCod ;
   private boolean[] BC00042_n23ISOCod ;
   private String[] BC00042_A349CurCod ;
   private byte[] BC00042_A350CurPla ;
   private boolean[] BC00042_n350CurPla ;
   private String[] BC00042_A351CurDes ;
   private boolean[] BC00042_n351CurDes ;
   private String[] BC000439_A23ISOCod ;
   private boolean[] BC000439_n23ISOCod ;
   private String[] BC000439_A349CurCod ;
   private String[] BC000439_A387Period ;
   private String[] BC000439_A263PerID ;
   private String[] BC000440_A23ISOCod ;
   private boolean[] BC000440_n23ISOCod ;
   private String[] BC000440_A349CurCod ;
   private byte[] BC000440_A350CurPla ;
   private boolean[] BC000440_n350CurPla ;
   private String[] BC000440_A351CurDes ;
   private boolean[] BC000440_n351CurDes ;
}

final  class tcountry_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00042", "SELECT [ISOCod], [CurCode], [CurPlaces], [CurDescription] FROM [COUNTRYCURRENCIES] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00043", "SELECT [ISOCod], [CurCode], [CurPlaces], [CurDescription] FROM [COUNTRYCURRENCIES] WITH (NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00044", "SELECT [ISOCod], [ISOCttSeq], [ISOCttName], [ISOCttPhone], [ISOCttEmail], [ContactTypesCode] FROM [COUNTRYCONTACTTYPES] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [ContactTypesCode] = ? AND [ISOCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00045", "SELECT [ISOCod], [ISOCttSeq], [ISOCttName], [ISOCttPhone], [ISOCttEmail], [ContactTypesCode] FROM [COUNTRYCONTACTTYPES] WITH (NOLOCK) WHERE [ISOCod] = ? AND [ContactTypesCode] = ? AND [ISOCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00046", "SELECT [ContactTypesDescription] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00047", "SELECT [ISOCod], [ISOConfigID], [DateUpd], [ISOConfigDes], [ISOConfigValue], [lgnLogin] FROM [COUNTRYPARAMETER] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [ISOConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00048", "SELECT [ISOCod], [ISOConfigID], [DateUpd], [ISOConfigDes], [ISOConfigValue], [lgnLogin] FROM [COUNTRYPARAMETER] WITH (NOLOCK) WHERE [ISOCod] = ? AND [ISOConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00049", "SELECT [lgnLogin] FROM [LOGINS] WITH (NOLOCK) WHERE [lgnLogin] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000410", "SELECT [ISOCod], [ISODes] FROM [COUNTRY] WITH (UPDLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000411", "SELECT [ISOCod], [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000412", "SELECT TM1.[ISOCod], TM1.[ISODes] FROM [COUNTRY] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ISOCod] = ? ORDER BY TM1.[ISOCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000413", "SELECT [ISOCod] FROM [COUNTRY] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000414", "INSERT INTO [COUNTRY] ([ISOCod], [ISODes]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000415", "UPDATE [COUNTRY] SET [ISODes]=?  WHERE [ISOCod] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000416", "DELETE FROM [COUNTRY]  WHERE [ISOCod] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000417", "SELECT TOP 1 [DownloadID] FROM [DOWNLOAD] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000418", "SELECT TOP 1 [ISOCod], [AirLineCode] FROM [AIRLINES] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000419", "SELECT TOP 1 [ISOCod], [CurCode] FROM [COUNTRYCURRENCIES] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000420", "SELECT TM1.[ISOCod], TM1.[ISODes] FROM [COUNTRY] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ISOCod] = ? ORDER BY TM1.[ISOCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000421", "SELECT [ISOCod], [ISOConfigID], [DateUpd], [ISOConfigDes], [ISOConfigValue], [lgnLogin] FROM [COUNTRYPARAMETER] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? and [ISOConfigID] = ? ORDER BY [ISOCod], [ISOConfigID] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000422", "SELECT [ISOCod], [ISOConfigID] FROM [COUNTRYPARAMETER] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [ISOConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000423", "INSERT INTO [COUNTRYPARAMETER] ([ISOCod], [ISOConfigID], [DateUpd], [ISOConfigDes], [ISOConfigValue], [lgnLogin]) VALUES (?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000424", "UPDATE [COUNTRYPARAMETER] SET [DateUpd]=?, [ISOConfigDes]=?, [ISOConfigValue]=?, [lgnLogin]=?  WHERE [ISOCod] = ? AND [ISOConfigID] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000425", "DELETE FROM [COUNTRYPARAMETER]  WHERE [ISOCod] = ? AND [ISOConfigID] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000426", "SELECT [ISOCod], [ISOConfigID], [DateUpd], [ISOConfigDes], [ISOConfigValue], [lgnLogin] FROM [COUNTRYPARAMETER] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? ORDER BY [ISOCod], [ISOConfigID] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000427", "SELECT T1.[ISOCod], T1.[ISOCttSeq], T1.[ISOCttName], T1.[ISOCttPhone], T1.[ISOCttEmail], T2.[ContactTypesDescription], T1.[ContactTypesCode] FROM ([COUNTRYCONTACTTYPES] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [CONTACTTYPES] T2 WITH (NOLOCK) ON T2.[ContactTypesCode] = T1.[ContactTypesCode]) WHERE T1.[ISOCod] = ? and T1.[ContactTypesCode] = ? and T1.[ISOCttSeq] = ? ORDER BY T1.[ISOCod], T1.[ContactTypesCode], T1.[ISOCttSeq] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000428", "SELECT [ISOCod], [ContactTypesCode], [ISOCttSeq] FROM [COUNTRYCONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [ContactTypesCode] = ? AND [ISOCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000429", "INSERT INTO [COUNTRYCONTACTTYPES] ([ISOCod], [ISOCttSeq], [ISOCttName], [ISOCttPhone], [ISOCttEmail], [ContactTypesCode]) VALUES (?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000430", "UPDATE [COUNTRYCONTACTTYPES] SET [ISOCttName]=?, [ISOCttPhone]=?, [ISOCttEmail]=?  WHERE [ISOCod] = ? AND [ContactTypesCode] = ? AND [ISOCttSeq] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000431", "DELETE FROM [COUNTRYCONTACTTYPES]  WHERE [ISOCod] = ? AND [ContactTypesCode] = ? AND [ISOCttSeq] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000432", "SELECT [ContactTypesDescription] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000433", "SELECT T1.[ISOCod], T1.[ISOCttSeq], T1.[ISOCttName], T1.[ISOCttPhone], T1.[ISOCttEmail], T2.[ContactTypesDescription], T1.[ContactTypesCode] FROM ([COUNTRYCONTACTTYPES] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [CONTACTTYPES] T2 WITH (NOLOCK) ON T2.[ContactTypesCode] = T1.[ContactTypesCode]) WHERE T1.[ISOCod] = ? ORDER BY T1.[ISOCod], T1.[ContactTypesCode], T1.[ISOCttSeq] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000434", "SELECT [ISOCod], [CurCode], [CurPlaces], [CurDescription] FROM [COUNTRYCURRENCIES] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? and [CurCode] = ? ORDER BY [ISOCod], [CurCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000435", "SELECT [ISOCod], [CurCode] FROM [COUNTRYCURRENCIES] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000436", "INSERT INTO [COUNTRYCURRENCIES] ([ISOCod], [CurCode], [CurPlaces], [CurDescription]) VALUES (?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000437", "UPDATE [COUNTRYCURRENCIES] SET [CurPlaces]=?, [CurDescription]=?  WHERE [ISOCod] = ? AND [CurCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000438", "DELETE FROM [COUNTRYCURRENCIES]  WHERE [ISOCod] = ? AND [CurCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000439", "SELECT TOP 1 [ISOCod], [CurCode], [PeriodTypesCode], [PerID] FROM [PERIODS] WITH (NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000440", "SELECT [ISOCod], [CurCode], [CurPlaces], [CurDescription] FROM [COUNTRYCURRENCIES] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? ORDER BY [ISOCod], [CurCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((byte[]) buf[2])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((byte[]) buf[2])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 15 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 18 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 19 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               break;
            case 20 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 24 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               break;
            case 25 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 26 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               break;
            case 30 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 31 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 32 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((byte[]) buf[2])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 33 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 37 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               break;
            case 38 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((byte[]) buf[2])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 5, false);
               stmt.setShort(3, ((Number) parms[3]).shortValue());
               break;
            case 3 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 5, false);
               stmt.setShort(3, ((Number) parms[3]).shortValue());
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 30, false);
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 30, false);
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 8 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 10 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 11 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 12 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 30);
               }
               break;
            case 13 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 30);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 3, false);
               }
               break;
            case 14 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 15 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3);
               }
               break;
            case 16 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 17 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 18 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 19 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 30, false);
               break;
            case 20 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 30, false);
               break;
            case 21 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 30, false);
               stmt.setDateTime(3, (java.util.Date)parms[3], false);
               stmt.setVarchar(4, (String)parms[4], 100, false);
               stmt.setVarchar(5, (String)parms[5], 255, false);
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(6, (String)parms[7], 20);
               }
               break;
            case 22 :
               stmt.setDateTime(1, (java.util.Date)parms[0], false);
               stmt.setVarchar(2, (String)parms[1], 100, false);
               stmt.setVarchar(3, (String)parms[2], 255, false);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[4], 20);
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[6], 3, false);
               }
               stmt.setVarchar(6, (String)parms[7], 30, false);
               break;
            case 23 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 30, false);
               break;
            case 24 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 25 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 5, false);
               stmt.setShort(3, ((Number) parms[3]).shortValue());
               break;
            case 26 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 5, false);
               stmt.setShort(3, ((Number) parms[3]).shortValue());
               break;
            case 27 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setShort(2, ((Number) parms[2]).shortValue());
               stmt.setVarchar(3, (String)parms[3], 30, false);
               stmt.setVarchar(4, (String)parms[4], 30, false);
               stmt.setVarchar(5, (String)parms[5], 30, false);
               stmt.setVarchar(6, (String)parms[6], 5, false);
               break;
            case 28 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setVarchar(2, (String)parms[1], 30, false);
               stmt.setVarchar(3, (String)parms[2], 30, false);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[4], 3, false);
               }
               stmt.setVarchar(5, (String)parms[5], 5, false);
               stmt.setShort(6, ((Number) parms[6]).shortValue());
               break;
            case 29 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 5, false);
               stmt.setShort(3, ((Number) parms[3]).shortValue());
               break;
            case 30 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 31 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
            case 32 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               break;
            case 33 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               break;
            case 34 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(3, ((Number) parms[4]).byteValue());
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[6], 30);
               }
               break;
            case 35 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(1, ((Number) parms[1]).byteValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 30);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 3, false);
               }
               stmt.setVarchar(4, (String)parms[6], 3, false);
               break;
            case 36 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               break;
            case 37 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               stmt.setVarchar(2, (String)parms[2], 3, false);
               break;
            case 38 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 3, false);
               }
               break;
      }
   }

}


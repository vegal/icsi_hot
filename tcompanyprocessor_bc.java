/*
               File: tcompanyprocessor_bc
        Description: Processadora de Cart�o de Cr�dito
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:59.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tcompanyprocessor_bc extends GXWebPanel implements IGxSilentTrn
{
   public tcompanyprocessor_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tcompanyprocessor_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcompanyprocessor_bc.class ));
   }

   public tcompanyprocessor_bc( int remoteHandle ,
                                ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z1419Compa = A1419Compa ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_2S0( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2S260( ) ;
         }
         else
         {
            checkExtendedTable2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors2S260( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues2S0( ) ;
      }
   }

   public void e112S2( )
   {
      /* 'Back' Routine */
   }

   public void zm2S260( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         Z1420Compa = A1420Compa ;
      }
      if ( ( GX_JID == -2 ) )
      {
         Z1419Compa = A1419Compa ;
         Z1420Compa = A1420Compa ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load2S260( )
   {
      /* Using cursor BC002S4 */
      pr_default.execute(2, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound260 = (short)(1) ;
         A1420Compa = BC002S4_A1420Compa[0] ;
         zm2S260( -2) ;
      }
      pr_default.close(2);
      onLoadActions2S260( ) ;
   }

   public void onLoadActions2S260( )
   {
   }

   public void checkExtendedTable2S260( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors2S260( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey2S260( )
   {
      /* Using cursor BC002S5 */
      pr_default.execute(3, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound260 = (short)(1) ;
      }
      else
      {
         RcdFound260 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC002S3 */
      pr_default.execute(1, new Object[] {new Boolean(n1419Compa), A1419Compa});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm2S260( 2) ;
         RcdFound260 = (short)(1) ;
         A1419Compa = BC002S3_A1419Compa[0] ;
         n1419Compa = BC002S3_n1419Compa[0] ;
         A1420Compa = BC002S3_A1420Compa[0] ;
         Z1419Compa = A1419Compa ;
         sMode260 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load2S260( ) ;
         Gx_mode = sMode260 ;
      }
      else
      {
         RcdFound260 = (short)(0) ;
         initializeNonKey2S260( ) ;
         sMode260 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode260 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey2S260( ) ;
      if ( ( RcdFound260 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_2S0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency2S260( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002S2 */
         pr_default.execute(0, new Object[] {new Boolean(n1419Compa), A1419Compa});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"COMPANYPROCESSOR"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1420Compa, BC002S2_A1420Compa[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"COMPANYPROCESSOR"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2S260( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2S260( 0) ;
         checkOptimisticConcurrency2S260( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2S260( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002S6 */
                  pr_default.execute(4, new Object[] {new Boolean(n1419Compa), A1419Compa, A1420Compa});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2S260( ) ;
         }
         endLevel2S260( ) ;
      }
      closeExtendedTableCursors2S260( ) ;
   }

   public void update2S260( )
   {
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2S260( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2S260( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002S7 */
                  pr_default.execute(5, new Object[] {A1420Compa, new Boolean(n1419Compa), A1419Compa});
                  deferredUpdate2S260( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2S260( ) ;
      }
      closeExtendedTableCursors2S260( ) ;
   }

   public void deferredUpdate2S260( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2S260( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2S260( ) ;
         /* No cascading delete specified. */
         afterConfirm2S260( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2S260( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC002S8 */
               pr_default.execute(6, new Object[] {new Boolean(n1419Compa), A1419Compa});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode260 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2S260( ) ;
      Gx_mode = sMode260 ;
   }

   public void onDeleteControls2S260( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC002S9 */
         pr_default.execute(7, new Object[] {new Boolean(n1419Compa), A1419Compa});
         if ( (pr_default.getStatus(7) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Cadastro de maquinetas"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(7);
      }
   }

   public void endLevel2S260( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2S260( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues2S0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2S260( )
   {
      /* Using cursor BC002S10 */
      pr_default.execute(8, new Object[] {new Boolean(n1419Compa), A1419Compa});
      RcdFound260 = (short)(0) ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound260 = (short)(1) ;
         A1419Compa = BC002S10_A1419Compa[0] ;
         n1419Compa = BC002S10_n1419Compa[0] ;
         A1420Compa = BC002S10_A1420Compa[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2S260( )
   {
      pr_default.readNext(8);
      RcdFound260 = (short)(0) ;
      scanLoad2S260( ) ;
   }

   public void scanLoad2S260( )
   {
      sMode260 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound260 = (short)(1) ;
         A1419Compa = BC002S10_A1419Compa[0] ;
         n1419Compa = BC002S10_n1419Compa[0] ;
         A1420Compa = BC002S10_A1420Compa[0] ;
      }
      Gx_mode = sMode260 ;
   }

   public void scanEnd2S260( )
   {
      pr_default.close(8);
   }

   public void afterConfirm2S260( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2S260( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2S260( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2S260( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2S260( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2S260( )
   {
      /* Before Validate Rules */
   }

   public void addRow2S260( )
   {
      VarsToRow260( bcCompanyProcessor) ;
   }

   public void sendRow2S260( )
   {
   }

   public void readRow2S260( )
   {
      RowToVars260( bcCompanyProcessor, 0) ;
   }

   public void confirmValues2S0( )
   {
   }

   public void initializeNonKey2S260( )
   {
      A1420Compa = "" ;
   }

   public void initAll2S260( )
   {
      A1419Compa = "" ;
      n1419Compa = false ;
      initializeNonKey2S260( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow260( SdtCompanyProcessor obj260 )
   {
      obj260.setgxTv_SdtCompanyProcessor_Mode( Gx_mode );
      obj260.setgxTv_SdtCompanyProcessor_Companyprocessorlayout( A1420Compa );
      obj260.setgxTv_SdtCompanyProcessor_Companyprocessorcode( A1419Compa );
      obj260.setgxTv_SdtCompanyProcessor_Companyprocessorcode_Z( Z1419Compa );
      obj260.setgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z( Z1420Compa );
      obj260.setgxTv_SdtCompanyProcessor_Companyprocessorcode_N( (byte)((byte)((n1419Compa)?1:0)) );
      obj260.setgxTv_SdtCompanyProcessor_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars260( SdtCompanyProcessor obj260 ,
                             int forceLoad )
   {
      Gx_mode = obj260.getgxTv_SdtCompanyProcessor_Mode() ;
      A1420Compa = obj260.getgxTv_SdtCompanyProcessor_Companyprocessorlayout() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A1419Compa = obj260.getgxTv_SdtCompanyProcessor_Companyprocessorcode() ;
      }
      Z1419Compa = obj260.getgxTv_SdtCompanyProcessor_Companyprocessorcode_Z() ;
      Z1420Compa = obj260.getgxTv_SdtCompanyProcessor_Companyprocessorlayout_Z() ;
      n1419Compa = (boolean)((obj260.getgxTv_SdtCompanyProcessor_Companyprocessorcode_N()==0)?false:true) ;
      Gx_mode = obj260.getgxTv_SdtCompanyProcessor_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A1419Compa = (String)obj[0] ;
      n1419Compa = false ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey2S260( ) ;
      scanStart2S260( ) ;
      if ( ( RcdFound260 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z1419Compa = A1419Compa ;
      }
      onLoadActions2S260( ) ;
      zm2S260( 0) ;
      addRow2S260( ) ;
      scanEnd2S260( ) ;
      if ( ( RcdFound260 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars260( bcCompanyProcessor, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey2S260( ) ;
      if ( ( RcdFound260 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
         {
            A1419Compa = Z1419Compa ;
            n1419Compa = false ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update2S260( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2S260( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2S260( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow260( bcCompanyProcessor) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars260( bcCompanyProcessor, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey2S260( ) ;
      if ( ( RcdFound260 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
         {
            A1419Compa = Z1419Compa ;
            n1419Compa = false ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A1419Compa, Z1419Compa) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tcompanyprocessor_bc");
      VarsToRow260( bcCompanyProcessor) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcCompanyProcessor.getgxTv_SdtCompanyProcessor_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcCompanyProcessor.setgxTv_SdtCompanyProcessor_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtCompanyProcessor sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcCompanyProcessor ) )
      {
         bcCompanyProcessor = sdt ;
         if ( ( GXutil.strcmp(bcCompanyProcessor.getgxTv_SdtCompanyProcessor_Mode(), "") == 0 ) )
         {
            bcCompanyProcessor.setgxTv_SdtCompanyProcessor_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow260( bcCompanyProcessor) ;
         }
         else
         {
            RowToVars260( bcCompanyProcessor, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcCompanyProcessor.getgxTv_SdtCompanyProcessor_Mode(), "") == 0 ) )
         {
            bcCompanyProcessor.setgxTv_SdtCompanyProcessor_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars260( bcCompanyProcessor, 1) ;
      return  ;
   }

   public SdtCompanyProcessor getCompanyProcessor_BC( )
   {
      return bcCompanyProcessor ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z1419Compa = "" ;
      A1419Compa = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode_Z = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z = "" ;
      gxTv_SdtCompanyProcessor_Companyprocessorcode_N = (byte)(0) ;
      GX_JID = 0 ;
      Z1420Compa = "" ;
      A1420Compa = "" ;
      n1419Compa = false ;
      BC002S4_A1419Compa = new String[] {""} ;
      BC002S4_n1419Compa = new boolean[] {false} ;
      BC002S4_A1420Compa = new String[] {""} ;
      RcdFound260 = (short)(0) ;
      BC002S5_A1419Compa = new String[] {""} ;
      BC002S5_n1419Compa = new boolean[] {false} ;
      BC002S3_A1419Compa = new String[] {""} ;
      BC002S3_n1419Compa = new boolean[] {false} ;
      BC002S3_A1420Compa = new String[] {""} ;
      sMode260 = "" ;
      BC002S2_A1419Compa = new String[] {""} ;
      BC002S2_n1419Compa = new boolean[] {false} ;
      BC002S2_A1420Compa = new String[] {""} ;
      BC002S9_A1313CadIA = new String[] {""} ;
      BC002S9_A1423POSDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002S9_A1237ICSI_ = new String[] {""} ;
      BC002S10_A1419Compa = new String[] {""} ;
      BC002S10_n1419Compa = new boolean[] {false} ;
      BC002S10_A1420Compa = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tcompanyprocessor_bc__default(),
         new Object[] {
             new Object[] {
            BC002S2_A1419Compa, BC002S2_A1420Compa
            }
            , new Object[] {
            BC002S3_A1419Compa, BC002S3_A1420Compa
            }
            , new Object[] {
            BC002S4_A1419Compa, BC002S4_A1420Compa
            }
            , new Object[] {
            BC002S5_A1419Compa
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002S9_A1313CadIA, BC002S9_A1423POSDa, BC002S9_A1237ICSI_
            }
            , new Object[] {
            BC002S10_A1419Compa, BC002S10_A1420Compa
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private byte gxTv_SdtCompanyProcessor_Companyprocessorcode_N ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound260 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode260 ;
   private boolean n1419Compa ;
   private String Z1419Compa ;
   private String A1419Compa ;
   private String gxTv_SdtCompanyProcessor_Companyprocessorcode_Z ;
   private String gxTv_SdtCompanyProcessor_Companyprocessorlayout_Z ;
   private String Z1420Compa ;
   private String A1420Compa ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtCompanyProcessor bcCompanyProcessor ;
   private IDataStoreProvider pr_default ;
   private String[] BC002S4_A1419Compa ;
   private boolean[] BC002S4_n1419Compa ;
   private String[] BC002S4_A1420Compa ;
   private String[] BC002S5_A1419Compa ;
   private boolean[] BC002S5_n1419Compa ;
   private String[] BC002S3_A1419Compa ;
   private boolean[] BC002S3_n1419Compa ;
   private String[] BC002S3_A1420Compa ;
   private String[] BC002S2_A1419Compa ;
   private boolean[] BC002S2_n1419Compa ;
   private String[] BC002S2_A1420Compa ;
   private String[] BC002S9_A1313CadIA ;
   private java.util.Date[] BC002S9_A1423POSDa ;
   private String[] BC002S9_A1237ICSI_ ;
   private String[] BC002S10_A1419Compa ;
   private boolean[] BC002S10_n1419Compa ;
   private String[] BC002S10_A1420Compa ;
}

final  class tcompanyprocessor_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC002S2", "SELECT [CompanyProcessorCode], [CompanyProcessorLayout] FROM [COMPANYPROCESSOR] WITH (UPDLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002S3", "SELECT [CompanyProcessorCode], [CompanyProcessorLayout] FROM [COMPANYPROCESSOR] WITH (NOLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002S4", "SELECT TM1.[CompanyProcessorCode], TM1.[CompanyProcessorLayout] FROM [COMPANYPROCESSOR] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[CompanyProcessorCode] = ? ORDER BY TM1.[CompanyProcessorCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002S5", "SELECT [CompanyProcessorCode] FROM [COMPANYPROCESSOR] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002S6", "INSERT INTO [COMPANYPROCESSOR] ([CompanyProcessorCode], [CompanyProcessorLayout]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002S7", "UPDATE [COMPANYPROCESSOR] SET [CompanyProcessorLayout]=?  WHERE [CompanyProcessorCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002S8", "DELETE FROM [COMPANYPROCESSOR]  WHERE [CompanyProcessorCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002S9", "SELECT TOP 1 [CadIATA], [POSData], [ICSI_CCConfCod] FROM [CADASTROSPOS] WITH (NOLOCK) WHERE [CompanyProcessorCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC002S10", "SELECT TM1.[CompanyProcessorCode], TM1.[CompanyProcessorLayout] FROM [COMPANYPROCESSOR] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[CompanyProcessorCode] = ? ORDER BY TM1.[CompanyProcessorCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 3 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 255, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 25, false);
               }
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25);
               }
               break;
            case 8 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 25, false);
               }
               break;
      }
   }

}


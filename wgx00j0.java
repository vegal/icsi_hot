/*
               File: Gx00J0
        Description: Selection List Message Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:15.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx00j0 extends GXWorkpanel
{
   public wgx00j0( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx00j0.class ));
   }

   public wgx00j0( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx00J0" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List Message Types" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 654 ;
   }

   protected int getFrmHeight( )
   {
      return 306 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx00J0.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      wgx00j0.this.aP0 = aP0;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load12( ) throws GXLoadInterruptException
   {
      subwgx00j012 = new subwgx00j012 ();
      lV5cMsgCod = GXutil.padr( GXutil.rtrim( AV5cMsgCod), (short)(20), "%") ;
      lV7cMSGSub = GXutil.padr( GXutil.rtrim( AV7cMSGSub), (short)(50), "%") ;
      /* Using cursor W000Z2 */
      pr_default.execute(0, new Object[] {lV5cMsgCod, new Byte(AV6cMsgSta), lV7cMSGSub});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A868MSGSub = W000Z2_A868MSGSub[0] ;
         n868MSGSub = W000Z2_n868MSGSub[0] ;
         A98MsgStat = W000Z2_A98MsgStat[0] ;
         n98MsgStat = W000Z2_n98MsgStat[0] ;
         A100MsgCod = W000Z2_A100MsgCod[0] ;
         /* Execute user event: e11V0Z2 */
         e11V0Z2 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx00J0_load12 extends GXLoadProducer
   {
      wgx00j0 _sf ;

      public Gx00J0_load12( wgx00j0 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer12();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load12();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors12();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow12( )
   {
      return true;
   }

   public void autoRefresh_flow12( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cMsgCod, cV5cMsgCod)!=0)||(AV6cMsgSta!=cV6cMsgSta)||(GXutil.strcmp(AV7cMSGSub, cV7cMSGSub)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow12() )) {
         subfile.refresh();
         resetSubfileConditions_flow12() ;
      }
   }

   public boolean getSearch_flow12( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow12( )
   {
      cV5cMsgCod = AV5cMsgCod ;
      cV6cMsgSta = AV6cMsgSta ;
      cV7cMSGSub = AV7cMSGSub ;
   }

   public void resetSearchConditions_flow12( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow12( )
   {
      return new subwgx00j012 ();
   }

   public boolean getSearch_flow12( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow12( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow12( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow12( )
   {
      GXRefreshCommand12 ();
   }

   public final  class Gx00J0_flow12 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx00j0 _sf ;

      public Gx00J0_flow12( wgx00j0 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow12();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow12(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow12();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow12();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow12(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow12();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow12(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow12(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow12(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow12();
      }

   }

   protected void GXRefreshCommand12( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V0Z2 */
      e12V0Z2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V0Z2( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV8pMsgCod = A100MsgCod ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer12( )
   {
      subwgx00j012 oAux = subwgx00j012 ;
      subwgx00j012 = new subwgx00j012 ();
      variablesToSubfile12 ();
      subGrd_1.addElement(subwgx00j012);
      subwgx00j012 = oAux;
   }

   private void e11V0Z2( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors12( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 654 , 306 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtavCmsgcode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),168, 24, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 24 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cMsgCod" );
      ((GXEdit) edtavCmsgcode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCmsgcode.addFocusListener(this);
      edtavCmsgcode.getGXComponent().setHelpId("HLP_WGx00J0.htm");
      edtavCmsgstatus = new GUIObjectByte ( new GXEdit(1, "9", UIFactory.getFont( "Courier New", 0, 9),168, 48, 17, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 48 , 17 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6cMsgSta" );
      ((GXEdit) edtavCmsgstatus.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtavCmsgstatus.addFocusListener(this);
      edtavCmsgstatus.getGXComponent().setHelpId("HLP_WGx00J0.htm");
      edtavCmsgsubject = new GUIObjectString ( new GXEdit(50, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),168, 72, 360, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 72 , 360 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV7cMSGSub" );
      ((GXEdit) edtavCmsgsubject.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCmsgsubject.addFocusListener(this);
      edtavCmsgsubject.getGXComponent().setHelpId("HLP_WGx00J0.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx00J0_load12(this), new Gx00J0_flow12(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 148, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 147 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A100MsgCod" ), "C�digo da Mensagem"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 147 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectByte ( new GXEdit(1, "9", UIFactory.getFont( "Courier New", 0, 9),0, 0, 118, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.NUMERIC, false, false, 0, false) , null ,  0 , 0 , 117 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A98MsgStat" ), "Status (1=Enabled)"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 117 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 96 , 321 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  546 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  546 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  546 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  546 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "C�digo da Mensagem", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 122 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "Status (1=Enabled)", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 109 , 13 );
      lbllbl11 = UIFactory.getLabel(GXPanel1, "MSGSubject", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 72 , 71 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                edtavCmsgcode ,
                edtavCmsgstatus ,
                edtavCmsgsubject ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void variablesToSubfile12( )
   {
      subwgx00j012.setMsgCode(A100MsgCod);
      subwgx00j012.setMsgStatus(A98MsgStat);
   }

   protected void subfileToVariables12( )
   {
      A100MsgCod = subwgx00j012.getMsgCode();
      A98MsgStat = subwgx00j012.getMsgStatus();
      if ( ( A98MsgStat != 0 ) )
      {
         n98MsgStat = false ;
      }
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      edtavCmsgcode.setValue( AV5cMsgCod );
      edtavCmsgstatus.setValue( AV6cMsgSta );
      edtavCmsgsubject.setValue( AV7cMSGSub );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      AV5cMsgCod = edtavCmsgcode.getValue() ;
      AV6cMsgSta = edtavCmsgstatus.getValue() ;
      AV7cMSGSub = edtavCmsgsubject.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx00j012 = ( subwgx00j012 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx00j012 = new subwgx00j012 ();
      }
      subfileToVariables12 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile12 ();
      subGrd_1.refreshLineValue(subwgx00j012);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx00j012 = ( subwgx00j012 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx00j012 = new subwgx00j012 ();
      }
      subfileToVariables12 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V0Z2 */
         e12V0Z2 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V0Z2 */
         e12V0Z2 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavCmsgcode.isEventSource(eventSource) ) {
         setGXCursor( edtavCmsgcode.getGXCursor() );
         return;
      }
      if ( edtavCmsgstatus.isEventSource(eventSource) ) {
         setGXCursor( edtavCmsgstatus.getGXCursor() );
         return;
      }
      if ( edtavCmsgsubject.isEventSource(eventSource) ) {
         setGXCursor( edtavCmsgsubject.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavCmsgcode.isEventSource(eventSource) ) {
         AV5cMsgCod = edtavCmsgcode.getValue() ;
         return;
      }
      if ( edtavCmsgstatus.isEventSource(eventSource) ) {
         AV6cMsgSta = edtavCmsgstatus.getValue() ;
         return;
      }
      if ( edtavCmsgsubject.isEventSource(eventSource) ) {
         AV7cMSGSub = edtavCmsgsubject.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V0Z2 */
         e12V0Z2 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = wgx00j0.this.AV8pMsgCod;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV8pMsgCod = "" ;
      subwgx00j012 = new subwgx00j012();
      scmdbuf = "" ;
      lV5cMsgCod = "" ;
      AV5cMsgCod = "" ;
      lV7cMSGSub = "" ;
      AV7cMSGSub = "" ;
      AV6cMsgSta = (byte)(0) ;
      W000Z2_A868MSGSub = new String[] {""} ;
      W000Z2_n868MSGSub = new boolean[] {false} ;
      W000Z2_A98MsgStat = new byte[1] ;
      W000Z2_n98MsgStat = new boolean[] {false} ;
      W000Z2_A100MsgCod = new String[] {""} ;
      A868MSGSub = "" ;
      n868MSGSub = false ;
      A98MsgStat = (byte)(0) ;
      n98MsgStat = false ;
      A100MsgCod = "" ;
      gxIsRefreshing = false ;
      cV5cMsgCod = "" ;
      cV6cMsgSta = (byte)(0) ;
      cV7cMSGSub = "" ;
      returnInSub = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx00j0__default(),
         new Object[] {
             new Object[] {
            W000Z2_A868MSGSub, W000Z2_n868MSGSub, W000Z2_A98MsgStat, W000Z2_n98MsgStat, W000Z2_A100MsgCod
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected byte AV6cMsgSta ;
   protected byte A98MsgStat ;
   protected byte cV6cMsgSta ;
   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean n868MSGSub ;
   protected boolean n98MsgStat ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String AV8pMsgCod ;
   protected String lV5cMsgCod ;
   protected String AV5cMsgCod ;
   protected String lV7cMSGSub ;
   protected String AV7cMSGSub ;
   protected String A868MSGSub ;
   protected String A100MsgCod ;
   protected String cV5cMsgCod ;
   protected String cV7cMSGSub ;
   protected String[] aP0 ;
   protected subwgx00j012 subwgx00j012 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W000Z2_A868MSGSub ;
   protected boolean[] W000Z2_n868MSGSub ;
   protected byte[] W000Z2_A98MsgStat ;
   protected boolean[] W000Z2_n98MsgStat ;
   protected String[] W000Z2_A100MsgCod ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtavCmsgcode ;
   protected GUIObjectByte edtavCmsgstatus ;
   protected GUIObjectString edtavCmsgsubject ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
   protected ILabel lbllbl11 ;
}

final  class wgx00j0__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W000Z2", "SELECT [MSGSubject], [MsgStatus], [MsgCode] FROM [MESSAGETYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ([MsgCode] like ?) AND ([MsgStatus] >= ?) AND ([MSGSubject] like ?) ORDER BY [MsgCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((byte[]) buf[2])[0] = rslt.getByte(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20);
               stmt.setByte(2, ((Number) parms[1]).byteValue());
               stmt.setVarchar(3, (String)parms[2], 50);
               break;
      }
   }

}


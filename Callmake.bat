@echo off
set MAKE_PROGRAM=%1
set JAVAC=%2
set CFLAGS=
set FILE=%3
set PATHPACK=%4

if "%4" == "/nologo" set CFLAGS=%4
if "%4" == "/nologo" set PATHPACK=
if "%4" == "/O" set CFLAGS=%4
if "%4" == "/O" set PATHPACK=
if "%4" == "-O" set CFLAGS=%4
if "%4" == "-O" set PATHPACK=

:loop
if .%5. == .. goto compile
   set CFLAGS=%5 %CFLAGS%
   shift
goto loop

:compile
%MAKE_PROGRAM% -f %FILE%.mak
:end
if ERRORLEVEL 1 goto dele

goto end2
:dele
if "%PATHPACK%" == "com\genexus\" goto dm
if exist %PATHPACK%%FILE%.class del %PATHPACK%%FILE%.class > nul
goto end2

:dm
del developermenu.class > nul

:end2

/*
               File: AlteraDadosEncriptacao
        Description: Altera Dados Encriptacao
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:5.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class palteradadosencriptacao extends GXProcedure
{
   public palteradadosencriptacao( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( palteradadosencriptacao.class ), "" );
   }

   public palteradadosencriptacao( int remoteHandle ,
                                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        java.util.Date[] aP1 ,
                        java.util.Date aP2 ,
                        int[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String aP0 ,
                             java.util.Date[] aP1 ,
                             java.util.Date aP2 ,
                             int[] aP3 )
   {
      palteradadosencriptacao.this.AV10LccbTa = aP0;
      palteradadosencriptacao.this.AV8DataIni = aP1[0];
      this.aP1 = aP1;
      palteradadosencriptacao.this.AV9DataFin = aP2;
      palteradadosencriptacao.this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV15Count = 0 ;
      while ( (( AV8DataIni.before( AV9DataFin ) ) || ( AV8DataIni.equals( AV9DataFin ) )) )
      {
         if ( ( GXutil.strcmp(AV10LccbTa, "L") == 0 ) )
         {
            /* Execute user subroutine: S1121 */
            S1121 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
         }
         if ( ( GXutil.strcmp(AV10LccbTa, "L1") == 0 ) )
         {
            /* Execute user subroutine: S1238 */
            S1238 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
         }
         if ( ( GXutil.strcmp(AV10LccbTa, "L2") == 0 ) )
         {
            /* Execute user subroutine: S1355 */
            S1355 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
         }
         AV8DataIni = GXutil.dadd(AV8DataIni,+(1)) ;
      }
      cleanup();
   }

   public void S1121( )
   {
      /* 'ATUALIZA_LCCBPLP' Routine */
      /* Using cursor P007C2 */
      pr_default.execute(0, new Object[] {AV8DataIni});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1499lccbC = P007C2_A1499lccbC[0] ;
         n1499lccbC = P007C2_n1499lccbC[0] ;
         A1223lccbD = P007C2_A1223lccbD[0] ;
         A1225lccbC = P007C2_A1225lccbC[0] ;
         A1150lccbE = P007C2_A1150lccbE[0] ;
         A1222lccbI = P007C2_A1222lccbI[0] ;
         A1224lccbC = P007C2_A1224lccbC[0] ;
         A1226lccbA = P007C2_A1226lccbA[0] ;
         A1227lccbO = P007C2_A1227lccbO[0] ;
         A1228lccbF = P007C2_A1228lccbF[0] ;
         if ( ( GXutil.strcmp(A1499lccbC, "") == 0 ) )
         {
            if ( A1223lccbD.equals( AV8DataIni ) )
            {
               AV14Atribu = GXutil.trim( A1225lccbC) ;
               GXt_char1 = AV13Result ;
               GXv_char2[0] = GXt_char1 ;
               new pcrypto(remoteHandle, context).execute( AV14Atribu, "E", GXv_char2) ;
               palteradadosencriptacao.this.GXt_char1 = GXv_char2[0] ;
               AV13Result = GXt_char1 ;
               A1499lccbC = AV13Result ;
               n1499lccbC = false ;
               AV15Count = (int)(AV15Count+1) ;
               /* Using cursor P007C3 */
               pr_default.execute(1, new Object[] {new Boolean(n1499lccbC), A1499lccbC, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            }
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
   }

   public void S1238( )
   {
      /* 'ATUALIZA_LCCBPLP1' Routine */
      /* Using cursor P007C4 */
      pr_default.execute(2, new Object[] {AV8DataIni});
      while ( (pr_default.getStatus(2) != 101) )
      {
         A1500lccbC = P007C4_A1500lccbC[0] ;
         n1500lccbC = P007C4_n1500lccbC[0] ;
         A1223lccbD = P007C4_A1223lccbD[0] ;
         A1225lccbC = P007C4_A1225lccbC[0] ;
         A1150lccbE = P007C4_A1150lccbE[0] ;
         A1222lccbI = P007C4_A1222lccbI[0] ;
         A1224lccbC = P007C4_A1224lccbC[0] ;
         A1226lccbA = P007C4_A1226lccbA[0] ;
         A1227lccbO = P007C4_A1227lccbO[0] ;
         A1228lccbF = P007C4_A1228lccbF[0] ;
         A1229lccbS = P007C4_A1229lccbS[0] ;
         A1230lccbS = P007C4_A1230lccbS[0] ;
         if ( ( GXutil.strcmp(A1500lccbC, "") == 0 ) )
         {
            if ( A1223lccbD.equals( AV8DataIni ) )
            {
               AV14Atribu = GXutil.trim( A1225lccbC) ;
               GXt_char1 = AV13Result ;
               GXv_char2[0] = GXt_char1 ;
               new pcrypto(remoteHandle, context).execute( AV14Atribu, "E", GXv_char2) ;
               palteradadosencriptacao.this.GXt_char1 = GXv_char2[0] ;
               AV13Result = GXt_char1 ;
               A1500lccbC = AV13Result ;
               n1500lccbC = false ;
               AV15Count = (int)(AV15Count+1) ;
               /* Using cursor P007C5 */
               pr_default.execute(3, new Object[] {new Boolean(n1500lccbC), A1500lccbC, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS)});
            }
         }
         pr_default.readNext(2);
      }
      pr_default.close(2);
   }

   public void S1355( )
   {
      /* 'ATUALIZA_LCCBPLP2' Routine */
      /* Using cursor P007C6 */
      pr_default.execute(4, new Object[] {AV8DataIni});
      while ( (pr_default.getStatus(4) != 101) )
      {
         A1501lccbC = P007C6_A1501lccbC[0] ;
         n1501lccbC = P007C6_n1501lccbC[0] ;
         A1223lccbD = P007C6_A1223lccbD[0] ;
         A1225lccbC = P007C6_A1225lccbC[0] ;
         A1150lccbE = P007C6_A1150lccbE[0] ;
         A1222lccbI = P007C6_A1222lccbI[0] ;
         A1224lccbC = P007C6_A1224lccbC[0] ;
         A1226lccbA = P007C6_A1226lccbA[0] ;
         A1227lccbO = P007C6_A1227lccbO[0] ;
         A1228lccbF = P007C6_A1228lccbF[0] ;
         A1231lccbT = P007C6_A1231lccbT[0] ;
         A1232lccbT = P007C6_A1232lccbT[0] ;
         if ( ( GXutil.strcmp(A1501lccbC, "") == 0 ) )
         {
            if ( A1223lccbD.equals( AV8DataIni ) )
            {
               AV14Atribu = GXutil.trim( A1225lccbC) ;
               GXt_char1 = AV13Result ;
               GXv_char2[0] = GXt_char1 ;
               new pcrypto(remoteHandle, context).execute( AV14Atribu, "E", GXv_char2) ;
               palteradadosencriptacao.this.GXt_char1 = GXv_char2[0] ;
               AV13Result = GXt_char1 ;
               A1501lccbC = AV13Result ;
               n1501lccbC = false ;
               AV15Count = (int)(AV15Count+1) ;
               /* Using cursor P007C7 */
               pr_default.execute(5, new Object[] {new Boolean(n1501lccbC), A1501lccbC, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
            }
         }
         pr_default.readNext(4);
      }
      pr_default.close(4);
   }

   protected void cleanup( )
   {
      this.aP1[0] = palteradadosencriptacao.this.AV8DataIni;
      this.aP3[0] = palteradadosencriptacao.this.AV15Count;
      Application.commit(context, remoteHandle, "DEFAULT", "palteradadosencriptacao");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV15Count = 0 ;
      returnInSub = false ;
      scmdbuf = "" ;
      P007C2_A1499lccbC = new String[] {""} ;
      P007C2_n1499lccbC = new boolean[] {false} ;
      P007C2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007C2_A1225lccbC = new String[] {""} ;
      P007C2_A1150lccbE = new String[] {""} ;
      P007C2_A1222lccbI = new String[] {""} ;
      P007C2_A1224lccbC = new String[] {""} ;
      P007C2_A1226lccbA = new String[] {""} ;
      P007C2_A1227lccbO = new String[] {""} ;
      P007C2_A1228lccbF = new String[] {""} ;
      A1499lccbC = "" ;
      n1499lccbC = false ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1150lccbE = "" ;
      A1222lccbI = "" ;
      A1224lccbC = "" ;
      A1226lccbA = "" ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      AV14Atribu = "" ;
      AV13Result = "" ;
      P007C4_A1500lccbC = new String[] {""} ;
      P007C4_n1500lccbC = new boolean[] {false} ;
      P007C4_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007C4_A1225lccbC = new String[] {""} ;
      P007C4_A1150lccbE = new String[] {""} ;
      P007C4_A1222lccbI = new String[] {""} ;
      P007C4_A1224lccbC = new String[] {""} ;
      P007C4_A1226lccbA = new String[] {""} ;
      P007C4_A1227lccbO = new String[] {""} ;
      P007C4_A1228lccbF = new String[] {""} ;
      P007C4_A1229lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P007C4_A1230lccbS = new short[1] ;
      A1500lccbC = "" ;
      n1500lccbC = false ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1230lccbS = (short)(0) ;
      P007C6_A1501lccbC = new String[] {""} ;
      P007C6_n1501lccbC = new boolean[] {false} ;
      P007C6_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007C6_A1225lccbC = new String[] {""} ;
      P007C6_A1150lccbE = new String[] {""} ;
      P007C6_A1222lccbI = new String[] {""} ;
      P007C6_A1224lccbC = new String[] {""} ;
      P007C6_A1226lccbA = new String[] {""} ;
      P007C6_A1227lccbO = new String[] {""} ;
      P007C6_A1228lccbF = new String[] {""} ;
      P007C6_A1231lccbT = new String[] {""} ;
      P007C6_A1232lccbT = new String[] {""} ;
      A1501lccbC = "" ;
      n1501lccbC = false ;
      A1231lccbT = "" ;
      A1232lccbT = "" ;
      GXt_char1 = "" ;
      GXv_char2 = new String [1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new palteradadosencriptacao__default(),
         new Object[] {
             new Object[] {
            P007C2_A1499lccbC, P007C2_n1499lccbC, P007C2_A1223lccbD, P007C2_A1225lccbC, P007C2_A1150lccbE, P007C2_A1222lccbI, P007C2_A1224lccbC, P007C2_A1226lccbA, P007C2_A1227lccbO, P007C2_A1228lccbF
            }
            , new Object[] {
            }
            , new Object[] {
            P007C4_A1500lccbC, P007C4_n1500lccbC, P007C4_A1223lccbD, P007C4_A1225lccbC, P007C4_A1150lccbE, P007C4_A1222lccbI, P007C4_A1224lccbC, P007C4_A1226lccbA, P007C4_A1227lccbO, P007C4_A1228lccbF,
            P007C4_A1229lccbS, P007C4_A1230lccbS
            }
            , new Object[] {
            }
            , new Object[] {
            P007C6_A1501lccbC, P007C6_n1501lccbC, P007C6_A1223lccbD, P007C6_A1225lccbC, P007C6_A1150lccbE, P007C6_A1222lccbI, P007C6_A1224lccbC, P007C6_A1226lccbA, P007C6_A1227lccbO, P007C6_A1228lccbF,
            P007C6_A1231lccbT, P007C6_A1232lccbT
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short A1230lccbS ;
   private short Gx_err ;
   private int AV15Count ;
   private String AV10LccbTa ;
   private String scmdbuf ;
   private String A1499lccbC ;
   private String A1225lccbC ;
   private String A1150lccbE ;
   private String A1222lccbI ;
   private String A1224lccbC ;
   private String A1226lccbA ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private String AV14Atribu ;
   private String AV13Result ;
   private String A1500lccbC ;
   private String A1501lccbC ;
   private String A1231lccbT ;
   private String A1232lccbT ;
   private String GXt_char1 ;
   private String GXv_char2[] ;
   private java.util.Date A1229lccbS ;
   private java.util.Date AV8DataIni ;
   private java.util.Date AV9DataFin ;
   private java.util.Date A1223lccbD ;
   private boolean returnInSub ;
   private boolean n1499lccbC ;
   private boolean n1500lccbC ;
   private boolean n1501lccbC ;
   private java.util.Date[] aP1 ;
   private int[] aP3 ;
   private IDataStoreProvider pr_default ;
   private String[] P007C2_A1499lccbC ;
   private boolean[] P007C2_n1499lccbC ;
   private java.util.Date[] P007C2_A1223lccbD ;
   private String[] P007C2_A1225lccbC ;
   private String[] P007C2_A1150lccbE ;
   private String[] P007C2_A1222lccbI ;
   private String[] P007C2_A1224lccbC ;
   private String[] P007C2_A1226lccbA ;
   private String[] P007C2_A1227lccbO ;
   private String[] P007C2_A1228lccbF ;
   private String[] P007C4_A1500lccbC ;
   private boolean[] P007C4_n1500lccbC ;
   private java.util.Date[] P007C4_A1223lccbD ;
   private String[] P007C4_A1225lccbC ;
   private String[] P007C4_A1150lccbE ;
   private String[] P007C4_A1222lccbI ;
   private String[] P007C4_A1224lccbC ;
   private String[] P007C4_A1226lccbA ;
   private String[] P007C4_A1227lccbO ;
   private String[] P007C4_A1228lccbF ;
   private java.util.Date[] P007C4_A1229lccbS ;
   private short[] P007C4_A1230lccbS ;
   private String[] P007C6_A1501lccbC ;
   private boolean[] P007C6_n1501lccbC ;
   private java.util.Date[] P007C6_A1223lccbD ;
   private String[] P007C6_A1225lccbC ;
   private String[] P007C6_A1150lccbE ;
   private String[] P007C6_A1222lccbI ;
   private String[] P007C6_A1224lccbC ;
   private String[] P007C6_A1226lccbA ;
   private String[] P007C6_A1227lccbO ;
   private String[] P007C6_A1228lccbF ;
   private String[] P007C6_A1231lccbT ;
   private String[] P007C6_A1232lccbT ;
}

final  class palteradadosencriptacao__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007C2", "SELECT [lccbCCNumEnc], [lccbDate], [lccbCCNum], [lccbEmpCod], [lccbIATA], [lccbCCard], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbCCNumEnc] = '') AND ([lccbDate] = ?) ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007C3", "UPDATE [LCCBPLP] SET [lccbCCNumEnc]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007C4", "SELECT [lccbCCNumEnc1], [lccbDate], [lccbCCNum], [lccbEmpCod], [lccbIATA], [lccbCCard], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq] FROM [LCCBPLP1] WITH (UPDLOCK) WHERE ([lccbCCNumEnc1] = '') AND ([lccbDate] = ?) ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007C5", "UPDATE [LCCBPLP1] SET [lccbCCNumEnc1]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbSubStDate] = ? AND [lccbSubStSeq] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007C6", "SELECT [lccbCCNumEnc2], [lccbDate], [lccbCCNum], [lccbEmpCod], [lccbIATA], [lccbCCard], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC] FROM [LCCBPLP2] WITH (UPDLOCK) WHERE ([lccbCCNumEnc2] = '') AND ([lccbDate] = ?) ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007C7", "UPDATE [LCCBPLP2] SET [lccbCCNumEnc2]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 44) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 7) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 19) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 44) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 7) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 19) ;
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDateTime(10) ;
               ((short[]) buf[11])[0] = rslt.getShort(11) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 44) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 7) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 19) ;
               ((String[]) buf[10])[0] = rslt.getString(10, 10) ;
               ((String[]) buf[11])[0] = rslt.getString(11, 4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 44);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               break;
            case 2 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               break;
            case 3 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 44);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               stmt.setDateTime(10, (java.util.Date)parms[10], false);
               stmt.setShort(11, ((Number) parms[11]).shortValue());
               break;
            case 4 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 44);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               stmt.setString(10, (String)parms[10], 10);
               stmt.setString(11, (String)parms[11], 4);
               break;
      }
   }

}


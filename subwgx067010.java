import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx067010 extends GXSubfileElement
{
   private String ContactTypesCode ;
   private String ContactTypesDescription ;
   public String getContactTypesCode( )
   {
      return ContactTypesCode ;
   }

   public void setContactTypesCode( String value )
   {
      ContactTypesCode = value;
   }

   public String getContactTypesDescription( )
   {
      return ContactTypesDescription ;
   }

   public void setContactTypesDescription( String value )
   {
      ContactTypesDescription = value;
   }

   public void clear( )
   {
      ContactTypesCode = "" ;
      ContactTypesDescription = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getContactTypesCode().compareTo(((subwgx067010) element).getContactTypesCode()) ;
            case 1 :
               return  getContactTypesDescription().compareTo(((subwgx067010) element).getContactTypesDescription()) ;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getContactTypesCode(), "") == 0 ) && ( GXutil.strcmp(getContactTypesDescription(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getContactTypesCode() );
            break;
         case 1 :
            cell.setValue( getContactTypesDescription() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getContactTypesCode()) == 0) );
         case 1 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getContactTypesDescription()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setContactTypesCode(value.getStringValue());
               break;
            case 1 :
               setContactTypesDescription(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setContactTypesCode(((subwgx067010) element).getContactTypesCode());
               return;
            case 1 :
               setContactTypesDescription(((subwgx067010) element).getContactTypesDescription());
               return;
      }
   }

}


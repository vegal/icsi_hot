CLS = \
       pr2getparm.class \
       patualizaconfig.class \
       pnewaudit.class \
       umaintenance.class \
       wmaintenance.class \
       GXcfg.class \

all  : $(CLS)
	@touch umaintenance.class

GXcfg.class : GXcfg.java
	@echo Compiling GXcfg.java
	@$(JAVAC) $(CFLAGS) GXcfg.java
pr2getparm.class : pr2getparm.java
	@echo Compiling pr2getparm.java
	@$(JAVAC) $(CFLAGS) pr2getparm.java
patualizaconfig.class : patualizaconfig.java
	@echo Compiling patualizaconfig.java
	@$(JAVAC) $(CFLAGS) patualizaconfig.java
pnewaudit.class : pnewaudit.java
	@echo Compiling pnewaudit.java
	@$(JAVAC) $(CFLAGS) pnewaudit.java
umaintenance.class : umaintenance.java
	@echo Compiling umaintenance.java
	@$(JAVAC) $(CFLAGS) umaintenance.java
wmaintenance.class : wmaintenance.java
	@echo Compiling wmaintenance.java
	@$(JAVAC) $(CFLAGS) wmaintenance.java

/*
               File: Gx06C2
        Description: Selection List Contacts
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:16.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx06c2 extends GXWorkpanel
{
   public wgx06c2( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx06c2.class ));
   }

   public wgx06c2( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx06C2" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List Contacts" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 654 ;
   }

   protected int getFrmHeight( )
   {
      return 402 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx06C2.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String[] aP2 ,
                        short[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String[] aP2 ,
                             short[] aP3 )
   {
      wgx06c2.this.A23ISOCod = aP0;
      wgx06c2.this.A1136AirLi = aP1;
      wgx06c2.this.aP2 = aP2;
      wgx06c2.this.aP3 = aP3;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load20( ) throws GXLoadInterruptException
   {
      subwgx06c220 = new subwgx06c220 ();
      lV5cContac = GXutil.padr( GXutil.rtrim( AV5cContac), (short)(5), "%") ;
      lV7cAirLin = GXutil.padr( GXutil.rtrim( AV7cAirLin), (short)(30), "%") ;
      lV8cAirLin = GXutil.padr( GXutil.rtrim( AV8cAirLin), (short)(25), "%") ;
      lV9cAirLin = GXutil.padr( GXutil.rtrim( AV9cAirLin), (short)(40), "%") ;
      /* Using cursor W00142 */
      pr_default.execute(0, new Object[] {A23ISOCod, A1136AirLi, lV5cContac, new Short(AV6cAirLin), lV7cAirLin, lV8cAirLin, lV9cAirLin});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A1134AirLi = W00142_A1134AirLi[0] ;
         A1135AirLi = W00142_A1135AirLi[0] ;
         A1133AirLi = W00142_A1133AirLi[0] ;
         A1138AirLi = W00142_A1138AirLi[0] ;
         A365Contac = W00142_A365Contac[0] ;
         /* Execute user event: e11V142 */
         e11V142 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx06C2_load20 extends GXLoadProducer
   {
      wgx06c2 _sf ;

      public Gx06C2_load20( wgx06c2 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer20();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load20();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors20();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow20( )
   {
      return true;
   }

   public void autoRefresh_flow20( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cContac, cV5cContac)!=0)||(AV6cAirLin!=cV6cAirLin)||(GXutil.strcmp(AV7cAirLin, cV7cAirLin)!=0)||(GXutil.strcmp(AV8cAirLin, cV8cAirLin)!=0)||(GXutil.strcmp(AV9cAirLin, cV9cAirLin)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow20() )) {
         subfile.refresh();
         resetSubfileConditions_flow20() ;
      }
   }

   public boolean getSearch_flow20( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow20( )
   {
      cV5cContac = AV5cContac ;
      cV6cAirLin = AV6cAirLin ;
      cV7cAirLin = AV7cAirLin ;
      cV8cAirLin = AV8cAirLin ;
      cV9cAirLin = AV9cAirLin ;
   }

   public void resetSearchConditions_flow20( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow20( )
   {
      return new subwgx06c220 ();
   }

   public boolean getSearch_flow20( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow20( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow20( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow20( )
   {
      GXRefreshCommand20 ();
   }

   public final  class Gx06C2_flow20 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx06c2 _sf ;

      public Gx06C2_flow20( wgx06c2 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow20();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow20(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow20();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow20();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow20(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow20();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow20(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow20(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow20(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow20();
      }

   }

   protected void GXRefreshCommand20( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V142 */
      e12V142 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V142( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV10pConta = A365Contac ;
      AV11pAirLi = A1138AirLi ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer20( )
   {
      subwgx06c220 oAux = subwgx06c220 ;
      subwgx06c220 = new subwgx06c220 ();
      variablesToSubfile20 ();
      subGrd_1.addElement(subwgx06c220);
      subwgx06c220 = oAux;
   }

   private void e11V142( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors20( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 654 , 402 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      dynISOCod = new GUIObjectString ( new GXComboBox(GXPanel1, this, 6) , GXPanel1 , 168 , 24 , 47 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A23ISOCod" );
      dynISOCod.addFocusListener(this);
      dynISOCod.addItemListener(this);
      dynISOCod.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      edtAirLineCode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),168, 48, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), true) , GXPanel1 , 168 , 48 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1136AirLi" );
      ((GXEdit) edtAirLineCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineCode.addFocusListener(this);
      edtAirLineCode.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      edtavCcontacttypescode = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),168, 72, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 72 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cContac" );
      ((GXEdit) edtavCcontacttypescode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCcontacttypescode.addFocusListener(this);
      edtavCcontacttypescode.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      edtavCairlinecttseq = new GUIObjectShort ( new GXEdit(4, "ZZZ9", UIFactory.getFont( "Courier New", 0, 9),168, 96, 38, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 96 , 38 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6cAirLin" );
      ((GXEdit) edtavCairlinecttseq.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtavCairlinecttseq.addFocusListener(this);
      edtavCairlinecttseq.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      edtavCairlinecttname = new GUIObjectString ( new GXEdit(30, "@!", UIFactory.getFont( "Courier New", 0, 9),168, 120, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 120 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV7cAirLin" );
      ((GXEdit) edtavCairlinecttname.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecttname.addFocusListener(this);
      edtavCairlinecttname.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      edtavCairlinecttphone = new GUIObjectString ( new GXEdit(25, "XXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),168, 144, 185, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 144 , 185 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV8cAirLin" );
      ((GXEdit) edtavCairlinecttphone.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecttphone.addFocusListener(this);
      edtavCairlinecttphone.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      edtavCairlinecttemail = new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),168, 168, 290, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 168 , 168 , 290 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV9cAirLin" );
      ((GXEdit) edtavCairlinecttemail.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecttemail.addFocusListener(this);
      edtavCairlinecttemail.getGXComponent().setHelpId("HLP_WGx06C2.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx06C2_load20(this), new Gx06C2_flow20(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 119, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 118 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A365Contac" ), "Contact Type Code"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 118 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectShort ( new GXEdit(4, "ZZZ9", UIFactory.getFont( "Courier New", 0, 9),0, 0, 115, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.NUMERIC, false, false, 0, false) , null ,  0 , 0 , 114 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1138AirLi" ), "Contact Sequence"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 114 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(30, "@!", UIFactory.getFont( "Courier New", 0, 9),0, 0, 218, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 217 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1133AirLi" ), "Contact Name"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 217 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 192 , 507 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  546 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  546 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  546 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  546 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Country code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 76 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "AirLine Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 73 , 13 );
      lbllbl11 = UIFactory.getLabel(GXPanel1, "Contact Type Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 72 , 110 , 13 );
      lbllbl13 = UIFactory.getLabel(GXPanel1, "Contact Sequence", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 96 , 106 , 13 );
      lbllbl15 = UIFactory.getLabel(GXPanel1, "Contact Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 120 , 81 , 13 );
      lbllbl17 = UIFactory.getLabel(GXPanel1, "Contact Name Phone", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 144 , 121 , 13 );
      lbllbl19 = UIFactory.getLabel(GXPanel1, "Contact Email", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 168 , 79 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                dynISOCod ,
                edtAirLineCode ,
                edtavCcontacttypescode ,
                edtavCairlinecttseq ,
                edtavCairlinecttname ,
                edtavCairlinecttphone ,
                edtavCairlinecttemail ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
      if ( (id == 6) || (id == 0) )
      {
         /* Using cursor W00143 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            ((GXComboBox) dynISOCod.getGXComponent()).addItem(W00143_A23ISOCod[0], W00143_A23ISOCod[0], (short)(0));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }
   }

   protected void variablesToSubfile20( )
   {
      subwgx06c220.setContactTypesCode(A365Contac);
      subwgx06c220.setAirLineCttSeq(A1138AirLi);
      subwgx06c220.setAirLineCttName(A1133AirLi);
      subwgx06c220.setISOCod(A23ISOCod);
      subwgx06c220.setAirLineCode(A1136AirLi);
   }

   protected void subfileToVariables20( )
   {
      A365Contac = subwgx06c220.getContactTypesCode();
      A1138AirLi = subwgx06c220.getAirLineCttSeq();
      A1133AirLi = subwgx06c220.getAirLineCttName();
      A23ISOCod = subwgx06c220.getISOCod();
      A1136AirLi = subwgx06c220.getAirLineCode();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      dynISOCod.setValue( A23ISOCod );
      edtAirLineCode.setValue( A1136AirLi );
      edtavCcontacttypescode.setValue( AV5cContac );
      edtavCairlinecttseq.setValue( AV6cAirLin );
      edtavCairlinecttname.setValue( AV7cAirLin );
      edtavCairlinecttphone.setValue( AV8cAirLin );
      edtavCairlinecttemail.setValue( AV9cAirLin );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      A23ISOCod = dynISOCod.getValue() ;
      A1136AirLi = edtAirLineCode.getValue() ;
      AV5cContac = edtavCcontacttypescode.getValue() ;
      AV6cAirLin = edtavCairlinecttseq.getValue() ;
      AV7cAirLin = edtavCairlinecttname.getValue() ;
      AV8cAirLin = edtavCairlinecttphone.getValue() ;
      AV9cAirLin = edtavCairlinecttemail.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx06c220 = ( subwgx06c220 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06c220 = new subwgx06c220 ();
      }
      subfileToVariables20 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile20 ();
      subGrd_1.refreshLineValue(subwgx06c220);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx06c220 = ( subwgx06c220 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06c220 = new subwgx06c220 ();
      }
      subfileToVariables20 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V142 */
         e12V142 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V142 */
         e12V142 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( dynISOCod.isEventSource(eventSource) ) {
         setGXCursor( dynISOCod.getGXCursor() );
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineCode.getGXCursor() );
         return;
      }
      if ( edtavCcontacttypescode.isEventSource(eventSource) ) {
         setGXCursor( edtavCcontacttypescode.getGXCursor() );
         return;
      }
      if ( edtavCairlinecttseq.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecttseq.getGXCursor() );
         return;
      }
      if ( edtavCairlinecttname.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecttname.getGXCursor() );
         return;
      }
      if ( edtavCairlinecttphone.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecttphone.getGXCursor() );
         return;
      }
      if ( edtavCairlinecttemail.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecttemail.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( dynISOCod.isEventSource(eventSource) ) {
         A23ISOCod = dynISOCod.getValue() ;
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         A1136AirLi = edtAirLineCode.getValue() ;
         return;
      }
      if ( edtavCcontacttypescode.isEventSource(eventSource) ) {
         AV5cContac = edtavCcontacttypescode.getValue() ;
         return;
      }
      if ( edtavCairlinecttseq.isEventSource(eventSource) ) {
         AV6cAirLin = edtavCairlinecttseq.getValue() ;
         return;
      }
      if ( edtavCairlinecttname.isEventSource(eventSource) ) {
         AV7cAirLin = edtavCairlinecttname.getValue() ;
         return;
      }
      if ( edtavCairlinecttphone.isEventSource(eventSource) ) {
         AV8cAirLin = edtavCairlinecttphone.getValue() ;
         return;
      }
      if ( edtavCairlinecttemail.isEventSource(eventSource) ) {
         AV9cAirLin = edtavCairlinecttemail.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V142 */
         e12V142 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP2[0] = wgx06c2.this.AV10pConta;
      this.aP3[0] = wgx06c2.this.AV11pAirLi;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV10pConta = "" ;
      AV11pAirLi = (short)(0) ;
      subwgx06c220 = new subwgx06c220();
      scmdbuf = "" ;
      lV5cContac = "" ;
      AV5cContac = "" ;
      lV7cAirLin = "" ;
      AV7cAirLin = "" ;
      lV8cAirLin = "" ;
      AV8cAirLin = "" ;
      lV9cAirLin = "" ;
      AV9cAirLin = "" ;
      AV6cAirLin = (short)(0) ;
      W00142_A23ISOCod = new String[] {""} ;
      W00142_A1136AirLi = new String[] {""} ;
      W00142_A1134AirLi = new String[] {""} ;
      W00142_A1135AirLi = new String[] {""} ;
      W00142_A1133AirLi = new String[] {""} ;
      W00142_A1138AirLi = new short[1] ;
      W00142_A365Contac = new String[] {""} ;
      A1134AirLi = "" ;
      A1135AirLi = "" ;
      A1133AirLi = "" ;
      A1138AirLi = (short)(0) ;
      A365Contac = "" ;
      gxIsRefreshing = false ;
      cV5cContac = "" ;
      cV6cAirLin = (short)(0) ;
      cV7cAirLin = "" ;
      cV8cAirLin = "" ;
      cV9cAirLin = "" ;
      returnInSub = false ;
      W00143_A23ISOCod = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx06c2__default(),
         new Object[] {
             new Object[] {
            W00142_A23ISOCod, W00142_A1136AirLi, W00142_A1134AirLi, W00142_A1135AirLi, W00142_A1133AirLi, W00142_A1138AirLi, W00142_A365Contac
            }
            , new Object[] {
            W00143_A23ISOCod
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short AV11pAirLi ;
   protected short AV6cAirLin ;
   protected short A1138AirLi ;
   protected short cV6cAirLin ;
   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String A23ISOCod ;
   protected String A1136AirLi ;
   protected String AV10pConta ;
   protected String lV5cContac ;
   protected String AV5cContac ;
   protected String lV7cAirLin ;
   protected String AV7cAirLin ;
   protected String lV8cAirLin ;
   protected String AV8cAirLin ;
   protected String lV9cAirLin ;
   protected String AV9cAirLin ;
   protected String A1134AirLi ;
   protected String A1135AirLi ;
   protected String A1133AirLi ;
   protected String A365Contac ;
   protected String cV5cContac ;
   protected String cV7cAirLin ;
   protected String cV8cAirLin ;
   protected String cV9cAirLin ;
   protected String[] aP2 ;
   protected short[] aP3 ;
   protected subwgx06c220 subwgx06c220 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00142_A23ISOCod ;
   protected String[] W00142_A1136AirLi ;
   protected String[] W00142_A1134AirLi ;
   protected String[] W00142_A1135AirLi ;
   protected String[] W00142_A1133AirLi ;
   protected short[] W00142_A1138AirLi ;
   protected String[] W00142_A365Contac ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString dynISOCod ;
   protected GUIObjectString edtAirLineCode ;
   protected GUIObjectString edtavCcontacttypescode ;
   protected GUIObjectShort edtavCairlinecttseq ;
   protected GUIObjectString edtavCairlinecttname ;
   protected GUIObjectString edtavCairlinecttphone ;
   protected GUIObjectString edtavCairlinecttemail ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
   protected ILabel lbllbl11 ;
   protected ILabel lbllbl13 ;
   protected ILabel lbllbl15 ;
   protected ILabel lbllbl17 ;
   protected ILabel lbllbl19 ;
   protected String[] W00143_A23ISOCod ;
}

final  class wgx06c2__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00142", "SELECT [ISOCod], [AirLineCode], [AirLineCttPhone], [AirLineCttEmail], [AirLineCttName], [AirLineCttSeq], [ContactTypesCode] FROM [AIRLINESCONTACTS] WITH (FASTFIRSTROW NOLOCK) WHERE ([ISOCod] = ? and [AirLineCode] = ?) AND ([ContactTypesCode] like ?) AND ([AirLineCttSeq] >= ?) AND ([AirLineCttName] like ?) AND ([AirLineCttPhone] like ?) AND ([AirLineCttEmail] like ?) ORDER BY [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("W00143", "SELECT [ISOCod] FROM [COUNTRY] WITH (NOLOCK) ORDER BY [ISOCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((short[]) buf[5])[0] = rslt.getShort(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               stmt.setVarchar(5, (String)parms[4], 30);
               stmt.setVarchar(6, (String)parms[5], 25);
               stmt.setVarchar(7, (String)parms[6], 40);
               break;
      }
   }

}


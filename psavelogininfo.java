/*
               File: SaveLoginInfo
        Description: Save Login Information
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class psavelogininfo extends GXProcedure
{
   public psavelogininfo( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psavelogininfo.class ), "" );
   }

   public psavelogininfo( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      psavelogininfo.this.AV8lgnLogi = aP0[0];
      this.aP0 = aP0;
      psavelogininfo.this.AV12lgnPwd = aP1[0];
      this.aP1 = aP1;
      psavelogininfo.this.AV11lgnMod = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV13s ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PWDCHGKEEP", "Number of days to keep Passwords", "S", "90", GXv_svchar2) ;
      psavelogininfo.this.GXt_char1 = GXv_svchar2[0] ;
      AV13s = GXt_char1 ;
      AV9PwdKeep = (int)(GXutil.val( AV13s, ".")) ;
      AV10PwdDel = GXutil.dadd(Gx_date,-(AV9PwdKeep)) ;
      if ( ( GXutil.strcmp(AV11lgnMod, "INS") == 0 ) )
      {
         /*
            INSERT RECORD ON TABLE LOGINS

         */
         A30lgnLogi = AV8lgnLogi ;
         A43lgnName = "First User" ;
         n43lgnName = false ;
         A55lgnEnab = (byte)(1) ;
         n55lgnEnab = false ;
         A56lgnEmai = "" ;
         n56lgnEmai = false ;
         A58lgnLast = GXutil.now(true, false) ;
         n58lgnLast = false ;
         A60lgnPWDF = (byte)(0) ;
         n60lgnPWDF = false ;
         A57lgnPwd = AV12lgnPwd ;
         n57lgnPwd = false ;
         A59lgnPwdL = GXutil.now(true, false) ;
         n59lgnPwdL = false ;
         A61lgnSupe = (byte)(1) ;
         n61lgnSupe = false ;
         /*
            INSERT RECORD ON TABLE LOGINSLASTLOGININFO

         */
         W30lgnLogi = A30lgnLogi ;
         A30lgnLogi = AV8lgnLogi ;
         A63lgnLast = AV12lgnPwd ;
         A62lgnLast = GXutil.resetTime(GXutil.now(true, false)) ;
         n62lgnLast = false ;
         /* Using cursor P00152 */
         pr_default.execute(0, new Object[] {A30lgnLogi, A63lgnLast, new Boolean(n62lgnLast), A62lgnLast});
         if ( (pr_default.getStatus(0) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         A30lgnLogi = W30lgnLogi ;
         /* End Insert */
         /* Using cursor P00153 */
         pr_default.execute(1, new Object[] {A30lgnLogi, new Boolean(n43lgnName), A43lgnName, new Boolean(n55lgnEnab), new Byte(A55lgnEnab), new Boolean(n56lgnEmai), A56lgnEmai, new Boolean(n57lgnPwd), A57lgnPwd, new Boolean(n58lgnLast), A58lgnLast, new Boolean(n59lgnPwdL), A59lgnPwdL, new Boolean(n60lgnPWDF), new Byte(A60lgnPWDF), new Boolean(n61lgnSupe), new Byte(A61lgnSupe)});
         if ( (pr_default.getStatus(1) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      /* Using cursor P00154 */
      pr_default.execute(2, new Object[] {AV8lgnLogi, AV8lgnLogi});
      while ( (pr_default.getStatus(2) != 101) )
      {
         A30lgnLogi = P00154_A30lgnLogi[0] ;
         A60lgnPWDF = P00154_A60lgnPWDF[0] ;
         n60lgnPWDF = P00154_n60lgnPWDF[0] ;
         A57lgnPwd = P00154_A57lgnPwd[0] ;
         n57lgnPwd = P00154_n57lgnPwd[0] ;
         A58lgnLast = P00154_A58lgnLast[0] ;
         n58lgnLast = P00154_n58lgnLast[0] ;
         W30lgnLogi = A30lgnLogi ;
         if ( ( GXutil.strcmp(AV11lgnMod, "PWD") == 0 ) )
         {
            A60lgnPWDF = (byte)(0) ;
            n60lgnPWDF = false ;
            A57lgnPwd = AV12lgnPwd ;
            n57lgnPwd = false ;
            A58lgnLast = GXutil.now(true, false) ;
            n58lgnLast = false ;
         }
         else if ( ( GXutil.strcmp(AV11lgnMod, "CHG") == 0 ) )
         {
            A60lgnPWDF = (byte)(1) ;
            n60lgnPWDF = false ;
            A57lgnPwd = AV12lgnPwd ;
            n57lgnPwd = false ;
            A58lgnLast = GXutil.now(true, false) ;
            n58lgnLast = false ;
         }
         else if ( ( GXutil.strcmp(AV11lgnMod, "LOG") == 0 ) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00155 */
            pr_default.execute(3, new Object[] {A30lgnLogi, AV10PwdDel});
            /* End optimized DELETE. */
            /*
               INSERT RECORD ON TABLE LOGINSLASTLOGININFO

            */
            W30lgnLogi = A30lgnLogi ;
            A30lgnLogi = AV8lgnLogi ;
            A62lgnLast = GXutil.resetTime(GXutil.now(true, false)) ;
            n62lgnLast = false ;
            A63lgnLast = AV12lgnPwd ;
            /* Using cursor P00156 */
            pr_default.execute(4, new Object[] {A30lgnLogi, A63lgnLast, new Boolean(n62lgnLast), A62lgnLast});
            if ( (pr_default.getStatus(4) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            A30lgnLogi = W30lgnLogi ;
            /* End Insert */
         }
         /* Using cursor P00157 */
         pr_default.execute(5, new Object[] {new Boolean(n60lgnPWDF), new Byte(A60lgnPWDF), new Boolean(n57lgnPwd), A57lgnPwd, new Boolean(n58lgnLast), A58lgnLast, A30lgnLogi});
         A30lgnLogi = W30lgnLogi ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(2);
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = psavelogininfo.this.AV8lgnLogi;
      this.aP1[0] = psavelogininfo.this.AV12lgnPwd;
      this.aP2[0] = psavelogininfo.this.AV11lgnMod;
      Application.commit(context, remoteHandle, "DEFAULT", "psavelogininfo");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV13s = "" ;
      GXt_char1 = "" ;
      GXv_svchar2 = new String [1] ;
      AV9PwdKeep = 0 ;
      AV10PwdDel = GXutil.nullDate() ;
      Gx_date = GXutil.nullDate() ;
      GX_INS187 = 0 ;
      A30lgnLogi = "" ;
      A43lgnName = "" ;
      n43lgnName = false ;
      A55lgnEnab = (byte)(0) ;
      n55lgnEnab = false ;
      A56lgnEmai = "" ;
      n56lgnEmai = false ;
      A58lgnLast = GXutil.resetTime( GXutil.nullDate() );
      n58lgnLast = false ;
      A60lgnPWDF = (byte)(0) ;
      n60lgnPWDF = false ;
      A57lgnPwd = "" ;
      n57lgnPwd = false ;
      A59lgnPwdL = GXutil.resetTime( GXutil.nullDate() );
      n59lgnPwdL = false ;
      A61lgnSupe = (byte)(0) ;
      n61lgnSupe = false ;
      GX_INS188 = 0 ;
      W30lgnLogi = "" ;
      A63lgnLast = "" ;
      A62lgnLast = GXutil.nullDate() ;
      n62lgnLast = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      returnInSub = false ;
      scmdbuf = "" ;
      P00154_A30lgnLogi = new String[] {""} ;
      P00154_A60lgnPWDF = new byte[1] ;
      P00154_n60lgnPWDF = new boolean[] {false} ;
      P00154_A57lgnPwd = new String[] {""} ;
      P00154_n57lgnPwd = new boolean[] {false} ;
      P00154_A58lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      P00154_n58lgnLast = new boolean[] {false} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new psavelogininfo__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P00154_A30lgnLogi, P00154_A60lgnPWDF, P00154_n60lgnPWDF, P00154_A57lgnPwd, P00154_n57lgnPwd, P00154_A58lgnLast, P00154_n58lgnLast
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte A55lgnEnab ;
   private byte A60lgnPWDF ;
   private byte A61lgnSupe ;
   private short Gx_err ;
   private int AV9PwdKeep ;
   private int GX_INS187 ;
   private int GX_INS188 ;
   private String AV11lgnMod ;
   private String AV13s ;
   private String GXt_char1 ;
   private String Gx_emsg ;
   private String scmdbuf ;
   private java.util.Date A58lgnLast ;
   private java.util.Date A59lgnPwdL ;
   private java.util.Date AV10PwdDel ;
   private java.util.Date Gx_date ;
   private java.util.Date A62lgnLast ;
   private boolean n43lgnName ;
   private boolean n55lgnEnab ;
   private boolean n56lgnEmai ;
   private boolean n58lgnLast ;
   private boolean n60lgnPWDF ;
   private boolean n57lgnPwd ;
   private boolean n59lgnPwdL ;
   private boolean n61lgnSupe ;
   private boolean n62lgnLast ;
   private boolean returnInSub ;
   private String AV8lgnLogi ;
   private String AV12lgnPwd ;
   private String GXv_svchar2[] ;
   private String A30lgnLogi ;
   private String A43lgnName ;
   private String A56lgnEmai ;
   private String A57lgnPwd ;
   private String W30lgnLogi ;
   private String A63lgnLast ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P00154_A30lgnLogi ;
   private byte[] P00154_A60lgnPWDF ;
   private boolean[] P00154_n60lgnPWDF ;
   private String[] P00154_A57lgnPwd ;
   private boolean[] P00154_n57lgnPwd ;
   private java.util.Date[] P00154_A58lgnLast ;
   private boolean[] P00154_n58lgnLast ;
}

final  class psavelogininfo__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P00152", "INSERT INTO [LOGINSLASTLOGININFO] ([lgnLogin], [lgnLastPwd], [lgnLastPwdDate]) VALUES (?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P00153", "INSERT INTO [LOGINS] ([lgnLogin], [lgnName], [lgnEnable], [lgnEmail], [lgnPwd], [lgnLastLogin], [lgnPwdLastChanged], [lgnPWDForceChange], [lgnSuperUser], [lgnPwdTmp]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00154", "SELECT [lgnLogin], [lgnPWDForceChange], [lgnPwd], [lgnLastLogin] FROM [LOGINS] WITH (UPDLOCK) WHERE ([lgnLogin] = ?) AND ([lgnLogin] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P00155", "DELETE FROM [LOGINSLASTLOGININFO]  WHERE ([lgnLogin] = ?) AND ([lgnLastPwdDate] < ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P00156", "INSERT INTO [LOGINSLASTLOGININFO] ([lgnLogin], [lgnLastPwd], [lgnLastPwdDate]) VALUES (?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P00157", "UPDATE [LOGINS] SET [lgnPWDForceChange]=?, [lgnPwd]=?, [lgnLastLogin]=?  WHERE [lgnLogin] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((byte[]) buf[1])[0] = rslt.getByte(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDateTime(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 255, false);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(3, (java.util.Date)parms[3]);
               }
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 50);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(3, ((Number) parms[4]).byteValue());
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[6], 80);
               }
               if ( ((Boolean) parms[7]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[8], 255);
               }
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(6, (java.util.Date)parms[10], false);
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(7, (java.util.Date)parms[12], false);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(8, ((Number) parms[14]).byteValue());
               }
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(9, ((Number) parms[16]).byteValue());
               }
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20);
               stmt.setVarchar(2, (String)parms[1], 20);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setDate(2, (java.util.Date)parms[1]);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 255, false);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(3, (java.util.Date)parms[3]);
               }
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(1, ((Number) parms[1]).byteValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 255);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(3, (java.util.Date)parms[5], false);
               }
               stmt.setVarchar(4, (String)parms[6], 20, false);
               break;
      }
   }

}


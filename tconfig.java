/*
               File: Config
        Description: System Configurations
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:0.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tconfig extends GXTransaction
{
   public tconfig( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tconfig.class ), "" );
   }

   public tconfig( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey033( )
   {
      A17ConfigV = "" ;
      A18ConfigD = "" ;
   }

   public void initAll033( )
   {
      A19ConfigI = "" ;
      K19ConfigI = A19ConfigI ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey033( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption030( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "Config" ;
   }

   protected String getFrmTitle( )
   {
      return "System Configurations" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return null ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 710 ;
   }

   protected int getFrmHeight( )
   {
      return 307 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TConfig.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return false;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 24 , 710 , 307 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtConfigID = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),167, 99, 244, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 167 , 99 , 244 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A19ConfigI" );
      ((GXEdit) edtConfigID.getGXComponent()).setAlignment(ILabel.LEFT);
      edtConfigID.addFocusListener(this);
      edtConfigID.getGXComponent().setHelpId("HLP_TConfig.htm");
      edtConfigValue = new GUIObjectString ( new GXEdit(255, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),167, 123, 430, 87, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 167 , 123 , 430 , 87 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A17ConfigV" );
      ((GXEdit) edtConfigValue.getGXComponent()).setAlignment(ILabel.LEFT);
      edtConfigValue.addFocusListener(this);
      edtConfigValue.getGXComponent().setHelpId("HLP_TConfig.htm");
      edtConfigDes = new GUIObjectString ( new GXEdit(100, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),167, 219, 430, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 167 , 219 , 430 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A18ConfigD" );
      ((GXEdit) edtConfigDes.getGXComponent()).setAlignment(ILabel.LEFT);
      edtConfigDes.addFocusListener(this);
      edtConfigDes.getGXComponent().setHelpId("HLP_TConfig.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  13 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_previous = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  144 ,  13 ,  40 ,  40  );
      bttBtn_previous.setTooltip("<");
      bttBtn_previous.addActionListener(this);
      bttBtn_previous.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  208 ,  13 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  250 ,  13 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_select = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  313 ,  13 ,  40 ,  40  );
      bttBtn_select.setTooltip("Select");
      bttBtn_select.addActionListener(this);
      bttBtn_select.setFiresEvents(false);
      bttBtn_delete = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  469 ,  13 ,  40 ,  40  );
      bttBtn_delete.setTooltip("Delete");
      bttBtn_delete.addActionListener(this);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  581 ,  13 ,  40 ,  40  );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  659 ,  13 ,  40 ,  40  );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      lbllbl3 = UIFactory.getLabel(GXPanel1, "Par�metro:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 23 , 103 , 62 , 13 );
      lbllbl5 = UIFactory.getLabel(GXPanel1, "Valor:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 23 , 123 , 34 , 13 );
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Descri��o do Par�metro:", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 23 , 223 , 141 , 13 );
      imgimg8  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 5 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtConfigID ,
                edtConfigValue ,
                edtConfigDes ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_first ,
                bttBtn_previous ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_select ,
                bttBtn_delete
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtConfigID, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey033( ) ;
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_enter.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_enter.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_enter.setBitmap( "gxconfirm_add.gif" );
         bttBtn_enter.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_enter.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_enter.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_enter.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_enter.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll033( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_delete.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_delete( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_select.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_select( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_last.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_last( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_next.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_next( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_previous.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_previous( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_first.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_first( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtConfigID.isEventSource(eventSource) ) {
         setGXCursor( edtConfigID.getGXCursor() );
         return;
      }
      if ( edtConfigValue.isEventSource(eventSource) ) {
         setGXCursor( edtConfigValue.getGXCursor() );
         return;
      }
      if ( edtConfigDes.isEventSource(eventSource) ) {
         setGXCursor( edtConfigDes.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtConfigID.isEventSource(eventSource) ) {
         valid_Configid ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtConfigID.isEventSource(eventSource) ) {
         A19ConfigI = edtConfigID.getValue() ;
         return;
      }
      if ( edtConfigValue.isEventSource(eventSource) ) {
         A17ConfigV = edtConfigValue.getValue() ;
         return;
      }
      if ( edtConfigDes.isEventSource(eventSource) ) {
         A18ConfigD = edtConfigDes.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtConfigID.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A19ConfigI, edtConfigID.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtConfigValue.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A17ConfigV, edtConfigValue.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtConfigDes.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A18ConfigD, edtConfigDes.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption030( ) ;
   }

   protected void setAddCaption( )
   {
      bttBtn_enter.setBitmap( "gxconfirm_add.gif" );
      bttBtn_enter.setTooltip( localUtil.getMessages().getMessage("captionadd") );
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_enter ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_delete ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   protected void VariablesToControls( )
   {
      edtConfigID.setValue( A19ConfigI );
      edtConfigValue.setValue( A17ConfigV );
      edtConfigDes.setValue( A18ConfigD );
   }

   protected void ControlsToVariables( )
   {
      A19ConfigI = edtConfigID.getValue() ;
      A17ConfigV = edtConfigValue.getValue() ;
      A18ConfigD = edtConfigDes.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Configid( )
   {
      if ( ( GXutil.strcmp(A19ConfigI, K19ConfigI) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K19ConfigI = A19ConfigI ;
            getEqualNoModal( ) ;
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         Gx_mode = "INS" ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_enter.getBitmap() ;
   }

   public void zm033( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z17ConfigV = T00033_A17ConfigV[0] ;
            Z18ConfigD = T00033_A18ConfigD[0] ;
         }
         else
         {
            Z17ConfigV = A17ConfigV ;
            Z18ConfigD = A18ConfigD ;
         }
      }
      if ( ( GX_JID == -1 ) )
      {
         Z19ConfigI = A19ConfigI ;
         Z17ConfigV = A17ConfigV ;
         Z18ConfigD = A18ConfigD ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load033( )
   {
      /* Using cursor T00034 */
      pr_default.execute(2, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A17ConfigV = T00034_A17ConfigV[0] ;
         A18ConfigD = T00034_A18ConfigD[0] ;
         zm033( -1) ;
      }
      pr_default.close(2);
      onLoadActions033( ) ;
   }

   public void onLoadActions033( )
   {
   }

   public void checkExtendedTable033( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors033( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey033( )
   {
      /* Using cursor T00035 */
      pr_default.execute(3, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound3 = (short)(1) ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00033 */
      pr_default.execute(1, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm033( 1) ;
         RcdFound3 = (short)(1) ;
         A19ConfigI = T00033_A19ConfigI[0] ;
         A17ConfigV = T00033_A17ConfigV[0] ;
         A18ConfigD = T00033_A18ConfigD[0] ;
         Z19ConfigI = A19ConfigI ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load033( ) ;
         Gx_mode = sMode3 ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
         initializeNonKey033( ) ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode3 ;
      }
      K19ConfigI = A19ConfigI ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey033( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound3 = (short)(0) ;
      /* Using cursor T00036 */
      pr_default.execute(4, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T00036_A19ConfigI[0], A19ConfigI) < 0 ) ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T00036_A19ConfigI[0], A19ConfigI) > 0 ) ) )
         {
            A19ConfigI = T00036_A19ConfigI[0] ;
            RcdFound3 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound3 = (short)(0) ;
      /* Using cursor T00037 */
      pr_default.execute(5, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T00037_A19ConfigI[0], A19ConfigI) > 0 ) ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T00037_A19ConfigI[0], A19ConfigI) < 0 ) ) )
         {
            A19ConfigI = T00037_A19ConfigI[0] ;
            RcdFound3 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey033( ) ;
      if ( ( RcdFound3 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtConfigID );
         }
         else if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
         {
            A19ConfigI = Z19ConfigI ;
            edtConfigID.setValue(A19ConfigI);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtConfigID );
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update033( ) ;
            setNextFocus( edtConfigID );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
         {
            Gx_mode = "INS" ;
            /* Insert record */
            insert033( ) ;
            setNextFocus( edtConfigID );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtConfigID );
            }
            else
            {
               Gx_mode = "INS" ;
               /* Insert record */
               insert033( ) ;
               setNextFocus( edtConfigID );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
      {
         A19ConfigI = Z19ConfigI ;
         edtConfigID.setValue(A19ConfigI);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtConfigID );
      }
      else
      {
        // GXutil.confirm( me(), localUtil.getMessages().getMessage("deleinfo") , true );
         if ( GXutil.Confirmed )
         {
            delete( ) ;
            handleErrors();
            afterTrn( ) ;
            setNextFocus( edtConfigID );
         }
      }
      if ( ( AnyError != 0 ) )
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         pushError( localUtil.getMessages().getMessage("keynfound") );
         AnyError = (short)(1) ;
         keepFocus();
      }
      setNextFocus( edtConfigValue );
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart033( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      setNextFocus( edtConfigValue );
      scanEnd033( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      setNextFocus( edtConfigValue );
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      setNextFocus( edtConfigValue );
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart033( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         context.msgStatus( localUtil.getMessages().getMessage("norectobrow") );
      }
      else
      {
         while ( ( RcdFound3 != 0 ) )
         {
            scanNext033( ) ;
         }
         Gx_mode = "UPD" ;
      }
      setNextFocus( edtConfigValue );
      scanEnd033( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void btn_select( )
   {
      GXv_svchar1[0] = A19ConfigI ;
      new wgx0030(remoteHandle, context).execute( GXv_svchar1) ;
      tconfig.this.A19ConfigI = GXv_svchar1[0] ;
      edtConfigID.setValue(A19ConfigI);
      edtConfigID.setValue( A19ConfigI );
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency033( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T00032 */
         pr_default.execute(0, new Object[] {A19ConfigI});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"CONFIG"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z17ConfigV, T00032_A17ConfigV[0]) != 0 ) || ( GXutil.strcmp(Z18ConfigD, T00032_A18ConfigD[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"CONFIG"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert033( )
   {
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm033( 0) ;
         checkOptimisticConcurrency033( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm033( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert033( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T00038 */
                  pr_default.execute(6, new Object[] {A19ConfigI, A17ConfigV, A18ConfigD});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                        resetCaption030( ) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load033( ) ;
         }
         endLevel033( ) ;
      }
      closeExtendedTableCursors033( ) ;
   }

   public void update033( )
   {
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency033( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm033( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate033( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T00039 */
                  pr_default.execute(7, new Object[] {A17ConfigV, A18ConfigD, A19ConfigI});
                  deferredUpdate033( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        context.msgStatus( localUtil.getMessages().getMessage("sucupdated") );
                        resetCaption030( ) ;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel033( ) ;
      }
      closeExtendedTableCursors033( ) ;
   }

   public void deferredUpdate033( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls033( ) ;
         /* No cascading delete specified. */
         afterConfirm033( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete033( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T000310 */
               pr_default.execute(8, new Object[] {A19ConfigI});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     move_next( ) ;
                     if ( ( RcdFound3 == 0 ) )
                     {
                        initAll033( ) ;
                        Gx_mode = "INS" ;
                     }
                     else
                     {
                        getByPrimaryKey( ) ;
                        Gx_mode = "UPD" ;
                     }
                     context.msgStatus( localUtil.getMessages().getMessage("sucdeleted") );
                     resetCaption030( ) ;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode3 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel033( ) ;
      Gx_mode = sMode3 ;
   }

   public void onDeleteControls033( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel033( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete033( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tconfig");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tconfig");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart033( )
   {
      /* Using cursor T000311 */
      pr_default.execute(9);
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A19ConfigI = T000311_A19ConfigI[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext033( )
   {
      pr_default.readNext(9);
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A19ConfigI = T000311_A19ConfigI[0] ;
      }
   }

   public void scanEnd033( )
   {
      pr_default.close(9);
   }

   public void afterConfirm033( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert033( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate033( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete033( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete033( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate033( )
   {
      /* Before Validate Rules */
   }

   public void confirm_030( )
   {
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls033( ) ;
         }
         else
         {
            checkExtendedTable033( ) ;
            closeExtendedTableCursors033( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_enter.getBitmap() ;
         PreviousTooltip = bttBtn_enter.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A17ConfigV = "" ;
      A18ConfigD = "" ;
      A19ConfigI = "" ;
      K19ConfigI = "" ;
      lastAnyError = 0 ;
      Z17ConfigV = "" ;
      Z18ConfigD = "" ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z19ConfigI = "" ;
      T00034_A19ConfigI = new String[] {""} ;
      T00034_A17ConfigV = new String[] {""} ;
      T00034_A18ConfigD = new String[] {""} ;
      RcdFound3 = (short)(0) ;
      Gx_BScreen = (byte)(0) ;
      T00035_A19ConfigI = new String[] {""} ;
      T00033_A19ConfigI = new String[] {""} ;
      T00033_A17ConfigV = new String[] {""} ;
      T00033_A18ConfigD = new String[] {""} ;
      sMode3 = "" ;
      T00036_A19ConfigI = new String[] {""} ;
      T00037_A19ConfigI = new String[] {""} ;
      GXv_svchar1 = new String [1] ;
      T00032_A19ConfigI = new String[] {""} ;
      T00032_A17ConfigV = new String[] {""} ;
      T00032_A18ConfigD = new String[] {""} ;
      T000311_A19ConfigI = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tconfig__default(),
         new Object[] {
             new Object[] {
            T00032_A19ConfigI, T00032_A17ConfigV, T00032_A18ConfigD
            }
            , new Object[] {
            T00033_A19ConfigI, T00033_A17ConfigV, T00033_A18ConfigD
            }
            , new Object[] {
            T00034_A19ConfigI, T00034_A17ConfigV, T00034_A18ConfigD
            }
            , new Object[] {
            T00035_A19ConfigI
            }
            , new Object[] {
            T00036_A19ConfigI
            }
            , new Object[] {
            T00037_A19ConfigI
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000311_A19ConfigI
            }
         }
      );
      reloadDynamicLists(0);
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound3 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String scmdbuf ;
   protected String sMode3 ;
   protected String A17ConfigV ;
   protected String A18ConfigD ;
   protected String A19ConfigI ;
   protected String K19ConfigI ;
   protected String Z17ConfigV ;
   protected String Z18ConfigD ;
   protected String Z19ConfigI ;
   protected String GXv_svchar1[] ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtConfigID ;
   protected GUIObjectString edtConfigValue ;
   protected GUIObjectString edtConfigDes ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_previous ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_select ;
   protected IGXButton bttBtn_delete ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected ILabel lbllbl3 ;
   protected ILabel lbllbl5 ;
   protected ILabel lbllbl7 ;
   protected IGXImage imgimg8 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T00034_A19ConfigI ;
   protected String[] T00034_A17ConfigV ;
   protected String[] T00034_A18ConfigD ;
   protected String[] T00035_A19ConfigI ;
   protected String[] T00033_A19ConfigI ;
   protected String[] T00033_A17ConfigV ;
   protected String[] T00033_A18ConfigD ;
   protected String[] T00036_A19ConfigI ;
   protected String[] T00037_A19ConfigI ;
   protected String[] T00032_A19ConfigI ;
   protected String[] T00032_A17ConfigV ;
   protected String[] T00032_A18ConfigD ;
   protected String[] T000311_A19ConfigI ;
}

final  class tconfig__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00032", "SELECT [ConfigID], [ConfigValue], [ConfigDes] FROM [CONFIG] WITH (UPDLOCK) WHERE [ConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00033", "SELECT [ConfigID], [ConfigValue], [ConfigDes] FROM [CONFIG] WITH (NOLOCK) WHERE [ConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00034", "SELECT TM1.[ConfigID], TM1.[ConfigValue], TM1.[ConfigDes] FROM [CONFIG] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ConfigID] = ? ORDER BY TM1.[ConfigID] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00035", "SELECT [ConfigID] FROM [CONFIG] WITH (FASTFIRSTROW NOLOCK) WHERE [ConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00036", "SELECT TOP 1 [ConfigID] FROM [CONFIG] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ConfigID] > ?) ORDER BY [ConfigID] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00037", "SELECT TOP 1 [ConfigID] FROM [CONFIG] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ConfigID] < ?) ORDER BY [ConfigID] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T00038", "INSERT INTO [CONFIG] ([ConfigID], [ConfigValue], [ConfigDes]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T00039", "UPDATE [CONFIG] SET [ConfigValue]=?, [ConfigDes]=?  WHERE [ConfigID] = ?", GX_NOMASK)
         ,new UpdateCursor("T000310", "DELETE FROM [CONFIG]  WHERE [ConfigID] = ?", GX_NOMASK)
         ,new ForEachCursor("T000311", "SELECT [ConfigID] FROM [CONFIG] WITH (FASTFIRSTROW NOLOCK) ORDER BY [ConfigID] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 255, false);
               stmt.setVarchar(3, (String)parms[2], 100, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 255, false);
               stmt.setVarchar(2, (String)parms[1], 100, false);
               stmt.setVarchar(3, (String)parms[2], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


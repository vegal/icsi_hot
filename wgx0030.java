/*
               File: Gx0030
        Description: Selection List System Configurations
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:15.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx0030 extends GXWorkpanel
{
   public wgx0030( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx0030.class ));
   }

   public wgx0030( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx0030" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List System Configurations" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 381 ;
   }

   protected int getFrmHeight( )
   {
      return 258 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx0030.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      wgx0030.this.aP0 = aP0;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load08( ) throws GXLoadInterruptException
   {
      subwgx003008 = new subwgx003008 ();
      lV5cConfig = GXutil.padr( GXutil.rtrim( AV5cConfig), (short)(20), "%") ;
      /* Using cursor W000Y2 */
      pr_default.execute(0, new Object[] {lV5cConfig});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A19ConfigI = W000Y2_A19ConfigI[0] ;
         /* Execute user event: e11V0Y2 */
         e11V0Y2 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx0030_load08 extends GXLoadProducer
   {
      wgx0030 _sf ;

      public Gx0030_load08( wgx0030 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer08();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load08();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors08();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow08( )
   {
      return true;
   }

   public void autoRefresh_flow08( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cConfig, cV5cConfig)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow08() )) {
         subfile.refresh();
         resetSubfileConditions_flow08() ;
      }
   }

   public boolean getSearch_flow08( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow08( )
   {
      cV5cConfig = AV5cConfig ;
   }

   public void resetSearchConditions_flow08( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow08( )
   {
      return new subwgx003008 ();
   }

   public boolean getSearch_flow08( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow08( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow08( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow08( )
   {
      GXRefreshCommand08 ();
   }

   public final  class Gx0030_flow08 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx0030 _sf ;

      public Gx0030_flow08( wgx0030 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow08();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow08(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow08();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow08();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow08(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow08();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow08(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow08(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow08(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow08();
      }

   }

   protected void GXRefreshCommand08( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V0Y2 */
      e12V0Y2 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V0Y2( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV6pConfig = A19ConfigI ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer08( )
   {
      subwgx003008 oAux = subwgx003008 ;
      subwgx003008 = new subwgx003008 ();
      variablesToSubfile08 ();
      subGrd_1.addElement(subwgx003008);
      subwgx003008 = oAux;
   }

   private void e11V0Y2( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors08( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 381 , 258 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtavCconfigid = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),105, 24, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 105 , 24 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cConfig" );
      ((GXEdit) edtavCconfigid.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCconfigid.addFocusListener(this);
      edtavCconfigid.getGXComponent().setHelpId("HLP_WGx0030.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx0030_load08(this), new Gx0030_flow08(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 148, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 147 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A19ConfigI" ), "Par�metro"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 147 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 48 , 203 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  273 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  273 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  273 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  273 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Par�metro", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 58 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                edtavCconfigid ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void variablesToSubfile08( )
   {
      subwgx003008.setConfigID(A19ConfigI);
   }

   protected void subfileToVariables08( )
   {
      A19ConfigI = subwgx003008.getConfigID();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      edtavCconfigid.setValue( AV5cConfig );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      AV5cConfig = edtavCconfigid.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx003008 = ( subwgx003008 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx003008 = new subwgx003008 ();
      }
      subfileToVariables08 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile08 ();
      subGrd_1.refreshLineValue(subwgx003008);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx003008 = ( subwgx003008 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx003008 = new subwgx003008 ();
      }
      subfileToVariables08 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V0Y2 */
         e12V0Y2 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V0Y2 */
         e12V0Y2 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavCconfigid.isEventSource(eventSource) ) {
         setGXCursor( edtavCconfigid.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavCconfigid.isEventSource(eventSource) ) {
         AV5cConfig = edtavCconfigid.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V0Y2 */
         e12V0Y2 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = wgx0030.this.AV6pConfig;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV6pConfig = "" ;
      subwgx003008 = new subwgx003008();
      scmdbuf = "" ;
      lV5cConfig = "" ;
      AV5cConfig = "" ;
      W000Y2_A19ConfigI = new String[] {""} ;
      A19ConfigI = "" ;
      gxIsRefreshing = false ;
      cV5cConfig = "" ;
      returnInSub = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx0030__default(),
         new Object[] {
             new Object[] {
            W000Y2_A19ConfigI
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String AV6pConfig ;
   protected String lV5cConfig ;
   protected String AV5cConfig ;
   protected String A19ConfigI ;
   protected String cV5cConfig ;
   protected String[] aP0 ;
   protected subwgx003008 subwgx003008 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W000Y2_A19ConfigI ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtavCconfigid ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
}

final  class wgx0030__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W000Y2", "SELECT [ConfigID] FROM [CONFIG] WITH (FASTFIRSTROW NOLOCK) WHERE [ConfigID] like ? ORDER BY [ConfigID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20);
               break;
      }
   }

}


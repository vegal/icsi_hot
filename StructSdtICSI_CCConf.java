
public final  class StructSdtICSI_CCConf implements Cloneable, java.io.Serializable
{
   public StructSdtICSI_CCConf( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Level1 = new java.util.Vector();
      gxTv_SdtICSI_CCConf_Mode = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIcsi_ccconfcod( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfcod ;
   }

   public void setIcsi_ccconfcod( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod = value ;
      return  ;
   }

   public String getIcsi_ccconfname( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfname ;
   }

   public void setIcsi_ccconfname( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname = value ;
      return  ;
   }

   public byte getIcsi_ccconfenab( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfenab ;
   }

   public void setIcsi_ccconfenab( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab = value ;
      return  ;
   }

   public java.util.Vector getLevel1( )
   {
      return gxTv_SdtICSI_CCConf_Level1 ;
   }

   public void setLevel1( java.util.Vector value )
   {
      gxTv_SdtICSI_CCConf_Level1 = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtICSI_CCConf_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtICSI_CCConf_Mode = value ;
      return  ;
   }

   public String getIcsi_ccconfcod_Z( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z ;
   }

   public void setIcsi_ccconfcod_Z( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = value ;
      return  ;
   }

   public String getIcsi_ccconfname_Z( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z ;
   }

   public void setIcsi_ccconfname_Z( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = value ;
      return  ;
   }

   public byte getIcsi_ccconfenab_Z( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z ;
   }

   public void setIcsi_ccconfenab_Z( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = value ;
      return  ;
   }

   public byte getIcsi_ccconfname_N( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfname_N ;
   }

   public void setIcsi_ccconfname_N( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = value ;
      return  ;
   }

   public byte getIcsi_ccconfenab_N( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N ;
   }

   public void setIcsi_ccconfenab_N( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = value ;
      return  ;
   }

   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab ;
   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z ;
   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfname_N ;
   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfcod ;
   protected String gxTv_SdtICSI_CCConf_Mode ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfname ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z ;
   protected java.util.Vector gxTv_SdtICSI_CCConf_Level1 ;
}


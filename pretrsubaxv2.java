/*
               File: RETRSubAxV2
        Description: Amex Novo Layout
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:13.79
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pretrsubaxv2 extends GXProcedure
{
   public pretrsubaxv2( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretrsubaxv2.class ), "" );
   }

   public pretrsubaxv2( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pretrsubaxv2.this.AV109Debug = aP0[0];
      this.aP0 = aP0;
      pretrsubaxv2.this.AV114FileS = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV110Versa = "00001" ;
      AV109Debug = GXutil.trim( GXutil.upper( AV109Debug)) ;
      if ( ( GXutil.strSearch( AV109Debug, "TESTMODE", 1) > 0 ) )
      {
         AV111TestM = (byte)(1) ;
      }
      else
      {
         AV111TestM = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV109Debug, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "Amex Submission Feedback - Version "+AV110Versa );
         context.msgStatus( "  Running mode: ["+AV109Debug+"] - Started at "+GXutil.time( ) );
         if ( ( GXutil.strcmp(GXutil.trim( AV114FileS), "") != 0 ) )
         {
            context.msgStatus( "  Reading Amex feedback file "+GXutil.trim( AV114FileS) );
            /* Execute user subroutine: S1137 */
            S1137 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            if ( ( AV119Conta > 0 ) )
            {
               context.msgStatus( Gx_msg );
               if ( ( GXutil.strcmp(AV140Erro_, "S") == 0 ) )
               {
                  /* Execute user subroutine: S13179 */
                  S13179 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     cleanup();
                     if (true) return;
                  }
               }
            }
            else
            {
               /* Execute user subroutine: S1298 */
               S1298 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               context.msgStatus( "Arquivo processado com sucesso" );
            }
         }
         else
         {
            context.msgStatus( "Caminho do arquivo n�o encontrado" );
         }
      }
      else
      {
         context.msgStatus( "Arquivo n�o processado. A configura��o no sistema est� \"NoBatch\"" );
      }
      cleanup();
   }

   public void S1137( )
   {
      /* 'VALIDARARQUIVO' Routine */
      AV113Error = context.getSessionInstances().getDelimitedFiles().dfropen( AV114FileS, 1024, ",", "\"", "") ;
      AV119Conta = 0 ;
      AV133OpenB = 0 ;
      AV134OpenB = 0 ;
      AV138Linha = 0 ;
      AV140Erro_ = "N" ;
      AV139Linha = "" ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char3[0] = AV115linha ;
         GXt_int4 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char3, (short)(150)) ;
         AV115linha = GXv_char3[0] ;
         AV113Error = GXt_int4 ;
         AV132RecTy = GXutil.substring( AV115linha, 1, 1) ;
         AV138Linha = (int)(AV138Linha+1) ;
         AV139Linha = AV115linha ;
         if ( ( AV138Linha == 1 ) && ( ( GXutil.strcmp(GXutil.substring( AV115linha, 1, 1), "H") != 0 ) || ( GXutil.strcmp(GXutil.substring( AV115linha, 118, 2), "AX") != 0 ) ) )
         {
            Gx_msg = Gx_msg + "File Header invalid" ;
            context.msgStatus( Gx_msg );
            AV119Conta = (long)(AV119Conta+1) ;
            AV140Erro_ = "S" ;
            if (true) break;
         }
         if ( ( GXutil.strcmp(AV132RecTy, "H") == 0 ) )
         {
            if ( ( AV133OpenB > 0 ) )
            {
               Gx_msg = Gx_msg + "File Header duplicated" ;
               context.msgStatus( Gx_msg );
               AV119Conta = (long)(AV119Conta+1) ;
            }
            AV133OpenB = (double)(AV133OpenB+1) ;
         }
         else if ( ( GXutil.strcmp(AV132RecTy, "T") == 0 ) )
         {
            if ( ( AV133OpenB < 0 ) )
            {
               Gx_msg = Gx_msg + "File Trailer duplicated" ;
               AV119Conta = (long)(AV119Conta+1) ;
            }
            AV133OpenB = (double)(AV133OpenB-1) ;
         }
      }
      if ( ( AV138Linha > 1 ) && ( GXutil.strcmp(GXutil.substring( AV139Linha, 1, 1), "T") != 0 ) )
      {
         Gx_msg = Gx_msg + "File Trailer invalid" ;
         context.msgStatus( Gx_msg );
         AV119Conta = (long)(AV119Conta+1) ;
         AV140Erro_ = "S" ;
      }
      if ( ( AV133OpenB > 0 ) )
      {
         Gx_msg = Gx_msg + "File Trailer missing" ;
         AV119Conta = (long)(AV119Conta+1) ;
      }
      if ( ( AV133OpenB < 0 ) )
      {
         Gx_msg = Gx_msg + "File Header missing" ;
         AV119Conta = (long)(AV119Conta+1) ;
      }
      AV113Error = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
   }

   public void S1298( )
   {
      /* 'PROCESSAARQUIVO' Routine */
      AV113Error = context.getSessionInstances().getDelimitedFiles().dfropen( AV114FileS, 1024, ",", "\"", "") ;
      GXt_char2 = AV127Short ;
      GXv_char3[0] = AV114FileS ;
      GXv_char5[0] = GXt_char2 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char3, GXv_char5) ;
      pretrsubaxv2.this.AV114FileS = GXv_char3[0] ;
      pretrsubaxv2.this.GXt_char2 = GXv_char5[0] ;
      AV127Short = GXt_char2 ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char5[0] = AV115linha ;
         GXt_int4 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char5, (short)(150)) ;
         AV115linha = GXv_char5[0] ;
         AV113Error = GXt_int4 ;
         if ( ( GXutil.strcmp(GXutil.substring( AV115linha, 1, 1), "H") == 0 ) )
         {
            AV121Error = GXutil.substring( AV115linha, 149, 2) ;
            AV130Refer = "AX" + GXutil.trim( GXutil.substring( AV115linha, 19, 6)) + "%" ;
         }
         else if ( ( GXutil.strcmp(GXutil.substring( AV115linha, 1, 1), "D") == 0 ) && ( ( GXutil.strcmp(AV121Error, "00") == 0 ) || ( GXutil.strcmp(AV121Error, "") == 0 ) ) )
         {
            AV131Refer = GXutil.trim( GXutil.substring( AV115linha, 118, 30)) ;
            AV122Error = GXutil.substring( AV115linha, 149, 2) ;
            if ( ( GXutil.strcmp(AV122Error, "") == 0 ) )
            {
               AV122Error = "00" ;
            }
            if ( ( AV111TestM == 0 ) )
            {
               if ( ( GXutil.strcmp(AV121Error, "00") == 0 ) || ( GXutil.strcmp(AV121Error, "") == 0 ) )
               {
                  /* Using cursor P006V2 */
                  pr_default.execute(0, new Object[] {AV131Refer});
                  while ( (pr_default.getStatus(0) != 101) )
                  {
                     A1193lccbS = P006V2_A1193lccbS[0] ;
                     n1193lccbS = P006V2_n1193lccbS[0] ;
                     A1184lccbS = P006V2_A1184lccbS[0] ;
                     n1184lccbS = P006V2_n1184lccbS[0] ;
                     A1224lccbC = P006V2_A1224lccbC[0] ;
                     A1227lccbO = P006V2_A1227lccbO[0] ;
                     A1196lccbR = P006V2_A1196lccbR[0] ;
                     n1196lccbR = P006V2_n1196lccbR[0] ;
                     A1197lccbR = P006V2_A1197lccbR[0] ;
                     n1197lccbR = P006V2_n1197lccbR[0] ;
                     A1201lccbR = P006V2_A1201lccbR[0] ;
                     n1201lccbR = P006V2_n1201lccbR[0] ;
                     A1205lccbR = P006V2_A1205lccbR[0] ;
                     n1205lccbR = P006V2_n1205lccbR[0] ;
                     A1199lccbR = P006V2_A1199lccbR[0] ;
                     n1199lccbR = P006V2_n1199lccbR[0] ;
                     A1198lccbR = P006V2_A1198lccbR[0] ;
                     n1198lccbR = P006V2_n1198lccbR[0] ;
                     A1204lccbR = P006V2_A1204lccbR[0] ;
                     n1204lccbR = P006V2_n1204lccbR[0] ;
                     A1203lccbR = P006V2_A1203lccbR[0] ;
                     n1203lccbR = P006V2_n1203lccbR[0] ;
                     A1185lccbB = P006V2_A1185lccbB[0] ;
                     n1185lccbB = P006V2_n1185lccbB[0] ;
                     A1150lccbE = P006V2_A1150lccbE[0] ;
                     A1222lccbI = P006V2_A1222lccbI[0] ;
                     A1223lccbD = P006V2_A1223lccbD[0] ;
                     A1225lccbC = P006V2_A1225lccbC[0] ;
                     A1226lccbA = P006V2_A1226lccbA[0] ;
                     A1228lccbF = P006V2_A1228lccbF[0] ;
                     if ( ( GXutil.strcmp(A1193lccbS, GXutil.trim( AV131Refer)) == 0 ) )
                     {
                        if ( ( GXutil.strcmp(AV122Error, "00") == 0 ) )
                        {
                           A1184lccbS = "RETOK" ;
                           n1184lccbS = false ;
                        }
                        else
                        {
                           A1184lccbS = "RETNOK" ;
                           n1184lccbS = false ;
                        }
                        A1196lccbR = GXutil.resetTime(GXutil.now(true, false)) ;
                        n1196lccbR = false ;
                        A1197lccbR = GXutil.now(true, false) ;
                        n1197lccbR = false ;
                        A1201lccbR = (int)(GXutil.val( AV122Error, ".")) ;
                        n1201lccbR = false ;
                        A1205lccbR = GXutil.substring( AV115linha, 70, 2) ;
                        n1205lccbR = false ;
                        A1199lccbR = GXutil.substring( AV127Short, 1, 20) ;
                        n1199lccbR = false ;
                        A1198lccbR = "F" ;
                        n1198lccbR = false ;
                        A1204lccbR = GXutil.substring( AV115linha, 13, 6) ;
                        n1204lccbR = false ;
                        A1203lccbR = (long)(GXutil.val( GXutil.trim( GXutil.substring( AV115linha, 19, 7)), ".")) ;
                        n1203lccbR = false ;
                        /* Using cursor P006V3 */
                        pr_default.execute(1, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1196lccbR), A1196lccbR, new Boolean(n1197lccbR), A1197lccbR, new Boolean(n1201lccbR), new Integer(A1201lccbR), new Boolean(n1205lccbR), A1205lccbR, new Boolean(n1199lccbR), A1199lccbR, new Boolean(n1198lccbR), A1198lccbR, new Boolean(n1204lccbR), A1204lccbR, new Boolean(n1203lccbR), new Long(A1203lccbR), A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                     }
                     pr_default.readNext(0);
                  }
                  pr_default.close(0);
               }
            }
         }
         /*
            INSERT RECORD ON TABLE LCCBPLP1

         */
         A1229lccbS = GXutil.now(true, false) ;
         A1186lccbS = "AX" ;
         n1186lccbS = false ;
         A1187lccbS = "Amex - Retorno realizado" ;
         n1187lccbS = false ;
         /* Using cursor P006V4 */
         pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
         if ( (pr_default.getStatus(2) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
      }
      AV113Error = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
   }

   public void S13179( )
   {
      /* 'ENVIAREMAIL' Routine */
      GXt_char2 = AV147To ;
      GXv_char5[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_TO_ICSI", "S", "", "", GXv_char5) ;
      pretrsubaxv2.this.GXt_char2 = GXv_char5[0] ;
      AV147To = GXt_char2 ;
      GXt_char2 = AV144CC ;
      GXv_char5[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_CC_ICSI", "S", "", "", GXv_char5) ;
      pretrsubaxv2.this.GXt_char2 = GXv_char5[0] ;
      AV144CC = GXt_char2 ;
      AV142BCC = "" ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV141Anexo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GXt_char2 = AV145Subje ;
      GXv_char5[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_RET_INVALIDO", "S", "TIESS ICSI - RETORNO INVALIDO", "", GXv_char5) ;
      pretrsubaxv2.this.GXt_char2 = GXv_char5[0] ;
      AV145Subje = GXt_char2 ;
      GXt_char2 = AV143Body ;
      GXv_char5[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_RET_BODY", "S", "<br>The return file [FILE] related to [ADM] is uncompleted.", "", GXv_char5) ;
      pretrsubaxv2.this.GXt_char2 = GXv_char5[0] ;
      AV143Body = GXt_char2 ;
      AV143Body = GXutil.strReplace( AV143Body, "[FILE]", AV146FileN) ;
      AV143Body = GXutil.strReplace( AV143Body, "[ADM]", "AX") ;
      context.msgStatus( "The return file "+GXutil.trim( AV146FileN)+" related to AX is uncompleted " );
      new penviaemail(remoteHandle, context).execute( AV145Subje, AV143Body, AV147To, AV144CC, AV142BCC, AV141Anexo) ;
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(pretrsubaxv2.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = pretrsubaxv2.this.AV109Debug;
      this.aP1[0] = pretrsubaxv2.this.AV114FileS;
      Application.commit(context, remoteHandle, "DEFAULT", "pretrsubaxv2");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV110Versa = "" ;
      AV111TestM = (byte)(0) ;
      GXt_char1 = "" ;
      returnInSub = false ;
      AV119Conta = 0 ;
      Gx_msg = "" ;
      AV140Erro_ = "" ;
      AV113Error = 0 ;
      AV133OpenB = 0 ;
      AV134OpenB = 0 ;
      AV138Linha = 0 ;
      AV139Linha = "" ;
      AV115linha = "" ;
      AV132RecTy = "" ;
      AV127Short = "" ;
      GXv_char3 = new String [1] ;
      GXt_int4 = (short)(0) ;
      AV121Error = "" ;
      AV130Refer = "" ;
      AV131Refer = "" ;
      AV122Error = "" ;
      scmdbuf = "" ;
      P006V2_A1193lccbS = new String[] {""} ;
      P006V2_n1193lccbS = new boolean[] {false} ;
      P006V2_A1184lccbS = new String[] {""} ;
      P006V2_n1184lccbS = new boolean[] {false} ;
      P006V2_A1224lccbC = new String[] {""} ;
      P006V2_A1227lccbO = new String[] {""} ;
      P006V2_A1196lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006V2_n1196lccbR = new boolean[] {false} ;
      P006V2_A1197lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006V2_n1197lccbR = new boolean[] {false} ;
      P006V2_A1201lccbR = new int[1] ;
      P006V2_n1201lccbR = new boolean[] {false} ;
      P006V2_A1205lccbR = new String[] {""} ;
      P006V2_n1205lccbR = new boolean[] {false} ;
      P006V2_A1199lccbR = new String[] {""} ;
      P006V2_n1199lccbR = new boolean[] {false} ;
      P006V2_A1198lccbR = new String[] {""} ;
      P006V2_n1198lccbR = new boolean[] {false} ;
      P006V2_A1204lccbR = new String[] {""} ;
      P006V2_n1204lccbR = new boolean[] {false} ;
      P006V2_A1203lccbR = new long[1] ;
      P006V2_n1203lccbR = new boolean[] {false} ;
      P006V2_A1185lccbB = new String[] {""} ;
      P006V2_n1185lccbB = new boolean[] {false} ;
      P006V2_A1150lccbE = new String[] {""} ;
      P006V2_A1222lccbI = new String[] {""} ;
      P006V2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006V2_A1225lccbC = new String[] {""} ;
      P006V2_A1226lccbA = new String[] {""} ;
      P006V2_A1228lccbF = new String[] {""} ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1224lccbC = "" ;
      A1227lccbO = "" ;
      A1196lccbR = GXutil.nullDate() ;
      n1196lccbR = false ;
      A1197lccbR = GXutil.resetTime( GXutil.nullDate() );
      n1197lccbR = false ;
      A1201lccbR = 0 ;
      n1201lccbR = false ;
      A1205lccbR = "" ;
      n1205lccbR = false ;
      A1199lccbR = "" ;
      n1199lccbR = false ;
      A1198lccbR = "" ;
      n1198lccbR = false ;
      A1204lccbR = "" ;
      n1204lccbR = false ;
      A1203lccbR = 0 ;
      n1203lccbR = false ;
      A1185lccbB = "" ;
      n1185lccbB = false ;
      A1150lccbE = "" ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1228lccbF = "" ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV147To = "" ;
      AV144CC = "" ;
      AV142BCC = "" ;
      GX_I = 0 ;
      AV141Anexo = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV141Anexo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV145Subje = "" ;
      AV143Body = "" ;
      GXv_char5 = new String [1] ;
      AV146FileN = "" ;
      GXt_char2 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretrsubaxv2__default(),
         new Object[] {
             new Object[] {
            P006V2_A1193lccbS, P006V2_n1193lccbS, P006V2_A1184lccbS, P006V2_n1184lccbS, P006V2_A1224lccbC, P006V2_A1227lccbO, P006V2_A1196lccbR, P006V2_n1196lccbR, P006V2_A1197lccbR, P006V2_n1197lccbR,
            P006V2_A1201lccbR, P006V2_n1201lccbR, P006V2_A1205lccbR, P006V2_n1205lccbR, P006V2_A1199lccbR, P006V2_n1199lccbR, P006V2_A1198lccbR, P006V2_n1198lccbR, P006V2_A1204lccbR, P006V2_n1204lccbR,
            P006V2_A1203lccbR, P006V2_n1203lccbR, P006V2_A1185lccbB, P006V2_n1185lccbB, P006V2_A1150lccbE, P006V2_A1222lccbI, P006V2_A1223lccbD, P006V2_A1225lccbC, P006V2_A1226lccbA, P006V2_A1228lccbF
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV111TestM ;
   private short GXt_int4 ;
   private short A1230lccbS ;
   private short Gx_err ;
   private int AV113Error ;
   private int AV138Linha ;
   private int A1201lccbR ;
   private int GX_INS236 ;
   private int GX_I ;
   private long AV119Conta ;
   private long A1203lccbR ;
   private double AV133OpenB ;
   private double AV134OpenB ;
   private String AV109Debug ;
   private String AV114FileS ;
   private String AV110Versa ;
   private String GXt_char1 ;
   private String Gx_msg ;
   private String AV140Erro_ ;
   private String AV139Linha ;
   private String AV132RecTy ;
   private String AV127Short ;
   private String GXv_char3[] ;
   private String AV121Error ;
   private String AV130Refer ;
   private String AV131Refer ;
   private String AV122Error ;
   private String scmdbuf ;
   private String A1193lccbS ;
   private String A1184lccbS ;
   private String A1224lccbC ;
   private String A1227lccbO ;
   private String A1205lccbR ;
   private String A1199lccbR ;
   private String A1198lccbR ;
   private String A1204lccbR ;
   private String A1185lccbB ;
   private String A1150lccbE ;
   private String A1222lccbI ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1228lccbF ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String GXv_char5[] ;
   private String GXt_char2 ;
   private java.util.Date A1197lccbR ;
   private java.util.Date A1229lccbS ;
   private java.util.Date A1196lccbR ;
   private java.util.Date A1223lccbD ;
   private boolean returnInSub ;
   private boolean n1193lccbS ;
   private boolean n1184lccbS ;
   private boolean n1196lccbR ;
   private boolean n1197lccbR ;
   private boolean n1201lccbR ;
   private boolean n1205lccbR ;
   private boolean n1199lccbR ;
   private boolean n1198lccbR ;
   private boolean n1204lccbR ;
   private boolean n1203lccbR ;
   private boolean n1185lccbB ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private String AV115linha ;
   private String A1187lccbS ;
   private String AV147To ;
   private String AV144CC ;
   private String AV142BCC ;
   private String AV141Anexo[] ;
   private String AV145Subje ;
   private String AV143Body ;
   private String AV146FileN ;
   private String[] aP0 ;
   private String[] aP1 ;
   private IDataStoreProvider pr_default ;
   private String[] P006V2_A1193lccbS ;
   private boolean[] P006V2_n1193lccbS ;
   private String[] P006V2_A1184lccbS ;
   private boolean[] P006V2_n1184lccbS ;
   private String[] P006V2_A1224lccbC ;
   private String[] P006V2_A1227lccbO ;
   private java.util.Date[] P006V2_A1196lccbR ;
   private boolean[] P006V2_n1196lccbR ;
   private java.util.Date[] P006V2_A1197lccbR ;
   private boolean[] P006V2_n1197lccbR ;
   private int[] P006V2_A1201lccbR ;
   private boolean[] P006V2_n1201lccbR ;
   private String[] P006V2_A1205lccbR ;
   private boolean[] P006V2_n1205lccbR ;
   private String[] P006V2_A1199lccbR ;
   private boolean[] P006V2_n1199lccbR ;
   private String[] P006V2_A1198lccbR ;
   private boolean[] P006V2_n1198lccbR ;
   private String[] P006V2_A1204lccbR ;
   private boolean[] P006V2_n1204lccbR ;
   private long[] P006V2_A1203lccbR ;
   private boolean[] P006V2_n1203lccbR ;
   private String[] P006V2_A1185lccbB ;
   private boolean[] P006V2_n1185lccbB ;
   private String[] P006V2_A1150lccbE ;
   private String[] P006V2_A1222lccbI ;
   private java.util.Date[] P006V2_A1223lccbD ;
   private String[] P006V2_A1225lccbC ;
   private String[] P006V2_A1226lccbA ;
   private String[] P006V2_A1228lccbF ;
}

final  class pretrsubaxv2__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006V2", "SELECT [lccbSubTrn], [lccbStatus], [lccbCCard], [lccbOpCode], [lccbRSubDate], [lccbRSubTime], [lccbRSubError], [lccbRSubAppCode], [lccbRSubFile], [lccbRSubType], [lccbRSubRO], [lccbRSubCCCF], [lccbBatchNum], [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCNum], [lccbAppCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbOpCode] = 'S' AND [lccbCCard] = 'AX' AND [lccbStatus] = 'PROCPLP') AND (([lccbOpCode] = 'S' and [lccbCCard] = 'AX' and [lccbStatus] = 'PROCPLP') AND ([lccbSubTrn] = RTRIM(LTRIM(?)))) ORDER BY [lccbOpCode], [lccbCCard], [lccbStatus], [lccbBatchNum], [lccbSubTrn] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006V3", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbRSubDate]=?, [lccbRSubTime]=?, [lccbRSubError]=?, [lccbRSubAppCode]=?, [lccbRSubFile]=?, [lccbRSubType]=?, [lccbRSubRO]=?, [lccbRSubCCCF]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006V4", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 2) ;
               ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(5) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDateTime(6) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((int[]) buf[10])[0] = rslt.getInt(7) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(8, 10) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(9, 20) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(11, 10) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((long[]) buf[20])[0] = rslt.getLong(12) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((String[]) buf[22])[0] = rslt.getString(13, 20) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(14, 3) ;
               ((String[]) buf[25])[0] = rslt.getString(15, 7) ;
               ((java.util.Date[]) buf[26])[0] = rslt.getGXDate(16) ;
               ((String[]) buf[27])[0] = rslt.getString(17, 44) ;
               ((String[]) buf[28])[0] = rslt.getString(18, 20) ;
               ((String[]) buf[29])[0] = rslt.getString(19, 19) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 30);
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(2, (java.util.Date)parms[3]);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(3, (java.util.Date)parms[5], false);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(4, ((Number) parms[7]).intValue());
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 10);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 1);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 10);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(9, ((Number) parms[17]).longValue());
               }
               stmt.setString(10, (String)parms[18], 3);
               stmt.setString(11, (String)parms[19], 7);
               stmt.setDate(12, (java.util.Date)parms[20]);
               stmt.setString(13, (String)parms[21], 2);
               stmt.setString(14, (String)parms[22], 44);
               stmt.setString(15, (String)parms[23], 20);
               stmt.setString(16, (String)parms[24], 1);
               stmt.setString(17, (String)parms[25], 19);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
      }
   }

}


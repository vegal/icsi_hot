/*
               File: SubmissaoRedecard
        Description: Submissao Redecard
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:24.79
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class psubmissaoredecard extends GXProcedure
{
   public psubmissaoredecard( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psubmissaoredecard.class ), "" );
   }

   public psubmissaoredecard( int remoteHandle ,
                              ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      psubmissaoredecard.this.AV8DebugMo = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV54Versao = "00033" ;
      AV8DebugMo = GXutil.trim( GXutil.upper( AV8DebugMo)) ;
      GXt_char1 = AV208UsaNo ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "USA_NOVA_BAND_HC", "Par�metro que indica se no arquivo de submiss�o Redecard a nova bandeira HC deve ser utilizada", "S", "Y", GXv_svchar2) ;
      psubmissaoredecard.this.GXt_char1 = GXv_svchar2[0] ;
      AV208UsaNo = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV209Bande ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER", "C�digo do cart�o Hipercard", "S", "HC", GXv_svchar2) ;
      psubmissaoredecard.this.GXt_char1 = GXv_svchar2[0] ;
      AV209Bande = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV210NovaB ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER_NEW", "C�digo do cart�o Hipercard para arquivos output", "S", "HP", GXv_svchar2) ;
      psubmissaoredecard.this.GXt_char1 = GXv_svchar2[0] ;
      AV210NovaB = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV211Bande ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO", "C�digo do cart�o ELO", "S", "EL", GXv_svchar2) ;
      psubmissaoredecard.this.GXt_char1 = GXv_svchar2[0] ;
      AV211Bande = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV212NovaB ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO_NEW", "C�digo do cart�o ELO para arquivos output", "S", "EL", GXv_svchar2) ;
      psubmissaoredecard.this.GXt_char1 = GXv_svchar2[0] ;
      AV212NovaB = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV192Aux ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "CA_Total_Trans", "Total de transa��es por Lote - Rede Card", "S", "200", GXv_svchar2) ;
      psubmissaoredecard.this.GXt_char1 = GXv_svchar2[0] ;
      AV192Aux = GXt_char1 ;
      AV191Total = (int)(GXutil.val( AV192Aux, ".")) ;
      context.msgStatus( "Redecard - Submission - Version "+AV54Versao );
      context.msgStatus( "  Running mode: ["+AV8DebugMo+"] - Started at "+GXutil.time( ) );
      context.msgStatus( "  Creating Submission file" );
      if ( ( GXutil.strSearch( AV8DebugMo, "SEPPIPE", 1) > 0 ) )
      {
         AV9Sep = "|" ;
      }
      else
      {
         AV9Sep = "" ;
      }
      if ( ( GXutil.strSearch( AV168RetSu, "TESTMODE", 1) > 0 ) )
      {
         AV148TestM = (byte)(1) ;
      }
      else
      {
         AV148TestM = (byte)(0) ;
      }
      /* Using cursor P00752 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         brk752 = false ;
         A1150lccbE = P00752_A1150lccbE[0] ;
         A1184lccbS = P00752_A1184lccbS[0] ;
         n1184lccbS = P00752_n1184lccbS[0] ;
         A1227lccbO = P00752_A1227lccbO[0] ;
         A1490Distr = P00752_A1490Distr[0] ;
         n1490Distr = P00752_n1490Distr[0] ;
         A1224lccbC = P00752_A1224lccbC[0] ;
         A1147lccbE = P00752_A1147lccbE[0] ;
         n1147lccbE = P00752_n1147lccbE[0] ;
         A1222lccbI = P00752_A1222lccbI[0] ;
         A1223lccbD = P00752_A1223lccbD[0] ;
         A1225lccbC = P00752_A1225lccbC[0] ;
         A1226lccbA = P00752_A1226lccbA[0] ;
         A1228lccbF = P00752_A1228lccbF[0] ;
         A1147lccbE = P00752_A1147lccbE[0] ;
         n1147lccbE = P00752_n1147lccbE[0] ;
         AV195lccbC = A1224lccbC ;
         /* Execute user subroutine: S1147 */
         S1147 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            pr_default.close(0);
            returnInSub = true;
            cleanup();
            if (true) return;
         }
         while ( (pr_default.getStatus(0) != 101) && ( GXutil.strcmp(P00752_A1227lccbO[0], A1227lccbO) == 0 ) && ( GXutil.strcmp(P00752_A1184lccbS[0], A1184lccbS) == 0 ) && ( GXutil.strcmp(P00752_A1224lccbC[0], A1224lccbC) == 0 ) && ( GXutil.strcmp(P00752_A1490Distr[0], A1490Distr) == 0 ) )
         {
            brk752 = false ;
            A1150lccbE = P00752_A1150lccbE[0] ;
            A1222lccbI = P00752_A1222lccbI[0] ;
            A1223lccbD = P00752_A1223lccbD[0] ;
            A1225lccbC = P00752_A1225lccbC[0] ;
            A1226lccbA = P00752_A1226lccbA[0] ;
            A1228lccbF = P00752_A1228lccbF[0] ;
            brk752 = true ;
            pr_default.readNext(0);
         }
         if ( ! brk752 )
         {
            brk752 = true ;
            pr_default.readNext(0);
         }
      }
      pr_default.close(0);
      cleanup();
   }

   public void S1147( )
   {
      /* 'MAIN' Routine */
      context.msgStatus( "  Creating file "+AV12FileNa );
      AV172CriaF = "S" ;
      AV176NovoL = "N" ;
      AV177SeqRe = (short)(0) ;
      AV138trnTy = (byte)(1) ;
      while ( ( AV138trnTy <= 2 ) )
      {
         AV131OldEm = "" ;
         AV171CriaA = "S" ;
         AV177SeqRe = (short)(0) ;
         AV183Fecha = "N" ;
         AV160QtdPa = 0 ;
         AV10SeqReg = 0 ;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              new Byte(AV138trnTy) ,
                                              new Short(A1168lccbI) ,
                                              A1490Distr ,
                                              A1147lccbE ,
                                              AV195lccbC ,
                                              A1227lccbO ,
                                              A1184lccbS ,
                                              A1224lccbC },
                                              new int[] {
                                              TypeConstants.BYTE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         /* Using cursor P00753 */
         pr_default.execute(1, new Object[] {AV195lccbC});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1228lccbF = P00753_A1228lccbF[0] ;
            A1227lccbO = P00753_A1227lccbO[0] ;
            A1226lccbA = P00753_A1226lccbA[0] ;
            A1225lccbC = P00753_A1225lccbC[0] ;
            A1224lccbC = P00753_A1224lccbC[0] ;
            A1223lccbD = P00753_A1223lccbD[0] ;
            A1222lccbI = P00753_A1222lccbI[0] ;
            A1150lccbE = P00753_A1150lccbE[0] ;
            A1168lccbI = P00753_A1168lccbI[0] ;
            n1168lccbI = P00753_n1168lccbI[0] ;
            A1147lccbE = P00753_A1147lccbE[0] ;
            n1147lccbE = P00753_n1147lccbE[0] ;
            A1490Distr = P00753_A1490Distr[0] ;
            n1490Distr = P00753_n1490Distr[0] ;
            A1184lccbS = P00753_A1184lccbS[0] ;
            n1184lccbS = P00753_n1184lccbS[0] ;
            A1167lccbG = P00753_A1167lccbG[0] ;
            n1167lccbG = P00753_n1167lccbG[0] ;
            A1170lccbI = P00753_A1170lccbI[0] ;
            n1170lccbI = P00753_n1170lccbI[0] ;
            A1169lccbD = P00753_A1169lccbD[0] ;
            n1169lccbD = P00753_n1169lccbD[0] ;
            A1172lccbS = P00753_A1172lccbS[0] ;
            n1172lccbS = P00753_n1172lccbS[0] ;
            A1179lccbV = P00753_A1179lccbV[0] ;
            n1179lccbV = P00753_n1179lccbV[0] ;
            A1171lccbT = P00753_A1171lccbT[0] ;
            n1171lccbT = P00753_n1171lccbT[0] ;
            A1194lccbS = P00753_A1194lccbS[0] ;
            n1194lccbS = P00753_n1194lccbS[0] ;
            A1195lccbS = P00753_A1195lccbS[0] ;
            n1195lccbS = P00753_n1195lccbS[0] ;
            A1189lccbS = P00753_A1189lccbS[0] ;
            n1189lccbS = P00753_n1189lccbS[0] ;
            A1192lccbS = P00753_A1192lccbS[0] ;
            n1192lccbS = P00753_n1192lccbS[0] ;
            A1185lccbB = P00753_A1185lccbB[0] ;
            n1185lccbB = P00753_n1185lccbB[0] ;
            A1190lccbS = P00753_A1190lccbS[0] ;
            n1190lccbS = P00753_n1190lccbS[0] ;
            A1191lccbS = P00753_A1191lccbS[0] ;
            n1191lccbS = P00753_n1191lccbS[0] ;
            A1193lccbS = P00753_A1193lccbS[0] ;
            n1193lccbS = P00753_n1193lccbS[0] ;
            A1147lccbE = P00753_A1147lccbE[0] ;
            n1147lccbE = P00753_n1147lccbE[0] ;
            if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "R") == 0 ) )
            {
               if ( ( GXutil.strcmp(A1147lccbE, "1") == 0 ) )
               {
                  AV181Erro = "N" ;
                  if ( ( AV138trnTy == 2 ) && ( A1170lccbI <= 0.00 ) )
                  {
                     AV181Erro = "S" ;
                     AV182Error = "Redecard - install amount error" ;
                  }
                  if ( ( AV138trnTy == 2 ) && ( A1169lccbD < 0.00 ) )
                  {
                     AV181Erro = "S" ;
                     AV182Error = "Redecard - downpayment error" ;
                  }
                  if ( ( GXutil.strcmp(AV181Erro, "S") == 0 ) )
                  {
                     if ( ( AV148TestM == 0 ) )
                     {
                        A1184lccbS = "NOSUB" ;
                        n1184lccbS = false ;
                        /*
                           INSERT RECORD ON TABLE LCCBPLP1

                        */
                        A1229lccbS = GXutil.now(true, false) ;
                        A1186lccbS = "NOSUB" ;
                        n1186lccbS = false ;
                        A1187lccbS = AV182Error ;
                        n1187lccbS = false ;
                        /* Using cursor P00754 */
                        pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                        if ( (pr_default.getStatus(2) == 1) )
                        {
                           Gx_err = (short)(1) ;
                           Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                        }
                        else
                        {
                           Gx_err = (short)(0) ;
                           Gx_emsg = "" ;
                        }
                        /* End Insert */
                     }
                  }
                  else
                  {
                     if ( ( GXutil.strcmp(AV171CriaA, "S") == 0 ) )
                     {
                        /* Execute user subroutine: S124 */
                        S124 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        AV171CriaA = "N" ;
                        AV176NovoL = "S" ;
                     }
                     if ( ( GXutil.strcmp(AV131OldEm, A1150lccbE) != 0 ) )
                     {
                        if ( ( GXutil.strcmp(AV131OldEm, "") != 0 ) )
                        {
                           /* Execute user subroutine: S134 */
                           S134 ();
                           if ( returnInSub )
                           {
                              pr_default.close(1);
                              pr_default.close(1);
                              returnInSub = true;
                              if (true) return;
                           }
                           AV17SeqLot = (short)(0) ;
                           AV177SeqRe = (short)(0) ;
                           AV160QtdPa = 0 ;
                           AV176NovoL = "S" ;
                        }
                        AV131OldEm = A1150lccbE ;
                        context.msgStatus( "  Creating REDECARD Submission records for "+A1150lccbE );
                        AV220GXLvl = (byte)(0) ;
                        /* Using cursor P00755 */
                        pr_default.execute(3, new Object[] {AV131OldEm});
                        while ( (pr_default.getStatus(3) != 101) )
                        {
                           A1488ICSI_ = P00755_A1488ICSI_[0] ;
                           A1487ICSI_ = P00755_A1487ICSI_[0] ;
                           A1480ICSI_ = P00755_A1480ICSI_[0] ;
                           n1480ICSI_ = P00755_n1480ICSI_[0] ;
                           A1481ICSI_ = P00755_A1481ICSI_[0] ;
                           n1481ICSI_ = P00755_n1481ICSI_[0] ;
                           AV220GXLvl = (byte)(1) ;
                           AV134ICSI_ = GXutil.trim( A1480ICSI_) ;
                           AV149ICSI_ = GXutil.trim( A1481ICSI_) ;
                           /* Exiting from a For First loop. */
                           if (true) break;
                        }
                        pr_default.close(3);
                        if ( ( AV220GXLvl == 0 ) )
                        {
                           AV134ICSI_ = "" ;
                           AV149ICSI_ = "" ;
                           context.msgStatus( "    ERROR: POS number undefined" );
                        }
                     }
                     if ( ( AV177SeqRe > AV191Total ) )
                     {
                        /* Execute user subroutine: S134 */
                        S134 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        /* Execute user subroutine: S144 */
                        S144 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        /* Execute user subroutine: S154 */
                        S154 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        /* Execute user subroutine: S124 */
                        S124 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        AV160QtdPa = 0 ;
                        AV177SeqRe = (short)(0) ;
                        AV176NovoL = "S" ;
                     }
                     if ( ( GXutil.strcmp(AV176NovoL, "S") == 0 ) )
                     {
                        /* Execute user subroutine: S164 */
                        S164 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        AV17SeqLot = (short)(0) ;
                        AV177SeqRe = (short)(0) ;
                        AV160QtdPa = 0 ;
                        AV176NovoL = "N" ;
                     }
                     AV160QtdPa = (int)(AV160QtdPa+A1168lccbI) ;
                     AV177SeqRe = (short)(AV177SeqRe+1) ;
                     AV43lccbEm = A1150lccbE ;
                     AV45lccbIA = A1222lccbI ;
                     AV39lccbDa = A1223lccbD ;
                     if ( ( GXutil.strcmp(AV208UsaNo, "Y") == 0 ) )
                     {
                        if ( ( GXutil.strcmp(A1224lccbC, AV209Bande) == 0 ) )
                        {
                           AV13lccbCC = AV210NovaB ;
                        }
                        else if ( ( GXutil.strcmp(A1224lccbC, AV211Bande) == 0 ) )
                        {
                           AV13lccbCC = AV212NovaB ;
                        }
                        else
                        {
                           AV13lccbCC = A1224lccbC ;
                        }
                     }
                     else
                     {
                        AV13lccbCC = A1224lccbC ;
                     }
                     AV196Atrib = A1225lccbC ;
                     GXv_svchar2[0] = AV198Resul ;
                     new pcrypto(remoteHandle, context).execute( AV196Atrib, "D", GXv_svchar2) ;
                     psubmissaoredecard.this.AV198Resul = GXv_svchar2[0] ;
                     AV22lccbCC = AV198Resul ;
                     AV38lccbAp = GXutil.left( A1226lccbA, 6) ;
                     AV46lccbOp = A1227lccbO ;
                     AV44lccbFP = A1228lccbF ;
                     AV141lccbI = A1168lccbI ;
                     AV190Valor = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(A1172lccbS), 2).multiply(DecimalUtil.doubleToDec(100)))) ;
                     AV194lccbV = A1179lccbV ;
                     AV61lccbIn = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(A1170lccbI), 2).multiply(DecimalUtil.doubleToDec(100)))) ;
                     AV40lccbDo = 0 ;
                     AV27lccbTi = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(A1171lccbT), 2).multiply(DecimalUtil.doubleToDec(100)))) ;
                     AV24lccbSa = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(A1172lccbS), 2).multiply(DecimalUtil.doubleToDec(100)).subtract(DecimalUtil.doubleToDec(AV27lccbTi)))) ;
                     GX_I = 1 ;
                     while ( ( GX_I <= 4 ) )
                     {
                        AV34lccbTD[GX_I-1] = "" ;
                        GX_I = (int)(GX_I+1) ;
                     }
                     GX_I = 1 ;
                     while ( ( GX_I <= 4 ) )
                     {
                        AV33lccbPa[GX_I-1] = "" ;
                        GX_I = (int)(GX_I+1) ;
                     }
                     AV98i = (byte)(1) ;
                     /* Using cursor P00756 */
                     pr_default.execute(4, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A1231lccbT = P00756_A1231lccbT[0] ;
                        A1207lccbP = P00756_A1207lccbP[0] ;
                        n1207lccbP = P00756_n1207lccbP[0] ;
                        A1232lccbT = P00756_A1232lccbT[0] ;
                        AV34lccbTD[AV98i-1] = A1231lccbT ;
                        GXt_char3 = AV140s ;
                        GXv_svchar2[0] = A1207lccbP ;
                        GXv_char4[0] = " " ;
                        GXv_int5[0] = (short)(40) ;
                        GXv_char6[0] = "R" ;
                        GXv_char7[0] = GXt_char3 ;
                        new pr2strset(remoteHandle, context).execute( GXv_svchar2, GXv_char4, GXv_int5, GXv_char6, GXv_char7) ;
                        psubmissaoredecard.this.A1207lccbP = GXv_svchar2[0] ;
                        psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
                        AV140s = GXt_char3 ;
                        AV33lccbPa[AV98i-1] = AV140s ;
                        if ( ( AV98i == 1 ) )
                        {
                           AV67lccbTR = A1232lccbT ;
                        }
                        AV98i = (byte)(AV98i+1) ;
                        if ( ( AV98i > 4 ) )
                        {
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                        }
                        pr_default.readNext(4);
                     }
                     pr_default.close(4);
                     /* Execute user subroutine: S174 */
                     S174 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        pr_default.close(1);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Execute user subroutine: S184 */
                     S184 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        pr_default.close(1);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Execute user subroutine: S194 */
                     S194 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        pr_default.close(1);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Execute user subroutine: S204 */
                     S204 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        pr_default.close(1);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Execute user subroutine: S214 */
                     S214 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        pr_default.close(1);
                        returnInSub = true;
                        if (true) return;
                     }
                     if ( ( AV148TestM == 0 ) )
                     {
                        A1184lccbS = "PROCPLP" ;
                        n1184lccbS = false ;
                        A1194lccbS = GXutil.trim( GXutil.str( AV58ICSI_C, 10, 0)) ;
                        n1194lccbS = false ;
                        if ( ( AV138trnTy == 1 ) )
                        {
                           GXt_char3 = A1195lccbS ;
                           GXv_char7[0] = AV134ICSI_ ;
                           GXv_char6[0] = "0" ;
                           GXv_int5[0] = (short)(9) ;
                           GXv_char4[0] = "L" ;
                           GXv_svchar2[0] = GXt_char3 ;
                           new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
                           psubmissaoredecard.this.AV134ICSI_ = GXv_char7[0] ;
                           psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
                           A1195lccbS = GXt_char3 ;
                           n1195lccbS = false ;
                        }
                        else
                        {
                           GXt_char3 = A1195lccbS ;
                           GXv_char7[0] = AV149ICSI_ ;
                           GXv_char6[0] = "0" ;
                           GXv_int5[0] = (short)(9) ;
                           GXv_char4[0] = "L" ;
                           GXv_svchar2[0] = GXt_char3 ;
                           new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
                           psubmissaoredecard.this.AV149ICSI_ = GXv_char7[0] ;
                           psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
                           A1195lccbS = GXt_char3 ;
                           n1195lccbS = false ;
                        }
                        /*
                           INSERT RECORD ON TABLE LCCBPLP1

                        */
                        A1229lccbS = GXutil.now(true, false) ;
                        A1186lccbS = "PROCPLP" ;
                        n1186lccbS = false ;
                        A1187lccbS = "Redecard - Submiss�o realizada" ;
                        n1187lccbS = false ;
                        /* Using cursor P00757 */
                        pr_default.execute(5, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                        if ( (pr_default.getStatus(5) == 1) )
                        {
                           Gx_err = (short)(1) ;
                           Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                        }
                        else
                        {
                           Gx_err = (short)(0) ;
                           Gx_emsg = "" ;
                        }
                        /* End Insert */
                     }
                     A1189lccbS = Gx_date ;
                     n1189lccbS = false ;
                     A1192lccbS = GXutil.substring( AV150Short, 1, 20) ;
                     n1192lccbS = false ;
                     A1185lccbB = GXutil.trim( GXutil.str( AV58ICSI_C, 8, 0)) ;
                     n1185lccbB = false ;
                     A1190lccbS = GXutil.now(true, false) ;
                     n1190lccbS = false ;
                     A1191lccbS = "F" ;
                     n1191lccbS = false ;
                     A1193lccbS = AV139Refer ;
                     n1193lccbS = false ;
                  }
                  /* Using cursor P00758 */
                  pr_default.execute(6, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1194lccbS), A1194lccbS, new Boolean(n1195lccbS), A1195lccbS, new Boolean(n1189lccbS), A1189lccbS, new Boolean(n1192lccbS), A1192lccbS, new Boolean(n1185lccbB), A1185lccbB, new Boolean(n1190lccbS), A1190lccbS, new Boolean(n1191lccbS), A1191lccbS, new Boolean(n1193lccbS), A1193lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               }
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( ( GXutil.strcmp(AV131OldEm, "") != 0 ) )
         {
            if ( ( AV51TotalV > 0 ) )
            {
               /* Execute user subroutine: S134 */
               S134 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            AV160QtdPa = 0 ;
            AV177SeqRe = (short)(0) ;
         }
         if ( ( AV95GTotal > 0 ) )
         {
            /* Execute user subroutine: S144 */
            S144 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV183Fecha = "S" ;
         }
         if ( ( GXutil.strcmp(AV183Fecha, "S") == 0 ) )
         {
            /* Execute user subroutine: S154 */
            S154 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         AV173SCEAi = AV131OldEm ;
         AV12FileNa = AV150Short ;
         if ( ( AV138trnTy == 1 ) )
         {
            AV174SCETe = "Total de Vendas A Vista|" + GXutil.trim( GXutil.str( AV153tSale, 10, 0)) ;
            AV174SCETe = AV174SCETe + "|Total de Taxas A Vista|" + GXutil.trim( GXutil.str( AV155tTipC, 10, 0)) ;
            /* Execute user subroutine: S221 */
            S221 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV174SCETe = "Total de Vendas A Prazo|" + GXutil.trim( GXutil.str( AV154tSale, 10, 0)) ;
            AV174SCETe = AV174SCETe + "|Total de Taxas A Prazo|" + GXutil.trim( GXutil.str( AV157tTipP, 10, 0)) ;
            AV174SCETe = AV174SCETe + "|Total de Entradas A Prazo|" + GXutil.trim( GXutil.str( AV158tDown, 10, 0)) ;
            /* Execute user subroutine: S221 */
            S221 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         AV138trnTy = (byte)(AV138trnTy+1) ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      GXt_char3 = AV213Proce ;
      GXv_char7[0] = GXt_char3 ;
      new pr2getparm(remoteHandle, context).execute( "PROCESSA_SCHECK", "Verifica se processa o sanity check", "S", "N", GXv_char7) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV213Proce = GXt_char3 ;
      if ( ( GXutil.strcmp(AV213Proce, "Y") == 0 ) )
      {
         context.msgStatus( "Sanity Check " );
         GXv_char7[0] = AV214msgEr ;
         new psanitycheckrc(remoteHandle, context).execute( AV12FileNa, GXv_char7) ;
         psubmissaoredecard.this.AV214msgEr = GXv_char7[0] ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( AV214msgEr))==0)) )
         {
            /* Execute user subroutine: S231 */
            S231 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            context.msgStatus( AV214msgEr );
         }
      }
      AV173SCEAi = AV131OldEm ;
      AV12FileNa = AV150Short ;
      AV174SCETe = "Numero de linhas do arquivo|" + GXutil.trim( GXutil.str( AV170NumRe, 10, 0)) ;
      /* Execute user subroutine: S221 */
      S221 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      context.msgStatus( "                 Records      Tarif. Amt      Airpt. Tax     Downpayment" );
      context.msgStatus( "    Total CC  = "+GXutil.str( AV151cntCC, 8, 0)+"  "+localUtil.format( AV153tSale, "ZZ,ZZZ,ZZZ,ZZ9.99")+"  "+localUtil.format( AV155tTipC, "ZZ,ZZZ,ZZZ,ZZ9.99") );
      context.msgStatus( "    Total PLP = "+GXutil.str( AV152cntPL, 8, 0)+"  "+localUtil.format( AV154tSale, "ZZ,ZZZ,ZZZ,ZZ9.99")+"  "+localUtil.format( AV157tTipP, "ZZ,ZZZ,ZZZ,ZZ9.99")+"  "+localUtil.format( AV158tDown, "ZZ,ZZZ,ZZZ,ZZ9.99") );
   }

   public void S24306( )
   {
      /* 'INCREG' Routine */
      AV10SeqReg = (int)(AV10SeqReg+1) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV10SeqReg ;
      GXv_int9[0] = (byte)(5) ;
      GXv_int10[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int9, GXv_int10, GXv_char7) ;
      psubmissaoredecard.this.AV10SeqReg = (int)((int)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV170NumRe = (int)(AV170NumRe+1) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(5)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
   }

   public void S194( )
   {
      /* 'CREATEREFERENCE' Routine */
      AV180Num03 = AV177SeqRe ;
      GXt_char3 = AV139Refer ;
      GXv_int8[0] = AV56ICSI_C ;
      GXv_int10[0] = (byte)(5) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV56ICSI_C = (int)((int)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV139Refer = AV43lccbEm + AV13lccbCC + GXt_char3 + localUtil.format( AV180Num03, "999") ;
   }

   public void S124( )
   {
      /* 'RECORD00' Routine */
      AV176NovoL = "N" ;
      AV10SeqReg = 0 ;
      AV17SeqLot = (short)(0) ;
      AV177SeqRe = (short)(0) ;
      AV48TotalE = 0 ;
      AV81TotalP = 0 ;
      AV48TotalE = 0 ;
      AV51TotalV = 0 ;
      AV86GTotal = 0 ;
      AV89GTotal = 0 ;
      AV92GTotal = 0 ;
      AV95GTotal = 0 ;
      AV223GXLvl = (byte)(0) ;
      /* Using cursor P00759 */
      pr_default.execute(7);
      while ( (pr_default.getStatus(7) != 101) )
      {
         A1488ICSI_ = P00759_A1488ICSI_[0] ;
         A1487ICSI_ = P00759_A1487ICSI_[0] ;
         A1484ICSI_ = P00759_A1484ICSI_[0] ;
         n1484ICSI_ = P00759_n1484ICSI_[0] ;
         A1485ICSI_ = P00759_A1485ICSI_[0] ;
         n1485ICSI_ = P00759_n1485ICSI_[0] ;
         A1479ICSI_ = P00759_A1479ICSI_[0] ;
         n1479ICSI_ = P00759_n1479ICSI_[0] ;
         A1478ICSI_ = P00759_A1478ICSI_[0] ;
         n1478ICSI_ = P00759_n1478ICSI_[0] ;
         AV223GXLvl = (byte)(1) ;
         AV132ICSI_ = GXutil.trim( A1484ICSI_) ;
         AV133ICSI_ = GXutil.trim( A1485ICSI_) ;
         if ( ( AV148TestM == 0 ) )
         {
            A1479ICSI_ = (int)(A1479ICSI_+1) ;
            n1479ICSI_ = false ;
         }
         AV56ICSI_C = A1478ICSI_ ;
         AV58ICSI_C = A1479ICSI_ ;
         context.msgStatus( "  Info: File sequence="+GXutil.trim( GXutil.str( AV58ICSI_C, 10, 0))+", Batch="+GXutil.trim( GXutil.str( AV56ICSI_C, 10, 0)) );
         /* Using cursor P007510 */
         pr_default.execute(8, new Object[] {new Boolean(n1479ICSI_), new Integer(A1479ICSI_), A1487ICSI_, A1488ICSI_});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(7);
      if ( ( AV223GXLvl == 0 ) )
      {
         AV132ICSI_ = "0000000590" ;
         AV133ICSI_ = "R2-IATA" ;
         AV58ICSI_C = 1 ;
         AV56ICSI_C = 1 ;
         /*
            INSERT RECORD ON TABLE ICSI_CCINFO

         */
         A1487ICSI_ = "000" ;
         A1488ICSI_ = "RC" ;
         A1484ICSI_ = AV132ICSI_ ;
         n1484ICSI_ = false ;
         A1485ICSI_ = AV133ICSI_ ;
         n1485ICSI_ = false ;
         A1479ICSI_ = AV58ICSI_C ;
         n1479ICSI_ = false ;
         A1478ICSI_ = AV56ICSI_C ;
         n1478ICSI_ = false ;
         /* Using cursor P007511 */
         pr_default.execute(9, new Object[] {A1487ICSI_, A1488ICSI_, new Boolean(n1478ICSI_), new Integer(A1478ICSI_), new Boolean(n1479ICSI_), new Integer(A1479ICSI_), new Boolean(n1484ICSI_), A1484ICSI_, new Boolean(n1485ICSI_), A1485ICSI_});
         if ( (pr_default.getStatus(9) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         context.msgStatus( "  ERROR: REDECARD not configured" );
      }
      if ( ( GXutil.strcmp(AV172CriaF, "S") == 0 ) )
      {
         /* Execute user subroutine: S251 */
         S251 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV172CriaF = "N" ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00", (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "0000000000000", (short)(13)) ;
      AV178SeqAr = (short)(AV58ICSI_C) ;
      AV179RefAr = localUtil.format( AV178SeqAr, "9999") + AV21DataC + AV20HoraB ;
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV132ICSI_ ;
      GXv_char6[0] = "0" ;
      GXv_int5[0] = (short)(10) ;
      GXv_char4[0] = "L" ;
      GXv_svchar2[0] = GXt_char3 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV132ICSI_ = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(10)) ;
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV133ICSI_ ;
      GXv_char6[0] = " " ;
      GXv_int5[0] = (short)(30) ;
      GXv_char4[0] = "R" ;
      GXv_svchar2[0] = GXt_char3 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV133ICSI_ = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(30)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(5)), (short)(5)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV58ICSI_C ;
      GXv_int10[0] = (byte)(4) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV58ICSI_C = (int)((int)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(4)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV21DataC, (short)(6)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV20HoraB, (short)(6)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( " ", (short)(1)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "3", (short)(1)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(167)), (short)(167)) ;
      /* Execute user subroutine: S24306 */
      S24306 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S204( )
   {
      /* 'RECORD01' Routine */
      AV17SeqLot = (short)(AV17SeqLot+1) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "01", (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV139Refer, (short)(13)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "101", (short)(3)) ;
         AV151cntCC = (int)(AV151cntCC+1) ;
         AV153tSale = (double)(AV153tSale+AV24lccbSa) ;
         AV155tTipC = (double)(AV155tTipC+AV27lccbTi) ;
      }
      else
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "103", (short)(3)) ;
         AV152cntPL = (int)(AV152cntPL+1) ;
         AV154tSale = (double)(AV154tSale+AV24lccbSa) ;
         AV157tTipP = (double)(AV157tTipP+AV27lccbTi) ;
         AV158tDown = (double)(AV158tDown+AV40lccbDo) ;
         AV159tInst = (double)(AV159tInst+AV61lccbIn) ;
      }
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV22lccbCC ;
      GXv_char6[0] = GXt_char3 ;
      new pr2onlynumbers(remoteHandle, context).execute( GXv_char7, GXv_char6) ;
      psubmissaoredecard.this.AV22lccbCC = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char6[0] ;
      AV140s = "000000" + GXt_char3 ;
      AV140s = GXutil.right( AV140s, 17) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(17)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         GXt_char3 = AV140s ;
         GXv_char7[0] = AV134ICSI_ ;
         GXv_char6[0] = "0" ;
         GXv_int5[0] = (short)(9) ;
         GXv_char4[0] = "L" ;
         GXv_svchar2[0] = GXt_char3 ;
         new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
         psubmissaoredecard.this.AV134ICSI_ = GXv_char7[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
         AV140s = GXt_char3 ;
      }
      else
      {
         GXt_char3 = AV140s ;
         GXv_char7[0] = AV149ICSI_ ;
         GXv_char6[0] = "0" ;
         GXv_int5[0] = (short)(9) ;
         GXv_char4[0] = "L" ;
         GXv_svchar2[0] = GXt_char3 ;
         new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
         psubmissaoredecard.this.AV149ICSI_ = GXv_char7[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
         AV140s = GXt_char3 ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(9)) ;
      AV140s = GXutil.trim( AV38lccbAp) ;
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV140s ;
      GXv_char6[0] = "0" ;
      GXv_int5[0] = (short)(6) ;
      GXv_char4[0] = "L" ;
      GXv_svchar2[0] = GXt_char3 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV140s = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(6)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00000000", (short)(8)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV24lccbSa ;
      GXv_int10[0] = (byte)(15) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV24lccbSa = GXv_int8[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      if ( ( AV138trnTy == 2 ) )
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV40lccbDo ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV40lccbDo = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV27lccbTi ;
      GXv_int10[0] = (byte)(15) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV27lccbTi = GXv_int8[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      if ( ( AV138trnTy == 2 ) )
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV61lccbIn ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV61lccbIn = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
         AV64v = (double)(AV40lccbDo+AV27lccbTi+AV61lccbIn) ;
         AV64v = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV64v), 2))) ;
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV64v ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV64v = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      else
      {
         AV64v = (double)(AV24lccbSa+AV27lccbTi) ;
         AV64v = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV64v), 2))) ;
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV64v ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV64v = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      AV35YYMMDD = GXutil.trim( GXutil.str( GXutil.year( AV39lccbDa), 10, 0)) ;
      AV35YYMMDD = AV35YYMMDD + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( A1223lccbD)+100, 10, 0)), 2, 3) ;
      AV35YYMMDD = AV35YYMMDD + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( A1223lccbD)+100, 10, 0)), 2, 3) ;
      AV35YYMMDD = GXutil.substring( AV35YYMMDD, 3, 6) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV35YYMMDD, (short)(6)) ;
      if ( ( AV141lccbI < 1 ) )
      {
         AV141lccbI = (short)(1) ;
      }
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV141lccbI ;
      GXv_int10[0] = (byte)(2) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV141lccbI = (short)((short)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(2)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV177SeqRe ;
      GXv_int10[0] = (byte)(3) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV177SeqRe = (short)((short)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(3)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV56ICSI_C ;
      GXv_int10[0] = (byte)(5) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV56ICSI_C = (int)((int)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(5)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(109)), (short)(109)) ;
      }
      else
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(79)), (short)(79)) ;
      }
      AV140s = GXutil.trim( AV194lccbV) ;
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV140s ;
      GXv_char6[0] = "0" ;
      GXv_int5[0] = (short)(4) ;
      GXv_char4[0] = "L" ;
      GXv_svchar2[0] = GXt_char3 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV140s = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(4)) ;
      AV140s = "S" ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(1)) ;
      AV140s = GXutil.space( (short)(12)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(12)) ;
      AV51TotalV = (double)(AV51TotalV+AV24lccbSa) ;
      AV80TotalT = (double)(AV80TotalT+AV27lccbTi) ;
      if ( ( AV138trnTy == 2 ) )
      {
         AV48TotalE = (double)(AV48TotalE+AV40lccbDo) ;
         AV81TotalP = (double)(AV81TotalP+AV61lccbIn) ;
      }
      /* Execute user subroutine: S24306 */
      S24306 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S214( )
   {
      /* 'RECORD02' Routine */
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "02", (short)(2)) ;
      AV98i = (byte)(2) ;
      while ( ( AV98i <= 4 ) )
      {
         GXt_char3 = AV140s ;
         GXv_char7[0] = AV33lccbPa[AV98i-1] ;
         GXv_char6[0] = " " ;
         GXv_int5[0] = (short)(25) ;
         GXv_char4[0] = "R" ;
         GXv_svchar2[0] = GXt_char3 ;
         new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
         psubmissaoredecard.this.AV33lccbPa[AV98i-1] = GXv_char7[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV140s, 1, 25), (short)(25)) ;
         AV98i = (byte)(AV98i+1) ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "A", (short)(1)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(10)), (short)(10)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV35YYMMDD, (short)(6)) ;
      AV140s = GXutil.trim( AV43lccbEm) + GXutil.trim( GXutil.str( AV141lccbI, 10, 0)) ;
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV140s ;
      GXv_char6[0] = " " ;
      GXv_int5[0] = (short)(6) ;
      GXv_char4[0] = "R" ;
      GXv_svchar2[0] = GXt_char3 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV140s = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(6)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV143I_Fro[1-1], 1, 3), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV147Carri[1-1], 1, 4), (short)(4)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV145Claxs[1-1], 1, 2), (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV146Tarif[1-1], 1, 12), (short)(12)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV143I_Fro[2-1], 1, 3), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(3)), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV147Carri[2-1], 1, 4), (short)(4)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV145Claxs[2-1], 1, 2), (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV143I_Fro[3-1], 1, 3), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV147Carri[3-1], 1, 4), (short)(4)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV145Claxs[3-1], 1, 2), (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV143I_Fro[4-1], 1, 3), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV147Carri[4-1], 1, 4), (short)(4)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV145Claxs[4-1], 1, 2), (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV143I_Fro[5-1], 1, 3), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV144I_To[1-1], 1, 3), (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV13lccbCC, (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV43lccbEm, (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV21DataC, 1, 6), (short)(6)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "001", (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV111Fli_D[1-1], 1, 5), (short)(5)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV45lccbIA, 5, 3), (short)(3)) ;
      GXt_char3 = AV140s ;
      GXv_char7[0] = AV124CadNo ;
      GXv_char6[0] = " " ;
      GXv_int5[0] = (short)(30) ;
      GXv_char4[0] = "R" ;
      GXv_svchar2[0] = GXt_char3 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV124CadNo = GXv_char7[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV140s, 1, 30), (short)(30)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(36)), (short)(36)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV10SeqReg ;
      GXv_int10[0] = (byte)(5) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV10SeqReg = (int)((int)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.substring( AV140s, 1, 5), (short)(5)) ;
      AV170NumRe = (int)(AV170NumRe+1) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
   }

   public void S134( )
   {
      /* 'RECORD30' Routine */
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "30", (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "9999999999999", (short)(13)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "101", (short)(3)) ;
      }
      else
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "103", (short)(3)) ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(13)), (short)(13)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         GXt_char3 = AV140s ;
         GXv_char7[0] = AV134ICSI_ ;
         GXv_char6[0] = "0" ;
         GXv_int5[0] = (short)(9) ;
         GXv_char4[0] = "L" ;
         GXv_svchar2[0] = GXt_char3 ;
         new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
         psubmissaoredecard.this.AV134ICSI_ = GXv_char7[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
         AV140s = GXt_char3 ;
      }
      else
      {
         GXt_char3 = AV140s ;
         GXv_char7[0] = AV149ICSI_ ;
         GXv_char6[0] = "0" ;
         GXv_int5[0] = (short)(9) ;
         GXv_char4[0] = "L" ;
         GXv_svchar2[0] = GXt_char3 ;
         new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
         psubmissaoredecard.this.AV149ICSI_ = GXv_char7[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
         AV140s = GXt_char3 ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(9)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV21DataC, (short)(6)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV51TotalV ;
      GXv_int10[0] = (byte)(15) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV51TotalV = GXv_int8[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      if ( ( AV138trnTy == 2 ) )
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV48TotalE ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV48TotalE = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV80TotalT ;
      GXv_int10[0] = (byte)(15) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV80TotalT = GXv_int8[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = 0 ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
         AV64v = (double)(AV51TotalV+AV80TotalT) ;
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV64v ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV64v = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
         AV184Num05 = AV17SeqLot ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( localUtil.format( AV184Num05, "99999"), (short)(5)) ;
      }
      else
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV81TotalP ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV81TotalP = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( localUtil.format( AV160QtdPa, "99999"), (short)(5)) ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000", (short)(3)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV56ICSI_C ;
      GXv_int10[0] = (byte)(5) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV56ICSI_C = (int)((int)(GXv_int8[0])) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(5)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(126)), (short)(126)) ;
      AV95GTotal = (double)(AV95GTotal+AV51TotalV) ;
      AV86GTotal = (double)(AV86GTotal+AV48TotalE) ;
      AV89GTotal = (double)(AV89GTotal+AV81TotalP) ;
      AV92GTotal = (double)(AV92GTotal+AV80TotalT) ;
      AV51TotalV = 0 ;
      AV48TotalE = 0 ;
      AV81TotalP = 0 ;
      AV80TotalT = 0 ;
      /* Execute user subroutine: S24306 */
      S24306 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV17SeqLot = (short)(0) ;
      AV160QtdPa = 0 ;
      AV176NovoL = "S" ;
   }

   public void S144( )
   {
      /* 'RECORD40' Routine */
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "40", (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "9999999999999", (short)(13)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "101", (short)(3)) ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(14)), (short)(14)) ;
      }
      else
      {
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(17)), (short)(17)) ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(9)), (short)(9)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(6)), (short)(6)) ;
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV95GTotal ;
      GXv_int10[0] = (byte)(15) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV95GTotal = GXv_int8[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      if ( ( AV138trnTy == 2 ) )
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV86GTotal ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV86GTotal = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      GXt_char3 = AV140s ;
      GXv_int8[0] = AV92GTotal ;
      GXv_int10[0] = (byte)(15) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char3 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      psubmissaoredecard.this.AV92GTotal = GXv_int8[0] ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV140s = GXt_char3 ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      if ( ( AV138trnTy == 1 ) )
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = 0 ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
         AV64v = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV95GTotal), 2).add(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV92GTotal), 2)))) ;
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV64v ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV64v = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      else
      {
         GXt_char3 = AV140s ;
         GXv_int8[0] = AV89GTotal ;
         GXv_int10[0] = (byte)(15) ;
         GXv_int9[0] = (byte)(0) ;
         GXv_char7[0] = GXt_char3 ;
         new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
         psubmissaoredecard.this.AV89GTotal = GXv_int8[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
         AV140s = GXt_char3 ;
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV140s, (short)(15)) ;
      }
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(138)), (short)(138)) ;
      /* Execute user subroutine: S24306 */
      S24306 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S154( )
   {
      /* 'RECORD99' Routine */
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "99", (short)(2)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "9999999999999", (short)(13)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(96)), (short)(96)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000", (short)(3)) ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(131)), (short)(131)) ;
      /* Execute user subroutine: S24306 */
      S24306 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV86GTotal = 0 ;
      AV89GTotal = 0 ;
      AV92GTotal = 0 ;
      AV95GTotal = 0 ;
   }

   public void S174( )
   {
      /* 'DADOS HOT' Routine */
      AV142NUM_B = (long)(GXutil.val( AV34lccbTD[1-1], ".")) ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV143I_Fro[GX_I-1] = GXutil.space( (short)(3)) ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV144I_To[GX_I-1] = GXutil.space( (short)(3)) ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV145Claxs[GX_I-1] = GXutil.space( (short)(2)) ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV146Tarif[GX_I-1] = GXutil.space( (short)(12)) ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV147Carri[GX_I-1] = GXutil.space( (short)(4)) ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV111Fli_D[GX_I-1] = GXutil.space( (short)(5)) ;
         GX_I = (int)(GX_I+1) ;
      }
      AV224GXLvl = (byte)(0) ;
      /* Using cursor P007512 */
      pr_default.execute(10, new Object[] {new Long(AV142NUM_B), AV67lccbTR});
      while ( (pr_default.getStatus(10) != 101) )
      {
         A970DATA = P007512_A970DATA[0] ;
         A969TIPO_V = P007512_A969TIPO_V[0] ;
         A968NUM_BI = P007512_A968NUM_BI[0] ;
         A967IATA = P007512_A967IATA[0] ;
         A966CODE = P007512_A966CODE[0] ;
         A965PER_NA = P007512_A965PER_NA[0] ;
         A964CiaCod = P007512_A964CiaCod[0] ;
         A963ISOC = P007512_A963ISOC[0] ;
         A902NUM_BI = P007512_A902NUM_BI[0] ;
         n902NUM_BI = P007512_n902NUM_BI[0] ;
         A869ID_AGE = P007512_A869ID_AGE[0] ;
         n869ID_AGE = P007512_n869ID_AGE[0] ;
         AV224GXLvl = (byte)(1) ;
         AV125PER_N = A965PER_NA ;
         AV126IATA = A967IATA ;
         AV123DATA = A970DATA ;
         AV115Ver = (byte)(0) ;
         /* Using cursor P007513 */
         pr_default.execute(11, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A923I_From = P007513_A923I_From[0] ;
            n923I_From = P007513_n923I_From[0] ;
            A924I_To = P007513_A924I_To[0] ;
            n924I_To = P007513_n924I_To[0] ;
            A928Claxss = P007513_A928Claxss[0] ;
            n928Claxss = P007513_n928Claxss[0] ;
            A926Tariff = P007513_A926Tariff[0] ;
            n926Tariff = P007513_n926Tariff[0] ;
            A925Carrie = P007513_A925Carrie[0] ;
            n925Carrie = P007513_n925Carrie[0] ;
            A929Fli_Da = P007513_A929Fli_Da[0] ;
            n929Fli_Da = P007513_n929Fli_Da[0] ;
            A971ISeq = P007513_A971ISeq[0] ;
            AV115Ver = (byte)(AV115Ver+1) ;
            GXt_char3 = AV140s ;
            GXv_char7[0] = A923I_From ;
            GXv_char6[0] = " " ;
            GXv_int5[0] = (short)(3) ;
            GXv_char4[0] = "R" ;
            GXv_svchar2[0] = GXt_char3 ;
            new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
            psubmissaoredecard.this.A923I_From = GXv_char7[0] ;
            psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
            AV140s = GXt_char3 ;
            AV143I_Fro[AV115Ver-1] = AV140s ;
            GXt_char3 = AV140s ;
            GXv_char7[0] = A924I_To ;
            GXv_char6[0] = " " ;
            GXv_int5[0] = (short)(3) ;
            GXv_char4[0] = "R" ;
            GXv_svchar2[0] = GXt_char3 ;
            new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
            psubmissaoredecard.this.A924I_To = GXv_char7[0] ;
            psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
            AV140s = GXt_char3 ;
            AV144I_To[AV115Ver-1] = AV140s ;
            GXt_char3 = AV140s ;
            GXv_char7[0] = A928Claxss ;
            GXv_char6[0] = " " ;
            GXv_int5[0] = (short)(2) ;
            GXv_char4[0] = "R" ;
            GXv_svchar2[0] = GXt_char3 ;
            new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
            psubmissaoredecard.this.A928Claxss = GXv_char7[0] ;
            psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
            AV140s = GXt_char3 ;
            AV145Claxs[AV115Ver-1] = AV140s ;
            GXt_char3 = AV140s ;
            GXv_char7[0] = A926Tariff ;
            GXv_char6[0] = " " ;
            GXv_int5[0] = (short)(12) ;
            GXv_char4[0] = "R" ;
            GXv_svchar2[0] = GXt_char3 ;
            new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
            psubmissaoredecard.this.A926Tariff = GXv_char7[0] ;
            psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
            AV140s = GXt_char3 ;
            AV146Tarif[AV115Ver-1] = AV140s ;
            GXt_char3 = AV140s ;
            GXv_char7[0] = A925Carrie ;
            GXv_char6[0] = " " ;
            GXv_int5[0] = (short)(4) ;
            GXv_char4[0] = "R" ;
            GXv_svchar2[0] = GXt_char3 ;
            new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
            psubmissaoredecard.this.A925Carrie = GXv_char7[0] ;
            psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
            AV140s = GXt_char3 ;
            AV147Carri[AV115Ver-1] = AV140s ;
            GXt_char3 = AV140s ;
            GXv_char7[0] = A929Fli_Da ;
            GXv_char6[0] = " " ;
            GXv_int5[0] = (short)(5) ;
            GXv_char4[0] = "R" ;
            GXv_svchar2[0] = GXt_char3 ;
            new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar2) ;
            psubmissaoredecard.this.A929Fli_Da = GXv_char7[0] ;
            psubmissaoredecard.this.GXt_char3 = GXv_svchar2[0] ;
            AV140s = GXt_char3 ;
            AV111Fli_D[AV115Ver-1] = AV140s ;
            if ( ( AV115Ver == 5 ) )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(11);
         }
         pr_default.close(11);
         pr_default.readNext(10);
      }
      pr_default.close(10);
      if ( ( AV224GXLvl == 0 ) )
      {
         context.msgStatus( "  ERROR: Ticket information not found" );
      }
   }

   public void S184( )
   {
      /* 'DADOS AGENCIA' Routine */
      AV226GXLvl = (byte)(0) ;
      /* Using cursor P007514 */
      pr_default.execute(12, new Object[] {AV45lccbIA});
      while ( (pr_default.getStatus(12) != 101) )
      {
         A1313CadIA = P007514_A1313CadIA[0] ;
         A1314CadNo = P007514_A1314CadNo[0] ;
         n1314CadNo = P007514_n1314CadNo[0] ;
         AV226GXLvl = (byte)(1) ;
         AV124CadNo = A1314CadNo ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(12);
      if ( ( AV226GXLvl == 0 ) )
      {
         AV124CadNo = AV45lccbIA ;
         context.msgStatus( "  ERROR: IATA information not found" );
      }
   }

   public void S251( )
   {
      /* 'NOVOARQ' Routine */
      AV11DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      AV21DataC = GXutil.substring( AV11DataB, 3, 6) ;
      AV19HoraA = GXutil.time( ) ;
      AV20HoraB = GXutil.substring( AV19HoraA, 1, 2) + GXutil.substring( AV19HoraA, 4, 2) + GXutil.substring( AV19HoraA, 7, 2) ;
      AV186dia = (byte)(GXutil.day( Gx_date)) ;
      AV187mes = (byte)(GXutil.month( Gx_date)) ;
      AV188ano = GXutil.year( Gx_date) ;
      AV170NumRe = 0 ;
      if ( ( AV148TestM == 0 ) )
      {
         AV150Short = AV11DataB + "_" + GXutil.trim( GXutil.str( AV58ICSI_C, 10, 0)) + "_RC.REM" ;
      }
      else
      {
         AV150Short = "Prv" + GXutil.trim( AV11DataB) + AV20HoraB + "_i41RC" ;
      }
      AV202Audit = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio gera��o arquivo " + GXutil.trim( AV150Short) ;
      /* Execute user subroutine: S261 */
      S261 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV163ArqD1[1-1] = GXutil.now(true, false) ;
      GXt_char3 = AV12FileNa ;
      GXv_char7[0] = GXt_char3 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_SUB_CA", "Caminho dos arquivos de submiss�o Redecard", "F", "C:\\TEMP\\ICSI\\CA\\", GXv_char7) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV12FileNa = GXt_char3 ;
      AV12FileNa = AV12FileNa + AV150Short ;
      AV207FileN = AV12FileNa ;
      AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV12FileNa, AV9Sep, "", (byte)(0), "") ;
      AV164ArqN[1-1] = AV150Short ;
   }

   public void S221( )
   {
      /* 'LOG' Routine */
      /*
         INSERT RECORD ON TABLE SCEVENTS

      */
      A1301SCEAi = AV173SCEAi ;
      n1301SCEAi = false ;
      A1303SCERE = AV12FileNa ;
      n1303SCERE = false ;
      A1305SCETe = AV174SCETe ;
      n1305SCETe = false ;
      A1311SCEVe = AV54Versao ;
      n1311SCEVe = false ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.now(true, false) ;
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "RC" ;
      n1310SCETp = false ;
      A1306SCEAP = "SUB" ;
      n1306SCEAP = false ;
      /* Using cursor P007515 */
      pr_default.execute(13, new Object[] {new Boolean(n1298SCETk), A1298SCETk, new Boolean(n1299SCEDa), A1299SCEDa, new Boolean(n1300SCECR), A1300SCECR, new Boolean(n1301SCEAi), A1301SCEAi, new Boolean(n1303SCERE), A1303SCERE, new Boolean(n1305SCETe), A1305SCETe, new Boolean(n1306SCEAP), A1306SCEAP, new Boolean(n1307SCEIa), A1307SCEIa, new Boolean(n1310SCETp), A1310SCETp, new Boolean(n1311SCEVe), A1311SCEVe});
      /* Retrieving last key number assigned */
      /* Using cursor P007516 */
      pr_default.execute(14);
      A1312SCEId = P007516_A1312SCEId[0] ;
      n1312SCEId = P007516_n1312SCEId[0] ;
      pr_default.close(14);
      if ( (pr_default.getStatus(13) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      if ( ( GXutil.strSearch( AV8DebugMo, "VERBOSE", 1) > 0 ) )
      {
         context.msgStatus( "    "+A1305SCETe );
      }
   }

   public void S164( )
   {
      /* 'NOVOLOTE' Routine */
      AV17SeqLot = (short)(0) ;
      AV177SeqRe = (short)(0) ;
      AV227GXLvl = (byte)(0) ;
      /* Using cursor P007517 */
      pr_default.execute(15);
      while ( (pr_default.getStatus(15) != 101) )
      {
         A1488ICSI_ = P007517_A1488ICSI_[0] ;
         A1487ICSI_ = P007517_A1487ICSI_[0] ;
         A1478ICSI_ = P007517_A1478ICSI_[0] ;
         n1478ICSI_ = P007517_n1478ICSI_[0] ;
         AV227GXLvl = (byte)(1) ;
         if ( ( AV148TestM == 0 ) && ( GXutil.strcmp(AV176NovoL, "S") == 0 ) )
         {
            A1478ICSI_ = (int)(A1478ICSI_+1) ;
            n1478ICSI_ = false ;
         }
         AV56ICSI_C = A1478ICSI_ ;
         context.msgStatus( "  Info: New batch/RO="+GXutil.trim( GXutil.str( AV56ICSI_C, 10, 0)) );
         /* Using cursor P007518 */
         pr_default.execute(16, new Object[] {new Boolean(n1478ICSI_), new Integer(A1478ICSI_), A1487ICSI_, A1488ICSI_});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(15);
      if ( ( AV227GXLvl == 0 ) )
      {
         context.msgStatus( "  ERROR: REDECARD configuration missing!" );
      }
   }

   public void S261( )
   {
      /* 'AUDITTRAIL' Routine */
      AV199Audit = "Localhost" ;
      AV200Audit = "TIES_ICSI" ;
      AV201Audit = "PCI" ;
      GXv_char7[0] = AV199Audit ;
      GXv_char6[0] = AV200Audit ;
      GXv_char4[0] = AV201Audit ;
      GXv_svchar2[0] = AV202Audit ;
      new pnewaudit(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_char4, GXv_svchar2) ;
      psubmissaoredecard.this.AV199Audit = GXv_char7[0] ;
      psubmissaoredecard.this.AV200Audit = GXv_char6[0] ;
      psubmissaoredecard.this.AV201Audit = GXv_char4[0] ;
      psubmissaoredecard.this.AV202Audit = GXv_svchar2[0] ;
   }

   public void S231( )
   {
      /* 'COPIA_ARQUIVO_NA_PASTA_DA_ACCESSTAGE' Routine */
      GXt_char3 = AV204PathC ;
      GXv_char7[0] = GXt_char3 ;
      new pr2getparm(remoteHandle, context).execute( "PATHCOPSUBRC", "Caminho C�pia Submiss�o Redecard", "F", "C:\\R2tech\\Accesstage\\Submissa", GXv_char7) ;
      psubmissaoredecard.this.GXt_char3 = GXv_char7[0] ;
      AV204PathC = GXt_char3 ;
      AV203Diret.setSource( GXutil.trim( AV204PathC) );
      if ( AV203Diret.exists() )
      {
         AV205File.setSource( GXutil.trim( AV207FileN) );
         GXt_char3 = AV150Short ;
         GXv_char7[0] = AV207FileN ;
         GXv_char6[0] = GXt_char3 ;
         new pr2shortname(remoteHandle, context).execute( GXv_char7, GXv_char6) ;
         psubmissaoredecard.this.AV207FileN = GXv_char7[0] ;
         psubmissaoredecard.this.GXt_char3 = GXv_char6[0] ;
         AV150Short = GXt_char3 ;
         AV206Filen = GXutil.trim( AV204PathC) + "\\" + GXutil.trim( AV150Short) ;
         AV205File.copy(AV206Filen);
      }
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(psubmissaoredecard.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = psubmissaoredecard.this.AV8DebugMo;
      Application.commit(context, remoteHandle, "DEFAULT", "psubmissaoredecard");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV54Versao = "" ;
      AV208UsaNo = "" ;
      AV209Bande = "" ;
      AV210NovaB = "" ;
      AV211Bande = "" ;
      AV212NovaB = "" ;
      AV192Aux = "" ;
      AV191Total = 0 ;
      AV9Sep = "" ;
      AV168RetSu = "" ;
      AV148TestM = (byte)(0) ;
      scmdbuf = "" ;
      P00752_A1150lccbE = new String[] {""} ;
      P00752_A1184lccbS = new String[] {""} ;
      P00752_n1184lccbS = new boolean[] {false} ;
      P00752_A1227lccbO = new String[] {""} ;
      P00752_A1490Distr = new String[] {""} ;
      P00752_n1490Distr = new boolean[] {false} ;
      P00752_A1224lccbC = new String[] {""} ;
      P00752_A1147lccbE = new String[] {""} ;
      P00752_n1147lccbE = new boolean[] {false} ;
      P00752_A1222lccbI = new String[] {""} ;
      P00752_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00752_A1225lccbC = new String[] {""} ;
      P00752_A1226lccbA = new String[] {""} ;
      P00752_A1228lccbF = new String[] {""} ;
      brk752 = false ;
      A1150lccbE = "" ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1227lccbO = "" ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1224lccbC = "" ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1228lccbF = "" ;
      AV195lccbC = "" ;
      returnInSub = false ;
      AV12FileNa = "" ;
      AV172CriaF = "" ;
      AV176NovoL = "" ;
      AV177SeqRe = (short)(0) ;
      AV138trnTy = (byte)(0) ;
      AV131OldEm = "" ;
      AV171CriaA = "" ;
      AV183Fecha = "" ;
      AV160QtdPa = 0 ;
      AV10SeqReg = 0 ;
      A1168lccbI = (short)(0) ;
      P00753_A1228lccbF = new String[] {""} ;
      P00753_A1227lccbO = new String[] {""} ;
      P00753_A1226lccbA = new String[] {""} ;
      P00753_A1225lccbC = new String[] {""} ;
      P00753_A1224lccbC = new String[] {""} ;
      P00753_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00753_A1222lccbI = new String[] {""} ;
      P00753_A1150lccbE = new String[] {""} ;
      P00753_A1168lccbI = new short[1] ;
      P00753_n1168lccbI = new boolean[] {false} ;
      P00753_A1147lccbE = new String[] {""} ;
      P00753_n1147lccbE = new boolean[] {false} ;
      P00753_A1490Distr = new String[] {""} ;
      P00753_n1490Distr = new boolean[] {false} ;
      P00753_A1184lccbS = new String[] {""} ;
      P00753_n1184lccbS = new boolean[] {false} ;
      P00753_A1167lccbG = new String[] {""} ;
      P00753_n1167lccbG = new boolean[] {false} ;
      P00753_A1170lccbI = new double[1] ;
      P00753_n1170lccbI = new boolean[] {false} ;
      P00753_A1169lccbD = new double[1] ;
      P00753_n1169lccbD = new boolean[] {false} ;
      P00753_A1172lccbS = new double[1] ;
      P00753_n1172lccbS = new boolean[] {false} ;
      P00753_A1179lccbV = new String[] {""} ;
      P00753_n1179lccbV = new boolean[] {false} ;
      P00753_A1171lccbT = new double[1] ;
      P00753_n1171lccbT = new boolean[] {false} ;
      P00753_A1194lccbS = new String[] {""} ;
      P00753_n1194lccbS = new boolean[] {false} ;
      P00753_A1195lccbS = new String[] {""} ;
      P00753_n1195lccbS = new boolean[] {false} ;
      P00753_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P00753_n1189lccbS = new boolean[] {false} ;
      P00753_A1192lccbS = new String[] {""} ;
      P00753_n1192lccbS = new boolean[] {false} ;
      P00753_A1185lccbB = new String[] {""} ;
      P00753_n1185lccbB = new boolean[] {false} ;
      P00753_A1190lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P00753_n1190lccbS = new boolean[] {false} ;
      P00753_A1191lccbS = new String[] {""} ;
      P00753_n1191lccbS = new boolean[] {false} ;
      P00753_A1193lccbS = new String[] {""} ;
      P00753_n1193lccbS = new boolean[] {false} ;
      n1168lccbI = false ;
      A1167lccbG = "" ;
      n1167lccbG = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1169lccbD = 0 ;
      n1169lccbD = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1179lccbV = "" ;
      n1179lccbV = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1195lccbS = "" ;
      n1195lccbS = false ;
      A1189lccbS = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1192lccbS = "" ;
      n1192lccbS = false ;
      A1185lccbB = "" ;
      n1185lccbB = false ;
      A1190lccbS = GXutil.resetTime( GXutil.nullDate() );
      n1190lccbS = false ;
      A1191lccbS = "" ;
      n1191lccbS = false ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      AV181Erro = "" ;
      AV182Error = "" ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV17SeqLot = (short)(0) ;
      AV220GXLvl = (byte)(0) ;
      P00755_A1488ICSI_ = new String[] {""} ;
      P00755_A1487ICSI_ = new String[] {""} ;
      P00755_A1480ICSI_ = new String[] {""} ;
      P00755_n1480ICSI_ = new boolean[] {false} ;
      P00755_A1481ICSI_ = new String[] {""} ;
      P00755_n1481ICSI_ = new boolean[] {false} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      A1480ICSI_ = "" ;
      n1480ICSI_ = false ;
      A1481ICSI_ = "" ;
      n1481ICSI_ = false ;
      AV134ICSI_ = "" ;
      AV149ICSI_ = "" ;
      AV43lccbEm = "" ;
      AV45lccbIA = "" ;
      AV39lccbDa = GXutil.nullDate() ;
      AV13lccbCC = "" ;
      AV196Atrib = "" ;
      AV198Resul = "" ;
      AV22lccbCC = "" ;
      AV38lccbAp = "" ;
      AV46lccbOp = "" ;
      AV44lccbFP = "" ;
      AV141lccbI = (short)(0) ;
      AV190Valor = 0 ;
      AV194lccbV = "" ;
      AV61lccbIn = 0 ;
      AV40lccbDo = 0 ;
      AV27lccbTi = 0 ;
      AV24lccbSa = 0 ;
      GX_I = 0 ;
      AV34lccbTD = new String [4] ;
      GX_I = 1 ;
      while ( ( GX_I <= 4 ) )
      {
         AV34lccbTD[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV33lccbPa = new String [4] ;
      GX_I = 1 ;
      while ( ( GX_I <= 4 ) )
      {
         AV33lccbPa[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV98i = (byte)(0) ;
      P00756_A1150lccbE = new String[] {""} ;
      P00756_A1222lccbI = new String[] {""} ;
      P00756_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00756_A1224lccbC = new String[] {""} ;
      P00756_A1225lccbC = new String[] {""} ;
      P00756_A1226lccbA = new String[] {""} ;
      P00756_A1227lccbO = new String[] {""} ;
      P00756_A1228lccbF = new String[] {""} ;
      P00756_A1231lccbT = new String[] {""} ;
      P00756_A1207lccbP = new String[] {""} ;
      P00756_n1207lccbP = new boolean[] {false} ;
      P00756_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1207lccbP = "" ;
      n1207lccbP = false ;
      A1232lccbT = "" ;
      AV140s = "" ;
      AV67lccbTR = "" ;
      AV58ICSI_C = 0 ;
      Gx_date = GXutil.nullDate() ;
      AV150Short = "" ;
      AV139Refer = "" ;
      AV51TotalV = 0 ;
      AV95GTotal = 0 ;
      AV173SCEAi = "" ;
      AV174SCETe = "" ;
      AV153tSale = 0 ;
      AV155tTipC = 0 ;
      AV154tSale = 0 ;
      AV157tTipP = 0 ;
      AV158tDown = 0 ;
      AV15FileNo = 0 ;
      AV213Proce = "" ;
      AV214msgEr = "" ;
      AV170NumRe = 0 ;
      AV151cntCC = 0 ;
      AV152cntPL = 0 ;
      GXt_char1 = "" ;
      AV180Num03 = (short)(0) ;
      AV56ICSI_C = 0 ;
      AV48TotalE = 0 ;
      AV81TotalP = 0 ;
      AV86GTotal = 0 ;
      AV89GTotal = 0 ;
      AV92GTotal = 0 ;
      AV223GXLvl = (byte)(0) ;
      P00759_A1488ICSI_ = new String[] {""} ;
      P00759_A1487ICSI_ = new String[] {""} ;
      P00759_A1484ICSI_ = new String[] {""} ;
      P00759_n1484ICSI_ = new boolean[] {false} ;
      P00759_A1485ICSI_ = new String[] {""} ;
      P00759_n1485ICSI_ = new boolean[] {false} ;
      P00759_A1479ICSI_ = new int[1] ;
      P00759_n1479ICSI_ = new boolean[] {false} ;
      P00759_A1478ICSI_ = new int[1] ;
      P00759_n1478ICSI_ = new boolean[] {false} ;
      A1484ICSI_ = "" ;
      n1484ICSI_ = false ;
      A1485ICSI_ = "" ;
      n1485ICSI_ = false ;
      A1479ICSI_ = 0 ;
      n1479ICSI_ = false ;
      A1478ICSI_ = 0 ;
      n1478ICSI_ = false ;
      AV132ICSI_ = "" ;
      AV133ICSI_ = "" ;
      GX_INS273 = 0 ;
      AV178SeqAr = (short)(0) ;
      AV179RefAr = "" ;
      AV21DataC = "" ;
      AV20HoraB = "" ;
      AV159tInst = 0 ;
      AV64v = 0 ;
      AV35YYMMDD = "" ;
      AV80TotalT = 0 ;
      AV143I_Fro = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV143I_Fro[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV147Carri = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV147Carri[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV145Claxs = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV145Claxs[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV146Tarif = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV146Tarif[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV144I_To = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV144I_To[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV111Fli_D = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV111Fli_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV124CadNo = "" ;
      AV184Num05 = 0 ;
      GXv_int8 = new double [1] ;
      GXv_int10 = new byte [1] ;
      GXv_int9 = new byte [1] ;
      AV142NUM_B = 0 ;
      AV224GXLvl = (byte)(0) ;
      P007512_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P007512_A969TIPO_V = new String[] {""} ;
      P007512_A968NUM_BI = new String[] {""} ;
      P007512_A967IATA = new String[] {""} ;
      P007512_A966CODE = new String[] {""} ;
      P007512_A965PER_NA = new String[] {""} ;
      P007512_A964CiaCod = new String[] {""} ;
      P007512_A963ISOC = new String[] {""} ;
      P007512_A902NUM_BI = new long[1] ;
      P007512_n902NUM_BI = new boolean[] {false} ;
      P007512_A869ID_AGE = new String[] {""} ;
      P007512_n869ID_AGE = new boolean[] {false} ;
      A970DATA = GXutil.nullDate() ;
      A969TIPO_V = "" ;
      A968NUM_BI = "" ;
      A967IATA = "" ;
      A966CODE = "" ;
      A965PER_NA = "" ;
      A964CiaCod = "" ;
      A963ISOC = "" ;
      A902NUM_BI = 0 ;
      n902NUM_BI = false ;
      A869ID_AGE = "" ;
      n869ID_AGE = false ;
      AV125PER_N = "" ;
      AV126IATA = "" ;
      AV123DATA = GXutil.nullDate() ;
      AV115Ver = (byte)(0) ;
      P007513_A963ISOC = new String[] {""} ;
      P007513_A964CiaCod = new String[] {""} ;
      P007513_A965PER_NA = new String[] {""} ;
      P007513_A966CODE = new String[] {""} ;
      P007513_A967IATA = new String[] {""} ;
      P007513_A968NUM_BI = new String[] {""} ;
      P007513_A969TIPO_V = new String[] {""} ;
      P007513_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P007513_A923I_From = new String[] {""} ;
      P007513_n923I_From = new boolean[] {false} ;
      P007513_A924I_To = new String[] {""} ;
      P007513_n924I_To = new boolean[] {false} ;
      P007513_A928Claxss = new String[] {""} ;
      P007513_n928Claxss = new boolean[] {false} ;
      P007513_A926Tariff = new String[] {""} ;
      P007513_n926Tariff = new boolean[] {false} ;
      P007513_A925Carrie = new String[] {""} ;
      P007513_n925Carrie = new boolean[] {false} ;
      P007513_A929Fli_Da = new String[] {""} ;
      P007513_n929Fli_Da = new boolean[] {false} ;
      P007513_A971ISeq = new int[1] ;
      A923I_From = "" ;
      n923I_From = false ;
      A924I_To = "" ;
      n924I_To = false ;
      A928Claxss = "" ;
      n928Claxss = false ;
      A926Tariff = "" ;
      n926Tariff = false ;
      A925Carrie = "" ;
      n925Carrie = false ;
      A929Fli_Da = "" ;
      n929Fli_Da = false ;
      A971ISeq = 0 ;
      GXv_int5 = new short [1] ;
      AV226GXLvl = (byte)(0) ;
      P007514_A1313CadIA = new String[] {""} ;
      P007514_A1314CadNo = new String[] {""} ;
      P007514_n1314CadNo = new boolean[] {false} ;
      A1313CadIA = "" ;
      A1314CadNo = "" ;
      n1314CadNo = false ;
      AV11DataB = "" ;
      AV19HoraA = "" ;
      AV186dia = (byte)(0) ;
      AV187mes = (byte)(0) ;
      AV188ano = 0 ;
      AV202Audit = "" ;
      AV163ArqD1 = new java.util.Date [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV163ArqD1[GX_I-1] = GXutil.resetTime( GXutil.nullDate() );
         GX_I = (int)(GX_I+1) ;
      }
      AV207FileN = "" ;
      AV164ArqN = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV164ArqN[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      A1305SCETe = "" ;
      GX_INS248 = 0 ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1303SCERE = "" ;
      n1303SCERE = false ;
      n1305SCETe = false ;
      A1311SCEVe = "" ;
      n1311SCEVe = false ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "" ;
      n1310SCETp = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      P007516_A1312SCEId = new long[1] ;
      P007516_n1312SCEId = new boolean[] {false} ;
      A1312SCEId = 0 ;
      n1312SCEId = false ;
      AV227GXLvl = (byte)(0) ;
      P007517_A1488ICSI_ = new String[] {""} ;
      P007517_A1487ICSI_ = new String[] {""} ;
      P007517_A1478ICSI_ = new int[1] ;
      P007517_n1478ICSI_ = new boolean[] {false} ;
      AV199Audit = "" ;
      AV200Audit = "" ;
      AV201Audit = "" ;
      GXv_char4 = new String [1] ;
      GXv_svchar2 = new String [1] ;
      AV204PathC = "" ;
      AV203Diret = new com.genexus.util.GXDirectory();
      AV205File = new com.genexus.util.GXFile();
      GXt_char3 = "" ;
      GXv_char7 = new String [1] ;
      GXv_char6 = new String [1] ;
      AV206Filen = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new psubmissaoredecard__default(),
         new Object[] {
             new Object[] {
            P00752_A1150lccbE, P00752_A1184lccbS, P00752_n1184lccbS, P00752_A1227lccbO, P00752_A1490Distr, P00752_n1490Distr, P00752_A1224lccbC, P00752_A1147lccbE, P00752_n1147lccbE, P00752_A1222lccbI,
            P00752_A1223lccbD, P00752_A1225lccbC, P00752_A1226lccbA, P00752_A1228lccbF
            }
            , new Object[] {
            P00753_A1228lccbF, P00753_A1227lccbO, P00753_A1226lccbA, P00753_A1225lccbC, P00753_A1224lccbC, P00753_A1223lccbD, P00753_A1222lccbI, P00753_A1150lccbE, P00753_A1168lccbI, P00753_n1168lccbI,
            P00753_A1147lccbE, P00753_n1147lccbE, P00753_A1490Distr, P00753_n1490Distr, P00753_A1184lccbS, P00753_n1184lccbS, P00753_A1167lccbG, P00753_n1167lccbG, P00753_A1170lccbI, P00753_n1170lccbI,
            P00753_A1169lccbD, P00753_n1169lccbD, P00753_A1172lccbS, P00753_n1172lccbS, P00753_A1179lccbV, P00753_n1179lccbV, P00753_A1171lccbT, P00753_n1171lccbT, P00753_A1194lccbS, P00753_n1194lccbS,
            P00753_A1195lccbS, P00753_n1195lccbS, P00753_A1189lccbS, P00753_n1189lccbS, P00753_A1192lccbS, P00753_n1192lccbS, P00753_A1185lccbB, P00753_n1185lccbB, P00753_A1190lccbS, P00753_n1190lccbS,
            P00753_A1191lccbS, P00753_n1191lccbS, P00753_A1193lccbS, P00753_n1193lccbS
            }
            , new Object[] {
            }
            , new Object[] {
            P00755_A1488ICSI_, P00755_A1487ICSI_, P00755_A1480ICSI_, P00755_n1480ICSI_, P00755_A1481ICSI_, P00755_n1481ICSI_
            }
            , new Object[] {
            P00756_A1150lccbE, P00756_A1222lccbI, P00756_A1223lccbD, P00756_A1224lccbC, P00756_A1225lccbC, P00756_A1226lccbA, P00756_A1227lccbO, P00756_A1228lccbF, P00756_A1231lccbT, P00756_A1207lccbP,
            P00756_n1207lccbP, P00756_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P00759_A1488ICSI_, P00759_A1487ICSI_, P00759_A1484ICSI_, P00759_n1484ICSI_, P00759_A1485ICSI_, P00759_n1485ICSI_, P00759_A1479ICSI_, P00759_n1479ICSI_, P00759_A1478ICSI_, P00759_n1478ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007512_A970DATA, P007512_A969TIPO_V, P007512_A968NUM_BI, P007512_A967IATA, P007512_A966CODE, P007512_A965PER_NA, P007512_A964CiaCod, P007512_A963ISOC, P007512_A902NUM_BI, P007512_n902NUM_BI,
            P007512_A869ID_AGE, P007512_n869ID_AGE
            }
            , new Object[] {
            P007513_A963ISOC, P007513_A964CiaCod, P007513_A965PER_NA, P007513_A966CODE, P007513_A967IATA, P007513_A968NUM_BI, P007513_A969TIPO_V, P007513_A970DATA, P007513_A923I_From, P007513_n923I_From,
            P007513_A924I_To, P007513_n924I_To, P007513_A928Claxss, P007513_n928Claxss, P007513_A926Tariff, P007513_n926Tariff, P007513_A925Carrie, P007513_n925Carrie, P007513_A929Fli_Da, P007513_n929Fli_Da,
            P007513_A971ISeq
            }
            , new Object[] {
            P007514_A1313CadIA, P007514_A1314CadNo, P007514_n1314CadNo
            }
            , new Object[] {
            }
            , new Object[] {
            P007516_A1312SCEId
            }
            , new Object[] {
            P007517_A1488ICSI_, P007517_A1487ICSI_, P007517_A1478ICSI_, P007517_n1478ICSI_
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV148TestM ;
   private byte AV138trnTy ;
   private byte AV220GXLvl ;
   private byte AV98i ;
   private byte AV223GXLvl ;
   private byte GXv_int10[] ;
   private byte GXv_int9[] ;
   private byte AV224GXLvl ;
   private byte AV115Ver ;
   private byte AV226GXLvl ;
   private byte AV186dia ;
   private byte AV187mes ;
   private byte AV227GXLvl ;
   private short AV177SeqRe ;
   private short A1168lccbI ;
   private short A1230lccbS ;
   private short Gx_err ;
   private short AV17SeqLot ;
   private short AV141lccbI ;
   private short AV180Num03 ;
   private short AV178SeqAr ;
   private short GXv_int5[] ;
   private int AV191Total ;
   private int AV160QtdPa ;
   private int AV10SeqReg ;
   private int GX_INS236 ;
   private int GX_I ;
   private int AV58ICSI_C ;
   private int AV170NumRe ;
   private int AV151cntCC ;
   private int AV152cntPL ;
   private int AV56ICSI_C ;
   private int A1479ICSI_ ;
   private int A1478ICSI_ ;
   private int GX_INS273 ;
   private int AV184Num05 ;
   private int A971ISeq ;
   private int GX_INS248 ;
   private long AV15FileNo ;
   private long AV142NUM_B ;
   private long A902NUM_BI ;
   private long A1312SCEId ;
   private double A1170lccbI ;
   private double A1169lccbD ;
   private double A1172lccbS ;
   private double A1171lccbT ;
   private double AV190Valor ;
   private double AV61lccbIn ;
   private double AV40lccbDo ;
   private double AV27lccbTi ;
   private double AV24lccbSa ;
   private double AV51TotalV ;
   private double AV95GTotal ;
   private double AV153tSale ;
   private double AV155tTipC ;
   private double AV154tSale ;
   private double AV157tTipP ;
   private double AV158tDown ;
   private double AV48TotalE ;
   private double AV81TotalP ;
   private double AV86GTotal ;
   private double AV89GTotal ;
   private double AV92GTotal ;
   private double AV159tInst ;
   private double AV64v ;
   private double AV80TotalT ;
   private double GXv_int8[] ;
   private double AV188ano ;
   private String AV8DebugMo ;
   private String AV54Versao ;
   private String AV208UsaNo ;
   private String AV192Aux ;
   private String AV9Sep ;
   private String AV168RetSu ;
   private String scmdbuf ;
   private String A1150lccbE ;
   private String A1184lccbS ;
   private String A1227lccbO ;
   private String A1490Distr ;
   private String A1224lccbC ;
   private String A1147lccbE ;
   private String A1222lccbI ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1228lccbF ;
   private String AV195lccbC ;
   private String AV12FileNa ;
   private String AV172CriaF ;
   private String AV176NovoL ;
   private String AV131OldEm ;
   private String AV171CriaA ;
   private String AV183Fecha ;
   private String A1167lccbG ;
   private String A1179lccbV ;
   private String A1194lccbS ;
   private String A1195lccbS ;
   private String A1192lccbS ;
   private String A1185lccbB ;
   private String A1191lccbS ;
   private String A1193lccbS ;
   private String AV181Erro ;
   private String AV182Error ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String A1480ICSI_ ;
   private String A1481ICSI_ ;
   private String AV134ICSI_ ;
   private String AV149ICSI_ ;
   private String AV43lccbEm ;
   private String AV45lccbIA ;
   private String AV13lccbCC ;
   private String AV196Atrib ;
   private String AV198Resul ;
   private String AV22lccbCC ;
   private String AV38lccbAp ;
   private String AV46lccbOp ;
   private String AV44lccbFP ;
   private String AV194lccbV ;
   private String AV34lccbTD[] ;
   private String AV33lccbPa[] ;
   private String A1231lccbT ;
   private String A1207lccbP ;
   private String A1232lccbT ;
   private String AV140s ;
   private String AV67lccbTR ;
   private String AV150Short ;
   private String AV139Refer ;
   private String AV173SCEAi ;
   private String AV174SCETe ;
   private String GXt_char1 ;
   private String A1484ICSI_ ;
   private String A1485ICSI_ ;
   private String AV132ICSI_ ;
   private String AV133ICSI_ ;
   private String AV179RefAr ;
   private String AV21DataC ;
   private String AV20HoraB ;
   private String AV35YYMMDD ;
   private String AV145Claxs[] ;
   private String AV111Fli_D[] ;
   private String AV124CadNo ;
   private String A969TIPO_V ;
   private String A968NUM_BI ;
   private String A967IATA ;
   private String A966CODE ;
   private String A965PER_NA ;
   private String A964CiaCod ;
   private String A963ISOC ;
   private String A869ID_AGE ;
   private String AV125PER_N ;
   private String AV126IATA ;
   private String A928Claxss ;
   private String A929Fli_Da ;
   private String A1313CadIA ;
   private String A1314CadNo ;
   private String AV11DataB ;
   private String AV19HoraA ;
   private String AV207FileN ;
   private String AV164ArqN[] ;
   private String A1305SCETe ;
   private String A1301SCEAi ;
   private String A1303SCERE ;
   private String A1311SCEVe ;
   private String A1298SCETk ;
   private String A1300SCECR ;
   private String A1307SCEIa ;
   private String A1310SCETp ;
   private String A1306SCEAP ;
   private String GXv_char4[] ;
   private String AV204PathC ;
   private String GXt_char3 ;
   private String GXv_char7[] ;
   private String GXv_char6[] ;
   private String AV206Filen ;
   private java.util.Date A1190lccbS ;
   private java.util.Date A1229lccbS ;
   private java.util.Date AV163ArqD1[] ;
   private java.util.Date A1299SCEDa ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1189lccbS ;
   private java.util.Date AV39lccbDa ;
   private java.util.Date Gx_date ;
   private java.util.Date A970DATA ;
   private java.util.Date AV123DATA ;
   private boolean brk752 ;
   private boolean n1184lccbS ;
   private boolean n1490Distr ;
   private boolean n1147lccbE ;
   private boolean returnInSub ;
   private boolean n1168lccbI ;
   private boolean n1167lccbG ;
   private boolean n1170lccbI ;
   private boolean n1169lccbD ;
   private boolean n1172lccbS ;
   private boolean n1179lccbV ;
   private boolean n1171lccbT ;
   private boolean n1194lccbS ;
   private boolean n1195lccbS ;
   private boolean n1189lccbS ;
   private boolean n1192lccbS ;
   private boolean n1185lccbB ;
   private boolean n1190lccbS ;
   private boolean n1191lccbS ;
   private boolean n1193lccbS ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n1480ICSI_ ;
   private boolean n1481ICSI_ ;
   private boolean n1207lccbP ;
   private boolean n1484ICSI_ ;
   private boolean n1485ICSI_ ;
   private boolean n1479ICSI_ ;
   private boolean n1478ICSI_ ;
   private boolean n902NUM_BI ;
   private boolean n869ID_AGE ;
   private boolean n923I_From ;
   private boolean n924I_To ;
   private boolean n928Claxss ;
   private boolean n926Tariff ;
   private boolean n925Carrie ;
   private boolean n929Fli_Da ;
   private boolean n1314CadNo ;
   private boolean n1301SCEAi ;
   private boolean n1303SCERE ;
   private boolean n1305SCETe ;
   private boolean n1311SCEVe ;
   private boolean n1298SCETk ;
   private boolean n1299SCEDa ;
   private boolean n1300SCECR ;
   private boolean n1307SCEIa ;
   private boolean n1310SCETp ;
   private boolean n1306SCEAP ;
   private boolean n1312SCEId ;
   private String AV209Bande ;
   private String AV210NovaB ;
   private String AV211Bande ;
   private String AV212NovaB ;
   private String A1187lccbS ;
   private String AV213Proce ;
   private String AV214msgEr ;
   private String AV143I_Fro[] ;
   private String AV147Carri[] ;
   private String AV146Tarif[] ;
   private String AV144I_To[] ;
   private String A923I_From ;
   private String A924I_To ;
   private String A926Tariff ;
   private String A925Carrie ;
   private String AV202Audit ;
   private String AV199Audit ;
   private String AV200Audit ;
   private String AV201Audit ;
   private String GXv_svchar2[] ;
   private com.genexus.util.GXFile AV205File ;
   private com.genexus.util.GXDirectory AV203Diret ;
   private String[] aP0 ;
   private IDataStoreProvider pr_default ;
   private String[] P00752_A1150lccbE ;
   private String[] P00752_A1184lccbS ;
   private boolean[] P00752_n1184lccbS ;
   private String[] P00752_A1227lccbO ;
   private String[] P00752_A1490Distr ;
   private boolean[] P00752_n1490Distr ;
   private String[] P00752_A1224lccbC ;
   private String[] P00752_A1147lccbE ;
   private boolean[] P00752_n1147lccbE ;
   private String[] P00752_A1222lccbI ;
   private java.util.Date[] P00752_A1223lccbD ;
   private String[] P00752_A1225lccbC ;
   private String[] P00752_A1226lccbA ;
   private String[] P00752_A1228lccbF ;
   private String[] P00753_A1228lccbF ;
   private String[] P00753_A1227lccbO ;
   private String[] P00753_A1226lccbA ;
   private String[] P00753_A1225lccbC ;
   private String[] P00753_A1224lccbC ;
   private java.util.Date[] P00753_A1223lccbD ;
   private String[] P00753_A1222lccbI ;
   private String[] P00753_A1150lccbE ;
   private short[] P00753_A1168lccbI ;
   private boolean[] P00753_n1168lccbI ;
   private String[] P00753_A1147lccbE ;
   private boolean[] P00753_n1147lccbE ;
   private String[] P00753_A1490Distr ;
   private boolean[] P00753_n1490Distr ;
   private String[] P00753_A1184lccbS ;
   private boolean[] P00753_n1184lccbS ;
   private String[] P00753_A1167lccbG ;
   private boolean[] P00753_n1167lccbG ;
   private double[] P00753_A1170lccbI ;
   private boolean[] P00753_n1170lccbI ;
   private double[] P00753_A1169lccbD ;
   private boolean[] P00753_n1169lccbD ;
   private double[] P00753_A1172lccbS ;
   private boolean[] P00753_n1172lccbS ;
   private String[] P00753_A1179lccbV ;
   private boolean[] P00753_n1179lccbV ;
   private double[] P00753_A1171lccbT ;
   private boolean[] P00753_n1171lccbT ;
   private String[] P00753_A1194lccbS ;
   private boolean[] P00753_n1194lccbS ;
   private String[] P00753_A1195lccbS ;
   private boolean[] P00753_n1195lccbS ;
   private java.util.Date[] P00753_A1189lccbS ;
   private boolean[] P00753_n1189lccbS ;
   private String[] P00753_A1192lccbS ;
   private boolean[] P00753_n1192lccbS ;
   private String[] P00753_A1185lccbB ;
   private boolean[] P00753_n1185lccbB ;
   private java.util.Date[] P00753_A1190lccbS ;
   private boolean[] P00753_n1190lccbS ;
   private String[] P00753_A1191lccbS ;
   private boolean[] P00753_n1191lccbS ;
   private String[] P00753_A1193lccbS ;
   private boolean[] P00753_n1193lccbS ;
   private String[] P00755_A1488ICSI_ ;
   private String[] P00755_A1487ICSI_ ;
   private String[] P00755_A1480ICSI_ ;
   private boolean[] P00755_n1480ICSI_ ;
   private String[] P00755_A1481ICSI_ ;
   private boolean[] P00755_n1481ICSI_ ;
   private String[] P00756_A1150lccbE ;
   private String[] P00756_A1222lccbI ;
   private java.util.Date[] P00756_A1223lccbD ;
   private String[] P00756_A1224lccbC ;
   private String[] P00756_A1225lccbC ;
   private String[] P00756_A1226lccbA ;
   private String[] P00756_A1227lccbO ;
   private String[] P00756_A1228lccbF ;
   private String[] P00756_A1231lccbT ;
   private String[] P00756_A1207lccbP ;
   private boolean[] P00756_n1207lccbP ;
   private String[] P00756_A1232lccbT ;
   private String[] P00759_A1488ICSI_ ;
   private String[] P00759_A1487ICSI_ ;
   private String[] P00759_A1484ICSI_ ;
   private boolean[] P00759_n1484ICSI_ ;
   private String[] P00759_A1485ICSI_ ;
   private boolean[] P00759_n1485ICSI_ ;
   private int[] P00759_A1479ICSI_ ;
   private boolean[] P00759_n1479ICSI_ ;
   private int[] P00759_A1478ICSI_ ;
   private boolean[] P00759_n1478ICSI_ ;
   private java.util.Date[] P007512_A970DATA ;
   private String[] P007512_A969TIPO_V ;
   private String[] P007512_A968NUM_BI ;
   private String[] P007512_A967IATA ;
   private String[] P007512_A966CODE ;
   private String[] P007512_A965PER_NA ;
   private String[] P007512_A964CiaCod ;
   private String[] P007512_A963ISOC ;
   private long[] P007512_A902NUM_BI ;
   private boolean[] P007512_n902NUM_BI ;
   private String[] P007512_A869ID_AGE ;
   private boolean[] P007512_n869ID_AGE ;
   private String[] P007513_A963ISOC ;
   private String[] P007513_A964CiaCod ;
   private String[] P007513_A965PER_NA ;
   private String[] P007513_A966CODE ;
   private String[] P007513_A967IATA ;
   private String[] P007513_A968NUM_BI ;
   private String[] P007513_A969TIPO_V ;
   private java.util.Date[] P007513_A970DATA ;
   private String[] P007513_A923I_From ;
   private boolean[] P007513_n923I_From ;
   private String[] P007513_A924I_To ;
   private boolean[] P007513_n924I_To ;
   private String[] P007513_A928Claxss ;
   private boolean[] P007513_n928Claxss ;
   private String[] P007513_A926Tariff ;
   private boolean[] P007513_n926Tariff ;
   private String[] P007513_A925Carrie ;
   private boolean[] P007513_n925Carrie ;
   private String[] P007513_A929Fli_Da ;
   private boolean[] P007513_n929Fli_Da ;
   private int[] P007513_A971ISeq ;
   private String[] P007514_A1313CadIA ;
   private String[] P007514_A1314CadNo ;
   private boolean[] P007514_n1314CadNo ;
   private long[] P007516_A1312SCEId ;
   private boolean[] P007516_n1312SCEId ;
   private String[] P007517_A1488ICSI_ ;
   private String[] P007517_A1487ICSI_ ;
   private int[] P007517_A1478ICSI_ ;
   private boolean[] P007517_n1478ICSI_ ;
}

final  class psubmissaoredecard__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P00753( byte AV138trnTy ,
                                        short A1168lccbI ,
                                        String A1490Distr ,
                                        String A1147lccbE ,
                                        String AV195lccbC ,
                                        String A1227lccbO ,
                                        String A1184lccbS ,
                                        String A1224lccbC )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT T1.[lccbFPAC_PLP], T1.[lccbOpCode], T1.[lccbAppCode], T1.[lccbCCNum], T1.[lccbCCard]," ;
      scmdbuf = scmdbuf + " T1.[lccbDate], T1.[lccbIATA], T1.[lccbEmpCod], T1.[lccbInstallments], T2.[lccbEmpEnab]," ;
      scmdbuf = scmdbuf + " T1.[DistribuicaoTransacoesTipo], T1.[lccbStatus], T1.[lccbGDS], T1.[lccbInstAmount]," ;
      scmdbuf = scmdbuf + " T1.[lccbDownPayment], T1.[lccbSaleAmount], T1.[lccbValidDate], T1.[lccbTip], T1.[lccbSubRO]," ;
      scmdbuf = scmdbuf + " T1.[lccbSubPOS], T1.[lccbSubDate], T1.[lccbSubFile], T1.[lccbBatchNum], T1.[lccbSubTime]," ;
      scmdbuf = scmdbuf + " T1.[lccbSubType], T1.[lccbSubTrn] FROM ([LCCBPLP] T1 WITH (NOLOCK) INNER JOIN [LCCBEMP]" ;
      scmdbuf = scmdbuf + " T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod])" ;
      scmdbuf = scmdbuf + " WHERE (T1.[lccbOpCode] = 'S' and T1.[lccbStatus] = 'TOSUB' and T1.[lccbCCard] = '" + GXutil.rtrim( GXutil.strReplace( AV195lccbC, "'", "''")) + "')" ;
      scmdbuf = scmdbuf + " and (SUBSTRING(T1.[DistribuicaoTransacoesTipo], 1, 1) = 'R')" ;
      scmdbuf = scmdbuf + " and (T2.[lccbEmpEnab] = '1')" ;
      sWhereString = "" ;
      if ( ( AV138trnTy == 1 ) )
      {
         sWhereString = sWhereString + " and (T1.[lccbInstallments] <= 1)" ;
      }
      if ( ( AV138trnTy == 2 ) )
      {
         sWhereString = sWhereString + " and (T1.[lccbInstallments] > 1)" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY T1.[lccbOpCode], T1.[lccbStatus], T1.[lccbCCard], T1.[DistribuicaoTransacoesTipo], T1.[lccbInstallments]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 1 :
                  return conditional_P00753( ((Number) dynConstraints[0]).byteValue() , ((Number) dynConstraints[1]).shortValue() , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00752", "SELECT T1.[lccbEmpCod], T1.[lccbStatus], T1.[lccbOpCode], T1.[DistribuicaoTransacoesTipo], T1.[lccbCCard], T2.[lccbEmpEnab], T1.[lccbIATA], T1.[lccbDate], T1.[lccbCCNum], T1.[lccbAppCode], T1.[lccbFPAC_PLP] FROM ([LCCBPLP] T1 WITH (NOLOCK) INNER JOIN [LCCBEMP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod]) WHERE (T1.[lccbOpCode] = 'S' and T1.[lccbStatus] = 'TOSUB') AND (SUBSTRING(T1.[DistribuicaoTransacoesTipo], 1, 1) = 'R') AND (T2.[lccbEmpEnab] = '1') ORDER BY T1.[lccbOpCode], T1.[lccbStatus], T1.[lccbCCard], T1.[DistribuicaoTransacoesTipo] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00753", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P00754", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00755", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCPOS1], [ICSI_CCPOS2] FROM [ICSI_CCINFO] WITH (NOLOCK) WHERE [ICSI_EmpCod] = ? and [ICSI_CCCod] = 'RC' ORDER BY [ICSI_EmpCod], [ICSI_CCCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P00756", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbPaxName], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P00757", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P00758", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbSubRO]=?, [lccbSubPOS]=?, [lccbSubDate]=?, [lccbSubFile]=?, [lccbBatchNum]=?, [lccbSubTime]=?, [lccbSubType]=?, [lccbSubTrn]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00759", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCEstab], [ICSI_CCNome], [ICSI_CCSeqFile], [ICSI_CCLote] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = '000' AND [ICSI_CCCod] = 'RC') AND ([ICSI_EmpCod] = '000' and [ICSI_CCCod] = 'RC') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007510", "UPDATE [ICSI_CCINFO] SET [ICSI_CCSeqFile]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007511", "INSERT INTO [ICSI_CCINFO] ([ICSI_EmpCod], [ICSI_CCCod], [ICSI_CCLote], [ICSI_CCSeqFile], [ICSI_CCEstab], [ICSI_CCNome], [ICSI_CCPOS1], [ICSI_CCPOS2], [ICSI_CCPOS3], [ICSI_SplitPLP]) VALUES (?, ?, ?, ?, ?, ?, '', '', '', convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007512", "SELECT [DATA], [TIPO_VEND], [NUM_BIL], [IATA], [CODE], [PER_NAME], [CiaCod], [ISOC], [NUM_BIL2], [ID_AGENCY] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([CODE] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007513", "SELECT [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [I_From], [I_To], [Claxss], [Tariff], [Carrier], [Fli_DateC], [ISeq] FROM [HOT1] WITH (NOLOCK) WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007514", "SELECT TOP 1 [CadIATA], [CadNome] FROM [CADAGE] WITH (NOLOCK) WHERE [CadIATA] = ? ORDER BY [CadIATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007515", "INSERT INTO [SCEVENTS] ([SCETkt], [SCEDate], [SCECRS], [SCEAirLine], [SCERETName], [SCEText], [SCEAPP], [SCEIata], [SCETpEvento], [SCEVersao], [SCEAirportCode], [SCEFopID], [SCEFPAM], [SCEParcela]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '', convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007516", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
         ,new ForEachCursor("P007517", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCLote] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = '000' AND [ICSI_CCCod] = 'RC') AND ([ICSI_EmpCod] = '000' and [ICSI_CCCod] = 'RC') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007518", "UPDATE [ICSI_CCINFO] SET [ICSI_CCLote]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 8) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getString(7, 7) ;
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[11])[0] = rslt.getString(9, 44) ;
               ((String[]) buf[12])[0] = rslt.getString(10, 20) ;
               ((String[]) buf[13])[0] = rslt.getString(11, 19) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((short[]) buf[8])[0] = rslt.getShort(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 1) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 4) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 8) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 4) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(17, 4) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((double[]) buf[26])[0] = rslt.getDouble(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((String[]) buf[28])[0] = rslt.getString(19, 10) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[32])[0] = rslt.getGXDate(21) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((String[]) buf[34])[0] = rslt.getString(22, 20) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((String[]) buf[36])[0] = rslt.getString(23, 20) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[38])[0] = rslt.getGXDateTime(24) ;
               ((boolean[]) buf[39])[0] = rslt.wasNull();
               ((String[]) buf[40])[0] = rslt.getString(25, 1) ;
               ((boolean[]) buf[41])[0] = rslt.wasNull();
               ((String[]) buf[42])[0] = rslt.getString(26, 20) ;
               ((boolean[]) buf[43])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 50) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(11, 4) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((int[]) buf[6])[0] = rslt.getInt(5) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((int[]) buf[8])[0] = rslt.getInt(6) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               break;
            case 10 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 2) ;
               ((long[]) buf[8])[0] = rslt.getLong(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 2) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getVarchar(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 5) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((int[]) buf[20])[0] = rslt.getInt(15) ;
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 45) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 14 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 15 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((int[]) buf[2])[0] = rslt.getInt(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 10);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(4, (java.util.Date)parms[7]);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 20);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(7, (java.util.Date)parms[13], false);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 1);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 20);
               }
               stmt.setString(10, (String)parms[18], 3);
               stmt.setString(11, (String)parms[19], 7);
               stmt.setDate(12, (java.util.Date)parms[20]);
               stmt.setString(13, (String)parms[21], 2);
               stmt.setString(14, (String)parms[22], 44);
               stmt.setString(15, (String)parms[23], 20);
               stmt.setString(16, (String)parms[24], 1);
               stmt.setString(17, (String)parms[25], 19);
               break;
            case 8 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
            case 9 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 10);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(3, ((Number) parms[3]).intValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(4, ((Number) parms[5]).intValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[7], 20);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[9], 30);
               }
               break;
            case 10 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 4);
               break;
            case 11 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               break;
            case 12 :
               stmt.setString(1, (String)parms[0], 7);
               break;
            case 13 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 5);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 3);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 150);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 4);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 11);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 3);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 5);
               }
               break;
            case 16 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
      }
   }

}


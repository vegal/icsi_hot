import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtLogins extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtLogins( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtLogins.class));
   }

   public SdtLogins( int remoteHandle ,
                     ModelContext context )
   {
      super( context, "SdtLogins");
      initialize( remoteHandle) ;
   }

   public SdtLogins( int remoteHandle ,
                     StructSdtLogins struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV30lgnLogin )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV30lgnLogin});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLogin") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnlogin = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnName") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnname = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnEnable") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnenable = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnEmail") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnemail = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwd") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwd = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwdTmp") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdtmp = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastLogin") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtLogins_Lgnlastlogin = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtLogins_Lgnlastlogin = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwdLastChanged") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtLogins_Lgnpwdlastchanged = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtLogins_Lgnpwdlastchanged = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPWDForceChange") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdforcechange = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnSuperUser") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnsuperuser = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "LastLoginInfo") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtLogins_Lastlogininfo.readxml(oReader, "LastLoginInfo") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Profiles") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtLogins_Profiles.readxml(oReader, "Profiles") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLogin_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnlogin_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnName_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnname_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnEnable_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnenable_Z = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnEmail_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnemail_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwd_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwd_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwdTmp_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdtmp_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastLogin_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtLogins_Lgnlastlogin_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtLogins_Lgnlastlogin_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwdLastChanged_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtLogins_Lgnpwdlastchanged_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtLogins_Lgnpwdlastchanged_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPWDForceChange_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdforcechange_Z = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnSuperUser_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnsuperuser_Z = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLogin_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnlogin_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnName_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnname_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnEnable_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnenable_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnEmail_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnemail_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwd_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwd_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwdTmp_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLastLogin_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnlastlogin_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPwdLastChanged_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnPWDForceChange_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnSuperUser_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Lgnsuperuser_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Logins" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("lgnLogin", GXutil.rtrim( gxTv_SdtLogins_Lgnlogin));
      oWriter.writeElement("lgnName", GXutil.rtrim( gxTv_SdtLogins_Lgnname));
      oWriter.writeElement("lgnEnable", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnenable, 1, 0)));
      oWriter.writeElement("lgnEmail", GXutil.rtrim( gxTv_SdtLogins_Lgnemail));
      oWriter.writeElement("lgnPwd", GXutil.rtrim( gxTv_SdtLogins_Lgnpwd));
      oWriter.writeElement("lgnPwdTmp", GXutil.rtrim( gxTv_SdtLogins_Lgnpwdtmp));
      if ( (GXutil.nullDate().equals(gxTv_SdtLogins_Lgnlastlogin)) )
      {
         oWriter.writeElement("lgnLastLogin", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtLogins_Lgnlastlogin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtLogins_Lgnlastlogin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtLogins_Lgnlastlogin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtLogins_Lgnlastlogin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtLogins_Lgnlastlogin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtLogins_Lgnlastlogin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("lgnLastLogin", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtLogins_Lgnpwdlastchanged)) )
      {
         oWriter.writeElement("lgnPwdLastChanged", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtLogins_Lgnpwdlastchanged), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtLogins_Lgnpwdlastchanged), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtLogins_Lgnpwdlastchanged), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtLogins_Lgnpwdlastchanged), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtLogins_Lgnpwdlastchanged), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtLogins_Lgnpwdlastchanged), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("lgnPwdLastChanged", sDateCnv);
      }
      oWriter.writeElement("lgnPWDForceChange", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnpwdforcechange, 1, 0)));
      oWriter.writeElement("lgnSuperUser", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnsuperuser, 1, 0)));
      gxTv_SdtLogins_Lastlogininfo.writexml(oWriter, "LastLoginInfo", "IataICSI");
      gxTv_SdtLogins_Profiles.writexml(oWriter, "Profiles", "IataICSI");
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtLogins_Mode));
      oWriter.writeElement("lgnLogin_Z", GXutil.rtrim( gxTv_SdtLogins_Lgnlogin_Z));
      oWriter.writeElement("lgnName_Z", GXutil.rtrim( gxTv_SdtLogins_Lgnname_Z));
      oWriter.writeElement("lgnEnable_Z", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnenable_Z, 1, 0)));
      oWriter.writeElement("lgnEmail_Z", GXutil.rtrim( gxTv_SdtLogins_Lgnemail_Z));
      oWriter.writeElement("lgnPwd_Z", GXutil.rtrim( gxTv_SdtLogins_Lgnpwd_Z));
      oWriter.writeElement("lgnPwdTmp_Z", GXutil.rtrim( gxTv_SdtLogins_Lgnpwdtmp_Z));
      if ( (GXutil.nullDate().equals(gxTv_SdtLogins_Lgnlastlogin_Z)) )
      {
         oWriter.writeElement("lgnLastLogin_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtLogins_Lgnlastlogin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtLogins_Lgnlastlogin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtLogins_Lgnlastlogin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtLogins_Lgnlastlogin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtLogins_Lgnlastlogin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtLogins_Lgnlastlogin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("lgnLastLogin_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtLogins_Lgnpwdlastchanged_Z)) )
      {
         oWriter.writeElement("lgnPwdLastChanged_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtLogins_Lgnpwdlastchanged_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtLogins_Lgnpwdlastchanged_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtLogins_Lgnpwdlastchanged_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtLogins_Lgnpwdlastchanged_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtLogins_Lgnpwdlastchanged_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtLogins_Lgnpwdlastchanged_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("lgnPwdLastChanged_Z", sDateCnv);
      }
      oWriter.writeElement("lgnPWDForceChange_Z", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnpwdforcechange_Z, 1, 0)));
      oWriter.writeElement("lgnSuperUser_Z", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnsuperuser_Z, 1, 0)));
      oWriter.writeElement("lgnLogin_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnlogin_N, 1, 0)));
      oWriter.writeElement("lgnName_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnname_N, 1, 0)));
      oWriter.writeElement("lgnEnable_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnenable_N, 1, 0)));
      oWriter.writeElement("lgnEmail_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnemail_N, 1, 0)));
      oWriter.writeElement("lgnPwd_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnpwd_N, 1, 0)));
      oWriter.writeElement("lgnPwdTmp_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnpwdtmp_N, 1, 0)));
      oWriter.writeElement("lgnLastLogin_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnlastlogin_N, 1, 0)));
      oWriter.writeElement("lgnPwdLastChanged_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnpwdlastchanged_N, 1, 0)));
      oWriter.writeElement("lgnPWDForceChange_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnpwdforcechange_N, 1, 0)));
      oWriter.writeElement("lgnSuperUser_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Lgnsuperuser_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnlogin( )
   {
      return gxTv_SdtLogins_Lgnlogin ;
   }

   public void setgxTv_SdtLogins_Lgnlogin( String value )
   {
      gxTv_SdtLogins_Lgnlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnlogin = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnlogin_SetNull( )
   {
      gxTv_SdtLogins_Lgnlogin_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnlogin = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnname( )
   {
      return gxTv_SdtLogins_Lgnname ;
   }

   public void setgxTv_SdtLogins_Lgnname( String value )
   {
      gxTv_SdtLogins_Lgnname_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnname = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnname_SetNull( )
   {
      gxTv_SdtLogins_Lgnname_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnname = "" ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnenable( )
   {
      return gxTv_SdtLogins_Lgnenable ;
   }

   public void setgxTv_SdtLogins_Lgnenable( byte value )
   {
      gxTv_SdtLogins_Lgnenable_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnenable = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnenable_SetNull( )
   {
      gxTv_SdtLogins_Lgnenable_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnenable = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnemail( )
   {
      return gxTv_SdtLogins_Lgnemail ;
   }

   public void setgxTv_SdtLogins_Lgnemail( String value )
   {
      gxTv_SdtLogins_Lgnemail_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnemail_SetNull( )
   {
      gxTv_SdtLogins_Lgnemail_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnemail = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnpwd( )
   {
      return gxTv_SdtLogins_Lgnpwd ;
   }

   public void setgxTv_SdtLogins_Lgnpwd( String value )
   {
      gxTv_SdtLogins_Lgnpwd_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwd = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwd_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwd_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnpwd = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnpwdtmp( )
   {
      return gxTv_SdtLogins_Lgnpwdtmp ;
   }

   public void setgxTv_SdtLogins_Lgnpwdtmp( String value )
   {
      gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdtmp = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdtmp_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnpwdtmp = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtLogins_Lgnlastlogin( )
   {
      return gxTv_SdtLogins_Lgnlastlogin ;
   }

   public void setgxTv_SdtLogins_Lgnlastlogin( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnlastlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnlastlogin = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnlastlogin_SetNull( )
   {
      gxTv_SdtLogins_Lgnlastlogin_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnlastlogin = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public java.util.Date getgxTv_SdtLogins_Lgnpwdlastchanged( )
   {
      return gxTv_SdtLogins_Lgnpwdlastchanged ;
   }

   public void setgxTv_SdtLogins_Lgnpwdlastchanged( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdlastchanged = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdlastchanged_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnpwdlastchanged = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnpwdforcechange( )
   {
      return gxTv_SdtLogins_Lgnpwdforcechange ;
   }

   public void setgxTv_SdtLogins_Lgnpwdforcechange( byte value )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdforcechange = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdforcechange_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnpwdforcechange = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnsuperuser( )
   {
      return gxTv_SdtLogins_Lgnsuperuser ;
   }

   public void setgxTv_SdtLogins_Lgnsuperuser( byte value )
   {
      gxTv_SdtLogins_Lgnsuperuser_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnsuperuser_SetNull( )
   {
      gxTv_SdtLogins_Lgnsuperuser_N = (byte)(1) ;
      gxTv_SdtLogins_Lgnsuperuser = (byte)(0) ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtLogins_Lastlogininfo( )
   {
      return gxTv_SdtLogins_Lastlogininfo ;
   }

   public void setgxTv_SdtLogins_Lastlogininfo( GxSilentTrnGridCollection value )
   {
      gxTv_SdtLogins_Lastlogininfo = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lastlogininfo_SetNull( )
   {
      gxTv_SdtLogins_Lastlogininfo = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtLogins_Profiles( )
   {
      return gxTv_SdtLogins_Profiles ;
   }

   public void setgxTv_SdtLogins_Profiles( GxSilentTrnGridCollection value )
   {
      gxTv_SdtLogins_Profiles = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_SetNull( )
   {
      gxTv_SdtLogins_Profiles = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public String getgxTv_SdtLogins_Mode( )
   {
      return gxTv_SdtLogins_Mode ;
   }

   public void setgxTv_SdtLogins_Mode( String value )
   {
      gxTv_SdtLogins_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Mode_SetNull( )
   {
      gxTv_SdtLogins_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnlogin_Z( )
   {
      return gxTv_SdtLogins_Lgnlogin_Z ;
   }

   public void setgxTv_SdtLogins_Lgnlogin_Z( String value )
   {
      gxTv_SdtLogins_Lgnlogin_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnlogin_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnlogin_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnname_Z( )
   {
      return gxTv_SdtLogins_Lgnname_Z ;
   }

   public void setgxTv_SdtLogins_Lgnname_Z( String value )
   {
      gxTv_SdtLogins_Lgnname_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnname_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnname_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnenable_Z( )
   {
      return gxTv_SdtLogins_Lgnenable_Z ;
   }

   public void setgxTv_SdtLogins_Lgnenable_Z( byte value )
   {
      gxTv_SdtLogins_Lgnenable_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnenable_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnenable_Z = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnemail_Z( )
   {
      return gxTv_SdtLogins_Lgnemail_Z ;
   }

   public void setgxTv_SdtLogins_Lgnemail_Z( String value )
   {
      gxTv_SdtLogins_Lgnemail_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnemail_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnemail_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnpwd_Z( )
   {
      return gxTv_SdtLogins_Lgnpwd_Z ;
   }

   public void setgxTv_SdtLogins_Lgnpwd_Z( String value )
   {
      gxTv_SdtLogins_Lgnpwd_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwd_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwd_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Lgnpwdtmp_Z( )
   {
      return gxTv_SdtLogins_Lgnpwdtmp_Z ;
   }

   public void setgxTv_SdtLogins_Lgnpwdtmp_Z( String value )
   {
      gxTv_SdtLogins_Lgnpwdtmp_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdtmp_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdtmp_Z = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtLogins_Lgnlastlogin_Z( )
   {
      return gxTv_SdtLogins_Lgnlastlogin_Z ;
   }

   public void setgxTv_SdtLogins_Lgnlastlogin_Z( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnlastlogin_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnlastlogin_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnlastlogin_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public java.util.Date getgxTv_SdtLogins_Lgnpwdlastchanged_Z( )
   {
      return gxTv_SdtLogins_Lgnpwdlastchanged_Z ;
   }

   public void setgxTv_SdtLogins_Lgnpwdlastchanged_Z( java.util.Date value )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdlastchanged_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnpwdforcechange_Z( )
   {
      return gxTv_SdtLogins_Lgnpwdforcechange_Z ;
   }

   public void setgxTv_SdtLogins_Lgnpwdforcechange_Z( byte value )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdforcechange_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_Z = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnsuperuser_Z( )
   {
      return gxTv_SdtLogins_Lgnsuperuser_Z ;
   }

   public void setgxTv_SdtLogins_Lgnsuperuser_Z( byte value )
   {
      gxTv_SdtLogins_Lgnsuperuser_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnsuperuser_Z_SetNull( )
   {
      gxTv_SdtLogins_Lgnsuperuser_Z = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnlogin_N( )
   {
      return gxTv_SdtLogins_Lgnlogin_N ;
   }

   public void setgxTv_SdtLogins_Lgnlogin_N( byte value )
   {
      gxTv_SdtLogins_Lgnlogin_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnlogin_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnlogin_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnname_N( )
   {
      return gxTv_SdtLogins_Lgnname_N ;
   }

   public void setgxTv_SdtLogins_Lgnname_N( byte value )
   {
      gxTv_SdtLogins_Lgnname_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnname_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnname_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnenable_N( )
   {
      return gxTv_SdtLogins_Lgnenable_N ;
   }

   public void setgxTv_SdtLogins_Lgnenable_N( byte value )
   {
      gxTv_SdtLogins_Lgnenable_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnenable_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnenable_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnemail_N( )
   {
      return gxTv_SdtLogins_Lgnemail_N ;
   }

   public void setgxTv_SdtLogins_Lgnemail_N( byte value )
   {
      gxTv_SdtLogins_Lgnemail_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnemail_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnemail_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnpwd_N( )
   {
      return gxTv_SdtLogins_Lgnpwd_N ;
   }

   public void setgxTv_SdtLogins_Lgnpwd_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwd_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwd_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwd_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnpwdtmp_N( )
   {
      return gxTv_SdtLogins_Lgnpwdtmp_N ;
   }

   public void setgxTv_SdtLogins_Lgnpwdtmp_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwdtmp_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdtmp_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnlastlogin_N( )
   {
      return gxTv_SdtLogins_Lgnlastlogin_N ;
   }

   public void setgxTv_SdtLogins_Lgnlastlogin_N( byte value )
   {
      gxTv_SdtLogins_Lgnlastlogin_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnlastlogin_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnlastlogin_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnpwdlastchanged_N( )
   {
      return gxTv_SdtLogins_Lgnpwdlastchanged_N ;
   }

   public void setgxTv_SdtLogins_Lgnpwdlastchanged_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdlastchanged_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnpwdforcechange_N( )
   {
      return gxTv_SdtLogins_Lgnpwdforcechange_N ;
   }

   public void setgxTv_SdtLogins_Lgnpwdforcechange_N( byte value )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnpwdforcechange_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Lgnsuperuser_N( )
   {
      return gxTv_SdtLogins_Lgnsuperuser_N ;
   }

   public void setgxTv_SdtLogins_Lgnsuperuser_N( byte value )
   {
      gxTv_SdtLogins_Lgnsuperuser_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Lgnsuperuser_N_SetNull( )
   {
      gxTv_SdtLogins_Lgnsuperuser_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tlogins_bc obj ;
      obj = new tlogins_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtLogins_Lgnlogin = "" ;
      gxTv_SdtLogins_Lgnname = "" ;
      gxTv_SdtLogins_Lgnenable = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail = "" ;
      gxTv_SdtLogins_Lgnpwd = "" ;
      gxTv_SdtLogins_Lgnpwdtmp = "" ;
      gxTv_SdtLogins_Lgnlastlogin = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtLogins_Lgnpwdlastchanged = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtLogins_Lgnpwdforcechange = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser = (byte)(0) ;
      gxTv_SdtLogins_Lastlogininfo = new GxSilentTrnGridCollection(SdtLogins_LastLoginInfo.class, "Logins.LastLoginInfo", "IataICSI");
      gxTv_SdtLogins_Profiles = new GxSilentTrnGridCollection(SdtLogins_Profiles.class, "Logins.Profiles", "IataICSI");
      gxTv_SdtLogins_Mode = "" ;
      gxTv_SdtLogins_Lgnlogin_Z = "" ;
      gxTv_SdtLogins_Lgnname_Z = "" ;
      gxTv_SdtLogins_Lgnenable_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail_Z = "" ;
      gxTv_SdtLogins_Lgnpwd_Z = "" ;
      gxTv_SdtLogins_Lgnpwdtmp_Z = "" ;
      gxTv_SdtLogins_Lgnlastlogin_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtLogins_Lgnpwdlastchanged_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtLogins_Lgnpwdforcechange_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnname_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnenable_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwd_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnlastlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      sDateCnv = "" ;
      sNumToPad = "" ;
      return  ;
   }

   public SdtLogins Clone( )
   {
      SdtLogins sdt ;
      tlogins_bc obj ;
      sdt = (SdtLogins)(clone()) ;
      obj = (tlogins_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtLogins struct )
   {
      setgxTv_SdtLogins_Lgnlogin(struct.getLgnlogin());
      setgxTv_SdtLogins_Lgnname(struct.getLgnname());
      setgxTv_SdtLogins_Lgnenable(struct.getLgnenable());
      setgxTv_SdtLogins_Lgnemail(struct.getLgnemail());
      setgxTv_SdtLogins_Lgnpwd(struct.getLgnpwd());
      setgxTv_SdtLogins_Lgnpwdtmp(struct.getLgnpwdtmp());
      setgxTv_SdtLogins_Lgnlastlogin(struct.getLgnlastlogin());
      setgxTv_SdtLogins_Lgnpwdlastchanged(struct.getLgnpwdlastchanged());
      setgxTv_SdtLogins_Lgnpwdforcechange(struct.getLgnpwdforcechange());
      setgxTv_SdtLogins_Lgnsuperuser(struct.getLgnsuperuser());
      setgxTv_SdtLogins_Lastlogininfo(new GxSilentTrnGridCollection(SdtLogins_LastLoginInfo.class, "Logins.LastLoginInfo", "IataICSI", struct.getLastlogininfo()));
      setgxTv_SdtLogins_Profiles(new GxSilentTrnGridCollection(SdtLogins_Profiles.class, "Logins.Profiles", "IataICSI", struct.getProfiles()));
      setgxTv_SdtLogins_Mode(struct.getMode());
      setgxTv_SdtLogins_Lgnlogin_Z(struct.getLgnlogin_Z());
      setgxTv_SdtLogins_Lgnname_Z(struct.getLgnname_Z());
      setgxTv_SdtLogins_Lgnenable_Z(struct.getLgnenable_Z());
      setgxTv_SdtLogins_Lgnemail_Z(struct.getLgnemail_Z());
      setgxTv_SdtLogins_Lgnpwd_Z(struct.getLgnpwd_Z());
      setgxTv_SdtLogins_Lgnpwdtmp_Z(struct.getLgnpwdtmp_Z());
      setgxTv_SdtLogins_Lgnlastlogin_Z(struct.getLgnlastlogin_Z());
      setgxTv_SdtLogins_Lgnpwdlastchanged_Z(struct.getLgnpwdlastchanged_Z());
      setgxTv_SdtLogins_Lgnpwdforcechange_Z(struct.getLgnpwdforcechange_Z());
      setgxTv_SdtLogins_Lgnsuperuser_Z(struct.getLgnsuperuser_Z());
      setgxTv_SdtLogins_Lgnlogin_N(struct.getLgnlogin_N());
      setgxTv_SdtLogins_Lgnname_N(struct.getLgnname_N());
      setgxTv_SdtLogins_Lgnenable_N(struct.getLgnenable_N());
      setgxTv_SdtLogins_Lgnemail_N(struct.getLgnemail_N());
      setgxTv_SdtLogins_Lgnpwd_N(struct.getLgnpwd_N());
      setgxTv_SdtLogins_Lgnpwdtmp_N(struct.getLgnpwdtmp_N());
      setgxTv_SdtLogins_Lgnlastlogin_N(struct.getLgnlastlogin_N());
      setgxTv_SdtLogins_Lgnpwdlastchanged_N(struct.getLgnpwdlastchanged_N());
      setgxTv_SdtLogins_Lgnpwdforcechange_N(struct.getLgnpwdforcechange_N());
      setgxTv_SdtLogins_Lgnsuperuser_N(struct.getLgnsuperuser_N());
   }

   public StructSdtLogins getStruct( )
   {
      StructSdtLogins struct = new StructSdtLogins ();
      struct.setLgnlogin(getgxTv_SdtLogins_Lgnlogin());
      struct.setLgnname(getgxTv_SdtLogins_Lgnname());
      struct.setLgnenable(getgxTv_SdtLogins_Lgnenable());
      struct.setLgnemail(getgxTv_SdtLogins_Lgnemail());
      struct.setLgnpwd(getgxTv_SdtLogins_Lgnpwd());
      struct.setLgnpwdtmp(getgxTv_SdtLogins_Lgnpwdtmp());
      struct.setLgnlastlogin(getgxTv_SdtLogins_Lgnlastlogin());
      struct.setLgnpwdlastchanged(getgxTv_SdtLogins_Lgnpwdlastchanged());
      struct.setLgnpwdforcechange(getgxTv_SdtLogins_Lgnpwdforcechange());
      struct.setLgnsuperuser(getgxTv_SdtLogins_Lgnsuperuser());
      struct.setLastlogininfo(getgxTv_SdtLogins_Lastlogininfo().getStruct());
      struct.setProfiles(getgxTv_SdtLogins_Profiles().getStruct());
      struct.setMode(getgxTv_SdtLogins_Mode());
      struct.setLgnlogin_Z(getgxTv_SdtLogins_Lgnlogin_Z());
      struct.setLgnname_Z(getgxTv_SdtLogins_Lgnname_Z());
      struct.setLgnenable_Z(getgxTv_SdtLogins_Lgnenable_Z());
      struct.setLgnemail_Z(getgxTv_SdtLogins_Lgnemail_Z());
      struct.setLgnpwd_Z(getgxTv_SdtLogins_Lgnpwd_Z());
      struct.setLgnpwdtmp_Z(getgxTv_SdtLogins_Lgnpwdtmp_Z());
      struct.setLgnlastlogin_Z(getgxTv_SdtLogins_Lgnlastlogin_Z());
      struct.setLgnpwdlastchanged_Z(getgxTv_SdtLogins_Lgnpwdlastchanged_Z());
      struct.setLgnpwdforcechange_Z(getgxTv_SdtLogins_Lgnpwdforcechange_Z());
      struct.setLgnsuperuser_Z(getgxTv_SdtLogins_Lgnsuperuser_Z());
      struct.setLgnlogin_N(getgxTv_SdtLogins_Lgnlogin_N());
      struct.setLgnname_N(getgxTv_SdtLogins_Lgnname_N());
      struct.setLgnenable_N(getgxTv_SdtLogins_Lgnenable_N());
      struct.setLgnemail_N(getgxTv_SdtLogins_Lgnemail_N());
      struct.setLgnpwd_N(getgxTv_SdtLogins_Lgnpwd_N());
      struct.setLgnpwdtmp_N(getgxTv_SdtLogins_Lgnpwdtmp_N());
      struct.setLgnlastlogin_N(getgxTv_SdtLogins_Lgnlastlogin_N());
      struct.setLgnpwdlastchanged_N(getgxTv_SdtLogins_Lgnpwdlastchanged_N());
      struct.setLgnpwdforcechange_N(getgxTv_SdtLogins_Lgnpwdforcechange_N());
      struct.setLgnsuperuser_N(getgxTv_SdtLogins_Lgnsuperuser_N());
      return struct ;
   }

   protected byte gxTv_SdtLogins_Lgnenable ;
   protected byte gxTv_SdtLogins_Lgnpwdforcechange ;
   protected byte gxTv_SdtLogins_Lgnsuperuser ;
   protected byte gxTv_SdtLogins_Lgnenable_Z ;
   protected byte gxTv_SdtLogins_Lgnpwdforcechange_Z ;
   protected byte gxTv_SdtLogins_Lgnsuperuser_Z ;
   protected byte gxTv_SdtLogins_Lgnlogin_N ;
   protected byte gxTv_SdtLogins_Lgnname_N ;
   protected byte gxTv_SdtLogins_Lgnenable_N ;
   protected byte gxTv_SdtLogins_Lgnemail_N ;
   protected byte gxTv_SdtLogins_Lgnpwd_N ;
   protected byte gxTv_SdtLogins_Lgnpwdtmp_N ;
   protected byte gxTv_SdtLogins_Lgnlastlogin_N ;
   protected byte gxTv_SdtLogins_Lgnpwdlastchanged_N ;
   protected byte gxTv_SdtLogins_Lgnpwdforcechange_N ;
   protected byte gxTv_SdtLogins_Lgnsuperuser_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtLogins_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String sDateCnv ;
   protected String sNumToPad ;
   protected java.util.Date gxTv_SdtLogins_Lgnlastlogin ;
   protected java.util.Date gxTv_SdtLogins_Lgnpwdlastchanged ;
   protected java.util.Date gxTv_SdtLogins_Lgnlastlogin_Z ;
   protected java.util.Date gxTv_SdtLogins_Lgnpwdlastchanged_Z ;
   protected String gxTv_SdtLogins_Lgnlogin ;
   protected String gxTv_SdtLogins_Lgnname ;
   protected String gxTv_SdtLogins_Lgnemail ;
   protected String gxTv_SdtLogins_Lgnpwd ;
   protected String gxTv_SdtLogins_Lgnpwdtmp ;
   protected String gxTv_SdtLogins_Lgnlogin_Z ;
   protected String gxTv_SdtLogins_Lgnname_Z ;
   protected String gxTv_SdtLogins_Lgnemail_Z ;
   protected String gxTv_SdtLogins_Lgnpwd_Z ;
   protected String gxTv_SdtLogins_Lgnpwdtmp_Z ;
   protected GxSilentTrnGridCollection gxTv_SdtLogins_Lastlogininfo ;
   protected GxSilentTrnGridCollection gxTv_SdtLogins_Profiles ;
}


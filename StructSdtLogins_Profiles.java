
public final  class StructSdtLogins_Profiles implements Cloneable, java.io.Serializable
{
   public StructSdtLogins_Profiles( )
   {
      gxTv_SdtLogins_Profiles_Ustcode = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription = "" ;
      gxTv_SdtLogins_Profiles_Mode = "" ;
      gxTv_SdtLogins_Profiles_Modified = (short)(0) ;
      gxTv_SdtLogins_Profiles_Ustcode_Z = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription_Z = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getUstcode( )
   {
      return gxTv_SdtLogins_Profiles_Ustcode ;
   }

   public void setUstcode( String value )
   {
      gxTv_SdtLogins_Profiles_Ustcode = value ;
      return  ;
   }

   public String getUstdescription( )
   {
      return gxTv_SdtLogins_Profiles_Ustdescription ;
   }

   public void setUstdescription( String value )
   {
      gxTv_SdtLogins_Profiles_Ustdescription = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtLogins_Profiles_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtLogins_Profiles_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtLogins_Profiles_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtLogins_Profiles_Modified = value ;
      return  ;
   }

   public String getUstcode_Z( )
   {
      return gxTv_SdtLogins_Profiles_Ustcode_Z ;
   }

   public void setUstcode_Z( String value )
   {
      gxTv_SdtLogins_Profiles_Ustcode_Z = value ;
      return  ;
   }

   public String getUstdescription_Z( )
   {
      return gxTv_SdtLogins_Profiles_Ustdescription_Z ;
   }

   public void setUstdescription_Z( String value )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_Z = value ;
      return  ;
   }

   public byte getUstdescription_N( )
   {
      return gxTv_SdtLogins_Profiles_Ustdescription_N ;
   }

   public void setUstdescription_N( byte value )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_N = value ;
      return  ;
   }

   protected byte gxTv_SdtLogins_Profiles_Ustdescription_N ;
   protected short gxTv_SdtLogins_Profiles_Modified ;
   protected String gxTv_SdtLogins_Profiles_Mode ;
   protected String gxTv_SdtLogins_Profiles_Ustcode ;
   protected String gxTv_SdtLogins_Profiles_Ustdescription ;
   protected String gxTv_SdtLogins_Profiles_Ustcode_Z ;
   protected String gxTv_SdtLogins_Profiles_Ustdescription_Z ;
}


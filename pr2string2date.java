/*
               File: R2String2Date
        Description: Transforma 'YYMMDD' ou 'YYYYMMDD' em data
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2string2date extends GXProcedure
{
   public pr2string2date( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2string2date.class ), "" );
   }

   public pr2string2date( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        java.util.Date[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             java.util.Date[] aP1 )
   {
      pr2string2date.this.AV13sDate = aP0;
      pr2string2date.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV8tDate = GXutil.trim( AV13sDate) ;
      if ( ( GXutil.len( GXutil.trim( AV8tDate)) == 6 ) )
      {
         AV12i_d = (byte)(GXutil.val( GXutil.substring( AV8tDate, 5, 2), ".")) ;
         AV11i_m = (byte)(GXutil.val( GXutil.substring( AV8tDate, 3, 2), ".")) ;
         AV10i_y = (short)(2000+GXutil.val( GXutil.substring( AV8tDate, 1, 2), ".")) ;
      }
      else
      {
         AV12i_d = (byte)(GXutil.val( GXutil.substring( AV8tDate, 7, 2), ".")) ;
         AV11i_m = (byte)(GXutil.val( GXutil.substring( AV8tDate, 5, 2), ".")) ;
         AV10i_y = (short)(GXutil.val( GXutil.substring( AV8tDate, 1, 4), ".")) ;
      }
      if ( ( AV12i_d == 0 ) || ( AV11i_m == 0 ) || ( AV10i_y == 0 ) )
      {
         AV12i_d = (byte)(1) ;
         AV11i_m = (byte)(1) ;
         AV10i_y = (short)(1980) ;
      }
      AV9dDate = localUtil.ymdtod( AV10i_y, AV11i_m, AV12i_d) ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP1[0] = pr2string2date.this.AV9dDate;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9dDate = GXutil.nullDate() ;
      AV8tDate = "" ;
      AV12i_d = (byte)(0) ;
      AV11i_m = (byte)(0) ;
      AV10i_y = (short)(0) ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV12i_d ;
   private byte AV11i_m ;
   private short AV10i_y ;
   private short Gx_err ;
   private String AV13sDate ;
   private String AV8tDate ;
   private java.util.Date AV9dDate ;
   private java.util.Date[] aP1 ;
}


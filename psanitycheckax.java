/*
               File: sanitycheckAX
        Description: sanitycheck AX
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:22.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class psanitycheckax extends GXProcedure
{
   public psanitycheckax( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psanitycheckax.class ), "" );
   }

   public psanitycheckax( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String[] aP2 )
   {
      psanitycheckax.this.AV24FileNa = aP0;
      psanitycheckax.this.AV42lccbEm = aP1;
      psanitycheckax.this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV26Caminh = AV24FileNa ;
      AV18File.setSource( AV26Caminh );
      if ( AV18File.exists() )
      {
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfropen( AV26Caminh, 255, "%", "\"", "") ;
         AV28RegCou = 0 ;
         AV10ErroHe = "Header Not Found." ;
         AV11ErroTr = "Trailer Not Found." ;
         while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
         {
            GXv_char1[0] = AV9linha ;
            GXt_int2 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char1, (short)(255)) ;
            AV9linha = GXv_char1[0] ;
            AV27RetVal = GXt_int2 ;
            AV28RegCou = (int)(AV28RegCou+1) ;
            if ( ( AV28RegCou == 1 ) )
            {
               /* Execute user subroutine: S1179 */
               S1179 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               AV10ErroHe = AV15Erro ;
            }
         }
         if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV9linha))==0)) )
         {
            /* Execute user subroutine: S12127 */
            S12127 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            AV11ErroTr = AV15Erro ;
         }
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         AV25msgErr = "" ;
         if ( ! ( ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV11ErroTr))==0)) ) )
         {
            AV25msgErr = AV10ErroHe + "=ErrHeader / ErrTrailer=" + AV11ErroTr ;
            GXt_svchar3 = AV29ShortN ;
            GXv_char1[0] = AV26Caminh ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2shortname(remoteHandle, context).execute( GXv_char1, GXv_char4) ;
            psanitycheckax.this.AV26Caminh = GXv_char1[0] ;
            psanitycheckax.this.GXt_svchar3 = GXv_char4[0] ;
            AV29ShortN = GXt_svchar3 ;
            GXt_svchar3 = AV12BasePa ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getpath(remoteHandle, context).execute( AV26Caminh, GXv_char4) ;
            psanitycheckax.this.GXt_svchar3 = GXv_char4[0] ;
            AV12BasePa = GXt_svchar3 ;
            AV30SubFol = "Error\\" ;
            AV12BasePa = AV12BasePa + AV30SubFol ;
            AV19NewFil = GXutil.trim( AV12BasePa) + GXutil.trim( AV29ShortN) ;
            AV18File.setSource( AV26Caminh );
            AV18File.rename(GXutil.trim( AV19NewFil));
            AV14ShortN = GXutil.trim( AV29ShortN) + ".err" ;
            AV31Arquiv = GXutil.trim( AV12BasePa) + GXutil.trim( AV14ShortN) ;
            AV13LOGFil.openURL(AV31Arquiv);
            AV10ErroHe = AV10ErroHe + GXutil.newLine( ) ;
            AV11ErroTr = AV11ErroTr + GXutil.newLine( ) ;
            AV13LOGFil.writeRawText(AV10ErroHe);
            AV13LOGFil.writeRawText(AV11ErroTr);
            AV13LOGFil.close();
            GXt_svchar3 = AV23Subjec ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_SUBJ_AX", "Mensagem no subject do e-mail do tipo Sanity AX", "S", "Problem submission file AX", GXv_char4) ;
            psanitycheckax.this.GXt_svchar3 = GXv_char4[0] ;
            AV23Subjec = GXt_svchar3 ;
            GXt_svchar3 = AV21Body ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_BODY_AX", "Mensagem no corpo do e-mail do tipo Sanity AX", "S", "<br>The submission file [FILE] related to AX was rejected in the Sanity Check.<br><br>Please check the file.<br><br>Thanks", GXv_char4) ;
            psanitycheckax.this.GXt_svchar3 = GXv_char4[0] ;
            AV21Body = GXt_svchar3 ;
            AV21Body = GXutil.strReplace( AV21Body, "[FILE]", AV29ShortN) ;
            GXt_svchar3 = AV40To ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "", "S", "", GXv_char4) ;
            psanitycheckax.this.GXt_svchar3 = GXv_char4[0] ;
            AV40To = GXutil.trim( GXt_svchar3) ;
            GXt_svchar3 = AV22CC ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "", "S", "", GXv_char4) ;
            psanitycheckax.this.GXt_svchar3 = GXv_char4[0] ;
            AV22CC = GXutil.trim( GXt_svchar3) ;
            AV20BCC = "" ;
            GX_I = 1 ;
            while ( ( GX_I <= 5 ) )
            {
               AV8Anexos[GX_I-1] = "" ;
               GX_I = (int)(GX_I+1) ;
            }
            new penviaemail(remoteHandle, context).execute( AV23Subjec, AV21Body, AV40To, AV22CC, AV20BCC, AV8Anexos) ;
         }
      }
      cleanup();
   }

   public void S1179( )
   {
      /* 'CHECKHEADER' Routine */
      AV15Erro = "" ;
      AV17LinhaA = GXutil.strReplace( AV9linha, " ", "x") ;
      if ( ( GXutil.len( GXutil.trim( AV17LinhaA)) != 150 ) )
      {
         AV15Erro = "Header size record Not equal 150 / " ;
      }
      AV32yy = GXutil.substring( AV9linha, 12, 2) ;
      AV33mm = GXutil.substring( AV9linha, 14, 2) ;
      AV34dd = GXutil.substring( AV9linha, 16, 2) ;
      AV46val = (int)(GXutil.val( AV32yy, ".")) ;
      if ( ( AV46val > 40 ) )
      {
         AV35Nyy = (int)(GXutil.val( "19"+GXutil.trim( AV32yy), ".")) ;
      }
      else
      {
         AV35Nyy = (int)(GXutil.val( "20"+GXutil.trim( AV32yy), ".")) ;
      }
      AV36Nmm = (int)(GXutil.val( AV33mm, ".")) ;
      AV37Ndd = (int)(GXutil.val( AV34dd, ".")) ;
      AV16DataGe = localUtil.ymdtod( AV35Nyy, AV36Nmm, AV37Ndd) ;
      if ( (GXutil.nullDate().equals(AV16DataGe)) )
      {
         AV15Erro = AV15Erro + "Header Date Not Valid / " ;
      }
      AV48Data1 = GXutil.resetTime( AV16DataGe );
      AV49Data2 = GXutil.resetTime( GXutil.dadd( AV16DataGe , + ( 1 )) );
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 1), "H") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Identifier Not equal H / " ;
      }
      AV38Sequen = GXutil.substring( AV9linha, 2, 10) ;
      if ( ( GXutil.val( AV38Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Merchant Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 18, 1), "3") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Information Not equal 000 / " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 19, 6) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Invoice Not Valid / " ;
      }
      AV41TipoVe = GXutil.substring( AV9linha, 56, 1) ;
      if ( ( GXutil.val( AV41TipoVe, ".") <= 0 ) && ( GXutil.strcmp(AV41TipoVe, "0") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Type Sales Not Valid / " ;
      }
   }

   public void S12127( )
   {
      /* 'CHECKTRAILER' Routine */
      AV15Erro = "" ;
      AV17LinhaA = GXutil.strReplace( AV9linha, " ", "x") ;
      if ( ( GXutil.len( GXutil.trim( AV17LinhaA)) != 150 ) )
      {
         AV15Erro = "Trailer size record Not equal 150 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 1), "T") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Identifier Not equal T / " ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( AV15Erro))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) )
      {
         AV44vTotal = (double)(GXutil.val( GXutil.substring( AV9linha, 7, 17), ".")/ (double) (100)) ;
         AV43TotalS = 0 ;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV41TipoVe ,
                                              new Short(A1168lccbI) ,
                                              A1190lccbS ,
                                              AV48Data1 ,
                                              AV49Data2 ,
                                              A1490Distr ,
                                              new Double(A1172lccbS) ,
                                              A1184lccbS ,
                                              A1150lccbE ,
                                              AV42lccbEm ,
                                              A1227lccbO },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DOUBLE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         /* Using cursor P007P2 */
         pr_default.execute(0, new Object[] {AV48Data1, AV49Data2, AV42lccbEm});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1150lccbE = P007P2_A1150lccbE[0] ;
            A1172lccbS = P007P2_A1172lccbS[0] ;
            n1172lccbS = P007P2_n1172lccbS[0] ;
            A1168lccbI = P007P2_A1168lccbI[0] ;
            n1168lccbI = P007P2_n1168lccbI[0] ;
            A1490Distr = P007P2_A1490Distr[0] ;
            n1490Distr = P007P2_n1490Distr[0] ;
            A1190lccbS = P007P2_A1190lccbS[0] ;
            n1190lccbS = P007P2_n1190lccbS[0] ;
            A1184lccbS = P007P2_A1184lccbS[0] ;
            n1184lccbS = P007P2_n1184lccbS[0] ;
            A1227lccbO = P007P2_A1227lccbO[0] ;
            A1171lccbT = P007P2_A1171lccbT[0] ;
            n1171lccbT = P007P2_n1171lccbT[0] ;
            A1170lccbI = P007P2_A1170lccbI[0] ;
            n1170lccbI = P007P2_n1170lccbI[0] ;
            A1224lccbC = P007P2_A1224lccbC[0] ;
            A1222lccbI = P007P2_A1222lccbI[0] ;
            A1223lccbD = P007P2_A1223lccbD[0] ;
            A1225lccbC = P007P2_A1225lccbC[0] ;
            A1226lccbA = P007P2_A1226lccbA[0] ;
            A1228lccbF = P007P2_A1228lccbF[0] ;
            if ( ( GXutil.strcmp(AV41TipoVe, "1") == 0 ) )
            {
               AV43TotalS = (double)(AV43TotalS+A1172lccbS) ;
            }
            else
            {
               AV43TotalS = (double)(AV43TotalS+((A1170lccbI+A1171lccbT))) ;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV47dif1 = (double)(AV44vTotal-AV43TotalS) ;
         if ( ( AV47dif1 <= -0.005 ) || ( AV47dif1 >= 0.005 ) )
         {
            AV15Erro = AV15Erro + "Total Amount Invoice not OK: " + GXutil.trim( GXutil.str( AV44vTotal, 14, 2)) + " <> " + GXutil.trim( GXutil.str( AV43TotalS, 14, 2)) ;
         }
      }
   }

   protected void cleanup( )
   {
      this.aP2[0] = psanitycheckax.this.AV25msgErr;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25msgErr = "" ;
      AV26Caminh = "" ;
      AV18File = new com.genexus.util.GXFile();
      AV27RetVal = 0 ;
      AV28RegCou = 0 ;
      AV10ErroHe = "" ;
      AV11ErroTr = "" ;
      AV9linha = "" ;
      GXt_int2 = (short)(0) ;
      returnInSub = false ;
      AV15Erro = "" ;
      AV29ShortN = "" ;
      GXv_char1 = new String [1] ;
      AV12BasePa = "" ;
      AV30SubFol = "" ;
      AV19NewFil = "" ;
      AV14ShortN = "" ;
      AV31Arquiv = "" ;
      AV13LOGFil = new com.genexus.xml.XMLWriter();
      AV23Subjec = "" ;
      AV21Body = "" ;
      AV40To = "" ;
      AV22CC = "" ;
      GXv_char4 = new String [1] ;
      AV20BCC = "" ;
      GX_I = 0 ;
      AV8Anexos = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV8Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV17LinhaA = "" ;
      AV32yy = "" ;
      AV33mm = "" ;
      AV34dd = "" ;
      AV46val = 0 ;
      AV35Nyy = 0 ;
      GXt_svchar3 = "" ;
      AV36Nmm = 0 ;
      AV37Ndd = 0 ;
      AV16DataGe = GXutil.nullDate() ;
      AV48Data1 = GXutil.resetTime( GXutil.nullDate() );
      AV49Data2 = GXutil.resetTime( GXutil.nullDate() );
      AV38Sequen = "" ;
      AV39Sequen = "" ;
      AV41TipoVe = "" ;
      AV44vTotal = 0 ;
      AV43TotalS = 0 ;
      scmdbuf = "" ;
      A1168lccbI = (short)(0) ;
      A1190lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1490Distr = "" ;
      A1172lccbS = 0 ;
      A1184lccbS = "" ;
      A1150lccbE = "" ;
      A1227lccbO = "" ;
      P007P2_A1150lccbE = new String[] {""} ;
      P007P2_A1172lccbS = new double[1] ;
      P007P2_n1172lccbS = new boolean[] {false} ;
      P007P2_A1168lccbI = new short[1] ;
      P007P2_n1168lccbI = new boolean[] {false} ;
      P007P2_A1490Distr = new String[] {""} ;
      P007P2_n1490Distr = new boolean[] {false} ;
      P007P2_A1190lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P007P2_n1190lccbS = new boolean[] {false} ;
      P007P2_A1184lccbS = new String[] {""} ;
      P007P2_n1184lccbS = new boolean[] {false} ;
      P007P2_A1227lccbO = new String[] {""} ;
      P007P2_A1171lccbT = new double[1] ;
      P007P2_n1171lccbT = new boolean[] {false} ;
      P007P2_A1170lccbI = new double[1] ;
      P007P2_n1170lccbI = new boolean[] {false} ;
      P007P2_A1224lccbC = new String[] {""} ;
      P007P2_A1222lccbI = new String[] {""} ;
      P007P2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007P2_A1225lccbC = new String[] {""} ;
      P007P2_A1226lccbA = new String[] {""} ;
      P007P2_A1228lccbF = new String[] {""} ;
      n1172lccbS = false ;
      n1168lccbI = false ;
      n1490Distr = false ;
      n1190lccbS = false ;
      n1184lccbS = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1224lccbC = "" ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1228lccbF = "" ;
      AV47dif1 = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new psanitycheckax__default(),
         new Object[] {
             new Object[] {
            P007P2_A1150lccbE, P007P2_A1172lccbS, P007P2_n1172lccbS, P007P2_A1168lccbI, P007P2_n1168lccbI, P007P2_A1490Distr, P007P2_n1490Distr, P007P2_A1190lccbS, P007P2_n1190lccbS, P007P2_A1184lccbS,
            P007P2_n1184lccbS, P007P2_A1227lccbO, P007P2_A1171lccbT, P007P2_n1171lccbT, P007P2_A1170lccbI, P007P2_n1170lccbI, P007P2_A1224lccbC, P007P2_A1222lccbI, P007P2_A1223lccbD, P007P2_A1225lccbC,
            P007P2_A1226lccbA, P007P2_A1228lccbF
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short GXt_int2 ;
   private short A1168lccbI ;
   private short Gx_err ;
   private int AV28RegCou ;
   private int GX_I ;
   private int AV46val ;
   private int AV35Nyy ;
   private int AV36Nmm ;
   private int AV37Ndd ;
   private long AV27RetVal ;
   private double AV44vTotal ;
   private double AV43TotalS ;
   private double A1172lccbS ;
   private double A1171lccbT ;
   private double A1170lccbI ;
   private double AV47dif1 ;
   private String AV42lccbEm ;
   private String GXv_char1[] ;
   private String AV12BasePa ;
   private String GXv_char4[] ;
   private String AV32yy ;
   private String AV33mm ;
   private String AV34dd ;
   private String AV41TipoVe ;
   private String scmdbuf ;
   private String A1490Distr ;
   private String A1184lccbS ;
   private String A1150lccbE ;
   private String A1227lccbO ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1228lccbF ;
   private java.util.Date AV48Data1 ;
   private java.util.Date AV49Data2 ;
   private java.util.Date A1190lccbS ;
   private java.util.Date AV16DataGe ;
   private java.util.Date A1223lccbD ;
   private boolean returnInSub ;
   private boolean n1172lccbS ;
   private boolean n1168lccbI ;
   private boolean n1490Distr ;
   private boolean n1190lccbS ;
   private boolean n1184lccbS ;
   private boolean n1171lccbT ;
   private boolean n1170lccbI ;
   private String AV24FileNa ;
   private String AV25msgErr ;
   private String AV26Caminh ;
   private String AV10ErroHe ;
   private String AV11ErroTr ;
   private String AV9linha ;
   private String AV15Erro ;
   private String AV29ShortN ;
   private String AV30SubFol ;
   private String AV19NewFil ;
   private String AV14ShortN ;
   private String AV31Arquiv ;
   private String AV23Subjec ;
   private String AV21Body ;
   private String AV40To ;
   private String AV22CC ;
   private String AV20BCC ;
   private String AV8Anexos[] ;
   private String AV17LinhaA ;
   private String GXt_svchar3 ;
   private String AV38Sequen ;
   private String AV39Sequen ;
   private com.genexus.xml.XMLWriter AV13LOGFil ;
   private com.genexus.util.GXFile AV18File ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P007P2_A1150lccbE ;
   private double[] P007P2_A1172lccbS ;
   private boolean[] P007P2_n1172lccbS ;
   private short[] P007P2_A1168lccbI ;
   private boolean[] P007P2_n1168lccbI ;
   private String[] P007P2_A1490Distr ;
   private boolean[] P007P2_n1490Distr ;
   private java.util.Date[] P007P2_A1190lccbS ;
   private boolean[] P007P2_n1190lccbS ;
   private String[] P007P2_A1184lccbS ;
   private boolean[] P007P2_n1184lccbS ;
   private String[] P007P2_A1227lccbO ;
   private double[] P007P2_A1171lccbT ;
   private boolean[] P007P2_n1171lccbT ;
   private double[] P007P2_A1170lccbI ;
   private boolean[] P007P2_n1170lccbI ;
   private String[] P007P2_A1224lccbC ;
   private String[] P007P2_A1222lccbI ;
   private java.util.Date[] P007P2_A1223lccbD ;
   private String[] P007P2_A1225lccbC ;
   private String[] P007P2_A1226lccbA ;
   private String[] P007P2_A1228lccbF ;
}

final  class psanitycheckax__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P007P2( String AV41TipoVe ,
                                        short A1168lccbI ,
                                        java.util.Date A1190lccbS ,
                                        java.util.Date AV48Data1 ,
                                        java.util.Date AV49Data2 ,
                                        String A1490Distr ,
                                        double A1172lccbS ,
                                        String A1184lccbS ,
                                        String A1150lccbE ,
                                        String AV42lccbEm ,
                                        String A1227lccbO )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT [lccbEmpCod], [lccbSaleAmount], [lccbInstallments], [DistribuicaoTransacoesTipo]," ;
      scmdbuf = scmdbuf + " [lccbSubTime], [lccbStatus], [lccbOpCode], [lccbTip], [lccbInstAmount], [lccbCCard]," ;
      scmdbuf = scmdbuf + " [lccbIATA], [lccbDate], [lccbCCNum], [lccbAppCode], [lccbFPAC_PLP] FROM [LCCBPLP]" ;
      scmdbuf = scmdbuf + " WITH (NOLOCK)" ;
      scmdbuf = scmdbuf + " WHERE ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and ([lccbSubTime] >= " + (AV48Data1.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.ttoc( AV48Data1, 10, 8, 0, 0, "-", ":", " ")+"',120)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      scmdbuf = scmdbuf + " and ([lccbSubTime] <= " + (AV49Data2.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.ttoc( AV49Data2, 10, 8, 0, 0, "-", ":", " ")+"',120)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      scmdbuf = scmdbuf + " and ([DistribuicaoTransacoesTipo] = 'AAX' or [DistribuicaoTransacoesTipo] = 'AHC')" ;
      scmdbuf = scmdbuf + " and ([lccbSaleAmount] > 0.00)" ;
      scmdbuf = scmdbuf + " and ([lccbStatus] = 'PROCPLP')" ;
      scmdbuf = scmdbuf + " and ([lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV42lccbEm, "'", "''")) + "')" ;
      sWhereString = "" ;
      if ( ( GXutil.strcmp(AV41TipoVe, "1") == 0 ) )
      {
         sWhereString = sWhereString + " and ([lccbInstallments] <= 1)" ;
      }
      if ( ( GXutil.strcmp(AV41TipoVe, "1") != 0 ) )
      {
         sWhereString = sWhereString + " and ([lccbInstallments] > 1)" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY [lccbOpCode], [lccbCCard], [lccbStatus]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 0 :
                  return conditional_P007P2( (String)dynConstraints[0] , ((Number) dynConstraints[1]).shortValue() , (java.util.Date)dynConstraints[2] , (java.util.Date)dynConstraints[3] , (java.util.Date)dynConstraints[4] , (String)dynConstraints[5] , ((Number) dynConstraints[6]).doubleValue() , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007P2", "scmdbuf",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((double[]) buf[1])[0] = rslt.getDouble(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((short[]) buf[3])[0] = rslt.getShort(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getString(4, 4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDateTime(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getString(6, 8) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(7, 1) ;
               ((double[]) buf[12])[0] = rslt.getDouble(8) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((double[]) buf[14])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(10, 2) ;
               ((String[]) buf[17])[0] = rslt.getString(11, 7) ;
               ((java.util.Date[]) buf[18])[0] = rslt.getGXDate(12) ;
               ((String[]) buf[19])[0] = rslt.getString(13, 44) ;
               ((String[]) buf[20])[0] = rslt.getString(14, 20) ;
               ((String[]) buf[21])[0] = rslt.getString(15, 19) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

}


/*
               File: RETInsert
        Description: Insere RET na tabela HOT (Main)
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: March 27, 2014 11:43:44.6
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pretinsert extends GXProcedure
{
   public pretinsert( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretinsert.class ), "" );
   }

   public pretinsert( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pretinsert.this.AV176FileS = aP0[0];
      this.aP0 = aP0;
      pretinsert.this.AV39DebugM = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV178Filen ;
      GXv_char2[0] = AV176FileS ;
      GXv_char3[0] = GXt_char1 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char2, GXv_char3) ;
      pretinsert.this.AV176FileS = GXv_char2[0] ;
      pretinsert.this.GXt_char1 = GXv_char3[0] ;
      AV178Filen = GXt_char1 ;
      AV153Versa = "10021" ;
      AV39DebugM = GXutil.trim( GXutil.upper( AV39DebugM)) ;
      context.msgStatus( "RETInsert - Version "+AV153Versa );
      context.msgStatus( "  Running mode: ["+AV39DebugM+"] - Started at "+GXutil.time( ) );
      AV210TACNO = (byte)(0) ;
      AV142s = AV39DebugM ;
      AV78i = GXutil.strSearch( AV142s, "ONLY", 1) ;
      while ( ( AV78i > 0 ) )
      {
         AV210TACNO = (byte)(AV210TACNO+1) ;
         AV78i = (int)(AV78i+4) ;
         AV211TACNO[AV210TACNO-1] = GXutil.substring( AV142s, AV78i, 3) ;
         AV78i = (int)(AV78i+3) ;
         AV142s = GXutil.substring( AV142s, AV78i, 200) ;
         AV78i = GXutil.strSearch( AV142s, "ONLY", 1) ;
      }
      if ( ( AV210TACNO > 0 ) )
      {
         AV142s = "" ;
         AV78i = 1 ;
         while ( ( AV78i <= AV210TACNO ) )
         {
            AV142s = AV142s + AV211TACNO[AV78i-1] + " " ;
            AV78i = (int)(AV78i+1) ;
         }
      }
      else
      {
         AV142s = "<All>" ;
      }
      context.msgStatus( "    Records inserted for: "+AV142s );
      /* Execute user subroutine: S1144 */
      S1144 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S1144( )
   {
      /* 'MAIN' Routine */
      if ( ( GXutil.strSearch( AV39DebugM, "NOINSERT", 1) == 0 ) )
      {
         /* Execute user subroutine: S121 */
         S121 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S121( )
   {
      /* 'INSEREHOT' Routine */
      AV11HOT0_i = 0 ;
      AV10HOT0_D = 0 ;
      AV75HOT1_I = 0 ;
      AV74HOT1_D = 0 ;
      AV77HOT2_I = 0 ;
      AV76HOT2_D = 0 ;
      AV188HOT3_ = 0 ;
      AV189HOT3_ = 0 ;
      AV198HOT4_ = 0 ;
      AV199HOT4_ = 0 ;
      AV190HOT5_ = 0 ;
      AV191HOT5_ = 0 ;
      AV169TRNN_ = "000000" ;
      /* Using cursor P005W2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A994HntLin = P005W2_A994HntLin[0] ;
         n994HntLin = P005W2_n994HntLin[0] ;
         A993HntUsu = P005W2_A993HntUsu[0] ;
         n993HntUsu = P005W2_n993HntUsu[0] ;
         A997HntSeq = P005W2_A997HntSeq[0] ;
         A998HntSub = P005W2_A998HntSub[0] ;
         AV12Linha = A994HntLin ;
         AV64HntSeq = A997HntSeq ;
         AV127RCID = GXutil.substring( AV12Linha, 1, 1) ;
         AV168TRNN_ = GXutil.substring( AV12Linha, 2, 6) ;
         if ( ( GXutil.strcmp(AV169TRNN_, AV168TRNN_) != 0 ) )
         {
            /* Execute user subroutine: S132 */
            S132 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
         }
         if ( ( GXutil.strcmp(AV127RCID, "1") == 0 ) )
         {
            AV35CRS = GXutil.substring( AV12Linha, 8, 4) ;
            AV148SPED = GXutil.substring( AV12Linha, 2, 6) ;
            context.msgStatus( "  Processing File "+AV35CRS+"-"+AV148SPED );
         }
         else if ( ( GXutil.strcmp(AV127RCID, "2") == 0 ) && ( AV54Flag_I == 0 ) )
         {
            AV54Flag_I = (byte)(1) ;
            AV165TRNC = GXutil.substring( AV12Linha, 48, 4) ;
            AV8AGTN = GXutil.substring( AV12Linha, 8, 7) ;
            AV158TDNR = GXutil.substring( AV12Linha, 35, 10) ;
            AV175TKT_D = GXutil.substring( AV12Linha, 23, 6) ;
            AV122PXNM = GXutil.substring( AV12Linha, 75, 49) ;
            AV174TKT_C = GXutil.substring( AV12Linha, 19, 4) ;
            AV206CPUI = GXutil.substring( AV12Linha, 19, 4) ;
            AV152TACN = GXutil.substring( AV12Linha, 68, 5) ;
            AV214ISOC = GXutil.substring( AV12Linha, 247, 2) ;
            if ( ( GXutil.strSearch( AV39DebugM, "ISOC", 1) == 0 ) )
            {
               AV214ISOC = "" ;
            }
            if ( ( AV210TACNO > 0 ) )
            {
               AV54Flag_I = (byte)(0) ;
               AV78i = 1 ;
               while ( ( AV78i <= AV210TACNO ) )
               {
                  if ( ( GXutil.strcmp(AV211TACNO[AV78i-1], GXutil.substring( AV152TACN, 1, 3)) == 0 ) )
                  {
                     AV54Flag_I = (byte)(1) ;
                     if (true) break;
                  }
                  AV78i = (int)(AV78i+1) ;
               }
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "3") == 0 ) )
         {
            AV78i = 8 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 28 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 48 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 68 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 88 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 108 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 128 ;
            /* Execute user subroutine: S142 */
            S142 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "4") == 0 ) )
         {
            AV161TOUR = GXutil.substring( AV12Linha, 72, 15) ;
            AV114ORIN = GXutil.substring( AV12Linha, 40, 32) ;
            AV174TKT_C = AV174TKT_C + " " + GXutil.substring( AV12Linha, 35, 1) ;
            AV182INLS = GXutil.substring( AV12Linha, 35, 4) ;
         }
         else if ( ( GXutil.strcmp(AV127RCID, "5") == 0 ) )
         {
            if ( ( AV55Flag_I == 0 ) )
            {
               AV55Flag_I = (byte)(1) ;
               AV192Count = (short)(0) ;
               AV86IT05po = AV64HntSeq ;
               AV157TDAM = GXutil.substring( AV12Linha, 87, 11) ;
               AV34CORT = GXutil.substring( AV12Linha, 177, 5) ;
               AV28COAM = GXutil.substring( AV12Linha, 182, 11) ;
               AV36CUTP = GXutil.substring( AV12Linha, 98, 4) ;
            }
            AV78i = 30 ;
            /* Execute user subroutine: S152 */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 49 ;
            /* Execute user subroutine: S152 */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 68 ;
            /* Execute user subroutine: S152 */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 114 ;
            /* Execute user subroutine: S152 */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 133 ;
            /* Execute user subroutine: S152 */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 152 ;
            /* Execute user subroutine: S152 */
            S152 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "6") == 0 ) )
         {
            AV78i = 0 ;
            while ( ( AV78i <= 2 ) )
            {
               AV213j = (int)(8+AV78i*80) ;
               if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, AV213j, 5)), "") != 0 ) && ( AV9Count_I < 40 ) )
               {
                  AV9Count_I = (byte)(AV9Count_I+1) ;
                  AV19IT06_O[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 5) ;
                  AV213j = (int)(13+AV78i*80) ;
                  AV14IT06_D[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 5) ;
                  AV213j = (int)(34+AV78i*80) ;
                  AV13IT06_C[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 4) ;
                  AV213j = (int)(39+AV78i*80) ;
                  AV205IT06_[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 2) ;
                  AV213j = (int)(41+AV78i*80) ;
                  AV17IT06_F[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 5) ;
                  AV213j = (int)(56+AV78i*80) ;
                  AV16IT06_F[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 15) ;
                  AV213j = (int)(71+AV78i*80) ;
                  AV18IT06_F[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 5) ;
                  AV213j = (int)(84+AV78i*80) ;
                  AV15IT06_F[AV9Count_I-1] = GXutil.substring( AV12Linha, AV213j, 2) ;
               }
               AV78i = (int)(AV78i+1) ;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "7") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV165TRNC, 1, 3), "TKT") == 0 ) )
         {
            if ( ( AV184Flag_ == 0 ) )
            {
               AV184Flag_ = (byte)(1) ;
               AV187Count = (byte)(0) ;
               AV183FCMI = GXutil.substring( AV12Linha, 184, 1) ;
            }
            AV78i = 8 ;
            /* Execute user subroutine: S162 */
            S162 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV78i = 96 ;
            /* Execute user subroutine: S162 */
            S162 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "8") == 0 ) )
         {
            if ( ( AV57Flag_I == 0 ) )
            {
               AV61FPTP = GXutil.substring( AV12Linha, 50, 2) ;
               AV57Flag_I = (byte)(1) ;
            }
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( A997HntSeq );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( (byte)(1) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXutil.substring( AV12Linha, 8, 19) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXutil.substring( AV12Linha, 27, 11) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( GXutil.substring( AV12Linha, 38, 6) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( GXutil.substring( AV12Linha, 44, 4) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( GXutil.substring( AV12Linha, 48, 2) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXutil.substring( AV12Linha, 50, 4) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( GXutil.substring( AV12Linha, 60, 4) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( GXutil.substring( AV12Linha, 64, 27) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( GXutil.substring( AV12Linha, 91, 1) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( GXutil.substring( AV12Linha, 92, 2) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( GXutil.substring( AV12Linha, 94, 1) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( GXutil.substring( AV12Linha, 95, 25) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( GXutil.substring( AV12Linha, 120, 11) );
            if ( ( GXutil.strcmp(GXutil.trim( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp()), "") != 0 ) )
            {
               AV180retfo.add(AV181retfo.Clone(), 0);
               AV179Count = (byte)(AV179Count+1) ;
            }
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( A997HntSeq );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( (byte)(2) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXutil.substring( AV12Linha, 131, 19) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXutil.substring( AV12Linha, 150, 11) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( GXutil.substring( AV12Linha, 161, 6) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( GXutil.substring( AV12Linha, 167, 4) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( GXutil.substring( AV12Linha, 171, 2) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXutil.substring( AV12Linha, 173, 4) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( GXutil.substring( AV12Linha, 183, 4) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( GXutil.substring( AV12Linha, 187, 27) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( GXutil.substring( AV12Linha, 214, 1) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( GXutil.substring( AV12Linha, 215, 2) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( GXutil.substring( AV12Linha, 217, 1) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( GXutil.substring( AV12Linha, 218, 25) );
            AV181retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( GXutil.substring( AV12Linha, 243, 11) );
            if ( ( GXutil.strcmp(GXutil.trim( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp()), "") != 0 ) )
            {
               AV180retfo.add(AV181retfo.Clone(), 0);
               AV179Count = (byte)(AV179Count+1) ;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "9") == 0 ) )
         {
            if ( ( AV208Flag_ == 0 ) )
            {
               AV207ENRS = GXutil.trim( GXutil.substring( AV12Linha, 8, 147)) ;
               AV208Flag_ = (byte)(1) ;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "A") == 0 ) )
         {
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Rfid_it0a( GXutil.substring( AV12Linha, 8, 86) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Bera_it0a( GXutil.substring( AV12Linha, 94, 13) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Icdn_it0a( GXutil.substring( AV12Linha, 107, 15) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Cdgt_it0a( (byte)(GXutil.val( GXutil.substring( AV12Linha, 122, 1), ".")) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Amil_it0a( GXutil.substring( AV12Linha, 123, 72) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Rfic_it0a( GXutil.substring( AV12Linha, 195, 1) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Mpoc_it0a( GXutil.substring( AV12Linha, 196, 11) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Mpeq_it0a( GXutil.substring( AV12Linha, 207, 11) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Mpev_it0a( GXutil.substring( AV12Linha, 218, 11) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Mpsc_it0a( GXutil.substring( AV12Linha, 229, 11) );
            AV224IT0A_.setgxTv_SdtIT0A_IT0AItem_Mptx_it0a( GXutil.substring( AV12Linha, 240, 11) );
            AV225IT0A.add(AV224IT0A_.Clone(), 0);
         }
         else if ( ( GXutil.strcmp(AV127RCID, "B") == 0 ) )
         {
            AV226OAAI_ = GXutil.substring( AV12Linha, 8, 88) ;
         }
         else if ( ( GXutil.strcmp(AV127RCID, "C") == 0 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV12Linha, 9, 1), "") != 0 ) )
            {
               AV227IT0C_.setgxTv_SdtIT0C_IT0CItem_Plid_it0c( GXutil.substring( AV12Linha, 9, 1) );
               AV227IT0C_.setgxTv_SdtIT0C_IT0CItem_Pltx_it0c( GXutil.substring( AV12Linha, 10, 86) );
               AV228IT0C.add(AV227IT0C_.Clone(), 0);
            }
            if ( ( GXutil.strcmp(GXutil.substring( AV12Linha, 96, 1), "") != 0 ) )
            {
               AV227IT0C_.setgxTv_SdtIT0C_IT0CItem_Plid_it0c( GXutil.substring( AV12Linha, 96, 1) );
               AV227IT0C_.setgxTv_SdtIT0C_IT0CItem_Pltx_it0c( GXutil.substring( AV12Linha, 97, 86) );
               AV228IT0C.add(AV227IT0C_.Clone(), 0);
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "D") == 0 ) )
         {
            AV229IT0D_.setgxTv_SdtIT0D_IT0DItem_Cpnr_it0d( (byte)(GXutil.val( GXutil.substring( AV12Linha, 9, 1), ".")) );
            AV229IT0D_.setgxTv_SdtIT0D_IT0DItem_Plid_it0d( GXutil.substring( AV12Linha, 10, 1) );
            AV229IT0D_.setgxTv_SdtIT0D_IT0DItem_Pltx_it0d( GXutil.substring( AV12Linha, 11, 86) );
            AV230IT0D.add(AV229IT0D_.Clone(), 0);
            if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, 97, 1)), "") != 0 ) )
            {
               AV229IT0D_.setgxTv_SdtIT0D_IT0DItem_Cpnr_it0d( (byte)(GXutil.val( GXutil.substring( AV12Linha, 9, 1), ".")) );
               AV229IT0D_.setgxTv_SdtIT0D_IT0DItem_Plid_it0d( GXutil.substring( AV12Linha, 97, 1) );
               AV229IT0D_.setgxTv_SdtIT0D_IT0DItem_Pltx_it0d( GXutil.substring( AV12Linha, 98, 86) );
               AV230IT0D.add(AV229IT0D_.Clone(), 0);
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "E") == 0 ) )
         {
            AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Plid_it0e( GXutil.substring( AV12Linha, 8, 1) );
            AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Spin_it0e( GXutil.substring( AV12Linha, 9, 34) );
            AV232IT0E.add(AV231IT0E_.Clone(), 0);
            if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, 43, 1)), "") != 0 ) )
            {
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Plid_it0e( GXutil.substring( AV12Linha, 43, 1) );
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Spin_it0e( GXutil.substring( AV12Linha, 44, 34) );
               AV232IT0E.add(AV231IT0E_.Clone(), 0);
            }
            if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, 43, 1)), "") != 0 ) )
            {
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Plid_it0e( GXutil.substring( AV12Linha, 78, 1) );
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Spin_it0e( GXutil.substring( AV12Linha, 79, 34) );
               AV232IT0E.add(AV231IT0E_.Clone(), 0);
            }
            if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, 113, 1)), "") != 0 ) )
            {
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Plid_it0e( GXutil.substring( AV12Linha, 113, 1) );
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Spin_it0e( GXutil.substring( AV12Linha, 114, 34) );
               AV232IT0E.add(AV231IT0E_.Clone(), 0);
            }
            if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, 148, 1)), "") != 0 ) )
            {
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Plid_it0e( GXutil.substring( AV12Linha, 148, 1) );
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Spin_it0e( GXutil.substring( AV12Linha, 149, 34) );
               AV232IT0E.add(AV231IT0E_.Clone(), 0);
            }
            if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, 183, 1)), "") != 0 ) )
            {
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Plid_it0e( GXutil.substring( AV12Linha, 183, 1) );
               AV231IT0E_.setgxTv_SdtIT0E_IT0EItem_Spin_it0e( GXutil.substring( AV12Linha, 184, 34) );
               AV232IT0E.add(AV231IT0E_.Clone(), 0);
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "G") == 0 ) )
         {
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emcp_it0g( (byte)(GXutil.val( GXutil.substring( AV12Linha, 8, 1), ".")) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emcv_it0g( (long)(GXutil.val( GXutil.substring( AV12Linha, 9, 11), ".")) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Cutp_it0g( GXutil.substring( AV12Linha, 20, 4) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emrt_it0g( GXutil.substring( AV12Linha, 24, 15) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emrc_it0g( (byte)(GXutil.val( GXutil.substring( AV12Linha, 39, 1), ".")) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emsc_it0g( GXutil.substring( AV12Linha, 40, 3) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emoc_it0g( GXutil.substring( AV12Linha, 43, 2) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Xboa_it0g( GXutil.substring( AV12Linha, 45, 1) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Xbru_it0g( GXutil.substring( AV12Linha, 46, 12) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Xbne_it0g( GXutil.substring( AV12Linha, 58, 12) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emrm_it0g( GXutil.substring( AV12Linha, 70, 70) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Emci_it0g( GXutil.substring( AV12Linha, 140, 1) );
            AV233IT0G_.setgxTv_SdtIT0G_IT0GItem_Xbct_it0g( GXutil.substring( AV12Linha, 141, 3) );
            AV234IT0G.add(AV233IT0G_.Clone(), 0);
         }
         if ( ( GXutil.strcmp(AV127RCID, "Z") == 0 ) )
         {
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
   }

   public void S142( )
   {
      /* 'IT03_RELATED' Routine */
      AV213j = (int)(AV78i+4) ;
      if ( ( AV197Count < 10 ) && ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, AV213j, 15)), "") != 0 ) )
      {
         AV197Count = (byte)(AV197Count+1) ;
         AV202IT03_[AV197Count-1] = GXutil.substring( AV12Linha, 148, 6) ;
         AV203IT03_[AV197Count-1] = GXutil.substring( AV12Linha, 154, 15) ;
         AV201IT03_[AV197Count-1] = GXutil.trim( GXutil.substring( AV12Linha, AV78i, 4)) ;
         AV78i = (int)(AV78i+4) ;
         AV200IT03_[AV197Count-1] = GXutil.trim( GXutil.substring( AV12Linha, AV78i, 15)) ;
      }
   }

   public void S152( )
   {
      /* 'IT05_TAXES' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, AV78i, 2)), "") != 0 ) && ( AV192Count < 100 ) )
      {
         AV192Count = (short)(AV192Count+1) ;
         AV160Tax_c[AV192Count-1] = GXutil.substring( AV12Linha, AV78i, 2) ;
         /* Execute user subroutine: S171 */
         S171 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV78i = (int)(AV78i+8) ;
         AV193Tax_A[AV192Count-1] = (double)(GXutil.val( GXutil.substring( AV12Linha, AV78i, 11), ".")/ (double) (AV194Fator)) ;
         if ( ( GXutil.strcmp(AV160Tax_c[AV192Count-1], "XT") != 0 ) )
         {
            AV195TAX_1 = (double)(AV195TAX_1+(AV193Tax_A[AV192Count-1])) ;
         }
      }
   }

   public void S162( )
   {
      /* 'IT07_FARECALC' Routine */
      if ( ( AV187Count < 10 ) && ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV12Linha, AV78i, 87)), "") != 0 ) )
      {
         AV187Count = (byte)(AV187Count+1) ;
         AV185FRCA[AV187Count-1] = GXutil.substring( AV12Linha, AV78i, 87) ;
      }
   }

   public void S132( )
   {
      /* 'CHECKTRN' Routine */
      if ( ( AV54Flag_I == 1 ) )
      {
         AV213j = (int)(GXutil.val( GXutil.substring( AV148SPED, 5, 2), ".")) ;
         if ( ( AV213j <= 10 ) )
         {
            AV119PER_N = GXutil.substring( AV148SPED, 1, 4) + "1" ;
         }
         else if ( ( AV213j <= 20 ) )
         {
            AV119PER_N = GXutil.substring( AV148SPED, 1, 4) + "2" ;
         }
         else
         {
            AV119PER_N = GXutil.substring( AV148SPED, 1, 4) + "3" ;
         }
         AV29CODE = AV165TRNC ;
         AV81IATA = GXutil.substring( AV8AGTN, 1, 7) ;
         AV108NUM_B = AV158TDNR ;
         /* Execute user subroutine: S181 */
         S181 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(AV221FileD, "N") == 0 ) )
         {
            if ( ( GXutil.strcmp(AV61FPTP, "") == 0 ) )
            {
               AV159TIPO_ = "XX" ;
            }
            else
            {
               AV159TIPO_ = GXutil.substring( AV61FPTP, 1, 2) ;
            }
            /* Execute user subroutine: S171 */
            S171 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            GXt_date5 = AV38DATA ;
            GXv_date6[0] = GXt_date5 ;
            new pr2string2date(remoteHandle, context).execute( AV175TKT_D, GXv_date6) ;
            pretinsert.this.GXt_date5 = GXv_date6[0] ;
            AV38DATA = GXt_date5 ;
            AV196TAX_2 = (double)(GXutil.val( AV157TDAM, ".")/ (double) (AV194Fator)) ;
            AV172VALOR = (double)(AV196TAX_2-AV195TAX_1) ;
            AV31COMMIS = (double)(GXutil.val( AV34CORT, ".")/ (double) (AV194Fator)) ;
            AV98LIT_CO = (double)(-1*GXutil.val( AV28COAM, ".")/ (double) (AV194Fator)) ;
            AV162TOUR_ = AV161TOUR ;
            AV118PAX_N = AV122PXNM ;
            AV164TRANS = AV174TKT_C ;
            AV120PLP_D = AV35CRS ;
            AV83ISSUE_ = AV114ORIN ;
            AV99MOEDA = AV36CUTP ;
            AV109NUM_B = (long)(GXutil.val( AV158TDNR, ".")) ;
            AV26CiaCod = AV152TACN ;
            AV212AIRPT = 0 ;
            AV78i = 1 ;
            while ( ( AV78i <= AV180retfo.size() ) )
            {
               if ( ( GXutil.strcmp(GXutil.substring( ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV180retfo.elementAt(-1+AV78i)).getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), 1, 2), "CC") == 0 ) || ( GXutil.strcmp(GXutil.substring( ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV180retfo.elementAt(-1+AV78i)).getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), 1, 4), "MSCC") == 0 ) )
               {
                  AV181retfo = ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV180retfo.elementAt(-1+AV78i)).Clone() ;
                  AV212AIRPT = (double)(AV212AIRPT+(GXutil.val( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam(), ".")/ (double) (AV194Fator))) ;
               }
               AV78i = (int)(AV78i+1) ;
            }
            /* Execute user subroutine: S191 */
            S191 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV78i = 1 ;
            while ( ( AV78i <= AV9Count_I ) )
            {
               AV82ISeq = AV78i ;
               AV110Num_B = (long)(GXutil.val( AV158TDNR, ".")) ;
               AV79I_From = AV19IT06_O[AV78i-1] ;
               AV80I_To = AV14IT06_D[AV78i-1] ;
               AV24Carrie = AV13IT06_C[AV78i-1] ;
               AV156Tarif = AV16IT06_F[AV78i-1] ;
               AV59Flight = AV18IT06_F[AV78i-1] ;
               AV27Claxss = AV205IT06_[AV78i-1] ;
               AV58Fli_Da = AV17IT06_F[AV78i-1] ;
               /* Execute user subroutine: S201 */
               S201 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV78i = (int)(AV78i+1) ;
            }
            AV78i = 1 ;
            while ( ( AV78i <= AV180retfo.size() ) )
            {
               AV181retfo = ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV180retfo.elementAt(-1+AV78i)).Clone() ;
               /* Execute user subroutine: S211 */
               S211 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV78i = (int)(AV78i+1) ;
            }
            AV78i = 1 ;
            while ( ( AV78i <= AV192Count ) )
            {
               /* Execute user subroutine: S221 */
               S221 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV78i = (int)(AV78i+1) ;
            }
            AV78i = 1 ;
            while ( ( AV78i <= AV197Count ) )
            {
               /* Execute user subroutine: S231 */
               S231 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV78i = (int)(AV78i+1) ;
            }
            AV78i = 1 ;
            while ( ( AV78i <= AV187Count ) )
            {
               /* Execute user subroutine: S241 */
               S241 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV78i = (int)(AV78i+1) ;
            }
            /* Execute user subroutine: S251 */
            S251 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         AV169TRNN_ = AV168TRNN_ ;
         AV54Flag_I = (byte)(0) ;
         AV55Flag_I = (byte)(0) ;
         AV56Flag_I = (byte)(0) ;
         AV184Flag_ = (byte)(0) ;
         AV57Flag_I = (byte)(0) ;
         AV208Flag_ = (byte)(0) ;
         AV197Count = (byte)(0) ;
         AV192Count = (short)(0) ;
         AV9Count_I = (byte)(0) ;
         AV187Count = (byte)(0) ;
         AV179Count = (byte)(0) ;
         AV180retfo.clear();
         AV61FPTP = "" ;
         AV207ENRS = "" ;
         AV172VALOR = 0 ;
         AV195TAX_1 = 0 ;
         AV196TAX_2 = 0 ;
         AV31COMMIS = 0 ;
         AV98LIT_CO = 0 ;
         AV162TOUR_ = "" ;
         AV118PAX_N = "" ;
         AV164TRANS = "" ;
         AV83ISSUE_ = "" ;
         AV99MOEDA = "" ;
         AV36CUTP = "" ;
         AV109NUM_B = 0 ;
         AV183FCMI = "" ;
         AV86IT05po = "" ;
      }
   }

   public void S171( )
   {
      /* 'GETFATORDIV' Routine */
      AV194Fator = (short)(GXutil.val( GXutil.substring( AV36CUTP, 4, 1), ".")) ;
      AV194Fator = (short)(java.lang.Math.pow(10,AV194Fator)) ;
   }

   public void S191( )
   {
      /* 'GRAVAHOT' Routine */
      /*
         INSERT RECORD ON TABLE HOT

      */
      A963ISOC = AV214ISOC ;
      A965PER_NA = AV119PER_N ;
      A966CODE = AV29CODE ;
      A967IATA = AV81IATA ;
      A968NUM_BI = AV108NUM_B ;
      A969TIPO_V = AV159TIPO_ ;
      A970DATA = AV38DATA ;
      A964CiaCod = AV26CiaCod ;
      A870VALOR_ = AV172VALOR ;
      n870VALOR_ = false ;
      A871COMMIS = AV31COMMIS ;
      n871COMMIS = false ;
      A883TAX_IT = AV31COMMIS ;
      n883TAX_IT = false ;
      A872LIT_CO = AV98LIT_CO ;
      n872LIT_CO = false ;
      A877TOUR_C = AV162TOUR_ ;
      n877TOUR_C = false ;
      A878PAX_NA = AV118PAX_N ;
      n878PAX_NA = false ;
      A889TRANS_ = AV164TRANS ;
      n889TRANS_ = false ;
      A894PLP_DO = AV120PLP_D ;
      n894PLP_DO = false ;
      A898ISSUE_ = AV83ISSUE_ ;
      n898ISSUE_ = false ;
      A900MOEDA = AV99MOEDA ;
      n900MOEDA = false ;
      A902NUM_BI = AV109NUM_B ;
      n902NUM_BI = false ;
      A875TAX_1 = AV195TAX_1 ;
      n875TAX_1 = false ;
      A876TAX_2 = AV196TAX_2 ;
      n876TAX_2 = false ;
      A905FCMI = AV183FCMI ;
      n905FCMI = false ;
      A906INLS = AV182INLS ;
      n906INLS = false ;
      A897NUMBER = AV86IT05po ;
      n897NUMBER = false ;
      A899PAYMT_ = GXutil.left( AV207ENRS, 100) ;
      n899PAYMT_ = false ;
      A908Loader = "RET" + GXutil.substring( AV153Versa, 1, 7) ;
      n908Loader = false ;
      A874AIRPT_ = AV212AIRPT ;
      n874AIRPT_ = false ;
      AV11HOT0_i = (int)(AV11HOT0_i+1) ;
      /* Using cursor P005W3 */
      pr_default.execute(1, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Boolean(n870VALOR_), new Double(A870VALOR_), new Boolean(n871COMMIS), new Double(A871COMMIS), new Boolean(n872LIT_CO), new Double(A872LIT_CO), new Boolean(n874AIRPT_), new Double(A874AIRPT_), new Boolean(n875TAX_1), new Double(A875TAX_1), new Boolean(n876TAX_2), new Double(A876TAX_2), new Boolean(n877TOUR_C), A877TOUR_C, new Boolean(n878PAX_NA), A878PAX_NA, new Boolean(n883TAX_IT), new Double(A883TAX_IT), new Boolean(n889TRANS_), A889TRANS_, new Boolean(n894PLP_DO), A894PLP_DO, new Boolean(n897NUMBER), A897NUMBER, new Boolean(n898ISSUE_), A898ISSUE_, new Boolean(n899PAYMT_), A899PAYMT_, new Boolean(n900MOEDA), A900MOEDA, new Boolean(n902NUM_BI), new Long(A902NUM_BI), new Boolean(n905FCMI), A905FCMI, new Boolean(n906INLS), A906INLS, new Boolean(n908Loader), A908Loader});
      if ( (pr_default.getStatus(1) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Optimized UPDATE. */
         /* Using cursor P005W4 */
         String AV86IT05po897Aux ;
         AV86IT05po897Aux = AV86IT05po ;
         pr_default.execute(2, new Object[] {new Boolean(n870VALOR_), new Double(AV172VALOR), new Boolean(n871COMMIS), new Double(AV31COMMIS), new Boolean(n872LIT_CO), new Double(AV98LIT_CO), new Boolean(n877TOUR_C), AV162TOUR_, new Boolean(n878PAX_NA), AV118PAX_N, new Boolean(n889TRANS_), AV164TRANS, new Boolean(n894PLP_DO), AV120PLP_D, new Boolean(n898ISSUE_), AV83ISSUE_, new Boolean(n900MOEDA), AV99MOEDA, new Boolean(n902NUM_BI), new Long(AV109NUM_B), new Boolean(n875TAX_1), new Double(AV195TAX_1), new Boolean(n876TAX_2), new Double(AV196TAX_2), new Boolean(n905FCMI), AV183FCMI, new Boolean(n906INLS), AV182INLS, new Boolean(n897NUMBER), AV86IT05po897Aux, AV153Versa, new Boolean(n874AIRPT_), new Double(AV212AIRPT), A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA});
         /* End optimized UPDATE. */
         AV10HOT0_D = (int)(AV10HOT0_D+1) ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      AV32Commit = (short)(AV32Commit+1) ;
      if ( ( AV32Commit >= 100 ) )
      {
         AV32Commit = (short)(0) ;
         Application.commit(context, remoteHandle, "DEFAULT", "pretinsert");
      }
   }

   public void S201( )
   {
      /* 'GRAVAHOT1' Routine */
      GXt_date5 = AV204Fli_D ;
      GXv_date6[0] = AV38DATA ;
      GXv_char3[0] = AV58Fli_Da ;
      GXv_date7[0] = GXt_date5 ;
      new pr2gds2date(remoteHandle, context).execute( GXv_date6, GXv_char3, GXv_date7) ;
      pretinsert.this.AV38DATA = GXv_date6[0] ;
      pretinsert.this.AV58Fli_Da = GXv_char3[0] ;
      pretinsert.this.GXt_date5 = GXv_date7[0] ;
      AV204Fli_D = GXt_date5 ;
      /*
         INSERT RECORD ON TABLE HOT1

      */
      A963ISOC = AV214ISOC ;
      A965PER_NA = AV119PER_N ;
      A966CODE = AV29CODE ;
      A967IATA = AV81IATA ;
      A968NUM_BI = AV108NUM_B ;
      A969TIPO_V = AV159TIPO_ ;
      A970DATA = AV38DATA ;
      A971ISeq = AV82ISeq ;
      A964CiaCod = AV26CiaCod ;
      A922Num_Bi = AV110Num_B ;
      n922Num_Bi = false ;
      A923I_From = AV79I_From ;
      n923I_From = false ;
      A924I_To = AV80I_To ;
      n924I_To = false ;
      A925Carrie = AV24Carrie ;
      n925Carrie = false ;
      A926Tariff = AV156Tarif ;
      n926Tariff = false ;
      A927Flight = AV59Flight ;
      n927Flight = false ;
      A928Claxss = AV27Claxss ;
      n928Claxss = false ;
      A929Fli_Da = AV58Fli_Da ;
      n929Fli_Da = false ;
      A930Fli_Da = AV204Fli_D ;
      n930Fli_Da = false ;
      AV75HOT1_I = (int)(AV75HOT1_I+1) ;
      /* Using cursor P005W5 */
      pr_default.execute(3, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Integer(A971ISeq), new Boolean(n922Num_Bi), new Long(A922Num_Bi), new Boolean(n923I_From), A923I_From, new Boolean(n924I_To), A924I_To, new Boolean(n925Carrie), A925Carrie, new Boolean(n926Tariff), A926Tariff, new Boolean(n927Flight), A927Flight, new Boolean(n928Claxss), A928Claxss, new Boolean(n929Fli_Da), A929Fli_Da, new Boolean(n930Fli_Da), A930Fli_Da});
      if ( (pr_default.getStatus(3) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Optimized UPDATE. */
         /* Using cursor P005W6 */
         pr_default.execute(4, new Object[] {new Boolean(n922Num_Bi), new Long(AV110Num_B), new Boolean(n923I_From), AV79I_From, new Boolean(n924I_To), AV80I_To, new Boolean(n925Carrie), AV24Carrie, new Boolean(n926Tariff), AV156Tarif, new Boolean(n927Flight), AV59Flight, new Boolean(n928Claxss), AV27Claxss, new Boolean(n929Fli_Da), AV58Fli_Da, new Boolean(n930Fli_Da), AV204Fli_D, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Integer(A971ISeq)});
         /* End optimized UPDATE. */
         AV74HOT1_D = (int)(AV74HOT1_D+1) ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S211( )
   {
      /* 'GRAVAHOT2' Routine */
      AV215FPAC = GXutil.trim( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac()) ;
      AV216APLC = GXutil.trim( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc()) ;
      AV222Atrib = AV215FPAC ;
      GXv_char3[0] = AV223Resul ;
      new pcrypto(remoteHandle, context).execute( AV222Atrib, "E", GXv_char3) ;
      pretinsert.this.AV223Resul = GXv_char3[0] ;
      AV217FPACE = AV223Resul ;
      AV218taman = GXutil.len( AV215FPAC) ;
      AV219FPACM = GXutil.left( AV215FPAC, 4) ;
      AV220k = (short)(1) ;
      while ( ( AV220k <= AV218taman - 8 ) )
      {
         AV219FPACM = AV219FPACM + "*" ;
         AV220k = (short)(AV220k+1) ;
      }
      AV219FPACM = AV219FPACM + GXutil.right( AV215FPAC, 4) ;
      AV242Pginf = GXutil.trim( AV219FPACM) + "*" + AV216APLC ;
      /*
         INSERT RECORD ON TABLE HOT2

      */
      A963ISOC = AV214ISOC ;
      A965PER_NA = AV119PER_N ;
      A966CODE = AV29CODE ;
      A967IATA = AV81IATA ;
      A968NUM_BI = AV108NUM_B ;
      A969TIPO_V = AV159TIPO_ ;
      A970DATA = AV38DATA ;
      A964CiaCod = AV26CiaCod ;
      A972SeqPag = AV78i ;
      A933PGCC_T = GXutil.substring( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), 3, 2) ;
      n933PGCC_T = false ;
      A934PGCCCF = "" ;
      n934PGCCCF = false ;
      A935PGAMOU = (double)(GXutil.val( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam(), ".")/ (double) (100)) ;
      n935PGAMOU = false ;
      A936PGTIP_ = GXutil.substring( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), 1, 2) ;
      n936PGTIP_ = false ;
      A937PGINFO = AV242Pginf ;
      n937PGINFO = false ;
      A1073PGCCN = AV217FPACE ;
      n1073PGCCN = false ;
      A939PGMOED = GXutil.substring( AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp(), 1, 3) ;
      n939PGMOED = false ;
      AV77HOT2_I = (int)(AV77HOT2_I+1) ;
      /* Using cursor P005W7 */
      pr_default.execute(5, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Integer(A972SeqPag), new Boolean(n933PGCC_T), A933PGCC_T, new Boolean(n934PGCCCF), A934PGCCCF, new Boolean(n935PGAMOU), new Double(A935PGAMOU), new Boolean(n936PGTIP_), A936PGTIP_, new Boolean(n937PGINFO), A937PGINFO, new Boolean(n939PGMOED), A939PGMOED, new Boolean(n1073PGCCN), A1073PGCCN});
      if ( (pr_default.getStatus(5) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Optimized UPDATE. */
         /* Using cursor P005W8 */
         pr_default.execute(6, new Object[] {AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam(), AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), new Boolean(n937PGINFO), AV242Pginf, new Boolean(n1073PGCCN), AV217FPACE, AV181retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp(), A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Integer(A972SeqPag)});
         /* End optimized UPDATE. */
         AV76HOT2_D = (int)(AV76HOT2_D+1) ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S221( )
   {
      /* 'GRAVAHOT3' Routine */
      /*
         INSERT RECORD ON TABLE HOT3

      */
      A963ISOC = AV214ISOC ;
      A965PER_NA = AV119PER_N ;
      A966CODE = AV29CODE ;
      A967IATA = AV81IATA ;
      A968NUM_BI = AV108NUM_B ;
      A964CiaCod = AV26CiaCod ;
      A969TIPO_V = AV159TIPO_ ;
      A970DATA = AV38DATA ;
      A973Tax_Co = AV160Tax_c[AV78i-1] ;
      A942Tax_Am = AV193Tax_A[AV78i-1] ;
      n942Tax_Am = false ;
      AV188HOT3_ = (int)(AV188HOT3_+1) ;
      /* Using cursor P005W9 */
      pr_default.execute(7, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A973Tax_Co, new Byte(A974Tax_Wh), A975Tax_Pd, new Boolean(n942Tax_Am), new Double(A942Tax_Am)});
      if ( (pr_default.getStatus(7) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P005W10 */
         pr_default.execute(8, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A973Tax_Co, new Byte(A974Tax_Wh), A975Tax_Pd, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A973Tax_Co, new Byte(A974Tax_Wh), A975Tax_Pd});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A963ISOC = P005W10_A963ISOC[0] ;
            A964CiaCod = P005W10_A964CiaCod[0] ;
            A965PER_NA = P005W10_A965PER_NA[0] ;
            A966CODE = P005W10_A966CODE[0] ;
            A967IATA = P005W10_A967IATA[0] ;
            A968NUM_BI = P005W10_A968NUM_BI[0] ;
            A969TIPO_V = P005W10_A969TIPO_V[0] ;
            A970DATA = P005W10_A970DATA[0] ;
            A973Tax_Co = P005W10_A973Tax_Co[0] ;
            A974Tax_Wh = P005W10_A974Tax_Wh[0] ;
            A975Tax_Pd = P005W10_A975Tax_Pd[0] ;
            A942Tax_Am = P005W10_A942Tax_Am[0] ;
            n942Tax_Am = P005W10_n942Tax_Am[0] ;
            A942Tax_Am = AV193Tax_A[AV78i-1] ;
            n942Tax_Am = false ;
            /* Using cursor P005W11 */
            pr_default.execute(9, new Object[] {new Boolean(n942Tax_Am), new Double(A942Tax_Am), A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A973Tax_Co, new Byte(A974Tax_Wh), A975Tax_Pd});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
         AV189HOT3_ = (int)(AV189HOT3_+1) ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S231( )
   {
      /* 'GRAVAHOT4' Routine */
      /*
         INSERT RECORD ON TABLE HOT4

      */
      A963ISOC = AV214ISOC ;
      A965PER_NA = AV119PER_N ;
      A966CODE = AV29CODE ;
      A967IATA = AV81IATA ;
      A968NUM_BI = AV108NUM_B ;
      A964CiaCod = AV26CiaCod ;
      A969TIPO_V = AV159TIPO_ ;
      A970DATA = AV38DATA ;
      A976Sub_Nu = GXutil.trim( AV200IT03_[AV78i-1]) ;
      A948Sub_De = "Original Tourcode--> " + GXutil.trim( AV203IT03_[AV78i-1]) ;
      n948Sub_De = false ;
      A949Sub_De = "Related Ticket/Doc. identifier--> " + GXutil.trim( AV201IT03_[AV78i-1]) ;
      n949Sub_De = false ;
      A950Sub_De = "Related Doc.Issue Date(YYMMDD)--> " + GXutil.trim( AV202IT03_[AV78i-1]) ;
      n950Sub_De = false ;
      AV198HOT4_ = (int)(AV198HOT4_+1) ;
      /* Using cursor P005W12 */
      pr_default.execute(10, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu, new Boolean(n948Sub_De), A948Sub_De, new Boolean(n949Sub_De), A949Sub_De, new Boolean(n950Sub_De), A950Sub_De});
      if ( (pr_default.getStatus(10) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P005W13 */
         pr_default.execute(11, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A963ISOC = P005W13_A963ISOC[0] ;
            A964CiaCod = P005W13_A964CiaCod[0] ;
            A965PER_NA = P005W13_A965PER_NA[0] ;
            A966CODE = P005W13_A966CODE[0] ;
            A967IATA = P005W13_A967IATA[0] ;
            A968NUM_BI = P005W13_A968NUM_BI[0] ;
            A969TIPO_V = P005W13_A969TIPO_V[0] ;
            A970DATA = P005W13_A970DATA[0] ;
            A976Sub_Nu = P005W13_A976Sub_Nu[0] ;
            A948Sub_De = P005W13_A948Sub_De[0] ;
            n948Sub_De = P005W13_n948Sub_De[0] ;
            A949Sub_De = P005W13_A949Sub_De[0] ;
            n949Sub_De = P005W13_n949Sub_De[0] ;
            A950Sub_De = P005W13_A950Sub_De[0] ;
            n950Sub_De = P005W13_n950Sub_De[0] ;
            A948Sub_De = "Original Tourcode--> " + GXutil.trim( AV203IT03_[AV78i-1]) ;
            n948Sub_De = false ;
            A949Sub_De = "Related Ticket/Doc. identifier--> " + GXutil.trim( AV201IT03_[AV78i-1]) ;
            n949Sub_De = false ;
            A950Sub_De = "Related Doc.Issue Date(YYMMDD)--> " + GXutil.trim( AV202IT03_[AV78i-1]) ;
            n950Sub_De = false ;
            /* Using cursor P005W14 */
            pr_default.execute(12, new Object[] {new Boolean(n948Sub_De), A948Sub_De, new Boolean(n949Sub_De), A949Sub_De, new Boolean(n950Sub_De), A950Sub_De, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(11);
         AV199HOT4_ = (int)(AV199HOT4_+1) ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S241( )
   {
      /* 'GRAVAHOT5' Routine */
      /*
         INSERT RECORD ON TABLE HOT5

      */
      A963ISOC = AV214ISOC ;
      A965PER_NA = AV119PER_N ;
      A966CODE = AV29CODE ;
      A967IATA = AV81IATA ;
      A968NUM_BI = AV108NUM_B ;
      A964CiaCod = AV26CiaCod ;
      A969TIPO_V = AV159TIPO_ ;
      A970DATA = AV38DATA ;
      A977FRCASe = (byte)(AV78i) ;
      A951FRCA = AV185FRCA[AV78i-1] ;
      n951FRCA = false ;
      AV190HOT5_ = (int)(AV190HOT5_+1) ;
      /* Using cursor P005W15 */
      pr_default.execute(13, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Byte(A977FRCASe), new Boolean(n951FRCA), A951FRCA});
      if ( (pr_default.getStatus(13) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P005W16 */
         pr_default.execute(14, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Byte(A977FRCASe), A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Byte(A977FRCASe)});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A963ISOC = P005W16_A963ISOC[0] ;
            A964CiaCod = P005W16_A964CiaCod[0] ;
            A965PER_NA = P005W16_A965PER_NA[0] ;
            A966CODE = P005W16_A966CODE[0] ;
            A967IATA = P005W16_A967IATA[0] ;
            A968NUM_BI = P005W16_A968NUM_BI[0] ;
            A969TIPO_V = P005W16_A969TIPO_V[0] ;
            A970DATA = P005W16_A970DATA[0] ;
            A977FRCASe = P005W16_A977FRCASe[0] ;
            A951FRCA = P005W16_A951FRCA[0] ;
            n951FRCA = P005W16_n951FRCA[0] ;
            A951FRCA = AV185FRCA[AV78i-1] ;
            n951FRCA = false ;
            /* Using cursor P005W17 */
            pr_default.execute(15, new Object[] {new Boolean(n951FRCA), A951FRCA, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Byte(A977FRCASe)});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(14);
         AV191HOT5_ = (int)(AV191HOT5_+1) ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S251( )
   {
      /* 'GRAVAIT0_' Routine */
      AV235Hot10.setgxTv_SdtSDT_HOT10_Isoc( AV214ISOC );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Ciacod( AV26CiaCod );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Per_name( AV119PER_N );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Code( AV29CODE );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Iata( AV81IATA );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Num_bil( AV108NUM_B );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Tipo_vend( AV159TIPO_ );
      AV235Hot10.setgxTv_SdtSDT_HOT10_Data( AV38DATA );
      AV235Hot10.setgxTv_SdtSDT_HOT10_H10type( "CAMPO" );
      AV236Indic = (short)(1) ;
      while ( ( AV236Indic <= AV225IT0A.size() ) )
      {
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10seq( localUtil.format( AV236Indic, "999") );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10recid( "IT0A" );
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Rfid_it0a(), "") != 0 ) || ( AV225IT0A.size() > 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "RFID" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Rfid_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Bera_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "BERA" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Bera_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Icdn_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "ICDN" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Icdn_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Cdgt_it0a() != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "CDGT" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( localUtil.format( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Cdgt_it0a(), "9") );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Amil_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "AMIL" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Amil_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Rfic_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "RFIC" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Rfic_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpoc_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "MPOC" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpoc_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpeq_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "MPEQ" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpeq_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpev_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "MPEV" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpev_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpsc_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "MPSC" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mpsc_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mptx_it0a(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "MPTX" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( GXutil.trim( ((SdtIT0A_IT0AItem)AV225IT0A.elementAt(-1+AV236Indic)).getgxTv_SdtIT0A_IT0AItem_Mptx_it0a()) );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         AV236Indic = (short)(AV236Indic+1) ;
      }
      if ( ( GXutil.strcmp(AV226OAAI_, "") != 0 ) )
      {
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10seq( "001" );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10recid( "IT0B" );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "OAAI" );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( AV226OAAI_ );
         GXv_char3[0] = "" ;
         new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
      }
      AV236Indic = (short)(1) ;
      while ( ( AV236Indic <= AV228IT0C.size() ) )
      {
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10seq( localUtil.format( AV236Indic, "999") );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10recid( "IT0C" );
         if ( ( GXutil.strcmp(((SdtIT0C_IT0CItem)AV228IT0C.elementAt(-1+AV236Indic)).getgxTv_SdtIT0C_IT0CItem_Plid_it0c(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "PLID" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0C_IT0CItem)AV228IT0C.elementAt(-1+AV236Indic)).getgxTv_SdtIT0C_IT0CItem_Plid_it0c() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0C_IT0CItem)AV228IT0C.elementAt(-1+AV236Indic)).getgxTv_SdtIT0C_IT0CItem_Pltx_it0c(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "PLTX" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0C_IT0CItem)AV228IT0C.elementAt(-1+AV236Indic)).getgxTv_SdtIT0C_IT0CItem_Pltx_it0c() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         AV236Indic = (short)(AV236Indic+1) ;
      }
      AV236Indic = (short)(1) ;
      while ( ( AV236Indic <= AV230IT0D.size() ) )
      {
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10seq( localUtil.format( AV236Indic, "999") );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10recid( "IT0D" );
         if ( ( ((SdtIT0D_IT0DItem)AV230IT0D.elementAt(-1+AV236Indic)).getgxTv_SdtIT0D_IT0DItem_Cpnr_it0d() != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "CPNR" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( localUtil.format( ((SdtIT0D_IT0DItem)AV230IT0D.elementAt(-1+AV236Indic)).getgxTv_SdtIT0D_IT0DItem_Cpnr_it0d(), "9") );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0D_IT0DItem)AV230IT0D.elementAt(-1+AV236Indic)).getgxTv_SdtIT0D_IT0DItem_Plid_it0d(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "PLID" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0D_IT0DItem)AV230IT0D.elementAt(-1+AV236Indic)).getgxTv_SdtIT0D_IT0DItem_Plid_it0d() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0D_IT0DItem)AV230IT0D.elementAt(-1+AV236Indic)).getgxTv_SdtIT0D_IT0DItem_Pltx_it0d(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "PLTX" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0D_IT0DItem)AV230IT0D.elementAt(-1+AV236Indic)).getgxTv_SdtIT0D_IT0DItem_Pltx_it0d() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         AV236Indic = (short)(AV236Indic+1) ;
      }
      AV236Indic = (short)(1) ;
      while ( ( AV236Indic <= AV232IT0E.size() ) )
      {
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10seq( localUtil.format( AV236Indic, "999") );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10recid( "IT0E" );
         if ( ( GXutil.strcmp(((SdtIT0E_IT0EItem)AV232IT0E.elementAt(-1+AV236Indic)).getgxTv_SdtIT0E_IT0EItem_Plid_it0e(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "PLID" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0E_IT0EItem)AV232IT0E.elementAt(-1+AV236Indic)).getgxTv_SdtIT0E_IT0EItem_Plid_it0e() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0E_IT0EItem)AV232IT0E.elementAt(-1+AV236Indic)).getgxTv_SdtIT0E_IT0EItem_Spin_it0e(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "SPIN" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0E_IT0EItem)AV232IT0E.elementAt(-1+AV236Indic)).getgxTv_SdtIT0E_IT0EItem_Spin_it0e() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         AV236Indic = (short)(AV236Indic+1) ;
      }
      AV236Indic = (short)(1) ;
      while ( ( AV236Indic <= AV234IT0G.size() ) )
      {
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10seq( localUtil.format( AV236Indic, "999") );
         AV235Hot10.setgxTv_SdtSDT_HOT10_H10recid( "IT0G" );
         if ( ( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emcp_it0g() != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMCP" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( localUtil.format( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emcp_it0g(), "9") );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emcv_it0g() != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMCV" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( localUtil.format( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emcv_it0g(), "99999999999") );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Cutp_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "CUTP" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Cutp_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emrt_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMRT" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emrt_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emrc_it0g() != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMRC" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( localUtil.format( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emrc_it0g(), "9") );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emsc_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMSC" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emsc_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emoc_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMOC" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emoc_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xboa_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "XBOA" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xboa_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xbru_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "XBRU" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xbru_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xbne_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "XBNE" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xbne_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emrm_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMRM" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emrm_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emci_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "EMCI" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Emci_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         if ( ( GXutil.strcmp(((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xbct_it0g(), "") != 0 ) )
         {
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10field( "XBCT" );
            AV235Hot10.setgxTv_SdtSDT_HOT10_H10content( ((SdtIT0G_IT0GItem)AV234IT0G.elementAt(-1+AV236Indic)).getgxTv_SdtIT0G_IT0GItem_Xbct_it0g() );
            GXv_char3[0] = "" ;
            new pmanipulahot10(remoteHandle, context).execute( AV235Hot10, "", "", "", "INSERT", GXv_char3) ;
         }
         AV236Indic = (short)(AV236Indic+1) ;
      }
      AV225IT0A.clear();
      AV228IT0C.clear();
      AV230IT0D.clear();
      AV232IT0E.clear();
      AV234IT0G.clear();
      AV226OAAI_ = "" ;
   }

   public void S181( )
   {
      /* 'SEARCHSUBFILE' Routine */
      AV247GXLvl = (byte)(0) ;
      /* Using cursor P005W18 */
      pr_default.execute(16, new Object[] {AV158TDNR});
      while ( (pr_default.getStatus(16) != 101) )
      {
         A1231lccbT = P005W18_A1231lccbT[0] ;
         A1150lccbE = P005W18_A1150lccbE[0] ;
         A1222lccbI = P005W18_A1222lccbI[0] ;
         A1223lccbD = P005W18_A1223lccbD[0] ;
         A1224lccbC = P005W18_A1224lccbC[0] ;
         A1225lccbC = P005W18_A1225lccbC[0] ;
         A1226lccbA = P005W18_A1226lccbA[0] ;
         A1227lccbO = P005W18_A1227lccbO[0] ;
         A1228lccbF = P005W18_A1228lccbF[0] ;
         A1232lccbT = P005W18_A1232lccbT[0] ;
         AV247GXLvl = (byte)(1) ;
         AV221FileD = "Y" ;
         pr_default.readNext(16);
      }
      pr_default.close(16);
      if ( ( AV247GXLvl == 0 ) )
      {
         AV221FileD = "N" ;
      }
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(pretinsert.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = pretinsert.this.AV176FileS;
      this.aP1[0] = pretinsert.this.AV39DebugM;
      Application.commit(context, remoteHandle, "DEFAULT", "pretinsert");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV178Filen = "" ;
      GXv_char2 = new String [1] ;
      AV153Versa = "" ;
      GXt_char1 = "" ;
      AV210TACNO = (byte)(0) ;
      AV142s = "" ;
      AV78i = 0 ;
      AV211TACNO = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV211TACNO[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      returnInSub = false ;
      AV11HOT0_i = 0 ;
      AV10HOT0_D = 0 ;
      AV75HOT1_I = 0 ;
      AV74HOT1_D = 0 ;
      AV77HOT2_I = 0 ;
      AV76HOT2_D = 0 ;
      AV188HOT3_ = 0 ;
      AV189HOT3_ = 0 ;
      AV198HOT4_ = 0 ;
      AV199HOT4_ = 0 ;
      AV190HOT5_ = 0 ;
      AV191HOT5_ = 0 ;
      AV169TRNN_ = "" ;
      scmdbuf = "" ;
      P005W2_A994HntLin = new String[] {""} ;
      P005W2_n994HntLin = new boolean[] {false} ;
      P005W2_A993HntUsu = new String[] {""} ;
      P005W2_n993HntUsu = new boolean[] {false} ;
      P005W2_A997HntSeq = new String[] {""} ;
      P005W2_A998HntSub = new byte[1] ;
      A994HntLin = "" ;
      n994HntLin = false ;
      A993HntUsu = "" ;
      n993HntUsu = false ;
      A997HntSeq = "" ;
      A998HntSub = (byte)(0) ;
      AV12Linha = "" ;
      AV64HntSeq = "" ;
      AV127RCID = "" ;
      AV168TRNN_ = "" ;
      AV35CRS = "" ;
      AV148SPED = "" ;
      GXt_char4 = "" ;
      AV54Flag_I = (byte)(0) ;
      AV165TRNC = "" ;
      AV8AGTN = "" ;
      AV158TDNR = "" ;
      AV175TKT_D = "" ;
      AV122PXNM = "" ;
      AV174TKT_C = "" ;
      AV206CPUI = "" ;
      AV152TACN = "" ;
      AV214ISOC = "" ;
      AV161TOUR = "" ;
      AV114ORIN = "" ;
      AV182INLS = "" ;
      AV55Flag_I = (byte)(0) ;
      AV192Count = (short)(0) ;
      AV86IT05po = "" ;
      AV157TDAM = "" ;
      AV34CORT = "" ;
      AV28COAM = "" ;
      AV36CUTP = "" ;
      AV213j = 0 ;
      AV9Count_I = (byte)(0) ;
      AV19IT06_O = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV19IT06_O[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV14IT06_D = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV14IT06_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV13IT06_C = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV13IT06_C[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV205IT06_ = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV205IT06_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV17IT06_F = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV17IT06_F[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV16IT06_F = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV16IT06_F[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV18IT06_F = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV18IT06_F[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV15IT06_F = new String [40] ;
      GX_I = 1 ;
      while ( ( GX_I <= 40 ) )
      {
         AV15IT06_F[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV184Flag_ = (byte)(0) ;
      AV187Count = (byte)(0) ;
      AV183FCMI = "" ;
      AV57Flag_I = (byte)(0) ;
      AV61FPTP = "" ;
      AV181retfo = new Sdtsdt_RETFOP_sdt_RETFOPItem(context);
      AV180retfo = new GxObjectCollection(Sdtsdt_RETFOP_sdt_RETFOPItem.class, "sdt_RETFOP.sdt_RETFOPItem", "IataICSI");
      AV179Count = (byte)(0) ;
      AV208Flag_ = (byte)(0) ;
      AV207ENRS = "" ;
      AV224IT0A_ = new SdtIT0A_IT0AItem(context);
      AV225IT0A = new GxObjectCollection(SdtIT0A_IT0AItem.class, "IT0A.IT0AItem", "IataICSI");
      AV226OAAI_ = "" ;
      AV227IT0C_ = new SdtIT0C_IT0CItem(context);
      AV228IT0C = new GxObjectCollection(SdtIT0C_IT0CItem.class, "IT0C.IT0CItem", "IataICSI");
      AV229IT0D_ = new SdtIT0D_IT0DItem(context);
      AV230IT0D = new GxObjectCollection(SdtIT0D_IT0DItem.class, "IT0D.IT0DItem", "IataICSI");
      AV231IT0E_ = new SdtIT0E_IT0EItem(context);
      AV232IT0E = new GxObjectCollection(SdtIT0E_IT0EItem.class, "IT0E.IT0EItem", "IataICSI");
      AV233IT0G_ = new SdtIT0G_IT0GItem(context);
      AV234IT0G = new GxObjectCollection(SdtIT0G_IT0GItem.class, "IT0G.IT0GItem", "IataICSI");
      AV197Count = (byte)(0) ;
      AV202IT03_ = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV202IT03_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV203IT03_ = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV203IT03_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV201IT03_ = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV201IT03_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV200IT03_ = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV200IT03_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV160Tax_c = new String [100] ;
      GX_I = 1 ;
      while ( ( GX_I <= 100 ) )
      {
         AV160Tax_c[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV193Tax_A = new double [100] ;
      AV194Fator = (short)(0) ;
      AV195TAX_1 = 0 ;
      AV185FRCA = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV185FRCA[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV119PER_N = "" ;
      AV29CODE = "" ;
      AV81IATA = "" ;
      AV108NUM_B = "" ;
      AV221FileD = "" ;
      AV159TIPO_ = "" ;
      AV38DATA = GXutil.nullDate() ;
      AV196TAX_2 = 0 ;
      AV172VALOR = 0 ;
      AV31COMMIS = 0 ;
      AV98LIT_CO = 0 ;
      AV162TOUR_ = "" ;
      AV118PAX_N = "" ;
      AV164TRANS = "" ;
      AV120PLP_D = "" ;
      AV83ISSUE_ = "" ;
      AV99MOEDA = "" ;
      AV109NUM_B = 0 ;
      AV26CiaCod = "" ;
      AV212AIRPT = 0 ;
      AV82ISeq = 0 ;
      AV110Num_B = 0 ;
      AV79I_From = "" ;
      AV80I_To = "" ;
      AV24Carrie = "" ;
      AV156Tarif = "" ;
      AV59Flight = "" ;
      AV27Claxss = "" ;
      AV58Fli_Da = "" ;
      AV56Flag_I = (byte)(0) ;
      GX_INS212 = 0 ;
      A963ISOC = "" ;
      A965PER_NA = "" ;
      A966CODE = "" ;
      A967IATA = "" ;
      A968NUM_BI = "" ;
      A969TIPO_V = "" ;
      A970DATA = GXutil.nullDate() ;
      A964CiaCod = "" ;
      A870VALOR_ = 0 ;
      n870VALOR_ = false ;
      A871COMMIS = 0 ;
      n871COMMIS = false ;
      A883TAX_IT = 0 ;
      n883TAX_IT = false ;
      A872LIT_CO = 0 ;
      n872LIT_CO = false ;
      A877TOUR_C = "" ;
      n877TOUR_C = false ;
      A878PAX_NA = "" ;
      n878PAX_NA = false ;
      A889TRANS_ = "" ;
      n889TRANS_ = false ;
      A894PLP_DO = "" ;
      n894PLP_DO = false ;
      A898ISSUE_ = "" ;
      n898ISSUE_ = false ;
      A900MOEDA = "" ;
      n900MOEDA = false ;
      A902NUM_BI = 0 ;
      n902NUM_BI = false ;
      A875TAX_1 = 0 ;
      n875TAX_1 = false ;
      A876TAX_2 = 0 ;
      n876TAX_2 = false ;
      A905FCMI = "" ;
      n905FCMI = false ;
      A906INLS = "" ;
      n906INLS = false ;
      A897NUMBER = "" ;
      n897NUMBER = false ;
      A899PAYMT_ = "" ;
      n899PAYMT_ = false ;
      A908Loader = "" ;
      n908Loader = false ;
      A874AIRPT_ = 0 ;
      n874AIRPT_ = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV32Commit = (short)(0) ;
      AV204Fli_D = GXutil.nullDate() ;
      GXt_date5 = GXutil.nullDate() ;
      GXv_date6 = new java.util.Date [1] ;
      GXv_date7 = new java.util.Date [1] ;
      GX_INS213 = 0 ;
      A971ISeq = 0 ;
      A922Num_Bi = 0 ;
      n922Num_Bi = false ;
      A923I_From = "" ;
      n923I_From = false ;
      A924I_To = "" ;
      n924I_To = false ;
      A925Carrie = "" ;
      n925Carrie = false ;
      A926Tariff = "" ;
      n926Tariff = false ;
      A927Flight = "" ;
      n927Flight = false ;
      A928Claxss = "" ;
      n928Claxss = false ;
      A929Fli_Da = "" ;
      n929Fli_Da = false ;
      A930Fli_Da = GXutil.nullDate() ;
      n930Fli_Da = false ;
      AV215FPAC = "" ;
      AV216APLC = "" ;
      AV222Atrib = "" ;
      AV223Resul = "" ;
      AV217FPACE = "" ;
      AV218taman = 0 ;
      AV219FPACM = "" ;
      AV220k = (short)(0) ;
      AV242Pginf = "" ;
      GX_INS214 = 0 ;
      A972SeqPag = 0 ;
      A933PGCC_T = "" ;
      n933PGCC_T = false ;
      A934PGCCCF = "" ;
      n934PGCCCF = false ;
      A935PGAMOU = 0 ;
      n935PGAMOU = false ;
      A936PGTIP_ = "" ;
      n936PGTIP_ = false ;
      A937PGINFO = "" ;
      n937PGINFO = false ;
      A1073PGCCN = "" ;
      n1073PGCCN = false ;
      A939PGMOED = "" ;
      n939PGMOED = false ;
      GX_INS215 = 0 ;
      A973Tax_Co = "" ;
      A942Tax_Am = 0 ;
      n942Tax_Am = false ;
      A974Tax_Wh = (byte)(0) ;
      A975Tax_Pd = "" ;
      P005W10_A963ISOC = new String[] {""} ;
      P005W10_A964CiaCod = new String[] {""} ;
      P005W10_A965PER_NA = new String[] {""} ;
      P005W10_A966CODE = new String[] {""} ;
      P005W10_A967IATA = new String[] {""} ;
      P005W10_A968NUM_BI = new String[] {""} ;
      P005W10_A969TIPO_V = new String[] {""} ;
      P005W10_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005W10_A973Tax_Co = new String[] {""} ;
      P005W10_A974Tax_Wh = new byte[1] ;
      P005W10_A975Tax_Pd = new String[] {""} ;
      P005W10_A942Tax_Am = new double[1] ;
      P005W10_n942Tax_Am = new boolean[] {false} ;
      GX_INS216 = 0 ;
      A976Sub_Nu = "" ;
      A948Sub_De = "" ;
      n948Sub_De = false ;
      A949Sub_De = "" ;
      n949Sub_De = false ;
      A950Sub_De = "" ;
      n950Sub_De = false ;
      P005W13_A963ISOC = new String[] {""} ;
      P005W13_A964CiaCod = new String[] {""} ;
      P005W13_A965PER_NA = new String[] {""} ;
      P005W13_A966CODE = new String[] {""} ;
      P005W13_A967IATA = new String[] {""} ;
      P005W13_A968NUM_BI = new String[] {""} ;
      P005W13_A969TIPO_V = new String[] {""} ;
      P005W13_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005W13_A976Sub_Nu = new String[] {""} ;
      P005W13_A948Sub_De = new String[] {""} ;
      P005W13_n948Sub_De = new boolean[] {false} ;
      P005W13_A949Sub_De = new String[] {""} ;
      P005W13_n949Sub_De = new boolean[] {false} ;
      P005W13_A950Sub_De = new String[] {""} ;
      P005W13_n950Sub_De = new boolean[] {false} ;
      GX_INS217 = 0 ;
      A977FRCASe = (byte)(0) ;
      A951FRCA = "" ;
      n951FRCA = false ;
      P005W16_A963ISOC = new String[] {""} ;
      P005W16_A964CiaCod = new String[] {""} ;
      P005W16_A965PER_NA = new String[] {""} ;
      P005W16_A966CODE = new String[] {""} ;
      P005W16_A967IATA = new String[] {""} ;
      P005W16_A968NUM_BI = new String[] {""} ;
      P005W16_A969TIPO_V = new String[] {""} ;
      P005W16_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005W16_A977FRCASe = new byte[1] ;
      P005W16_A951FRCA = new String[] {""} ;
      P005W16_n951FRCA = new boolean[] {false} ;
      AV235Hot10 = new SdtSDT_HOT10(context);
      AV236Indic = (short)(0) ;
      GXv_char3 = new String [1] ;
      AV247GXLvl = (byte)(0) ;
      P005W18_A1231lccbT = new String[] {""} ;
      P005W18_A1150lccbE = new String[] {""} ;
      P005W18_A1222lccbI = new String[] {""} ;
      P005W18_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P005W18_A1224lccbC = new String[] {""} ;
      P005W18_A1225lccbC = new String[] {""} ;
      P005W18_A1226lccbA = new String[] {""} ;
      P005W18_A1227lccbO = new String[] {""} ;
      P005W18_A1228lccbF = new String[] {""} ;
      P005W18_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1150lccbE = "" ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1224lccbC = "" ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      A1232lccbT = "" ;
      GX_I = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretinsert__default(),
         new Object[] {
             new Object[] {
            P005W2_A994HntLin, P005W2_n994HntLin, P005W2_A993HntUsu, P005W2_n993HntUsu, P005W2_A997HntSeq, P005W2_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P005W10_A963ISOC, P005W10_A964CiaCod, P005W10_A965PER_NA, P005W10_A966CODE, P005W10_A967IATA, P005W10_A968NUM_BI, P005W10_A969TIPO_V, P005W10_A970DATA, P005W10_A973Tax_Co, P005W10_A974Tax_Wh,
            P005W10_A975Tax_Pd, P005W10_A942Tax_Am, P005W10_n942Tax_Am
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P005W13_A963ISOC, P005W13_A964CiaCod, P005W13_A965PER_NA, P005W13_A966CODE, P005W13_A967IATA, P005W13_A968NUM_BI, P005W13_A969TIPO_V, P005W13_A970DATA, P005W13_A976Sub_Nu, P005W13_A948Sub_De,
            P005W13_n948Sub_De, P005W13_A949Sub_De, P005W13_n949Sub_De, P005W13_A950Sub_De, P005W13_n950Sub_De
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P005W16_A963ISOC, P005W16_A964CiaCod, P005W16_A965PER_NA, P005W16_A966CODE, P005W16_A967IATA, P005W16_A968NUM_BI, P005W16_A969TIPO_V, P005W16_A970DATA, P005W16_A977FRCASe, P005W16_A951FRCA,
            P005W16_n951FRCA
            }
            , new Object[] {
            }
            , new Object[] {
            P005W18_A1231lccbT, P005W18_A1150lccbE, P005W18_A1222lccbI, P005W18_A1223lccbD, P005W18_A1224lccbC, P005W18_A1225lccbC, P005W18_A1226lccbA, P005W18_A1227lccbO, P005W18_A1228lccbF, P005W18_A1232lccbT
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV210TACNO ;
   private byte A998HntSub ;
   private byte AV54Flag_I ;
   private byte AV55Flag_I ;
   private byte AV9Count_I ;
   private byte AV184Flag_ ;
   private byte AV187Count ;
   private byte AV57Flag_I ;
   private byte AV179Count ;
   private byte AV208Flag_ ;
   private byte AV197Count ;
   private byte AV56Flag_I ;
   private byte A974Tax_Wh ;
   private byte A977FRCASe ;
   private byte AV247GXLvl ;
   private short AV192Count ;
   private short AV194Fator ;
   private short Gx_err ;
   private short AV32Commit ;
   private short AV220k ;
   private short AV236Indic ;
   private int AV78i ;
   private int AV11HOT0_i ;
   private int AV10HOT0_D ;
   private int AV75HOT1_I ;
   private int AV74HOT1_D ;
   private int AV77HOT2_I ;
   private int AV76HOT2_D ;
   private int AV188HOT3_ ;
   private int AV189HOT3_ ;
   private int AV198HOT4_ ;
   private int AV199HOT4_ ;
   private int AV190HOT5_ ;
   private int AV191HOT5_ ;
   private int AV213j ;
   private int AV82ISeq ;
   private int GX_INS212 ;
   private int GX_INS213 ;
   private int A971ISeq ;
   private int AV218taman ;
   private int GX_INS214 ;
   private int A972SeqPag ;
   private int GX_INS215 ;
   private int GX_INS216 ;
   private int GX_INS217 ;
   private int GX_I ;
   private long AV109NUM_B ;
   private long AV110Num_B ;
   private long A902NUM_BI ;
   private long A922Num_Bi ;
   private double AV193Tax_A[] ;
   private double AV195TAX_1 ;
   private double AV196TAX_2 ;
   private double AV172VALOR ;
   private double AV31COMMIS ;
   private double AV98LIT_CO ;
   private double AV212AIRPT ;
   private double A870VALOR_ ;
   private double A871COMMIS ;
   private double A883TAX_IT ;
   private double A872LIT_CO ;
   private double A875TAX_1 ;
   private double A876TAX_2 ;
   private double A874AIRPT_ ;
   private double A935PGAMOU ;
   private double A942Tax_Am ;
   private String AV176FileS ;
   private String AV39DebugM ;
   private String AV178Filen ;
   private String GXv_char2[] ;
   private String AV153Versa ;
   private String GXt_char1 ;
   private String AV142s ;
   private String AV211TACNO[] ;
   private String AV169TRNN_ ;
   private String scmdbuf ;
   private String A997HntSeq ;
   private String AV64HntSeq ;
   private String AV127RCID ;
   private String AV168TRNN_ ;
   private String AV35CRS ;
   private String AV148SPED ;
   private String GXt_char4 ;
   private String AV165TRNC ;
   private String AV8AGTN ;
   private String AV158TDNR ;
   private String AV175TKT_D ;
   private String AV122PXNM ;
   private String AV206CPUI ;
   private String AV152TACN ;
   private String AV214ISOC ;
   private String AV161TOUR ;
   private String AV114ORIN ;
   private String AV182INLS ;
   private String AV86IT05po ;
   private String AV157TDAM ;
   private String AV34CORT ;
   private String AV28COAM ;
   private String AV36CUTP ;
   private String AV19IT06_O[] ;
   private String AV14IT06_D[] ;
   private String AV13IT06_C[] ;
   private String AV205IT06_[] ;
   private String AV17IT06_F[] ;
   private String AV16IT06_F[] ;
   private String AV18IT06_F[] ;
   private String AV15IT06_F[] ;
   private String AV183FCMI ;
   private String AV61FPTP ;
   private String AV207ENRS ;
   private String AV226OAAI_ ;
   private String AV202IT03_[] ;
   private String AV203IT03_[] ;
   private String AV201IT03_[] ;
   private String AV200IT03_[] ;
   private String AV160Tax_c[] ;
   private String AV185FRCA[] ;
   private String AV119PER_N ;
   private String AV29CODE ;
   private String AV81IATA ;
   private String AV108NUM_B ;
   private String AV221FileD ;
   private String AV159TIPO_ ;
   private String AV99MOEDA ;
   private String AV26CiaCod ;
   private String AV27Claxss ;
   private String AV58Fli_Da ;
   private String A963ISOC ;
   private String A965PER_NA ;
   private String A966CODE ;
   private String A967IATA ;
   private String A968NUM_BI ;
   private String A969TIPO_V ;
   private String A964CiaCod ;
   private String A900MOEDA ;
   private String A905FCMI ;
   private String A906INLS ;
   private String A908Loader ;
   private String Gx_emsg ;
   private String A928Claxss ;
   private String A929Fli_Da ;
   private String AV222Atrib ;
   private String AV223Resul ;
   private String A933PGCC_T ;
   private String A936PGTIP_ ;
   private String A973Tax_Co ;
   private String A975Tax_Pd ;
   private String A976Sub_Nu ;
   private String A951FRCA ;
   private String GXv_char3[] ;
   private String A1231lccbT ;
   private String A1150lccbE ;
   private String A1222lccbI ;
   private String A1224lccbC ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private String A1232lccbT ;
   private java.util.Date AV38DATA ;
   private java.util.Date A970DATA ;
   private java.util.Date AV204Fli_D ;
   private java.util.Date GXt_date5 ;
   private java.util.Date GXv_date6[] ;
   private java.util.Date GXv_date7[] ;
   private java.util.Date A930Fli_Da ;
   private java.util.Date A1223lccbD ;
   private boolean returnInSub ;
   private boolean n994HntLin ;
   private boolean n993HntUsu ;
   private boolean n870VALOR_ ;
   private boolean n871COMMIS ;
   private boolean n883TAX_IT ;
   private boolean n872LIT_CO ;
   private boolean n877TOUR_C ;
   private boolean n878PAX_NA ;
   private boolean n889TRANS_ ;
   private boolean n894PLP_DO ;
   private boolean n898ISSUE_ ;
   private boolean n900MOEDA ;
   private boolean n902NUM_BI ;
   private boolean n875TAX_1 ;
   private boolean n876TAX_2 ;
   private boolean n905FCMI ;
   private boolean n906INLS ;
   private boolean n897NUMBER ;
   private boolean n899PAYMT_ ;
   private boolean n908Loader ;
   private boolean n874AIRPT_ ;
   private boolean n922Num_Bi ;
   private boolean n923I_From ;
   private boolean n924I_To ;
   private boolean n925Carrie ;
   private boolean n926Tariff ;
   private boolean n927Flight ;
   private boolean n928Claxss ;
   private boolean n929Fli_Da ;
   private boolean n930Fli_Da ;
   private boolean n933PGCC_T ;
   private boolean n934PGCCCF ;
   private boolean n935PGAMOU ;
   private boolean n936PGTIP_ ;
   private boolean n937PGINFO ;
   private boolean n1073PGCCN ;
   private boolean n939PGMOED ;
   private boolean n942Tax_Am ;
   private boolean n948Sub_De ;
   private boolean n949Sub_De ;
   private boolean n950Sub_De ;
   private boolean n951FRCA ;
   private String A994HntLin ;
   private String AV12Linha ;
   private String A993HntUsu ;
   private String AV174TKT_C ;
   private String AV162TOUR_ ;
   private String AV118PAX_N ;
   private String AV164TRANS ;
   private String AV120PLP_D ;
   private String AV83ISSUE_ ;
   private String AV79I_From ;
   private String AV80I_To ;
   private String AV24Carrie ;
   private String AV156Tarif ;
   private String AV59Flight ;
   private String A877TOUR_C ;
   private String A878PAX_NA ;
   private String A889TRANS_ ;
   private String A894PLP_DO ;
   private String A898ISSUE_ ;
   private String A897NUMBER ;
   private String A899PAYMT_ ;
   private String A923I_From ;
   private String A924I_To ;
   private String A925Carrie ;
   private String A926Tariff ;
   private String A927Flight ;
   private String AV215FPAC ;
   private String AV216APLC ;
   private String AV217FPACE ;
   private String AV219FPACM ;
   private String AV242Pginf ;
   private String A934PGCCCF ;
   private String A937PGINFO ;
   private String A1073PGCCN ;
   private String A939PGMOED ;
   private String A948Sub_De ;
   private String A949Sub_De ;
   private String A950Sub_De ;
   private GxObjectCollection AV180retfo ;
   private GxObjectCollection AV225IT0A ;
   private GxObjectCollection AV228IT0C ;
   private GxObjectCollection AV230IT0D ;
   private GxObjectCollection AV232IT0E ;
   private GxObjectCollection AV234IT0G ;
   private Sdtsdt_RETFOP_sdt_RETFOPItem AV181retfo ;
   private SdtIT0A_IT0AItem AV224IT0A_ ;
   private SdtIT0C_IT0CItem AV227IT0C_ ;
   private SdtIT0D_IT0DItem AV229IT0D_ ;
   private SdtIT0E_IT0EItem AV231IT0E_ ;
   private SdtIT0G_IT0GItem AV233IT0G_ ;
   private SdtSDT_HOT10 AV235Hot10 ;
   private String[] aP0 ;
   private String[] aP1 ;
   private IDataStoreProvider pr_default ;
   private String[] P005W2_A994HntLin ;
   private boolean[] P005W2_n994HntLin ;
   private String[] P005W2_A993HntUsu ;
   private boolean[] P005W2_n993HntUsu ;
   private String[] P005W2_A997HntSeq ;
   private byte[] P005W2_A998HntSub ;
   private String[] P005W10_A963ISOC ;
   private String[] P005W10_A964CiaCod ;
   private String[] P005W10_A965PER_NA ;
   private String[] P005W10_A966CODE ;
   private String[] P005W10_A967IATA ;
   private String[] P005W10_A968NUM_BI ;
   private String[] P005W10_A969TIPO_V ;
   private java.util.Date[] P005W10_A970DATA ;
   private String[] P005W10_A973Tax_Co ;
   private byte[] P005W10_A974Tax_Wh ;
   private String[] P005W10_A975Tax_Pd ;
   private double[] P005W10_A942Tax_Am ;
   private boolean[] P005W10_n942Tax_Am ;
   private String[] P005W13_A963ISOC ;
   private String[] P005W13_A964CiaCod ;
   private String[] P005W13_A965PER_NA ;
   private String[] P005W13_A966CODE ;
   private String[] P005W13_A967IATA ;
   private String[] P005W13_A968NUM_BI ;
   private String[] P005W13_A969TIPO_V ;
   private java.util.Date[] P005W13_A970DATA ;
   private String[] P005W13_A976Sub_Nu ;
   private String[] P005W13_A948Sub_De ;
   private boolean[] P005W13_n948Sub_De ;
   private String[] P005W13_A949Sub_De ;
   private boolean[] P005W13_n949Sub_De ;
   private String[] P005W13_A950Sub_De ;
   private boolean[] P005W13_n950Sub_De ;
   private String[] P005W16_A963ISOC ;
   private String[] P005W16_A964CiaCod ;
   private String[] P005W16_A965PER_NA ;
   private String[] P005W16_A966CODE ;
   private String[] P005W16_A967IATA ;
   private String[] P005W16_A968NUM_BI ;
   private String[] P005W16_A969TIPO_V ;
   private java.util.Date[] P005W16_A970DATA ;
   private byte[] P005W16_A977FRCASe ;
   private String[] P005W16_A951FRCA ;
   private boolean[] P005W16_n951FRCA ;
   private String[] P005W18_A1231lccbT ;
   private String[] P005W18_A1150lccbE ;
   private String[] P005W18_A1222lccbI ;
   private java.util.Date[] P005W18_A1223lccbD ;
   private String[] P005W18_A1224lccbC ;
   private String[] P005W18_A1225lccbC ;
   private String[] P005W18_A1226lccbA ;
   private String[] P005W18_A1227lccbO ;
   private String[] P005W18_A1228lccbF ;
   private String[] P005W18_A1232lccbT ;
}

final  class pretinsert__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P005W2", "SELECT [HntLine], [HntUsuCod], [HntSeq], [HntSub] FROM [RETSPECTEMP] WITH (NOLOCK) WHERE [HntUsuCod] = 'RETClone' or [HntUsuCod] = 'RETCloneRFN' or SUBSTRING([HntLine], 1, 1) = '1' or SUBSTRING([HntLine], 1, 1) = 'Z' ORDER BY [HntSeq] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005W3", "INSERT INTO [HOT] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [VALOR_USD], [COMMISSION], [LIT_COMM], [AIRPT_TAX], [TAX_1], [TAX_2], [TOUR_CODE], [PAX_NAME], [TAX_IT], [TRANS_NO], [PLP_DOC], [NUMBER], [ISSUE_INFO], [PAYMT_INFO], [MOEDA], [NUM_BIL2], [FCMI], [INLS], [LoaderVers], [ID_AGENCY], [OVER_COMM], [TAX_BR], [TAX_DE], [TAX_XT], [TAX_US], [TAX_OT], [TAX_CH], [TAX_XX], [TAX_CP], [OVER_INCE], [ADM_ACM], [CC_TYPE], [CCCF], [DUE_AGT_AA], [DESC_PERCT], [RA_NUMBER], [CONJ_TO], [VOIDTKT], [IVAAmou], [CPUI], [HOT_FORM], [EFRT], [END_DATE], [BEG_DATE], [TCNR], [TCND], [ESAC], [NAME_RET], [DATA_HEADER], [CODE2], [SETT_DATE], [HOTREMARK]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(int, 0), convert(int, 0), convert(int, 0), '', convert(int, 0), convert(int, 0), '', '', convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', '', '', '', convert( DATETIME, '17530101', 112 ), '', convert( DATETIME, '17530101', 112 ), '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W4", "UPDATE [HOT] SET [VALOR_USD]=?, [COMMISSION]=?, [LIT_COMM]=?, [TOUR_CODE]=?, [PAX_NAME]=?, [TRANS_NO]=?, [PLP_DOC]=?, [ISSUE_INFO]=?, [MOEDA]=?, [NUM_BIL2]=?, [TAX_1]=?, [TAX_2]=?, [FCMI]=?, [INLS]=?, [NUMBER]=?, [LoaderVers]='RET' + SUBSTRING(?, 1, 7), [AIRPT_TAX]=?  WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W5", "INSERT INTO [HOT1] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [ISeq], [Num_BilP], [I_From], [I_To], [Carrier], [Tariff], [Flight], [Claxss], [Fli_DateC], [Fli_DateD], [Fli_Time], [STPO]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W6", "UPDATE [HOT1] SET [Num_BilP]=?, [I_From]=?, [I_To]=?, [Carrier]=?, [Tariff]=?, [Flight]=?, [Claxss]=?, [Fli_DateC]=?, [Fli_DateD]=?  WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [ISeq] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W7", "INSERT INTO [HOT2] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [SeqPag], [PGCC_TYPE], [PGCCCF], [PGAMOUNT], [PGTIP_VEND], [PGINFO], [PGMOEDA], [PGCCNUMBER], [APLC], [EXDA], [PGCCMASKED]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W8", "UPDATE [HOT2] SET [PGCC_TYPE]=SUBSTRING(?, 3, 2), [PGCCCF]='', [PGAMOUNT]=CAST(CONVERT( DECIMAL(28,14), ?) AS decimal( 20, 5)) / 100, [PGTIP_VEND]=SUBSTRING(?, 1, 2), [PGINFO]=?, [PGCCNUMBER]=?, [PGMOEDA]=SUBSTRING(?, 1, 3)  WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [SeqPag] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W9", "INSERT INTO [HOT3] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Tax_Cod], [Tax_Wh], [Tax_Pd], [Tax_Amt], [Tax_Hot]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005W10", "SELECT [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Tax_Cod], [Tax_Wh], [Tax_Pd], [Tax_Amt] FROM [HOT3] WITH (UPDLOCK) WHERE ([ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [Tax_Cod] = ? AND [Tax_Wh] = ? AND [Tax_Pd] = ?) AND ([ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [Tax_Cod] = ? and [Tax_Wh] = ? and [Tax_Pd] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P005W11", "UPDATE [HOT3] SET [Tax_Amt]=?  WHERE [ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [Tax_Cod] = ? AND [Tax_Wh] = ? AND [Tax_Pd] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W12", "INSERT INTO [HOT4] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Sub_NumTkt], [Sub_Desc1], [Sub_Desc2], [Sub_Desc3], [Sub_tptkt], [Sub_Amt1], [Sub_Amt2], [Sub_Amt3]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005W13", "SELECT [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Sub_NumTkt], [Sub_Desc1], [Sub_Desc2], [Sub_Desc3] FROM [HOT4] WITH (UPDLOCK) WHERE ([ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [Sub_NumTkt] = ?) AND ([ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [Sub_NumTkt] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P005W14", "UPDATE [HOT4] SET [Sub_Desc1]=?, [Sub_Desc2]=?, [Sub_Desc3]=?  WHERE [ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [Sub_NumTkt] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005W15", "INSERT INTO [HOT5] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [FRCASeq], [FRCA]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005W16", "SELECT [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [FRCASeq], [FRCA] FROM [HOT5] WITH (UPDLOCK) WHERE ([ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [FRCASeq] = ?) AND ([ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [FRCASeq] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P005W17", "UPDATE [HOT5] SET [FRCA]=?  WHERE [ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [FRCASeq] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005W18", "SELECT [lccbTDNR], [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbTDNR] = ? ORDER BY [lccbTDNR] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 8) ;
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((byte[]) buf[9])[0] = rslt.getByte(10) ;
               ((String[]) buf[10])[0] = rslt.getString(11, 10) ;
               ((double[]) buf[11])[0] = rslt.getDouble(12) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 20) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(10) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               break;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((byte[]) buf[8])[0] = rslt.getByte(9) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 87) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 7) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 19) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(9, ((Number) parms[9]).doubleValue());
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(10, ((Number) parms[11]).doubleValue());
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[13]).doubleValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(14, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(15, (String)parms[21], 100);
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(16, (String)parms[23], 50);
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(17, ((Number) parms[25]).doubleValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(18, (String)parms[27], 20);
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(19, (String)parms[29], 20);
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(20, (String)parms[31], 20);
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(21, (String)parms[33], 100);
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(22, (String)parms[35], 100);
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(23, (String)parms[37], 5);
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(24, ((Number) parms[39]).longValue());
               }
               if ( ((Boolean) parms[40]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(25, (String)parms[41], 1);
               }
               if ( ((Boolean) parms[42]).booleanValue() )
               {
                  stmt.setNull( 26 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(26, (String)parms[43], 4);
               }
               if ( ((Boolean) parms[44]).booleanValue() )
               {
                  stmt.setNull( 27 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(27, (String)parms[45], 10);
               }
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(1, ((Number) parms[1]).doubleValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(2, ((Number) parms[3]).doubleValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(3, ((Number) parms[5]).doubleValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[7], 100);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(6, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(7, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(8, (String)parms[15], 100);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 5);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(10, ((Number) parms[19]).longValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[25], 1);
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(14, (String)parms[27], 4);
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(15, (String)parms[29], 20);
               }
               stmt.setString(16, (String)parms[30], 10);
               if ( ((Boolean) parms[31]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(17, ((Number) parms[32]).doubleValue());
               }
               stmt.setString(18, (String)parms[33], 2);
               stmt.setString(19, (String)parms[34], 20);
               stmt.setString(20, (String)parms[35], 20);
               stmt.setString(21, (String)parms[36], 20);
               stmt.setString(22, (String)parms[37], 20);
               stmt.setString(23, (String)parms[38], 20);
               stmt.setString(24, (String)parms[39], 20);
               stmt.setDate(25, (java.util.Date)parms[40]);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setInt(9, ((Number) parms[8]).intValue());
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(10, ((Number) parms[10]).longValue());
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 20);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[14], 20);
               }
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(13, (String)parms[16], 20);
               }
               if ( ((Boolean) parms[17]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(14, (String)parms[18], 30);
               }
               if ( ((Boolean) parms[19]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(15, (String)parms[20], 15);
               }
               if ( ((Boolean) parms[21]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(16, (String)parms[22], 2);
               }
               if ( ((Boolean) parms[23]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(17, (String)parms[24], 5);
               }
               if ( ((Boolean) parms[25]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(18, (java.util.Date)parms[26]);
               }
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(1, ((Number) parms[1]).longValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 20);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[7], 20);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[9], 30);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(6, (String)parms[11], 15);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 2);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 5);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(9, (java.util.Date)parms[17]);
               }
               stmt.setString(10, (String)parms[18], 2);
               stmt.setString(11, (String)parms[19], 20);
               stmt.setString(12, (String)parms[20], 20);
               stmt.setString(13, (String)parms[21], 20);
               stmt.setString(14, (String)parms[22], 20);
               stmt.setString(15, (String)parms[23], 20);
               stmt.setString(16, (String)parms[24], 20);
               stmt.setDate(17, (java.util.Date)parms[25]);
               stmt.setInt(18, ((Number) parms[26]).intValue());
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setInt(9, ((Number) parms[8]).intValue());
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[10], 2);
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 30);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[14]).doubleValue());
               }
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[16], 2);
               }
               if ( ((Boolean) parms[17]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(14, (String)parms[18], 32);
               }
               if ( ((Boolean) parms[19]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(15, (String)parms[20], 3);
               }
               if ( ((Boolean) parms[21]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(16, (String)parms[22], 1024);
               }
               break;
            case 6 :
               stmt.setString(1, (String)parms[0], 4);
               stmt.setString(2, (String)parms[1], 11);
               stmt.setString(3, (String)parms[2], 4);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[4], 32);
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[6], 1024);
               }
               stmt.setString(6, (String)parms[7], 4);
               stmt.setString(7, (String)parms[8], 2);
               stmt.setString(8, (String)parms[9], 20);
               stmt.setString(9, (String)parms[10], 20);
               stmt.setString(10, (String)parms[11], 20);
               stmt.setString(11, (String)parms[12], 20);
               stmt.setString(12, (String)parms[13], 20);
               stmt.setString(13, (String)parms[14], 20);
               stmt.setDate(14, (java.util.Date)parms[15]);
               stmt.setInt(15, ((Number) parms[16]).intValue());
               break;
            case 7 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setByte(10, ((Number) parms[9]).byteValue());
               stmt.setString(11, (String)parms[10], 10);
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[12]).doubleValue());
               }
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setByte(10, ((Number) parms[9]).byteValue());
               stmt.setString(11, (String)parms[10], 10);
               stmt.setString(12, (String)parms[11], 2);
               stmt.setString(13, (String)parms[12], 20);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 20);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setString(17, (String)parms[16], 20);
               stmt.setString(18, (String)parms[17], 20);
               stmt.setDate(19, (java.util.Date)parms[18]);
               stmt.setString(20, (String)parms[19], 10);
               stmt.setByte(21, ((Number) parms[20]).byteValue());
               stmt.setString(22, (String)parms[21], 10);
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(1, ((Number) parms[1]).doubleValue());
               }
               stmt.setString(2, (String)parms[2], 2);
               stmt.setString(3, (String)parms[3], 20);
               stmt.setString(4, (String)parms[4], 20);
               stmt.setString(5, (String)parms[5], 20);
               stmt.setString(6, (String)parms[6], 20);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 20);
               stmt.setDate(9, (java.util.Date)parms[9]);
               stmt.setString(10, (String)parms[10], 10);
               stmt.setByte(11, ((Number) parms[11]).byteValue());
               stmt.setString(12, (String)parms[12], 10);
               break;
            case 10 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 20);
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(10, (String)parms[10], 40);
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 40);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[14], 40);
               }
               break;
            case 11 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 20);
               stmt.setString(10, (String)parms[9], 2);
               stmt.setString(11, (String)parms[10], 20);
               stmt.setString(12, (String)parms[11], 20);
               stmt.setString(13, (String)parms[12], 20);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 20);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setDate(17, (java.util.Date)parms[16]);
               stmt.setString(18, (String)parms[17], 20);
               break;
            case 12 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 40);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 40);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 40);
               }
               stmt.setString(4, (String)parms[6], 2);
               stmt.setString(5, (String)parms[7], 20);
               stmt.setString(6, (String)parms[8], 20);
               stmt.setString(7, (String)parms[9], 20);
               stmt.setString(8, (String)parms[10], 20);
               stmt.setString(9, (String)parms[11], 20);
               stmt.setString(10, (String)parms[12], 20);
               stmt.setDate(11, (java.util.Date)parms[13]);
               stmt.setString(12, (String)parms[14], 20);
               break;
            case 13 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setByte(9, ((Number) parms[8]).byteValue());
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[10], 87);
               }
               break;
            case 14 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setByte(9, ((Number) parms[8]).byteValue());
               stmt.setString(10, (String)parms[9], 2);
               stmt.setString(11, (String)parms[10], 20);
               stmt.setString(12, (String)parms[11], 20);
               stmt.setString(13, (String)parms[12], 20);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 20);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setDate(17, (java.util.Date)parms[16]);
               stmt.setByte(18, ((Number) parms[17]).byteValue());
               break;
            case 15 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 87);
               }
               stmt.setString(2, (String)parms[2], 2);
               stmt.setString(3, (String)parms[3], 20);
               stmt.setString(4, (String)parms[4], 20);
               stmt.setString(5, (String)parms[5], 20);
               stmt.setString(6, (String)parms[6], 20);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 20);
               stmt.setDate(9, (java.util.Date)parms[9]);
               stmt.setByte(10, ((Number) parms[10]).byteValue());
               break;
            case 16 :
               stmt.setString(1, (String)parms[0], 10);
               break;
      }
   }

}


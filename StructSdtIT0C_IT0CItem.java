
public final  class StructSdtIT0C_IT0CItem implements Cloneable, java.io.Serializable
{
   public StructSdtIT0C_IT0CItem( )
   {
      gxTv_SdtIT0C_IT0CItem_Plid_it0c = "" ;
      gxTv_SdtIT0C_IT0CItem_Pltx_it0c = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getPlid_it0c( )
   {
      return gxTv_SdtIT0C_IT0CItem_Plid_it0c ;
   }

   public void setPlid_it0c( String value )
   {
      gxTv_SdtIT0C_IT0CItem_Plid_it0c = value ;
      return  ;
   }

   public String getPltx_it0c( )
   {
      return gxTv_SdtIT0C_IT0CItem_Pltx_it0c ;
   }

   public void setPltx_it0c( String value )
   {
      gxTv_SdtIT0C_IT0CItem_Pltx_it0c = value ;
      return  ;
   }

   protected String gxTv_SdtIT0C_IT0CItem_Plid_it0c ;
   protected String gxTv_SdtIT0C_IT0CItem_Pltx_it0c ;
}


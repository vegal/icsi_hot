
public final  class StructSdtUserProfiles_Level1Item implements Cloneable, java.io.Serializable
{
   public StructSdtUserProfiles_Level1Item( )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode = "" ;
      gxTv_SdtUserProfiles_Level1Item_Uspdescription = "" ;
      gxTv_SdtUserProfiles_Level1Item_Mode = "" ;
      gxTv_SdtUserProfiles_Level1Item_Modified = (short)(0) ;
      gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = "" ;
      gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getUspcode( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspcode ;
   }

   public void setUspcode( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode = value ;
      return  ;
   }

   public String getUspdescription( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspdescription ;
   }

   public void setUspdescription( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspdescription = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtUserProfiles_Level1Item_Modified = value ;
      return  ;
   }

   public String getUspcode_Z( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspcode_Z ;
   }

   public void setUspcode_Z( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = value ;
      return  ;
   }

   public String getUspdescription_Z( )
   {
      return gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z ;
   }

   public void setUspdescription_Z( String value )
   {
      gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = value ;
      return  ;
   }

   protected short gxTv_SdtUserProfiles_Level1Item_Modified ;
   protected String gxTv_SdtUserProfiles_Level1Item_Mode ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspcode ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspdescription ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspcode_Z ;
   protected String gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtAirlines extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtAirlines( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtAirlines.class));
   }

   public SdtAirlines( int remoteHandle ,
                       ModelContext context )
   {
      super( context, "SdtAirlines");
      initialize( remoteHandle) ;
   }

   public SdtAirlines( int remoteHandle ,
                       StructSdtAirlines struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV23ISOCod ,
                     String AV1136AirLineCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV23ISOCod,AV1136AirLineCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Isocod = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinedescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Isodes = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineAbbreviation") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlineabbreviation = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirlineAddress") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlineaddress = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineAddressCompl") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlineaddresscompl = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCity") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecity = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineState") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinestate = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineZIP") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinezip = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineDatIns") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtAirlines_Airlinedatins = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtAirlines_Airlinedatins = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineDatMod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtAirlines_Airlinedatmod = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtAirlines_Airlinedatmod = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCompleteAddress") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecompleteaddress = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineTypeSettlement") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinetypesettlement = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineSta") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinesta = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirlineBankTransferMode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinebanktransfermode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Level1") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtAirlines_Level1.readxml(oReader, "Level1") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Contacts") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtAirlines_Contacts.readxml(oReader, "Contacts") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Isocod_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinedescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Isodes_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineAbbreviation_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlineabbreviation_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirlineAddress_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlineaddress_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineAddressCompl_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlineaddresscompl_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCity_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecity_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineState_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinestate_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineZIP_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinezip_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineDatIns_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtAirlines_Airlinedatins_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtAirlines_Airlinedatins_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineDatMod_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtAirlines_Airlinedatmod_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtAirlines_Airlinedatmod_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCompleteAddress_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecompleteaddress_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineTypeSettlement_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinetypesettlement_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineSta_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinesta_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirlineBankTransferMode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinebanktransfermode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Isodes_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCompleteAddress_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineSta_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinesta_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirlineBankTransferMode_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Airlines" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ISOCod", GXutil.rtrim( gxTv_SdtAirlines_Isocod));
      oWriter.writeElement("AirLineCode", GXutil.rtrim( gxTv_SdtAirlines_Airlinecode));
      oWriter.writeElement("AirLineDescription", GXutil.rtrim( gxTv_SdtAirlines_Airlinedescription));
      oWriter.writeElement("ISODes", GXutil.rtrim( gxTv_SdtAirlines_Isodes));
      oWriter.writeElement("AirLineAbbreviation", GXutil.rtrim( gxTv_SdtAirlines_Airlineabbreviation));
      oWriter.writeElement("AirlineAddress", GXutil.rtrim( gxTv_SdtAirlines_Airlineaddress));
      oWriter.writeElement("AirLineAddressCompl", GXutil.rtrim( gxTv_SdtAirlines_Airlineaddresscompl));
      oWriter.writeElement("AirLineCity", GXutil.rtrim( gxTv_SdtAirlines_Airlinecity));
      oWriter.writeElement("AirLineState", GXutil.rtrim( gxTv_SdtAirlines_Airlinestate));
      oWriter.writeElement("AirLineZIP", GXutil.rtrim( gxTv_SdtAirlines_Airlinezip));
      if ( (GXutil.nullDate().equals(gxTv_SdtAirlines_Airlinedatins)) )
      {
         oWriter.writeElement("AirLineDatIns", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtAirlines_Airlinedatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtAirlines_Airlinedatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtAirlines_Airlinedatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtAirlines_Airlinedatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtAirlines_Airlinedatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtAirlines_Airlinedatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("AirLineDatIns", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtAirlines_Airlinedatmod)) )
      {
         oWriter.writeElement("AirLineDatMod", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtAirlines_Airlinedatmod), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtAirlines_Airlinedatmod), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtAirlines_Airlinedatmod), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtAirlines_Airlinedatmod), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtAirlines_Airlinedatmod), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtAirlines_Airlinedatmod), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("AirLineDatMod", sDateCnv);
      }
      oWriter.writeElement("AirLineCompleteAddress", GXutil.rtrim( gxTv_SdtAirlines_Airlinecompleteaddress));
      oWriter.writeElement("AirLineTypeSettlement", GXutil.rtrim( gxTv_SdtAirlines_Airlinetypesettlement));
      oWriter.writeElement("AirLineSta", GXutil.rtrim( gxTv_SdtAirlines_Airlinesta));
      oWriter.writeElement("AirlineBankTransferMode", GXutil.rtrim( gxTv_SdtAirlines_Airlinebanktransfermode));
      gxTv_SdtAirlines_Level1.writexml(oWriter, "Level1", "IataICSI");
      gxTv_SdtAirlines_Contacts.writexml(oWriter, "Contacts", "IataICSI");
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtAirlines_Mode));
      oWriter.writeElement("ISOCod_Z", GXutil.rtrim( gxTv_SdtAirlines_Isocod_Z));
      oWriter.writeElement("AirLineCode_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinecode_Z));
      oWriter.writeElement("AirLineDescription_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinedescription_Z));
      oWriter.writeElement("ISODes_Z", GXutil.rtrim( gxTv_SdtAirlines_Isodes_Z));
      oWriter.writeElement("AirLineAbbreviation_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlineabbreviation_Z));
      oWriter.writeElement("AirlineAddress_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlineaddress_Z));
      oWriter.writeElement("AirLineAddressCompl_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlineaddresscompl_Z));
      oWriter.writeElement("AirLineCity_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinecity_Z));
      oWriter.writeElement("AirLineState_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinestate_Z));
      oWriter.writeElement("AirLineZIP_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinezip_Z));
      if ( (GXutil.nullDate().equals(gxTv_SdtAirlines_Airlinedatins_Z)) )
      {
         oWriter.writeElement("AirLineDatIns_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtAirlines_Airlinedatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtAirlines_Airlinedatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtAirlines_Airlinedatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtAirlines_Airlinedatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtAirlines_Airlinedatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtAirlines_Airlinedatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("AirLineDatIns_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtAirlines_Airlinedatmod_Z)) )
      {
         oWriter.writeElement("AirLineDatMod_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtAirlines_Airlinedatmod_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtAirlines_Airlinedatmod_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtAirlines_Airlinedatmod_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtAirlines_Airlinedatmod_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtAirlines_Airlinedatmod_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtAirlines_Airlinedatmod_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("AirLineDatMod_Z", sDateCnv);
      }
      oWriter.writeElement("AirLineCompleteAddress_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinecompleteaddress_Z));
      oWriter.writeElement("AirLineTypeSettlement_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinetypesettlement_Z));
      oWriter.writeElement("AirLineSta_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinesta_Z));
      oWriter.writeElement("AirlineBankTransferMode_Z", GXutil.rtrim( gxTv_SdtAirlines_Airlinebanktransfermode_Z));
      oWriter.writeElement("ISODes_N", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Isodes_N, 1, 0)));
      oWriter.writeElement("AirLineCompleteAddress_N", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Airlinecompleteaddress_N, 1, 0)));
      oWriter.writeElement("AirLineSta_N", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Airlinesta_N, 1, 0)));
      oWriter.writeElement("AirlineBankTransferMode_N", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Airlinebanktransfermode_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtAirlines_Isocod( )
   {
      return gxTv_SdtAirlines_Isocod ;
   }

   public void setgxTv_SdtAirlines_Isocod( String value )
   {
      gxTv_SdtAirlines_Isocod = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Isocod_SetNull( )
   {
      gxTv_SdtAirlines_Isocod = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinecode( )
   {
      return gxTv_SdtAirlines_Airlinecode ;
   }

   public void setgxTv_SdtAirlines_Airlinecode( String value )
   {
      gxTv_SdtAirlines_Airlinecode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecode_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecode = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinedescription( )
   {
      return gxTv_SdtAirlines_Airlinedescription ;
   }

   public void setgxTv_SdtAirlines_Airlinedescription( String value )
   {
      gxTv_SdtAirlines_Airlinedescription = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinedescription_SetNull( )
   {
      gxTv_SdtAirlines_Airlinedescription = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Isodes( )
   {
      return gxTv_SdtAirlines_Isodes ;
   }

   public void setgxTv_SdtAirlines_Isodes( String value )
   {
      gxTv_SdtAirlines_Isodes_N = (byte)(0) ;
      gxTv_SdtAirlines_Isodes = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Isodes_SetNull( )
   {
      gxTv_SdtAirlines_Isodes_N = (byte)(1) ;
      gxTv_SdtAirlines_Isodes = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlineabbreviation( )
   {
      return gxTv_SdtAirlines_Airlineabbreviation ;
   }

   public void setgxTv_SdtAirlines_Airlineabbreviation( String value )
   {
      gxTv_SdtAirlines_Airlineabbreviation = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlineabbreviation_SetNull( )
   {
      gxTv_SdtAirlines_Airlineabbreviation = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlineaddress( )
   {
      return gxTv_SdtAirlines_Airlineaddress ;
   }

   public void setgxTv_SdtAirlines_Airlineaddress( String value )
   {
      gxTv_SdtAirlines_Airlineaddress = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlineaddress_SetNull( )
   {
      gxTv_SdtAirlines_Airlineaddress = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlineaddresscompl( )
   {
      return gxTv_SdtAirlines_Airlineaddresscompl ;
   }

   public void setgxTv_SdtAirlines_Airlineaddresscompl( String value )
   {
      gxTv_SdtAirlines_Airlineaddresscompl = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlineaddresscompl_SetNull( )
   {
      gxTv_SdtAirlines_Airlineaddresscompl = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinecity( )
   {
      return gxTv_SdtAirlines_Airlinecity ;
   }

   public void setgxTv_SdtAirlines_Airlinecity( String value )
   {
      gxTv_SdtAirlines_Airlinecity = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecity_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecity = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinestate( )
   {
      return gxTv_SdtAirlines_Airlinestate ;
   }

   public void setgxTv_SdtAirlines_Airlinestate( String value )
   {
      gxTv_SdtAirlines_Airlinestate = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinestate_SetNull( )
   {
      gxTv_SdtAirlines_Airlinestate = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinezip( )
   {
      return gxTv_SdtAirlines_Airlinezip ;
   }

   public void setgxTv_SdtAirlines_Airlinezip( String value )
   {
      gxTv_SdtAirlines_Airlinezip = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinezip_SetNull( )
   {
      gxTv_SdtAirlines_Airlinezip = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtAirlines_Airlinedatins( )
   {
      return gxTv_SdtAirlines_Airlinedatins ;
   }

   public void setgxTv_SdtAirlines_Airlinedatins( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatins = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinedatins_SetNull( )
   {
      gxTv_SdtAirlines_Airlinedatins = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public java.util.Date getgxTv_SdtAirlines_Airlinedatmod( )
   {
      return gxTv_SdtAirlines_Airlinedatmod ;
   }

   public void setgxTv_SdtAirlines_Airlinedatmod( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatmod = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinedatmod_SetNull( )
   {
      gxTv_SdtAirlines_Airlinedatmod = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinecompleteaddress( )
   {
      return gxTv_SdtAirlines_Airlinecompleteaddress ;
   }

   public void setgxTv_SdtAirlines_Airlinecompleteaddress( String value )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinecompleteaddress = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecompleteaddress_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(1) ;
      gxTv_SdtAirlines_Airlinecompleteaddress = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinetypesettlement( )
   {
      return gxTv_SdtAirlines_Airlinetypesettlement ;
   }

   public void setgxTv_SdtAirlines_Airlinetypesettlement( String value )
   {
      gxTv_SdtAirlines_Airlinetypesettlement = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinetypesettlement_SetNull( )
   {
      gxTv_SdtAirlines_Airlinetypesettlement = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinesta( )
   {
      return gxTv_SdtAirlines_Airlinesta ;
   }

   public void setgxTv_SdtAirlines_Airlinesta( String value )
   {
      gxTv_SdtAirlines_Airlinesta_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinesta = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinesta_SetNull( )
   {
      gxTv_SdtAirlines_Airlinesta_N = (byte)(1) ;
      gxTv_SdtAirlines_Airlinesta = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinebanktransfermode( )
   {
      return gxTv_SdtAirlines_Airlinebanktransfermode ;
   }

   public void setgxTv_SdtAirlines_Airlinebanktransfermode( String value )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinebanktransfermode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinebanktransfermode_SetNull( )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(1) ;
      gxTv_SdtAirlines_Airlinebanktransfermode = "" ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtAirlines_Level1( )
   {
      return gxTv_SdtAirlines_Level1 ;
   }

   public void setgxTv_SdtAirlines_Level1( GxSilentTrnGridCollection value )
   {
      gxTv_SdtAirlines_Level1 = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1_SetNull( )
   {
      gxTv_SdtAirlines_Level1 = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtAirlines_Contacts( )
   {
      return gxTv_SdtAirlines_Contacts ;
   }

   public void setgxTv_SdtAirlines_Contacts( GxSilentTrnGridCollection value )
   {
      gxTv_SdtAirlines_Contacts = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Contacts_SetNull( )
   {
      gxTv_SdtAirlines_Contacts = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Mode( )
   {
      return gxTv_SdtAirlines_Mode ;
   }

   public void setgxTv_SdtAirlines_Mode( String value )
   {
      gxTv_SdtAirlines_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Mode_SetNull( )
   {
      gxTv_SdtAirlines_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Isocod_Z( )
   {
      return gxTv_SdtAirlines_Isocod_Z ;
   }

   public void setgxTv_SdtAirlines_Isocod_Z( String value )
   {
      gxTv_SdtAirlines_Isocod_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Isocod_Z_SetNull( )
   {
      gxTv_SdtAirlines_Isocod_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinecode_Z( )
   {
      return gxTv_SdtAirlines_Airlinecode_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinecode_Z( String value )
   {
      gxTv_SdtAirlines_Airlinecode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecode_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinedescription_Z( )
   {
      return gxTv_SdtAirlines_Airlinedescription_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinedescription_Z( String value )
   {
      gxTv_SdtAirlines_Airlinedescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinedescription_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinedescription_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Isodes_Z( )
   {
      return gxTv_SdtAirlines_Isodes_Z ;
   }

   public void setgxTv_SdtAirlines_Isodes_Z( String value )
   {
      gxTv_SdtAirlines_Isodes_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Isodes_Z_SetNull( )
   {
      gxTv_SdtAirlines_Isodes_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlineabbreviation_Z( )
   {
      return gxTv_SdtAirlines_Airlineabbreviation_Z ;
   }

   public void setgxTv_SdtAirlines_Airlineabbreviation_Z( String value )
   {
      gxTv_SdtAirlines_Airlineabbreviation_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlineabbreviation_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlineabbreviation_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlineaddress_Z( )
   {
      return gxTv_SdtAirlines_Airlineaddress_Z ;
   }

   public void setgxTv_SdtAirlines_Airlineaddress_Z( String value )
   {
      gxTv_SdtAirlines_Airlineaddress_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlineaddress_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlineaddress_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlineaddresscompl_Z( )
   {
      return gxTv_SdtAirlines_Airlineaddresscompl_Z ;
   }

   public void setgxTv_SdtAirlines_Airlineaddresscompl_Z( String value )
   {
      gxTv_SdtAirlines_Airlineaddresscompl_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlineaddresscompl_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlineaddresscompl_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinecity_Z( )
   {
      return gxTv_SdtAirlines_Airlinecity_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinecity_Z( String value )
   {
      gxTv_SdtAirlines_Airlinecity_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecity_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecity_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinestate_Z( )
   {
      return gxTv_SdtAirlines_Airlinestate_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinestate_Z( String value )
   {
      gxTv_SdtAirlines_Airlinestate_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinestate_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinestate_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinezip_Z( )
   {
      return gxTv_SdtAirlines_Airlinezip_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinezip_Z( String value )
   {
      gxTv_SdtAirlines_Airlinezip_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinezip_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinezip_Z = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtAirlines_Airlinedatins_Z( )
   {
      return gxTv_SdtAirlines_Airlinedatins_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinedatins_Z( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatins_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinedatins_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinedatins_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public java.util.Date getgxTv_SdtAirlines_Airlinedatmod_Z( )
   {
      return gxTv_SdtAirlines_Airlinedatmod_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinedatmod_Z( java.util.Date value )
   {
      gxTv_SdtAirlines_Airlinedatmod_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinedatmod_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinedatmod_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinecompleteaddress_Z( )
   {
      return gxTv_SdtAirlines_Airlinecompleteaddress_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinecompleteaddress_Z( String value )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecompleteaddress_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinetypesettlement_Z( )
   {
      return gxTv_SdtAirlines_Airlinetypesettlement_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinetypesettlement_Z( String value )
   {
      gxTv_SdtAirlines_Airlinetypesettlement_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinetypesettlement_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinetypesettlement_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinesta_Z( )
   {
      return gxTv_SdtAirlines_Airlinesta_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinesta_Z( String value )
   {
      gxTv_SdtAirlines_Airlinesta_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinesta_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinesta_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Airlinebanktransfermode_Z( )
   {
      return gxTv_SdtAirlines_Airlinebanktransfermode_Z ;
   }

   public void setgxTv_SdtAirlines_Airlinebanktransfermode_Z( String value )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinebanktransfermode_Z_SetNull( )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtAirlines_Isodes_N( )
   {
      return gxTv_SdtAirlines_Isodes_N ;
   }

   public void setgxTv_SdtAirlines_Isodes_N( byte value )
   {
      gxTv_SdtAirlines_Isodes_N = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Isodes_N_SetNull( )
   {
      gxTv_SdtAirlines_Isodes_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtAirlines_Airlinecompleteaddress_N( )
   {
      return gxTv_SdtAirlines_Airlinecompleteaddress_N ;
   }

   public void setgxTv_SdtAirlines_Airlinecompleteaddress_N( byte value )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_N = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinecompleteaddress_N_SetNull( )
   {
      gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtAirlines_Airlinesta_N( )
   {
      return gxTv_SdtAirlines_Airlinesta_N ;
   }

   public void setgxTv_SdtAirlines_Airlinesta_N( byte value )
   {
      gxTv_SdtAirlines_Airlinesta_N = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinesta_N_SetNull( )
   {
      gxTv_SdtAirlines_Airlinesta_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtAirlines_Airlinebanktransfermode_N( )
   {
      return gxTv_SdtAirlines_Airlinebanktransfermode_N ;
   }

   public void setgxTv_SdtAirlines_Airlinebanktransfermode_N( byte value )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_N = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Airlinebanktransfermode_N_SetNull( )
   {
      gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tairlines_bc obj ;
      obj = new tairlines_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtAirlines_Isocod = "" ;
      gxTv_SdtAirlines_Airlinecode = "" ;
      gxTv_SdtAirlines_Airlinedescription = "" ;
      gxTv_SdtAirlines_Isodes = "" ;
      gxTv_SdtAirlines_Airlineabbreviation = "" ;
      gxTv_SdtAirlines_Airlineaddress = "" ;
      gxTv_SdtAirlines_Airlineaddresscompl = "" ;
      gxTv_SdtAirlines_Airlinecity = "" ;
      gxTv_SdtAirlines_Airlinestate = "" ;
      gxTv_SdtAirlines_Airlinezip = "" ;
      gxTv_SdtAirlines_Airlinedatins = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtAirlines_Airlinedatmod = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtAirlines_Airlinecompleteaddress = "" ;
      gxTv_SdtAirlines_Airlinetypesettlement = "" ;
      gxTv_SdtAirlines_Airlinesta = "" ;
      gxTv_SdtAirlines_Airlinebanktransfermode = "" ;
      gxTv_SdtAirlines_Level1 = new GxSilentTrnGridCollection(SdtAirlines_Level1Item.class, "Airlines.Level1Item", "IataICSI");
      gxTv_SdtAirlines_Contacts = new GxSilentTrnGridCollection(SdtAirlines_Contacts.class, "Airlines.Contacts", "IataICSI");
      gxTv_SdtAirlines_Mode = "" ;
      gxTv_SdtAirlines_Isocod_Z = "" ;
      gxTv_SdtAirlines_Airlinecode_Z = "" ;
      gxTv_SdtAirlines_Airlinedescription_Z = "" ;
      gxTv_SdtAirlines_Isodes_Z = "" ;
      gxTv_SdtAirlines_Airlineabbreviation_Z = "" ;
      gxTv_SdtAirlines_Airlineaddress_Z = "" ;
      gxTv_SdtAirlines_Airlineaddresscompl_Z = "" ;
      gxTv_SdtAirlines_Airlinecity_Z = "" ;
      gxTv_SdtAirlines_Airlinestate_Z = "" ;
      gxTv_SdtAirlines_Airlinezip_Z = "" ;
      gxTv_SdtAirlines_Airlinedatins_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtAirlines_Airlinedatmod_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtAirlines_Airlinecompleteaddress_Z = "" ;
      gxTv_SdtAirlines_Airlinetypesettlement_Z = "" ;
      gxTv_SdtAirlines_Airlinesta_Z = "" ;
      gxTv_SdtAirlines_Airlinebanktransfermode_Z = "" ;
      gxTv_SdtAirlines_Isodes_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinesta_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      sDateCnv = "" ;
      sNumToPad = "" ;
      return  ;
   }

   public SdtAirlines Clone( )
   {
      SdtAirlines sdt ;
      tairlines_bc obj ;
      sdt = (SdtAirlines)(clone()) ;
      obj = (tairlines_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtAirlines struct )
   {
      setgxTv_SdtAirlines_Isocod(struct.getIsocod());
      setgxTv_SdtAirlines_Airlinecode(struct.getAirlinecode());
      setgxTv_SdtAirlines_Airlinedescription(struct.getAirlinedescription());
      setgxTv_SdtAirlines_Isodes(struct.getIsodes());
      setgxTv_SdtAirlines_Airlineabbreviation(struct.getAirlineabbreviation());
      setgxTv_SdtAirlines_Airlineaddress(struct.getAirlineaddress());
      setgxTv_SdtAirlines_Airlineaddresscompl(struct.getAirlineaddresscompl());
      setgxTv_SdtAirlines_Airlinecity(struct.getAirlinecity());
      setgxTv_SdtAirlines_Airlinestate(struct.getAirlinestate());
      setgxTv_SdtAirlines_Airlinezip(struct.getAirlinezip());
      setgxTv_SdtAirlines_Airlinedatins(struct.getAirlinedatins());
      setgxTv_SdtAirlines_Airlinedatmod(struct.getAirlinedatmod());
      setgxTv_SdtAirlines_Airlinecompleteaddress(struct.getAirlinecompleteaddress());
      setgxTv_SdtAirlines_Airlinetypesettlement(struct.getAirlinetypesettlement());
      setgxTv_SdtAirlines_Airlinesta(struct.getAirlinesta());
      setgxTv_SdtAirlines_Airlinebanktransfermode(struct.getAirlinebanktransfermode());
      setgxTv_SdtAirlines_Level1(new GxSilentTrnGridCollection(SdtAirlines_Level1Item.class, "Airlines.Level1Item", "IataICSI", struct.getLevel1()));
      setgxTv_SdtAirlines_Contacts(new GxSilentTrnGridCollection(SdtAirlines_Contacts.class, "Airlines.Contacts", "IataICSI", struct.getContacts()));
      setgxTv_SdtAirlines_Mode(struct.getMode());
      setgxTv_SdtAirlines_Isocod_Z(struct.getIsocod_Z());
      setgxTv_SdtAirlines_Airlinecode_Z(struct.getAirlinecode_Z());
      setgxTv_SdtAirlines_Airlinedescription_Z(struct.getAirlinedescription_Z());
      setgxTv_SdtAirlines_Isodes_Z(struct.getIsodes_Z());
      setgxTv_SdtAirlines_Airlineabbreviation_Z(struct.getAirlineabbreviation_Z());
      setgxTv_SdtAirlines_Airlineaddress_Z(struct.getAirlineaddress_Z());
      setgxTv_SdtAirlines_Airlineaddresscompl_Z(struct.getAirlineaddresscompl_Z());
      setgxTv_SdtAirlines_Airlinecity_Z(struct.getAirlinecity_Z());
      setgxTv_SdtAirlines_Airlinestate_Z(struct.getAirlinestate_Z());
      setgxTv_SdtAirlines_Airlinezip_Z(struct.getAirlinezip_Z());
      setgxTv_SdtAirlines_Airlinedatins_Z(struct.getAirlinedatins_Z());
      setgxTv_SdtAirlines_Airlinedatmod_Z(struct.getAirlinedatmod_Z());
      setgxTv_SdtAirlines_Airlinecompleteaddress_Z(struct.getAirlinecompleteaddress_Z());
      setgxTv_SdtAirlines_Airlinetypesettlement_Z(struct.getAirlinetypesettlement_Z());
      setgxTv_SdtAirlines_Airlinesta_Z(struct.getAirlinesta_Z());
      setgxTv_SdtAirlines_Airlinebanktransfermode_Z(struct.getAirlinebanktransfermode_Z());
      setgxTv_SdtAirlines_Isodes_N(struct.getIsodes_N());
      setgxTv_SdtAirlines_Airlinecompleteaddress_N(struct.getAirlinecompleteaddress_N());
      setgxTv_SdtAirlines_Airlinesta_N(struct.getAirlinesta_N());
      setgxTv_SdtAirlines_Airlinebanktransfermode_N(struct.getAirlinebanktransfermode_N());
   }

   public StructSdtAirlines getStruct( )
   {
      StructSdtAirlines struct = new StructSdtAirlines ();
      struct.setIsocod(getgxTv_SdtAirlines_Isocod());
      struct.setAirlinecode(getgxTv_SdtAirlines_Airlinecode());
      struct.setAirlinedescription(getgxTv_SdtAirlines_Airlinedescription());
      struct.setIsodes(getgxTv_SdtAirlines_Isodes());
      struct.setAirlineabbreviation(getgxTv_SdtAirlines_Airlineabbreviation());
      struct.setAirlineaddress(getgxTv_SdtAirlines_Airlineaddress());
      struct.setAirlineaddresscompl(getgxTv_SdtAirlines_Airlineaddresscompl());
      struct.setAirlinecity(getgxTv_SdtAirlines_Airlinecity());
      struct.setAirlinestate(getgxTv_SdtAirlines_Airlinestate());
      struct.setAirlinezip(getgxTv_SdtAirlines_Airlinezip());
      struct.setAirlinedatins(getgxTv_SdtAirlines_Airlinedatins());
      struct.setAirlinedatmod(getgxTv_SdtAirlines_Airlinedatmod());
      struct.setAirlinecompleteaddress(getgxTv_SdtAirlines_Airlinecompleteaddress());
      struct.setAirlinetypesettlement(getgxTv_SdtAirlines_Airlinetypesettlement());
      struct.setAirlinesta(getgxTv_SdtAirlines_Airlinesta());
      struct.setAirlinebanktransfermode(getgxTv_SdtAirlines_Airlinebanktransfermode());
      struct.setLevel1(getgxTv_SdtAirlines_Level1().getStruct());
      struct.setContacts(getgxTv_SdtAirlines_Contacts().getStruct());
      struct.setMode(getgxTv_SdtAirlines_Mode());
      struct.setIsocod_Z(getgxTv_SdtAirlines_Isocod_Z());
      struct.setAirlinecode_Z(getgxTv_SdtAirlines_Airlinecode_Z());
      struct.setAirlinedescription_Z(getgxTv_SdtAirlines_Airlinedescription_Z());
      struct.setIsodes_Z(getgxTv_SdtAirlines_Isodes_Z());
      struct.setAirlineabbreviation_Z(getgxTv_SdtAirlines_Airlineabbreviation_Z());
      struct.setAirlineaddress_Z(getgxTv_SdtAirlines_Airlineaddress_Z());
      struct.setAirlineaddresscompl_Z(getgxTv_SdtAirlines_Airlineaddresscompl_Z());
      struct.setAirlinecity_Z(getgxTv_SdtAirlines_Airlinecity_Z());
      struct.setAirlinestate_Z(getgxTv_SdtAirlines_Airlinestate_Z());
      struct.setAirlinezip_Z(getgxTv_SdtAirlines_Airlinezip_Z());
      struct.setAirlinedatins_Z(getgxTv_SdtAirlines_Airlinedatins_Z());
      struct.setAirlinedatmod_Z(getgxTv_SdtAirlines_Airlinedatmod_Z());
      struct.setAirlinecompleteaddress_Z(getgxTv_SdtAirlines_Airlinecompleteaddress_Z());
      struct.setAirlinetypesettlement_Z(getgxTv_SdtAirlines_Airlinetypesettlement_Z());
      struct.setAirlinesta_Z(getgxTv_SdtAirlines_Airlinesta_Z());
      struct.setAirlinebanktransfermode_Z(getgxTv_SdtAirlines_Airlinebanktransfermode_Z());
      struct.setIsodes_N(getgxTv_SdtAirlines_Isodes_N());
      struct.setAirlinecompleteaddress_N(getgxTv_SdtAirlines_Airlinecompleteaddress_N());
      struct.setAirlinesta_N(getgxTv_SdtAirlines_Airlinesta_N());
      struct.setAirlinebanktransfermode_N(getgxTv_SdtAirlines_Airlinebanktransfermode_N());
      return struct ;
   }

   protected byte gxTv_SdtAirlines_Isodes_N ;
   protected byte gxTv_SdtAirlines_Airlinecompleteaddress_N ;
   protected byte gxTv_SdtAirlines_Airlinesta_N ;
   protected byte gxTv_SdtAirlines_Airlinebanktransfermode_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtAirlines_Airlinetypesettlement ;
   protected String gxTv_SdtAirlines_Airlinesta ;
   protected String gxTv_SdtAirlines_Airlinebanktransfermode ;
   protected String gxTv_SdtAirlines_Mode ;
   protected String gxTv_SdtAirlines_Airlinetypesettlement_Z ;
   protected String gxTv_SdtAirlines_Airlinesta_Z ;
   protected String gxTv_SdtAirlines_Airlinebanktransfermode_Z ;
   protected String sTagName ;
   protected String GXt_char2 ;
   protected String sDateCnv ;
   protected String sNumToPad ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatins ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatmod ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatins_Z ;
   protected java.util.Date gxTv_SdtAirlines_Airlinedatmod_Z ;
   protected String gxTv_SdtAirlines_Isocod ;
   protected String gxTv_SdtAirlines_Airlinecode ;
   protected String gxTv_SdtAirlines_Airlinedescription ;
   protected String gxTv_SdtAirlines_Isodes ;
   protected String gxTv_SdtAirlines_Airlineabbreviation ;
   protected String gxTv_SdtAirlines_Airlineaddress ;
   protected String gxTv_SdtAirlines_Airlineaddresscompl ;
   protected String gxTv_SdtAirlines_Airlinecity ;
   protected String gxTv_SdtAirlines_Airlinestate ;
   protected String gxTv_SdtAirlines_Airlinezip ;
   protected String gxTv_SdtAirlines_Airlinecompleteaddress ;
   protected String gxTv_SdtAirlines_Isocod_Z ;
   protected String gxTv_SdtAirlines_Airlinecode_Z ;
   protected String gxTv_SdtAirlines_Airlinedescription_Z ;
   protected String gxTv_SdtAirlines_Isodes_Z ;
   protected String gxTv_SdtAirlines_Airlineabbreviation_Z ;
   protected String gxTv_SdtAirlines_Airlineaddress_Z ;
   protected String gxTv_SdtAirlines_Airlineaddresscompl_Z ;
   protected String gxTv_SdtAirlines_Airlinecity_Z ;
   protected String gxTv_SdtAirlines_Airlinestate_Z ;
   protected String gxTv_SdtAirlines_Airlinezip_Z ;
   protected String gxTv_SdtAirlines_Airlinecompleteaddress_Z ;
   protected GxSilentTrnGridCollection gxTv_SdtAirlines_Level1 ;
   protected GxSilentTrnGridCollection gxTv_SdtAirlines_Contacts ;
}


import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx06c220 extends GXSubfileElement
{
   private String ContactTypesCode ;
   private short AirLineCttSeq ;
   private String AirLineCttName ;
   private String ISOCod ;
   private String ISOCodDescription ;
   private String AirLineCode ;
   public String getContactTypesCode( )
   {
      return ContactTypesCode ;
   }

   public void setContactTypesCode( String value )
   {
      ContactTypesCode = value;
   }

   public short getAirLineCttSeq( )
   {
      return AirLineCttSeq ;
   }

   public void setAirLineCttSeq( short value )
   {
      AirLineCttSeq = value;
   }

   public String getAirLineCttName( )
   {
      return AirLineCttName ;
   }

   public void setAirLineCttName( String value )
   {
      AirLineCttName = value;
   }

   public String getISOCod( )
   {
      return ISOCod ;
   }

   public void setISOCod( String value )
   {
      ISOCod = value;
   }

   public String getISOCodDescription( )
   {
      return ISOCodDescription ;
   }

   public void setISOCodDescription( String value )
   {
      ISOCodDescription = value;
   }

   public String getAirLineCode( )
   {
      return AirLineCode ;
   }

   public void setAirLineCode( String value )
   {
      AirLineCode = value;
   }

   public void clear( )
   {
      ContactTypesCode = "" ;
      AirLineCttSeq = 0 ;
      AirLineCttName = "" ;
      ISOCod = "" ;
      AirLineCode = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getContactTypesCode().compareTo(((subwgx06c220) element).getContactTypesCode()) ;
            case 1 :
               if ( getAirLineCttSeq() > ((subwgx06c220) element).getAirLineCttSeq() ) return 1;
               if ( getAirLineCttSeq() < ((subwgx06c220) element).getAirLineCttSeq() ) return -1;
               return 0;
            case 2 :
               return  getAirLineCttName().compareTo(((subwgx06c220) element).getAirLineCttName()) ;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getContactTypesCode(), "") == 0 ) && ( getAirLineCttSeq() == 0 ) && ( GXutil.strcmp(getAirLineCttName(), "") == 0 ) && ( GXutil.strcmp(getISOCod(), "") == 0 ) && ( GXutil.strcmp(getAirLineCode(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getContactTypesCode() );
            break;
         case 1 :
            cell.setValue( getAirLineCttSeq() );
            break;
         case 2 :
            cell.setValue( getAirLineCttName() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getContactTypesCode()) == 0) );
         case 1 :
            return ( (((GUIObjectShort) cell).getValue() == getAirLineCttSeq()) );
         case 2 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCttName()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setContactTypesCode(value.getStringValue());
               break;
            case 1 :
               setAirLineCttSeq(value.getShortValue());
               break;
            case 2 :
               setAirLineCttName(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setContactTypesCode(((subwgx06c220) element).getContactTypesCode());
               return;
            case 1 :
               setAirLineCttSeq(((subwgx06c220) element).getAirLineCttSeq());
               return;
            case 2 :
               setAirLineCttName(((subwgx06c220) element).getAirLineCttName());
               return;
      }
   }

}


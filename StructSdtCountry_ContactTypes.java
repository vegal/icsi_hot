
public final  class StructSdtCountry_ContactTypes implements Cloneable, java.io.Serializable
{
   public StructSdtCountry_ContactTypes( )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttseq = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Isocttname = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttphone = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttemail = "" ;
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription = "" ;
      gxTv_SdtCountry_ContactTypes_Mode = "" ;
      gxTv_SdtCountry_ContactTypes_Modified = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttseq_Z = (short)(0) ;
      gxTv_SdtCountry_ContactTypes_Isocttname_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttphone_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Isocttemail_Z = "" ;
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getContacttypescode( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypescode ;
   }

   public void setContacttypescode( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode = value ;
      return  ;
   }

   public short getIsocttseq( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttseq ;
   }

   public void setIsocttseq( short value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttseq = value ;
      return  ;
   }

   public String getIsocttname( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttname ;
   }

   public void setIsocttname( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttname = value ;
      return  ;
   }

   public String getIsocttphone( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttphone ;
   }

   public void setIsocttphone( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttphone = value ;
      return  ;
   }

   public String getIsocttemail( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttemail ;
   }

   public void setIsocttemail( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttemail = value ;
      return  ;
   }

   public String getContacttypesdescription( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypesdescription ;
   }

   public void setContacttypesdescription( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtCountry_ContactTypes_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCountry_ContactTypes_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtCountry_ContactTypes_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtCountry_ContactTypes_Modified = value ;
      return  ;
   }

   public String getContacttypescode_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypescode_Z ;
   }

   public void setContacttypescode_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypescode_Z = value ;
      return  ;
   }

   public short getIsocttseq_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttseq_Z ;
   }

   public void setIsocttseq_Z( short value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttseq_Z = value ;
      return  ;
   }

   public String getIsocttname_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttname_Z ;
   }

   public void setIsocttname_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttname_Z = value ;
      return  ;
   }

   public String getIsocttphone_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttphone_Z ;
   }

   public void setIsocttphone_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttphone_Z = value ;
      return  ;
   }

   public String getIsocttemail_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Isocttemail_Z ;
   }

   public void setIsocttemail_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Isocttemail_Z = value ;
      return  ;
   }

   public String getContacttypesdescription_Z( )
   {
      return gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z ;
   }

   public void setContacttypesdescription_Z( String value )
   {
      gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z = value ;
      return  ;
   }

   protected short gxTv_SdtCountry_ContactTypes_Isocttseq ;
   protected short gxTv_SdtCountry_ContactTypes_Modified ;
   protected short gxTv_SdtCountry_ContactTypes_Isocttseq_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Mode ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypescode ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttname ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttphone ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttemail ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypesdescription ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypescode_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttname_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttphone_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Isocttemail_Z ;
   protected String gxTv_SdtCountry_ContactTypes_Contacttypesdescription_Z ;
}


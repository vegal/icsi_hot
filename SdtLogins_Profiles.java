import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtLogins_Profiles extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtLogins_Profiles( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtLogins_Profiles.class));
   }

   public SdtLogins_Profiles( int remoteHandle ,
                              ModelContext context )
   {
      super( context, "SdtLogins_Profiles");
      initialize( remoteHandle) ;
   }

   public SdtLogins_Profiles( int remoteHandle ,
                              StructSdtLogins_Profiles struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtLogins_Profiles( )
   {
      super( new ModelContext(SdtLogins_Profiles.class), "SdtLogins_Profiles");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Ustcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Ustdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Ustcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Ustdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustDescription_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Logins.Profiles" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ustCode", GXutil.rtrim( gxTv_SdtLogins_Profiles_Ustcode));
      oWriter.writeElement("ustDescription", GXutil.rtrim( gxTv_SdtLogins_Profiles_Ustdescription));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtLogins_Profiles_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtLogins_Profiles_Modified, 4, 0)));
      oWriter.writeElement("ustCode_Z", GXutil.rtrim( gxTv_SdtLogins_Profiles_Ustcode_Z));
      oWriter.writeElement("ustDescription_Z", GXutil.rtrim( gxTv_SdtLogins_Profiles_Ustdescription_Z));
      oWriter.writeElement("ustDescription_N", GXutil.trim( GXutil.str( gxTv_SdtLogins_Profiles_Ustdescription_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtLogins_Profiles_Ustcode( )
   {
      return gxTv_SdtLogins_Profiles_Ustcode ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustcode( String value )
   {
      gxTv_SdtLogins_Profiles_Ustcode = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustcode_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Ustcode = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Profiles_Ustdescription( )
   {
      return gxTv_SdtLogins_Profiles_Ustdescription ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustdescription( String value )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(0) ;
      gxTv_SdtLogins_Profiles_Ustdescription = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustdescription_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(1) ;
      gxTv_SdtLogins_Profiles_Ustdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Profiles_Mode( )
   {
      return gxTv_SdtLogins_Profiles_Mode ;
   }

   public void setgxTv_SdtLogins_Profiles_Mode( String value )
   {
      gxTv_SdtLogins_Profiles_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Mode_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtLogins_Profiles_Modified( )
   {
      return gxTv_SdtLogins_Profiles_Modified ;
   }

   public void setgxTv_SdtLogins_Profiles_Modified( short value )
   {
      gxTv_SdtLogins_Profiles_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Modified_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtLogins_Profiles_Ustcode_Z( )
   {
      return gxTv_SdtLogins_Profiles_Ustcode_Z ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustcode_Z( String value )
   {
      gxTv_SdtLogins_Profiles_Ustcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustcode_Z_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Ustcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtLogins_Profiles_Ustdescription_Z( )
   {
      return gxTv_SdtLogins_Profiles_Ustdescription_Z ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustdescription_Z( String value )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustdescription_Z_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtLogins_Profiles_Ustdescription_N( )
   {
      return gxTv_SdtLogins_Profiles_Ustdescription_N ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustdescription_N( byte value )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_N = value ;
      return  ;
   }

   public void setgxTv_SdtLogins_Profiles_Ustdescription_N_SetNull( )
   {
      gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtLogins_Profiles_Ustcode = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription = "" ;
      gxTv_SdtLogins_Profiles_Mode = "" ;
      gxTv_SdtLogins_Profiles_Modified = (short)(0) ;
      gxTv_SdtLogins_Profiles_Ustcode_Z = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription_Z = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char3 = "" ;
      return  ;
   }

   public SdtLogins_Profiles Clone( )
   {
      return (SdtLogins_Profiles)(clone()) ;
   }

   public void setStruct( StructSdtLogins_Profiles struct )
   {
      setgxTv_SdtLogins_Profiles_Ustcode(struct.getUstcode());
      setgxTv_SdtLogins_Profiles_Ustdescription(struct.getUstdescription());
      setgxTv_SdtLogins_Profiles_Mode(struct.getMode());
      setgxTv_SdtLogins_Profiles_Modified(struct.getModified());
      setgxTv_SdtLogins_Profiles_Ustcode_Z(struct.getUstcode_Z());
      setgxTv_SdtLogins_Profiles_Ustdescription_Z(struct.getUstdescription_Z());
      setgxTv_SdtLogins_Profiles_Ustdescription_N(struct.getUstdescription_N());
   }

   public StructSdtLogins_Profiles getStruct( )
   {
      StructSdtLogins_Profiles struct = new StructSdtLogins_Profiles ();
      struct.setUstcode(getgxTv_SdtLogins_Profiles_Ustcode());
      struct.setUstdescription(getgxTv_SdtLogins_Profiles_Ustdescription());
      struct.setMode(getgxTv_SdtLogins_Profiles_Mode());
      struct.setModified(getgxTv_SdtLogins_Profiles_Modified());
      struct.setUstcode_Z(getgxTv_SdtLogins_Profiles_Ustcode_Z());
      struct.setUstdescription_Z(getgxTv_SdtLogins_Profiles_Ustdescription_Z());
      struct.setUstdescription_N(getgxTv_SdtLogins_Profiles_Ustdescription_N());
      return struct ;
   }

   protected byte gxTv_SdtLogins_Profiles_Ustdescription_N ;
   protected short gxTv_SdtLogins_Profiles_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtLogins_Profiles_Mode ;
   protected String sTagName ;
   protected String GXt_char3 ;
   protected String gxTv_SdtLogins_Profiles_Ustcode ;
   protected String gxTv_SdtLogins_Profiles_Ustdescription ;
   protected String gxTv_SdtLogins_Profiles_Ustcode_Z ;
   protected String gxTv_SdtLogins_Profiles_Ustdescription_Z ;
}


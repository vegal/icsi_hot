import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtConfig extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtConfig( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtConfig.class));
   }

   public SdtConfig( int remoteHandle ,
                     ModelContext context )
   {
      super( context, "SdtConfig");
      initialize( remoteHandle) ;
   }

   public SdtConfig( int remoteHandle ,
                     StructSdtConfig struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV19ConfigID )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV19ConfigID});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ConfigID") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Configid = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ConfigValue") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Configvalue = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ConfigDes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Configdes = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ConfigID_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Configid_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ConfigValue_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Configvalue_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ConfigDes_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtConfig_Configdes_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Config" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ConfigID", GXutil.rtrim( gxTv_SdtConfig_Configid));
      oWriter.writeElement("ConfigValue", GXutil.rtrim( gxTv_SdtConfig_Configvalue));
      oWriter.writeElement("ConfigDes", GXutil.rtrim( gxTv_SdtConfig_Configdes));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtConfig_Mode));
      oWriter.writeElement("ConfigID_Z", GXutil.rtrim( gxTv_SdtConfig_Configid_Z));
      oWriter.writeElement("ConfigValue_Z", GXutil.rtrim( gxTv_SdtConfig_Configvalue_Z));
      oWriter.writeElement("ConfigDes_Z", GXutil.rtrim( gxTv_SdtConfig_Configdes_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtConfig_Configid( )
   {
      return gxTv_SdtConfig_Configid ;
   }

   public void setgxTv_SdtConfig_Configid( String value )
   {
      gxTv_SdtConfig_Configid = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Configid_SetNull( )
   {
      gxTv_SdtConfig_Configid = "" ;
      return  ;
   }

   public String getgxTv_SdtConfig_Configvalue( )
   {
      return gxTv_SdtConfig_Configvalue ;
   }

   public void setgxTv_SdtConfig_Configvalue( String value )
   {
      gxTv_SdtConfig_Configvalue = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Configvalue_SetNull( )
   {
      gxTv_SdtConfig_Configvalue = "" ;
      return  ;
   }

   public String getgxTv_SdtConfig_Configdes( )
   {
      return gxTv_SdtConfig_Configdes ;
   }

   public void setgxTv_SdtConfig_Configdes( String value )
   {
      gxTv_SdtConfig_Configdes = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Configdes_SetNull( )
   {
      gxTv_SdtConfig_Configdes = "" ;
      return  ;
   }

   public String getgxTv_SdtConfig_Mode( )
   {
      return gxTv_SdtConfig_Mode ;
   }

   public void setgxTv_SdtConfig_Mode( String value )
   {
      gxTv_SdtConfig_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Mode_SetNull( )
   {
      gxTv_SdtConfig_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtConfig_Configid_Z( )
   {
      return gxTv_SdtConfig_Configid_Z ;
   }

   public void setgxTv_SdtConfig_Configid_Z( String value )
   {
      gxTv_SdtConfig_Configid_Z = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Configid_Z_SetNull( )
   {
      gxTv_SdtConfig_Configid_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtConfig_Configvalue_Z( )
   {
      return gxTv_SdtConfig_Configvalue_Z ;
   }

   public void setgxTv_SdtConfig_Configvalue_Z( String value )
   {
      gxTv_SdtConfig_Configvalue_Z = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Configvalue_Z_SetNull( )
   {
      gxTv_SdtConfig_Configvalue_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtConfig_Configdes_Z( )
   {
      return gxTv_SdtConfig_Configdes_Z ;
   }

   public void setgxTv_SdtConfig_Configdes_Z( String value )
   {
      gxTv_SdtConfig_Configdes_Z = value ;
      return  ;
   }

   public void setgxTv_SdtConfig_Configdes_Z_SetNull( )
   {
      gxTv_SdtConfig_Configdes_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tconfig_bc obj ;
      obj = new tconfig_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtConfig_Configid = "" ;
      gxTv_SdtConfig_Configvalue = "" ;
      gxTv_SdtConfig_Configdes = "" ;
      gxTv_SdtConfig_Mode = "" ;
      gxTv_SdtConfig_Configid_Z = "" ;
      gxTv_SdtConfig_Configvalue_Z = "" ;
      gxTv_SdtConfig_Configdes_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtConfig Clone( )
   {
      SdtConfig sdt ;
      tconfig_bc obj ;
      sdt = (SdtConfig)(clone()) ;
      obj = (tconfig_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtConfig struct )
   {
      setgxTv_SdtConfig_Configid(struct.getConfigid());
      setgxTv_SdtConfig_Configvalue(struct.getConfigvalue());
      setgxTv_SdtConfig_Configdes(struct.getConfigdes());
      setgxTv_SdtConfig_Mode(struct.getMode());
      setgxTv_SdtConfig_Configid_Z(struct.getConfigid_Z());
      setgxTv_SdtConfig_Configvalue_Z(struct.getConfigvalue_Z());
      setgxTv_SdtConfig_Configdes_Z(struct.getConfigdes_Z());
   }

   public StructSdtConfig getStruct( )
   {
      StructSdtConfig struct = new StructSdtConfig ();
      struct.setConfigid(getgxTv_SdtConfig_Configid());
      struct.setConfigvalue(getgxTv_SdtConfig_Configvalue());
      struct.setConfigdes(getgxTv_SdtConfig_Configdes());
      struct.setMode(getgxTv_SdtConfig_Mode());
      struct.setConfigid_Z(getgxTv_SdtConfig_Configid_Z());
      struct.setConfigvalue_Z(getgxTv_SdtConfig_Configvalue_Z());
      struct.setConfigdes_Z(getgxTv_SdtConfig_Configdes_Z());
      return struct ;
   }

   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtConfig_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtConfig_Configid ;
   protected String gxTv_SdtConfig_Configvalue ;
   protected String gxTv_SdtConfig_Configdes ;
   protected String gxTv_SdtConfig_Configid_Z ;
   protected String gxTv_SdtConfig_Configvalue_Z ;
   protected String gxTv_SdtConfig_Configdes_Z ;
}


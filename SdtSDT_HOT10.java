import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtSDT_HOT10 extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtSDT_HOT10( )
   {
      this(  new ModelContext(SdtSDT_HOT10.class));
   }

   public SdtSDT_HOT10( ModelContext context )
   {
      super( context, "SdtSDT_HOT10");
   }

   public SdtSDT_HOT10( StructSdtSDT_HOT10 struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOC") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Isoc = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CiaCod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Ciacod = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PER_NAME") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Per_name = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CODE") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Code = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "IATA") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Iata = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "NUM_BIL") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Num_bil = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "TIPO_VEND") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_Tipo_vend = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "DATA") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtSDT_HOT10_Data = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtSDT_HOT10_Data = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "H10RecID") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_H10recid = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "H10Field") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_H10field = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "H10Seq") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_H10seq = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "H10Type") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_H10type = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "H10Content") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSDT_HOT10_H10content = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "SDT_HOT10" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ISOC", GXutil.rtrim( gxTv_SdtSDT_HOT10_Isoc));
      oWriter.writeElement("CiaCod", GXutil.rtrim( gxTv_SdtSDT_HOT10_Ciacod));
      oWriter.writeElement("PER_NAME", GXutil.rtrim( gxTv_SdtSDT_HOT10_Per_name));
      oWriter.writeElement("CODE", GXutil.rtrim( gxTv_SdtSDT_HOT10_Code));
      oWriter.writeElement("IATA", GXutil.rtrim( gxTv_SdtSDT_HOT10_Iata));
      oWriter.writeElement("NUM_BIL", GXutil.rtrim( gxTv_SdtSDT_HOT10_Num_bil));
      oWriter.writeElement("TIPO_VEND", GXutil.rtrim( gxTv_SdtSDT_HOT10_Tipo_vend));
      if ( (GXutil.nullDate().equals(gxTv_SdtSDT_HOT10_Data)) )
      {
         oWriter.writeElement("DATA", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtSDT_HOT10_Data), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtSDT_HOT10_Data), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtSDT_HOT10_Data), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("DATA", sDateCnv);
      }
      oWriter.writeElement("H10RecID", GXutil.rtrim( gxTv_SdtSDT_HOT10_H10recid));
      oWriter.writeElement("H10Field", GXutil.rtrim( gxTv_SdtSDT_HOT10_H10field));
      oWriter.writeElement("H10Seq", GXutil.rtrim( gxTv_SdtSDT_HOT10_H10seq));
      oWriter.writeElement("H10Type", GXutil.rtrim( gxTv_SdtSDT_HOT10_H10type));
      oWriter.writeElement("H10Content", GXutil.rtrim( gxTv_SdtSDT_HOT10_H10content));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Isoc( )
   {
      return gxTv_SdtSDT_HOT10_Isoc ;
   }

   public void setgxTv_SdtSDT_HOT10_Isoc( String value )
   {
      gxTv_SdtSDT_HOT10_Isoc = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Isoc_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Isoc = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Ciacod( )
   {
      return gxTv_SdtSDT_HOT10_Ciacod ;
   }

   public void setgxTv_SdtSDT_HOT10_Ciacod( String value )
   {
      gxTv_SdtSDT_HOT10_Ciacod = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Ciacod_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Ciacod = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Per_name( )
   {
      return gxTv_SdtSDT_HOT10_Per_name ;
   }

   public void setgxTv_SdtSDT_HOT10_Per_name( String value )
   {
      gxTv_SdtSDT_HOT10_Per_name = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Per_name_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Per_name = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Code( )
   {
      return gxTv_SdtSDT_HOT10_Code ;
   }

   public void setgxTv_SdtSDT_HOT10_Code( String value )
   {
      gxTv_SdtSDT_HOT10_Code = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Code_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Code = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Iata( )
   {
      return gxTv_SdtSDT_HOT10_Iata ;
   }

   public void setgxTv_SdtSDT_HOT10_Iata( String value )
   {
      gxTv_SdtSDT_HOT10_Iata = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Iata_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Iata = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Num_bil( )
   {
      return gxTv_SdtSDT_HOT10_Num_bil ;
   }

   public void setgxTv_SdtSDT_HOT10_Num_bil( String value )
   {
      gxTv_SdtSDT_HOT10_Num_bil = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Num_bil_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Num_bil = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_Tipo_vend( )
   {
      return gxTv_SdtSDT_HOT10_Tipo_vend ;
   }

   public void setgxTv_SdtSDT_HOT10_Tipo_vend( String value )
   {
      gxTv_SdtSDT_HOT10_Tipo_vend = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Tipo_vend_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Tipo_vend = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtSDT_HOT10_Data( )
   {
      return gxTv_SdtSDT_HOT10_Data ;
   }

   public void setgxTv_SdtSDT_HOT10_Data( java.util.Date value )
   {
      gxTv_SdtSDT_HOT10_Data = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_Data_SetNull( )
   {
      gxTv_SdtSDT_HOT10_Data = GXutil.nullDate() ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_H10recid( )
   {
      return gxTv_SdtSDT_HOT10_H10recid ;
   }

   public void setgxTv_SdtSDT_HOT10_H10recid( String value )
   {
      gxTv_SdtSDT_HOT10_H10recid = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_H10recid_SetNull( )
   {
      gxTv_SdtSDT_HOT10_H10recid = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_H10field( )
   {
      return gxTv_SdtSDT_HOT10_H10field ;
   }

   public void setgxTv_SdtSDT_HOT10_H10field( String value )
   {
      gxTv_SdtSDT_HOT10_H10field = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_H10field_SetNull( )
   {
      gxTv_SdtSDT_HOT10_H10field = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_H10seq( )
   {
      return gxTv_SdtSDT_HOT10_H10seq ;
   }

   public void setgxTv_SdtSDT_HOT10_H10seq( String value )
   {
      gxTv_SdtSDT_HOT10_H10seq = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_H10seq_SetNull( )
   {
      gxTv_SdtSDT_HOT10_H10seq = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_H10type( )
   {
      return gxTv_SdtSDT_HOT10_H10type ;
   }

   public void setgxTv_SdtSDT_HOT10_H10type( String value )
   {
      gxTv_SdtSDT_HOT10_H10type = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_H10type_SetNull( )
   {
      gxTv_SdtSDT_HOT10_H10type = "" ;
      return  ;
   }

   public String getgxTv_SdtSDT_HOT10_H10content( )
   {
      return gxTv_SdtSDT_HOT10_H10content ;
   }

   public void setgxTv_SdtSDT_HOT10_H10content( String value )
   {
      gxTv_SdtSDT_HOT10_H10content = value ;
      return  ;
   }

   public void setgxTv_SdtSDT_HOT10_H10content_SetNull( )
   {
      gxTv_SdtSDT_HOT10_H10content = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtSDT_HOT10_Isoc = "" ;
      gxTv_SdtSDT_HOT10_Ciacod = "" ;
      gxTv_SdtSDT_HOT10_Per_name = "" ;
      gxTv_SdtSDT_HOT10_Code = "" ;
      gxTv_SdtSDT_HOT10_Iata = "" ;
      gxTv_SdtSDT_HOT10_Num_bil = "" ;
      gxTv_SdtSDT_HOT10_Tipo_vend = "" ;
      gxTv_SdtSDT_HOT10_Data = GXutil.nullDate() ;
      gxTv_SdtSDT_HOT10_H10recid = "" ;
      gxTv_SdtSDT_HOT10_H10field = "" ;
      gxTv_SdtSDT_HOT10_H10seq = "" ;
      gxTv_SdtSDT_HOT10_H10type = "" ;
      gxTv_SdtSDT_HOT10_H10content = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      sDateCnv = "" ;
      sNumToPad = "" ;
      return  ;
   }

   public SdtSDT_HOT10 Clone( )
   {
      return (SdtSDT_HOT10)(clone()) ;
   }

   public void setStruct( StructSdtSDT_HOT10 struct )
   {
      setgxTv_SdtSDT_HOT10_Isoc(struct.getIsoc());
      setgxTv_SdtSDT_HOT10_Ciacod(struct.getCiacod());
      setgxTv_SdtSDT_HOT10_Per_name(struct.getPer_name());
      setgxTv_SdtSDT_HOT10_Code(struct.getCode());
      setgxTv_SdtSDT_HOT10_Iata(struct.getIata());
      setgxTv_SdtSDT_HOT10_Num_bil(struct.getNum_bil());
      setgxTv_SdtSDT_HOT10_Tipo_vend(struct.getTipo_vend());
      setgxTv_SdtSDT_HOT10_Data(struct.getData());
      setgxTv_SdtSDT_HOT10_H10recid(struct.getH10recid());
      setgxTv_SdtSDT_HOT10_H10field(struct.getH10field());
      setgxTv_SdtSDT_HOT10_H10seq(struct.getH10seq());
      setgxTv_SdtSDT_HOT10_H10type(struct.getH10type());
      setgxTv_SdtSDT_HOT10_H10content(struct.getH10content());
   }

   public StructSdtSDT_HOT10 getStruct( )
   {
      StructSdtSDT_HOT10 struct = new StructSdtSDT_HOT10 ();
      struct.setIsoc(getgxTv_SdtSDT_HOT10_Isoc());
      struct.setCiacod(getgxTv_SdtSDT_HOT10_Ciacod());
      struct.setPer_name(getgxTv_SdtSDT_HOT10_Per_name());
      struct.setCode(getgxTv_SdtSDT_HOT10_Code());
      struct.setIata(getgxTv_SdtSDT_HOT10_Iata());
      struct.setNum_bil(getgxTv_SdtSDT_HOT10_Num_bil());
      struct.setTipo_vend(getgxTv_SdtSDT_HOT10_Tipo_vend());
      struct.setData(getgxTv_SdtSDT_HOT10_Data());
      struct.setH10recid(getgxTv_SdtSDT_HOT10_H10recid());
      struct.setH10field(getgxTv_SdtSDT_HOT10_H10field());
      struct.setH10seq(getgxTv_SdtSDT_HOT10_H10seq());
      struct.setH10type(getgxTv_SdtSDT_HOT10_H10type());
      struct.setH10content(getgxTv_SdtSDT_HOT10_H10content());
      return struct ;
   }

   private short nOutParmCount ;
   private short readOk ;
   private String gxTv_SdtSDT_HOT10_Isoc ;
   private String gxTv_SdtSDT_HOT10_Tipo_vend ;
   private String gxTv_SdtSDT_HOT10_H10recid ;
   private String gxTv_SdtSDT_HOT10_H10field ;
   private String gxTv_SdtSDT_HOT10_H10seq ;
   private String gxTv_SdtSDT_HOT10_H10type ;
   private String sTagName ;
   private String GXt_char1 ;
   private String sDateCnv ;
   private String sNumToPad ;
   private java.util.Date gxTv_SdtSDT_HOT10_Data ;
   private String gxTv_SdtSDT_HOT10_Ciacod ;
   private String gxTv_SdtSDT_HOT10_Per_name ;
   private String gxTv_SdtSDT_HOT10_Code ;
   private String gxTv_SdtSDT_HOT10_Iata ;
   private String gxTv_SdtSDT_HOT10_Num_bil ;
   private String gxTv_SdtSDT_HOT10_H10content ;
}


import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx06n110 extends GXSubfileElement
{
   private int ICSI_CCError ;
   private String ICSI_CCConfCod ;
   public int getICSI_CCError( )
   {
      return ICSI_CCError ;
   }

   public void setICSI_CCError( int value )
   {
      ICSI_CCError = value;
   }

   public String getICSI_CCConfCod( )
   {
      return ICSI_CCConfCod ;
   }

   public void setICSI_CCConfCod( String value )
   {
      ICSI_CCConfCod = value;
   }

   public void clear( )
   {
      ICSI_CCError = 0 ;
      ICSI_CCConfCod = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               if ( getICSI_CCError() > ((subwgx06n110) element).getICSI_CCError() ) return 1;
               if ( getICSI_CCError() < ((subwgx06n110) element).getICSI_CCError() ) return -1;
               return 0;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( getICSI_CCError() == 0 ) && ( GXutil.strcmp(getICSI_CCConfCod(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getICSI_CCError() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( (((GUIObjectInt) cell).getValue() == getICSI_CCError()) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setICSI_CCError(value.getIntValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setICSI_CCError(((subwgx06n110) element).getICSI_CCError());
               return;
      }
   }

}


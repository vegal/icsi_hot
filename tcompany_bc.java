/*
               File: tcompany_bc
        Description: Company
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:0.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tcompany_bc extends GXWebPanel implements IGxSilentTrn
{
   public tcompany_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tcompany_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcompany_bc.class ));
   }

   public tcompany_bc( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z865Compan = A865Compan ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_020( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls02181( ) ;
         }
         else
         {
            checkExtendedTable02181( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors02181( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues020( ) ;
      }
   }

   public void e11022( )
   {
      /* 'Back' Routine */
   }

   public void zm02181( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         Z866Compan = A866Compan ;
      }
      if ( ( GX_JID == -2 ) )
      {
         Z865Compan = A865Compan ;
         Z866Compan = A866Compan ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load02181( )
   {
      /* Using cursor BC00024 */
      pr_default.execute(2, new Object[] {A865Compan});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound181 = (short)(1) ;
         A866Compan = BC00024_A866Compan[0] ;
         zm02181( -2) ;
      }
      pr_default.close(2);
      onLoadActions02181( ) ;
   }

   public void onLoadActions02181( )
   {
   }

   public void checkExtendedTable02181( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors02181( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey02181( )
   {
      /* Using cursor BC00025 */
      pr_default.execute(3, new Object[] {A865Compan});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound181 = (short)(1) ;
      }
      else
      {
         RcdFound181 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC00023 */
      pr_default.execute(1, new Object[] {A865Compan});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm02181( 2) ;
         RcdFound181 = (short)(1) ;
         A865Compan = BC00023_A865Compan[0] ;
         A866Compan = BC00023_A866Compan[0] ;
         Z865Compan = A865Compan ;
         sMode181 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load02181( ) ;
         Gx_mode = sMode181 ;
      }
      else
      {
         RcdFound181 = (short)(0) ;
         initializeNonKey02181( ) ;
         sMode181 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode181 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey02181( ) ;
      if ( ( RcdFound181 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_020( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency02181( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00022 */
         pr_default.execute(0, new Object[] {A865Compan});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"COMPANY"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z866Compan, BC00022_A866Compan[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"COMPANY"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert02181( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm02181( 0) ;
         checkOptimisticConcurrency02181( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm02181( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert02181( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00026 */
                  pr_default.execute(4, new Object[] {A865Compan, A866Compan});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load02181( ) ;
         }
         endLevel02181( ) ;
      }
      closeExtendedTableCursors02181( ) ;
   }

   public void update02181( )
   {
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency02181( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm02181( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate02181( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00027 */
                  pr_default.execute(5, new Object[] {A866Compan, A865Compan});
                  deferredUpdate02181( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel02181( ) ;
      }
      closeExtendedTableCursors02181( ) ;
   }

   public void deferredUpdate02181( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate02181( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls02181( ) ;
         /* No cascading delete specified. */
         afterConfirm02181( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete02181( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC00028 */
               pr_default.execute(6, new Object[] {A865Compan});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode181 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel02181( ) ;
      Gx_mode = sMode181 ;
   }

   public void onDeleteControls02181( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC00029 */
         pr_default.execute(7, new Object[] {A865Compan});
         if ( (pr_default.getStatus(7) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"File Downloads"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(7);
      }
   }

   public void endLevel02181( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete02181( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues020( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart02181( )
   {
      /* Using cursor BC000210 */
      pr_default.execute(8, new Object[] {A865Compan});
      RcdFound181 = (short)(0) ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound181 = (short)(1) ;
         A865Compan = BC000210_A865Compan[0] ;
         A866Compan = BC000210_A866Compan[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext02181( )
   {
      pr_default.readNext(8);
      RcdFound181 = (short)(0) ;
      scanLoad02181( ) ;
   }

   public void scanLoad02181( )
   {
      sMode181 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound181 = (short)(1) ;
         A865Compan = BC000210_A865Compan[0] ;
         A866Compan = BC000210_A866Compan[0] ;
      }
      Gx_mode = sMode181 ;
   }

   public void scanEnd02181( )
   {
      pr_default.close(8);
   }

   public void afterConfirm02181( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert02181( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate02181( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete02181( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete02181( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate02181( )
   {
      /* Before Validate Rules */
   }

   public void addRow02181( )
   {
      VarsToRow181( bcCompany) ;
   }

   public void sendRow02181( )
   {
   }

   public void readRow02181( )
   {
      RowToVars181( bcCompany, 0) ;
   }

   public void confirmValues020( )
   {
   }

   public void initializeNonKey02181( )
   {
      A866Compan = "" ;
   }

   public void initAll02181( )
   {
      A865Compan = "" ;
      initializeNonKey02181( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow181( SdtCompany obj181 )
   {
      obj181.setgxTv_SdtCompany_Mode( Gx_mode );
      obj181.setgxTv_SdtCompany_Companydes( A866Compan );
      obj181.setgxTv_SdtCompany_Companycod( A865Compan );
      obj181.setgxTv_SdtCompany_Companycod_Z( Z865Compan );
      obj181.setgxTv_SdtCompany_Companydes_Z( Z866Compan );
      obj181.setgxTv_SdtCompany_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars181( SdtCompany obj181 ,
                             int forceLoad )
   {
      Gx_mode = obj181.getgxTv_SdtCompany_Mode() ;
      A866Compan = obj181.getgxTv_SdtCompany_Companydes() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A865Compan = obj181.getgxTv_SdtCompany_Companycod() ;
      }
      Z865Compan = obj181.getgxTv_SdtCompany_Companycod_Z() ;
      Z866Compan = obj181.getgxTv_SdtCompany_Companydes_Z() ;
      Gx_mode = obj181.getgxTv_SdtCompany_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A865Compan = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey02181( ) ;
      scanStart02181( ) ;
      if ( ( RcdFound181 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z865Compan = A865Compan ;
      }
      onLoadActions02181( ) ;
      zm02181( 0) ;
      addRow02181( ) ;
      scanEnd02181( ) ;
      if ( ( RcdFound181 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars181( bcCompany, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey02181( ) ;
      if ( ( RcdFound181 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
         {
            A865Compan = Z865Compan ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update02181( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert02181( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert02181( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow181( bcCompany) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars181( bcCompany, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey02181( ) ;
      if ( ( RcdFound181 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
         {
            A865Compan = Z865Compan ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A865Compan, Z865Compan) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tcompany_bc");
      VarsToRow181( bcCompany) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcCompany.getgxTv_SdtCompany_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcCompany.setgxTv_SdtCompany_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtCompany sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcCompany ) )
      {
         bcCompany = sdt ;
         if ( ( GXutil.strcmp(bcCompany.getgxTv_SdtCompany_Mode(), "") == 0 ) )
         {
            bcCompany.setgxTv_SdtCompany_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow181( bcCompany) ;
         }
         else
         {
            RowToVars181( bcCompany, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcCompany.getgxTv_SdtCompany_Mode(), "") == 0 ) )
         {
            bcCompany.setgxTv_SdtCompany_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars181( bcCompany, 1) ;
      return  ;
   }

   public SdtCompany getCompany_BC( )
   {
      return bcCompany ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z865Compan = "" ;
      A865Compan = "" ;
      gxTv_SdtCompany_Companycod_Z = "" ;
      gxTv_SdtCompany_Companydes_Z = "" ;
      GX_JID = 0 ;
      Z866Compan = "" ;
      A866Compan = "" ;
      BC00024_A865Compan = new String[] {""} ;
      BC00024_A866Compan = new String[] {""} ;
      RcdFound181 = (short)(0) ;
      BC00025_A865Compan = new String[] {""} ;
      BC00023_A865Compan = new String[] {""} ;
      BC00023_A866Compan = new String[] {""} ;
      sMode181 = "" ;
      BC00022_A865Compan = new String[] {""} ;
      BC00022_A866Compan = new String[] {""} ;
      BC00029_A42Downloa = new long[1] ;
      BC000210_A865Compan = new String[] {""} ;
      BC000210_A866Compan = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tcompany_bc__default(),
         new Object[] {
             new Object[] {
            BC00022_A865Compan, BC00022_A866Compan
            }
            , new Object[] {
            BC00023_A865Compan, BC00023_A866Compan
            }
            , new Object[] {
            BC00024_A865Compan, BC00024_A866Compan
            }
            , new Object[] {
            BC00025_A865Compan
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC00029_A42Downloa
            }
            , new Object[] {
            BC000210_A865Compan, BC000210_A866Compan
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound181 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode181 ;
   private String Z865Compan ;
   private String A865Compan ;
   private String gxTv_SdtCompany_Companycod_Z ;
   private String gxTv_SdtCompany_Companydes_Z ;
   private String Z866Compan ;
   private String A866Compan ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtCompany bcCompany ;
   private IDataStoreProvider pr_default ;
   private String[] BC00024_A865Compan ;
   private String[] BC00024_A866Compan ;
   private String[] BC00025_A865Compan ;
   private String[] BC00023_A865Compan ;
   private String[] BC00023_A866Compan ;
   private String[] BC00022_A865Compan ;
   private String[] BC00022_A866Compan ;
   private long[] BC00029_A42Downloa ;
   private String[] BC000210_A865Compan ;
   private String[] BC000210_A866Compan ;
}

final  class tcompany_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00022", "SELECT [CompanyCod], [CompanyDes] FROM [COMPANY] WITH (UPDLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00023", "SELECT [CompanyCod], [CompanyDes] FROM [COMPANY] WITH (NOLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00024", "SELECT TM1.[CompanyCod], TM1.[CompanyDes] FROM [COMPANY] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[CompanyCod] = ? ORDER BY TM1.[CompanyCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00025", "SELECT [CompanyCod] FROM [COMPANY] WITH (FASTFIRSTROW NOLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC00026", "INSERT INTO [COMPANY] ([CompanyCod], [CompanyDes]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC00027", "UPDATE [COMPANY] SET [CompanyDes]=?  WHERE [CompanyCod] = ?", GX_NOMASK)
         ,new UpdateCursor("BC00028", "DELETE FROM [COMPANY]  WHERE [CompanyCod] = ?", GX_NOMASK)
         ,new ForEachCursor("BC00029", "SELECT TOP 1 [DownloadID] FROM [DOWNLOAD] WITH (NOLOCK) WHERE [CompanyCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000210", "SELECT TM1.[CompanyCod], TM1.[CompanyDes] FROM [COMPANY] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[CompanyCod] = ? ORDER BY TM1.[CompanyCod] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 40, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 40, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


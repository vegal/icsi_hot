/*
               File: NewMainICS
        Description: New Main ICS
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:55:33.10
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class anewmainics extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      anewmainics pgm = new anewmainics (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};
      String[] aP1 = new String[] {""};
      String[] aP2 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
         aP1[0] = (String) args[1];
         aP2[0] = (String) args[2];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0, aP1, aP2);
   }

   public anewmainics( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( anewmainics.class ), "" );
   }

   public anewmainics( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      anewmainics.this.AV8Menu = aP0[0];
      this.aP0 = aP0;
      anewmainics.this.AV15Submen = aP1[0];
      this.aP1 = aP1;
      anewmainics.this.AV17Modo = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      context.msgStatus( "*****************************************************************" );
      context.msgStatus( "*********************** Versao 1.00.14 **************************" );
      context.msgStatus( "***********************Inicio Processamento**********************" );
      context.msgStatus( "*****************************************************************" );
      AV25BCC = "" ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV23Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      if ( ( GXutil.strcmp(AV8Menu, "1") == 0 ) )
      {
         if ( ( GXutil.strcmp(AV15Submen, "1") == 0 ) || ( GXutil.strcmp(AV15Submen, "2") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            context.msgStatus( "***********************Inicio Processamento**********************" );
            context.msgStatus( "-MENU 1 - SUBMENU 1 - RET LOADER " );
            context.msgStatus( "***********************Inicio Processamento**********************" );
            context.msgStatus( "--MENU 1 - SUBMENU 1 - VALIDACAO UPLOAD FOLDER " );
            /* Execute user subroutine: S16289 */
            S16289 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            GXt_char1 = AV14Path ;
            GXv_svchar2[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "CAMINHO_ENTRADA_RETS", "Caminho entrada das Rets", "F", "C:\\Temp\\ICSI\\", GXv_svchar2) ;
            anewmainics.this.GXt_char1 = GXv_svchar2[0] ;
            AV14Path = GXt_char1 ;
            AV13Direto.setSource( GXutil.trim( AV14Path) );
            context.msgStatus( "--MENU 1 - SUBMENU 1 - LEITURA ARQUIVOS RET " );
            AV62GXV2 = 1 ;
            AV61GXV1 = AV13Direto.getFiles("") ;
            while ( ( AV62GXV2 <= AV61GXV1.getItemCount() ) )
            {
               AV12File = AV61GXV1.item((short)(AV62GXV2)) ;
               AV10Fileso = AV12File.getAbsoluteName() ;
               AV11ShortN = AV12File.getName() ;
               context.msgStatus( "--MENU 1 - SUBMENU 1 - PROCESSAMENTO RET "+GXutil.trim( AV11ShortN) );
               /* Execute user subroutine: S12173 */
               S12173 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               if ( ( GXutil.strcmp(AV30Erro_P, "N") == 0 ) )
               {
                  AV35FileSt = (byte)(2) ;
                  AV34FileRe = "Processing" ;
                  /* Execute user subroutine: S141 */
                  S141 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     cleanup();
                     if (true) return;
                  }
                  GXt_char1 = AV9DebugMo ;
                  GXv_svchar2[0] = GXt_char1 ;
                  new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_1_1", "Debug Mode Carregado Hot e LCCB", "S", "NORMAL-VERBOSE-ISOC-NORMAL-RETDUMP-KEEPFPACGC-ICSI", GXv_svchar2) ;
                  anewmainics.this.GXt_char1 = GXv_svchar2[0] ;
                  AV9DebugMo = GXt_char1 ;
                  GXv_svchar2[0] = AV9DebugMo ;
                  GXv_char3[0] = AV10Fileso ;
                  GXv_char4[0] = AV15Submen ;
                  new prethandling(remoteHandle, context).execute( GXv_svchar2, GXv_char3, GXv_char4) ;
                  anewmainics.this.AV9DebugMo = GXv_svchar2[0] ;
                  anewmainics.this.AV10Fileso = GXv_char3[0] ;
                  anewmainics.this.AV15Submen = GXv_char4[0] ;
                  AV35FileSt = (byte)(3) ;
                  AV34FileRe = "Processed" ;
                  /* Execute user subroutine: S141 */
                  S141 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     cleanup();
                     if (true) return;
                  }
                  AV22AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio proc. do carregamento arquivo " + GXutil.trim( AV11ShortN) ;
                  /* Execute user subroutine: S11164 */
                  S11164 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     cleanup();
                     if (true) return;
                  }
                  AV12File.setSource( AV10Fileso );
                  AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
                  AV12File.setSource( AV10Fileso );
                  AV12File.delete();
                  context.msgStatus( "--MENU 1 - SUBMENU 1 - ATUALIZANDO TABELA DE DISTRIBUICAO " );
                  new pdistribuicaotransacoes(remoteHandle, context).execute( ) ;
               }
               else
               {
                  AV12File.setSource( AV10Fileso );
                  AV12File.copy(AV14Path+"Error\\"+GXutil.trim( AV11ShortN));
               }
               AV62GXV2 = (int)(AV62GXV2+1) ;
            }
         }
         if ( ( GXutil.strcmp(AV15Submen, "3") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            context.msgStatus( "--MENU 1 - SUBMENU 3 - GERA��O SUBMISS�O AMEX " );
            GXt_char1 = AV9DebugMo ;
            GXv_char4[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_1_3", "Debug Mode do Gerador de arquivos de submiss�o Amex", "S", "", GXv_char4) ;
            anewmainics.this.GXt_char1 = GXv_char4[0] ;
            AV9DebugMo = GXt_char1 ;
            GXv_char4[0] = AV9DebugMo ;
            new pretsubaxv2(remoteHandle, context).execute( GXv_char4) ;
            anewmainics.this.AV9DebugMo = GXv_char4[0] ;
         }
         if ( ( GXutil.strcmp(AV15Submen, "4") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            context.msgStatus( "--MENU 1 - SUBMENU 4 - GERA��O SUBMISS�O REDECARD " );
            GXt_char1 = AV9DebugMo ;
            GXv_char4[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_1_4", "Debug Mode do Gerador de arquivos de submiss�o Master Card", "S", "", GXv_char4) ;
            anewmainics.this.GXt_char1 = GXv_char4[0] ;
            AV9DebugMo = GXt_char1 ;
            GXv_char4[0] = AV9DebugMo ;
            new psubmissaoredecard(remoteHandle, context).execute( GXv_char4) ;
            anewmainics.this.AV9DebugMo = GXv_char4[0] ;
         }
         context.msgStatus( "--MENU 1 - SUBMENU 5 - GERA��O SUBMISS�O CIELO " );
         if ( ( GXutil.strcmp(AV15Submen, "5") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            GXt_char1 = AV9DebugMo ;
            GXv_char4[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_1_5", "Debug Mode do Gerador de arquivos de submiss�o Visa", "S", "", GXv_char4) ;
            anewmainics.this.GXt_char1 = GXv_char4[0] ;
            AV9DebugMo = GXt_char1 ;
            GXv_char4[0] = AV9DebugMo ;
            new pretsubcielo(remoteHandle, context).execute( GXv_char4) ;
            anewmainics.this.AV9DebugMo = GXv_char4[0] ;
         }
         if ( ( GXutil.strcmp(AV15Submen, "6") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            context.msgStatus( "--MENU 1 - SUBMENU 6 - GERA��O ARQUIVO 4122 " );
            GXt_char1 = AV9DebugMo ;
            GXv_char4[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_1_6", "Debug Mode do R4122", "S", "DEBUG", GXv_char4) ;
            anewmainics.this.GXt_char1 = GXv_char4[0] ;
            AV9DebugMo = GXt_char1 ;
            AV16Data = GXutil.nullDate() ;
            GXv_char4[0] = AV9DebugMo ;
            GXv_date5[0] = AV16Data ;
            GXv_date6[0] = AV16Data ;
            GXv_char3[0] = "G" ;
            new putilitario_reti1022(remoteHandle, context).execute( GXv_char4, GXv_date5, GXv_date6, GXv_char3) ;
            anewmainics.this.AV9DebugMo = GXv_char4[0] ;
            anewmainics.this.AV16Data = GXv_date5[0] ;
            anewmainics.this.AV16Data = GXv_date6[0] ;
         }
         context.msgStatus( "--MENU 1 - SUBMENU 7 - GERA��O ARQUIVO 1085 " );
         if ( ( GXutil.strcmp(AV15Submen, "7") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            new pr1085(remoteHandle, context).execute( ) ;
         }
         if ( ( GXutil.strcmp(AV15Submen, "8") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
         {
            new psendfilestam(remoteHandle, context).execute( ) ;
         }
      }
      if ( ( GXutil.strcmp(AV8Menu, "2") == 0 ) )
      {
         /* Execute user subroutine: S17367 */
         S17367 ();
         if ( returnInSub )
         {
            returnInSub = true;
            cleanup();
            if (true) return;
         }
      }
      context.msgStatus( "*****************************************************************" );
      context.msgStatus( "*********************** Versao 1.00.14 **************************" );
      context.msgStatus( "**********************Fim Processamento**************************" );
      context.msgStatus( "*****************************************************************" );
      cleanup();
   }

   public void S11164( )
   {
      /* 'AUDITTRAIL' Routine */
      AV21Auditl = "Localhost" ;
      AV18AuditT = "TIES_ICSI" ;
      AV19AuditT = "PCI" ;
      GXv_char4[0] = AV21Auditl ;
      GXv_char3[0] = AV18AuditT ;
      GXv_svchar2[0] = AV19AuditT ;
      GXv_svchar7[0] = AV22AuditT ;
      new pnewaudit(remoteHandle, context).execute( GXv_char4, GXv_char3, GXv_svchar2, GXv_svchar7) ;
      anewmainics.this.AV21Auditl = GXv_char4[0] ;
      anewmainics.this.AV18AuditT = GXv_char3[0] ;
      anewmainics.this.AV19AuditT = GXv_svchar2[0] ;
      anewmainics.this.AV22AuditT = GXv_svchar7[0] ;
   }

   public void S12173( )
   {
      /* 'VALIDADUPLICIDADE' Routine */
      AV30Erro_P = "N" ;
      AV63GXLvl1 = (byte)(0) ;
      /* Using cursor P007H2 */
      pr_default.execute(0, new Object[] {AV11ShortN});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1505FileS = P007H2_A1505FileS[0] ;
         n1505FileS = P007H2_n1505FileS[0] ;
         A1504FileN = P007H2_A1504FileN[0] ;
         n1504FileN = P007H2_n1504FileN[0] ;
         A1507FileI = P007H2_A1507FileI[0] ;
         AV63GXLvl1 = (byte)(1) ;
         AV35FileSt = (byte)(4) ;
         AV34FileRe = "Duplicated" ;
         /* Execute user subroutine: S132 */
         S132 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
         AV30Erro_P = "S" ;
         GXt_char1 = AV47Subjec ;
         GXv_svchar7[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_DUPLIC_SUBJECT", "S", "IATA TIESS GSA - Duplicated file", "", GXv_svchar7) ;
         anewmainics.this.GXt_char1 = GXv_svchar7[0] ;
         AV47Subjec = GXt_char1 ;
         GXt_char1 = AV26Body ;
         GXv_svchar7[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_DUPLIC_BODY", "S", "<br>**** ATTENTION ****<br>File [FILE] is duplicated - Please check and take action.", "", GXv_svchar7) ;
         anewmainics.this.GXt_char1 = GXv_svchar7[0] ;
         AV26Body = GXt_char1 ;
         AV26Body = GXutil.strReplace( AV26Body, "[FILE]", AV11ShortN) ;
         GXt_char1 = AV50To ;
         GXv_svchar7[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char1 = GXv_svchar7[0] ;
         AV50To = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV27CC ;
         GXv_svchar7[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char1 = GXv_svchar7[0] ;
         AV27CC = GXutil.trim( GXt_char1) ;
         context.msgStatus( "--MENU 1 - SUBMENU 1 - ALERTA - Arquivo Duplicado "+GXutil.trim( AV11ShortN) );
         new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( ( AV63GXLvl1 == 0 ) )
      {
         AV35FileSt = (byte)(1) ;
         AV34FileRe = "Upload Efetuado" ;
         /* Execute user subroutine: S132 */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV45RetVal = context.getSessionInstances().getDelimitedFiles().dfropen( AV10Fileso, 400, "��", "\"", "") ;
         AV42QtdeLi = 0 ;
         AV51TotalT = 0 ;
         while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
         {
            GXv_svchar7[0] = AV38Linha ;
            GXt_int9 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_svchar7, (short)(250)) ;
            AV38Linha = GXv_svchar7[0] ;
            AV45RetVal = GXt_int9 ;
            AV42QtdeLi = (double)(AV42QtdeLi+1) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV38Linha, 1, 1), "Z") == 0 ) )
            {
               AV51TotalT = GXutil.val( GXutil.trim( GXutil.substring( AV38Linha, 2, 11)), ".") ;
            }
         }
         AV45RetVal = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         if ( ( ( AV51TotalT == AV42QtdeLi ) ) || ( ( AV51TotalT == 2 ) && ( AV42QtdeLi == 1 ) ) )
         {
         }
         else
         {
            GXt_char8 = AV47Subjec ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CORRUP_SUBJECT", "S", "IATA TIESS GSA - Corrupted file", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV47Subjec = GXt_char8 ;
            GXt_char8 = AV26Body ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CORRUP_BODY", "S", "<br>**** ATTENTION ****<br>File [FILE] is corrupted - Please check and take action.", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV26Body = GXt_char8 ;
            AV26Body = GXutil.strReplace( AV26Body, "[FILE]", AV11ShortN) ;
            GXt_char8 = AV50To ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV50To = GXt_char8 ;
            GXt_char8 = AV27CC ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV27CC = GXt_char8 ;
            AV35FileSt = (byte)(4) ;
            AV34FileRe = "Corrupted" ;
            /* Execute user subroutine: S141 */
            S141 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30Erro_P = "S" ;
            context.msgStatus( "--MENU 1 - SUBMENU 1 - ALERTA - Arquivo Corrompido "+GXutil.trim( AV11ShortN) );
            new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
         }
      }
   }

   public void S132( )
   {
      /* 'INSERE_FILE_NAME' Routine */
      AV31FileID = 0 ;
      AV32FileNa = "" ;
      /*
         INSERT RECORD ON TABLE FILES

      */
      A964CiaCod = "001" ;
      n964CiaCod = false ;
      A1503FileD = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      n1503FileD = false ;
      A1504FileN = GXutil.trim( AV12File.getName()) ;
      n1504FileN = false ;
      A1506FileR = AV34FileRe ;
      n1506FileR = false ;
      A1505FileS = AV35FileSt ;
      n1505FileS = false ;
      /* Using cursor P007H3 */
      pr_default.execute(1, new Object[] {new Boolean(n964CiaCod), A964CiaCod, new Boolean(n1503FileD), A1503FileD, new Boolean(n1504FileN), A1504FileN, new Boolean(n1505FileS), new Byte(A1505FileS), new Boolean(n1506FileR), A1506FileR});
      /* Retrieving last key number assigned */
      /* Using cursor P007H4 */
      pr_default.execute(2);
      A1507FileI = P007H4_A1507FileI[0] ;
      pr_default.close(2);
      if ( (pr_default.getStatus(1) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      AV31FileID = A1507FileI ;
      AV32FileNa = A1504FileN ;
   }

   public void S141( )
   {
      /* 'ALTERA_FILE_NAME' Routine */
      /* Using cursor P007H5 */
      pr_default.execute(3, new Object[] {new Integer(AV31FileID), new Integer(AV31FileID)});
      while ( (pr_default.getStatus(3) != 101) )
      {
         A1507FileI = P007H5_A1507FileI[0] ;
         A1506FileR = P007H5_A1506FileR[0] ;
         n1506FileR = P007H5_n1506FileR[0] ;
         A1505FileS = P007H5_A1505FileS[0] ;
         n1505FileS = P007H5_n1505FileS[0] ;
         A1506FileR = AV34FileRe ;
         n1506FileR = false ;
         A1505FileS = AV35FileSt ;
         n1505FileS = false ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         /* Using cursor P007H6 */
         pr_default.execute(4, new Object[] {new Boolean(n1506FileR), A1506FileR, new Boolean(n1505FileS), new Byte(A1505FileS), new Integer(A1507FileI)});
         if (true) break;
         /* Using cursor P007H7 */
         pr_default.execute(5, new Object[] {new Boolean(n1506FileR), A1506FileR, new Boolean(n1505FileS), new Byte(A1505FileS), new Integer(A1507FileI)});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(3);
   }

   public void S15262( )
   {
      /* 'EXECUTASHELL' Routine */
      GXt_char8 = AV41Pasta ;
      GXv_svchar7[0] = GXt_char8 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATENCSAB", "Caminhoi onde esta a BAT que ir� encriptar a RET Sabre", "S", "C:\\R2Tech\\Encrypt", GXv_svchar7) ;
      anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
      AV41Pasta = GXutil.trim( GXt_char8) ;
      AV28Comand = GXutil.trim( AV41Pasta) ;
      AV22AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " Encripta��o de arquivo RET." ;
      /* Execute user subroutine: S11164 */
      S11164 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV44Retorn = (byte)(GXutil.shell( AV28Comand, 1)) ;
      context.msgStatus( "--MENU 1 - SUBMENU 1 - Arquivo Sabre Encriptado" );
      GXt_char8 = AV41Pasta ;
      GXv_svchar7[0] = GXt_char8 ;
      new pr2getparm(remoteHandle, context).execute( "PATHBATENCGAL", "Caminhoi onde esta a BAT que ir� encriptar a RET Galileo", "S", "C:\\R2Tech\\Encrypt", GXv_svchar7) ;
      anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
      AV41Pasta = GXutil.trim( GXt_char8) ;
      AV28Comand = GXutil.trim( AV41Pasta) ;
      AV22AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " Encripta��o de arquivo RET." ;
      /* Execute user subroutine: S11164 */
      S11164 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV44Retorn = (byte)(GXutil.shell( AV28Comand, 1)) ;
      context.msgStatus( "--MENU 1 - SUBMENU 1 - Arquivo Galileo Encriptado" );
   }

   public void S16289( )
   {
      /* 'VALIDA_FOLDERS_UPLOAD' Routine */
      GXt_char8 = AV14Path ;
      GXv_svchar7[0] = GXt_char8 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILESAB", "Caminho entrada das Rets", "F", "C:\\R2TECH\\Sistemas\\ICSI\\RET\\SABRE", GXv_svchar7) ;
      anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
      AV14Path = GXt_char8 ;
      AV13Direto.setSource( GXutil.trim( AV14Path) );
      GXutil.msg( this, AV14Path );
      AV66GXV4 = 1 ;
      AV65GXV3 = AV13Direto.getFiles("") ;
      while ( ( AV66GXV4 <= AV65GXV3.getItemCount() ) )
      {
         AV12File = AV65GXV3.item((short)(AV66GXV4)) ;
         AV29DirCou = (double)(AV29DirCou+1) ;
         AV66GXV4 = (int)(AV66GXV4+1) ;
      }
      Gx_msg = localUtil.format( AV29DirCou, "ZZZZZZ9.99") ;
      GXutil.msg( this, Gx_msg );
      if ( ( AV29DirCou <= 0 ) )
      {
         GXt_char8 = AV47Subjec ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV47Subjec = GXt_char8 ;
         GXt_char8 = AV26Body ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_BODY", "S", "<br>**** ATTENTION ****<br>The RET file related to Sabre was not received today.", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV26Body = GXt_char8 ;
         AV26Body = GXutil.strReplace( AV26Body, "[GDS]", "SABRE") ;
         GXt_char8 = AV50To ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV50To = GXutil.trim( GXt_char8) ;
         GXt_char8 = AV27CC ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV27CC = GXutil.trim( GXt_char8) ;
         context.msgStatus( "--MENU 1 - SUBMENU 1 - ALERTA - RET SABRE nao encontrada" );
         new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
      }
      GXt_char8 = AV14Path ;
      GXv_svchar7[0] = GXt_char8 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILEGAL", "Caminho entrada das Rets", "F", "C:\\R2TECH\\Sistemas\\ICSI\\RET\\GALILEO", GXv_svchar7) ;
      anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
      AV14Path = GXt_char8 ;
      AV13Direto.setSource( GXutil.trim( AV14Path) );
      AV69GXV6 = 1 ;
      AV68GXV5 = AV13Direto.getFiles("") ;
      while ( ( AV69GXV6 <= AV68GXV5.getItemCount() ) )
      {
         AV12File = AV68GXV5.item((short)(AV69GXV6)) ;
         AV29DirCou = (double)(AV29DirCou+1) ;
         AV69GXV6 = (int)(AV69GXV6+1) ;
      }
      if ( ( AV29DirCou <= 0 ) )
      {
         GXt_char8 = AV47Subjec ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV47Subjec = GXt_char8 ;
         GXt_char8 = AV26Body ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_BODY", "S", "<br>**** ATTENTION ****<br>The RET file related to Sabre was not received today.", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV26Body = GXt_char8 ;
         AV26Body = GXutil.strReplace( AV26Body, "[GDS]", "GALILEO") ;
         GXt_char8 = AV50To ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV50To = GXutil.trim( GXt_char8) ;
         GXt_char8 = AV27CC ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV27CC = GXutil.trim( GXt_char8) ;
         context.msgStatus( "--MENU 1 - SUBMENU 1 - ALERTA - RET GALILEO nao encontrada" );
         new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
      }
      GXt_char8 = AV14Path ;
      GXv_svchar7[0] = GXt_char8 ;
      new pr2getparm(remoteHandle, context).execute( "PATHRETFILEAMA", "Caminho entrada das Rets", "F", "C:\\R2TECH\\Sistemas\\ICSI\\RET\\AMADEUS", GXv_svchar7) ;
      anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
      AV14Path = GXt_char8 ;
      AV13Direto.setSource( GXutil.trim( AV14Path) );
      AV71GXV8 = 1 ;
      AV70GXV7 = AV13Direto.getFiles("") ;
      while ( ( AV71GXV8 <= AV70GXV7.getItemCount() ) )
      {
         AV12File = AV70GXV7.item((short)(AV71GXV8)) ;
         AV29DirCou = (double)(AV29DirCou+1) ;
         AV71GXV8 = (int)(AV71GXV8+1) ;
      }
      if ( ( AV29DirCou <= 0 ) )
      {
         GXt_char8 = AV47Subjec ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV47Subjec = GXt_char8 ;
         GXt_char8 = AV26Body ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_BODY", "S", "<br>**** ATTENTION ****<br>The RET file related to Sabre was not received today.", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV26Body = GXt_char8 ;
         AV26Body = GXutil.strReplace( AV26Body, "[GDS]", "AMADEUS") ;
         GXt_char8 = AV50To ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV50To = GXutil.trim( GXt_char8) ;
         GXt_char8 = AV27CC ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV27CC = GXutil.trim( GXt_char8) ;
         context.msgStatus( "--MENU 1 - SUBMENU 1 - ALERTA - RET AMADEUS nao encontrada" );
         new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
      }
      context.msgStatus( "--MENU 1 - SUBMENU 1 - VALIDACAO ENCRIPTACAO SABRE E GALILEO " );
      /* Execute user subroutine: S15262 */
      S15262 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S17367( )
   {
      /* 'PROCESSA_RETORNOS' Routine */
      AV29DirCou = 0 ;
      if ( ( GXutil.strcmp(AV15Submen, "1") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
      {
         context.msgStatus( "--MENU 2 - SUBMENU 1 - LEITURA ARQUIVO RETORNO AMEX " );
         context.msgStatus( "--MENU 2 - SUBMENU 1 - VALIDACAO SE EXISTE ARQUIVO DE RETORNO AMEX E SE AS CONFIGURACOES ESTAO OK" );
         GXt_char8 = AV14Path ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "PATHRETFILAX", "Caminho retorno submiss�o Amex", "F", "C:\\R2TECH\\Return\\Files\\LAX", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV14Path = GXt_char8 ;
         AV13Direto.setSource( GXutil.trim( AV14Path) );
         AV73GXV10 = 1 ;
         AV72GXV9 = AV13Direto.getFiles("") ;
         while ( ( AV73GXV10 <= AV72GXV9.getItemCount() ) )
         {
            AV12File = AV72GXV9.item((short)(AV73GXV10)) ;
            AV11ShortN = AV12File.getName() ;
            AV57FullPa = AV12File.getAbsoluteName() ;
            AV29DirCou = (double)(AV29DirCou+1) ;
            GXt_char8 = AV58BasePa ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPFILAX", "Caminho da c�pia do arquivo para processamento.", "F", "C:\\R2TECH\\Return\\Process\\LAX", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV58BasePa = GXt_char8 ;
            GXutil.msg( this, AV58BasePa );
            AV13Direto.setSource( GXutil.trim( AV58BasePa) );
            if ( AV13Direto.exists() )
            {
               AV32FileNa = GXutil.trim( AV58BasePa) + "\\" + GXutil.trim( AV11ShortN) ;
               AV12File.setSource( GXutil.trim( AV57FullPa) );
               AV12File.copy(AV32FileNa);
               AV12File.setSource( AV57FullPa );
               AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
               AV12File.setSource( AV57FullPa );
               AV12File.delete();
            }
            else
            {
               GXt_char8 = AV47Subjec ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV47Subjec = GXt_char8 ;
               GXt_char8 = AV26Body ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_COP_BODY", "S", "<br>The parameter related to the copy of the return file [ADM] processing folder does not exist.", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV26Body = GXt_char8 ;
               AV26Body = GXutil.strReplace( AV26Body, "[ADM]", "AMEX") ;
               GXt_char8 = AV50To ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV50To = GXutil.trim( GXt_char8) ;
               GXt_char8 = AV27CC ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV27CC = GXutil.trim( GXt_char8) ;
               context.msgStatus( "--MENU 2 - SUBMENU 1 - ALERTA - A Configuracao que indica onde o arquivo de retorno AMEX deve ser copiado esta invalida" );
               new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
            }
            AV73GXV10 = (int)(AV73GXV10+1) ;
         }
         if ( ( AV29DirCou <= 0 ) )
         {
            GXt_char8 = AV47Subjec ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV47Subjec = GXt_char8 ;
            GXt_char8 = AV26Body ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_ADM_BODY", "S", "<br><br>The return file related to [ADM] was not received today.", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV26Body = GXt_char8 ;
            AV26Body = GXutil.strReplace( AV26Body, "[ADM]", "AMEX") ;
            GXt_char8 = AV50To ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV50To = GXutil.trim( GXt_char8) ;
            GXt_char8 = AV27CC ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV27CC = GXutil.trim( GXt_char8) ;
            context.msgStatus( "--MENU 2 - SUBMENU 1 - ALERTA - Nao foram encontrados arquivos de retorno TIVIT AX" );
            new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
         }
         else
         {
            GXt_char8 = AV14Path ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPFILAX", "Caminho retorno submiss�o Amex", "F", "C:\\R2TECH\\Return\\Files\\LAX", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV14Path = GXt_char8 ;
            AV13Direto.setSource( GXutil.trim( AV14Path) );
            AV75GXV12 = 1 ;
            AV74GXV11 = AV13Direto.getFiles("") ;
            while ( ( AV75GXV12 <= AV74GXV11.getItemCount() ) )
            {
               AV12File = AV74GXV11.item((short)(AV75GXV12)) ;
               AV10Fileso = AV12File.getAbsoluteName() ;
               AV11ShortN = AV12File.getName() ;
               AV20AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio proc. do carregamento arquivo " + GXutil.trim( AV11ShortN) ;
               /* Execute user subroutine: S11164 */
               S11164 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               context.msgStatus( "--MENU 2 - SUBMENU 1 - PROCESSAMENTO RETORNO AMEX" );
               GXt_char8 = AV9DebugMo ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_2_1", "Debug Mode do Gerador de arquivos de submiss�o Amex", "S", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV9DebugMo = GXt_char8 ;
               GXv_svchar7[0] = AV9DebugMo ;
               GXv_char4[0] = AV10Fileso ;
               new pretrsubaxv2(remoteHandle, context).execute( GXv_svchar7, GXv_char4) ;
               anewmainics.this.AV9DebugMo = GXv_svchar7[0] ;
               anewmainics.this.AV10Fileso = GXv_char4[0] ;
               AV12File.setSource( AV10Fileso );
               AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
               AV12File.setSource( AV10Fileso );
               AV12File.delete();
               AV75GXV12 = (int)(AV75GXV12+1) ;
            }
         }
      }
      AV29DirCou = 0 ;
      if ( ( GXutil.strcmp(AV15Submen, "2") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
      {
         context.msgStatus( "--MENU 2 - SUBMENU 2 - VALIDACAO SE EXISTE ARQUIVO DE RETORNO REDECARD E SE AS CONFIGURACOES ESTAO OK" );
         GXt_char8 = AV14Path ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "PATHRETFILRED", "Caminho retorno submiss�o Amex", "F", "C:\\R2TECH\\Return\\Files\\LAX", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV14Path = GXt_char8 ;
         AV13Direto.setSource( GXutil.trim( AV14Path) );
         AV77GXV14 = 1 ;
         AV76GXV13 = AV13Direto.getFiles("") ;
         while ( ( AV77GXV14 <= AV76GXV13.getItemCount() ) )
         {
            AV12File = AV76GXV13.item((short)(AV77GXV14)) ;
            AV11ShortN = AV12File.getName() ;
            AV57FullPa = AV12File.getAbsoluteName() ;
            AV29DirCou = (double)(AV29DirCou+1) ;
            GXt_char8 = AV58BasePa ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPFILRED", "Caminho da c�pia do arquivo para processamento.", "F", "C:\\R2TECH\\Return\\Process\\LAX", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV58BasePa = GXt_char8 ;
            GXutil.msg( this, AV58BasePa );
            AV13Direto.setSource( GXutil.trim( AV58BasePa) );
            if ( AV13Direto.exists() )
            {
               AV32FileNa = GXutil.trim( AV58BasePa) + "\\" + GXutil.trim( AV11ShortN) ;
               AV12File.setSource( GXutil.trim( AV57FullPa) );
               AV12File.copy(AV32FileNa);
               AV12File.setSource( AV57FullPa );
               AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
               AV12File.setSource( AV57FullPa );
               AV12File.delete();
            }
            else
            {
               GXt_char8 = AV47Subjec ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV47Subjec = GXt_char8 ;
               GXt_char8 = AV26Body ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_COP_BODY", "S", "<br>The parameter related to the copy of the return file [ADM] processing folder does not exist.", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV26Body = GXt_char8 ;
               AV26Body = GXutil.strReplace( AV26Body, "[ADM]", "REDECARD") ;
               GXt_char8 = AV50To ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV50To = GXutil.trim( GXt_char8) ;
               GXt_char8 = AV27CC ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV27CC = GXutil.trim( GXt_char8) ;
               context.msgStatus( "--MENU 2 - SUBMENU 2 - ALERTA - A Configuracao que indica onde o arquivo de retorno REDECARD deve ser copiado esta invalida" );
               new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
            }
            AV77GXV14 = (int)(AV77GXV14+1) ;
         }
         if ( ( AV29DirCou <= 0 ) )
         {
            GXt_char8 = AV47Subjec ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV47Subjec = GXt_char8 ;
            GXt_char8 = AV26Body ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_ADM_BODY", "S", "<br><br>The return file related to [ADM] was not received today.", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV26Body = GXt_char8 ;
            AV26Body = GXutil.strReplace( AV26Body, "[ADM]", "REDECARD") ;
            GXt_char8 = AV50To ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV50To = GXutil.trim( GXt_char8) ;
            GXt_char8 = AV27CC ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV27CC = GXutil.trim( GXt_char8) ;
            context.msgStatus( "--MENU 2 - SUBMENU 2 - ALERTA - Nao foram encontrados arquivos de retorno REDECARD" );
            new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
         }
         else
         {
            GXt_char8 = AV14Path ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPFILRED", "Caminho retorno submiss�o Amex", "F", "C:\\R2TECH\\Return\\Files\\LAX", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV14Path = GXt_char8 ;
            AV13Direto.setSource( GXutil.trim( AV14Path) );
            AV79GXV16 = 1 ;
            AV78GXV15 = AV13Direto.getFiles("") ;
            while ( ( AV79GXV16 <= AV78GXV15.getItemCount() ) )
            {
               AV12File = AV78GXV15.item((short)(AV79GXV16)) ;
               AV10Fileso = AV12File.getAbsoluteName() ;
               AV11ShortN = AV12File.getName() ;
               AV20AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio proc. do carregamento arquivo " + GXutil.trim( AV11ShortN) ;
               /* Execute user subroutine: S11164 */
               S11164 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               context.msgStatus( "--MENU 2 - SUBMENU 2 - PROCESSAMENTO RETORNO REDECARD" );
               GXt_char8 = AV9DebugMo ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_2_2", "Debug Mode do Gerador de arquivos de submiss�o Master Card", "S", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV9DebugMo = GXt_char8 ;
               GXv_svchar7[0] = AV9DebugMo ;
               GXv_char4[0] = AV10Fileso ;
               new pretrsubrc01(remoteHandle, context).execute( GXv_svchar7, GXv_char4) ;
               anewmainics.this.AV9DebugMo = GXv_svchar7[0] ;
               anewmainics.this.AV10Fileso = GXv_char4[0] ;
               AV12File.setSource( AV10Fileso );
               AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
               AV12File.setSource( AV10Fileso );
               AV12File.delete();
               AV79GXV16 = (int)(AV79GXV16+1) ;
            }
         }
      }
      AV29DirCou = 0 ;
      if ( ( GXutil.strcmp(AV15Submen, "3") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
      {
         context.msgStatus( "--MENU 2 - SUBMENU 3 - VALIDACAO SE EXISTE ARQUIVO DE RETORNO CIELO E SE AS CONFIGURACOES ESTAO OK" );
         GXt_char8 = AV14Path ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "PATHRETFILVIS", "Caminho retorno submiss�o Amex", "F", "C:\\R2TECH\\Return\\Files\\LAX", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV14Path = GXt_char8 ;
         AV13Direto.setSource( GXutil.trim( AV14Path) );
         AV81GXV18 = 1 ;
         AV80GXV17 = AV13Direto.getFiles("") ;
         while ( ( AV81GXV18 <= AV80GXV17.getItemCount() ) )
         {
            AV12File = AV80GXV17.item((short)(AV81GXV18)) ;
            AV11ShortN = AV12File.getName() ;
            AV57FullPa = AV12File.getAbsoluteName() ;
            AV29DirCou = (double)(AV29DirCou+1) ;
            GXt_char8 = AV58BasePa ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPFILVIS", "Caminho da c�pia do arquivo para processamento.", "F", "C:\\R2TECH\\Return\\Process\\LAX", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV58BasePa = GXt_char8 ;
            GXutil.msg( this, AV58BasePa );
            AV13Direto.setSource( GXutil.trim( AV58BasePa) );
            if ( AV13Direto.exists() )
            {
               AV32FileNa = GXutil.trim( AV58BasePa) + "\\" + GXutil.trim( AV11ShortN) ;
               AV12File.setSource( GXutil.trim( AV57FullPa) );
               AV12File.copy(AV32FileNa);
               AV12File.setSource( AV57FullPa );
               AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
               AV12File.setSource( AV57FullPa );
               AV12File.delete();
            }
            else
            {
               GXt_char8 = AV47Subjec ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV47Subjec = GXt_char8 ;
               GXt_char8 = AV26Body ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_COP_BODY", "S", "<br>The parameter related to the copy of the return file [ADM] processing folder does not exist.", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV26Body = GXt_char8 ;
               AV26Body = GXutil.strReplace( AV26Body, "[ADM]", "CIELO") ;
               GXt_char8 = AV50To ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV50To = GXutil.trim( GXt_char8) ;
               GXt_char8 = AV27CC ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV27CC = GXutil.trim( GXt_char8) ;
               context.msgStatus( "--MENU 2 - SUBMENU 3 - ALERTA - A Configuracao que indica onde o arquivo de retorno CIELO deve ser copiado esta invalida" );
               new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
            }
            AV81GXV18 = (int)(AV81GXV18+1) ;
         }
         if ( ( AV29DirCou <= 0 ) )
         {
            GXt_char8 = AV47Subjec ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_FNF_SUBJECT", "S", "IATA TIESS GSA - File not Found", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV47Subjec = GXt_char8 ;
            GXt_char8 = AV26Body ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_ADM_BODY", "S", "<br><br>The return file related to [ADM] was not received today.", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV26Body = GXt_char8 ;
            AV26Body = GXutil.strReplace( AV26Body, "[ADM]", "CIELO") ;
            GXt_char8 = AV50To ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV50To = GXutil.trim( GXt_char8) ;
            GXt_char8 = AV27CC ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "S", "", "", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV27CC = GXutil.trim( GXt_char8) ;
            context.msgStatus( "--MENU 2 - SUBMENU 3 - ALERTA - Nao foram encontrados arquivos de retorno TIVIT CIELO" );
            new penviaemail(remoteHandle, context).execute( AV47Subjec, AV26Body, AV50To, AV27CC, AV25BCC, AV23Anexos) ;
         }
         else
         {
            GXt_char8 = AV14Path ;
            GXv_svchar7[0] = GXt_char8 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPFILVIS", "Caminho retorno submiss�o Amex", "F", "C:\\R2TECH\\Return\\Files\\LAX", GXv_svchar7) ;
            anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
            AV14Path = GXt_char8 ;
            AV13Direto.setSource( GXutil.trim( AV14Path) );
            AV83GXV20 = 1 ;
            AV82GXV19 = AV13Direto.getFiles("") ;
            while ( ( AV83GXV20 <= AV82GXV19.getItemCount() ) )
            {
               AV12File = AV82GXV19.item((short)(AV83GXV20)) ;
               AV10Fileso = AV12File.getAbsoluteName() ;
               AV11ShortN = AV12File.getName() ;
               AV20AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio proc. do carregamento arquivo " + GXutil.trim( AV11ShortN) ;
               /* Execute user subroutine: S11164 */
               S11164 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               context.msgStatus( "--MENU 2 - SUBMENU 3 - PROCESSAMENTO RETORNO CIELO" );
               GXt_char8 = AV9DebugMo ;
               GXv_svchar7[0] = GXt_char8 ;
               new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_2_3", "Debug Mode do Gerador de arquivos de submiss�o Visa", "S", "", GXv_svchar7) ;
               anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
               AV9DebugMo = GXt_char8 ;
               GXv_svchar7[0] = AV9DebugMo ;
               GXv_char4[0] = AV10Fileso ;
               new pretrsubcielo(remoteHandle, context).execute( GXv_svchar7, GXv_char4) ;
               anewmainics.this.AV9DebugMo = GXv_svchar7[0] ;
               anewmainics.this.AV10Fileso = GXv_char4[0] ;
               AV12File.setSource( AV10Fileso );
               AV12File.copy(AV14Path+"Processed\\"+GXutil.trim( AV11ShortN));
               AV12File.setSource( AV10Fileso );
               AV12File.delete();
               AV83GXV20 = (int)(AV83GXV20+1) ;
            }
         }
      }
      if ( ( GXutil.strcmp(AV15Submen, "4") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
      {
         GXt_char8 = AV9DebugMo ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_2_4", "Debug Mode do R1089 Aceitos", "S", "DEBUG", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV9DebugMo = GXt_char8 ;
         GXv_svchar7[0] = AV9DebugMo ;
         GXv_char4[0] = AV17Modo ;
         new preti1010(remoteHandle, context).execute( GXv_svchar7, GXv_char4) ;
         anewmainics.this.AV9DebugMo = GXv_svchar7[0] ;
         anewmainics.this.AV17Modo = GXv_char4[0] ;
      }
      if ( ( GXutil.strcmp(AV15Submen, "5") == 0 ) || ( GXutil.strcmp(AV15Submen, "0") == 0 ) )
      {
         GXt_char8 = AV9DebugMo ;
         GXv_svchar7[0] = GXt_char8 ;
         new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_2_5", "Debug Mode do R1092 Rejeitados", "S", "DEBUG", GXv_svchar7) ;
         anewmainics.this.GXt_char8 = GXv_svchar7[0] ;
         AV9DebugMo = GXt_char8 ;
         GXv_svchar7[0] = AV9DebugMo ;
         new preti1012(remoteHandle, context).execute( GXv_svchar7) ;
         anewmainics.this.AV9DebugMo = GXv_svchar7[0] ;
      }
   }

 /*  public static Object refClasses( )
   {
      GXutil.refClasses(pnewmainics.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = anewmainics.this.AV8Menu;
      this.aP1[0] = anewmainics.this.AV15Submen;
      this.aP2[0] = anewmainics.this.AV17Modo;
      Application.commit(context, remoteHandle, "DEFAULT", "anewmainics");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25BCC = "" ;
      GX_I = 0 ;
      AV23Anexos = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV23Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      returnInSub = false ;
      AV14Path = "" ;
      AV13Direto = new com.genexus.util.GXDirectory();
      AV62GXV2 = 0 ;
      AV61GXV1 = new com.genexus.util.GXFileCollection();
      AV12File = new com.genexus.util.GXFile();
      AV10Fileso = "" ;
      AV11ShortN = "" ;
      AV30Erro_P = "" ;
      AV35FileSt = (byte)(0) ;
      AV34FileRe = "" ;
      AV9DebugMo = "" ;
      AV22AuditT = "" ;
      AV16Data = GXutil.nullDate() ;
      GXv_date5 = new java.util.Date [1] ;
      GXv_date6 = new java.util.Date [1] ;
      AV21Auditl = "" ;
      AV18AuditT = "" ;
      AV19AuditT = "" ;
      GXv_char3 = new String [1] ;
      GXv_svchar2 = new String [1] ;
      AV63GXLvl1 = (byte)(0) ;
      scmdbuf = "" ;
      P007H2_A1505FileS = new byte[1] ;
      P007H2_n1505FileS = new boolean[] {false} ;
      P007H2_A1504FileN = new String[] {""} ;
      P007H2_n1504FileN = new boolean[] {false} ;
      P007H2_A1507FileI = new int[1] ;
      A1505FileS = (byte)(0) ;
      n1505FileS = false ;
      A1504FileN = "" ;
      n1504FileN = false ;
      A1507FileI = 0 ;
      AV47Subjec = "" ;
      AV26Body = "" ;
      AV50To = "" ;
      AV27CC = "" ;
      GXt_char1 = "" ;
      AV45RetVal = 0 ;
      AV42QtdeLi = 0 ;
      AV51TotalT = 0 ;
      AV38Linha = "" ;
      GXt_int9 = (short)(0) ;
      AV31FileID = 0 ;
      AV32FileNa = "" ;
      GX_INS277 = 0 ;
      A964CiaCod = "" ;
      n964CiaCod = false ;
      A1503FileD = GXutil.resetTime( GXutil.nullDate() );
      n1503FileD = false ;
      A1506FileR = "" ;
      n1506FileR = false ;
      P007H4_A1507FileI = new int[1] ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      P007H5_A1507FileI = new int[1] ;
      P007H5_A1506FileR = new String[] {""} ;
      P007H5_n1506FileR = new boolean[] {false} ;
      P007H5_A1505FileS = new byte[1] ;
      P007H5_n1505FileS = new boolean[] {false} ;
      AV41Pasta = "" ;
      AV28Comand = "" ;
      AV44Retorn = (byte)(0) ;
      AV66GXV4 = 0 ;
      AV65GXV3 = new com.genexus.util.GXFileCollection();
      AV29DirCou = 0 ;
      Gx_msg = "" ;
      AV69GXV6 = 0 ;
      AV68GXV5 = new com.genexus.util.GXFileCollection();
      AV71GXV8 = 0 ;
      AV70GXV7 = new com.genexus.util.GXFileCollection();
      AV73GXV10 = 0 ;
      AV72GXV9 = new com.genexus.util.GXFileCollection();
      AV57FullPa = "" ;
      AV58BasePa = "" ;
      AV75GXV12 = 0 ;
      AV74GXV11 = new com.genexus.util.GXFileCollection();
      AV20AuditT = "" ;
      AV77GXV14 = 0 ;
      AV76GXV13 = new com.genexus.util.GXFileCollection();
      AV79GXV16 = 0 ;
      AV78GXV15 = new com.genexus.util.GXFileCollection();
      AV81GXV18 = 0 ;
      AV80GXV17 = new com.genexus.util.GXFileCollection();
      AV83GXV20 = 0 ;
      AV82GXV19 = new com.genexus.util.GXFileCollection();
      GXv_char4 = new String [1] ;
      GXt_char8 = "" ;
      GXv_svchar7 = new String [1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new anewmainics__default(),
         new Object[] {
             new Object[] {
            P007H2_A1505FileS, P007H2_n1505FileS, P007H2_A1504FileN, P007H2_n1504FileN, P007H2_A1507FileI
            }
            , new Object[] {
            }
            , new Object[] {
            P007H4_A1507FileI
            }
            , new Object[] {
            P007H5_A1507FileI, P007H5_A1506FileR, P007H5_n1506FileR, P007H5_A1505FileS, P007H5_n1505FileS
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV35FileSt ;
   private byte AV63GXLvl1 ;
   private byte A1505FileS ;
   private byte AV44Retorn ;
   private short GXt_int9 ;
   private short Gx_err ;
   private int GX_I ;
   private int AV62GXV2 ;
   private int A1507FileI ;
   private int AV31FileID ;
   private int GX_INS277 ;
   private int AV66GXV4 ;
   private int AV69GXV6 ;
   private int AV71GXV8 ;
   private int AV73GXV10 ;
   private int AV75GXV12 ;
   private int AV77GXV14 ;
   private int AV79GXV16 ;
   private int AV81GXV18 ;
   private int AV83GXV20 ;
   private double AV45RetVal ;
   private double AV42QtdeLi ;
   private double AV51TotalT ;
   private double AV29DirCou ;
   private String AV8Menu ;
   private String AV15Submen ;
   private String AV17Modo ;
   private String AV14Path ;
   private String AV10Fileso ;
   private String AV11ShortN ;
   private String AV9DebugMo ;
   private String GXv_char3[] ;
   private String scmdbuf ;
   private String GXt_char1 ;
   private String A964CiaCod ;
   private String Gx_emsg ;
   private String AV41Pasta ;
   private String Gx_msg ;
   private String AV57FullPa ;
   private String AV58BasePa ;
   private String GXv_char4[] ;
   private String GXt_char8 ;
   private java.util.Date A1503FileD ;
   private java.util.Date AV16Data ;
   private java.util.Date GXv_date5[] ;
   private java.util.Date GXv_date6[] ;
   private boolean returnInSub ;
   private boolean n1505FileS ;
   private boolean n1504FileN ;
   private boolean n964CiaCod ;
   private boolean n1503FileD ;
   private boolean n1506FileR ;
   private String AV30Erro_P ;
   private String AV25BCC ;
   private String AV23Anexos[] ;
   private String AV34FileRe ;
   private String AV22AuditT ;
   private String AV21Auditl ;
   private String AV18AuditT ;
   private String AV19AuditT ;
   private String GXv_svchar2[] ;
   private String A1504FileN ;
   private String AV47Subjec ;
   private String AV26Body ;
   private String AV50To ;
   private String AV27CC ;
   private String AV38Linha ;
   private String AV32FileNa ;
   private String A1506FileR ;
   private String AV28Comand ;
   private String AV20AuditT ;
   private String GXv_svchar7[] ;
   private com.genexus.util.GXDirectory AV13Direto ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private byte[] P007H2_A1505FileS ;
   private boolean[] P007H2_n1505FileS ;
   private String[] P007H2_A1504FileN ;
   private boolean[] P007H2_n1504FileN ;
   private int[] P007H2_A1507FileI ;
   private int[] P007H4_A1507FileI ;
   private int[] P007H5_A1507FileI ;
   private String[] P007H5_A1506FileR ;
   private boolean[] P007H5_n1506FileR ;
   private byte[] P007H5_A1505FileS ;
   private boolean[] P007H5_n1505FileS ;
   private com.genexus.util.GXFile AV12File ;
   private com.genexus.util.GXFileCollection AV61GXV1 ;
   private com.genexus.util.GXFileCollection AV65GXV3 ;
   private com.genexus.util.GXFileCollection AV68GXV5 ;
   private com.genexus.util.GXFileCollection AV70GXV7 ;
   private com.genexus.util.GXFileCollection AV72GXV9 ;
   private com.genexus.util.GXFileCollection AV74GXV11 ;
   private com.genexus.util.GXFileCollection AV76GXV13 ;
   private com.genexus.util.GXFileCollection AV78GXV15 ;
   private com.genexus.util.GXFileCollection AV80GXV17 ;
   private com.genexus.util.GXFileCollection AV82GXV19 ;
}

final  class anewmainics__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007H2", "SELECT [FileStatus], [FileName], [FileID] FROM [FILES] WITH (NOLOCK) WHERE ([FileName] = RTRIM(LTRIM(?))) AND ([FileStatus] = 3) ORDER BY [FileID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007H3", "INSERT INTO [FILES] ([CiaCod], [FileDate], [FileName], [FileStatus], [FileRemark]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007H4", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
         ,new ForEachCursor("P007H5", "SELECT TOP 1 [FileID], [FileRemark], [FileStatus] FROM [FILES] WITH (UPDLOCK) WHERE ([FileID] = ?) AND ([FileID] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007H6", "UPDATE [FILES] SET [FileRemark]=?, [FileStatus]=?  WHERE [FileID] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007H7", "UPDATE [FILES] SET [FileRemark]=?, [FileStatus]=?  WHERE [FileID] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((byte[]) buf[0])[0] = rslt.getByte(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((int[]) buf[4])[0] = rslt.getInt(3) ;
               break;
            case 2 :
               ((int[]) buf[0])[0] = rslt.getInt(1) ;
               break;
            case 3 :
               ((int[]) buf[0])[0] = rslt.getInt(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 255);
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 20);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 250);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(4, ((Number) parms[7]).byteValue());
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[9], 50);
               }
               break;
            case 3 :
               stmt.setInt(1, ((Number) parms[0]).intValue());
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 50);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(2, ((Number) parms[3]).byteValue());
               }
               stmt.setInt(3, ((Number) parms[4]).intValue());
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 50);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(2, ((Number) parms[3]).byteValue());
               }
               stmt.setInt(3, ((Number) parms[4]).intValue());
               break;
      }
   }

}


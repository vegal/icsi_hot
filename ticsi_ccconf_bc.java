/*
               File: ticsi_ccconf_bc
        Description: Configura��o das Empresas de cart�o no ICSI
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:6.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class ticsi_ccconf_bc extends GXWebPanel implements IGxSilentTrn
{
   public ticsi_ccconf_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public ticsi_ccconf_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( ticsi_ccconf_bc.class ));
   }

   public ticsi_ccconf_bc( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z1237ICSI_ = A1237ICSI_ ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_2I0( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2I238( ) ;
         }
         else
         {
            checkExtendedTable2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors2I238( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         /* Save parent mode. */
         sMode238 = Gx_mode ;
         confirm_2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            /* Restore parent mode. */
            Gx_mode = sMode238 ;
            IsConfirmed = (short)(1) ;
         }
         /* Restore parent mode. */
         Gx_mode = sMode238 ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues2I0( ) ;
      }
   }

   public void confirm_2I239( )
   {
      nGXsfl_239_idx = (short)(0) ;
      while ( ( nGXsfl_239_idx < bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().size() ) )
      {
         readRow2I239( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound239 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_239 != 0 ) )
         {
            getKey2I239( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound239 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate2I239( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable2I239( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                     }
                     closeExtendedTableCursors2I239( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound239 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey2I239( ) ;
                     load2I239( ) ;
                     beforeValidate2I239( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls2I239( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_239 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate2I239( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable2I239( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                           }
                           closeExtendedTableCursors2I239( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void e112I2( )
   {
      /* 'Back' Routine */
   }

   public void zm2I238( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         Z1421ICSI_ = A1421ICSI_ ;
         Z1235ICSI_ = A1235ICSI_ ;
         Z1238ICSI_ = A1238ICSI_ ;
         Z1236ICSI_ = A1236ICSI_ ;
      }
      if ( ( GX_JID == -1 ) )
      {
         Z1237ICSI_ = A1237ICSI_ ;
         Z1421ICSI_ = A1421ICSI_ ;
         Z1235ICSI_ = A1235ICSI_ ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load2I238( )
   {
      /* Using cursor BC002I6 */
      pr_default.execute(4, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound238 = (short)(1) ;
         A1421ICSI_ = BC002I6_A1421ICSI_[0] ;
         n1421ICSI_ = BC002I6_n1421ICSI_[0] ;
         A1235ICSI_ = BC002I6_A1235ICSI_[0] ;
         n1235ICSI_ = BC002I6_n1235ICSI_[0] ;
         zm2I238( -1) ;
      }
      pr_default.close(4);
      onLoadActions2I238( ) ;
   }

   public void onLoadActions2I238( )
   {
   }

   public void checkExtendedTable2I238( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors2I238( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey2I238( )
   {
      /* Using cursor BC002I7 */
      pr_default.execute(5, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound238 = (short)(1) ;
      }
      else
      {
         RcdFound238 = (short)(0) ;
      }
      pr_default.close(5);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC002I5 */
      pr_default.execute(3, new Object[] {A1237ICSI_});
      if ( (pr_default.getStatus(3) != 101) )
      {
         zm2I238( 1) ;
         RcdFound238 = (short)(1) ;
         A1237ICSI_ = BC002I5_A1237ICSI_[0] ;
         A1421ICSI_ = BC002I5_A1421ICSI_[0] ;
         n1421ICSI_ = BC002I5_n1421ICSI_[0] ;
         A1235ICSI_ = BC002I5_A1235ICSI_[0] ;
         n1235ICSI_ = BC002I5_n1235ICSI_[0] ;
         Z1237ICSI_ = A1237ICSI_ ;
         sMode238 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load2I238( ) ;
         Gx_mode = sMode238 ;
      }
      else
      {
         RcdFound238 = (short)(0) ;
         initializeNonKey2I238( ) ;
         sMode238 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode238 ;
      }
      pr_default.close(3);
   }

   public void getEqualNoModal( )
   {
      getKey2I238( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_2I0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency2I238( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002I4 */
         pr_default.execute(2, new Object[] {A1237ICSI_});
         if ( ! (pr_default.getStatus(2) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"ICSI_CCCONF"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(2) == 101) || ( GXutil.strcmp(Z1421ICSI_, BC002I4_A1421ICSI_[0]) != 0 ) || ( Z1235ICSI_ != BC002I4_A1235ICSI_[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"ICSI_CCCONF"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2I238( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2I238( 0) ;
         checkOptimisticConcurrency2I238( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2I238( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002I8 */
                  pr_default.execute(6, new Object[] {A1237ICSI_, new Boolean(n1421ICSI_), A1421ICSI_, new Boolean(n1235ICSI_), new Byte(A1235ICSI_)});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2I238( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2I238( ) ;
         }
         endLevel2I238( ) ;
      }
      closeExtendedTableCursors2I238( ) ;
   }

   public void update2I238( )
   {
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I238( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2I238( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002I9 */
                  pr_default.execute(7, new Object[] {new Boolean(n1421ICSI_), A1421ICSI_, new Boolean(n1235ICSI_), new Byte(A1235ICSI_), A1237ICSI_});
                  deferredUpdate2I238( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2I238( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           getByPrimaryKey( ) ;
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2I238( ) ;
      }
      closeExtendedTableCursors2I238( ) ;
   }

   public void deferredUpdate2I238( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2I238( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2I238( ) ;
         scanStart2I239( ) ;
         while ( ( RcdFound239 != 0 ) )
         {
            getByPrimaryKey2I239( ) ;
            delete2I239( ) ;
            scanNext2I239( ) ;
         }
         scanEnd2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I238( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete2I238( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002I10 */
                  pr_default.execute(8, new Object[] {A1237ICSI_});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      sMode238 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2I238( ) ;
      Gx_mode = sMode238 ;
   }

   public void onDeleteControls2I238( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC002I11 */
         pr_default.execute(9, new Object[] {A1237ICSI_});
         if ( (pr_default.getStatus(9) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Cadastro de maquinetas"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(9);
      }
   }

   public void processNestedLevel2I239( )
   {
      nGXsfl_239_idx = (short)(0) ;
      while ( ( nGXsfl_239_idx < bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().size() ) )
      {
         readRow2I239( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound239 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_239 != 0 ) )
         {
            standaloneNotModal2I239( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert2I239( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete2I239( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update2I239( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_239 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_239 = (short)(1) ;
               Gxremove239 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_239 = (short)(0) ;
               Gxremove239 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().removeElement(nGXsfl_239_idx);
            nGXsfl_239_idx = (short)(nGXsfl_239_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow239( ((SdtICSI_CCConf_Level1Item)bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().elementAt(-1+nGXsfl_239_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll2I239( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processLevel2I238( )
   {
      /* Save parent mode. */
      sMode238 = Gx_mode ;
      processNestedLevel2I239( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode238 ;
      /* ' Update level parameters */
   }

   public void endLevel2I238( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(2);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2I238( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues2I0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2I238( )
   {
      /* Using cursor BC002I12 */
      pr_default.execute(10, new Object[] {A1237ICSI_});
      RcdFound238 = (short)(0) ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound238 = (short)(1) ;
         A1237ICSI_ = BC002I12_A1237ICSI_[0] ;
         A1421ICSI_ = BC002I12_A1421ICSI_[0] ;
         n1421ICSI_ = BC002I12_n1421ICSI_[0] ;
         A1235ICSI_ = BC002I12_A1235ICSI_[0] ;
         n1235ICSI_ = BC002I12_n1235ICSI_[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2I238( )
   {
      pr_default.readNext(10);
      RcdFound238 = (short)(0) ;
      scanLoad2I238( ) ;
   }

   public void scanLoad2I238( )
   {
      sMode238 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound238 = (short)(1) ;
         A1237ICSI_ = BC002I12_A1237ICSI_[0] ;
         A1421ICSI_ = BC002I12_A1421ICSI_[0] ;
         n1421ICSI_ = BC002I12_n1421ICSI_[0] ;
         A1235ICSI_ = BC002I12_A1235ICSI_[0] ;
         n1235ICSI_ = BC002I12_n1235ICSI_[0] ;
      }
      Gx_mode = sMode238 ;
   }

   public void scanEnd2I238( )
   {
      pr_default.close(10);
   }

   public void afterConfirm2I238( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2I238( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2I238( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2I238( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2I238( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2I238( )
   {
      /* Before Validate Rules */
   }

   public void addRow2I238( )
   {
      VarsToRow238( bcICSI_CCConf) ;
   }

   public void sendRow2I238( )
   {
   }

   public void readRow2I238( )
   {
      RowToVars238( bcICSI_CCConf, 0) ;
   }

   public void zm2I239( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         Z1236ICSI_ = A1236ICSI_ ;
         Z1421ICSI_ = A1421ICSI_ ;
         Z1235ICSI_ = A1235ICSI_ ;
      }
      if ( ( GX_JID == -2 ) )
      {
         Z1237ICSI_ = A1237ICSI_ ;
         Z1238ICSI_ = A1238ICSI_ ;
         Z1236ICSI_ = A1236ICSI_ ;
      }
   }

   public void standaloneNotModal2I239( )
   {
   }

   public void standaloneModal2I239( )
   {
   }

   public void load2I239( )
   {
      /* Using cursor BC002I13 */
      pr_default.execute(11, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound239 = (short)(1) ;
         A1236ICSI_ = BC002I13_A1236ICSI_[0] ;
         n1236ICSI_ = BC002I13_n1236ICSI_[0] ;
         zm2I239( -2) ;
      }
      pr_default.close(11);
      onLoadActions2I239( ) ;
   }

   public void onLoadActions2I239( )
   {
   }

   public void checkExtendedTable2I239( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal2I239( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2I239( )
   {
   }

   public void enableDisable2I239( )
   {
   }

   public void getKey2I239( )
   {
      /* Using cursor BC002I14 */
      pr_default.execute(12, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound239 = (short)(1) ;
      }
      else
      {
         RcdFound239 = (short)(0) ;
      }
      pr_default.close(12);
   }

   public void getByPrimaryKey2I239( )
   {
      /* Using cursor BC002I3 */
      pr_default.execute(1, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm2I239( 2) ;
         RcdFound239 = (short)(1) ;
         initializeNonKey2I239( ) ;
         A1238ICSI_ = BC002I3_A1238ICSI_[0] ;
         A1236ICSI_ = BC002I3_A1236ICSI_[0] ;
         n1236ICSI_ = BC002I3_n1236ICSI_[0] ;
         Z1237ICSI_ = A1237ICSI_ ;
         Z1238ICSI_ = A1238ICSI_ ;
         sMode239 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2I239( ) ;
         load2I239( ) ;
         Gx_mode = sMode239 ;
      }
      else
      {
         RcdFound239 = (short)(0) ;
         initializeNonKey2I239( ) ;
         sMode239 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2I239( ) ;
         Gx_mode = sMode239 ;
      }
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency2I239( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002I2 */
         pr_default.execute(0, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"ICSI_CCCONF1"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1236ICSI_, BC002I2_A1236ICSI_[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"ICSI_CCCONF1"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2I239( )
   {
      beforeValidate2I239( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I239( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2I239( 0) ;
         checkOptimisticConcurrency2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I239( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2I239( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002I15 */
                  pr_default.execute(13, new Object[] {A1237ICSI_, new Integer(A1238ICSI_), new Boolean(n1236ICSI_), A1236ICSI_});
                  if ( (pr_default.getStatus(13) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2I239( ) ;
         }
         endLevel2I239( ) ;
      }
      closeExtendedTableCursors2I239( ) ;
   }

   public void update2I239( )
   {
      beforeValidate2I239( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2I239( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2I239( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2I239( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002I16 */
                  pr_default.execute(14, new Object[] {new Boolean(n1236ICSI_), A1236ICSI_, A1237ICSI_, new Integer(A1238ICSI_)});
                  deferredUpdate2I239( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey2I239( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2I239( ) ;
      }
      closeExtendedTableCursors2I239( ) ;
   }

   public void deferredUpdate2I239( )
   {
   }

   public void delete2I239( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2I239( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2I239( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2I239( ) ;
         /* No cascading delete specified. */
         afterConfirm2I239( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2I239( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC002I17 */
               pr_default.execute(15, new Object[] {A1237ICSI_, new Integer(A1238ICSI_)});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode239 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2I239( ) ;
      Gx_mode = sMode239 ;
   }

   public void onDeleteControls2I239( )
   {
      standaloneModal2I239( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel2I239( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2I239( )
   {
      /* Using cursor BC002I18 */
      pr_default.execute(16, new Object[] {A1237ICSI_});
      RcdFound239 = (short)(0) ;
      if ( (pr_default.getStatus(16) != 101) )
      {
         RcdFound239 = (short)(1) ;
         A1238ICSI_ = BC002I18_A1238ICSI_[0] ;
         A1236ICSI_ = BC002I18_A1236ICSI_[0] ;
         n1236ICSI_ = BC002I18_n1236ICSI_[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2I239( )
   {
      pr_default.readNext(16);
      RcdFound239 = (short)(0) ;
      scanLoad2I239( ) ;
   }

   public void scanLoad2I239( )
   {
      sMode239 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(16) != 101) )
      {
         RcdFound239 = (short)(1) ;
         A1238ICSI_ = BC002I18_A1238ICSI_[0] ;
         A1236ICSI_ = BC002I18_A1236ICSI_[0] ;
         n1236ICSI_ = BC002I18_n1236ICSI_[0] ;
      }
      Gx_mode = sMode239 ;
   }

   public void scanEnd2I239( )
   {
      pr_default.close(16);
   }

   public void afterConfirm2I239( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2I239( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2I239( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2I239( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2I239( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2I239( )
   {
      /* Before Validate Rules */
   }

   public void addRow2I239( )
   {
      SdtICSI_CCConf_Level1Item obj239 ;
      obj239 = new SdtICSI_CCConf_Level1Item(remoteHandle);
      VarsToRow239( obj239) ;
      bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().add(obj239, 0);
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Mode( "UPD" );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Modified( (short)(0) );
   }

   public void sendRow2I239( )
   {
   }

   public void readRow2I239( )
   {
      nGXsfl_239_idx = (short)(nGXsfl_239_idx+1) ;
      RowToVars239( ((SdtICSI_CCConf_Level1Item)bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().elementAt(-1+nGXsfl_239_idx)), 0) ;
   }

   public void confirmValues2I0( )
   {
   }

   public void initializeNonKey2I238( )
   {
      A1421ICSI_ = "" ;
      n1421ICSI_ = false ;
      A1235ICSI_ = (byte)(0) ;
      n1235ICSI_ = false ;
   }

   public void initAll2I238( )
   {
      A1237ICSI_ = "" ;
      initializeNonKey2I238( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void initializeNonKey2I239( )
   {
      A1236ICSI_ = "" ;
      n1236ICSI_ = false ;
   }

   public void initAll2I239( )
   {
      A1238ICSI_ = 0 ;
      initializeNonKey2I239( ) ;
   }

   public void standaloneModalInsert2I239( )
   {
   }

   public void VarsToRow238( SdtICSI_CCConf obj238 )
   {
      obj238.setgxTv_SdtICSI_CCConf_Mode( Gx_mode );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfname( A1421ICSI_ );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfenab( A1235ICSI_ );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfcod( A1237ICSI_ );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z( Z1237ICSI_ );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z( Z1421ICSI_ );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z( Z1235ICSI_ );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfname_N( (byte)((byte)((n1421ICSI_)?1:0)) );
      obj238.setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N( (byte)((byte)((n1235ICSI_)?1:0)) );
      obj238.setgxTv_SdtICSI_CCConf_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars238( SdtICSI_CCConf obj238 ,
                             int forceLoad )
   {
      Gx_mode = obj238.getgxTv_SdtICSI_CCConf_Mode() ;
      A1421ICSI_ = obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfname() ;
      A1235ICSI_ = obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfenab() ;
      A1237ICSI_ = obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfcod() ;
      Z1237ICSI_ = obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z() ;
      Z1421ICSI_ = obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z() ;
      Z1235ICSI_ = obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z() ;
      n1421ICSI_ = (boolean)((obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfname_N()==0)?false:true) ;
      n1235ICSI_ = (boolean)((obj238.getgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N()==0)?false:true) ;
      Gx_mode = obj238.getgxTv_SdtICSI_CCConf_Mode() ;
      return  ;
   }

   public void VarsToRow239( SdtICSI_CCConf_Level1Item obj239 )
   {
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Mode( Gx_mode );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc( A1236ICSI_ );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror( A1238ICSI_ );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z( Z1238ICSI_ );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z( Z1236ICSI_ );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N( (byte)((byte)((n1236ICSI_)?1:0)) );
      obj239.setgxTv_SdtICSI_CCConf_Level1Item_Modified( nIsMod_239 );
      return  ;
   }

   public void RowToVars239( SdtICSI_CCConf_Level1Item obj239 ,
                             int forceLoad )
   {
      Gx_mode = obj239.getgxTv_SdtICSI_CCConf_Level1Item_Mode() ;
      A1236ICSI_ = obj239.getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc() ;
      A1238ICSI_ = obj239.getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror() ;
      Z1238ICSI_ = obj239.getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z() ;
      Z1236ICSI_ = obj239.getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z() ;
      n1236ICSI_ = (boolean)((obj239.getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N()==0)?false:true) ;
      nIsMod_239 = obj239.getgxTv_SdtICSI_CCConf_Level1Item_Modified() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A1237ICSI_ = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey2I238( ) ;
      scanStart2I238( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z1237ICSI_ = A1237ICSI_ ;
      }
      onLoadActions2I238( ) ;
      zm2I238( 0) ;
      addRow2I238( ) ;
      bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Level1().clearCollection();
      if ( ( RcdFound238 == 1 ) )
      {
         scanStart2I239( ) ;
         nGXsfl_239_idx = (short)(1) ;
         while ( ( RcdFound239 != 0 ) )
         {
            onLoadActions2I239( ) ;
            Z1237ICSI_ = A1237ICSI_ ;
            Z1238ICSI_ = A1238ICSI_ ;
            zm2I239( 0) ;
            nRcdExists_239 = (short)(1) ;
            nIsMod_239 = (short)(0) ;
            addRow2I239( ) ;
            nGXsfl_239_idx = (short)(nGXsfl_239_idx+1) ;
            scanNext2I239( ) ;
         }
         scanEnd2I239( ) ;
      }
      scanEnd2I238( ) ;
      if ( ( RcdFound238 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars238( bcICSI_CCConf, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey2I238( ) ;
      if ( ( RcdFound238 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
         {
            A1237ICSI_ = Z1237ICSI_ ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update2I238( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2I238( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2I238( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow238( bcICSI_CCConf) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars238( bcICSI_CCConf, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey2I238( ) ;
      if ( ( RcdFound238 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
         {
            A1237ICSI_ = Z1237ICSI_ ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A1237ICSI_, Z1237ICSI_) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "ticsi_ccconf_bc");
      VarsToRow238( bcICSI_CCConf) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcICSI_CCConf.setgxTv_SdtICSI_CCConf_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtICSI_CCConf sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcICSI_CCConf ) )
      {
         bcICSI_CCConf = sdt ;
         if ( ( GXutil.strcmp(bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Mode(), "") == 0 ) )
         {
            bcICSI_CCConf.setgxTv_SdtICSI_CCConf_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow238( bcICSI_CCConf) ;
         }
         else
         {
            RowToVars238( bcICSI_CCConf, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcICSI_CCConf.getgxTv_SdtICSI_CCConf_Mode(), "") == 0 ) )
         {
            bcICSI_CCConf.setgxTv_SdtICSI_CCConf_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars238( bcICSI_CCConf, 1) ;
      return  ;
   }

   public SdtICSI_CCConf getICSI_CCConf_BC( )
   {
      return bcICSI_CCConf ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z1237ICSI_ = "" ;
      A1237ICSI_ = "" ;
      sMode238 = "" ;
      nIsMod_239 = (short)(0) ;
      RcdFound239 = (short)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = 0 ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(0) ;
      GX_JID = 0 ;
      Z1421ICSI_ = "" ;
      A1421ICSI_ = "" ;
      Z1235ICSI_ = (byte)(0) ;
      A1235ICSI_ = (byte)(0) ;
      Z1238ICSI_ = 0 ;
      A1238ICSI_ = 0 ;
      Z1236ICSI_ = "" ;
      A1236ICSI_ = "" ;
      BC002I6_A1237ICSI_ = new String[] {""} ;
      BC002I6_A1421ICSI_ = new String[] {""} ;
      BC002I6_n1421ICSI_ = new boolean[] {false} ;
      BC002I6_A1235ICSI_ = new byte[1] ;
      BC002I6_n1235ICSI_ = new boolean[] {false} ;
      RcdFound238 = (short)(0) ;
      n1421ICSI_ = false ;
      n1235ICSI_ = false ;
      BC002I7_A1237ICSI_ = new String[] {""} ;
      BC002I5_A1237ICSI_ = new String[] {""} ;
      BC002I5_A1421ICSI_ = new String[] {""} ;
      BC002I5_n1421ICSI_ = new boolean[] {false} ;
      BC002I5_A1235ICSI_ = new byte[1] ;
      BC002I5_n1235ICSI_ = new boolean[] {false} ;
      BC002I4_A1237ICSI_ = new String[] {""} ;
      BC002I4_A1421ICSI_ = new String[] {""} ;
      BC002I4_n1421ICSI_ = new boolean[] {false} ;
      BC002I4_A1235ICSI_ = new byte[1] ;
      BC002I4_n1235ICSI_ = new boolean[] {false} ;
      BC002I11_A1313CadIA = new String[] {""} ;
      BC002I11_A1423POSDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002I11_A1237ICSI_ = new String[] {""} ;
      nRcdExists_239 = (short)(0) ;
      Gxremove239 = (byte)(0) ;
      BC002I12_A1237ICSI_ = new String[] {""} ;
      BC002I12_A1421ICSI_ = new String[] {""} ;
      BC002I12_n1421ICSI_ = new boolean[] {false} ;
      BC002I12_A1235ICSI_ = new byte[1] ;
      BC002I12_n1235ICSI_ = new boolean[] {false} ;
      BC002I13_A1237ICSI_ = new String[] {""} ;
      BC002I13_A1238ICSI_ = new int[1] ;
      BC002I13_A1236ICSI_ = new String[] {""} ;
      BC002I13_n1236ICSI_ = new boolean[] {false} ;
      n1236ICSI_ = false ;
      Gx_BScreen = (byte)(0) ;
      BC002I14_A1237ICSI_ = new String[] {""} ;
      BC002I14_A1238ICSI_ = new int[1] ;
      BC002I3_A1237ICSI_ = new String[] {""} ;
      BC002I3_A1238ICSI_ = new int[1] ;
      BC002I3_A1236ICSI_ = new String[] {""} ;
      BC002I3_n1236ICSI_ = new boolean[] {false} ;
      sMode239 = "" ;
      BC002I2_A1237ICSI_ = new String[] {""} ;
      BC002I2_A1238ICSI_ = new int[1] ;
      BC002I2_A1236ICSI_ = new String[] {""} ;
      BC002I2_n1236ICSI_ = new boolean[] {false} ;
      BC002I18_A1237ICSI_ = new String[] {""} ;
      BC002I18_A1238ICSI_ = new int[1] ;
      BC002I18_A1236ICSI_ = new String[] {""} ;
      BC002I18_n1236ICSI_ = new boolean[] {false} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new ticsi_ccconf_bc__default(),
         new Object[] {
             new Object[] {
            BC002I2_A1237ICSI_, BC002I2_A1238ICSI_, BC002I2_A1236ICSI_, BC002I2_n1236ICSI_
            }
            , new Object[] {
            BC002I3_A1237ICSI_, BC002I3_A1238ICSI_, BC002I3_A1236ICSI_, BC002I3_n1236ICSI_
            }
            , new Object[] {
            BC002I4_A1237ICSI_, BC002I4_A1421ICSI_, BC002I4_n1421ICSI_, BC002I4_A1235ICSI_, BC002I4_n1235ICSI_
            }
            , new Object[] {
            BC002I5_A1237ICSI_, BC002I5_A1421ICSI_, BC002I5_n1421ICSI_, BC002I5_A1235ICSI_, BC002I5_n1235ICSI_
            }
            , new Object[] {
            BC002I6_A1237ICSI_, BC002I6_A1421ICSI_, BC002I6_n1421ICSI_, BC002I6_A1235ICSI_, BC002I6_n1235ICSI_
            }
            , new Object[] {
            BC002I7_A1237ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002I11_A1313CadIA, BC002I11_A1423POSDa, BC002I11_A1237ICSI_
            }
            , new Object[] {
            BC002I12_A1237ICSI_, BC002I12_A1421ICSI_, BC002I12_n1421ICSI_, BC002I12_A1235ICSI_, BC002I12_n1235ICSI_
            }
            , new Object[] {
            BC002I13_A1237ICSI_, BC002I13_A1238ICSI_, BC002I13_A1236ICSI_, BC002I13_n1236ICSI_
            }
            , new Object[] {
            BC002I14_A1237ICSI_, BC002I14_A1238ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002I18_A1237ICSI_, BC002I18_A1238ICSI_, BC002I18_A1236ICSI_, BC002I18_n1236ICSI_
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z ;
   private byte gxTv_SdtICSI_CCConf_Icsi_ccconfname_N ;
   private byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N ;
   private byte gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N ;
   private byte Z1235ICSI_ ;
   private byte A1235ICSI_ ;
   private byte Gxremove239 ;
   private byte Gx_BScreen ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short nGXsfl_239_idx=1 ;
   private short nIsMod_239 ;
   private short RcdFound239 ;
   private short RcdFound238 ;
   private short nRcdExists_239 ;
   private int trnEnded ;
   private int gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z ;
   private int GX_JID ;
   private int Z1238ICSI_ ;
   private int A1238ICSI_ ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String Z1237ICSI_ ;
   private String A1237ICSI_ ;
   private String sMode238 ;
   private String gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z ;
   private String gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z ;
   private String Z1236ICSI_ ;
   private String A1236ICSI_ ;
   private String sMode239 ;
   private boolean n1421ICSI_ ;
   private boolean n1235ICSI_ ;
   private boolean n1236ICSI_ ;
   private String gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z ;
   private String Z1421ICSI_ ;
   private String A1421ICSI_ ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtICSI_CCConf bcICSI_CCConf ;
   private IDataStoreProvider pr_default ;
   private String[] BC002I6_A1237ICSI_ ;
   private String[] BC002I6_A1421ICSI_ ;
   private boolean[] BC002I6_n1421ICSI_ ;
   private byte[] BC002I6_A1235ICSI_ ;
   private boolean[] BC002I6_n1235ICSI_ ;
   private String[] BC002I7_A1237ICSI_ ;
   private String[] BC002I5_A1237ICSI_ ;
   private String[] BC002I5_A1421ICSI_ ;
   private boolean[] BC002I5_n1421ICSI_ ;
   private byte[] BC002I5_A1235ICSI_ ;
   private boolean[] BC002I5_n1235ICSI_ ;
   private String[] BC002I4_A1237ICSI_ ;
   private String[] BC002I4_A1421ICSI_ ;
   private boolean[] BC002I4_n1421ICSI_ ;
   private byte[] BC002I4_A1235ICSI_ ;
   private boolean[] BC002I4_n1235ICSI_ ;
   private String[] BC002I11_A1313CadIA ;
   private java.util.Date[] BC002I11_A1423POSDa ;
   private String[] BC002I11_A1237ICSI_ ;
   private String[] BC002I12_A1237ICSI_ ;
   private String[] BC002I12_A1421ICSI_ ;
   private boolean[] BC002I12_n1421ICSI_ ;
   private byte[] BC002I12_A1235ICSI_ ;
   private boolean[] BC002I12_n1235ICSI_ ;
   private String[] BC002I13_A1237ICSI_ ;
   private int[] BC002I13_A1238ICSI_ ;
   private String[] BC002I13_A1236ICSI_ ;
   private boolean[] BC002I13_n1236ICSI_ ;
   private String[] BC002I14_A1237ICSI_ ;
   private int[] BC002I14_A1238ICSI_ ;
   private String[] BC002I3_A1237ICSI_ ;
   private int[] BC002I3_A1238ICSI_ ;
   private String[] BC002I3_A1236ICSI_ ;
   private boolean[] BC002I3_n1236ICSI_ ;
   private String[] BC002I2_A1237ICSI_ ;
   private int[] BC002I2_A1238ICSI_ ;
   private String[] BC002I2_A1236ICSI_ ;
   private boolean[] BC002I2_n1236ICSI_ ;
   private String[] BC002I18_A1237ICSI_ ;
   private int[] BC002I18_A1238ICSI_ ;
   private String[] BC002I18_A1236ICSI_ ;
   private boolean[] BC002I18_n1236ICSI_ ;
}

final  class ticsi_ccconf_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC002I2", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (UPDLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I3", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I4", "SELECT [ICSI_CCConfCod], [ICSI_CCConfName], [ICSI_CCConfEnab] FROM [ICSI_CCCONF] WITH (UPDLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I5", "SELECT [ICSI_CCConfCod], [ICSI_CCConfName], [ICSI_CCConfEnab] FROM [ICSI_CCCONF] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I6", "SELECT TM1.[ICSI_CCConfCod], TM1.[ICSI_CCConfName], TM1.[ICSI_CCConfEnab] FROM [ICSI_CCCONF] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ICSI_CCConfCod] = ? ORDER BY TM1.[ICSI_CCConfCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I7", "SELECT [ICSI_CCConfCod] FROM [ICSI_CCCONF] WITH (FASTFIRSTROW NOLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002I8", "INSERT INTO [ICSI_CCCONF] ([ICSI_CCConfCod], [ICSI_CCConfName], [ICSI_CCConfEnab]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002I9", "UPDATE [ICSI_CCCONF] SET [ICSI_CCConfName]=?, [ICSI_CCConfEnab]=?  WHERE [ICSI_CCConfCod] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002I10", "DELETE FROM [ICSI_CCCONF]  WHERE [ICSI_CCConfCod] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002I11", "SELECT TOP 1 [CadIATA], [POSData], [ICSI_CCConfCod] FROM [CADASTROSPOS] WITH (NOLOCK) WHERE [ICSI_CCConfCod] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC002I12", "SELECT TM1.[ICSI_CCConfCod], TM1.[ICSI_CCConfName], TM1.[ICSI_CCConfEnab] FROM [ICSI_CCCONF] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ICSI_CCConfCod] = ? ORDER BY TM1.[ICSI_CCConfCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I13", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (FASTFIRSTROW NOLOCK) WHERE [ICSI_CCConfCod] = ? and [ICSI_CCError] = ? ORDER BY [ICSI_CCConfCod], [ICSI_CCError] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002I14", "SELECT [ICSI_CCConfCod], [ICSI_CCError] FROM [ICSI_CCCONF1] WITH (FASTFIRSTROW NOLOCK) WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002I15", "INSERT INTO [ICSI_CCCONF1] ([ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002I16", "UPDATE [ICSI_CCCONF1] SET [ICSI_CCErrorDsc]=?  WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002I17", "DELETE FROM [ICSI_CCCONF1]  WHERE [ICSI_CCConfCod] = ? AND [ICSI_CCError] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002I18", "SELECT [ICSI_CCConfCod], [ICSI_CCError], [ICSI_CCErrorDsc] FROM [ICSI_CCCONF1] WITH (FASTFIRSTROW NOLOCK) WHERE [ICSI_CCConfCod] = ? ORDER BY [ICSI_CCConfCod], [ICSI_CCError] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 6 :
               stmt.setString(1, (String)parms[0], 2);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 40);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(3, ((Number) parms[4]).byteValue());
               }
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 40);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(2, ((Number) parms[3]).byteValue());
               }
               stmt.setString(3, (String)parms[4], 2);
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 9 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 10 :
               stmt.setString(1, (String)parms[0], 2);
               break;
            case 11 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 12 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 13 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 40);
               }
               break;
            case 14 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 40);
               }
               stmt.setString(2, (String)parms[2], 2);
               stmt.setInt(3, ((Number) parms[3]).intValue());
               break;
            case 15 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 16 :
               stmt.setString(1, (String)parms[0], 2);
               break;
      }
   }

}


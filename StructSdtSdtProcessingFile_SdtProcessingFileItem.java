
public final  class StructSdtSdtProcessingFile_SdtProcessingFileItem implements Cloneable, java.io.Serializable
{
   public StructSdtSdtProcessingFile_SdtProcessingFileItem( )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha = "" ;
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus = "" ;
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getLoglinha( )
   {
      return gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha ;
   }

   public void setLoglinha( String value )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha = value ;
      return  ;
   }

   public String getLogstatus( )
   {
      return gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus ;
   }

   public void setLogstatus( String value )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus = value ;
      return  ;
   }

   public String getLogdescricao( )
   {
      return gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao ;
   }

   public void setLogdescricao( String value )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao = value ;
      return  ;
   }

   protected String gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha ;
   protected String gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus ;
   protected String gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao ;
}


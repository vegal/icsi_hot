/*
               File: Gx06N1
        Description: Selection List ICSI CCConf1
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:16.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx06n1 extends GXWorkpanel
{
   public wgx06n1( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx06n1.class ));
   }

   public wgx06n1( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx06N1" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List ICSI CCConf1" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 444 ;
   }

   protected int getFrmHeight( )
   {
      return 282 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx06N1.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String aP0 ,
                        int[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             int[] aP1 )
   {
      wgx06n1.this.A1237ICSI_ = aP0;
      wgx06n1.this.aP1 = aP1;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load10( ) throws GXLoadInterruptException
   {
      subwgx06n110 = new subwgx06n110 ();
      /* Using cursor W00162 */
      pr_default.execute(0, new Object[] {A1237ICSI_, new Integer(AV5cICSI_C)});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A1238ICSI_ = W00162_A1238ICSI_[0] ;
         /* Execute user event: e11V162 */
         e11V162 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx06N1_load10 extends GXLoadProducer
   {
      wgx06n1 _sf ;

      public Gx06N1_load10( wgx06n1 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer10();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load10();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors10();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow10( )
   {
      return true;
   }

   public void autoRefresh_flow10( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (AV5cICSI_C!=cV5cICSI_C) ) || (!loadedFirstTime && ! isLoadAtStartup_flow10() )) {
         subfile.refresh();
         resetSubfileConditions_flow10() ;
      }
   }

   public boolean getSearch_flow10( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow10( )
   {
      cV5cICSI_C = AV5cICSI_C ;
   }

   public void resetSearchConditions_flow10( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow10( )
   {
      return new subwgx06n110 ();
   }

   public boolean getSearch_flow10( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow10( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow10( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow10( )
   {
      GXRefreshCommand10 ();
   }

   public final  class Gx06N1_flow10 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx06n1 _sf ;

      public Gx06N1_flow10( wgx06n1 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow10();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow10(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow10();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow10();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow10(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow10();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow10(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow10(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow10(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow10();
      }

   }

   protected void GXRefreshCommand10( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V162 */
      e12V162 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V162( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV6pICSI_C = A1238ICSI_ ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer10( )
   {
      subwgx06n110 oAux = subwgx06n110 ;
      subwgx06n110 = new subwgx06n110 ();
      variablesToSubfile10 ();
      subGrd_1.addElement(subwgx06n110);
      subwgx06n110 = oAux;
   }

   private void e11V162( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors10( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 444 , 282 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtICSI_CCConfCod = new GUIObjectString ( new GXEdit(2, "XX", UIFactory.getFont( "Courier New", 0, 9),266, 24, 24, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), true) , GXPanel1 , 266 , 24 , 24 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1237ICSI_" );
      ((GXEdit) edtICSI_CCConfCod.getGXComponent()).setAlignment(ILabel.LEFT);
      edtICSI_CCConfCod.addFocusListener(this);
      edtICSI_CCConfCod.getGXComponent().setHelpId("HLP_WGx06N1.htm");
      edtavCicsi_ccerror = new GUIObjectInt ( new GXEdit(6, "ZZZZZ9", UIFactory.getFont( "Courier New", 0, 9),266, 48, 52, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 266 , 48 , 52 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cICSI_C" );
      ((GXEdit) edtavCicsi_ccerror.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtavCicsi_ccerror.addFocusListener(this);
      edtavCicsi_ccerror.getGXComponent().setHelpId("HLP_WGx06N1.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx06N1_load10(this), new Gx06N1_flow10(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectInt ( new GXEdit(6, "ZZZZZ9", UIFactory.getFont( "Courier New", 0, 9),0, 0, 232, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.NUMERIC, false, false, 0, false) , null ,  0 , 0 , 231 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1238ICSI_" ), "C�digo do erro de submiss�o ao Cart�o"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 231 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 72 , 287 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  336 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  336 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  336 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  336 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Company Credit Card", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 119 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "C�digo do erro de submiss�o ao Cart�o", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 223 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                edtICSI_CCConfCod ,
                edtavCicsi_ccerror ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void variablesToSubfile10( )
   {
      subwgx06n110.setICSI_CCError(A1238ICSI_);
      subwgx06n110.setICSI_CCConfCod(A1237ICSI_);
   }

   protected void subfileToVariables10( )
   {
      A1238ICSI_ = subwgx06n110.getICSI_CCError();
      A1237ICSI_ = subwgx06n110.getICSI_CCConfCod();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      edtICSI_CCConfCod.setValue( A1237ICSI_ );
      edtavCicsi_ccerror.setValue( AV5cICSI_C );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      A1237ICSI_ = edtICSI_CCConfCod.getValue() ;
      AV5cICSI_C = edtavCicsi_ccerror.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx06n110 = ( subwgx06n110 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06n110 = new subwgx06n110 ();
      }
      subfileToVariables10 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile10 ();
      subGrd_1.refreshLineValue(subwgx06n110);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx06n110 = ( subwgx06n110 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06n110 = new subwgx06n110 ();
      }
      subfileToVariables10 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V162 */
         e12V162 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V162 */
         e12V162 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtICSI_CCConfCod.isEventSource(eventSource) ) {
         setGXCursor( edtICSI_CCConfCod.getGXCursor() );
         return;
      }
      if ( edtavCicsi_ccerror.isEventSource(eventSource) ) {
         setGXCursor( edtavCicsi_ccerror.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtICSI_CCConfCod.isEventSource(eventSource) ) {
         A1237ICSI_ = edtICSI_CCConfCod.getValue() ;
         return;
      }
      if ( edtavCicsi_ccerror.isEventSource(eventSource) ) {
         AV5cICSI_C = edtavCicsi_ccerror.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V162 */
         e12V162 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP1[0] = wgx06n1.this.AV6pICSI_C;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV6pICSI_C = 0 ;
      subwgx06n110 = new subwgx06n110();
      scmdbuf = "" ;
      AV5cICSI_C = 0 ;
      W00162_A1237ICSI_ = new String[] {""} ;
      W00162_A1238ICSI_ = new int[1] ;
      A1238ICSI_ = 0 ;
      gxIsRefreshing = false ;
      cV5cICSI_C = 0 ;
      returnInSub = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx06n1__default(),
         new Object[] {
             new Object[] {
            W00162_A1237ICSI_, W00162_A1238ICSI_
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected int AV6pICSI_C ;
   protected int AV5cICSI_C ;
   protected int A1238ICSI_ ;
   protected int cV5cICSI_C ;
   protected String A1237ICSI_ ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected int[] aP1 ;
   protected subwgx06n110 subwgx06n110 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00162_A1237ICSI_ ;
   protected int[] W00162_A1238ICSI_ ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtICSI_CCConfCod ;
   protected GUIObjectInt edtavCicsi_ccerror ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
}

final  class wgx06n1__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00162", "SELECT [ICSI_CCConfCod], [ICSI_CCError] FROM [ICSI_CCCONF1] WITH (FASTFIRSTROW NOLOCK) WHERE [ICSI_CCConfCod] = ? and [ICSI_CCError] >= ? ORDER BY [ICSI_CCConfCod], [ICSI_CCError] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((int[]) buf[1])[0] = rslt.getInt(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
      }
   }

}


/*
               File: Airlines
        Description: Airline Data
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tairlines extends GXTransaction
{
   public tairlines( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tairlines.class ), "" );
   }

   public tairlines( int remoteHandle ,
                     ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey2B226( )
   {
      A1126AirLi = GXutil.resetTime( GXutil.nullDate() );
      A1130Airli = "" ;
      n1130Airli = false ;
      A1118AirLi = "" ;
      A1119AirLi = "" ;
      A1120Airli = "" ;
      A1121AirLi = "" ;
      A1122AirLi = "" ;
      A1123AirLi = "" ;
      A1124AirLi = "" ;
      A1127AirLi = "" ;
      n1127AirLi = false ;
      A1128AirLi = "S" ;
      A1129AirLi = "" ;
      n1129AirLi = false ;
   }

   public void initAll2B226( )
   {
      A1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      A23ISOCod = "" ;
      A20ISODes = "" ;
      n20ISODes = false ;
      A1136AirLi = "" ;
      K23ISOCod = A23ISOCod ;
      K1136AirLi = A1136AirLi ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey2B226( ) ;
   }

   public void standaloneModalInsert( )
   {
      A1125AirLi = i1125AirLi ;
      edtAirLineDatIns.setValue(A1125AirLi);
      A1128AirLi = i1128AirLi ;
      cmbAirLineTypeSettlement.setValue(A1128AirLi);
   }

   public void initializeNonKey2B227( )
   {
      AV26AlterB = (byte)(0) ;
      A1131AirLI = "" ;
      A1132AirLi = "" ;
   }

   public void initAll2B227( )
   {
      A1137AirLi = "" ;
      K1137AirLi = A1137AirLi ;
      geteqAfterKey227 = (byte)(1) ;
      initializeNonKey2B227( ) ;
   }

   public void standaloneModalInsert2B227( )
   {
   }

   public void initializeNonKey2B228( )
   {
      A1133AirLi = "" ;
      A1134AirLi = "" ;
      A1135AirLi = "" ;
   }

   public void initAll2B228( )
   {
      A365Contac = "" ;
      A366Contac = "" ;
      A1138AirLi = (short)(0) ;
      K365Contac = A365Contac ;
      K1138AirLi = A1138AirLi ;
      geteqAfterKey228 = (byte)(1) ;
      initializeNonKey2B228( ) ;
   }

   public void standaloneModalInsert2B228( )
   {
   }

   public void resetCaption2B0( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "Airlines" ;
   }

   protected String getFrmTitle( )
   {
      return "Airline Data" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 1221 ;
   }

   protected int getFrmHeight( )
   {
      return 986 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TAirlines.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      tairlines.this.AV11ISOCod = aP0[0];
      this.aP0 = aP0;
      tairlines.this.AV13AirLin = aP1[0];
      this.aP1 = aP1;
      tairlines.this.Gx_mode = aP2[0];
      this.aP2 = aP2;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 1221 , 986 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      dynISOCod = new GUIObjectString ( new GXComboBox(GXPanel1, this, 11) , GXPanel1 , 203 , 89 , 47 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A23ISOCod" );
      dynISOCod.addFocusListener(this);
      dynISOCod.addItemListener(this);
      dynISOCod.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineCode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 113, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 113 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1136AirLi" );
      ((GXEdit) edtAirLineCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineCode.addFocusListener(this);
      edtAirLineCode.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineDescription = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 137, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 137 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1118AirLi" );
      ((GXEdit) edtAirLineDescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineDescription.addFocusListener(this);
      edtAirLineDescription.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtISODes = new GUIObjectString ( new GXEdit(30, "@!", UIFactory.getFont( "Courier New", 0, 9),203, 161, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), true) , GXPanel1 , 203 , 161 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A20ISODes" );
      ((GXEdit) edtISODes.getGXComponent()).setAlignment(ILabel.LEFT);
      edtISODes.addFocusListener(this);
      edtISODes.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineAbbreviation = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 185, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 185 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1119AirLi" );
      ((GXEdit) edtAirLineAbbreviation.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineAbbreviation.addFocusListener(this);
      edtAirLineAbbreviation.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirlineAddress = new GUIObjectString ( new GXEdit(200, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 209, 430, 70, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 209 , 430 , 70 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1120Airli" );
      ((GXEdit) edtAirlineAddress.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirlineAddress.addFocusListener(this);
      edtAirlineAddress.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineAddressCompl = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 281, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 281 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1121AirLi" );
      ((GXEdit) edtAirLineAddressCompl.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineAddressCompl.addFocusListener(this);
      edtAirLineAddressCompl.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineCity = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 305, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 305 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1122AirLi" );
      ((GXEdit) edtAirLineCity.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineCity.addFocusListener(this);
      edtAirLineCity.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineState = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 329, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 329 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1123AirLi" );
      ((GXEdit) edtAirLineState.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineState.addFocusListener(this);
      edtAirLineState.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineZIP = new GUIObjectString ( new GXEdit(10, "XXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 353, 80, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 353 , 80 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1124AirLi" );
      ((GXEdit) edtAirLineZIP.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineZIP.addFocusListener(this);
      edtAirLineZIP.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineDatIns = new GUIObjectDate ( new GXEdit(10, "99/99/9999 99:99", UIFactory.getFont( "Courier New", 0, 9),203, 377, 143, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.DATETIME, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 377 , 143 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1125AirLi" );
      ((GXEdit) edtAirLineDatIns.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineDatIns.addFocusListener(this);
      edtAirLineDatIns.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineDatMod = new GUIObjectDate ( new GXEdit(10, "99/99/9999 99:99", UIFactory.getFont( "Courier New", 0, 9),203, 401, 143, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.DATETIME, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 401 , 143 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1126AirLi" );
      ((GXEdit) edtAirLineDatMod.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineDatMod.addFocusListener(this);
      edtAirLineDatMod.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineCompleteAddress = new GUIObjectString ( new GXEdit(200, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 425, 430, 70, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 425 , 430 , 70 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1127AirLi" );
      ((GXEdit) edtAirLineCompleteAddress.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineCompleteAddress.addFocusListener(this);
      edtAirLineCompleteAddress.getGXComponent().setHelpId("HLP_TAirlines.htm");
      cmbAirLineTypeSettlement = new GUIObjectString ( new GXComboBox(GXPanel1) , GXPanel1 , 203 , 497 , 117 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1128AirLi" );
      ((GXComboBox) cmbAirLineTypeSettlement.getGXComponent()).addItem( "S","Settlement");
      ((GXComboBox) cmbAirLineTypeSettlement.getGXComponent()).addItem( "I","ICCS");
      ((GXComboBox) cmbAirLineTypeSettlement.getGXComponent()).addItem( "N","No Settlement");
      cmbAirLineTypeSettlement.addFocusListener(this);
      cmbAirLineTypeSettlement.addItemListener(this);
      cmbAirLineTypeSettlement.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirLineSta = new GUIObjectString ( new GXEdit(1, "X", UIFactory.getFont( "Courier New", 0, 9),203, 521, 17, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 521 , 17 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1129AirLi" );
      ((GXEdit) edtAirLineSta.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineSta.addFocusListener(this);
      edtAirLineSta.getGXComponent().setHelpId("HLP_TAirlines.htm");
      edtAirlineBankTransferMode = new GUIObjectString ( new GXEdit(1, "X", UIFactory.getFont( "Courier New", 0, 9),203, 545, 17, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 545 , 17 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1130Airli" );
      ((GXEdit) edtAirlineBankTransferMode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirlineBankTransferMode.addFocusListener(this);
      edtAirlineBankTransferMode.getGXComponent().setHelpId("HLP_TAirlines.htm");
      addSubfile ( subGrd_1  = new GXSubfileTRN ( new Airlines_flow43(this) , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(3, "XXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 60, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 59 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1137AirLi" ), "Currency"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 59 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(500, "", UIFactory.getFont( "Courier New", 0, 9),0, 0, 428, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.LONGVARCHAR, false, false, 0, false) , null ,  0 , 0 , 427 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1131AirLI" ), "Data Bank"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 427 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(3, "XXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 98, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 97 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1132AirLi" ), "Tranf. Currency"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 97 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      }, 9 , 18 , GXPanel1 , 21 , 569 , 641 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(false);
      addSubfile ( subGrd_2  = new GXSubfileTRN ( new Airlines_flow47(this) , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 119, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 118 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A365Contac" ), "Contact Type Code"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 118 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectShort ( new GXEdit(4, "ZZZ9", UIFactory.getFont( "Courier New", 0, 9),0, 0, 115, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.NUMERIC, false, false, 0, false) , null ,  0 , 0 , 114 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1138AirLi" ), "Contact Sequence"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 114 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(30, "@!", UIFactory.getFont( "Courier New", 0, 9),0, 0, 218, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 217 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1133AirLi" ), "Contact Name"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 217 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(25, "XXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 183, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 182 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1134AirLi" ), "Contact Name Phone"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 182 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 288, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 287 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1135AirLi" ), "Contact Email"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 287 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , true , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 218, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 217 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A366Contac" ), "Contact Type Description"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 217 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 785 , 1196 , 193 ,  18 ));
      subGrd_2.addActionListener(this);
      subGrd_2.addFocusListener(this);
      subGrd_2.setSortOnClick(false);
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "Country code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 76 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "AirLine Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 73 , 13 );
      lbllbl16 = UIFactory.getLabel(GXPanel1, "Airline Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 137 , 72 , 13 );
      lbllbl18 = UIFactory.getLabel(GXPanel1, "Country Description", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 161 , 112 , 13 );
      lbllbl20 = UIFactory.getLabel(GXPanel1, "Airline Abbreviation", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 185 , 111 , 13 );
      lbllbl22 = UIFactory.getLabel(GXPanel1, "Airline Address", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 209 , 85 , 13 );
      lbllbl24 = UIFactory.getLabel(GXPanel1, "Airline Address Complement", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 281 , 157 , 13 );
      lbllbl26 = UIFactory.getLabel(GXPanel1, "City Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 305 , 58 , 13 );
      lbllbl28 = UIFactory.getLabel(GXPanel1, "State", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 329 , 31 , 13 );
      lbllbl30 = UIFactory.getLabel(GXPanel1, "ZIP Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 353 , 54 , 13 );
      lbllbl32 = UIFactory.getLabel(GXPanel1, "Record Creation Date", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 377 , 124 , 13 );
      lbllbl34 = UIFactory.getLabel(GXPanel1, "Record Modification Date", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 401 , 146 , 13 );
      lbllbl36 = UIFactory.getLabel(GXPanel1, "Complete Address", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 425 , 102 , 13 );
      lbllbl38 = UIFactory.getLabel(GXPanel1, "Type Settlement", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 497 , 93 , 13 );
      lbllbl40 = UIFactory.getLabel(GXPanel1, "Status", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 521 , 37 , 13 );
      lbllbl42 = UIFactory.getLabel(GXPanel1, "Airline Bank Transfer Mode", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 545 , 155 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      subGrd_2.getColumn(0).addContextAction(this, "BROWSE", localUtil.getMessages().getMessage("toolbrowsedata"));
      focusManager.setControlList(new IFocusableControl[] {
                dynISOCod ,
                edtAirLineCode ,
                edtAirLineDescription ,
                edtISODes ,
                edtAirLineAbbreviation ,
                edtAirlineAddress ,
                edtAirLineAddressCompl ,
                edtAirLineCity ,
                edtAirLineState ,
                edtAirLineZIP ,
                edtAirLineDatIns ,
                edtAirLineDatMod ,
                edtAirLineCompleteAddress ,
                cmbAirLineTypeSettlement ,
                edtAirLineSta ,
                edtAirlineBankTransferMode ,
                subGrd_1 ,
                subGrd_2 ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      valid_Isocod();
      setFocus(edtAirLineCode, true);
   }

   public void reloadDynamicLists( int id )
   {
      if ( (id == 11) || (id == 0) )
      {
         /* Using cursor T002B2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            ((GXComboBox) dynISOCod.getGXComponent()).addItem(T002B2_A23ISOCod[0], T002B2_A23ISOCod[0], (short)(0));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }
   }

   public void clear( )
   {
      initializeNonKey2B226( ) ;
      subGrd_1.startLoad();
      subtairlines43 = new subtairlines43 ();
      subGrd_1.endLoad();
      subGrd_2.startLoad();
      subtairlines47 = new subtairlines47 ();
      subGrd_2.endLoad();
   }

   public void disable_std_buttons( )
   {
      bttBtn_first.setGXEnabled( 0 );
      bttBtn_prev.setGXEnabled( 0 );
      bttBtn_next.setGXEnabled( 0 );
      bttBtn_last.setGXEnabled( 0 );
      bttBtn_exit2.setGXEnabled( 0 );
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         dynISOCod.setEnabled( 0 );
         edtAirLineCode.setEnabled( 0 );
         edtAirLineDescription.setEnabled( 0 );
         edtISODes.setEnabled( 0 );
         edtAirLineAbbreviation.setEnabled( 0 );
         edtAirlineAddress.setEnabled( 0 );
         edtAirLineAddressCompl.setEnabled( 0 );
         edtAirLineCity.setEnabled( 0 );
         edtAirLineState.setEnabled( 0 );
         edtAirLineZIP.setEnabled( 0 );
         edtAirLineDatIns.setEnabled( 0 );
         edtAirLineDatMod.setEnabled( 0 );
         edtAirLineCompleteAddress.setEnabled( 0 );
         cmbAirLineTypeSettlement.setEnabled( 0 );
         edtAirLineSta.setEnabled( 0 );
         edtAirlineBankTransferMode.setEnabled( 0 );
         subGrd_1.getColumn(0).setEnabled( 0 );
         subGrd_1.getColumn(1).setEnabled( 0 );
         subGrd_1.getColumn(2).setEnabled( 0 );
         subGrd_2.getColumn(0).setEnabled( 0 );
         subGrd_2.getColumn(1).setEnabled( 0 );
         subGrd_2.getColumn(2).setEnabled( 0 );
         subGrd_2.getColumn(3).setEnabled( 0 );
         subGrd_2.getColumn(4).setEnabled( 0 );
         subGrd_2.getColumn(5).setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll2B226( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   public void loadToBuffer43( )
   {
      subtairlines43 oAux = subtairlines43 ;
      subtairlines43 = new subtairlines43 ();
      variablesToSubfile43 ();
      subGrd_1.addElement(subtairlines43);
      subtairlines43 = oAux;
   }

   public boolean isLoadAtStartup_flow43( )
   {
      return false;
   }

   public void autoRefresh_flow43( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
   }

   public boolean getSearch_flow43( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow43( )
   {
   }

   public void resetSearchConditions_flow43( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow43( )
   {
      if ( subGrd_1.getItemCount() > 0 )
      {
         subtairlines43 = ( subtairlines43 ) subGrd_1.getElementAt(subGrd_1.getItemCount() -1);
         subfileToVariables43 ();
         /* Save values for previous() function. */
      }
      subtairlines43 = new subtairlines43 ();
      initAll2B227( ) ;
      sMode227 = Gx_mode ;
      Gx_mode = "INS" ;
      standaloneModal2B227( ) ;
      Gx_mode = sMode227 ;
      variablesToSubfile43 ();
      return subtairlines43 ;
   }

   public boolean getSearch_flow43( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow43( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow43( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      String Gx_mode = element.getMode();
      subtairlines43 subtairlines43  = ( subtairlines43 ) element;
      if ( col == 0 )
      {
         return ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  );
      }
      return !enabled;
   }

   public void refresh_flow43( )
   {
   }

   public final  class Airlines_flow43 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      tairlines _sf ;

      public Airlines_flow43( tairlines uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow43();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow43(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow43();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow43();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow43(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow43();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow43(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow43(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow43(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow43();
      }

   }

   public void loadToBuffer47( )
   {
      subtairlines47 oAux = subtairlines47 ;
      subtairlines47 = new subtairlines47 ();
      variablesToSubfile47 ();
      subGrd_2.addElement(subtairlines47);
      subtairlines47 = oAux;
   }

   public boolean isLoadAtStartup_flow47( )
   {
      return false;
   }

   public void autoRefresh_flow47( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
   }

   public boolean getSearch_flow47( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow47( )
   {
   }

   public void resetSearchConditions_flow47( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow47( )
   {
      if ( subGrd_2.getItemCount() > 0 )
      {
         subtairlines47 = ( subtairlines47 ) subGrd_2.getElementAt(subGrd_2.getItemCount() -1);
         subfileToVariables47 ();
         /* Save values for previous() function. */
      }
      subtairlines47 = new subtairlines47 ();
      initAll2B228( ) ;
      sMode228 = Gx_mode ;
      Gx_mode = "INS" ;
      standaloneModal2B228( ) ;
      Gx_mode = sMode228 ;
      variablesToSubfile47 ();
      return subtairlines47 ;
   }

   public boolean getSearch_flow47( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow47( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow47( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      String Gx_mode = element.getMode();
      subtairlines47 subtairlines47  = ( subtairlines47 ) element;
      if ( col == 0 )
      {
         return ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  );
      }
      if ( col == 1 )
      {
         return ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  );
      }
      return !enabled;
   }

   public void refresh_flow47( )
   {
   }

   public final  class Airlines_flow47 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      tairlines _sf ;

      public Airlines_flow47( tairlines uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow47();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow47(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow47();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow47();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow47(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow47();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow47(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow47(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow47(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow47();
      }

   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
      /* Execute user event: e112B2 */
      e112B2 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         GXEnter( );
         return;
      }
      if ( subGrd_2.isEventSource(eventSource) ) {
         GXEnter( );
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( dynISOCod.isEventSource(eventSource) ) {
         setGXCursor( dynISOCod.getGXCursor() );
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineCode.getGXCursor() );
         return;
      }
      if ( edtAirLineDescription.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineDescription.getGXCursor() );
         return;
      }
      if ( edtISODes.isEventSource(eventSource) ) {
         setGXCursor( edtISODes.getGXCursor() );
         return;
      }
      if ( edtAirLineAbbreviation.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineAbbreviation.getGXCursor() );
         return;
      }
      if ( edtAirlineAddress.isEventSource(eventSource) ) {
         setGXCursor( edtAirlineAddress.getGXCursor() );
         return;
      }
      if ( edtAirLineAddressCompl.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineAddressCompl.getGXCursor() );
         return;
      }
      if ( edtAirLineCity.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineCity.getGXCursor() );
         return;
      }
      if ( edtAirLineState.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineState.getGXCursor() );
         return;
      }
      if ( edtAirLineZIP.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineZIP.getGXCursor() );
         return;
      }
      if ( edtAirLineDatIns.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineDatIns.getGXCursor() );
         return;
      }
      if ( edtAirLineDatMod.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineDatMod.getGXCursor() );
         return;
      }
      if ( edtAirLineCompleteAddress.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineCompleteAddress.getGXCursor() );
         return;
      }
      if ( cmbAirLineTypeSettlement.isEventSource(eventSource) ) {
         setGXCursor( cmbAirLineTypeSettlement.getGXCursor() );
         return;
      }
      if ( edtAirLineSta.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineSta.getGXCursor() );
         return;
      }
      if ( edtAirlineBankTransferMode.isEventSource(eventSource) ) {
         setGXCursor( edtAirlineBankTransferMode.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtAirLineDatIns.isEventSource(eventSource) ) {
         valid_Airlinedatins ();
         return;
      }
      if ( edtAirLineDatMod.isEventSource(eventSource) ) {
         valid_Airlinedatmod ();
         return;
      }
      if ( cmbAirLineTypeSettlement.isEventSource(eventSource) ) {
         valid_Airlinetypesettlement ();
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         valid_Airlinecode ();
         return;
      }
      if ( edtAirLineDescription.isEventSource(eventSource) ) {
         valid_Airlinedescription ();
         return;
      }
      if ( subGrd_1.getColumn(0).isEventSource(eventSource) ) {
         subtairlines43 = (subtairlines43)subGrd_1.cloneCurrentElement();
         if ( ( subtairlines43.isDeleted() != 1 ) )
         {
            subfileToVariables43 ();
            sMode227 = Gx_mode ;
            Gx_mode = subtairlines43.getTrnMode() ;
            valid_Airlinecurcode ();
            Gx_mode = sMode227 ;
         }
         return;
      }
      if ( subGrd_1.getColumn(1).isEventSource(eventSource) ) {
         subtairlines43 = (subtairlines43)subGrd_1.cloneCurrentElement();
         if ( ( subtairlines43.isDeleted() != 1 ) )
         {
            subfileToVariables43 ();
            sMode227 = Gx_mode ;
            Gx_mode = subtairlines43.getTrnMode() ;
            valid_Airlinedatabank ();
            Gx_mode = sMode227 ;
         }
         return;
      }
      if ( subGrd_1.getColumn(2).isEventSource(eventSource) ) {
         subtairlines43 = (subtairlines43)subGrd_1.cloneCurrentElement();
         if ( ( subtairlines43.isDeleted() != 1 ) )
         {
            subfileToVariables43 ();
            sMode227 = Gx_mode ;
            Gx_mode = subtairlines43.getTrnMode() ;
            valid_Airlinecurtrans ();
            Gx_mode = sMode227 ;
         }
         return;
      }
      if ( dynISOCod.isEventSource(eventSource) ) {
         valid_Isocod ();
         return;
      }
      if ( subGrd_2.getColumn(0).isEventSource(eventSource) ) {
         subtairlines47 = (subtairlines47)subGrd_2.cloneCurrentElement();
         if ( ( subtairlines47.isDeleted() != 1 ) )
         {
            subfileToVariables47 ();
            sMode228 = Gx_mode ;
            Gx_mode = subtairlines47.getTrnMode() ;
            valid_Contacttypescode ();
            Gx_mode = sMode228 ;
         }
         return;
      }
      if ( subGrd_2.getColumn(1).isEventSource(eventSource) ) {
         subtairlines47 = (subtairlines47)subGrd_2.cloneCurrentElement();
         if ( ( subtairlines47.isDeleted() != 1 ) )
         {
            subfileToVariables47 ();
            sMode228 = Gx_mode ;
            Gx_mode = subtairlines47.getTrnMode() ;
            valid_Airlinecttseq ();
            Gx_mode = sMode228 ;
         }
         return;
      }
      if ( subGrd_2.getColumn(5).isEventSource(eventSource) ) {
         subtairlines47 = (subtairlines47)subGrd_2.cloneCurrentElement();
         if ( ( subtairlines47.isDeleted() != 1 ) )
         {
            subfileToVariables47 ();
            sMode228 = Gx_mode ;
            Gx_mode = subtairlines47.getTrnMode() ;
            valid_Contacttypesdescription ();
            Gx_mode = sMode228 ;
         }
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( dynISOCod.isEventSource(eventSource) ) {
         A23ISOCod = dynISOCod.getValue() ;
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         A1136AirLi = edtAirLineCode.getValue() ;
         return;
      }
      if ( edtAirLineDescription.isEventSource(eventSource) ) {
         A1118AirLi = edtAirLineDescription.getValue() ;
         return;
      }
      if ( edtISODes.isEventSource(eventSource) ) {
         A20ISODes = edtISODes.getValue() ;
         if ( ( GXutil.strcmp(A20ISODes, "") != 0 ) )
         {
            n20ISODes = false ;
         }
         return;
      }
      if ( edtAirLineAbbreviation.isEventSource(eventSource) ) {
         A1119AirLi = edtAirLineAbbreviation.getValue() ;
         return;
      }
      if ( edtAirlineAddress.isEventSource(eventSource) ) {
         A1120Airli = edtAirlineAddress.getValue() ;
         return;
      }
      if ( edtAirLineAddressCompl.isEventSource(eventSource) ) {
         A1121AirLi = edtAirLineAddressCompl.getValue() ;
         return;
      }
      if ( edtAirLineCity.isEventSource(eventSource) ) {
         A1122AirLi = edtAirLineCity.getValue() ;
         return;
      }
      if ( edtAirLineState.isEventSource(eventSource) ) {
         A1123AirLi = edtAirLineState.getValue() ;
         return;
      }
      if ( edtAirLineZIP.isEventSource(eventSource) ) {
         A1124AirLi = edtAirLineZIP.getValue() ;
         return;
      }
      if ( edtAirLineDatIns.isEventSource(eventSource) ) {
         A1125AirLi = edtAirLineDatIns.getValue() ;
         return;
      }
      if ( edtAirLineDatMod.isEventSource(eventSource) ) {
         A1126AirLi = edtAirLineDatMod.getValue() ;
         return;
      }
      if ( edtAirLineCompleteAddress.isEventSource(eventSource) ) {
         A1127AirLi = edtAirLineCompleteAddress.getValue() ;
         if ( ( GXutil.strcmp(A1127AirLi, "") != 0 ) )
         {
            n1127AirLi = false ;
         }
         return;
      }
      if ( cmbAirLineTypeSettlement.isEventSource(eventSource) ) {
         A1128AirLi = cmbAirLineTypeSettlement.getValue() ;
         return;
      }
      if ( edtAirLineSta.isEventSource(eventSource) ) {
         A1129AirLi = edtAirLineSta.getValue() ;
         if ( ( GXutil.strcmp(A1129AirLi, "") != 0 ) )
         {
            n1129AirLi = false ;
         }
         return;
      }
      if ( edtAirlineBankTransferMode.isEventSource(eventSource) ) {
         A1130Airli = edtAirlineBankTransferMode.getValue() ;
         if ( ( GXutil.strcmp(A1130Airli, "") != 0 ) )
         {
            n1130Airli = false ;
         }
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      if ( ( GXutil.strcmp(action, "BROWSE") == 0 ) )
      {
         return promptHandler(eventSource) ;
      }
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( dynISOCod.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A23ISOCod, dynISOCod.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineCode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1136AirLi, edtAirLineCode.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtAirLineDescription.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1118AirLi, edtAirLineDescription.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtISODes.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A20ISODes, edtISODes.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineAbbreviation.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1119AirLi, edtAirLineAbbreviation.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirlineAddress.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1120Airli, edtAirlineAddress.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineAddressCompl.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1121AirLi, edtAirLineAddressCompl.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineCity.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1122AirLi, edtAirLineCity.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineState.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1123AirLi, edtAirLineState.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineZIP.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1124AirLi, edtAirLineZIP.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineDatIns.isEventSource(eventSource) ) && ( !( A1125AirLi.equals( edtAirLineDatIns.getValue() ) ) ) )
      {
         return true;
      }
      if ( ( edtAirLineDatMod.isEventSource(eventSource) ) && ( !( A1126AirLi.equals( edtAirLineDatMod.getValue() ) ) ) )
      {
         return true;
      }
      if ( ( edtAirLineCompleteAddress.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1127AirLi, edtAirLineCompleteAddress.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( cmbAirLineTypeSettlement.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1128AirLi, cmbAirLineTypeSettlement.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirLineSta.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1129AirLi, edtAirLineSta.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( edtAirlineBankTransferMode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1130Airli, edtAirlineBankTransferMode.getValue()) != 0 ) ) )
      {
         return true;
      }
      if (subGrd_1.elementsEventSource(eventSource))
      {
         return true;
      }
      if (subGrd_2.elementsEventSource(eventSource))
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption2B0( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      if (( subGrd_2.getColumn(0).isEventSource(eventSource) ) && ( subGrd_2.getColumn(0).isEnabled() )) {
         eventLevelContext( );
         prompt_365_228( ) ;
         eventLevelResetContext( );
         return true ;
      }
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
      if ( subGrd_2 .isEventSource(eventSource)) {
         if ( row < 0 ) {
            subtairlines47 = ( subtairlines47 ) subGrd_2.getCurrentElement() ;
         }
         else
         {
            subtairlines47 = ( subtairlines47 ) subGrd_2.getElementAt(row) ;
         }
         subfileToVariables47 ();
         if ( ( subtairlines47.isDeleted() == 1 ) )
         {
            sMode228 = Gx_mode ;
            Gx_mode = "DLT" ;
         }
         else
         {
            sMode228 = Gx_mode ;
            Gx_mode = "INS" ;
         }
         validate_on_delete228 ();
         Gx_mode = sMode228 ;
      }
      if ( subGrd_1 .isEventSource(eventSource)) {
         if ( row < 0 ) {
            subtairlines43 = ( subtairlines43 ) subGrd_1.getCurrentElement() ;
         }
         else
         {
            subtairlines43 = ( subtairlines43 ) subGrd_1.getElementAt(row) ;
         }
         subfileToVariables43 ();
         if ( ( subtairlines43.isDeleted() == 1 ) )
         {
            sMode227 = Gx_mode ;
            Gx_mode = "DLT" ;
         }
         else
         {
            sMode227 = Gx_mode ;
            Gx_mode = "INS" ;
         }
         validate_on_delete227 ();
         Gx_mode = sMode227 ;
      }
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtAirLineCode.isEventSource(eventSource) )
      {
         edtAirLineCode.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineDescription.isEventSource(eventSource) )
      {
         edtAirLineDescription.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineAbbreviation.isEventSource(eventSource) )
      {
         edtAirLineAbbreviation.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirlineAddress.isEventSource(eventSource) )
      {
         edtAirlineAddress.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineAddressCompl.isEventSource(eventSource) )
      {
         edtAirLineAddressCompl.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineCity.isEventSource(eventSource) )
      {
         edtAirLineCity.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineState.isEventSource(eventSource) )
      {
         edtAirLineState.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineZIP.isEventSource(eventSource) )
      {
         edtAirLineZIP.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineDatIns.isEventSource(eventSource) )
      {
         edtAirLineDatIns.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineDatMod.isEventSource(eventSource) )
      {
         edtAirLineDatMod.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineCompleteAddress.isEventSource(eventSource) )
      {
         edtAirLineCompleteAddress.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( cmbAirLineTypeSettlement.isEventSource(eventSource) )
      {
         cmbAirLineTypeSettlement.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirLineSta.isEventSource(eventSource) )
      {
         edtAirLineSta.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( edtAirlineBankTransferMode.isEventSource(eventSource) )
      {
         edtAirlineBankTransferMode.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void variablesToSubfile43( )
   {
      subtairlines43.setAirLineCurCode(A1137AirLi);
      subtairlines43.setVAlterBank(AV26AlterB);
      subtairlines43.setAirLIneDataBank(A1131AirLI);
      subtairlines43.setAirLineCurTrans(A1132AirLi);
      subtairlines43.setZAirLineCurTrans(Z1132AirLi);
   }

   protected void subfileToVariables43( )
   {
      A1137AirLi = subtairlines43.getAirLineCurCode();
      AV26AlterB = subtairlines43.getVAlterBank();
      A1131AirLI = subtairlines43.getAirLIneDataBank();
      A1132AirLi = subtairlines43.getAirLineCurTrans();
      Z1132AirLi = subtairlines43.getZAirLineCurTrans();
   }

   protected void variablesToSubfile47( )
   {
      subtairlines47.setContactTypesCode(A365Contac);
      subtairlines47.setAirLineCttSeq(A1138AirLi);
      subtairlines47.setAirLineCttName(A1133AirLi);
      subtairlines47.setAirLineCttPhone(A1134AirLi);
      subtairlines47.setAirLineCttEmail(A1135AirLi);
      subtairlines47.setContactTypesDescription(A366Contac);
      subtairlines47.setZAirLineCttName(Z1133AirLi);
      subtairlines47.setZAirLineCttPhone(Z1134AirLi);
      subtairlines47.setZAirLineCttEmail(Z1135AirLi);
   }

   protected void subfileToVariables47( )
   {
      A365Contac = subtairlines47.getContactTypesCode();
      A1138AirLi = subtairlines47.getAirLineCttSeq();
      A1133AirLi = subtairlines47.getAirLineCttName();
      A1134AirLi = subtairlines47.getAirLineCttPhone();
      A1135AirLi = subtairlines47.getAirLineCttEmail();
      A366Contac = subtairlines47.getContactTypesDescription();
      Z1133AirLi = subtairlines47.getZAirLineCttName();
      Z1134AirLi = subtairlines47.getZAirLineCttPhone();
      Z1135AirLi = subtairlines47.getZAirLineCttEmail();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      dynISOCod.setValue( A23ISOCod );
      edtAirLineCode.setValue( A1136AirLi );
      edtAirLineDescription.setValue( A1118AirLi );
      edtISODes.setValue( A20ISODes );
      edtAirLineAbbreviation.setValue( A1119AirLi );
      edtAirlineAddress.setValue( A1120Airli );
      edtAirLineAddressCompl.setValue( A1121AirLi );
      edtAirLineCity.setValue( A1122AirLi );
      edtAirLineState.setValue( A1123AirLi );
      edtAirLineZIP.setValue( A1124AirLi );
      edtAirLineDatIns.setValue( A1125AirLi );
      edtAirLineDatMod.setValue( A1126AirLi );
      edtAirLineCompleteAddress.setValue( A1127AirLi );
      cmbAirLineTypeSettlement.setValue( A1128AirLi );
      edtAirLineSta.setValue( A1129AirLi );
      edtAirlineBankTransferMode.setValue( A1130Airli );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      A23ISOCod = dynISOCod.getValue() ;
      A1136AirLi = edtAirLineCode.getValue() ;
      A1118AirLi = edtAirLineDescription.getValue() ;
      A20ISODes = edtISODes.getValue() ;
      n20ISODes = false ;
      if ( ( GXutil.strcmp(A20ISODes, "") != 0 ) )
      {
         n20ISODes = false ;
      }
      A1119AirLi = edtAirLineAbbreviation.getValue() ;
      A1120Airli = edtAirlineAddress.getValue() ;
      A1121AirLi = edtAirLineAddressCompl.getValue() ;
      A1122AirLi = edtAirLineCity.getValue() ;
      A1123AirLi = edtAirLineState.getValue() ;
      A1124AirLi = edtAirLineZIP.getValue() ;
      A1125AirLi = edtAirLineDatIns.getValue() ;
      A1126AirLi = edtAirLineDatMod.getValue() ;
      A1127AirLi = edtAirLineCompleteAddress.getValue() ;
      n1127AirLi = false ;
      if ( ( GXutil.strcmp(A1127AirLi, "") != 0 ) )
      {
         n1127AirLi = false ;
      }
      A1128AirLi = cmbAirLineTypeSettlement.getValue() ;
      A1129AirLi = edtAirLineSta.getValue() ;
      n1129AirLi = false ;
      if ( ( GXutil.strcmp(A1129AirLi, "") != 0 ) )
      {
         n1129AirLi = false ;
      }
      A1130Airli = edtAirlineBankTransferMode.getValue() ;
      n1130Airli = false ;
      if ( ( GXutil.strcmp(A1130Airli, "") != 0 ) )
      {
         n1130Airli = false ;
      }
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subtairlines43 = ( subtairlines43 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subtairlines43 = new subtairlines43 ();
      }
      subfileToVariables43 ();
      if ( subGrd_2.inValidElement() )
      {
         subtairlines47 = ( subtairlines47 ) subGrd_2.getCurrentElement() ;
      }
      else
      {
         subtairlines47 = new subtairlines47 ();
      }
      subfileToVariables47 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile43 ();
      subGrd_1.refreshLineValue(subtairlines43);
      variablesToSubfile47 ();
      subGrd_2.refreshLineValue(subtairlines47);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subtairlines43 = ( subtairlines43 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subtairlines43 = new subtairlines43 ();
      }
      subfileToVariables43 ();
      if ( subGrd_2.inValidElement() )
      {
         subtairlines47 = ( subtairlines47 ) subGrd_2.getCurrentElement() ;
      }
      else
      {
         subtairlines47 = new subtairlines47 ();
      }
      subfileToVariables47 ();
   }

   public void prompt_365_228( )
   {
      GXv_svchar1[0] = A365Contac ;
      new wgx0670(remoteHandle, context).execute( GXv_svchar1) ;
      tairlines.this.A365Contac = GXv_svchar1[0] ;
      ((subtairlines47)subGrd_2.getCurrentElement()).setContactTypesCode(A365Contac);
      subGrd_2.refreshLineValue(subGrd_2.getSelectedElement());
      subGrd_2.repaint();
      resetCaption2B0( ) ;
   }

   public void valid_Isocod( )
   {
      /* Using cursor T002B3 */
      pr_default.execute(1, new Object[] {A23ISOCod});
      if ( (pr_default.getStatus(1) == 101) )
      {
         GXutil.msg( me(), "No matching 'Country'." );
         AnyError = (short)(1) ;
         setNextFocus( dynISOCod );
         setFocusNext();
      }
      A20ISODes = T002B3_A20ISODes[0] ;
      n20ISODes = T002B3_n20ISODes[0] ;
      pr_default.close(1);
      edtISODes.setValue( A20ISODes );
      pr_default.close(1);
   }

   public void valid_Airlinecode( )
   {
      if ( ( GXutil.strcmp(A23ISOCod, K23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, K1136AirLi) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K23ISOCod = A23ISOCod ;
            K1136AirLi = A1136AirLi ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void valid_Airlinedescription( )
   {
   }

   public void valid_Airlinedatins( )
   {
      if ( ! ( (GXutil.nullDate().equals(A1125AirLi)) || (( A1125AirLi.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1125AirLi.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         GXutil.msg( me(), "Field Record Creation Date is out of range" );
         AnyError = (short)(1) ;
         setNextFocus( edtAirLineDatIns );
         setFocusNext();
      }
   }

   public void valid_Airlinedatmod( )
   {
      if ( ! ( (GXutil.nullDate().equals(A1126AirLi)) || (( A1126AirLi.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1126AirLi.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         GXutil.msg( me(), "Field Record Modification Date is out of range" );
         AnyError = (short)(1) ;
         setNextFocus( edtAirLineDatMod );
         setFocusNext();
      }
   }

   public void valid_Airlinetypesettlement( )
   {
      if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
      {
         A1130Airli = "" ;
         edtAirlineBankTransferMode.setValue(A1130Airli);
         n1130Airli = false ;
      }
      edtAirlineBankTransferMode.setValue( A1130Airli );
   }

   public void valid_Airlinecurcode( )
   {
      if ( ( GXutil.strcmp(A1137AirLi, K1137AirLi) != 0 ) || ( geteqAfterKey227 == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey227 = (byte)(0) ;
         }
         else
         {
            geteqAfterKey227 = (byte)(1) ;
         }
      }
   }

   public void valid_Airlinedatabank( )
   {
   }

   public void valid_Airlinecurtrans( )
   {
      reloadGridRow();
   }

   public void validate_on_delete227( )
   {
      /* No delete mode formulas found. */
   }

   public void valid_Contacttypescode( )
   {
      /* Using cursor T002B4 */
      pr_default.execute(2, new Object[] {A365Contac});
      if ( (pr_default.getStatus(2) == 101) )
      {
         GXutil.msg( me(), "No matching 'Contact Types'." );
         AnyError = (short)(1) ;
         subGrd_2.cancelSubfileMove();
      }
      A366Contac = T002B4_A366Contac[0] ;
      pr_default.close(2);
      ((subtairlines47)subGrd_2.getCurrentElement()).setContactTypesDescription(A366Contac);
      subGrd_2.refreshLineValue(subGrd_2.getSelectedElement());
      subGrd_2.repaint();
      pr_default.close(2);
   }

   public void valid_Airlinecttseq( )
   {
      if ( ( GXutil.strcmp(A365Contac, K365Contac) != 0 ) || ( A1138AirLi != K1138AirLi ) || ( geteqAfterKey228 == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey228 = (byte)(0) ;
         }
         else
         {
            geteqAfterKey228 = (byte)(1) ;
         }
      }
   }

   public void valid_Contacttypesdescription( )
   {
      reloadGridRow();
   }

   public void validate_on_delete228( )
   {
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor T002B4 */
         pr_default.execute(2, new Object[] {A365Contac});
         A366Contac = T002B4_A366Contac[0] ;
         pr_default.close(2);
      }
   }

   public void e112B2( )
   {
      eventNoLevelContext();
      /* Start Routine */
   }

   public void e122B2( )
   {
      eventLevelContext();
      /* 'back' Routine */
      pr_default.close(2);
      pr_default.close(1);
      if (canCleanup()) {
         returnInSub = true;
      }
      if (true) return;
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
         {
            sMode226 = Gx_mode ;
            Gx_mode = "UPD" ;
            if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               A23ISOCod = AV11ISOCod ;
               dynISOCod.setValue(A23ISOCod);
            }
            if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  )
            {
               A1136AirLi = AV13AirLin ;
               edtAirLineCode.setValue(A1136AirLi);
            }
            Gx_mode = sMode226 ;
         }
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            getByPrimaryKey( ) ;
            if ( ( RcdFound226 != 1 ) )
            {
               pushError( localUtil.getMessages().getMessage("noinsert") );
               AnyError = (short)(1) ;
               keepFocus();
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            i1125AirLi = A1125AirLi ;
            i1128AirLi = A1128AirLi ;
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm2B226( int GX_JID )
   {
      if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1126AirLi = T002B10_A1126AirLi[0] ;
            Z1125AirLi = T002B10_A1125AirLi[0] ;
            Z1130Airli = T002B10_A1130Airli[0] ;
            Z1118AirLi = T002B10_A1118AirLi[0] ;
            Z1119AirLi = T002B10_A1119AirLi[0] ;
            Z1120Airli = T002B10_A1120Airli[0] ;
            Z1121AirLi = T002B10_A1121AirLi[0] ;
            Z1122AirLi = T002B10_A1122AirLi[0] ;
            Z1123AirLi = T002B10_A1123AirLi[0] ;
            Z1124AirLi = T002B10_A1124AirLi[0] ;
            Z1127AirLi = T002B10_A1127AirLi[0] ;
            Z1128AirLi = T002B10_A1128AirLi[0] ;
            Z1129AirLi = T002B10_A1129AirLi[0] ;
         }
         else
         {
            Z1126AirLi = A1126AirLi ;
            Z1125AirLi = A1125AirLi ;
            Z1130Airli = A1130Airli ;
            Z1118AirLi = A1118AirLi ;
            Z1119AirLi = A1119AirLi ;
            Z1120Airli = A1120Airli ;
            Z1121AirLi = A1121AirLi ;
            Z1122AirLi = A1122AirLi ;
            Z1123AirLi = A1123AirLi ;
            Z1124AirLi = A1124AirLi ;
            Z1127AirLi = A1127AirLi ;
            Z1128AirLi = A1128AirLi ;
            Z1129AirLi = A1129AirLi ;
         }
      }
      if ( ( GX_JID == -21 ) )
      {
         Z1136AirLi = A1136AirLi ;
         Z1126AirLi = A1126AirLi ;
         Z1125AirLi = A1125AirLi ;
         Z1130Airli = A1130Airli ;
         Z1118AirLi = A1118AirLi ;
         Z1119AirLi = A1119AirLi ;
         Z1120Airli = A1120Airli ;
         Z1121AirLi = A1121AirLi ;
         Z1122AirLi = A1122AirLi ;
         Z1123AirLi = A1123AirLi ;
         Z1124AirLi = A1124AirLi ;
         Z1127AirLi = A1127AirLi ;
         Z1128AirLi = A1128AirLi ;
         Z1129AirLi = A1129AirLi ;
         Z23ISOCod = A23ISOCod ;
      }
   }

   public void standaloneNotModal( )
   {
      dynISOCod.setEnabled( 0 );
      edtAirlineBankTransferMode.setEnabled( 1 );
      Gx_BScreen = (byte)(0) ;
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         A1125AirLi = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
         edtAirLineDatIns.setValue(A1125AirLi);
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  )
      {
         A1136AirLi = AV13AirLin ;
         edtAirLineCode.setValue(A1136AirLi);
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         A23ISOCod = AV11ISOCod ;
         dynISOCod.setValue(A23ISOCod);
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  && ((GXutil.strcmp("", GXutil.rtrim( A1128AirLi))==0)) && ( Gx_BScreen == 0 ) )
      {
         A1128AirLi = "S" ;
         cmbAirLineTypeSettlement.setValue(A1128AirLi);
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
      {
         /* Using cursor T002B3 */
         pr_default.execute(1, new Object[] {A23ISOCod});
         A20ISODes = T002B3_A20ISODes[0] ;
         n20ISODes = T002B3_n20ISODes[0] ;
         pr_default.close(1);
         if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
         {
            A1130Airli = "" ;
            edtAirlineBankTransferMode.setValue(A1130Airli);
            n1130Airli = false ;
         }
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         subGrd_1.setEnabled( 0 );
         subGrd_2.setEnabled( 0 );
      }
   }

   public void load2B226( )
   {
      /* Using cursor T002B11 */
      pr_default.execute(9, new Object[] {A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound226 = (short)(1) ;
         A1126AirLi = T002B11_A1126AirLi[0] ;
         A1125AirLi = T002B11_A1125AirLi[0] ;
         A1130Airli = T002B11_A1130Airli[0] ;
         n1130Airli = T002B11_n1130Airli[0] ;
         A1118AirLi = T002B11_A1118AirLi[0] ;
         A20ISODes = T002B11_A20ISODes[0] ;
         n20ISODes = T002B11_n20ISODes[0] ;
         A1119AirLi = T002B11_A1119AirLi[0] ;
         A1120Airli = T002B11_A1120Airli[0] ;
         A1121AirLi = T002B11_A1121AirLi[0] ;
         A1122AirLi = T002B11_A1122AirLi[0] ;
         A1123AirLi = T002B11_A1123AirLi[0] ;
         A1124AirLi = T002B11_A1124AirLi[0] ;
         A1127AirLi = T002B11_A1127AirLi[0] ;
         n1127AirLi = T002B11_n1127AirLi[0] ;
         A1128AirLi = T002B11_A1128AirLi[0] ;
         A1129AirLi = T002B11_A1129AirLi[0] ;
         n1129AirLi = T002B11_n1129AirLi[0] ;
         zm2B226( -21) ;
      }
      pr_default.close(9);
      onLoadActions2B226( ) ;
   }

   public void onLoadActions2B226( )
   {
      if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
      {
         A1130Airli = "" ;
         edtAirlineBankTransferMode.setValue(A1130Airli);
         n1130Airli = false ;
      }
   }

   public void checkExtendedTable2B226( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      /* Using cursor T002B3 */
      pr_default.execute(1, new Object[] {A23ISOCod});
      if ( (pr_default.getStatus(1) == 101) )
      {
         pushError( "No matching 'Country'." );
         AnyError = (short)(1) ;
         setNextFocus( dynISOCod );
      }
      A20ISODes = T002B3_A20ISODes[0] ;
      n20ISODes = T002B3_n20ISODes[0] ;
      pr_default.close(1);
      if ( ! ( (GXutil.nullDate().equals(A1125AirLi)) || (( A1125AirLi.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1125AirLi.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         pushError( "Field Record Creation Date is out of range" );
         AnyError = (short)(1) ;
         keepFocus();
      }
      if ( ! ( (GXutil.nullDate().equals(A1126AirLi)) || (( A1126AirLi.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1126AirLi.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         pushError( "Field Record Modification Date is out of range" );
         AnyError = (short)(1) ;
         keepFocus();
      }
      if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
      {
         A1130Airli = "" ;
         edtAirlineBankTransferMode.setValue(A1130Airli);
         n1130Airli = false ;
      }
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2B226( )
   {
      pr_default.close(1);
   }

   public void enableDisable( )
   {
   }

   public void getKey2B226( )
   {
      /* Using cursor T002B12 */
      pr_default.execute(10, new Object[] {A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(10) != 101) )
      {
         RcdFound226 = (short)(1) ;
      }
      else
      {
         RcdFound226 = (short)(0) ;
      }
      pr_default.close(10);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T002B10 */
      pr_default.execute(8, new Object[] {A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(8) != 101) )
      {
         zm2B226( 21) ;
         RcdFound226 = (short)(1) ;
         A1136AirLi = T002B10_A1136AirLi[0] ;
         A1126AirLi = T002B10_A1126AirLi[0] ;
         A1125AirLi = T002B10_A1125AirLi[0] ;
         A1130Airli = T002B10_A1130Airli[0] ;
         n1130Airli = T002B10_n1130Airli[0] ;
         A1118AirLi = T002B10_A1118AirLi[0] ;
         A1119AirLi = T002B10_A1119AirLi[0] ;
         A1120Airli = T002B10_A1120Airli[0] ;
         A1121AirLi = T002B10_A1121AirLi[0] ;
         A1122AirLi = T002B10_A1122AirLi[0] ;
         A1123AirLi = T002B10_A1123AirLi[0] ;
         A1124AirLi = T002B10_A1124AirLi[0] ;
         A1127AirLi = T002B10_A1127AirLi[0] ;
         n1127AirLi = T002B10_n1127AirLi[0] ;
         A1128AirLi = T002B10_A1128AirLi[0] ;
         A1129AirLi = T002B10_A1129AirLi[0] ;
         n1129AirLi = T002B10_n1129AirLi[0] ;
         A23ISOCod = T002B10_A23ISOCod[0] ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         sMode226 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2B226( ) ;
         Gx_mode = sMode226 ;
      }
      else
      {
         RcdFound226 = (short)(0) ;
         initializeNonKey2B226( ) ;
         sMode226 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode226 ;
      }
      K23ISOCod = A23ISOCod ;
      K1136AirLi = A1136AirLi ;
      pr_default.close(8);
      subGrd_1.startLoad();
      subtairlines43 = new subtairlines43 ();
      if ( ( RcdFound226 == 1 ) )
      {
         scanStart2B227( ) ;
         while ( ( RcdFound227 != 0 ) )
         {
            getByPrimaryKey2B227( ) ;
            addRow2B227( ) ;
            scanNext2B227( ) ;
         }
         scanEnd2B227( ) ;
      }
      subGrd_1.endLoad(new subtairlines43());
      subGrd_2.startLoad();
      subtairlines47 = new subtairlines47 ();
      if ( ( RcdFound226 == 1 ) )
      {
         scanStart2B228( ) ;
         while ( ( RcdFound228 != 0 ) )
         {
            getByPrimaryKey2B228( ) ;
            addRow2B228( ) ;
            scanNext2B228( ) ;
         }
         scanEnd2B228( ) ;
      }
      subGrd_2.endLoad(new subtairlines47());
   }

   public void getEqualNoModal( )
   {
      getKey2B226( ) ;
      if ( ( RcdFound226 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound226 = (short)(0) ;
      /* Using cursor T002B13 */
      pr_default.execute(11, new Object[] {A23ISOCod, A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(11) != 101) )
      {
         while ( (pr_default.getStatus(11) != 101) && ( ( GXutil.strcmp(T002B13_A23ISOCod[0], A23ISOCod) < 0 ) || ( GXutil.strcmp(T002B13_A23ISOCod[0], A23ISOCod) == 0 ) && ( GXutil.strcmp(T002B13_A1136AirLi[0], A1136AirLi) < 0 ) ) )
         {
            pr_default.readNext(11);
         }
         if ( (pr_default.getStatus(11) != 101) && ( ( GXutil.strcmp(T002B13_A23ISOCod[0], A23ISOCod) > 0 ) || ( GXutil.strcmp(T002B13_A23ISOCod[0], A23ISOCod) == 0 ) && ( GXutil.strcmp(T002B13_A1136AirLi[0], A1136AirLi) > 0 ) ) )
         {
            A23ISOCod = T002B13_A23ISOCod[0] ;
            A1136AirLi = T002B13_A1136AirLi[0] ;
            RcdFound226 = (short)(1) ;
         }
      }
      pr_default.close(11);
   }

   public void move_previous( )
   {
      RcdFound226 = (short)(0) ;
      /* Using cursor T002B14 */
      pr_default.execute(12, new Object[] {A23ISOCod, A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(12) != 101) )
      {
         while ( (pr_default.getStatus(12) != 101) && ( ( GXutil.strcmp(T002B14_A23ISOCod[0], A23ISOCod) > 0 ) || ( GXutil.strcmp(T002B14_A23ISOCod[0], A23ISOCod) == 0 ) && ( GXutil.strcmp(T002B14_A1136AirLi[0], A1136AirLi) > 0 ) ) )
         {
            pr_default.readNext(12);
         }
         if ( (pr_default.getStatus(12) != 101) && ( ( GXutil.strcmp(T002B14_A23ISOCod[0], A23ISOCod) < 0 ) || ( GXutil.strcmp(T002B14_A23ISOCod[0], A23ISOCod) == 0 ) && ( GXutil.strcmp(T002B14_A1136AirLi[0], A1136AirLi) < 0 ) ) )
         {
            A23ISOCod = T002B14_A23ISOCod[0] ;
            A1136AirLi = T002B14_A1136AirLi[0] ;
            RcdFound226 = (short)(1) ;
         }
      }
      pr_default.close(12);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey2B226( ) ;
      if ( ( RcdFound226 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( dynISOCod );
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            dynISOCod.setValue(A23ISOCod);
            A1136AirLi = Z1136AirLi ;
            edtAirLineCode.setValue(A1136AirLi);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( dynISOCod );
         }
         else
         {
            /* Update record */
            update2B226( ) ;
            setNextFocus( dynISOCod );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
         {
            /* Insert record */
            insert2B226( ) ;
            setNextFocus( dynISOCod );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( dynISOCod );
            }
            else
            {
               /* Insert record */
               insert2B226( ) ;
               setNextFocus( dynISOCod );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
      {
         A23ISOCod = Z23ISOCod ;
         dynISOCod.setValue(A23ISOCod);
         A1136AirLi = Z1136AirLi ;
         edtAirLineCode.setValue(A1136AirLi);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( dynISOCod );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( dynISOCod );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void checkOptimisticConcurrency2B226( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002B9 */
         pr_default.execute(7, new Object[] {A23ISOCod, A1136AirLi});
         if ( ! (pr_default.getStatus(7) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"AIRLINES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         Gx_longc = false ;
         if ( (pr_default.getStatus(7) == 101) || !( Z1126AirLi.equals( T002B9_A1126AirLi[0] ) ) || !( Z1125AirLi.equals( T002B9_A1125AirLi[0] ) ) || ( GXutil.strcmp(Z1130Airli, T002B9_A1130Airli[0]) != 0 ) || ( GXutil.strcmp(Z1118AirLi, T002B9_A1118AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1119AirLi, T002B9_A1119AirLi[0]) != 0 ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || ( GXutil.strcmp(Z1120Airli, T002B9_A1120Airli[0]) != 0 ) || ( GXutil.strcmp(Z1121AirLi, T002B9_A1121AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1122AirLi, T002B9_A1122AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1123AirLi, T002B9_A1123AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1124AirLi, T002B9_A1124AirLi[0]) != 0 ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || ( GXutil.strcmp(Z1127AirLi, T002B9_A1127AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1128AirLi, T002B9_A1128AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1129AirLi, T002B9_A1129AirLi[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"AIRLINES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2B226( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2B226( 0) ;
         checkOptimisticConcurrency2B226( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2B226( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B15 */
                  pr_default.execute(13, new Object[] {A1136AirLi, A1126AirLi, A1125AirLi, new Boolean(n1130Airli), A1130Airli, A1118AirLi, A1119AirLi, A1120Airli, A1121AirLi, A1122AirLi, A1123AirLi, A1124AirLi, new Boolean(n1127AirLi), A1127AirLi, A1128AirLi, new Boolean(n1129AirLi), A1129AirLi, A23ISOCod});
                  if ( (pr_default.getStatus(13) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2B226( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                           resetCaption2B0( ) ;
                           subGrd_1.startLoad();
                           subtairlines43 = new subtairlines43 ();
                           subGrd_1.endLoad();
                           subGrd_2.startLoad();
                           subtairlines47 = new subtairlines47 ();
                           subGrd_2.endLoad();
                        }
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2B226( ) ;
         }
         endLevel2B226( ) ;
      }
      closeExtendedTableCursors2B226( ) ;
   }

   public void update2B226( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B226( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2B226( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B16 */
                  pr_default.execute(14, new Object[] {A1126AirLi, A1125AirLi, new Boolean(n1130Airli), A1130Airli, A1118AirLi, A1119AirLi, A1120Airli, A1121AirLi, A1122AirLi, A1123AirLi, A1124AirLi, new Boolean(n1127AirLi), A1127AirLi, A1128AirLi, new Boolean(n1129AirLi), A1129AirLi, A23ISOCod, A1136AirLi});
                  deferredUpdate2B226( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2B226( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           loopOnce = true;
                        }
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2B226( ) ;
      }
      closeExtendedTableCursors2B226( ) ;
   }

   public void deferredUpdate2B226( )
   {
   }

   public void delete( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2B226( ) ;
         scanStart2B228( ) ;
         while ( ( RcdFound228 != 0 ) )
         {
            getByPrimaryKey2B228( ) ;
            delete2B228( ) ;
            scanNext2B228( ) ;
         }
         scanEnd2B228( ) ;
         scanStart2B227( ) ;
         while ( ( RcdFound227 != 0 ) )
         {
            getByPrimaryKey2B227( ) ;
            delete2B227( ) ;
            scanNext2B227( ) ;
         }
         scanEnd2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete2B226( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B17 */
                  pr_default.execute(15, new Object[] {A23ISOCod, A1136AirLi});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
      }
      sMode226 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2B226( ) ;
      Gx_mode = sMode226 ;
   }

   public void onDeleteControls2B226( )
   {
      standaloneModal( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor T002B18 */
         pr_default.execute(16, new Object[] {A23ISOCod});
         A20ISODes = T002B18_A20ISODes[0] ;
         n20ISODes = T002B18_n20ISODes[0] ;
         pr_default.close(16);
      }
   }

   public void processNestedLevel2B227( )
   {
      nGXsfl_43_idx = (short)(0) ;
      while ( ( nGXsfl_43_idx < subGrd_1.getItemCount() ) )
      {
         readRow2B227( ) ;
         if ( ( subtairlines43.isLoaded() != 0 ) || ( subtairlines43.isChanged() != 0 ) )
         {
            standaloneNotModal2B227( ) ;
            getKey2B227( ) ;
            if ( ( subtairlines43.isLoaded() == 0 ) && ( subtairlines43.isDeleted() == 0 ) )
            {
               if ( ( RcdFound227 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  insert2B227( ) ;
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("noupdate") );
                  AnyError = (short)(1) ;
                  setNextFocus( dynISOCod );
               }
            }
            else
            {
               if ( ( RcdFound227 != 0 ) )
               {
                  if ( ( subtairlines43.isDeleted() != 0 ) && ( subtairlines43.isLoaded() != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     delete2B227( ) ;
                  }
                  else
                  {
                     if ( ( subtairlines43.isChanged() != 0 ) && ( subtairlines43.isLoaded() != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        update2B227( ) ;
                     }
                  }
               }
               else
               {
                  if ( ( subtairlines43.isDeleted() == 0 ) )
                  {
                     pushError( localUtil.getMessages().getMessage("recdeleted") );
                     AnyError = (short)(1) ;
                     setNextFocus( dynISOCod );
                  }
               }
            }
         }
      }
      subGrd_1.endLoad(new subtairlines43());
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void processNestedLevel2B228( )
   {
      nGXsfl_47_idx = (short)(0) ;
      while ( ( nGXsfl_47_idx < subGrd_2.getItemCount() ) )
      {
         readRow2B228( ) ;
         if ( ( subtairlines47.isLoaded() != 0 ) || ( subtairlines47.isChanged() != 0 ) )
         {
            standaloneNotModal2B228( ) ;
            getKey2B228( ) ;
            if ( ( subtairlines47.isLoaded() == 0 ) && ( subtairlines47.isDeleted() == 0 ) )
            {
               if ( ( RcdFound228 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  insert2B228( ) ;
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("noupdate") );
                  AnyError = (short)(1) ;
                  setNextFocus( dynISOCod );
               }
            }
            else
            {
               if ( ( RcdFound228 != 0 ) )
               {
                  if ( ( subtairlines47.isDeleted() != 0 ) && ( subtairlines47.isLoaded() != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     delete2B228( ) ;
                  }
                  else
                  {
                     if ( ( subtairlines47.isChanged() != 0 ) && ( subtairlines47.isLoaded() != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        update2B228( ) ;
                     }
                  }
               }
               else
               {
                  if ( ( subtairlines47.isDeleted() == 0 ) )
                  {
                     pushError( localUtil.getMessages().getMessage("recdeleted") );
                     AnyError = (short)(1) ;
                     setNextFocus( dynISOCod );
                  }
               }
            }
         }
      }
      subGrd_2.endLoad(new subtairlines47());
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void processLevel2B226( )
   {
      /* Save parent mode. */
      sMode226 = Gx_mode ;
      processNestedLevel2B227( ) ;
      processNestedLevel2B228( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode226 ;
      /* ' Update level parameters */
   }

   public void endLevel2B226( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(7);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2B226( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         pr_default.close(6);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(3);
         pr_default.close(16);
         pr_default.close(2);
         Application.commit(context, remoteHandle, "DEFAULT", "tairlines");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         pr_default.close(6);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(3);
         pr_default.close(16);
         pr_default.close(2);
         Application.rollback(context, remoteHandle, "DEFAULT", "tairlines");
         nGXsfl_43_idx = (short)(0) ;
         while ( ( nGXsfl_43_idx < subGrd_1.getItemCount() ) )
         {
            readRow2B227( ) ;
            /* Using cursor T002B8 */
            pr_default.execute(6, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
            if ( (pr_default.getStatus(6) != 101) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
               {
                  Z1132AirLi = T002B8_A1132AirLi[0] ;
               }
               else
               {
                  Z1132AirLi = A1132AirLi ;
               }
               variablesToSubfile43 ();
            }
            pr_default.close(6);
         }
      }
      IsModified = (short)(0) ;
   }

   public void scanStart2B226( )
   {
      /* Using cursor T002B19 */
      pr_default.execute(17);
      RcdFound226 = (short)(0) ;
      if ( (pr_default.getStatus(17) != 101) )
      {
         RcdFound226 = (short)(1) ;
         A23ISOCod = T002B19_A23ISOCod[0] ;
         A1136AirLi = T002B19_A1136AirLi[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2B226( )
   {
      pr_default.readNext(17);
      RcdFound226 = (short)(0) ;
      if ( (pr_default.getStatus(17) != 101) )
      {
         RcdFound226 = (short)(1) ;
         A23ISOCod = T002B19_A23ISOCod[0] ;
         A1136AirLi = T002B19_A1136AirLi[0] ;
      }
   }

   public void scanEnd2B226( )
   {
      pr_default.close(17);
   }

   public void afterConfirm2B226( )
   {
      /* After Confirm Rules */
      A1126AirLi = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      edtAirLineDatMod.setValue(A1126AirLi);
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1136AirLi))==0)) )
      {
         pushError( "Airline Code cannot be empty" );
         AnyError = (short)(1) ;
         keepFocus();
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1118AirLi))==0)) )
      {
         pushError( "Airline Description cannot be empty" );
         AnyError = (short)(1) ;
         keepFocus();
         return  ;
      }
   }

   public void beforeInsert2B226( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2B226( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2B226( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2B226( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2B226( )
   {
      /* Before Validate Rules */
   }

   public void zm2B227( int GX_JID )
   {
      if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1132AirLi = T002B8_A1132AirLi[0] ;
         }
         else
         {
            Z1132AirLi = A1132AirLi ;
         }
      }
      if ( ( GX_JID == -23 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z1137AirLi = A1137AirLi ;
         Z1131AirLI = A1131AirLI ;
         Z1132AirLi = A1132AirLi ;
      }
   }

   public void standaloneNotModal2B227( )
   {
      subGrd_1.getColumn(0).setEnabled( 1 );
      subGrd_1.getColumn(1).setEnabled( 1 );
      subGrd_1.getColumn(2).setEnabled( 1 );
   }

   public void standaloneModal2B227( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         subGrd_1.setEnabled( 0 );
         subGrd_2.setEnabled( 0 );
      }
   }

   public void load2B227( )
   {
      /* Using cursor T002B20 */
      pr_default.execute(18, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
      if ( (pr_default.getStatus(18) != 101) )
      {
         RcdFound227 = (short)(1) ;
         A1131AirLI = T002B20_A1131AirLI[0] ;
         A1132AirLi = T002B20_A1132AirLi[0] ;
         zm2B227( -23) ;
      }
      pr_default.close(18);
      onLoadActions2B227( ) ;
   }

   public void onLoadActions2B227( )
   {
   }

   public void checkExtendedTable2B227( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal2B227( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2B227( )
   {
   }

   public void enableDisable2B227( )
   {
   }

   public void getKey2B227( )
   {
      /* Using cursor T002B21 */
      pr_default.execute(19, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
      if ( (pr_default.getStatus(19) != 101) )
      {
         RcdFound227 = (short)(1) ;
      }
      else
      {
         RcdFound227 = (short)(0) ;
      }
      pr_default.close(19);
   }

   public void getByPrimaryKey2B227( )
   {
      /* Using cursor T002B8 */
      pr_default.execute(6, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
      if ( (pr_default.getStatus(6) != 101) )
      {
         zm2B227( 23) ;
         RcdFound227 = (short)(1) ;
         initializeNonKey2B227( ) ;
         A1137AirLi = T002B8_A1137AirLi[0] ;
         A1131AirLI = T002B8_A1131AirLI[0] ;
         A1132AirLi = T002B8_A1132AirLi[0] ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z1137AirLi = A1137AirLi ;
         sMode227 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2B227( ) ;
         Gx_mode = sMode227 ;
      }
      else
      {
         RcdFound227 = (short)(0) ;
         initializeNonKey2B227( ) ;
         sMode227 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2B227( ) ;
         Gx_mode = sMode227 ;
      }
      K1137AirLi = A1137AirLi ;
      pr_default.close(6);
   }

   public void checkOptimisticConcurrency2B227( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002B7 */
         pr_default.execute(5, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
         if ( ! (pr_default.getStatus(5) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"AIRLINESBANK"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(5) == 101) || ( GXutil.strcmp(Z1132AirLi, T002B7_A1132AirLi[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"AIRLINESBANK"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2B227( )
   {
      beforeValidate2B227( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B227( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2B227( 0) ;
         checkOptimisticConcurrency2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B227( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2B227( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B22 */
                  pr_default.execute(20, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi, A1131AirLI, A1132AirLi});
                  if ( (pr_default.getStatus(20) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2B227( ) ;
         }
         endLevel2B227( ) ;
      }
      closeExtendedTableCursors2B227( ) ;
   }

   public void update2B227( )
   {
      beforeValidate2B227( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B227( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B227( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2B227( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B23 */
                  pr_default.execute(21, new Object[] {A1131AirLI, A1132AirLi, A23ISOCod, A1136AirLi, A1137AirLi});
                  deferredUpdate2B227( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey2B227( ) ;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2B227( ) ;
      }
      closeExtendedTableCursors2B227( ) ;
   }

   public void deferredUpdate2B227( )
   {
   }

   public void delete2B227( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2B227( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B227( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2B227( ) ;
         /* No cascading delete specified. */
         afterConfirm2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2B227( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T002B24 */
               pr_default.execute(22, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode227 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2B227( ) ;
      Gx_mode = sMode227 ;
   }

   public void onDeleteControls2B227( )
   {
      standaloneModal2B227( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel2B227( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(5);
      }
   }

   public void scanStart2B227( )
   {
      /* Using cursor T002B25 */
      pr_default.execute(23, new Object[] {A23ISOCod, A1136AirLi});
      RcdFound227 = (short)(0) ;
      if ( (pr_default.getStatus(23) != 101) )
      {
         RcdFound227 = (short)(1) ;
         A1137AirLi = T002B25_A1137AirLi[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2B227( )
   {
      pr_default.readNext(23);
      RcdFound227 = (short)(0) ;
      if ( (pr_default.getStatus(23) != 101) )
      {
         RcdFound227 = (short)(1) ;
         A1137AirLi = T002B25_A1137AirLi[0] ;
      }
   }

   public void scanEnd2B227( )
   {
      pr_default.close(23);
   }

   public void afterConfirm2B227( )
   {
      /* After Confirm Rules */
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  && true /* Level */ && ( AV26AlterB == 0 ) )
      {
         pushError( "User cannot delete data bank" );
         AnyError = (short)(1) ;
         keepFocus();
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1137AirLi))==0)) )
      {
         pushError( "Currency cannot be empty" );
         AnyError = (short)(1) ;
         keepFocus();
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1131AirLI))==0)) )
      {
         pushError( "Bank Data cannot be empty" );
         AnyError = (short)(1) ;
         keepFocus();
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1132AirLi))==0)) )
      {
         pushError( "Transf. Currency cannot be empty" );
         AnyError = (short)(1) ;
         keepFocus();
         return  ;
      }
   }

   public void beforeInsert2B227( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2B227( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2B227( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2B227( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2B227( )
   {
      /* Before Validate Rules */
   }

   public void addRow2B227( )
   {
      loadToBuffer43();
   }

   public void readRow2B227( )
   {
      subtairlines43 = ( subtairlines43 ) subGrd_1.getElementAt(nGXsfl_43_idx) ;
      subfileToVariables43 ();
      nGXsfl_43_idx = (short)(nGXsfl_43_idx+1) ;
   }

   public void zm2B228( int GX_JID )
   {
      if ( ( GX_JID == 24 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z1133AirLi = T002B6_A1133AirLi[0] ;
            Z1134AirLi = T002B6_A1134AirLi[0] ;
            Z1135AirLi = T002B6_A1135AirLi[0] ;
         }
         else
         {
            Z1133AirLi = A1133AirLi ;
            Z1134AirLi = A1134AirLi ;
            Z1135AirLi = A1135AirLi ;
         }
      }
      if ( ( GX_JID == -24 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z1138AirLi = A1138AirLi ;
         Z1133AirLi = A1133AirLi ;
         Z1134AirLi = A1134AirLi ;
         Z1135AirLi = A1135AirLi ;
         Z365Contac = A365Contac ;
      }
   }

   public void standaloneNotModal2B228( )
   {
   }

   public void standaloneModal2B228( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         subGrd_1.setEnabled( 0 );
         subGrd_2.setEnabled( 0 );
      }
   }

   public void load2B228( )
   {
      /* Using cursor T002B26 */
      pr_default.execute(24, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
      if ( (pr_default.getStatus(24) != 101) )
      {
         RcdFound228 = (short)(1) ;
         A1133AirLi = T002B26_A1133AirLi[0] ;
         A1134AirLi = T002B26_A1134AirLi[0] ;
         A1135AirLi = T002B26_A1135AirLi[0] ;
         A366Contac = T002B26_A366Contac[0] ;
         zm2B228( -24) ;
      }
      pr_default.close(24);
      onLoadActions2B228( ) ;
   }

   public void onLoadActions2B228( )
   {
   }

   public void checkExtendedTable2B228( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal2B228( ) ;
      Gx_BScreen = (byte)(0) ;
      /* Using cursor T002B4 */
      pr_default.execute(2, new Object[] {A365Contac});
      if ( (pr_default.getStatus(2) == 101) )
      {
         pushError( "No matching 'Contact Types'." );
         AnyError = (short)(1) ;
         setNextFocus( new GXSubfileCell(subGrd_2, nGXsfl_47_idx - 1, 0) );
      }
      A366Contac = T002B4_A366Contac[0] ;
      pr_default.close(2);
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2B228( )
   {
      pr_default.close(2);
   }

   public void enableDisable2B228( )
   {
   }

   public void getKey2B228( )
   {
      /* Using cursor T002B27 */
      pr_default.execute(25, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
      if ( (pr_default.getStatus(25) != 101) )
      {
         RcdFound228 = (short)(1) ;
      }
      else
      {
         RcdFound228 = (short)(0) ;
      }
      pr_default.close(25);
   }

   public void getByPrimaryKey2B228( )
   {
      /* Using cursor T002B6 */
      pr_default.execute(4, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
      if ( (pr_default.getStatus(4) != 101) )
      {
         zm2B228( 24) ;
         RcdFound228 = (short)(1) ;
         initializeNonKey2B228( ) ;
         A1138AirLi = T002B6_A1138AirLi[0] ;
         A1133AirLi = T002B6_A1133AirLi[0] ;
         A1134AirLi = T002B6_A1134AirLi[0] ;
         A1135AirLi = T002B6_A1135AirLi[0] ;
         A365Contac = T002B6_A365Contac[0] ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z365Contac = A365Contac ;
         Z1138AirLi = A1138AirLi ;
         sMode228 = Gx_mode ;
         Gx_mode = "DSP" ;
         load2B228( ) ;
         Gx_mode = sMode228 ;
      }
      else
      {
         RcdFound228 = (short)(0) ;
         initializeNonKey2B228( ) ;
         sMode228 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2B228( ) ;
         Gx_mode = sMode228 ;
      }
      K365Contac = A365Contac ;
      K1138AirLi = A1138AirLi ;
      pr_default.close(4);
   }

   public void checkOptimisticConcurrency2B228( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T002B5 */
         pr_default.execute(3, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
         if ( ! (pr_default.getStatus(3) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"AIRLINESCONTACTS"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(3) == 101) || ( GXutil.strcmp(Z1133AirLi, T002B5_A1133AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1134AirLi, T002B5_A1134AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1135AirLi, T002B5_A1135AirLi[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"AIRLINESCONTACTS"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert2B228( )
   {
      beforeValidate2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B228( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2B228( 0) ;
         checkOptimisticConcurrency2B228( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2B228( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B28 */
                  pr_default.execute(26, new Object[] {A23ISOCod, A1136AirLi, new Short(A1138AirLi), A1133AirLi, A1134AirLi, A1135AirLi, A365Contac});
                  if ( (pr_default.getStatus(26) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load2B228( ) ;
         }
         endLevel2B228( ) ;
      }
      closeExtendedTableCursors2B228( ) ;
   }

   public void update2B228( )
   {
      beforeValidate2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B228( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B228( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2B228( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T002B29 */
                  pr_default.execute(27, new Object[] {A1133AirLi, A1134AirLi, A1135AirLi, A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
                  deferredUpdate2B228( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey2B228( ) ;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel2B228( ) ;
      }
      closeExtendedTableCursors2B228( ) ;
   }

   public void deferredUpdate2B228( )
   {
   }

   public void delete2B228( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B228( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2B228( ) ;
         /* No cascading delete specified. */
         afterConfirm2B228( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T002B30 */
               pr_default.execute(28, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode228 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2B228( ) ;
      Gx_mode = sMode228 ;
   }

   public void onDeleteControls2B228( )
   {
      standaloneModal2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor T002B31 */
         pr_default.execute(29, new Object[] {A365Contac});
         A366Contac = T002B31_A366Contac[0] ;
         pr_default.close(29);
      }
   }

   public void endLevel2B228( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(3);
      }
   }

   public void scanStart2B228( )
   {
      /* Using cursor T002B32 */
      pr_default.execute(30, new Object[] {A23ISOCod, A1136AirLi});
      RcdFound228 = (short)(0) ;
      if ( (pr_default.getStatus(30) != 101) )
      {
         RcdFound228 = (short)(1) ;
         A365Contac = T002B32_A365Contac[0] ;
         A1138AirLi = T002B32_A1138AirLi[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2B228( )
   {
      pr_default.readNext(30);
      RcdFound228 = (short)(0) ;
      if ( (pr_default.getStatus(30) != 101) )
      {
         RcdFound228 = (short)(1) ;
         A365Contac = T002B32_A365Contac[0] ;
         A1138AirLi = T002B32_A1138AirLi[0] ;
      }
   }

   public void scanEnd2B228( )
   {
      pr_default.close(30);
   }

   public void afterConfirm2B228( )
   {
      /* After Confirm Rules */
      if ( true /* Level */ && ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         GXt_int2 = A1138AirLi ;
         GXv_int3[0] = GXt_int2 ;
         new pairlinecttseq(remoteHandle, context).execute( A23ISOCod, A1136AirLi, A365Contac, GXv_int3) ;
         tairlines.this.GXt_int2 = GXv_int3[0] ;
         A1138AirLi = GXt_int2 ;
      }
   }

   public void beforeInsert2B228( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2B228( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2B228( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2B228( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2B228( )
   {
      /* Before Validate Rules */
   }

   public void addRow2B228( )
   {
      loadToBuffer47();
   }

   public void readRow2B228( )
   {
      subtairlines47 = ( subtairlines47 ) subGrd_2.getElementAt(nGXsfl_47_idx) ;
      subfileToVariables47 ();
      nGXsfl_47_idx = (short)(nGXsfl_47_idx+1) ;
   }

   public void confirm_2B0( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2B226( ) ;
         }
         else
         {
            checkExtendedTable2B226( ) ;
            closeExtendedTableCursors2B226( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         /* Save parent mode. */
         sMode226 = Gx_mode ;
         confirm_2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            confirm_2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Restore parent mode. */
               Gx_mode = sMode226 ;
               PreviousBitmap = bttBtn_exit1.getBitmap() ;
               PreviousTooltip = bttBtn_exit1.getTooltip() ;
               IsConfirmed = (short)(1) ;
            }
         }
         /* Restore parent mode. */
         Gx_mode = sMode226 ;
      }
   }

   public void confirm_2B228( )
   {
      nGXsfl_47_idx = (short)(0) ;
      while ( ( nGXsfl_47_idx < subGrd_2.getItemCount() ) )
      {
         readRow2B228( ) ;
         if ( ( subtairlines47.isLoaded() != 0 ) || ( subtairlines47.isChanged() != 0 ) )
         {
            getKey2B228( ) ;
            if ( ( subtairlines47.isLoaded() == 0 ) && ( subtairlines47.isDeleted() == 0 ) )
            {
               if ( ( RcdFound228 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate2B228( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable2B228( ) ;
                     closeExtendedTableCursors2B228( ) ;
                     if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
                     {
                        PreviousBitmap = bttBtn_exit1.getBitmap() ;
                        PreviousTooltip = bttBtn_exit1.getTooltip() ;
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("noupdate") );
                  AnyError = (short)(1) ;
                  setNextFocus( dynISOCod );
               }
            }
            else
            {
               if ( ( RcdFound228 != 0 ) )
               {
                  if ( ( subtairlines47.isDeleted() != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey2B228( ) ;
                     load2B228( ) ;
                     beforeValidate2B228( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls2B228( ) ;
                     }
                  }
                  else
                  {
                     if ( ( subtairlines47.isChanged() != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate2B228( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable2B228( ) ;
                           closeExtendedTableCursors2B228( ) ;
                           if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
                           {
                              PreviousBitmap = bttBtn_exit1.getBitmap() ;
                              PreviousTooltip = bttBtn_exit1.getTooltip() ;
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( subtairlines47.isDeleted() == 0 ) )
                  {
                     pushError( localUtil.getMessages().getMessage("recdeleted") );
                     AnyError = (short)(1) ;
                     setNextFocus( dynISOCod );
                  }
               }
            }
         }
      }
      subGrd_2.endLoad();
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void confirm_2B227( )
   {
      nGXsfl_43_idx = (short)(0) ;
      while ( ( nGXsfl_43_idx < subGrd_1.getItemCount() ) )
      {
         readRow2B227( ) ;
         if ( ( subtairlines43.isLoaded() != 0 ) || ( subtairlines43.isChanged() != 0 ) )
         {
            getKey2B227( ) ;
            if ( ( subtairlines43.isLoaded() == 0 ) && ( subtairlines43.isDeleted() == 0 ) )
            {
               if ( ( RcdFound227 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate2B227( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable2B227( ) ;
                     closeExtendedTableCursors2B227( ) ;
                     if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
                     {
                        PreviousBitmap = bttBtn_exit1.getBitmap() ;
                        PreviousTooltip = bttBtn_exit1.getTooltip() ;
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("noupdate") );
                  AnyError = (short)(1) ;
                  setNextFocus( dynISOCod );
               }
            }
            else
            {
               if ( ( RcdFound227 != 0 ) )
               {
                  if ( ( subtairlines43.isDeleted() != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey2B227( ) ;
                     load2B227( ) ;
                     beforeValidate2B227( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls2B227( ) ;
                     }
                  }
                  else
                  {
                     if ( ( subtairlines43.isChanged() != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate2B227( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable2B227( ) ;
                           closeExtendedTableCursors2B227( ) ;
                           if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
                           {
                              PreviousBitmap = bttBtn_exit1.getBitmap() ;
                              PreviousTooltip = bttBtn_exit1.getTooltip() ;
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( subtairlines43.isDeleted() == 0 ) )
                  {
                     pushError( localUtil.getMessages().getMessage("recdeleted") );
                     AnyError = (short)(1) ;
                     setNextFocus( dynISOCod );
                  }
               }
            }
         }
      }
      subGrd_1.endLoad();
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = tairlines.this.AV11ISOCod;
      this.aP1[0] = tairlines.this.AV13AirLin;
      this.aP2[0] = tairlines.this.Gx_mode;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(29);
      pr_default.close(16);
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A1126AirLi = GXutil.resetTime( GXutil.nullDate() );
      A1130Airli = "" ;
      n1130Airli = false ;
      A1118AirLi = "" ;
      A1119AirLi = "" ;
      A1120Airli = "" ;
      A1121AirLi = "" ;
      A1122AirLi = "" ;
      A1123AirLi = "" ;
      A1124AirLi = "" ;
      A1127AirLi = "" ;
      n1127AirLi = false ;
      A1128AirLi = "" ;
      A1129AirLi = "" ;
      n1129AirLi = false ;
      A1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      A23ISOCod = "" ;
      A20ISODes = "" ;
      n20ISODes = false ;
      A1136AirLi = "" ;
      K23ISOCod = "" ;
      K1136AirLi = "" ;
      i1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      i1128AirLi = "" ;
      AV26AlterB = (byte)(0) ;
      A1131AirLI = "" ;
      A1132AirLi = "" ;
      A1137AirLi = "" ;
      K1137AirLi = "" ;
      A1133AirLi = "" ;
      A1134AirLi = "" ;
      A1135AirLi = "" ;
      A365Contac = "" ;
      A366Contac = "" ;
      A1138AirLi = (short)(0) ;
      K365Contac = "" ;
      K1138AirLi = (short)(0) ;
      scmdbuf = "" ;
      T002B2_A23ISOCod = new String[] {""} ;
      subtairlines43 = new subtairlines43();
      sMode227 = "" ;
      subtairlines47 = new subtairlines47();
      sMode228 = "" ;
      lastAnyError = 0 ;
      Z1132AirLi = "" ;
      Z1133AirLi = "" ;
      Z1134AirLi = "" ;
      Z1135AirLi = "" ;
      GXv_svchar1 = new String [1] ;
      T002B3_A20ISODes = new String[] {""} ;
      T002B3_n20ISODes = new boolean[] {false} ;
      T002B4_A366Contac = new String[] {""} ;
      returnInSub = false ;
      sMode226 = "" ;
      RcdFound226 = (short)(0) ;
      Z1126AirLi = GXutil.resetTime( GXutil.nullDate() );
      Z1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      Z1130Airli = "" ;
      Z1118AirLi = "" ;
      Z1119AirLi = "" ;
      Z1120Airli = "" ;
      Z1121AirLi = "" ;
      Z1122AirLi = "" ;
      Z1123AirLi = "" ;
      Z1124AirLi = "" ;
      Z1127AirLi = "" ;
      Z1128AirLi = "" ;
      Z1129AirLi = "" ;
      GX_JID = 0 ;
      Z1136AirLi = "" ;
      Z23ISOCod = "" ;
      Gx_BScreen = (byte)(0) ;
      T002B11_A1136AirLi = new String[] {""} ;
      T002B11_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      T002B11_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      T002B11_A1130Airli = new String[] {""} ;
      T002B11_n1130Airli = new boolean[] {false} ;
      T002B11_A1118AirLi = new String[] {""} ;
      T002B11_A20ISODes = new String[] {""} ;
      T002B11_n20ISODes = new boolean[] {false} ;
      T002B11_A1119AirLi = new String[] {""} ;
      T002B11_A1120Airli = new String[] {""} ;
      T002B11_A1121AirLi = new String[] {""} ;
      T002B11_A1122AirLi = new String[] {""} ;
      T002B11_A1123AirLi = new String[] {""} ;
      T002B11_A1124AirLi = new String[] {""} ;
      T002B11_A1127AirLi = new String[] {""} ;
      T002B11_n1127AirLi = new boolean[] {false} ;
      T002B11_A1128AirLi = new String[] {""} ;
      T002B11_A1129AirLi = new String[] {""} ;
      T002B11_n1129AirLi = new boolean[] {false} ;
      T002B11_A23ISOCod = new String[] {""} ;
      T002B12_A23ISOCod = new String[] {""} ;
      T002B12_A1136AirLi = new String[] {""} ;
      T002B10_A1136AirLi = new String[] {""} ;
      T002B10_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      T002B10_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      T002B10_A1130Airli = new String[] {""} ;
      T002B10_n1130Airli = new boolean[] {false} ;
      T002B10_A1118AirLi = new String[] {""} ;
      T002B10_A1119AirLi = new String[] {""} ;
      T002B10_A1120Airli = new String[] {""} ;
      T002B10_A1121AirLi = new String[] {""} ;
      T002B10_A1122AirLi = new String[] {""} ;
      T002B10_A1123AirLi = new String[] {""} ;
      T002B10_A1124AirLi = new String[] {""} ;
      T002B10_A1127AirLi = new String[] {""} ;
      T002B10_n1127AirLi = new boolean[] {false} ;
      T002B10_A1128AirLi = new String[] {""} ;
      T002B10_A1129AirLi = new String[] {""} ;
      T002B10_n1129AirLi = new boolean[] {false} ;
      T002B10_A23ISOCod = new String[] {""} ;
      RcdFound227 = (short)(0) ;
      RcdFound228 = (short)(0) ;
      T002B13_A23ISOCod = new String[] {""} ;
      T002B13_A1136AirLi = new String[] {""} ;
      T002B14_A23ISOCod = new String[] {""} ;
      T002B14_A1136AirLi = new String[] {""} ;
      T002B9_A1136AirLi = new String[] {""} ;
      T002B9_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      T002B9_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      T002B9_A1130Airli = new String[] {""} ;
      T002B9_n1130Airli = new boolean[] {false} ;
      T002B9_A1118AirLi = new String[] {""} ;
      T002B9_A1119AirLi = new String[] {""} ;
      T002B9_A1120Airli = new String[] {""} ;
      T002B9_A1121AirLi = new String[] {""} ;
      T002B9_A1122AirLi = new String[] {""} ;
      T002B9_A1123AirLi = new String[] {""} ;
      T002B9_A1124AirLi = new String[] {""} ;
      T002B9_A1127AirLi = new String[] {""} ;
      T002B9_n1127AirLi = new boolean[] {false} ;
      T002B9_A1128AirLi = new String[] {""} ;
      T002B9_A1129AirLi = new String[] {""} ;
      T002B9_n1129AirLi = new boolean[] {false} ;
      T002B9_A23ISOCod = new String[] {""} ;
      Gx_longc = false ;
      T002B18_A20ISODes = new String[] {""} ;
      T002B18_n20ISODes = new boolean[] {false} ;
      T002B8_A23ISOCod = new String[] {""} ;
      T002B8_A1136AirLi = new String[] {""} ;
      T002B8_A1137AirLi = new String[] {""} ;
      T002B8_A1131AirLI = new String[] {""} ;
      T002B8_A1132AirLi = new String[] {""} ;
      T002B19_A23ISOCod = new String[] {""} ;
      T002B19_A1136AirLi = new String[] {""} ;
      Z1137AirLi = "" ;
      Z1131AirLI = "" ;
      T002B20_A23ISOCod = new String[] {""} ;
      T002B20_A1136AirLi = new String[] {""} ;
      T002B20_A1137AirLi = new String[] {""} ;
      T002B20_A1131AirLI = new String[] {""} ;
      T002B20_A1132AirLi = new String[] {""} ;
      T002B21_A23ISOCod = new String[] {""} ;
      T002B21_A1136AirLi = new String[] {""} ;
      T002B21_A1137AirLi = new String[] {""} ;
      T002B7_A23ISOCod = new String[] {""} ;
      T002B7_A1136AirLi = new String[] {""} ;
      T002B7_A1137AirLi = new String[] {""} ;
      T002B7_A1131AirLI = new String[] {""} ;
      T002B7_A1132AirLi = new String[] {""} ;
      T002B25_A23ISOCod = new String[] {""} ;
      T002B25_A1136AirLi = new String[] {""} ;
      T002B25_A1137AirLi = new String[] {""} ;
      Z1138AirLi = (short)(0) ;
      Z365Contac = "" ;
      T002B26_A23ISOCod = new String[] {""} ;
      T002B26_A1136AirLi = new String[] {""} ;
      T002B26_A1138AirLi = new short[1] ;
      T002B26_A1133AirLi = new String[] {""} ;
      T002B26_A1134AirLi = new String[] {""} ;
      T002B26_A1135AirLi = new String[] {""} ;
      T002B26_A366Contac = new String[] {""} ;
      T002B26_A365Contac = new String[] {""} ;
      T002B27_A23ISOCod = new String[] {""} ;
      T002B27_A1136AirLi = new String[] {""} ;
      T002B27_A365Contac = new String[] {""} ;
      T002B27_A1138AirLi = new short[1] ;
      T002B6_A23ISOCod = new String[] {""} ;
      T002B6_A1136AirLi = new String[] {""} ;
      T002B6_A1138AirLi = new short[1] ;
      T002B6_A1133AirLi = new String[] {""} ;
      T002B6_A1134AirLi = new String[] {""} ;
      T002B6_A1135AirLi = new String[] {""} ;
      T002B6_A365Contac = new String[] {""} ;
      T002B5_A23ISOCod = new String[] {""} ;
      T002B5_A1136AirLi = new String[] {""} ;
      T002B5_A1138AirLi = new short[1] ;
      T002B5_A1133AirLi = new String[] {""} ;
      T002B5_A1134AirLi = new String[] {""} ;
      T002B5_A1135AirLi = new String[] {""} ;
      T002B5_A365Contac = new String[] {""} ;
      T002B31_A366Contac = new String[] {""} ;
      T002B32_A23ISOCod = new String[] {""} ;
      T002B32_A1136AirLi = new String[] {""} ;
      T002B32_A365Contac = new String[] {""} ;
      T002B32_A1138AirLi = new short[1] ;
      GXt_int2 = (short)(0) ;
      GXv_int3 = new short [1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tairlines__default(),
         new Object[] {
             new Object[] {
            T002B2_A23ISOCod
            }
            , new Object[] {
            T002B3_A20ISODes, T002B3_n20ISODes
            }
            , new Object[] {
            T002B4_A366Contac
            }
            , new Object[] {
            T002B5_A23ISOCod, T002B5_A1136AirLi, T002B5_A1138AirLi, T002B5_A1133AirLi, T002B5_A1134AirLi, T002B5_A1135AirLi, T002B5_A365Contac
            }
            , new Object[] {
            T002B6_A23ISOCod, T002B6_A1136AirLi, T002B6_A1138AirLi, T002B6_A1133AirLi, T002B6_A1134AirLi, T002B6_A1135AirLi, T002B6_A365Contac
            }
            , new Object[] {
            T002B7_A23ISOCod, T002B7_A1136AirLi, T002B7_A1137AirLi, T002B7_A1131AirLI, T002B7_A1132AirLi
            }
            , new Object[] {
            T002B8_A23ISOCod, T002B8_A1136AirLi, T002B8_A1137AirLi, T002B8_A1131AirLI, T002B8_A1132AirLi
            }
            , new Object[] {
            T002B9_A1136AirLi, T002B9_A1126AirLi, T002B9_A1125AirLi, T002B9_A1130Airli, T002B9_n1130Airli, T002B9_A1118AirLi, T002B9_A1119AirLi, T002B9_A1120Airli, T002B9_A1121AirLi, T002B9_A1122AirLi,
            T002B9_A1123AirLi, T002B9_A1124AirLi, T002B9_A1127AirLi, T002B9_n1127AirLi, T002B9_A1128AirLi, T002B9_A1129AirLi, T002B9_n1129AirLi, T002B9_A23ISOCod
            }
            , new Object[] {
            T002B10_A1136AirLi, T002B10_A1126AirLi, T002B10_A1125AirLi, T002B10_A1130Airli, T002B10_n1130Airli, T002B10_A1118AirLi, T002B10_A1119AirLi, T002B10_A1120Airli, T002B10_A1121AirLi, T002B10_A1122AirLi,
            T002B10_A1123AirLi, T002B10_A1124AirLi, T002B10_A1127AirLi, T002B10_n1127AirLi, T002B10_A1128AirLi, T002B10_A1129AirLi, T002B10_n1129AirLi, T002B10_A23ISOCod
            }
            , new Object[] {
            T002B11_A1136AirLi, T002B11_A1126AirLi, T002B11_A1125AirLi, T002B11_A1130Airli, T002B11_n1130Airli, T002B11_A1118AirLi, T002B11_A20ISODes, T002B11_n20ISODes, T002B11_A1119AirLi, T002B11_A1120Airli,
            T002B11_A1121AirLi, T002B11_A1122AirLi, T002B11_A1123AirLi, T002B11_A1124AirLi, T002B11_A1127AirLi, T002B11_n1127AirLi, T002B11_A1128AirLi, T002B11_A1129AirLi, T002B11_n1129AirLi, T002B11_A23ISOCod
            }
            , new Object[] {
            T002B12_A23ISOCod, T002B12_A1136AirLi
            }
            , new Object[] {
            T002B13_A23ISOCod, T002B13_A1136AirLi
            }
            , new Object[] {
            T002B14_A23ISOCod, T002B14_A1136AirLi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002B18_A20ISODes, T002B18_n20ISODes
            }
            , new Object[] {
            T002B19_A23ISOCod, T002B19_A1136AirLi
            }
            , new Object[] {
            T002B20_A23ISOCod, T002B20_A1136AirLi, T002B20_A1137AirLi, T002B20_A1131AirLI, T002B20_A1132AirLi
            }
            , new Object[] {
            T002B21_A23ISOCod, T002B21_A1136AirLi, T002B21_A1137AirLi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002B25_A23ISOCod, T002B25_A1136AirLi, T002B25_A1137AirLi
            }
            , new Object[] {
            T002B26_A23ISOCod, T002B26_A1136AirLi, T002B26_A1138AirLi, T002B26_A1133AirLi, T002B26_A1134AirLi, T002B26_A1135AirLi, T002B26_A366Contac, T002B26_A365Contac
            }
            , new Object[] {
            T002B27_A23ISOCod, T002B27_A1136AirLi, T002B27_A365Contac, T002B27_A1138AirLi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002B31_A366Contac
            }
            , new Object[] {
            T002B32_A23ISOCod, T002B32_A1136AirLi, T002B32_A365Contac, T002B32_A1138AirLi
            }
         }
      );
      reloadDynamicLists(0);
      A1128AirLi = "S" ;
      cmbAirLineTypeSettlement.setValue(A1128AirLi);
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte AV26AlterB ;
   protected byte geteqAfterKey227= 1 ;
   protected byte geteqAfterKey228= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short A1138AirLi ;
   protected short K1138AirLi ;
   protected short RcdFound226 ;
   protected short RcdFound227 ;
   protected short RcdFound228 ;
   protected short nGXsfl_43_idx=1 ;
   protected short nGXsfl_47_idx=1 ;
   protected short Z1138AirLi ;
   protected short GXt_int2 ;
   protected short GXv_int3[] ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String A1130Airli ;
   protected String A1128AirLi ;
   protected String A1129AirLi ;
   protected String i1128AirLi ;
   protected String scmdbuf ;
   protected String sMode227 ;
   protected String sMode228 ;
   protected String sMode226 ;
   protected String Z1130Airli ;
   protected String Z1128AirLi ;
   protected String Z1129AirLi ;
   protected java.util.Date A1126AirLi ;
   protected java.util.Date A1125AirLi ;
   protected java.util.Date i1125AirLi ;
   protected java.util.Date Z1126AirLi ;
   protected java.util.Date Z1125AirLi ;
   protected boolean n1130Airli ;
   protected boolean n1127AirLi ;
   protected boolean n1129AirLi ;
   protected boolean n20ISODes ;
   protected boolean returnInSub ;
   protected boolean Gx_longc ;
   protected String A1131AirLI ;
   protected String Z1131AirLI ;
   protected String A1118AirLi ;
   protected String A1119AirLi ;
   protected String A1120Airli ;
   protected String A1121AirLi ;
   protected String A1122AirLi ;
   protected String A1123AirLi ;
   protected String A1124AirLi ;
   protected String A1127AirLi ;
   protected String A23ISOCod ;
   protected String A20ISODes ;
   protected String A1136AirLi ;
   protected String K23ISOCod ;
   protected String K1136AirLi ;
   protected String A1132AirLi ;
   protected String A1137AirLi ;
   protected String K1137AirLi ;
   protected String A1133AirLi ;
   protected String A1134AirLi ;
   protected String A1135AirLi ;
   protected String A365Contac ;
   protected String A366Contac ;
   protected String K365Contac ;
   protected String AV11ISOCod ;
   protected String AV13AirLin ;
   protected String Z1132AirLi ;
   protected String Z1133AirLi ;
   protected String Z1134AirLi ;
   protected String Z1135AirLi ;
   protected String GXv_svchar1[] ;
   protected String Z1118AirLi ;
   protected String Z1119AirLi ;
   protected String Z1120Airli ;
   protected String Z1121AirLi ;
   protected String Z1122AirLi ;
   protected String Z1123AirLi ;
   protected String Z1124AirLi ;
   protected String Z1127AirLi ;
   protected String Z1136AirLi ;
   protected String Z23ISOCod ;
   protected String Z1137AirLi ;
   protected String Z365Contac ;
   protected String[] aP0 ;
   protected String[] aP1 ;
   protected String[] aP2 ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString dynISOCod ;
   protected GUIObjectString edtAirLineCode ;
   protected GUIObjectString edtAirLineDescription ;
   protected GUIObjectString edtISODes ;
   protected GUIObjectString edtAirLineAbbreviation ;
   protected GUIObjectString edtAirlineAddress ;
   protected GUIObjectString edtAirLineAddressCompl ;
   protected GUIObjectString edtAirLineCity ;
   protected GUIObjectString edtAirLineState ;
   protected GUIObjectString edtAirLineZIP ;
   protected GUIObjectDate edtAirLineDatIns ;
   protected GUIObjectDate edtAirLineDatMod ;
   protected GUIObjectString edtAirLineCompleteAddress ;
   protected GUIObjectString cmbAirLineTypeSettlement ;
   protected GUIObjectString edtAirLineSta ;
   protected GUIObjectString edtAirlineBankTransferMode ;
   protected GXSubfileTRN subGrd_1 ;
   protected GXSubfileTRN subGrd_2 ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected ILabel lbllbl16 ;
   protected ILabel lbllbl18 ;
   protected ILabel lbllbl20 ;
   protected ILabel lbllbl22 ;
   protected ILabel lbllbl24 ;
   protected ILabel lbllbl26 ;
   protected ILabel lbllbl28 ;
   protected ILabel lbllbl30 ;
   protected ILabel lbllbl32 ;
   protected ILabel lbllbl34 ;
   protected ILabel lbllbl36 ;
   protected ILabel lbllbl38 ;
   protected ILabel lbllbl40 ;
   protected ILabel lbllbl42 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T002B2_A23ISOCod ;
   protected subtairlines43 subtairlines43 ;
   protected subtairlines47 subtairlines47 ;
   protected String[] T002B3_A20ISODes ;
   protected boolean[] T002B3_n20ISODes ;
   protected String[] T002B4_A366Contac ;
   protected String[] T002B11_A1136AirLi ;
   protected java.util.Date[] T002B11_A1126AirLi ;
   protected java.util.Date[] T002B11_A1125AirLi ;
   protected String[] T002B11_A1130Airli ;
   protected boolean[] T002B11_n1130Airli ;
   protected String[] T002B11_A1118AirLi ;
   protected String[] T002B11_A20ISODes ;
   protected boolean[] T002B11_n20ISODes ;
   protected String[] T002B11_A1119AirLi ;
   protected String[] T002B11_A1120Airli ;
   protected String[] T002B11_A1121AirLi ;
   protected String[] T002B11_A1122AirLi ;
   protected String[] T002B11_A1123AirLi ;
   protected String[] T002B11_A1124AirLi ;
   protected String[] T002B11_A1127AirLi ;
   protected boolean[] T002B11_n1127AirLi ;
   protected String[] T002B11_A1128AirLi ;
   protected String[] T002B11_A1129AirLi ;
   protected boolean[] T002B11_n1129AirLi ;
   protected String[] T002B11_A23ISOCod ;
   protected String[] T002B12_A23ISOCod ;
   protected String[] T002B12_A1136AirLi ;
   protected String[] T002B10_A1136AirLi ;
   protected java.util.Date[] T002B10_A1126AirLi ;
   protected java.util.Date[] T002B10_A1125AirLi ;
   protected String[] T002B10_A1130Airli ;
   protected boolean[] T002B10_n1130Airli ;
   protected String[] T002B10_A1118AirLi ;
   protected String[] T002B10_A1119AirLi ;
   protected String[] T002B10_A1120Airli ;
   protected String[] T002B10_A1121AirLi ;
   protected String[] T002B10_A1122AirLi ;
   protected String[] T002B10_A1123AirLi ;
   protected String[] T002B10_A1124AirLi ;
   protected String[] T002B10_A1127AirLi ;
   protected boolean[] T002B10_n1127AirLi ;
   protected String[] T002B10_A1128AirLi ;
   protected String[] T002B10_A1129AirLi ;
   protected boolean[] T002B10_n1129AirLi ;
   protected String[] T002B10_A23ISOCod ;
   protected String[] T002B13_A23ISOCod ;
   protected String[] T002B13_A1136AirLi ;
   protected String[] T002B14_A23ISOCod ;
   protected String[] T002B14_A1136AirLi ;
   protected String[] T002B9_A1136AirLi ;
   protected java.util.Date[] T002B9_A1126AirLi ;
   protected java.util.Date[] T002B9_A1125AirLi ;
   protected String[] T002B9_A1130Airli ;
   protected boolean[] T002B9_n1130Airli ;
   protected String[] T002B9_A1118AirLi ;
   protected String[] T002B9_A1119AirLi ;
   protected String[] T002B9_A1120Airli ;
   protected String[] T002B9_A1121AirLi ;
   protected String[] T002B9_A1122AirLi ;
   protected String[] T002B9_A1123AirLi ;
   protected String[] T002B9_A1124AirLi ;
   protected String[] T002B9_A1127AirLi ;
   protected boolean[] T002B9_n1127AirLi ;
   protected String[] T002B9_A1128AirLi ;
   protected String[] T002B9_A1129AirLi ;
   protected boolean[] T002B9_n1129AirLi ;
   protected String[] T002B9_A23ISOCod ;
   protected String[] T002B18_A20ISODes ;
   protected boolean[] T002B18_n20ISODes ;
   protected String[] T002B8_A23ISOCod ;
   protected String[] T002B8_A1136AirLi ;
   protected String[] T002B8_A1137AirLi ;
   protected String[] T002B8_A1131AirLI ;
   protected String[] T002B8_A1132AirLi ;
   protected String[] T002B19_A23ISOCod ;
   protected String[] T002B19_A1136AirLi ;
   protected String[] T002B20_A23ISOCod ;
   protected String[] T002B20_A1136AirLi ;
   protected String[] T002B20_A1137AirLi ;
   protected String[] T002B20_A1131AirLI ;
   protected String[] T002B20_A1132AirLi ;
   protected String[] T002B21_A23ISOCod ;
   protected String[] T002B21_A1136AirLi ;
   protected String[] T002B21_A1137AirLi ;
   protected String[] T002B7_A23ISOCod ;
   protected String[] T002B7_A1136AirLi ;
   protected String[] T002B7_A1137AirLi ;
   protected String[] T002B7_A1131AirLI ;
   protected String[] T002B7_A1132AirLi ;
   protected String[] T002B25_A23ISOCod ;
   protected String[] T002B25_A1136AirLi ;
   protected String[] T002B25_A1137AirLi ;
   protected String[] T002B26_A23ISOCod ;
   protected String[] T002B26_A1136AirLi ;
   protected short[] T002B26_A1138AirLi ;
   protected String[] T002B26_A1133AirLi ;
   protected String[] T002B26_A1134AirLi ;
   protected String[] T002B26_A1135AirLi ;
   protected String[] T002B26_A366Contac ;
   protected String[] T002B26_A365Contac ;
   protected String[] T002B27_A23ISOCod ;
   protected String[] T002B27_A1136AirLi ;
   protected String[] T002B27_A365Contac ;
   protected short[] T002B27_A1138AirLi ;
   protected String[] T002B6_A23ISOCod ;
   protected String[] T002B6_A1136AirLi ;
   protected short[] T002B6_A1138AirLi ;
   protected String[] T002B6_A1133AirLi ;
   protected String[] T002B6_A1134AirLi ;
   protected String[] T002B6_A1135AirLi ;
   protected String[] T002B6_A365Contac ;
   protected String[] T002B5_A23ISOCod ;
   protected String[] T002B5_A1136AirLi ;
   protected short[] T002B5_A1138AirLi ;
   protected String[] T002B5_A1133AirLi ;
   protected String[] T002B5_A1134AirLi ;
   protected String[] T002B5_A1135AirLi ;
   protected String[] T002B5_A365Contac ;
   protected String[] T002B31_A366Contac ;
   protected String[] T002B32_A23ISOCod ;
   protected String[] T002B32_A1136AirLi ;
   protected String[] T002B32_A365Contac ;
   protected short[] T002B32_A1138AirLi ;
}

final  class tairlines__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T002B2", "SELECT [ISOCod] FROM [COUNTRY] WITH (NOLOCK) ORDER BY [ISOCod] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B3", "SELECT [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B4", "SELECT [ContactTypesDescription] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B5", "SELECT [ISOCod], [AirLineCode], [AirLineCttSeq], [AirLineCttName], [AirLineCttPhone], [AirLineCttEmail], [ContactTypesCode] FROM [AIRLINESCONTACTS] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B6", "SELECT [ISOCod], [AirLineCode], [AirLineCttSeq], [AirLineCttName], [AirLineCttPhone], [AirLineCttEmail], [ContactTypesCode] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B7", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B8", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B9", "SELECT [AirLineCode], [AirLineDatMod], [AirLineDatIns], [AirlineBankTransferMode], [AirLineDescription], [AirLineAbbreviation], [AirlineAddress], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineZIP], [AirLineCompleteAddress], [AirLineTypeSettlement], [AirLineSta], [ISOCod] FROM [AIRLINES] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B10", "SELECT [AirLineCode], [AirLineDatMod], [AirLineDatIns], [AirlineBankTransferMode], [AirLineDescription], [AirLineAbbreviation], [AirlineAddress], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineZIP], [AirLineCompleteAddress], [AirLineTypeSettlement], [AirLineSta], [ISOCod] FROM [AIRLINES] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B11", "SELECT TM1.[AirLineCode], TM1.[AirLineDatMod], TM1.[AirLineDatIns], TM1.[AirlineBankTransferMode], TM1.[AirLineDescription], T2.[ISODes], TM1.[AirLineAbbreviation], TM1.[AirlineAddress], TM1.[AirLineAddressCompl], TM1.[AirLineCity], TM1.[AirLineState], TM1.[AirLineZIP], TM1.[AirLineCompleteAddress], TM1.[AirLineTypeSettlement], TM1.[AirLineSta], TM1.[ISOCod] FROM ([AIRLINES] TM1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [COUNTRY] T2 WITH (NOLOCK) ON T2.[ISOCod] = TM1.[ISOCod]) WHERE TM1.[ISOCod] = ? and TM1.[AirLineCode] = ? ORDER BY TM1.[ISOCod], TM1.[AirLineCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B12", "SELECT [ISOCod], [AirLineCode] FROM [AIRLINES] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B13", "SELECT TOP 1 [ISOCod], [AirLineCode] FROM [AIRLINES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ISOCod] > ? or [ISOCod] = ? and [AirLineCode] > ?) ORDER BY [ISOCod], [AirLineCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002B14", "SELECT TOP 1 [ISOCod], [AirLineCode] FROM [AIRLINES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ISOCod] < ? or [ISOCod] = ? and [AirLineCode] < ?) ORDER BY [ISOCod] DESC, [AirLineCode] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T002B15", "INSERT INTO [AIRLINES] ([AirLineCode], [AirLineDatMod], [AirLineDatIns], [AirlineBankTransferMode], [AirLineDescription], [AirLineAbbreviation], [AirlineAddress], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineZIP], [AirLineCompleteAddress], [AirLineTypeSettlement], [AirLineSta], [ISOCod]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002B16", "UPDATE [AIRLINES] SET [AirLineDatMod]=?, [AirLineDatIns]=?, [AirlineBankTransferMode]=?, [AirLineDescription]=?, [AirLineAbbreviation]=?, [AirlineAddress]=?, [AirLineAddressCompl]=?, [AirLineCity]=?, [AirLineState]=?, [AirLineZIP]=?, [AirLineCompleteAddress]=?, [AirLineTypeSettlement]=?, [AirLineSta]=?  WHERE [ISOCod] = ? AND [AirLineCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T002B17", "DELETE FROM [AIRLINES]  WHERE [ISOCod] = ? AND [AirLineCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T002B18", "SELECT [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B19", "SELECT [ISOCod], [AirLineCode] FROM [AIRLINES] WITH (FASTFIRSTROW NOLOCK) ORDER BY [ISOCod], [AirLineCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B20", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (NOLOCK) WHERE [ISOCod] = ? and [AirLineCode] = ? and [AirLineCurCode] = ? ORDER BY [ISOCod], [AirLineCode], [AirLineCurCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B21", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode] FROM [AIRLINESBANK] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("T002B22", "INSERT INTO [AIRLINESBANK] ([ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002B23", "UPDATE [AIRLINESBANK] SET [AirLIneDataBank]=?, [AirLineCurTrans]=?  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T002B24", "DELETE FROM [AIRLINESBANK]  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T002B25", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode] FROM [AIRLINESBANK] WITH (NOLOCK) WHERE [ISOCod] = ? and [AirLineCode] = ? ORDER BY [ISOCod], [AirLineCode], [AirLineCurCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B26", "SELECT T1.[ISOCod], T1.[AirLineCode], T1.[AirLineCttSeq], T1.[AirLineCttName], T1.[AirLineCttPhone], T1.[AirLineCttEmail], T2.[ContactTypesDescription], T1.[ContactTypesCode] FROM ([AIRLINESCONTACTS] T1 WITH (NOLOCK) INNER JOIN [CONTACTTYPES] T2 WITH (NOLOCK) ON T2.[ContactTypesCode] = T1.[ContactTypesCode]) WHERE T1.[ISOCod] = ? and T1.[AirLineCode] = ? and T1.[ContactTypesCode] = ? and T1.[AirLineCttSeq] = ? ORDER BY T1.[ISOCod], T1.[AirLineCode], T1.[ContactTypesCode], T1.[AirLineCttSeq] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B27", "SELECT [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("T002B28", "INSERT INTO [AIRLINESCONTACTS] ([ISOCod], [AirLineCode], [AirLineCttSeq], [AirLineCttName], [AirLineCttPhone], [AirLineCttEmail], [ContactTypesCode]) VALUES (?, ?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T002B29", "UPDATE [AIRLINESCONTACTS] SET [AirLineCttName]=?, [AirLineCttPhone]=?, [AirLineCttEmail]=?  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ?", GX_NOMASK)
         ,new UpdateCursor("T002B30", "DELETE FROM [AIRLINESCONTACTS]  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ?", GX_NOMASK)
         ,new ForEachCursor("T002B31", "SELECT [ContactTypesDescription] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T002B32", "SELECT [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE [ISOCod] = ? and [AirLineCode] = ? ORDER BY [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 1) ;
               ((String[]) buf[15])[0] = rslt.getString(14, 1) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getVarchar(15) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 1) ;
               ((String[]) buf[15])[0] = rslt.getString(14, 1) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getVarchar(15) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(14, 1) ;
               ((String[]) buf[17])[0] = rslt.getString(15, 1) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getVarchar(16) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 18 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 19 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 23 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 24 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
               break;
            case 25 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
            case 29 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 30 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 11 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 20, false);
               break;
            case 12 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 20, false);
               break;
            case 13 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               stmt.setDateTime(3, (java.util.Date)parms[2], false);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[4], 1);
               }
               stmt.setVarchar(5, (String)parms[5], 30, false);
               stmt.setVarchar(6, (String)parms[6], 5, false);
               stmt.setVarchar(7, (String)parms[7], 200, false);
               stmt.setVarchar(8, (String)parms[8], 30, false);
               stmt.setVarchar(9, (String)parms[9], 30, false);
               stmt.setVarchar(10, (String)parms[10], 5, false);
               stmt.setVarchar(11, (String)parms[11], 10, false);
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 200);
               }
               stmt.setString(13, (String)parms[14], 1);
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(14, (String)parms[16], 1);
               }
               stmt.setVarchar(15, (String)parms[17], 3, false);
               break;
            case 14 :
               stmt.setDateTime(1, (java.util.Date)parms[0], false);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 1);
               }
               stmt.setVarchar(4, (String)parms[4], 30, false);
               stmt.setVarchar(5, (String)parms[5], 5, false);
               stmt.setVarchar(6, (String)parms[6], 200, false);
               stmt.setVarchar(7, (String)parms[7], 30, false);
               stmt.setVarchar(8, (String)parms[8], 30, false);
               stmt.setVarchar(9, (String)parms[9], 5, false);
               stmt.setVarchar(10, (String)parms[10], 10, false);
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 200);
               }
               stmt.setString(12, (String)parms[13], 1);
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[15], 1);
               }
               stmt.setVarchar(14, (String)parms[16], 3, false);
               stmt.setVarchar(15, (String)parms[17], 20, false);
               break;
            case 15 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 16 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               break;
            case 18 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 19 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 20 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               stmt.setLongVarchar(4, (String)parms[3], false);
               stmt.setVarchar(5, (String)parms[4], 3, false);
               break;
            case 21 :
               stmt.setLongVarchar(1, (String)parms[0], false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               stmt.setVarchar(5, (String)parms[4], 3, false);
               break;
            case 22 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 23 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 24 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 25 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 26 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setVarchar(4, (String)parms[3], 30, false);
               stmt.setVarchar(5, (String)parms[4], 25, false);
               stmt.setVarchar(6, (String)parms[5], 40, false);
               stmt.setVarchar(7, (String)parms[6], 5, false);
               break;
            case 27 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setVarchar(2, (String)parms[1], 25, false);
               stmt.setVarchar(3, (String)parms[2], 40, false);
               stmt.setVarchar(4, (String)parms[3], 3, false);
               stmt.setVarchar(5, (String)parms[4], 20, false);
               stmt.setVarchar(6, (String)parms[5], 5, false);
               stmt.setShort(7, ((Number) parms[6]).shortValue());
               break;
            case 28 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 29 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 30 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
      }
   }

}


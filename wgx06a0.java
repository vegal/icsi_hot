/*
               File: Gx06A0
        Description: Selection List Airline Data
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:16.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx06a0 extends GXWorkpanel
{
   public wgx06a0( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx06a0.class ));
   }

   public wgx06a0( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx06A0" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List Airline Data" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 653 ;
   }

   protected int getFrmHeight( )
   {
      return 402 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx06A0.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      wgx06a0.this.aP0 = aP0;
      wgx06a0.this.aP1 = aP1;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load20( ) throws GXLoadInterruptException
   {
      subwgx06a020 = new subwgx06a020 ();
      lV5cISOCod = GXutil.padr( GXutil.rtrim( AV5cISOCod), (short)(3), "%") ;
      lV6cAirLin = GXutil.padr( GXutil.rtrim( AV6cAirLin), (short)(20), "%") ;
      lV7cAirLin = GXutil.padr( GXutil.rtrim( AV7cAirLin), (short)(30), "%") ;
      lV8cAirLin = GXutil.padr( GXutil.rtrim( AV8cAirLin), (short)(5), "%") ;
      lV9cAirLin = GXutil.padr( GXutil.rtrim( AV9cAirLin), (short)(30), "%") ;
      lV10cAirLi = GXutil.padr( GXutil.rtrim( AV10cAirLi), (short)(30), "%") ;
      lV11cAirLi = GXutil.padr( GXutil.rtrim( AV11cAirLi), (short)(5), "%") ;
      /* Using cursor W00122 */
      pr_default.execute(0, new Object[] {lV5cISOCod, lV6cAirLin, lV7cAirLin, lV8cAirLin, lV9cAirLin, lV10cAirLi, lV11cAirLi});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A1119AirLi = W00122_A1119AirLi[0] ;
         A1121AirLi = W00122_A1121AirLi[0] ;
         A1122AirLi = W00122_A1122AirLi[0] ;
         A1123AirLi = W00122_A1123AirLi[0] ;
         A1118AirLi = W00122_A1118AirLi[0] ;
         A1136AirLi = W00122_A1136AirLi[0] ;
         A23ISOCod = W00122_A23ISOCod[0] ;
         /* Execute user event: e11V122 */
         e11V122 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx06A0_load20 extends GXLoadProducer
   {
      wgx06a0 _sf ;

      public Gx06A0_load20( wgx06a0 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer20();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load20();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors20();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow20( )
   {
      return true;
   }

   public void autoRefresh_flow20( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cISOCod, cV5cISOCod)!=0)||(GXutil.strcmp(AV6cAirLin, cV6cAirLin)!=0)||(GXutil.strcmp(AV7cAirLin, cV7cAirLin)!=0)||(GXutil.strcmp(AV8cAirLin, cV8cAirLin)!=0)||(GXutil.strcmp(AV9cAirLin, cV9cAirLin)!=0)||(GXutil.strcmp(AV10cAirLi, cV10cAirLi)!=0)||(GXutil.strcmp(AV11cAirLi, cV11cAirLi)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow20() )) {
         subfile.refresh();
         resetSubfileConditions_flow20() ;
      }
   }

   public boolean getSearch_flow20( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow20( )
   {
      cV5cISOCod = AV5cISOCod ;
      cV6cAirLin = AV6cAirLin ;
      cV7cAirLin = AV7cAirLin ;
      cV8cAirLin = AV8cAirLin ;
      cV9cAirLin = AV9cAirLin ;
      cV10cAirLi = AV10cAirLi ;
      cV11cAirLi = AV11cAirLi ;
   }

   public void resetSearchConditions_flow20( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow20( )
   {
      return new subwgx06a020 ();
   }

   public boolean getSearch_flow20( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow20( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow20( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow20( )
   {
      GXRefreshCommand20 ();
   }

   public final  class Gx06A0_flow20 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx06a0 _sf ;

      public Gx06A0_flow20( wgx06a0 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow20();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow20(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow20();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow20();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow20(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow20();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow20(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow20(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow20(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow20();
      }

   }

   protected void GXRefreshCommand20( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V122 */
      e12V122 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V122( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV12pISOCo = A23ISOCod ;
      AV13pAirLi = A1136AirLi ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer20( )
   {
      subwgx06a020 oAux = subwgx06a020 ;
      subwgx06a020 = new subwgx06a020 ();
      variablesToSubfile20 ();
      subGrd_1.addElement(subwgx06a020);
      subwgx06a020 = oAux;
   }

   private void e11V122( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors20( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 653 , 402 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      dynavCisocod = new GUIObjectString ( new GXComboBox(GXPanel1, this, 6) , GXPanel1 , 203 , 24 , 47 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cISOCod" );
      dynavCisocod.addFocusListener(this);
      dynavCisocod.addItemListener(this);
      dynavCisocod.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      edtavCairlinecode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 48, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 48 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6cAirLin" );
      ((GXEdit) edtavCairlinecode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecode.addFocusListener(this);
      edtavCairlinecode.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      edtavCairlinedescription = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 72, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 72 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV7cAirLin" );
      ((GXEdit) edtavCairlinedescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinedescription.addFocusListener(this);
      edtavCairlinedescription.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      edtavCairlineabbreviation = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 96, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 96 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV8cAirLin" );
      ((GXEdit) edtavCairlineabbreviation.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlineabbreviation.addFocusListener(this);
      edtavCairlineabbreviation.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      edtavCairlineaddresscompl = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 120, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 120 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV9cAirLin" );
      ((GXEdit) edtavCairlineaddresscompl.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlineaddresscompl.addFocusListener(this);
      edtavCairlineaddresscompl.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      edtavCairlinecity = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 144, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 144 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV10cAirLi" );
      ((GXEdit) edtavCairlinecity.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecity.addFocusListener(this);
      edtavCairlinecity.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      edtavCairlinestate = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),203, 168, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 203 , 168 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV11cAirLi" );
      ((GXEdit) edtavCairlinestate.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinestate.addFocusListener(this);
      edtavCairlinestate.getGXComponent().setHelpId("HLP_WGx06A0.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx06A0_load20(this), new Gx06A0_flow20(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXComboBox(GXPanel1, this, 21) , null ,  0 , 0 , 84 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A23ISOCod" ), "Country code"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 84 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 148, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 147 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1136AirLi" ), "AirLine Code"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 147 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 218, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 217 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1118AirLi" ), "Airline Name"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 217 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 192 , 506 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  545 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  545 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  545 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  545 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Country code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 76 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "AirLine Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 73 , 13 );
      lbllbl11 = UIFactory.getLabel(GXPanel1, "Airline Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 72 , 72 , 13 );
      lbllbl13 = UIFactory.getLabel(GXPanel1, "Airline Abbreviation", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 96 , 111 , 13 );
      lbllbl15 = UIFactory.getLabel(GXPanel1, "Airline Address Complement", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 120 , 157 , 13 );
      lbllbl17 = UIFactory.getLabel(GXPanel1, "City Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 144 , 58 , 13 );
      lbllbl19 = UIFactory.getLabel(GXPanel1, "State", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 168 , 31 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                dynavCisocod ,
                edtavCairlinecode ,
                edtavCairlinedescription ,
                edtavCairlineabbreviation ,
                edtavCairlineaddresscompl ,
                edtavCairlinecity ,
                edtavCairlinestate ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
      if ( (id == 6) || (id == 0) )
      {
         /* Using cursor W00123 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            ((GXComboBox) dynavCisocod.getGXComponent()).addItem(W00123_A23ISOCod[0], W00123_A23ISOCod[0], (short)(0));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }
      if ( (id == 21) || (id == 0) )
      {
         /* Using cursor W00124 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            ((GXComboBox) subGrd_1.getColumn(0).getGXComponent()).addItem(W00124_A23ISOCod[0], W00124_A23ISOCod[0], (short)(0));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }
   }

   protected void variablesToSubfile20( )
   {
      subwgx06a020.setISOCod(A23ISOCod);
      subwgx06a020.setAirLineCode(A1136AirLi);
      subwgx06a020.setAirLineDescription(A1118AirLi);
   }

   protected void subfileToVariables20( )
   {
      A23ISOCod = subwgx06a020.getISOCod();
      A1136AirLi = subwgx06a020.getAirLineCode();
      A1118AirLi = subwgx06a020.getAirLineDescription();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      dynavCisocod.setValue( AV5cISOCod );
      edtavCairlinecode.setValue( AV6cAirLin );
      edtavCairlinedescription.setValue( AV7cAirLin );
      edtavCairlineabbreviation.setValue( AV8cAirLin );
      edtavCairlineaddresscompl.setValue( AV9cAirLin );
      edtavCairlinecity.setValue( AV10cAirLi );
      edtavCairlinestate.setValue( AV11cAirLi );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      AV5cISOCod = dynavCisocod.getValue() ;
      AV6cAirLin = edtavCairlinecode.getValue() ;
      AV7cAirLin = edtavCairlinedescription.getValue() ;
      AV8cAirLin = edtavCairlineabbreviation.getValue() ;
      AV9cAirLin = edtavCairlineaddresscompl.getValue() ;
      AV10cAirLi = edtavCairlinecity.getValue() ;
      AV11cAirLi = edtavCairlinestate.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx06a020 = ( subwgx06a020 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06a020 = new subwgx06a020 ();
      }
      subfileToVariables20 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile20 ();
      subGrd_1.refreshLineValue(subwgx06a020);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx06a020 = ( subwgx06a020 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06a020 = new subwgx06a020 ();
      }
      subfileToVariables20 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V122 */
         e12V122 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V122 */
         e12V122 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( dynavCisocod.isEventSource(eventSource) ) {
         setGXCursor( dynavCisocod.getGXCursor() );
         return;
      }
      if ( edtavCairlinecode.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecode.getGXCursor() );
         return;
      }
      if ( edtavCairlinedescription.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinedescription.getGXCursor() );
         return;
      }
      if ( edtavCairlineabbreviation.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlineabbreviation.getGXCursor() );
         return;
      }
      if ( edtavCairlineaddresscompl.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlineaddresscompl.getGXCursor() );
         return;
      }
      if ( edtavCairlinecity.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecity.getGXCursor() );
         return;
      }
      if ( edtavCairlinestate.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinestate.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( dynavCisocod.isEventSource(eventSource) ) {
         AV5cISOCod = dynavCisocod.getValue() ;
         return;
      }
      if ( edtavCairlinecode.isEventSource(eventSource) ) {
         AV6cAirLin = edtavCairlinecode.getValue() ;
         return;
      }
      if ( edtavCairlinedescription.isEventSource(eventSource) ) {
         AV7cAirLin = edtavCairlinedescription.getValue() ;
         return;
      }
      if ( edtavCairlineabbreviation.isEventSource(eventSource) ) {
         AV8cAirLin = edtavCairlineabbreviation.getValue() ;
         return;
      }
      if ( edtavCairlineaddresscompl.isEventSource(eventSource) ) {
         AV9cAirLin = edtavCairlineaddresscompl.getValue() ;
         return;
      }
      if ( edtavCairlinecity.isEventSource(eventSource) ) {
         AV10cAirLi = edtavCairlinecity.getValue() ;
         return;
      }
      if ( edtavCairlinestate.isEventSource(eventSource) ) {
         AV11cAirLi = edtavCairlinestate.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V122 */
         e12V122 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = wgx06a0.this.AV12pISOCo;
      this.aP1[0] = wgx06a0.this.AV13pAirLi;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV12pISOCo = "" ;
      AV13pAirLi = "" ;
      subwgx06a020 = new subwgx06a020();
      scmdbuf = "" ;
      lV5cISOCod = "" ;
      AV5cISOCod = "" ;
      lV6cAirLin = "" ;
      AV6cAirLin = "" ;
      lV7cAirLin = "" ;
      AV7cAirLin = "" ;
      lV8cAirLin = "" ;
      AV8cAirLin = "" ;
      lV9cAirLin = "" ;
      AV9cAirLin = "" ;
      lV10cAirLi = "" ;
      AV10cAirLi = "" ;
      lV11cAirLi = "" ;
      AV11cAirLi = "" ;
      W00122_A1119AirLi = new String[] {""} ;
      W00122_A1121AirLi = new String[] {""} ;
      W00122_A1122AirLi = new String[] {""} ;
      W00122_A1123AirLi = new String[] {""} ;
      W00122_A1118AirLi = new String[] {""} ;
      W00122_A1136AirLi = new String[] {""} ;
      W00122_A23ISOCod = new String[] {""} ;
      A1119AirLi = "" ;
      A1121AirLi = "" ;
      A1122AirLi = "" ;
      A1123AirLi = "" ;
      A1118AirLi = "" ;
      A1136AirLi = "" ;
      A23ISOCod = "" ;
      gxIsRefreshing = false ;
      cV5cISOCod = "" ;
      cV6cAirLin = "" ;
      cV7cAirLin = "" ;
      cV8cAirLin = "" ;
      cV9cAirLin = "" ;
      cV10cAirLi = "" ;
      cV11cAirLi = "" ;
      returnInSub = false ;
      W00123_A23ISOCod = new String[] {""} ;
      W00124_A23ISOCod = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx06a0__default(),
         new Object[] {
             new Object[] {
            W00122_A1119AirLi, W00122_A1121AirLi, W00122_A1122AirLi, W00122_A1123AirLi, W00122_A1118AirLi, W00122_A1136AirLi, W00122_A23ISOCod
            }
            , new Object[] {
            W00123_A23ISOCod
            }
            , new Object[] {
            W00124_A23ISOCod
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String AV12pISOCo ;
   protected String AV13pAirLi ;
   protected String lV5cISOCod ;
   protected String AV5cISOCod ;
   protected String lV6cAirLin ;
   protected String AV6cAirLin ;
   protected String lV7cAirLin ;
   protected String AV7cAirLin ;
   protected String lV8cAirLin ;
   protected String AV8cAirLin ;
   protected String lV9cAirLin ;
   protected String AV9cAirLin ;
   protected String lV10cAirLi ;
   protected String AV10cAirLi ;
   protected String lV11cAirLi ;
   protected String AV11cAirLi ;
   protected String A1119AirLi ;
   protected String A1121AirLi ;
   protected String A1122AirLi ;
   protected String A1123AirLi ;
   protected String A1118AirLi ;
   protected String A1136AirLi ;
   protected String A23ISOCod ;
   protected String cV5cISOCod ;
   protected String cV6cAirLin ;
   protected String cV7cAirLin ;
   protected String cV8cAirLin ;
   protected String cV9cAirLin ;
   protected String cV10cAirLi ;
   protected String cV11cAirLi ;
   protected String[] aP0 ;
   protected String[] aP1 ;
   protected subwgx06a020 subwgx06a020 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00122_A1119AirLi ;
   protected String[] W00122_A1121AirLi ;
   protected String[] W00122_A1122AirLi ;
   protected String[] W00122_A1123AirLi ;
   protected String[] W00122_A1118AirLi ;
   protected String[] W00122_A1136AirLi ;
   protected String[] W00122_A23ISOCod ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString dynavCisocod ;
   protected GUIObjectString edtavCairlinecode ;
   protected GUIObjectString edtavCairlinedescription ;
   protected GUIObjectString edtavCairlineabbreviation ;
   protected GUIObjectString edtavCairlineaddresscompl ;
   protected GUIObjectString edtavCairlinecity ;
   protected GUIObjectString edtavCairlinestate ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
   protected ILabel lbllbl11 ;
   protected ILabel lbllbl13 ;
   protected ILabel lbllbl15 ;
   protected ILabel lbllbl17 ;
   protected ILabel lbllbl19 ;
   protected String[] W00123_A23ISOCod ;
   protected String[] W00124_A23ISOCod ;
}

final  class wgx06a0__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00122", "SELECT [AirLineAbbreviation], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineDescription], [AirLineCode], [ISOCod] FROM [AIRLINES] WITH (FASTFIRSTROW NOLOCK) WHERE ([ISOCod] like ?) AND ([AirLineCode] like ?) AND ([AirLineDescription] like ?) AND ([AirLineAbbreviation] like ?) AND ([AirLineAddressCompl] like ?) AND ([AirLineCity] like ?) AND ([AirLineState] like ?) ORDER BY [ISOCod], [AirLineCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("W00123", "SELECT [ISOCod] FROM [COUNTRY] WITH (NOLOCK) ORDER BY [ISOCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("W00124", "SELECT [ISOCod] FROM [COUNTRY] WITH (NOLOCK) ORDER BY [ISOCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3);
               stmt.setVarchar(2, (String)parms[1], 20);
               stmt.setVarchar(3, (String)parms[2], 30);
               stmt.setVarchar(4, (String)parms[3], 5);
               stmt.setVarchar(5, (String)parms[4], 30);
               stmt.setVarchar(6, (String)parms[5], 30);
               stmt.setVarchar(7, (String)parms[6], 5);
               break;
      }
   }

}


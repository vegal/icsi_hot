/*
               File: Utilitarios
        Description: Utilitarios
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:4.83
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class autilitarios extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      autilitarios pgm = new autilitarios (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};
      String[] aP1 = new String[] {""};
      String[] aP2 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
         aP1[0] = (String) args[1];
         aP2[0] = (String) args[2];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0, aP1, aP2);
   }

   public autilitarios( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( autilitarios.class ), "" );
   }

   public autilitarios( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      autilitarios.this.AV8Codigo = aP0[0];
      this.aP0 = aP0;
      autilitarios.this.AV9Parm1 = aP1[0];
      this.aP1 = aP1;
      autilitarios.this.AV10Parm2 = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( GXutil.strcmp(AV8Codigo, "7") != 0 ) && ( GXutil.strcmp(AV8Codigo, "8") != 0 ) )
      {
         context.msgStatus( "** U T I L I T A R I O S **" );
         context.msgStatus( "---------------------------" );
         context.msgStatus( "" );
         context.msgStatus( "PARAMETROS" );
         context.msgStatus( "Codigo do Utilitario: "+GXutil.trim( AV8Codigo) );
         context.msgStatus( "Parametro 1: "+GXutil.trim( AV9Parm1) );
         context.msgStatus( "Parametro 2: "+GXutil.trim( AV10Parm2) );
         context.msgStatus( "" );
      }
      if ( ( GXutil.strcmp(AV8Codigo, "1") == 0 ) )
      {
         context.msgStatus( "Buscando bilhetes..." );
         AV17GXLvl2 = (byte)(0) ;
         /* Using cursor P00762 */
         pr_default.execute(0, new Object[] {AV9Parm1});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1150lccbE = P00762_A1150lccbE[0] ;
            A1222lccbI = P00762_A1222lccbI[0] ;
            A1227lccbO = P00762_A1227lccbO[0] ;
            A1228lccbF = P00762_A1228lccbF[0] ;
            A1231lccbT = P00762_A1231lccbT[0] ;
            A1224lccbC = P00762_A1224lccbC[0] ;
            A1223lccbD = P00762_A1223lccbD[0] ;
            A1225lccbC = P00762_A1225lccbC[0] ;
            A1226lccbA = P00762_A1226lccbA[0] ;
            A1172lccbS = P00762_A1172lccbS[0] ;
            n1172lccbS = P00762_n1172lccbS[0] ;
            A1184lccbS = P00762_A1184lccbS[0] ;
            n1184lccbS = P00762_n1184lccbS[0] ;
            A1194lccbS = P00762_A1194lccbS[0] ;
            n1194lccbS = P00762_n1194lccbS[0] ;
            A1201lccbR = P00762_A1201lccbR[0] ;
            n1201lccbR = P00762_n1201lccbR[0] ;
            A1192lccbS = P00762_A1192lccbS[0] ;
            n1192lccbS = P00762_n1192lccbS[0] ;
            A1199lccbR = P00762_A1199lccbR[0] ;
            n1199lccbR = P00762_n1199lccbR[0] ;
            A1490Distr = P00762_A1490Distr[0] ;
            n1490Distr = P00762_n1490Distr[0] ;
            A1232lccbT = P00762_A1232lccbT[0] ;
            A1172lccbS = P00762_A1172lccbS[0] ;
            n1172lccbS = P00762_n1172lccbS[0] ;
            A1184lccbS = P00762_A1184lccbS[0] ;
            n1184lccbS = P00762_n1184lccbS[0] ;
            A1194lccbS = P00762_A1194lccbS[0] ;
            n1194lccbS = P00762_n1194lccbS[0] ;
            A1201lccbR = P00762_A1201lccbR[0] ;
            n1201lccbR = P00762_n1201lccbR[0] ;
            A1192lccbS = P00762_A1192lccbS[0] ;
            n1192lccbS = P00762_n1192lccbS[0] ;
            A1199lccbR = P00762_A1199lccbR[0] ;
            n1199lccbR = P00762_n1199lccbR[0] ;
            A1490Distr = P00762_A1490Distr[0] ;
            n1490Distr = P00762_n1490Distr[0] ;
            AV17GXLvl2 = (byte)(1) ;
            context.msgStatus( "Bandeira: "+GXutil.trim( A1224lccbC) );
            context.msgStatus( "Data de Venda: "+GXutil.trim( localUtil.dtoc( A1223lccbD, 2, "/")) );
            context.msgStatus( "Cartao: "+GXutil.trim( A1225lccbC) );
            context.msgStatus( "Autorizacao: "+GXutil.trim( A1226lccbA) );
            context.msgStatus( "Valor: "+GXutil.trim( GXutil.str( A1172lccbS, 12, 2)) );
            context.msgStatus( "Status: "+GXutil.trim( A1184lccbS) );
            context.msgStatus( "Lote: "+GXutil.trim( A1194lccbS) );
            context.msgStatus( "C�d Erro: "+GXutil.trim( GXutil.str( A1201lccbR, 10, 0)) );
            context.msgStatus( "Submissao: "+GXutil.trim( A1192lccbS) );
            context.msgStatus( "Retorno: "+GXutil.trim( A1199lccbR) );
            context.msgStatus( "Processadora: "+GXutil.trim( A1490Distr) );
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( ( AV17GXLvl2 == 0 ) )
         {
            context.msgStatus( "Bilhete nao encontrado" );
         }
      }
      if ( ( GXutil.strcmp(AV8Codigo, "2") == 0 ) )
      {
         context.msgStatus( "Buscando cartoes..." );
         AV18GXLvl5 = (byte)(0) ;
         /* Using cursor P00763 */
         pr_default.execute(1, new Object[] {AV9Parm1});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1228lccbF = P00763_A1228lccbF[0] ;
            A1227lccbO = P00763_A1227lccbO[0] ;
            A1226lccbA = P00763_A1226lccbA[0] ;
            A1224lccbC = P00763_A1224lccbC[0] ;
            A1222lccbI = P00763_A1222lccbI[0] ;
            A1150lccbE = P00763_A1150lccbE[0] ;
            A1223lccbD = P00763_A1223lccbD[0] ;
            A1225lccbC = P00763_A1225lccbC[0] ;
            A1172lccbS = P00763_A1172lccbS[0] ;
            n1172lccbS = P00763_n1172lccbS[0] ;
            if ( ( GXutil.strcmp(AV10Parm2, "") == 0 ) || ( A1223lccbD.equals( localUtil.ctod( GXutil.trim( AV10Parm2), 2) ) ) )
            {
               AV18GXLvl5 = (byte)(1) ;
               context.msgStatus( "Bandeira: "+GXutil.trim( A1224lccbC) );
               context.msgStatus( "Data de Venda: "+GXutil.trim( localUtil.dtoc( A1223lccbD, 2, "/")) );
               context.msgStatus( "Valor: "+GXutil.trim( GXutil.str( A1172lccbS, 12, 2)) );
               AV11i = 1 ;
               /* Using cursor P00764 */
               pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1231lccbT = P00764_A1231lccbT[0] ;
                  A1232lccbT = P00764_A1232lccbT[0] ;
                  context.msgStatus( "Bilhete: "+GXutil.trim( GXutil.str( AV11i, 10, 0))+" - "+GXutil.trim( A1231lccbT) );
                  AV11i = (int)(AV11i+1) ;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               context.msgStatus( "------" );
               context.msgStatus( "" );
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( ( AV18GXLvl5 == 0 ) )
         {
            context.msgStatus( "Bilhete nao encontrado" );
         }
      }
      if ( ( GXutil.strcmp(AV8Codigo, "3") == 0 ) )
      {
         context.msgStatus( "Buscando lotes pendentes de retorno..." );
         /* Using cursor P00765 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            brk765 = false ;
            A1184lccbS = P00765_A1184lccbS[0] ;
            n1184lccbS = P00765_n1184lccbS[0] ;
            A1224lccbC = P00765_A1224lccbC[0] ;
            A1490Distr = P00765_A1490Distr[0] ;
            n1490Distr = P00765_n1490Distr[0] ;
            A1223lccbD = P00765_A1223lccbD[0] ;
            A1194lccbS = P00765_A1194lccbS[0] ;
            n1194lccbS = P00765_n1194lccbS[0] ;
            A1150lccbE = P00765_A1150lccbE[0] ;
            A1222lccbI = P00765_A1222lccbI[0] ;
            A1225lccbC = P00765_A1225lccbC[0] ;
            A1226lccbA = P00765_A1226lccbA[0] ;
            A1227lccbO = P00765_A1227lccbO[0] ;
            A1228lccbF = P00765_A1228lccbF[0] ;
            if ( ( GXutil.strcmp(AV9Parm1, "") == 0 ) || ( (( A1223lccbD.after( localUtil.ctod( GXutil.trim( AV9Parm1), 2) ) ) || ( A1223lccbD.equals( localUtil.ctod( GXutil.trim( AV9Parm1), 2) ) )) ) )
            {
               if ( ( GXutil.strcmp(AV10Parm2, "") == 0 ) || ( (( A1223lccbD.before( localUtil.ctod( GXutil.trim( AV10Parm2), 2) ) ) || ( A1223lccbD.equals( localUtil.ctod( GXutil.trim( AV10Parm2), 2) ) )) ) )
               {
                  context.msgStatus( "Bandeira: "+GXutil.trim( A1490Distr)+" "+GXutil.trim( A1224lccbC) );
                  context.msgStatus( "Lote: "+GXutil.trim( A1194lccbS) );
                  context.msgStatus( "Data Venda: "+GXutil.trim( localUtil.dtoc( A1223lccbD, 2, "/")) );
                  context.msgStatus( "----" );
                  context.msgStatus( "" );
                  while ( (pr_default.getStatus(3) != 101) && ( GXutil.strcmp(P00765_A1184lccbS[0], A1184lccbS) == 0 ) && P00765_A1223lccbD[0].equals( A1223lccbD ) && ( GXutil.strcmp(P00765_A1490Distr[0], A1490Distr) == 0 ) && ( GXutil.strcmp(P00765_A1224lccbC[0], A1224lccbC) == 0 ) )
                  {
                     brk765 = false ;
                     A1150lccbE = P00765_A1150lccbE[0] ;
                     A1222lccbI = P00765_A1222lccbI[0] ;
                     A1225lccbC = P00765_A1225lccbC[0] ;
                     A1226lccbA = P00765_A1226lccbA[0] ;
                     A1227lccbO = P00765_A1227lccbO[0] ;
                     A1228lccbF = P00765_A1228lccbF[0] ;
                     W1184lccbS = A1184lccbS ;
                     n1184lccbS = false ;
                     W1490Distr = A1490Distr ;
                     n1490Distr = false ;
                     A1184lccbS = W1184lccbS ;
                     n1184lccbS = false ;
                     A1490Distr = W1490Distr ;
                     n1490Distr = false ;
                     brk765 = true ;
                     pr_default.readNext(3);
                  }
               }
            }
            if ( ! brk765 )
            {
               brk765 = true ;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }
      if ( ( GXutil.strcmp(AV8Codigo, "4") == 0 ) )
      {
         AV22Datefr = localUtil.ctod( GXutil.trim( AV9Parm1), 2) ;
         AV23Dateto = localUtil.ctod( GXutil.trim( AV10Parm2), 2) ;
         /* Using cursor P00766 */
         pr_default.execute(4, new Object[] {AV22Datefr, AV22Datefr, AV23Dateto});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1184lccbS = P00766_A1184lccbS[0] ;
            n1184lccbS = P00766_n1184lccbS[0] ;
            A1223lccbD = P00766_A1223lccbD[0] ;
            A1150lccbE = P00766_A1150lccbE[0] ;
            A1222lccbI = P00766_A1222lccbI[0] ;
            A1224lccbC = P00766_A1224lccbC[0] ;
            A1225lccbC = P00766_A1225lccbC[0] ;
            A1226lccbA = P00766_A1226lccbA[0] ;
            A1227lccbO = P00766_A1227lccbO[0] ;
            A1228lccbF = P00766_A1228lccbF[0] ;
            if ( ( ( GXutil.strcmp(A1184lccbS, "OKI1010") == 0 ) ) || ( ( GXutil.strcmp(A1184lccbS, "I1012") == 0 ) ) )
            {
               if ( ( GXutil.strcmp(A1184lccbS, "OKI1010") == 0 ) )
               {
                  A1184lccbS = "RETOK" ;
                  n1184lccbS = false ;
               }
               else if ( ( GXutil.strcmp(A1184lccbS, "I1012") == 0 ) )
               {
                  A1184lccbS = "RETNOK" ;
                  n1184lccbS = false ;
               }
               /* Using cursor P00767 */
               pr_default.execute(5, new Object[] {new Boolean(n1184lccbS), A1184lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }
      if ( ( GXutil.strcmp(AV8Codigo, "5") == 0 ) )
      {
         GXt_char11 = AV12DebugM ;
         GXv_svchar12[0] = GXt_char11 ;
         new pr2getparm(remoteHandle, context).execute( "DEBUGMODE_1_6", "Debug Mode do R4122", "S", "DEBUG", GXv_svchar12) ;
         autilitarios.this.GXt_char11 = GXv_svchar12[0] ;
         AV12DebugM = GXt_char11 ;
         AV22Datefr = localUtil.ctod( GXutil.trim( AV9Parm1), 2) ;
         AV23Dateto = localUtil.ctod( GXutil.trim( AV10Parm2), 2) ;
         GXv_svchar12[0] = AV12DebugM ;
         GXv_date13[0] = AV22Datefr ;
         GXv_date14[0] = AV23Dateto ;
         GXv_char15[0] = "R" ;
         new putilitario_reti1022(remoteHandle, context).execute( GXv_svchar12, GXv_date13, GXv_date14, GXv_char15) ;
         autilitarios.this.AV12DebugM = GXv_svchar12[0] ;
         autilitarios.this.AV22Datefr = GXv_date13[0] ;
         autilitarios.this.AV23Dateto = GXv_date14[0] ;
      }
      if ( ( GXutil.strcmp(AV8Codigo, "6") == 0 ) )
      {
         /* Using cursor P00768 */
         pr_default.execute(6, new Object[] {AV9Parm1});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1298SCETk = P00768_A1298SCETk[0] ;
            n1298SCETk = P00768_n1298SCETk[0] ;
            A1306SCEAP = P00768_A1306SCEAP[0] ;
            n1306SCEAP = P00768_n1306SCEAP[0] ;
            A1305SCETe = P00768_A1305SCETe[0] ;
            n1305SCETe = P00768_n1305SCETe[0] ;
            A1312SCEId = P00768_A1312SCEId[0] ;
            context.msgStatus( A1306SCEAP );
            context.msgStatus( A1305SCETe );
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }
      if ( ( GXutil.strcmp(AV8Codigo, "7") == 0 ) )
      {
         AV26GXLvl1 = (byte)(0) ;
         /* Using cursor P00769 */
         pr_default.execute(7, new Object[] {AV9Parm1});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1150lccbE = P00769_A1150lccbE[0] ;
            A1222lccbI = P00769_A1222lccbI[0] ;
            A1227lccbO = P00769_A1227lccbO[0] ;
            A1228lccbF = P00769_A1228lccbF[0] ;
            A1231lccbT = P00769_A1231lccbT[0] ;
            A1192lccbS = P00769_A1192lccbS[0] ;
            n1192lccbS = P00769_n1192lccbS[0] ;
            A1223lccbD = P00769_A1223lccbD[0] ;
            A1490Distr = P00769_A1490Distr[0] ;
            n1490Distr = P00769_n1490Distr[0] ;
            A1224lccbC = P00769_A1224lccbC[0] ;
            A1226lccbA = P00769_A1226lccbA[0] ;
            A1225lccbC = P00769_A1225lccbC[0] ;
            A1199lccbR = P00769_A1199lccbR[0] ;
            n1199lccbR = P00769_n1199lccbR[0] ;
            A1201lccbR = P00769_A1201lccbR[0] ;
            n1201lccbR = P00769_n1201lccbR[0] ;
            A1184lccbS = P00769_A1184lccbS[0] ;
            n1184lccbS = P00769_n1184lccbS[0] ;
            A1172lccbS = P00769_A1172lccbS[0] ;
            n1172lccbS = P00769_n1172lccbS[0] ;
            A1194lccbS = P00769_A1194lccbS[0] ;
            n1194lccbS = P00769_n1194lccbS[0] ;
            A1196lccbR = P00769_A1196lccbR[0] ;
            n1196lccbR = P00769_n1196lccbR[0] ;
            A1232lccbT = P00769_A1232lccbT[0] ;
            A1192lccbS = P00769_A1192lccbS[0] ;
            n1192lccbS = P00769_n1192lccbS[0] ;
            A1490Distr = P00769_A1490Distr[0] ;
            n1490Distr = P00769_n1490Distr[0] ;
            A1199lccbR = P00769_A1199lccbR[0] ;
            n1199lccbR = P00769_n1199lccbR[0] ;
            A1201lccbR = P00769_A1201lccbR[0] ;
            n1201lccbR = P00769_n1201lccbR[0] ;
            A1184lccbS = P00769_A1184lccbS[0] ;
            n1184lccbS = P00769_n1184lccbS[0] ;
            A1172lccbS = P00769_A1172lccbS[0] ;
            n1172lccbS = P00769_n1172lccbS[0] ;
            A1194lccbS = P00769_A1194lccbS[0] ;
            n1194lccbS = P00769_n1194lccbS[0] ;
            A1196lccbR = P00769_A1196lccbR[0] ;
            n1196lccbR = P00769_n1196lccbR[0] ;
            AV26GXLvl1 = (byte)(1) ;
            AV13msg1 = GXutil.trim( A1231lccbT) + ";Cart�o;" + GXutil.trim( A1225lccbC) + ";" + GXutil.trim( A1226lccbA) + ";" + GXutil.trim( A1224lccbC) + ";" + GXutil.trim( A1490Distr) + ";" + GXutil.trim( localUtil.dtoc( A1223lccbD, 2, "/")) + ";" + GXutil.trim( A1192lccbS) + ";" ;
            AV13msg1 = AV13msg1 + GXutil.trim( A1194lccbS) + ";" + GXutil.trim( GXutil.str( A1172lccbS, 12, 2)) + ";" + GXutil.trim( A1184lccbS) + ";" + GXutil.trim( GXutil.str( A1201lccbR, 10, 0)) + ";" + GXutil.trim( A1199lccbR) + ";" ;
            AV13msg1 = AV13msg1 + GXutil.trim( localUtil.dtoc( A1196lccbR, 2, "/")) ;
            context.msgStatus( AV13msg1 );
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(7);
         }
         pr_default.close(7);
         if ( ( AV26GXLvl1 == 0 ) )
         {
            AV14Encont = 0 ;
            /* Execute user subroutine: S12184 */
            S12184 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            if ( ( AV14Encont == 0 ) )
            {
               /* Execute user subroutine: S11168 */
               S11168 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
            }
            if ( ( AV14Encont == 0 ) )
            {
               context.msgStatus( GXutil.trim( AV9Parm1)+";Bilhete nao encontrado" );
            }
         }
      }
      if ( ( GXutil.strcmp(AV8Codigo, "8") == 0 ) )
      {
         AV27GXLvl1 = (byte)(0) ;
         /* Using cursor P007610 */
         pr_default.execute(8, new Object[] {AV9Parm1});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A1306SCEAP = P007610_A1306SCEAP[0] ;
            n1306SCEAP = P007610_n1306SCEAP[0] ;
            A1298SCETk = P007610_A1298SCETk[0] ;
            n1298SCETk = P007610_n1298SCETk[0] ;
            A1312SCEId = P007610_A1312SCEId[0] ;
            AV27GXLvl1 = (byte)(1) ;
            AV13msg1 = GXutil.trim( AV9Parm1) + ";NOSUB" ;
            context.msgStatus( AV13msg1 );
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(8);
         }
         pr_default.close(8);
         if ( ( AV27GXLvl1 == 0 ) )
         {
            context.msgStatus( GXutil.trim( AV9Parm1)+";Bilhete nao encontrado" );
         }
      }
      cleanup();
   }

   public void S11168( )
   {
      /* 'BUSCAHOT' Routine */
      /* Using cursor P007611 */
      pr_default.execute(9, new Object[] {AV9Parm1});
      while ( (pr_default.getStatus(9) != 101) )
      {
         A968NUM_BI = P007611_A968NUM_BI[0] ;
         A874AIRPT_ = P007611_A874AIRPT_[0] ;
         n874AIRPT_ = P007611_n874AIRPT_[0] ;
         A963ISOC = P007611_A963ISOC[0] ;
         A964CiaCod = P007611_A964CiaCod[0] ;
         A965PER_NA = P007611_A965PER_NA[0] ;
         A966CODE = P007611_A966CODE[0] ;
         A967IATA = P007611_A967IATA[0] ;
         A969TIPO_V = P007611_A969TIPO_V[0] ;
         A970DATA = P007611_A970DATA[0] ;
         if ( ( A874AIRPT_ != 0 ) )
         {
            AV13msg1 = GXutil.trim( AV9Parm1) + ";HOT Cart�o" ;
            context.msgStatus( AV13msg1 );
         }
         else
         {
            AV13msg1 = GXutil.trim( AV9Parm1) + ";HOT Cash" ;
            context.msgStatus( AV13msg1 );
         }
         AV14Encont = (int)(AV14Encont+1) ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(9);
      }
      pr_default.close(9);
   }

   public void S12184( )
   {
      /* 'BUSCAEVENTOS' Routine */
      /* Using cursor P007612 */
      pr_default.execute(10, new Object[] {AV9Parm1});
      while ( (pr_default.getStatus(10) != 101) )
      {
         A1306SCEAP = P007612_A1306SCEAP[0] ;
         n1306SCEAP = P007612_n1306SCEAP[0] ;
         A1298SCETk = P007612_A1298SCETk[0] ;
         n1298SCETk = P007612_n1298SCETk[0] ;
         A1312SCEId = P007612_A1312SCEId[0] ;
         AV13msg1 = GXutil.trim( AV9Parm1) + ";Cart�o NOSUB" ;
         context.msgStatus( AV13msg1 );
         AV14Encont = (int)(AV14Encont+1) ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(10);
      }
      pr_default.close(10);
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(putilitarios.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = autilitarios.this.AV8Codigo;
      this.aP1[0] = autilitarios.this.AV9Parm1;
      this.aP2[0] = autilitarios.this.AV10Parm2;
      Application.commit(context, remoteHandle, "DEFAULT", "autilitarios");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV17GXLvl2 = (byte)(0) ;
      scmdbuf = "" ;
      P00762_A1150lccbE = new String[] {""} ;
      P00762_A1222lccbI = new String[] {""} ;
      P00762_A1227lccbO = new String[] {""} ;
      P00762_A1228lccbF = new String[] {""} ;
      P00762_A1231lccbT = new String[] {""} ;
      P00762_A1224lccbC = new String[] {""} ;
      P00762_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00762_A1225lccbC = new String[] {""} ;
      P00762_A1226lccbA = new String[] {""} ;
      P00762_A1172lccbS = new double[1] ;
      P00762_n1172lccbS = new boolean[] {false} ;
      P00762_A1184lccbS = new String[] {""} ;
      P00762_n1184lccbS = new boolean[] {false} ;
      P00762_A1194lccbS = new String[] {""} ;
      P00762_n1194lccbS = new boolean[] {false} ;
      P00762_A1201lccbR = new int[1] ;
      P00762_n1201lccbR = new boolean[] {false} ;
      P00762_A1192lccbS = new String[] {""} ;
      P00762_n1192lccbS = new boolean[] {false} ;
      P00762_A1199lccbR = new String[] {""} ;
      P00762_n1199lccbR = new boolean[] {false} ;
      P00762_A1490Distr = new String[] {""} ;
      P00762_n1490Distr = new boolean[] {false} ;
      P00762_A1232lccbT = new String[] {""} ;
      A1150lccbE = "" ;
      A1222lccbI = "" ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      A1231lccbT = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1201lccbR = 0 ;
      n1201lccbR = false ;
      A1192lccbS = "" ;
      n1192lccbS = false ;
      A1199lccbR = "" ;
      n1199lccbR = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1232lccbT = "" ;
      GXt_char3 = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      GXt_char4 = "" ;
      GXt_char5 = "" ;
      GXt_char6 = "" ;
      GXt_char7 = "" ;
      GXt_char8 = "" ;
      AV18GXLvl5 = (byte)(0) ;
      P00763_A1228lccbF = new String[] {""} ;
      P00763_A1227lccbO = new String[] {""} ;
      P00763_A1226lccbA = new String[] {""} ;
      P00763_A1224lccbC = new String[] {""} ;
      P00763_A1222lccbI = new String[] {""} ;
      P00763_A1150lccbE = new String[] {""} ;
      P00763_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00763_A1225lccbC = new String[] {""} ;
      P00763_A1172lccbS = new double[1] ;
      P00763_n1172lccbS = new boolean[] {false} ;
      AV11i = 0 ;
      P00764_A1150lccbE = new String[] {""} ;
      P00764_A1222lccbI = new String[] {""} ;
      P00764_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00764_A1224lccbC = new String[] {""} ;
      P00764_A1225lccbC = new String[] {""} ;
      P00764_A1226lccbA = new String[] {""} ;
      P00764_A1227lccbO = new String[] {""} ;
      P00764_A1228lccbF = new String[] {""} ;
      P00764_A1231lccbT = new String[] {""} ;
      P00764_A1232lccbT = new String[] {""} ;
      P00765_A1184lccbS = new String[] {""} ;
      P00765_n1184lccbS = new boolean[] {false} ;
      P00765_A1224lccbC = new String[] {""} ;
      P00765_A1490Distr = new String[] {""} ;
      P00765_n1490Distr = new boolean[] {false} ;
      P00765_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00765_A1194lccbS = new String[] {""} ;
      P00765_n1194lccbS = new boolean[] {false} ;
      P00765_A1150lccbE = new String[] {""} ;
      P00765_A1222lccbI = new String[] {""} ;
      P00765_A1225lccbC = new String[] {""} ;
      P00765_A1226lccbA = new String[] {""} ;
      P00765_A1227lccbO = new String[] {""} ;
      P00765_A1228lccbF = new String[] {""} ;
      brk765 = false ;
      GXt_char10 = "" ;
      GXt_char9 = "" ;
      W1184lccbS = "" ;
      W1490Distr = "" ;
      AV22Datefr = GXutil.nullDate() ;
      AV23Dateto = GXutil.nullDate() ;
      P00766_A1184lccbS = new String[] {""} ;
      P00766_n1184lccbS = new boolean[] {false} ;
      P00766_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00766_A1150lccbE = new String[] {""} ;
      P00766_A1222lccbI = new String[] {""} ;
      P00766_A1224lccbC = new String[] {""} ;
      P00766_A1225lccbC = new String[] {""} ;
      P00766_A1226lccbA = new String[] {""} ;
      P00766_A1227lccbO = new String[] {""} ;
      P00766_A1228lccbF = new String[] {""} ;
      AV12DebugM = "" ;
      GXv_svchar12 = new String [1] ;
      GXv_date13 = new java.util.Date [1] ;
      GXv_date14 = new java.util.Date [1] ;
      GXv_char15 = new String [1] ;
      P00768_A1298SCETk = new String[] {""} ;
      P00768_n1298SCETk = new boolean[] {false} ;
      P00768_A1306SCEAP = new String[] {""} ;
      P00768_n1306SCEAP = new boolean[] {false} ;
      P00768_A1305SCETe = new String[] {""} ;
      P00768_n1305SCETe = new boolean[] {false} ;
      P00768_A1312SCEId = new long[1] ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      A1305SCETe = "" ;
      n1305SCETe = false ;
      A1312SCEId = 0 ;
      AV26GXLvl1 = (byte)(0) ;
      P00769_A1150lccbE = new String[] {""} ;
      P00769_A1222lccbI = new String[] {""} ;
      P00769_A1227lccbO = new String[] {""} ;
      P00769_A1228lccbF = new String[] {""} ;
      P00769_A1231lccbT = new String[] {""} ;
      P00769_A1192lccbS = new String[] {""} ;
      P00769_n1192lccbS = new boolean[] {false} ;
      P00769_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00769_A1490Distr = new String[] {""} ;
      P00769_n1490Distr = new boolean[] {false} ;
      P00769_A1224lccbC = new String[] {""} ;
      P00769_A1226lccbA = new String[] {""} ;
      P00769_A1225lccbC = new String[] {""} ;
      P00769_A1199lccbR = new String[] {""} ;
      P00769_n1199lccbR = new boolean[] {false} ;
      P00769_A1201lccbR = new int[1] ;
      P00769_n1201lccbR = new boolean[] {false} ;
      P00769_A1184lccbS = new String[] {""} ;
      P00769_n1184lccbS = new boolean[] {false} ;
      P00769_A1172lccbS = new double[1] ;
      P00769_n1172lccbS = new boolean[] {false} ;
      P00769_A1194lccbS = new String[] {""} ;
      P00769_n1194lccbS = new boolean[] {false} ;
      P00769_A1196lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P00769_n1196lccbR = new boolean[] {false} ;
      P00769_A1232lccbT = new String[] {""} ;
      A1196lccbR = GXutil.nullDate() ;
      n1196lccbR = false ;
      AV13msg1 = "" ;
      AV14Encont = 0 ;
      returnInSub = false ;
      AV27GXLvl1 = (byte)(0) ;
      P007610_A1306SCEAP = new String[] {""} ;
      P007610_n1306SCEAP = new boolean[] {false} ;
      P007610_A1298SCETk = new String[] {""} ;
      P007610_n1298SCETk = new boolean[] {false} ;
      P007610_A1312SCEId = new long[1] ;
      GXt_char11 = "" ;
      P007611_A968NUM_BI = new String[] {""} ;
      P007611_A874AIRPT_ = new double[1] ;
      P007611_n874AIRPT_ = new boolean[] {false} ;
      P007611_A963ISOC = new String[] {""} ;
      P007611_A964CiaCod = new String[] {""} ;
      P007611_A965PER_NA = new String[] {""} ;
      P007611_A966CODE = new String[] {""} ;
      P007611_A967IATA = new String[] {""} ;
      P007611_A969TIPO_V = new String[] {""} ;
      P007611_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      A968NUM_BI = "" ;
      A874AIRPT_ = 0 ;
      n874AIRPT_ = false ;
      A963ISOC = "" ;
      A964CiaCod = "" ;
      A965PER_NA = "" ;
      A966CODE = "" ;
      A967IATA = "" ;
      A969TIPO_V = "" ;
      A970DATA = GXutil.nullDate() ;
      P007612_A1306SCEAP = new String[] {""} ;
      P007612_n1306SCEAP = new boolean[] {false} ;
      P007612_A1298SCETk = new String[] {""} ;
      P007612_n1298SCETk = new boolean[] {false} ;
      P007612_A1312SCEId = new long[1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new autilitarios__default(),
         new Object[] {
             new Object[] {
            P00762_A1150lccbE, P00762_A1222lccbI, P00762_A1227lccbO, P00762_A1228lccbF, P00762_A1231lccbT, P00762_A1224lccbC, P00762_A1223lccbD, P00762_A1225lccbC, P00762_A1226lccbA, P00762_A1172lccbS,
            P00762_n1172lccbS, P00762_A1184lccbS, P00762_n1184lccbS, P00762_A1194lccbS, P00762_n1194lccbS, P00762_A1201lccbR, P00762_n1201lccbR, P00762_A1192lccbS, P00762_n1192lccbS, P00762_A1199lccbR,
            P00762_n1199lccbR, P00762_A1490Distr, P00762_n1490Distr, P00762_A1232lccbT
            }
            , new Object[] {
            P00763_A1228lccbF, P00763_A1227lccbO, P00763_A1226lccbA, P00763_A1224lccbC, P00763_A1222lccbI, P00763_A1150lccbE, P00763_A1223lccbD, P00763_A1225lccbC, P00763_A1172lccbS, P00763_n1172lccbS
            }
            , new Object[] {
            P00764_A1150lccbE, P00764_A1222lccbI, P00764_A1223lccbD, P00764_A1224lccbC, P00764_A1225lccbC, P00764_A1226lccbA, P00764_A1227lccbO, P00764_A1228lccbF, P00764_A1231lccbT, P00764_A1232lccbT
            }
            , new Object[] {
            P00765_A1184lccbS, P00765_n1184lccbS, P00765_A1224lccbC, P00765_A1490Distr, P00765_n1490Distr, P00765_A1223lccbD, P00765_A1194lccbS, P00765_n1194lccbS, P00765_A1150lccbE, P00765_A1222lccbI,
            P00765_A1225lccbC, P00765_A1226lccbA, P00765_A1227lccbO, P00765_A1228lccbF
            }
            , new Object[] {
            P00766_A1184lccbS, P00766_n1184lccbS, P00766_A1223lccbD, P00766_A1150lccbE, P00766_A1222lccbI, P00766_A1224lccbC, P00766_A1225lccbC, P00766_A1226lccbA, P00766_A1227lccbO, P00766_A1228lccbF
            }
            , new Object[] {
            }
            , new Object[] {
            P00768_A1298SCETk, P00768_n1298SCETk, P00768_A1306SCEAP, P00768_n1306SCEAP, P00768_A1305SCETe, P00768_n1305SCETe, P00768_A1312SCEId
            }
            , new Object[] {
            P00769_A1150lccbE, P00769_A1222lccbI, P00769_A1227lccbO, P00769_A1228lccbF, P00769_A1231lccbT, P00769_A1192lccbS, P00769_n1192lccbS, P00769_A1223lccbD, P00769_A1490Distr, P00769_n1490Distr,
            P00769_A1224lccbC, P00769_A1226lccbA, P00769_A1225lccbC, P00769_A1199lccbR, P00769_n1199lccbR, P00769_A1201lccbR, P00769_n1201lccbR, P00769_A1184lccbS, P00769_n1184lccbS, P00769_A1172lccbS,
            P00769_n1172lccbS, P00769_A1194lccbS, P00769_n1194lccbS, P00769_A1196lccbR, P00769_n1196lccbR, P00769_A1232lccbT
            }
            , new Object[] {
            P007610_A1306SCEAP, P007610_n1306SCEAP, P007610_A1298SCETk, P007610_n1298SCETk, P007610_A1312SCEId
            }
            , new Object[] {
            P007611_A968NUM_BI, P007611_A874AIRPT_, P007611_n874AIRPT_, P007611_A963ISOC, P007611_A964CiaCod, P007611_A965PER_NA, P007611_A966CODE, P007611_A967IATA, P007611_A969TIPO_V, P007611_A970DATA
            }
            , new Object[] {
            P007612_A1306SCEAP, P007612_n1306SCEAP, P007612_A1298SCETk, P007612_n1298SCETk, P007612_A1312SCEId
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV17GXLvl2 ;
   private byte AV18GXLvl5 ;
   private byte AV26GXLvl1 ;
   private byte AV27GXLvl1 ;
   private short Gx_err ;
   private int A1201lccbR ;
   private int AV11i ;
   private int AV14Encont ;
   private long A1312SCEId ;
   private double A1172lccbS ;
   private double A874AIRPT_ ;
   private String scmdbuf ;
   private String A1150lccbE ;
   private String A1222lccbI ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private String A1231lccbT ;
   private String A1224lccbC ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1184lccbS ;
   private String A1194lccbS ;
   private String A1192lccbS ;
   private String A1199lccbR ;
   private String A1490Distr ;
   private String A1232lccbT ;
   private String GXt_char3 ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXt_char4 ;
   private String GXt_char5 ;
   private String GXt_char6 ;
   private String GXt_char7 ;
   private String GXt_char8 ;
   private String GXt_char10 ;
   private String GXt_char9 ;
   private String W1184lccbS ;
   private String W1490Distr ;
   private String GXv_char15[] ;
   private String A1298SCETk ;
   private String A1306SCEAP ;
   private String A1305SCETe ;
   private String GXt_char11 ;
   private String A968NUM_BI ;
   private String A963ISOC ;
   private String A964CiaCod ;
   private String A965PER_NA ;
   private String A966CODE ;
   private String A967IATA ;
   private String A969TIPO_V ;
   private java.util.Date A1223lccbD ;
   private java.util.Date AV22Datefr ;
   private java.util.Date AV23Dateto ;
   private java.util.Date GXv_date13[] ;
   private java.util.Date GXv_date14[] ;
   private java.util.Date A1196lccbR ;
   private java.util.Date A970DATA ;
   private boolean n1172lccbS ;
   private boolean n1184lccbS ;
   private boolean n1194lccbS ;
   private boolean n1201lccbR ;
   private boolean n1192lccbS ;
   private boolean n1199lccbR ;
   private boolean n1490Distr ;
   private boolean brk765 ;
   private boolean n1298SCETk ;
   private boolean n1306SCEAP ;
   private boolean n1305SCETe ;
   private boolean n1196lccbR ;
   private boolean returnInSub ;
   private boolean n874AIRPT_ ;
   private String AV8Codigo ;
   private String AV9Parm1 ;
   private String AV10Parm2 ;
   private String AV12DebugM ;
   private String GXv_svchar12[] ;
   private String AV13msg1 ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P00762_A1150lccbE ;
   private String[] P00762_A1222lccbI ;
   private String[] P00762_A1227lccbO ;
   private String[] P00762_A1228lccbF ;
   private String[] P00762_A1231lccbT ;
   private String[] P00762_A1224lccbC ;
   private java.util.Date[] P00762_A1223lccbD ;
   private String[] P00762_A1225lccbC ;
   private String[] P00762_A1226lccbA ;
   private double[] P00762_A1172lccbS ;
   private boolean[] P00762_n1172lccbS ;
   private String[] P00762_A1184lccbS ;
   private boolean[] P00762_n1184lccbS ;
   private String[] P00762_A1194lccbS ;
   private boolean[] P00762_n1194lccbS ;
   private int[] P00762_A1201lccbR ;
   private boolean[] P00762_n1201lccbR ;
   private String[] P00762_A1192lccbS ;
   private boolean[] P00762_n1192lccbS ;
   private String[] P00762_A1199lccbR ;
   private boolean[] P00762_n1199lccbR ;
   private String[] P00762_A1490Distr ;
   private boolean[] P00762_n1490Distr ;
   private String[] P00762_A1232lccbT ;
   private String[] P00763_A1228lccbF ;
   private String[] P00763_A1227lccbO ;
   private String[] P00763_A1226lccbA ;
   private String[] P00763_A1224lccbC ;
   private String[] P00763_A1222lccbI ;
   private String[] P00763_A1150lccbE ;
   private java.util.Date[] P00763_A1223lccbD ;
   private String[] P00763_A1225lccbC ;
   private double[] P00763_A1172lccbS ;
   private boolean[] P00763_n1172lccbS ;
   private String[] P00764_A1150lccbE ;
   private String[] P00764_A1222lccbI ;
   private java.util.Date[] P00764_A1223lccbD ;
   private String[] P00764_A1224lccbC ;
   private String[] P00764_A1225lccbC ;
   private String[] P00764_A1226lccbA ;
   private String[] P00764_A1227lccbO ;
   private String[] P00764_A1228lccbF ;
   private String[] P00764_A1231lccbT ;
   private String[] P00764_A1232lccbT ;
   private String[] P00765_A1184lccbS ;
   private boolean[] P00765_n1184lccbS ;
   private String[] P00765_A1224lccbC ;
   private String[] P00765_A1490Distr ;
   private boolean[] P00765_n1490Distr ;
   private java.util.Date[] P00765_A1223lccbD ;
   private String[] P00765_A1194lccbS ;
   private boolean[] P00765_n1194lccbS ;
   private String[] P00765_A1150lccbE ;
   private String[] P00765_A1222lccbI ;
   private String[] P00765_A1225lccbC ;
   private String[] P00765_A1226lccbA ;
   private String[] P00765_A1227lccbO ;
   private String[] P00765_A1228lccbF ;
   private String[] P00766_A1184lccbS ;
   private boolean[] P00766_n1184lccbS ;
   private java.util.Date[] P00766_A1223lccbD ;
   private String[] P00766_A1150lccbE ;
   private String[] P00766_A1222lccbI ;
   private String[] P00766_A1224lccbC ;
   private String[] P00766_A1225lccbC ;
   private String[] P00766_A1226lccbA ;
   private String[] P00766_A1227lccbO ;
   private String[] P00766_A1228lccbF ;
   private String[] P00768_A1298SCETk ;
   private boolean[] P00768_n1298SCETk ;
   private String[] P00768_A1306SCEAP ;
   private boolean[] P00768_n1306SCEAP ;
   private String[] P00768_A1305SCETe ;
   private boolean[] P00768_n1305SCETe ;
   private long[] P00768_A1312SCEId ;
   private String[] P00769_A1150lccbE ;
   private String[] P00769_A1222lccbI ;
   private String[] P00769_A1227lccbO ;
   private String[] P00769_A1228lccbF ;
   private String[] P00769_A1231lccbT ;
   private String[] P00769_A1192lccbS ;
   private boolean[] P00769_n1192lccbS ;
   private java.util.Date[] P00769_A1223lccbD ;
   private String[] P00769_A1490Distr ;
   private boolean[] P00769_n1490Distr ;
   private String[] P00769_A1224lccbC ;
   private String[] P00769_A1226lccbA ;
   private String[] P00769_A1225lccbC ;
   private String[] P00769_A1199lccbR ;
   private boolean[] P00769_n1199lccbR ;
   private int[] P00769_A1201lccbR ;
   private boolean[] P00769_n1201lccbR ;
   private String[] P00769_A1184lccbS ;
   private boolean[] P00769_n1184lccbS ;
   private double[] P00769_A1172lccbS ;
   private boolean[] P00769_n1172lccbS ;
   private String[] P00769_A1194lccbS ;
   private boolean[] P00769_n1194lccbS ;
   private java.util.Date[] P00769_A1196lccbR ;
   private boolean[] P00769_n1196lccbR ;
   private String[] P00769_A1232lccbT ;
   private String[] P007610_A1306SCEAP ;
   private boolean[] P007610_n1306SCEAP ;
   private String[] P007610_A1298SCETk ;
   private boolean[] P007610_n1298SCETk ;
   private long[] P007610_A1312SCEId ;
   private String[] P007611_A968NUM_BI ;
   private double[] P007611_A874AIRPT_ ;
   private boolean[] P007611_n874AIRPT_ ;
   private String[] P007611_A963ISOC ;
   private String[] P007611_A964CiaCod ;
   private String[] P007611_A965PER_NA ;
   private String[] P007611_A966CODE ;
   private String[] P007611_A967IATA ;
   private String[] P007611_A969TIPO_V ;
   private java.util.Date[] P007611_A970DATA ;
   private String[] P007612_A1306SCEAP ;
   private boolean[] P007612_n1306SCEAP ;
   private String[] P007612_A1298SCETk ;
   private boolean[] P007612_n1298SCETk ;
   private long[] P007612_A1312SCEId ;
}

final  class autilitarios__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00762", "SELECT TOP 1 T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T1.[lccbCCard], T1.[lccbDate], T1.[lccbCCNum], T1.[lccbAppCode], T2.[lccbSaleAmount], T2.[lccbStatus], T2.[lccbSubRO], T2.[lccbRSubError], T2.[lccbSubFile], T2.[lccbRSubFile], T2.[DistribuicaoTransacoesTipo], T1.[lccbTRNC] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate] = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum] AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP]) WHERE T1.[lccbTDNR] = RTRIM(LTRIM(?)) ORDER BY T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbDate], T1.[lccbCCard], T1.[lccbCCNum], T1.[lccbAppCode], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T1.[lccbTRNC] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P00763", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCard], [lccbIATA], [lccbEmpCod], [lccbDate], [lccbCCNum], [lccbSaleAmount] FROM [LCCBPLP] WITH (NOLOCK) WHERE [lccbCCNum] = RTRIM(LTRIM(?)) ORDER BY [lccbCCNum], [lccbDate] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00764", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00765", "SELECT [lccbStatus], [lccbCCard], [DistribuicaoTransacoesTipo], [lccbDate], [lccbSubRO], [lccbEmpCod], [lccbIATA], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (NOLOCK) WHERE [lccbStatus] = 'PROCPLP' ORDER BY [lccbStatus], [lccbDate], [DistribuicaoTransacoesTipo], [lccbCCard] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00766", "SELECT [lccbStatus], [lccbDate], [lccbEmpCod], [lccbIATA], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbDate] >= ?) AND (([lccbDate] >= ?) AND (( [lccbStatus] = 'OKI1010') or ( [lccbStatus] = 'I1012')) AND ([lccbDate] <= ?)) ORDER BY [lccbDate], [lccbStatus] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P00767", "UPDATE [LCCBPLP] SET [lccbStatus]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00768", "SELECT [SCETkt], [SCEAPP], [SCEText], [SCEId] FROM [SCEVENTS] WITH (NOLOCK) WHERE [SCETkt] = ? ORDER BY [SCEId] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00769", "SELECT TOP 1 T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T2.[lccbSubFile], T1.[lccbDate], T2.[DistribuicaoTransacoesTipo], T1.[lccbCCard], T1.[lccbAppCode], T1.[lccbCCNum], T2.[lccbRSubFile], T2.[lccbRSubError], T2.[lccbStatus], T2.[lccbSaleAmount], T2.[lccbSubRO], T2.[lccbRSubDate], T1.[lccbTRNC] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate] = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum] AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP]) WHERE T1.[lccbTDNR] = RTRIM(LTRIM(?)) ORDER BY T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbDate], T1.[lccbCCard], T1.[lccbCCNum], T1.[lccbAppCode], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T1.[lccbTRNC] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007610", "SELECT TOP 1 [SCEAPP], [SCETkt], [SCEId] FROM [SCEVENTS] WITH (NOLOCK) WHERE ([SCETkt] = RTRIM(LTRIM(?))) AND ([SCETkt] <> '') AND ([SCEAPP] = 'ICSI') ORDER BY [SCEId] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007611", "SELECT TOP 1 [NUM_BIL], [AIRPT_TAX], [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE SUBSTRING([NUM_BIL], 1, 10) = RTRIM(LTRIM(?)) ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007612", "SELECT TOP 1 [SCEAPP], [SCETkt], [SCEId] FROM [SCEVENTS] WITH (NOLOCK) WHERE ([SCETkt] = RTRIM(LTRIM(?))) AND ([SCETkt] <> '') AND ([SCEAPP] = 'ICSI') ORDER BY [SCEId] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 19) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 2) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 44) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 20) ;
               ((double[]) buf[9])[0] = rslt.getDouble(10) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(11, 8) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((String[]) buf[13])[0] = rslt.getString(12, 10) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((int[]) buf[15])[0] = rslt.getInt(13) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getString(14, 20) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getString(15, 20) ;
               ((boolean[]) buf[20])[0] = rslt.wasNull();
               ((String[]) buf[21])[0] = rslt.getString(16, 4) ;
               ((boolean[]) buf[22])[0] = rslt.wasNull();
               ((String[]) buf[23])[0] = rslt.getString(17, 4) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 7) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 3) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 44) ;
               ((double[]) buf[8])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 8) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getString(6, 3) ;
               ((String[]) buf[9])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[10])[0] = rslt.getString(8, 44) ;
               ((String[]) buf[11])[0] = rslt.getString(9, 20) ;
               ((String[]) buf[12])[0] = rslt.getString(10, 1) ;
               ((String[]) buf[13])[0] = rslt.getString(11, 19) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 8) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 7) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 19) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 4) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 150) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((long[]) buf[6])[0] = rslt.getLong(4) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 19) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 10) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(7) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 4) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(9, 2) ;
               ((String[]) buf[11])[0] = rslt.getString(10, 20) ;
               ((String[]) buf[12])[0] = rslt.getString(11, 44) ;
               ((String[]) buf[13])[0] = rslt.getString(12, 20) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((int[]) buf[15])[0] = rslt.getInt(13) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getString(14, 8) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((double[]) buf[19])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[20])[0] = rslt.wasNull();
               ((String[]) buf[21])[0] = rslt.getString(16, 10) ;
               ((boolean[]) buf[22])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[23])[0] = rslt.getGXDate(17) ;
               ((boolean[]) buf[24])[0] = rslt.wasNull();
               ((String[]) buf[25])[0] = rslt.getString(18, 4) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((long[]) buf[4])[0] = rslt.getLong(3) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((double[]) buf[1])[0] = rslt.getDouble(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 20) ;
               ((java.util.Date[]) buf[9])[0] = rslt.getGXDate(9) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((long[]) buf[4])[0] = rslt.getLong(3) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 4 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               stmt.setDate(2, (java.util.Date)parms[1]);
               stmt.setDate(3, (java.util.Date)parms[2]);
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 255);
               break;
      }
   }

}


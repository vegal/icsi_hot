/*
               File: R2GetPath
        Description: R2 Get Path
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:1.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2getpath extends GXProcedure
{
   public pr2getpath( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2getpath.class ), "" );
   }

   public pr2getpath( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String[] aP1 )
   {
      pr2getpath.this.AV8FileIn = aP0;
      pr2getpath.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV9FileOut = "" ;
      AV10i = GXutil.len( AV8FileIn) ;
      while ( ( AV10i >= 1 ) )
      {
         AV11c = GXutil.substring( AV8FileIn, AV10i, 1) ;
         if ( ( GXutil.strcmp(AV11c, "\\") == 0 ) || ( GXutil.strcmp(AV11c, ":") == 0 ) )
         {
            if (true) break;
         }
         AV12Count = (int)(AV12Count+1) ;
         AV10i = (int)(AV10i+-1) ;
      }
      AV13Tamanh = (int)(GXutil.len( AV8FileIn)-AV12Count) ;
      AV9FileOut = GXutil.substring( AV8FileIn, 1, AV13Tamanh) ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP1[0] = pr2getpath.this.AV9FileOut;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9FileOut = "" ;
      AV10i = 0 ;
      AV11c = "" ;
      AV12Count = 0 ;
      AV13Tamanh = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private int AV10i ;
   private int AV12Count ;
   private int AV13Tamanh ;
   private String AV8FileIn ;
   private String AV9FileOut ;
   private String AV11c ;
   private String[] aP1 ;
}


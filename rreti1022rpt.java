/*
               File: RETi1022rpt
        Description: Gera��o dos relat�rios i1022/i1023
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: August 27, 2015 8:27:26.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;
import com.genexus.reports.*;

public final  class rreti1022rpt extends GXReport
{
   public rreti1022rpt( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( rreti1022rpt.class ), "" );
   }

   public rreti1022rpt( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 )
   {
      rreti1022rpt.this.AV20lccbEm = aP0[0];
      this.aP0 = aP0;
      rreti1022rpt.this.AV21EmpNom = aP1[0];
      this.aP1 = aP1;
      rreti1022rpt.this.AV22FilePD = aP2[0];
      this.aP2 = aP2;
      rreti1022rpt.this.AV23FileTX = aP3[0];
      this.aP3 = aP3;
      rreti1022rpt.this.AV31DebugM = aP4[0];
      this.aP4 = aP4;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      M_top = 0 ;
      M_bot = 4 ;
      P_lines = (int)(66-M_bot) ;
      getPrinter().GxClearAttris() ;
      add_metrics( ) ;
      lineHeight = 16 ;
      PrtOffset = 0 ;
      gxXPage = 96 ;
      gxYPage = 96 ;
      getPrinter().GxSetDocName(AV22FilePD) ;
      getPrinter().GxSetDocFormat("PDF") ;
      try
      {
         Gx_out = "FIL" ;
         if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 9, 16838, 11906, 0, 1, 1, 0, 1, 1) )
         {
            cleanup();
            return;
         }
         getPrinter().setModal(true) ;
         P_lines = (int)(gxYPage-(lineHeight*4)) ;
         Gx_line = (int)(P_lines+1) ;
         getPrinter().setPageLines(P_lines);
         getPrinter().setLineHeight(lineHeight);
         getPrinter().setM_top(M_top);
         getPrinter().setM_bot(M_bot);
         GXt_char1 = AV65CarFim ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "UTILIZA_LINHA_FIM", "S= utiliza Char(20) / N= NewLine() ", "S", "N", GXv_svchar2) ;
         rreti1022rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV65CarFim = GXt_char1 ;
         AV32i = (short)(GXutil.strSearch( AV31DebugM, "PROCDATE=", 1)) ;
         if ( ( AV32i > 0 ) )
         {
            AV32i = (short)(AV32i+9) ;
            AV27sOutpu = GXutil.trim( GXutil.substring( AV31DebugM, AV32i, 10)) ;
         }
         else
         {
            GXt_char1 = AV27sOutpu ;
            GXv_svchar2[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "ICSIDiaRep4122", "Dia Relat�rio 4122", "S", "[TODAY]", GXv_svchar2) ;
            rreti1022rpt.this.GXt_char1 = GXv_svchar2[0] ;
            AV27sOutpu = GXt_char1 ;
            AV27sOutpu = GXutil.trim( AV27sOutpu) ;
         }
         if ( ( GXutil.strcmp(AV27sOutpu, "[TODAY]") == 0 ) )
         {
            AV29lccbSu = GXutil.resetTime( Gx_date );
         }
         else
         {
            AV29lccbSu = GXutil.resetTime( localUtil.ctod( AV27sOutpu, 2) );
         }
         AV35Decisa = "?" ;
         AV36Decisa = "?" ;
         AV37Decisa = "?" ;
         AV38sDecis = "" ;
         AV39sDecis = "" ;
         AV40sDecis = "" ;
         /* Using cursor R006M2 */
         pr_default.execute(0, new Object[] {AV20lccbEm});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1147lccbE = R006M2_A1147lccbE[0] ;
            n1147lccbE = R006M2_n1147lccbE[0] ;
            A1150lccbE = R006M2_A1150lccbE[0] ;
            A1166lccbP = R006M2_A1166lccbP[0] ;
            A1165lccbD = R006M2_A1165lccbD[0] ;
            n1165lccbD = R006M2_n1165lccbD[0] ;
            A1147lccbE = R006M2_A1147lccbE[0] ;
            n1147lccbE = R006M2_n1147lccbE[0] ;
            if ( ( GXutil.strcmp(A1166lccbP, "1") == 0 ) )
            {
               AV35Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "V") == 0 ) )
               {
                  AV38sDecis = "Processar com 1 parcela (� vista)" ;
               }
               else if ( ( GXutil.strcmp(A1165lccbD, "C") == 0 ) || ( GXutil.strcmp(A1165lccbD, "Y") == 0 ) )
               {
                  AV38sDecis = "Transformar para Cash (N�O USADO NO ICSI, CONTACTE A R2TECH !)" ;
               }
               else
               {
                  AV38sDecis = "Rejeitar a transa��o" ;
               }
            }
            else if ( ( GXutil.strcmp(A1166lccbP, "2") == 0 ) )
            {
               AV36Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "R") == 0 ) || ( GXutil.strcmp(A1165lccbD, "N") == 0 ) )
               {
                  AV39sDecis = "Processar com informa��es da RET, aceitando o valor da parcela dado pelo agente" ;
               }
               else
               {
                  AV39sDecis = "Rejeitar a transa��o" ;
               }
            }
            else if ( ( GXutil.strcmp(A1166lccbP, "3") == 0 ) )
            {
               AV37Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "R") == 0 ) || ( GXutil.strcmp(A1165lccbD, "Y") == 0 ) )
               {
                  AV40sDecis = "Processar com informa��es da RET, aceitando o valor da parcela dado pelo agente" ;
               }
               else
               {
                  if ( ( GXutil.strcmp(A1165lccbD, "C") == 0 ) )
                  {
                     AV40sDecis = "Calcular parcela pelo ICSI (baseado na tarifa)" ;
                  }
                  else
                  {
                     AV40sDecis = "Rejeitar a transa��o" ;
                  }
               }
            }
            else if ( ( GXutil.strcmp(A1166lccbP, "4") == 0 ) )
            {
               AV74Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "R") == 0 ) )
               {
                  AV75SDecis = "Processar com informa��es da RET, aceitando o valor da parcela dado pelo agente" ;
               }
               else
               {
                  AV75SDecis = "Rejeitar a transa��o" ;
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         h6M0( false, 195) ;
         getPrinter().GxDrawRect(7, Gx_line+2, 1001, Gx_line+180, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV38sDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 31, Gx_line+24, 510, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV39sDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 31, Gx_line+59, 510, Gx_line+75, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV40sDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 31, Gx_line+93, 510, Gx_line+109, 0) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV75SDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 31, Gx_line+130, 510, Gx_line+146, 0+256) ;
         getPrinter().GxAttris("MS Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Problema 1: Falta da quantidade de presta��es e/ou valor da parcela", 16, Gx_line+8, 411, Gx_line+21, 0+256) ;
         getPrinter().GxDrawText("Problema 2: Falta do c�digo de parcelamento ou n�o encontrado no banco de dados", 16, Gx_line+43, 497, Gx_line+56, 0+256) ;
         getPrinter().GxDrawText("Problema 3: Quantidade e/ou valor de presta��o diferentes do plano", 16, Gx_line+77, 406, Gx_line+90, 0+256) ;
         getPrinter().GxDrawText("Problema 4: N�mero de parcelas n�o � aceita pelo c�digo do plano", 16, Gx_line+114, 398, Gx_line+127, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+195) ;
         /* Execute user subroutine: S11102 */
         S11102 ();
         if ( returnInSub )
         {
         }
         /* Print footer for last page */
         ToSkip = (int)(P_lines+1) ;
         h6M0( true, 0) ;
         /* Close printer file */
         getPrinter().GxEndDocument() ;
         endPrinter();
      }
      catch ( ProcessInterruptedException e )
      {
      }
      cleanup();
   }

   public void S11102( ) throws ProcessInterruptedException
   {
      /* 'MAIN' Routine */
      if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
      {
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV23FileTX, "", "", (byte)(0), "270") ;
      }
      else
      {
         AV66xmlWri.openURL(AV23FileTX);
      }
      AV63Date = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      AV43LinhaA = "C" + GXutil.padl( AV20lccbEm, (short)(3), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV29lccbSu), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV29lccbSu), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV29lccbSu), 10, 0)), (short)(2), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.str( AV87ICSI_C, 8, 0), (short)(3), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.trim( GXutil.str( GXutil.year( AV63Date), 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV63Date), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV63Date), 10, 0)), (short)(2), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( GXutil.str( GXutil.hour( AV63Date), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.minute( AV63Date), 10, 0)), (short)(2), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.space( (short)(244)) ;
      if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
      {
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(270)) ;
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      else
      {
         AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(270), " ") ;
         AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
         AV66xmlWri.writeRawText(AV43LinhaA);
      }
      AV30nSeq = 0 ;
      AV101GXLvl = (byte)(0) ;
      /* Using cursor R006M3 */
      pr_default.execute(1, new Object[] {AV20lccbEm, AV29lccbSu});
      while ( (pr_default.getStatus(1) != 101) )
      {
         A1301SCEAi = R006M3_A1301SCEAi[0] ;
         n1301SCEAi = R006M3_n1301SCEAi[0] ;
         A1306SCEAP = R006M3_A1306SCEAP[0] ;
         n1306SCEAP = R006M3_n1306SCEAP[0] ;
         A1310SCETp = R006M3_A1310SCETp[0] ;
         n1310SCETp = R006M3_n1310SCETp[0] ;
         A1309SCEFo = R006M3_A1309SCEFo[0] ;
         n1309SCEFo = R006M3_n1309SCEFo[0] ;
         A1299SCEDa = R006M3_A1299SCEDa[0] ;
         n1299SCEDa = R006M3_n1299SCEDa[0] ;
         A1307SCEIa = R006M3_A1307SCEIa[0] ;
         n1307SCEIa = R006M3_n1307SCEIa[0] ;
         A1305SCETe = R006M3_A1305SCETe[0] ;
         n1305SCETe = R006M3_n1305SCETe[0] ;
         A1517SCEFP = R006M3_A1517SCEFP[0] ;
         n1517SCEFP = R006M3_n1517SCEFP[0] ;
         A1518SCEPa = R006M3_A1518SCEPa[0] ;
         n1518SCEPa = R006M3_n1518SCEPa[0] ;
         A1298SCETk = R006M3_A1298SCETk[0] ;
         n1298SCETk = R006M3_n1298SCETk[0] ;
         AV101GXLvl = (byte)(1) ;
         AV30nSeq = (int)(AV30nSeq+1) ;
         AV45Empres = A1301SCEAi ;
         AV44IATA = GXutil.substring( GXutil.trim( A1307SCEIa), 1, 7) ;
         AV46SCETkt = A1298SCETk ;
         AV59SCETpE = A1310SCETp ;
         AV60SCETex = GXutil.substring( GXutil.trim( A1305SCETe), 1, 40) ;
         AV85SCEFop = A1309SCEFo ;
         AV60SCETex = A1305SCETe ;
         AV93SCEFPA = GXutil.strReplace( GXutil.trim( GXutil.str( A1517SCEFP, 14, 2)), ".", "") ;
         AV102Scepa = GXutil.strReplace( GXutil.trim( GXutil.str( A1518SCEPa, 14, 2)), ".", "") ;
         AV94SCEPar = A1518SCEPa ;
         /* Execute user subroutine: S125 */
         S125 ();
         if ( returnInSub )
         {
            pr_default.close(1);
            getPrinter().GxEndPage() ;
            /* Close printer file */
            getPrinter().GxEndDocument() ;
            endPrinter();
            returnInSub = true;
            if (true) return;
         }
         AV80lccbFa = ((AV68lccbOr==0) ? AV48lccbSa-AV50lccbTi : AV68lccbOr-AV50lccbTi) ;
         AV77lccbFa = GXutil.strReplace( GXutil.trim( GXutil.str( AV80lccbFa, 14, 2)), ".", "") ;
         AV78lccbDo = GXutil.strReplace( GXutil.trim( GXutil.str( AV71lccbDo, 14, 2)), ".", "") ;
         AV79lccbIn = GXutil.strReplace( GXutil.trim( GXutil.str( AV72lccbIn, 14, 2)), ".", "") ;
         AV49lccbSa = GXutil.strReplace( GXutil.trim( GXutil.str( AV48lccbSa, 14, 2)), ".", "") ;
         AV51lccbTi = GXutil.strReplace( GXutil.trim( GXutil.str( AV50lccbTi, 14, 2)), ".", "") ;
         AV53lccbDa = GXutil.trim( GXutil.str( GXutil.year( AV52lccbDa), 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV52lccbDa), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV52lccbDa), 10, 0)), (short)(2), "0") ;
         AV58lccbIn = GXutil.trim( GXutil.str( AV57lccbIn, 10, 0)) ;
         /* Execute user subroutine: S135 */
         S135 ();
         if ( returnInSub )
         {
            pr_default.close(1);
            getPrinter().GxEndPage() ;
            /* Close printer file */
            getPrinter().GxEndDocument() ;
            endPrinter();
            returnInSub = true;
            if (true) return;
         }
         AV103GXLvl = (byte)(0) ;
         /* Using cursor R006M4 */
         pr_default.execute(2, new Object[] {AV20lccbEm, AV83lccbIA, AV52lccbDa, AV55lccbCC, AV84CCEnc, AV56lccbAp, AV46SCETkt});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1227lccbO = R006M4_A1227lccbO[0] ;
            A1228lccbF = R006M4_A1228lccbF[0] ;
            A1231lccbT = R006M4_A1231lccbT[0] ;
            A1226lccbA = R006M4_A1226lccbA[0] ;
            A1225lccbC = R006M4_A1225lccbC[0] ;
            A1224lccbC = R006M4_A1224lccbC[0] ;
            A1223lccbD = R006M4_A1223lccbD[0] ;
            A1222lccbI = R006M4_A1222lccbI[0] ;
            A1150lccbE = R006M4_A1150lccbE[0] ;
            A1171lccbT = R006M4_A1171lccbT[0] ;
            n1171lccbT = R006M4_n1171lccbT[0] ;
            A1171lccbT = R006M4_A1171lccbT[0] ;
            n1171lccbT = R006M4_n1171lccbT[0] ;
            if ( ( GXutil.strcmp(A1231lccbT, AV46SCETkt) != 0 ) )
            {
               AV103GXLvl = (byte)(1) ;
               AV46SCETkt = A1231lccbT ;
               AV80lccbFa = 0 ;
               AV77lccbFa = "" ;
               AV78lccbDo = "" ;
               AV79lccbIn = "" ;
               AV49lccbSa = "" ;
               AV50lccbTi = 0 ;
               AV95lccbTi = A1171lccbT ;
               AV51lccbTi = "" ;
               AV68lccbOr = 0 ;
               AV69lccbOr = 0 ;
               AV70lccbOr = 0 ;
               AV76lccbOr = 0 ;
               AV72lccbIn = 0 ;
               AV48lccbSa = 0 ;
               AV71lccbDo = 0 ;
               AV72lccbIn = 0 ;
               AV90lccbFa = "00000000000" ;
               AV86GrupoC = "Y" ;
               AV30nSeq = (int)(AV30nSeq+1) ;
               /* Execute user subroutine: S135 */
               S135 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  pr_default.close(2);
                  pr_default.close(1);
                  getPrinter().GxEndPage() ;
                  /* Close printer file */
                  getPrinter().GxEndDocument() ;
                  endPrinter();
                  returnInSub = true;
                  if (true) return;
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( ( AV103GXLvl == 0 ) )
         {
            AV86GrupoC = "N" ;
         }
         pr_default.readNext(1);
      }
      pr_default.close(1);
      if ( ( AV101GXLvl == 0 ) )
      {
         h6M0( false, 18) ;
         getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Sem ocorr�ncias a registrar", 39, Gx_line+3, 193, Gx_line+17, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+18) ;
      }
      AV43LinhaA = "F" + GXutil.padl( GXutil.trim( AV54nSeqCa), (short)(7), "0") ;
      AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(270), " ") ;
      if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
      {
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(270)) ;
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      }
      else
      {
         AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(270), " ") ;
         AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
         AV66xmlWri.writeRawText(AV43LinhaA);
         AV66xmlWri.close();
      }
   }

   public void S125( ) throws ProcessInterruptedException
   {
      /* 'VERIFICALCCB' Routine */
      /* Using cursor R006M5 */
      pr_default.execute(3, new Object[] {AV20lccbEm, AV46SCETkt});
      while ( (pr_default.getStatus(3) != 101) )
      {
         A1227lccbO = R006M5_A1227lccbO[0] ;
         A1228lccbF = R006M5_A1228lccbF[0] ;
         A1231lccbT = R006M5_A1231lccbT[0] ;
         A1150lccbE = R006M5_A1150lccbE[0] ;
         A1181lccbC = R006M5_A1181lccbC[0] ;
         n1181lccbC = R006M5_n1181lccbC[0] ;
         A1172lccbS = R006M5_A1172lccbS[0] ;
         n1172lccbS = R006M5_n1172lccbS[0] ;
         A1171lccbT = R006M5_A1171lccbT[0] ;
         n1171lccbT = R006M5_n1171lccbT[0] ;
         A1223lccbD = R006M5_A1223lccbD[0] ;
         A1224lccbC = R006M5_A1224lccbC[0] ;
         A1226lccbA = R006M5_A1226lccbA[0] ;
         A1168lccbI = R006M5_A1168lccbI[0] ;
         n1168lccbI = R006M5_n1168lccbI[0] ;
         A1225lccbC = R006M5_A1225lccbC[0] ;
         A1169lccbD = R006M5_A1169lccbD[0] ;
         n1169lccbD = R006M5_n1169lccbD[0] ;
         A1184lccbS = R006M5_A1184lccbS[0] ;
         n1184lccbS = R006M5_n1184lccbS[0] ;
         A1170lccbI = R006M5_A1170lccbI[0] ;
         n1170lccbI = R006M5_n1170lccbI[0] ;
         A1222lccbI = R006M5_A1222lccbI[0] ;
         A1163lccbP = R006M5_A1163lccbP[0] ;
         n1163lccbP = R006M5_n1163lccbP[0] ;
         A1514lccbF = R006M5_A1514lccbF[0] ;
         A1515lccbC = R006M5_A1515lccbC[0] ;
         A1178lccbO = R006M5_A1178lccbO[0] ;
         n1178lccbO = R006M5_n1178lccbO[0] ;
         A1175lccbO = R006M5_A1175lccbO[0] ;
         n1175lccbO = R006M5_n1175lccbO[0] ;
         A1176lccbO = R006M5_A1176lccbO[0] ;
         n1176lccbO = R006M5_n1176lccbO[0] ;
         A1177lccbO = R006M5_A1177lccbO[0] ;
         n1177lccbO = R006M5_n1177lccbO[0] ;
         A1181lccbC = R006M5_A1181lccbC[0] ;
         n1181lccbC = R006M5_n1181lccbC[0] ;
         A1172lccbS = R006M5_A1172lccbS[0] ;
         n1172lccbS = R006M5_n1172lccbS[0] ;
         A1171lccbT = R006M5_A1171lccbT[0] ;
         n1171lccbT = R006M5_n1171lccbT[0] ;
         A1168lccbI = R006M5_A1168lccbI[0] ;
         n1168lccbI = R006M5_n1168lccbI[0] ;
         A1169lccbD = R006M5_A1169lccbD[0] ;
         n1169lccbD = R006M5_n1169lccbD[0] ;
         A1184lccbS = R006M5_A1184lccbS[0] ;
         n1184lccbS = R006M5_n1184lccbS[0] ;
         A1170lccbI = R006M5_A1170lccbI[0] ;
         n1170lccbI = R006M5_n1170lccbI[0] ;
         A1163lccbP = R006M5_A1163lccbP[0] ;
         n1163lccbP = R006M5_n1163lccbP[0] ;
         A1514lccbF = R006M5_A1514lccbF[0] ;
         A1515lccbC = R006M5_A1515lccbC[0] ;
         A1178lccbO = R006M5_A1178lccbO[0] ;
         n1178lccbO = R006M5_n1178lccbO[0] ;
         A1175lccbO = R006M5_A1175lccbO[0] ;
         n1175lccbO = R006M5_n1175lccbO[0] ;
         A1176lccbO = R006M5_A1176lccbO[0] ;
         n1176lccbO = R006M5_n1176lccbO[0] ;
         A1177lccbO = R006M5_A1177lccbO[0] ;
         n1177lccbO = R006M5_n1177lccbO[0] ;
         AV47lccbCu = A1181lccbC ;
         AV48lccbSa = A1172lccbS ;
         AV50lccbTi = A1171lccbT ;
         AV52lccbDa = A1223lccbD ;
         AV55lccbCC = A1224lccbC ;
         AV56lccbAp = A1226lccbA ;
         AV57lccbIn = A1168lccbI ;
         AV61lccbCC = A1225lccbC ;
         AV71lccbDo = A1169lccbD ;
         AV64lccbst = A1184lccbS ;
         AV72lccbIn = A1170lccbI ;
         AV83lccbIA = A1222lccbI ;
         AV88lccbPl = A1163lccbP ;
         AV90lccbFa = GXutil.strReplace( GXutil.trim( GXutil.str( A1514lccbF, 14, 2)), ".", "") ;
         AV92lccbCa = GXutil.strReplace( GXutil.trim( GXutil.str( A1515lccbC, 14, 2)), ".", "") ;
         AV68lccbOr = A1178lccbO ;
         AV69lccbOr = A1175lccbO ;
         AV70lccbOr = A1176lccbO ;
         AV76lccbOr = A1177lccbO ;
         pr_default.readNext(3);
      }
      pr_default.close(3);
      AV84CCEnc = AV61lccbCC ;
      AV81Atribu = AV61lccbCC ;
      GXv_svchar2[0] = AV82Result ;
      new pcrypto(remoteHandle, context).execute( AV81Atribu, "D", GXv_svchar2) ;
      rreti1022rpt.this.AV82Result = GXv_svchar2[0] ;
      AV61lccbCC = AV82Result ;
   }

   public void S135( ) throws ProcessInterruptedException
   {
      /* 'IMPRIMELINHA' Routine */
      if ( ( GXutil.strcmp(AV85SCEFop, "E") == 0 ) )
      {
         AV49lccbSa = AV93SCEFPA ;
         AV78lccbDo = "" ;
         AV58lccbIn = "" ;
         AV79lccbIn = "" ;
      }
      AV54nSeqCa = GXutil.trim( GXutil.str( AV30nSeq, 10, 0)) ;
      AV43LinhaA = "6" ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV54nSeqCa), (short)(7), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV45Empres), (short)(3), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV44IATA), (short)(7), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.space( (short)(10)) ;
      AV43LinhaA = AV43LinhaA + GXutil.space( (short)(7)) ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV47lccbCu), (short)(3), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV90lccbFa), (short)(11), "0") ;
      AV43LinhaA = AV43LinhaA + "00000000000" ;
      AV43LinhaA = AV43LinhaA + GXutil.space( (short)(8)) ;
      AV43LinhaA = AV43LinhaA + "00000000000" ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV46SCETkt), (short)(10), "0") ;
      AV43LinhaA = AV43LinhaA + "0000" ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( "N", (short)(3), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.space( (short)(8)) ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV53lccbDa), (short)(8), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV55lccbCC), (short)(2), " ") ;
      AV43LinhaA = AV43LinhaA + ((GXutil.strcmp(AV85SCEFop, "E")==0) ? GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV48lccbSa, 14, 2)), ".", ""), (short)(11), "0") : GXutil.padl( GXutil.trim( AV49lccbSa), (short)(11), "0")) ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV92lccbCa), (short)(11), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV58lccbIn), (short)(2), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV79lccbIn), (short)(11), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV59SCETpE), (short)(3), "0") ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV60SCETex), (short)(40), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV61lccbCC), (short)(20), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.substring( GXutil.trim( AV56lccbAp), 1, 6), (short)(6), " ") ;
      AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV88lccbPl), (short)(6), " ") ;
      AV43LinhaA = AV43LinhaA + "DB" ;
      if ( ( GXutil.strcmp(AV85SCEFop, "W") == 0 ) )
      {
         AV69lccbOr = (double)(AV70lccbOr+AV76lccbOr) ;
         AV43LinhaA = AV43LinhaA + GXutil.trim( AV85SCEFop) ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV68lccbOr, 14, 2)), ".", ""), (short)(11), "0") ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV69lccbOr, 14, 2)), ".", ""), (short)(11), "0") ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV70lccbOr, 14, 2)), ".", ""), (short)(11), "0") ;
         AV73Indica = "Aviso:" ;
      }
      else if ( ( GXutil.strcmp(AV85SCEFop, "E") == 0 ) )
      {
         AV71lccbDo = (double)(AV72lccbIn+AV50lccbTi) ;
         AV43LinhaA = AV43LinhaA + GXutil.trim( AV85SCEFop) ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV48lccbSa, 14, 2)), ".", ""), (short)(11), "0") ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV71lccbDo, 14, 2)), ".", ""), (short)(11), "0") ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( GXutil.str( AV72lccbIn, 14, 2)), ".", ""), (short)(11), "0") ;
         AV73Indica = "Erro:" ;
      }
      if ( ( ( GXutil.strcmp(AV64lccbst, "NOSUB") == 0 ) ) || ( ( GXutil.strcmp(AV85SCEFop, "W") == 0 ) ) || ( ( GXutil.strcmp(AV85SCEFop, "E") == 0 ) ) || ( ( GXutil.strcmp(AV86GrupoC, "Y") == 0 ) ) )
      {
         if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
         {
            AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(270)) ;
            AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         }
         else
         {
            AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
            AV66xmlWri.writeRawText(AV43LinhaA);
         }
         AV33s1 = GXutil.left( AV60SCETex, 80) ;
         AV34s2 = GXutil.substring( AV60SCETex, 81, 80) ;
         h6M0( false, 44) ;
         getPrinter().GxDrawLine(7, Gx_line+42, 1002, Gx_line+42, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV46SCETkt, "XXXXXXXXXX")), 82, Gx_line+5, 131, Gx_line+19, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1307SCEIa, "@!")), 82, Gx_line+26, 136, Gx_line+40, 0+256) ;
         getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("C�d. IATA", 8, Gx_line+26, 59, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText("Documento", 8, Gx_line+5, 70, Gx_line+19, 0+256) ;
         getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 255, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV73Indica, "XXXXXXX")), 149, Gx_line+3, 218, Gx_line+17, 0+256) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV33s1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 282, Gx_line+3, 821, Gx_line+19, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV34s2, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 282, Gx_line+24, 821, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1310SCETp, "XXX")), 232, Gx_line+3, 270, Gx_line+19, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+44) ;
      }
   }

   public void h6M0( boolean bFoot ,
                     int Inc )
   {
      /* Skip the required number of lines */
      while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
      {
         if ( ( Gx_line + Inc >= P_lines ) )
         {
            if ( ( Gx_page > 0 ) )
            {
               /* Print footers */
               Gx_line = P_lines ;
               getPrinter().GxDrawLine(6, Gx_line+7, 1001, Gx_line+7, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 8, true, true, false, false, 0, 0, 0, 192, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("R2 Tecnologia", 911, Gx_line+10, 989, Gx_line+23, 0+256) ;
               Gx_OldLine = Gx_line ;
               Gx_line = (int)(Gx_line+24) ;
               if ( ! bFoot )
               {
                  getPrinter().GxEndPage() ;
               }
               if ( bFoot )
               {
                  return  ;
               }
            }
            ToSkip = 0 ;
            Gx_line = 0 ;
            Gx_page = (int)(Gx_page+1) ;
            /* Skip Margin Top Lines */
            Gx_line = (int)(Gx_line+(M_top*lineHeight)) ;
            /* Print headers */
            getPrinter().GxStartPage() ;
            getPrinter().setPage(Gx_page);
            getPrinter().GxDrawRect(6, Gx_line+3, 1001, Gx_line+73, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(localUtil.format( Gx_date, "99/99/9999"), 838, Gx_line+28, 891, Gx_line+42, 0+256) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( Gx_page, "ZZZZZ9")), 838, Gx_line+8, 873, Gx_line+22, 2+256) ;
            getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("P�GINA:", 779, Gx_line+8, 822, Gx_line+22, 0+256) ;
            getPrinter().GxDrawText("DATA:", 779, Gx_line+26, 809, Gx_line+40, 0+256) ;
            getPrinter().GxAttris("Arial", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("EMPRESA A�REA:", 14, Gx_line+53, 113, Gx_line+68, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("R2Tech Brasil", 14, Gx_line+6, 99, Gx_line+22, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV20lccbEm, "@!")), 124, Gx_line+52, 162, Gx_line+68, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV21EmpNom, "@!")), 187, Gx_line+52, 366, Gx_line+68, 0+256) ;
            getPrinter().GxAttris("MS Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("i1022", 14, Gx_line+27, 45, Gx_line+40, 0+256) ;
            getPrinter().GxAttris("MS Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("RELAT�RIO DE VENDAS A CR�DITO COM DISCREP�NCIAS ", 207, Gx_line+20, 717, Gx_line+40, 0+256) ;
            Gx_OldLine = Gx_line ;
            Gx_line = (int)(Gx_line+75) ;
            if (true) break;
         }
         else
         {
            PrtOffset = 0 ;
            Gx_line = (int)(Gx_line+1) ;
         }
         ToSkip = (int)(ToSkip-1) ;
      }
      getPrinter().setPage(Gx_page);
   }

   public void add_metrics( )
   {
      add_metrics0( ) ;
      add_metrics1( ) ;
      add_metrics2( ) ;
      add_metrics3( ) ;
   }

   public void add_metrics0( )
   {
      getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics1( )
   {
      getPrinter().setMetrics("MS Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics2( )
   {
      getPrinter().setMetrics("Arial", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 36, 48, 14, 36, 21, 64, 36, 36, 21, 64, 43, 21, 64, 48, 39, 48, 48, 14, 14, 21, 21, 22, 36, 64, 20, 64, 32, 21, 60, 48, 31, 43, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
   }

   public void add_metrics3( )
   {
      getPrinter().setMetrics("Arial", true, true, 58, 14, 72, 123,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 30, 35, 35, 55, 45, 14, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 21, 21, 37, 37, 37, 38, 61, 45, 45, 45, 45, 42, 38, 49, 45, 17, 35, 45, 38, 52, 45, 49, 42, 49, 45, 42, 38, 45, 42, 59, 42, 42, 38, 21, 18, 23, 37, 35, 21, 35, 38, 35, 38, 35, 21, 38, 38, 18, 18, 35, 18, 56, 38, 38, 38, 38, 25, 35, 21, 38, 35, 49, 35, 35, 32, 25, 17, 25, 37, 47, 35, 47, 18, 35, 32, 63, 35, 35, 21, 63, 42, 21, 63, 47, 38, 47, 47, 17, 18, 32, 32, 22, 35, 64, 21, 63, 35, 21, 59, 47, 32, 42, 18, 21, 36, 35, 35, 35, 17, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 21, 21, 36, 35, 21, 21, 21, 23, 35, 53, 53, 53, 38, 45, 45, 45, 45, 45, 45, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 35, 35, 35, 35, 35, 18, 18, 18, 18, 38, 38, 38, 38, 38, 38, 38, 35, 38, 38, 38, 38, 38, 35, 38, 35}) ;
   }

   protected int getOutputType( )
   {
      return OUTPUT_PDF;
   }

   protected void cleanup( )
   {
      this.aP0[0] = rreti1022rpt.this.AV20lccbEm;
      this.aP1[0] = rreti1022rpt.this.AV21EmpNom;
      this.aP2[0] = rreti1022rpt.this.AV22FilePD;
      this.aP3[0] = rreti1022rpt.this.AV23FileTX;
      this.aP4[0] = rreti1022rpt.this.AV31DebugM;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      M_top = 0 ;
      M_bot = 0 ;
      Gx_line = 0 ;
      ToSkip = 0 ;
      PrtOffset = 0 ;
      AV65CarFim = "" ;
      AV32i = (short)(0) ;
      AV27sOutpu = "" ;
      GXt_char1 = "" ;
      AV29lccbSu = GXutil.resetTime( GXutil.nullDate() );
      Gx_date = GXutil.nullDate() ;
      AV35Decisa = "" ;
      AV36Decisa = "" ;
      AV37Decisa = "" ;
      AV38sDecis = "" ;
      AV39sDecis = "" ;
      AV40sDecis = "" ;
      scmdbuf = "" ;
      R006M2_A1147lccbE = new String[] {""} ;
      R006M2_n1147lccbE = new boolean[] {false} ;
      R006M2_A1150lccbE = new String[] {""} ;
      R006M2_A1166lccbP = new String[] {""} ;
      R006M2_A1165lccbD = new String[] {""} ;
      R006M2_n1165lccbD = new boolean[] {false} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      A1166lccbP = "" ;
      A1165lccbD = "" ;
      n1165lccbD = false ;
      AV74Decisa = "" ;
      AV75SDecis = "" ;
      Gx_OldLine = 0 ;
      returnInSub = false ;
      A1310SCETp = "" ;
      A1307SCEIa = "" ;
      AV24FileNo = 0 ;
      AV66xmlWri = new com.genexus.xml.XMLWriter();
      AV63Date = GXutil.resetTime( GXutil.nullDate() );
      AV43LinhaA = "" ;
      AV87ICSI_C = 0 ;
      AV30nSeq = 0 ;
      AV101GXLvl = (byte)(0) ;
      R006M3_A1312SCEId = new long[1] ;
      R006M3_A1301SCEAi = new String[] {""} ;
      R006M3_n1301SCEAi = new boolean[] {false} ;
      R006M3_A1306SCEAP = new String[] {""} ;
      R006M3_n1306SCEAP = new boolean[] {false} ;
      R006M3_A1310SCETp = new String[] {""} ;
      R006M3_n1310SCETp = new boolean[] {false} ;
      R006M3_A1309SCEFo = new String[] {""} ;
      R006M3_n1309SCEFo = new boolean[] {false} ;
      R006M3_A1299SCEDa = new java.util.Date[] {GXutil.nullDate()} ;
      R006M3_n1299SCEDa = new boolean[] {false} ;
      R006M3_A1307SCEIa = new String[] {""} ;
      R006M3_n1307SCEIa = new boolean[] {false} ;
      R006M3_A1305SCETe = new String[] {""} ;
      R006M3_n1305SCETe = new boolean[] {false} ;
      R006M3_A1517SCEFP = new double[1] ;
      R006M3_n1517SCEFP = new boolean[] {false} ;
      R006M3_A1518SCEPa = new double[1] ;
      R006M3_n1518SCEPa = new boolean[] {false} ;
      R006M3_A1298SCETk = new String[] {""} ;
      R006M3_n1298SCETk = new boolean[] {false} ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      n1310SCETp = false ;
      A1309SCEFo = "" ;
      n1309SCEFo = false ;
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      n1299SCEDa = false ;
      n1307SCEIa = false ;
      A1305SCETe = "" ;
      n1305SCETe = false ;
      A1517SCEFP = 0 ;
      n1517SCEFP = false ;
      A1518SCEPa = 0 ;
      n1518SCEPa = false ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      AV45Empres = "" ;
      AV44IATA = "" ;
      AV46SCETkt = "" ;
      AV59SCETpE = "" ;
      AV60SCETex = "" ;
      AV85SCEFop = "" ;
      AV93SCEFPA = "" ;
      AV102Scepa = "" ;
      AV94SCEPar = 0 ;
      AV80lccbFa = 0 ;
      AV68lccbOr = 0 ;
      AV48lccbSa = 0 ;
      AV50lccbTi = 0 ;
      AV77lccbFa = "" ;
      AV78lccbDo = "" ;
      AV71lccbDo = 0 ;
      AV79lccbIn = "" ;
      AV72lccbIn = 0 ;
      AV49lccbSa = "" ;
      AV51lccbTi = "" ;
      AV53lccbDa = "" ;
      AV52lccbDa = GXutil.nullDate() ;
      AV58lccbIn = "" ;
      AV57lccbIn = (short)(0) ;
      AV103GXLvl = (byte)(0) ;
      AV83lccbIA = "" ;
      AV55lccbCC = "" ;
      AV84CCEnc = "" ;
      AV56lccbAp = "" ;
      R006M4_A1232lccbT = new String[] {""} ;
      R006M4_A1227lccbO = new String[] {""} ;
      R006M4_A1228lccbF = new String[] {""} ;
      R006M4_A1231lccbT = new String[] {""} ;
      R006M4_A1226lccbA = new String[] {""} ;
      R006M4_A1225lccbC = new String[] {""} ;
      R006M4_A1224lccbC = new String[] {""} ;
      R006M4_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      R006M4_A1222lccbI = new String[] {""} ;
      R006M4_A1150lccbE = new String[] {""} ;
      R006M4_A1171lccbT = new double[1] ;
      R006M4_n1171lccbT = new boolean[] {false} ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      A1231lccbT = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      AV95lccbTi = 0 ;
      AV69lccbOr = 0 ;
      AV70lccbOr = 0 ;
      AV76lccbOr = 0 ;
      AV90lccbFa = "" ;
      AV86GrupoC = "" ;
      AV54nSeqCa = "" ;
      R006M5_A1232lccbT = new String[] {""} ;
      R006M5_A1227lccbO = new String[] {""} ;
      R006M5_A1228lccbF = new String[] {""} ;
      R006M5_A1231lccbT = new String[] {""} ;
      R006M5_A1150lccbE = new String[] {""} ;
      R006M5_A1181lccbC = new String[] {""} ;
      R006M5_n1181lccbC = new boolean[] {false} ;
      R006M5_A1172lccbS = new double[1] ;
      R006M5_n1172lccbS = new boolean[] {false} ;
      R006M5_A1171lccbT = new double[1] ;
      R006M5_n1171lccbT = new boolean[] {false} ;
      R006M5_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      R006M5_A1224lccbC = new String[] {""} ;
      R006M5_A1226lccbA = new String[] {""} ;
      R006M5_A1168lccbI = new short[1] ;
      R006M5_n1168lccbI = new boolean[] {false} ;
      R006M5_A1225lccbC = new String[] {""} ;
      R006M5_A1169lccbD = new double[1] ;
      R006M5_n1169lccbD = new boolean[] {false} ;
      R006M5_A1184lccbS = new String[] {""} ;
      R006M5_n1184lccbS = new boolean[] {false} ;
      R006M5_A1170lccbI = new double[1] ;
      R006M5_n1170lccbI = new boolean[] {false} ;
      R006M5_A1222lccbI = new String[] {""} ;
      R006M5_A1163lccbP = new String[] {""} ;
      R006M5_n1163lccbP = new boolean[] {false} ;
      R006M5_A1514lccbF = new double[1] ;
      R006M5_A1515lccbC = new double[1] ;
      R006M5_A1178lccbO = new double[1] ;
      R006M5_n1178lccbO = new boolean[] {false} ;
      R006M5_A1175lccbO = new double[1] ;
      R006M5_n1175lccbO = new boolean[] {false} ;
      R006M5_A1176lccbO = new double[1] ;
      R006M5_n1176lccbO = new boolean[] {false} ;
      R006M5_A1177lccbO = new double[1] ;
      R006M5_n1177lccbO = new boolean[] {false} ;
      A1181lccbC = "" ;
      n1181lccbC = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1169lccbD = 0 ;
      n1169lccbD = false ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1163lccbP = "" ;
      n1163lccbP = false ;
      A1514lccbF = 0 ;
      A1515lccbC = 0 ;
      A1178lccbO = 0 ;
      n1178lccbO = false ;
      A1175lccbO = 0 ;
      n1175lccbO = false ;
      A1176lccbO = 0 ;
      n1176lccbO = false ;
      A1177lccbO = 0 ;
      n1177lccbO = false ;
      AV47lccbCu = "" ;
      AV61lccbCC = "" ;
      AV64lccbst = "" ;
      AV88lccbPl = "" ;
      AV92lccbCa = "" ;
      AV81Atribu = "" ;
      AV82Result = "" ;
      GXv_svchar2 = new String [1] ;
      AV73Indica = "" ;
      AV33s1 = "" ;
      AV34s2 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new rreti1022rpt__default(),
         new Object[] {
             new Object[] {
            R006M2_A1147lccbE, R006M2_n1147lccbE, R006M2_A1150lccbE, R006M2_A1166lccbP, R006M2_A1165lccbD, R006M2_n1165lccbD
            }
            , new Object[] {
            R006M3_A1312SCEId, R006M3_A1301SCEAi, R006M3_n1301SCEAi, R006M3_A1306SCEAP, R006M3_n1306SCEAP, R006M3_A1310SCETp, R006M3_n1310SCETp, R006M3_A1309SCEFo, R006M3_n1309SCEFo, R006M3_A1299SCEDa,
            R006M3_n1299SCEDa, R006M3_A1307SCEIa, R006M3_n1307SCEIa, R006M3_A1305SCETe, R006M3_n1305SCETe, R006M3_A1517SCEFP, R006M3_n1517SCEFP, R006M3_A1518SCEPa, R006M3_n1518SCEPa, R006M3_A1298SCETk,
            R006M3_n1298SCETk
            }
            , new Object[] {
            R006M4_A1232lccbT, R006M4_A1227lccbO, R006M4_A1228lccbF, R006M4_A1231lccbT, R006M4_A1226lccbA, R006M4_A1225lccbC, R006M4_A1224lccbC, R006M4_A1223lccbD, R006M4_A1222lccbI, R006M4_A1150lccbE,
            R006M4_A1171lccbT, R006M4_n1171lccbT
            }
            , new Object[] {
            R006M5_A1232lccbT, R006M5_A1227lccbO, R006M5_A1228lccbF, R006M5_A1231lccbT, R006M5_A1150lccbE, R006M5_A1181lccbC, R006M5_n1181lccbC, R006M5_A1172lccbS, R006M5_n1172lccbS, R006M5_A1171lccbT,
            R006M5_n1171lccbT, R006M5_A1223lccbD, R006M5_A1224lccbC, R006M5_A1226lccbA, R006M5_A1168lccbI, R006M5_n1168lccbI, R006M5_A1225lccbC, R006M5_A1169lccbD, R006M5_n1169lccbD, R006M5_A1184lccbS,
            R006M5_n1184lccbS, R006M5_A1170lccbI, R006M5_n1170lccbI, R006M5_A1222lccbI, R006M5_A1163lccbP, R006M5_n1163lccbP, R006M5_A1514lccbF, R006M5_A1515lccbC, R006M5_A1178lccbO, R006M5_n1178lccbO,
            R006M5_A1175lccbO, R006M5_n1175lccbO, R006M5_A1176lccbO, R006M5_n1176lccbO, R006M5_A1177lccbO, R006M5_n1177lccbO
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_line = 0 ;
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV101GXLvl ;
   private byte AV103GXLvl ;
   private short AV32i ;
   private short AV57lccbIn ;
   private short A1168lccbI ;
   private short Gx_err ;
   private int M_top ;
   private int M_bot ;
   private int Line ;
   private int ToSkip ;
   private int PrtOffset ;
   private int Gx_OldLine ;
   private int AV24FileNo ;
   private int AV87ICSI_C ;
   private int AV30nSeq ;
   private double A1517SCEFP ;
   private double A1518SCEPa ;
   private double AV94SCEPar ;
   private double AV80lccbFa ;
   private double AV68lccbOr ;
   private double AV48lccbSa ;
   private double AV50lccbTi ;
   private double AV71lccbDo ;
   private double AV72lccbIn ;
   private double A1171lccbT ;
   private double AV95lccbTi ;
   private double AV69lccbOr ;
   private double AV70lccbOr ;
   private double AV76lccbOr ;
   private double A1172lccbS ;
   private double A1169lccbD ;
   private double A1170lccbI ;
   private double A1514lccbF ;
   private double A1515lccbC ;
   private double A1178lccbO ;
   private double A1175lccbO ;
   private double A1176lccbO ;
   private double A1177lccbO ;
   private String AV20lccbEm ;
   private String AV21EmpNom ;
   private String AV22FilePD ;
   private String AV23FileTX ;
   private String AV31DebugM ;
   private String AV65CarFim ;
   private String AV27sOutpu ;
   private String GXt_char1 ;
   private String AV35Decisa ;
   private String AV36Decisa ;
   private String AV37Decisa ;
   private String AV38sDecis ;
   private String AV39sDecis ;
   private String AV40sDecis ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String A1166lccbP ;
   private String A1165lccbD ;
   private String AV74Decisa ;
   private String AV75SDecis ;
   private String A1310SCETp ;
   private String A1307SCEIa ;
   private String AV43LinhaA ;
   private String A1301SCEAi ;
   private String A1306SCEAP ;
   private String A1309SCEFo ;
   private String A1305SCETe ;
   private String A1298SCETk ;
   private String AV45Empres ;
   private String AV44IATA ;
   private String AV46SCETkt ;
   private String AV59SCETpE ;
   private String AV60SCETex ;
   private String AV85SCEFop ;
   private String AV102Scepa ;
   private String AV77lccbFa ;
   private String AV78lccbDo ;
   private String AV79lccbIn ;
   private String AV49lccbSa ;
   private String AV51lccbTi ;
   private String AV53lccbDa ;
   private String AV58lccbIn ;
   private String AV83lccbIA ;
   private String AV55lccbCC ;
   private String AV84CCEnc ;
   private String AV56lccbAp ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private String A1231lccbT ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String AV86GrupoC ;
   private String AV54nSeqCa ;
   private String A1181lccbC ;
   private String A1184lccbS ;
   private String A1163lccbP ;
   private String AV47lccbCu ;
   private String AV61lccbCC ;
   private String AV64lccbst ;
   private String AV88lccbPl ;
   private String AV81Atribu ;
   private String AV82Result ;
   private String AV73Indica ;
   private String AV33s1 ;
   private String AV34s2 ;
   private java.util.Date AV29lccbSu ;
   private java.util.Date AV63Date ;
   private java.util.Date A1299SCEDa ;
   private java.util.Date Gx_date ;
   private java.util.Date AV52lccbDa ;
   private java.util.Date A1223lccbD ;
   private boolean n1147lccbE ;
   private boolean n1165lccbD ;
   private boolean returnInSub ;
   private boolean n1301SCEAi ;
   private boolean n1306SCEAP ;
   private boolean n1310SCETp ;
   private boolean n1309SCEFo ;
   private boolean n1299SCEDa ;
   private boolean n1307SCEIa ;
   private boolean n1305SCETe ;
   private boolean n1517SCEFP ;
   private boolean n1518SCEPa ;
   private boolean n1298SCETk ;
   private boolean n1171lccbT ;
   private boolean n1181lccbC ;
   private boolean n1172lccbS ;
   private boolean n1168lccbI ;
   private boolean n1169lccbD ;
   private boolean n1184lccbS ;
   private boolean n1170lccbI ;
   private boolean n1163lccbP ;
   private boolean n1178lccbO ;
   private boolean n1175lccbO ;
   private boolean n1176lccbO ;
   private boolean n1177lccbO ;
   private String AV93SCEFPA ;
   private String AV90lccbFa ;
   private String AV92lccbCa ;
   private String GXv_svchar2[] ;
   private com.genexus.xml.XMLWriter AV66xmlWri ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
   private IDataStoreProvider pr_default ;
   private String[] R006M2_A1147lccbE ;
   private boolean[] R006M2_n1147lccbE ;
   private String[] R006M2_A1150lccbE ;
   private String[] R006M2_A1166lccbP ;
   private String[] R006M2_A1165lccbD ;
   private boolean[] R006M2_n1165lccbD ;
   private long[] R006M3_A1312SCEId ;
   private String[] R006M3_A1301SCEAi ;
   private boolean[] R006M3_n1301SCEAi ;
   private String[] R006M3_A1306SCEAP ;
   private boolean[] R006M3_n1306SCEAP ;
   private String[] R006M3_A1310SCETp ;
   private boolean[] R006M3_n1310SCETp ;
   private String[] R006M3_A1309SCEFo ;
   private boolean[] R006M3_n1309SCEFo ;
   private java.util.Date[] R006M3_A1299SCEDa ;
   private boolean[] R006M3_n1299SCEDa ;
   private String[] R006M3_A1307SCEIa ;
   private boolean[] R006M3_n1307SCEIa ;
   private String[] R006M3_A1305SCETe ;
   private boolean[] R006M3_n1305SCETe ;
   private double[] R006M3_A1517SCEFP ;
   private boolean[] R006M3_n1517SCEFP ;
   private double[] R006M3_A1518SCEPa ;
   private boolean[] R006M3_n1518SCEPa ;
   private String[] R006M3_A1298SCETk ;
   private boolean[] R006M3_n1298SCETk ;
   private String[] R006M4_A1232lccbT ;
   private String[] R006M4_A1227lccbO ;
   private String[] R006M4_A1228lccbF ;
   private String[] R006M4_A1231lccbT ;
   private String[] R006M4_A1226lccbA ;
   private String[] R006M4_A1225lccbC ;
   private String[] R006M4_A1224lccbC ;
   private java.util.Date[] R006M4_A1223lccbD ;
   private String[] R006M4_A1222lccbI ;
   private String[] R006M4_A1150lccbE ;
   private double[] R006M4_A1171lccbT ;
   private boolean[] R006M4_n1171lccbT ;
   private String[] R006M5_A1232lccbT ;
   private String[] R006M5_A1227lccbO ;
   private String[] R006M5_A1228lccbF ;
   private String[] R006M5_A1231lccbT ;
   private String[] R006M5_A1150lccbE ;
   private String[] R006M5_A1181lccbC ;
   private boolean[] R006M5_n1181lccbC ;
   private double[] R006M5_A1172lccbS ;
   private boolean[] R006M5_n1172lccbS ;
   private double[] R006M5_A1171lccbT ;
   private boolean[] R006M5_n1171lccbT ;
   private java.util.Date[] R006M5_A1223lccbD ;
   private String[] R006M5_A1224lccbC ;
   private String[] R006M5_A1226lccbA ;
   private short[] R006M5_A1168lccbI ;
   private boolean[] R006M5_n1168lccbI ;
   private String[] R006M5_A1225lccbC ;
   private double[] R006M5_A1169lccbD ;
   private boolean[] R006M5_n1169lccbD ;
   private String[] R006M5_A1184lccbS ;
   private boolean[] R006M5_n1184lccbS ;
   private double[] R006M5_A1170lccbI ;
   private boolean[] R006M5_n1170lccbI ;
   private String[] R006M5_A1222lccbI ;
   private String[] R006M5_A1163lccbP ;
   private boolean[] R006M5_n1163lccbP ;
   private double[] R006M5_A1514lccbF ;
   private double[] R006M5_A1515lccbC ;
   private double[] R006M5_A1178lccbO ;
   private boolean[] R006M5_n1178lccbO ;
   private double[] R006M5_A1175lccbO ;
   private boolean[] R006M5_n1175lccbO ;
   private double[] R006M5_A1176lccbO ;
   private boolean[] R006M5_n1176lccbO ;
   private double[] R006M5_A1177lccbO ;
   private boolean[] R006M5_n1177lccbO ;
}

final  class rreti1022rpt__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("R006M2", "SELECT T2.[lccbEmpEnab], T1.[lccbEmpCod], T1.[lccbProblemID], T1.[lccbDecActID] FROM ([LCCBEMPRESAPROBLEMA] T1 WITH (NOLOCK) INNER JOIN [LCCBEMP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod]) WHERE (T1.[lccbEmpCod] = ?) AND (T2.[lccbEmpEnab] = '1') ORDER BY T1.[lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("R006M3", "SELECT [SCEId], [SCEAirLine], [SCEAPP], [SCETpEvento], [SCEFopID], [SCEDate], [SCEIata], [SCEText], [SCEFPAM], [SCEParcela], [SCETkt] FROM [SCEVENTS] WITH (NOLOCK) WHERE ([SCEAirLine] = ? and [SCEDate] >= ?) AND ([SCEFopID] <> '') AND ([SCEFopID] <> 'L') AND (SUBSTRING([SCETpEvento], 1, 1) = '4' or SUBSTRING([SCETpEvento], 1, 1) = '9') AND ([SCEAPP] = 'ICSI') ORDER BY [SCEAirLine], [SCEDate], [SCETkt] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("R006M4", "SELECT T1.[lccbTRNC], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T1.[lccbAppCode], T1.[lccbCCNum], T1.[lccbCCard], T1.[lccbDate], T1.[lccbIATA], T1.[lccbEmpCod], T2.[lccbTip] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate] = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum] AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP]) WHERE (T1.[lccbEmpCod] = ? and T1.[lccbIATA] = ? and T1.[lccbDate] = ? and T1.[lccbCCard] = ? and T1.[lccbCCNum] = ? and T1.[lccbAppCode] = ?) AND (T1.[lccbTDNR] <> ?) ORDER BY T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbDate], T1.[lccbCCard], T1.[lccbCCNum], T1.[lccbAppCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("R006M5", "SELECT T1.[lccbTRNC], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T1.[lccbEmpCod], T2.[lccbCurrency], T2.[lccbSaleAmount], T2.[lccbTip], T1.[lccbDate], T1.[lccbCCard], T1.[lccbAppCode], T2.[lccbInstallments], T1.[lccbCCNum], T2.[lccbDownPayment], T2.[lccbStatus], T2.[lccbInstAmount], T1.[lccbIATA], T2.[lccbPlanCode], T2.[lccbFareAmount], T2.[lccbCashAmount], T2.[lccbOrgSaleAmount], T2.[lccbOrgDownPayment], T2.[lccbOrgInstAmount], T2.[lccbOrgTip] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate] = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum] AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP]) WHERE (T1.[lccbEmpCod] = ?) AND (T1.[lccbTDNR] = ?) ORDER BY T1.[lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 1 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getString(5, 5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[9])[0] = rslt.getGXDateTime(6) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(7, 11) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((String[]) buf[13])[0] = rslt.getString(8, 150) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((double[]) buf[15])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((double[]) buf[17])[0] = rslt.getDouble(10) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getString(11, 10) ;
               ((boolean[]) buf[20])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 19) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 2) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 7) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 3) ;
               ((double[]) buf[10])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 19) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 3) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((double[]) buf[7])[0] = rslt.getDouble(7) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((double[]) buf[9])[0] = rslt.getDouble(8) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(9) ;
               ((String[]) buf[12])[0] = rslt.getString(10, 2) ;
               ((String[]) buf[13])[0] = rslt.getString(11, 20) ;
               ((short[]) buf[14])[0] = rslt.getShort(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 44) ;
               ((double[]) buf[17])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getString(15, 8) ;
               ((boolean[]) buf[20])[0] = rslt.wasNull();
               ((double[]) buf[21])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[22])[0] = rslt.wasNull();
               ((String[]) buf[23])[0] = rslt.getString(17, 7) ;
               ((String[]) buf[24])[0] = rslt.getString(18, 20) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((double[]) buf[26])[0] = rslt.getDouble(19) ;
               ((double[]) buf[27])[0] = rslt.getDouble(20) ;
               ((double[]) buf[28])[0] = rslt.getDouble(21) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((double[]) buf[30])[0] = rslt.getDouble(22) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((double[]) buf[32])[0] = rslt.getDouble(23) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((double[]) buf[34])[0] = rslt.getDouble(24) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 10);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 10);
               break;
      }
   }

}


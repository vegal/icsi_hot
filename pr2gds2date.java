/*
               File: R2GDS2Date
        Description: Transforma string de data tipo GDS em data
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: March 27, 2014 11:39:53.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2gds2date extends GXProcedure
{
   public pr2gds2date( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2gds2date.class ), "" );
   }

   public pr2gds2date( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( java.util.Date[] aP0 ,
                        String[] aP1 ,
                        java.util.Date[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( java.util.Date[] aP0 ,
                             String[] aP1 ,
                             java.util.Date[] aP2 )
   {
      pr2gds2date.this.AV8BaseDat = aP0[0];
      this.aP0 = aP0;
      pr2gds2date.this.AV9sInput = aP1[0];
      this.aP1 = aP1;
      pr2gds2date.this.AV10dOutpu = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV16sBaseD ;
      GXv_date2[0] = AV8BaseDat ;
      GXv_int3[0] = (byte)(1) ;
      GXv_char4[0] = GXt_char1 ;
      new pr2date2string(remoteHandle, context).execute( GXv_date2, GXv_int3, GXv_char4) ;
      pr2gds2date.this.AV8BaseDat = GXv_date2[0] ;
      pr2gds2date.this.GXt_char1 = GXv_char4[0] ;
      AV16sBaseD = GXt_char1 ;
      AV11dia = (byte)(GXutil.val( GXutil.left( AV9sInput, 2), ".")) ;
      if ( ( AV11dia < 1 ) || ( AV11dia > 31 ) )
      {
         AV11dia = (byte)(1) ;
      }
      AV15mTable = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC" ;
      AV13s = GXutil.upper( GXutil.substring( AV9sInput, 3, 3)) ;
      AV14i = GXutil.strSearch( AV15mTable, AV13s, 1) ;
      if ( ( AV14i > 0 ) )
      {
         AV12mes = (byte)((AV14i-1)/ (double) (3)+1) ;
      }
      else
      {
         AV12mes = (byte)(1) ;
      }
      AV13s = GXutil.str( AV12mes, 2, 0) + GXutil.str( AV11dia, 2, 0) ;
      AV13s = GXutil.strReplace( AV13s, " ", "0") ;
      AV17ano = (short)(GXutil.val( GXutil.substring( AV16sBaseD, 1, 4), ".")) ;
      if ( ( GXutil.strcmp(AV13s, GXutil.substring( AV16sBaseD, 5, 4)) < 0 ) )
      {
         AV17ano = (short)(AV17ano+1) ;
      }
      AV10dOutpu = localUtil.ymdtod( AV17ano, AV12mes, AV11dia) ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2gds2date.this.AV8BaseDat;
      this.aP1[0] = pr2gds2date.this.AV9sInput;
      this.aP2[0] = pr2gds2date.this.AV10dOutpu;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV16sBaseD = "" ;
      GXt_char1 = "" ;
      GXv_date2 = new java.util.Date [1] ;
      GXv_int3 = new byte [1] ;
      GXv_char4 = new String [1] ;
      AV11dia = (byte)(0) ;
      AV15mTable = "" ;
      AV13s = "" ;
      AV14i = 0 ;
      AV12mes = (byte)(0) ;
      AV17ano = (short)(0) ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte GXv_int3[] ;
   private byte AV11dia ;
   private byte AV12mes ;
   private short AV17ano ;
   private short Gx_err ;
   private int AV14i ;
   private String AV9sInput ;
   private String AV16sBaseD ;
   private String GXt_char1 ;
   private String GXv_char4[] ;
   private String AV15mTable ;
   private String AV13s ;
   private java.util.Date AV8BaseDat ;
   private java.util.Date AV10dOutpu ;
   private java.util.Date GXv_date2[] ;
   private java.util.Date[] aP0 ;
   private String[] aP1 ;
   private java.util.Date[] aP2 ;
}


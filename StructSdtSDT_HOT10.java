
public final  class StructSdtSDT_HOT10 implements Cloneable, java.io.Serializable
{
   public StructSdtSDT_HOT10( )
   {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      cal.set(1, 0, 1, 0, 0, 0);
      cal.set(java.util.Calendar.MILLISECOND, 0);
      gxTv_SdtSDT_HOT10_Isoc = "" ;
      gxTv_SdtSDT_HOT10_Ciacod = "" ;
      gxTv_SdtSDT_HOT10_Per_name = "" ;
      gxTv_SdtSDT_HOT10_Code = "" ;
      gxTv_SdtSDT_HOT10_Iata = "" ;
      gxTv_SdtSDT_HOT10_Num_bil = "" ;
      gxTv_SdtSDT_HOT10_Tipo_vend = "" ;
      gxTv_SdtSDT_HOT10_Data = cal.getTime() ;
      gxTv_SdtSDT_HOT10_H10recid = "" ;
      gxTv_SdtSDT_HOT10_H10field = "" ;
      gxTv_SdtSDT_HOT10_H10seq = "" ;
      gxTv_SdtSDT_HOT10_H10type = "" ;
      gxTv_SdtSDT_HOT10_H10content = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIsoc( )
   {
      return gxTv_SdtSDT_HOT10_Isoc ;
   }

   public void setIsoc( String value )
   {
      gxTv_SdtSDT_HOT10_Isoc = value ;
      return  ;
   }

   public String getCiacod( )
   {
      return gxTv_SdtSDT_HOT10_Ciacod ;
   }

   public void setCiacod( String value )
   {
      gxTv_SdtSDT_HOT10_Ciacod = value ;
      return  ;
   }

   public String getPer_name( )
   {
      return gxTv_SdtSDT_HOT10_Per_name ;
   }

   public void setPer_name( String value )
   {
      gxTv_SdtSDT_HOT10_Per_name = value ;
      return  ;
   }

   public String getCode( )
   {
      return gxTv_SdtSDT_HOT10_Code ;
   }

   public void setCode( String value )
   {
      gxTv_SdtSDT_HOT10_Code = value ;
      return  ;
   }

   public String getIata( )
   {
      return gxTv_SdtSDT_HOT10_Iata ;
   }

   public void setIata( String value )
   {
      gxTv_SdtSDT_HOT10_Iata = value ;
      return  ;
   }

   public String getNum_bil( )
   {
      return gxTv_SdtSDT_HOT10_Num_bil ;
   }

   public void setNum_bil( String value )
   {
      gxTv_SdtSDT_HOT10_Num_bil = value ;
      return  ;
   }

   public String getTipo_vend( )
   {
      return gxTv_SdtSDT_HOT10_Tipo_vend ;
   }

   public void setTipo_vend( String value )
   {
      gxTv_SdtSDT_HOT10_Tipo_vend = value ;
      return  ;
   }

   public java.util.Date getData( )
   {
      return gxTv_SdtSDT_HOT10_Data ;
   }

   public void setData( java.util.Date value )
   {
      gxTv_SdtSDT_HOT10_Data = value ;
      return  ;
   }

   public String getH10recid( )
   {
      return gxTv_SdtSDT_HOT10_H10recid ;
   }

   public void setH10recid( String value )
   {
      gxTv_SdtSDT_HOT10_H10recid = value ;
      return  ;
   }

   public String getH10field( )
   {
      return gxTv_SdtSDT_HOT10_H10field ;
   }

   public void setH10field( String value )
   {
      gxTv_SdtSDT_HOT10_H10field = value ;
      return  ;
   }

   public String getH10seq( )
   {
      return gxTv_SdtSDT_HOT10_H10seq ;
   }

   public void setH10seq( String value )
   {
      gxTv_SdtSDT_HOT10_H10seq = value ;
      return  ;
   }

   public String getH10type( )
   {
      return gxTv_SdtSDT_HOT10_H10type ;
   }

   public void setH10type( String value )
   {
      gxTv_SdtSDT_HOT10_H10type = value ;
      return  ;
   }

   public String getH10content( )
   {
      return gxTv_SdtSDT_HOT10_H10content ;
   }

   public void setH10content( String value )
   {
      gxTv_SdtSDT_HOT10_H10content = value ;
      return  ;
   }

   protected String gxTv_SdtSDT_HOT10_Isoc ;
   protected String gxTv_SdtSDT_HOT10_Tipo_vend ;
   protected String gxTv_SdtSDT_HOT10_H10recid ;
   protected String gxTv_SdtSDT_HOT10_H10field ;
   protected String gxTv_SdtSDT_HOT10_H10seq ;
   protected String gxTv_SdtSDT_HOT10_H10type ;
   protected String gxTv_SdtSDT_HOT10_Ciacod ;
   protected String gxTv_SdtSDT_HOT10_Per_name ;
   protected String gxTv_SdtSDT_HOT10_Code ;
   protected String gxTv_SdtSDT_HOT10_Iata ;
   protected String gxTv_SdtSDT_HOT10_Num_bil ;
   protected String gxTv_SdtSDT_HOT10_H10content ;
   protected java.util.Date gxTv_SdtSDT_HOT10_Data ;
}


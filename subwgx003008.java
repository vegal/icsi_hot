import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx003008 extends GXSubfileElement
{
   private String ConfigID ;
   public String getConfigID( )
   {
      return ConfigID ;
   }

   public void setConfigID( String value )
   {
      ConfigID = value;
   }

   public void clear( )
   {
      ConfigID = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getConfigID().compareTo(((subwgx003008) element).getConfigID()) ;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getConfigID(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getConfigID() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getConfigID()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setConfigID(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setConfigID(((subwgx003008) element).getConfigID());
               return;
      }
   }

}


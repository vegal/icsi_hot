/*
               File: RETHandling
        Description: RET Handling (POO+RFN+SPEC) (Main)
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:3.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class prethandling extends GXProcedure
{
   public prethandling( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( prethandling.class ), "" );
   }

   public prethandling( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      prethandling.this.AV17DebugM = aP0[0];
      this.aP0 = aP0;
      prethandling.this.AV24FileSo = aP1[0];
      this.aP1 = aP1;
      prethandling.this.AV223Menu = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV84Versao = "10022" ;
      AV17DebugM = GXutil.trim( GXutil.upper( AV17DebugM)) ;
      context.msgStatus( "RETHandling (POO,RFN,SPECCOM) - Version "+AV84Versao );
      context.msgStatus( "  Running mode: ["+AV17DebugM+"] - Started at "+GXutil.time( ) );
      if ( ( GXutil.strSearch( AV17DebugM, "DONTSPLITRFN", 1) > 0 ) )
      {
         AV64SplitR = (byte)(0) ;
      }
      else
      {
         AV64SplitR = (byte)(1) ;
      }
      if ( ( GXutil.strSearch( AV17DebugM, "VERBOSE", 1) > 0 ) )
      {
         AV189Verbo = (byte)(1) ;
      }
      else
      {
         AV189Verbo = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV17DebugM, "NORFNCOMM", 1) > 0 ) )
      {
         AV214FlagC = (byte)(0) ;
      }
      else
      {
         AV214FlagC = (byte)(1) ;
      }
      /* Execute user subroutine: S1147 */
      S1147 (); /********  */
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      if ( ( GXutil.strcmp(AV224REVN , "220") == 0 ) )
      {
         context.msgStatus( "V220" );
         GXv_char3[0] = AV24FileSo ;
         GXv_char4[0] = AV17DebugM ;
         new preticsi_d220(remoteHandle, context).execute( GXv_char3, GXv_char4) ;
         prethandling.this.AV24FileSo = GXv_char3[0] ;
         prethandling.this.AV17DebugM = GXv_char4[0] ;
      }
      else
      {
         context.msgStatus( "V203" );
         GXv_char4[0] = AV24FileSo ;
         GXv_char3[0] = AV17DebugM ;
         new preticsi(remoteHandle, context).execute( GXv_char4, GXv_char3) ;
         prethandling.this.AV24FileSo = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
      }
      cleanup();
   }

   public void S1147( )
   {
      /* 'MAIN' Routine */
      GXt_char2 = AV22FileNa ;
      GXv_char4[0] = AV24FileSo ;
      GXv_char3[0] = GXt_char2 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char4, GXv_char3) ;
      prethandling.this.AV24FileSo = GXv_char4[0] ;
      prethandling.this.GXt_char2 = GXv_char3[0] ;
      AV22FileNa = GXt_char2 ;
      AV23FileNo = context.getSessionInstances().getDelimitedFiles().dfropen( AV24FileSo, 256, "^", "\"", "") ;
      /* Execute user subroutine: S121 */
      S121 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV23FileNo = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
   }

   public void S121( )
   {
      /* 'LOADHOTTEMP' Routine */
      AV10AppCod = "RETClone" ;
      if ( ( GXutil.strSearch( AV17DebugM, "LOADONLY", 1) == 0 ) )
      {
         /* Execute user subroutine: S131 */
         S131 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      /* Optimized DELETE. */
      /* Using cursor P005U2 */
      pr_default.execute(0);
      /* End optimized DELETE. */
      Application.commit(context, remoteHandle, "DEFAULT", "prethandling");
      AV86vHntSe = 0 ;
      AV28Flag_I = (byte)(0) ;
      AV81TRNN_O = "000000" ;
      context.msgStatus( "  Reading File "+AV22FileNa+"..." );
      AV224REVN = "" ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char4[0] = AV40Linha ;
         GXt_int5 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char4, (short)(258)) ;
         AV40Linha = GXv_char4[0] ;
         AV23FileNo = GXt_int5 ;
         if ( ( AV28Flag_I == 0 ) )
         {
            /* Execute user subroutine: S141 */
            S141 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         if ( ( AV28Flag_I == 1 ) )
         {
            /* Execute user subroutine: S151 */
            S151 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV26FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Linha, (short)(255)) ;
            AV26FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         }
      }
      Application.commit(context, remoteHandle, "DEFAULT", "prethandling");
      if ( ( GXutil.strSearch( AV17DebugM, "HOLDPROC", 1) > 0 ) && ( AV28Flag_I == 1 ) )
      {
         AV10AppCod = "RETClone" ;
         AV199cnt_H = 0 ;
         /* Using cursor P005U3 */
         pr_default.execute(1, new Object[] {AV15CRS, AV15CRS});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1440RETHe = P005U3_A1440RETHe[0] ;
            A1439RETHe = P005U3_A1439RETHe[0] ;
            n1439RETHe = P005U3_n1439RETHe[0] ;
            A1441RETHe = P005U3_A1441RETHe[0] ;
            AV40Linha = A1439RETHe ;
            AV199cnt_H = (int)(AV199cnt_H+1) ;
            /* Execute user subroutine: S151 */
            S151 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            /* Using cursor P005U4 */
            pr_default.execute(2, new Object[] {A1440RETHe, new Integer(A1441RETHe)});
            pr_default.readNext(1);
         }
         pr_default.close(1);
         AV86vHntSe = (int)(AV86vHntSe+1) ;
         AV63sHntSe = GXutil.padl( GXutil.trim( GXutil.str( AV86vHntSe, 10, 0)), (short)(7), "0") + "5" ;
         AV38IT0Zpo = GXutil.trim( AV63sHntSe) ;
         /*
            INSERT RECORD ON TABLE RETSPECTEMP

         */
         A997HntSeq = GXutil.trim( AV63sHntSe) ;
         A989HntRec = GXutil.substring( AV198IT0Z_, 1, 1) ;
         n989HntRec = false ;
         A993HntUsu = "RETClone" ;
         n993HntUsu = false ;
         A994HntLin = AV198IT0Z_ ;
         n994HntLin = false ;
         /* Using cursor P005U5 */
         pr_default.execute(3, new Object[] {A997HntSeq, new Byte(A998HntSub), new Boolean(n989HntRec), A989HntRec, new Boolean(n993HntUsu), A993HntUsu, new Boolean(n994HntLin), A994HntLin});
         if ( (pr_default.getStatus(3) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
      }
      Application.commit(context, remoteHandle, "DEFAULT", "prethandling");
      if ( ( AV28Flag_I == 2 ) )
      {
         AV26FileNo = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      }
      AV183Commi = (short)(0) ;
      if ( ( AV28Flag_I == 1 ) )
      {
         context.msgStatus( "  "+GXutil.trim( GXutil.str( AV188cnt_n, 10, 0))+" Refunds without Historical Data" );
         context.msgStatus( "  "+GXutil.trim( GXutil.str( AV186cnt_C, 10, 0))+" Exchange Rates not configured, using last date" );
         context.msgStatus( "  "+GXutil.trim( GXutil.str( AV187cnt_C, 10, 0))+" Exchange Rates not configured, using as local currency" );
         context.msgStatus( "  "+GXutil.trim( GXutil.str( AV185cnt_P, 10, 0))+" Point-Of-Origin Commission converted" );
         context.msgStatus( "  "+GXutil.trim( GXutil.str( AV184cnt_P, 10, 0))+" Point-Of-Origin Commission NOT converted due Exclude list" );
         context.msgStatus( "  "+GXutil.trim( GXutil.str( AV194cnt_S, 10, 0))+" Records with Base Commission converted" );
         if ( ( GXutil.strSearch( AV17DebugM, "HOLDPROC", 1) > 0 ) )
         {
            context.msgStatus( "  "+GXutil.trim( GXutil.str( AV199cnt_H, 10, 0))+" lines on hold for this GDS" );
            context.msgStatus( "  "+GXutil.trim( GXutil.str( AV200cnt_C, 10, 0))+" Refunds CCGR Accepted" );
            context.msgStatus( "  "+GXutil.trim( GXutil.str( AV201cnt_C, 10, 0))+" Refunds CCGR on hold" );
         }
      }
   }

   public void S151( )
   {
      /* 'CHECKRETLINE' Routine */
      AV86vHntSe = (int)(AV86vHntSe+1) ;
      AV63sHntSe = GXutil.padl( GXutil.trim( GXutil.str( AV86vHntSe, 10, 0)), (short)(7), "0") + "5" ;
      if ( ( GXutil.len( AV40Linha) <= 256 ) )
      {
         if ( ( GXutil.strSearch( AV17DebugM, "LOADONLY", 1) == 0 ) )
         {
            /* Execute user subroutine: S161 */
            S161 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
      else
      {
         context.msgStatus( "  Invalid line lenght : "+AV63sHntSe );
      }
      /*
         INSERT RECORD ON TABLE RETSPECTEMP

      */
      A997HntSeq = GXutil.trim( AV63sHntSe) ;
      A989HntRec = GXutil.substring( AV40Linha, 1, 1) ;
      n989HntRec = false ;
      A993HntUsu = AV10AppCod ;
      n993HntUsu = false ;
      A994HntLin = AV40Linha ;
      n994HntLin = false ;
      /* Using cursor P005U6 */
      pr_default.execute(4, new Object[] {A997HntSeq, new Byte(A998HntSub), new Boolean(n989HntRec), A989HntRec, new Boolean(n993HntUsu), A993HntUsu, new Boolean(n994HntLin), A994HntLin});
      if ( (pr_default.getStatus(4) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      AV183Commi = (short)(AV183Commi+1) ;
      if ( ( AV183Commi >= 100 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "prethandling");
         AV183Commi = (short)(0) ;
      }
   }

   public void S141( )
   {
      /* 'CHECKHEADER' Routine */
      AV28Flag_I = (byte)(2) ;
      if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, 1, 1), "1") == 0 ) )
      {
         AV77TPST = GXutil.substring( AV40Linha, 15, 4) ;
         if ( ( GXutil.strcmp(AV77TPST, "PROD") == 0 ) || ( GXutil.strcmp(AV77TPST, "TEST") == 0 ) )
         {
            AV28Flag_I = (byte)(1) ;
            AV10AppCod = "RETClone" ;
            AV15CRS = GXutil.substring( AV40Linha, 8, 4) ;
            AV224REVN = GXutil.substring( AV40Linha, 12, 3) ;
         }
      }
      if ( ( AV28Flag_I == 2 ) )
      {
         AV26FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV50FileDe, "", "", (byte)(0), "") ;
         AV62sdb = "File is not RET" ;
         context.msgStatus( "  "+AV62sdb );
         GXv_char4[0] = AV62sdb ;
         GXv_char3[0] = AV22FileNa ;
         GXv_char10[0] = AV84Versao ;
         GXv_char11[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char4, GXv_char3, GXv_char10, GXv_char11) ;
         prethandling.this.AV62sdb = GXv_char4[0] ;
         prethandling.this.AV22FileNa = GXv_char3[0] ;
         prethandling.this.AV84Versao = GXv_char10[0] ;
         prethandling.this.AV17DebugM = GXv_char11[0] ;
      }
   }

   public void S161( )
   {
      /* 'VERLINHAS' Routine */
      AV80TRNN_C = GXutil.substring( AV40Linha, 2, 6) ;
      if ( ( GXutil.strcmp(AV81TRNN_O, AV80TRNN_C) != 0 ) )
      {
         if ( ( GXutil.strcmp(AV97RFN_Mu, "") != 0 ) )
         {
            /* Execute user subroutine: S171 */
            S171 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         AV29Flag_I = (byte)(0) ;
         AV30Flag_I = (byte)(0) ;
         AV76TMFA = "" ;
         AV110NEW_T = "" ;
         AV97RFN_Mu = "" ;
         AV204IT05P = "" ;
         AV203IT05P = "" ;
         AV208IT08p = "" ;
         AV209IT08p = "" ;
         AV210IT08S = (byte)(0) ;
         AV211IT08S = (byte)(0) ;
      }
      AV81TRNN_O = AV80TRNN_C ;
      AV54RCID = GXutil.substring( AV40Linha, 1, 1) ;
      if ( ( GXutil.strcmp(AV54RCID, "1") == 0 ) )
      {
         AV15CRS = GXutil.substring( AV40Linha, 8, 4) ;
         AV35IT01po = GXutil.trim( AV63sHntSe) ;
      }
      else if ( ( GXutil.strcmp(AV54RCID, "Z") == 0 ) )
      {
         if ( ( GXutil.strSearch( AV17DebugM, "HOLDPROC", 1) > 0 ) )
         {
            AV198IT0Z_ = AV40Linha ;
         }
         else
         {
            AV10AppCod = "RETClone" ;
            AV38IT0Zpo = GXutil.trim( AV63sHntSe) ;
         }
      }
      else if ( ( GXutil.strcmp(AV54RCID, "2") == 0 ) )
      {
         AV8AGTN = GXutil.substring( AV40Linha, 8, 7) ;
         AV78TRNC = GXutil.trim( GXutil.substring( AV40Linha, 48, 4)) ;
         AV70TDNR = GXutil.trim( GXutil.substring( AV40Linha, 35, 10)) ;
         AV67TACN = GXutil.substring( AV40Linha, 68, 3) ;
         AV144DAIS = GXutil.substring( AV40Linha, 23, 6) ;
         AV190STAT = GXutil.substring( AV40Linha, 29, 3) ;
         AV48ORAC = "" ;
         AV36IT02po = GXutil.trim( AV63sHntSe) ;
         if ( ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "RFN") == 0 ) )
         {
            /* Execute user subroutine: S181 */
            S181 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV10AppCod = "RETClone" ;
         }
      }
      else if ( ( GXutil.strcmp(AV54RCID, "3") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "RFN") == 0 ) && ( AV57RFNSki == 0 ) )
      {
         /* Execute user subroutine: S191 */
         S191 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( GXutil.strcmp(AV54RCID, "5") == 0 ) && ( ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "TKT") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "MCO") == 0 ) ) && ( AV29Flag_I == 0 ) )
      {
         AV29Flag_I = (byte)(1) ;
         AV14CORT = GXutil.substring( AV40Linha, 177, 5) ;
         AV12COAM = GXutil.substring( AV40Linha, 182, 11) ;
         AV37IT05po = GXutil.trim( AV63sHntSe) ;
         GXt_char9 = AV191new_C ;
         GXv_char11[0] = GXt_char9 ;
         new pgetspeccomm(remoteHandle, context).execute( AV67TACN, AV190STAT, AV8AGTN, AV14CORT, GXv_char11) ;
         prethandling.this.GXt_char9 = GXv_char11[0] ;
         AV191new_C = GXt_char9 ;
         if ( ( GXutil.strcmp(AV191new_C, AV14CORT) != 0 ) )
         {
            GXt_char9 = AV40Linha ;
            GXv_char11[0] = AV40Linha ;
            GXv_int12[0] = (short)(177) ;
            GXv_char10[0] = AV191new_C ;
            GXv_char4[0] = GXt_char9 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
            prethandling.this.AV40Linha = GXv_char11[0] ;
            prethandling.this.AV191new_C = GXv_char10[0] ;
            prethandling.this.GXt_char9 = GXv_char4[0] ;
            AV40Linha = GXt_char9 ;
            AV14CORT = AV191new_C ;
            AV12COAM = "00000000000" ;
            GXt_char9 = AV40Linha ;
            GXv_char11[0] = AV40Linha ;
            GXv_int12[0] = (short)(182) ;
            GXv_char10[0] = AV12COAM ;
            GXv_char4[0] = GXt_char9 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
            prethandling.this.AV40Linha = GXv_char11[0] ;
            prethandling.this.AV12COAM = GXv_char10[0] ;
            prethandling.this.GXt_char9 = GXv_char4[0] ;
            AV40Linha = GXt_char9 ;
            AV194cnt_S = (int)(AV194cnt_S+1) ;
         }
      }
      else if ( ( GXutil.strcmp(AV54RCID, "5") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "RFN") == 0 ) && ( AV57RFNSki == 0 ) && ( GXutil.strcmp(AV97RFN_Mu, "") != 0 ) )
      {
         AV137Last_ = GXutil.trim( AV63sHntSe) ;
         if ( ( AV29Flag_I == 0 ) )
         {
            AV29Flag_I = (byte)(1) ;
            AV202TAX_T = 0 ;
            AV37IT05po = GXutil.trim( AV63sHntSe) ;
            AV111TDAM = GXutil.substring( AV40Linha, 87, 11) ;
            AV204IT05P = "" ;
            AV203IT05P = "" ;
            AV14CORT = GXutil.substring( AV40Linha, 177, 5) ;
            AV12COAM = GXutil.substring( AV40Linha, 182, 11) ;
         }
         AV205iAdd = (short)(0) ;
         while ( ( AV205iAdd <= 84 ) )
         {
            AV206iCoun = (short)(30) ;
            while ( ( AV206iCoun <= 68 ) )
            {
               AV34i = (int)(AV206iCoun+AV205iAdd) ;
               AV39j = (int)(AV34i+8) ;
               if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, AV34i, 2), "  ") != 0 ) && ( GXutil.strcmp(GXutil.substring( AV40Linha, AV34i, 2), "CP") != 0 ) )
               {
                  AV202TAX_T = (double)(AV202TAX_T+(GXutil.val( GXutil.substring( AV40Linha, AV39j, 11), "."))) ;
               }
               if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, AV34i, 2), "CP") == 0 ) )
               {
                  AV203IT05P = GXutil.trim( AV63sHntSe) ;
                  AV76TMFA = GXutil.substring( AV40Linha, AV39j, 11) ;
                  AV62sdb = "Achado campo CP de " + AV76TMFA ;
                  GXv_char11[0] = AV62sdb ;
                  GXv_char10[0] = AV22FileNa ;
                  GXv_char4[0] = AV84Versao ;
                  GXv_char3[0] = AV17DebugM ;
                  new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
                  prethandling.this.AV62sdb = GXv_char11[0] ;
                  prethandling.this.AV22FileNa = GXv_char10[0] ;
                  prethandling.this.AV84Versao = GXv_char4[0] ;
                  prethandling.this.AV17DebugM = GXv_char3[0] ;
               }
               if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, AV34i, 2), "  ") == 0 ) && ( GXutil.strcmp(AV204IT05P, "") == 0 ) )
               {
                  AV204IT05P = GXutil.trim( AV63sHntSe) ;
               }
               AV206iCoun = (short)(AV206iCoun+19) ;
            }
            AV205iAdd = (short)(AV205iAdd+84) ;
         }
      }
      else if ( ( GXutil.strcmp(AV54RCID, "6") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "TKT") == 0 ) && ( AV30Flag_I == 0 ) )
      {
         AV30Flag_I = (byte)(1) ;
         AV48ORAC = GXutil.substring( AV40Linha, 8, 3) ;
         AV21FBTD = GXutil.trim( GXutil.substring( AV40Linha, 56, 15)) ;
         /* Execute user subroutine: S201 */
         S201 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( GXutil.strcmp(AV54RCID, "8") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV78TRNC, 1, 3), "RFN") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, 50, 2), "CA") == 0 ) && ( GXutil.strcmp(AV208IT08p, "") == 0 ) )
         {
            AV208IT08p = GXutil.trim( AV63sHntSe) ;
            AV210IT08S = (byte)(1) ;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, 50, 2), "CC") == 0 ) && ( GXutil.strcmp(AV209IT08p, "") == 0 ) )
         {
            AV209IT08p = GXutil.trim( AV63sHntSe) ;
            AV211IT08S = (byte)(1) ;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, 173, 2), "CA") == 0 ) && ( GXutil.strcmp(AV208IT08p, "") == 0 ) )
         {
            AV208IT08p = GXutil.trim( AV63sHntSe) ;
            AV210IT08S = (byte)(2) ;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV40Linha, 173, 2), "CC") == 0 ) && ( GXutil.strcmp(AV209IT08p, "") == 0 ) )
         {
            AV209IT08p = GXutil.trim( AV63sHntSe) ;
            AV211IT08S = (byte)(2) ;
         }
      }
   }

   public void S201( )
   {
      /* 'COMPARA' Routine */
      AV34i = 1 ;
      while ( ( AV34i <= AV52qtdAir ) )
      {
         AV41ListAi = GXutil.trim( AV9AirptCo[AV34i-1][1-1]) ;
         AV42ListAi = GXutil.trim( AV9AirptCo[AV34i-1][2-1]) ;
         if ( ( GXutil.strcmp(AV41ListAi, AV67TACN) == 0 ) && ( GXutil.strcmp(AV42ListAi, AV48ORAC) == 0 ) )
         {
            AV43ListCo = AV9AirptCo[AV34i-1][3-1] ;
            AV27Flag_F = (short)(0) ;
            AV39j = 1 ;
            while ( ( AV39j <= AV51qtdAir ) )
            {
               AV19FareAi = AV85vFareB[AV39j-1][1-1] ;
               AV20FareBa = GXutil.trim( AV85vFareB[AV39j-1][2-1]) ;
               AV69TamFar = (short)(GXutil.len( AV20FareBa)) ;
               AV82uFBTD = GXutil.substring( AV21FBTD, 1, AV69TamFar) ;
               if ( ( GXutil.strcmp(AV19FareAi, AV41ListAi) == 0 ) && ( GXutil.strcmp(AV82uFBTD, AV20FareBa) == 0 ) )
               {
                  AV114SCETp = "W" ;
                  AV61SCETex = "NOT converted|Point of Origin is " + AV48ORAC + "|Fare " + GXutil.trim( AV21FBTD) + " is in Exclude list." ;
                  AV184cnt_P = (int)(AV184cnt_P+1) ;
                  /* Execute user subroutine: S211 */
                  S211 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
                  AV27Flag_F = (short)(1) ;
                  if (true) break;
               }
               AV39j = (int)(AV39j+1) ;
            }
            if ( ( GXutil.val( AV14CORT, ".") == 0 ) )
            {
               AV114SCETp = "W" ;
               AV61SCETex = "NOT converted|Commission is already zero." ;
               AV184cnt_P = (int)(AV184cnt_P+1) ;
               /* Execute user subroutine: S211 */
               S211 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV27Flag_F = (short)(1) ;
            }
            if ( ( AV27Flag_F == 0 ) )
            {
               AV65SubstS = GXutil.trim( AV43ListCo) ;
               AV65SubstS = GXutil.strReplace( AV65SubstS, ".", "") ;
               AV65SubstS = GXutil.padl( GXutil.trim( AV65SubstS), (short)(5), "0") ;
               AV66SubstS = "00000000000" ;
               AV11Baseco = (double)(GXutil.val( AV14CORT, ".")/ (double) (100)) ;
               /* Using cursor P005U7 */
               pr_default.execute(5, new Object[] {AV37IT05po, AV37IT05po});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A993HntUsu = P005U7_A993HntUsu[0] ;
                  n993HntUsu = P005U7_n993HntUsu[0] ;
                  A997HntSeq = P005U7_A997HntSeq[0] ;
                  A994HntLin = P005U7_A994HntLin[0] ;
                  n994HntLin = P005U7_n994HntLin[0] ;
                  A998HntSub = P005U7_A998HntSub[0] ;
                  if ( ( GXutil.strcmp(GXutil.substring( A993HntUsu, 1, 8), "RETClone") == 0 ) )
                  {
                     A994HntLin = GXutil.substring( A994HntLin, 1, 176) + AV65SubstS + AV66SubstS + GXutil.substring( A994HntLin, 193, 64) ;
                     n994HntLin = false ;
                     AV114SCETp = "C" ;
                     AV61SCETex = "Converted|" + AV8AGTN + "|" + GXutil.padl( GXutil.trim( GXutil.str( AV11Baseco, 6, 2)), (short)(5), "0") + "%|" + GXutil.padl( GXutil.trim( AV43ListCo), (short)(5), "0") + "%|" + "Point of Origin is " + GXutil.trim( AV48ORAC) + "|No Fare in Exclude list." ;
                     AV185cnt_P = (int)(AV185cnt_P+1) ;
                     /* Execute user subroutine: S211 */
                     S211 ();
                     if ( returnInSub )
                     {
                        pr_default.close(5);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Using cursor P005U8 */
                     pr_default.execute(6, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
                  }
                  pr_default.readNext(5);
               }
               pr_default.close(5);
            }
            if (true) break;
         }
         AV34i = (int)(AV34i+1) ;
      }
   }

   public void S181( )
   {
      /* 'RFND_IT02' Routine */
      AV57RFNSki = (byte)(1) ;
      AV34i = 1 ;
      while ( ( AV34i <= AV53qtdCRS ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV83vCRSs[AV34i-1][1-1]), GXutil.trim( AV67TACN)) == 0 ) && ( GXutil.strcmp(GXutil.trim( AV83vCRSs[AV34i-1][2-1]), GXutil.trim( AV15CRS)) == 0 ) )
         {
            AV57RFNSki = (byte)(0) ;
            AV16RFN_DA = GXutil.substring( AV40Linha, 23, 6) ;
            if ( ( AV64SplitR == 1 ) )
            {
               AV10AppCod = "RETCloneRFN" ;
            }
            else
            {
               AV10AppCod = "RETClone" ;
            }
            if (true) break;
         }
         AV34i = (int)(AV34i+1) ;
      }
      if ( ( AV57RFNSki == 1 ) || ( GXutil.strSearch( AV17DebugM, "NORFN", 1) > 0 ) )
      {
         AV10AppCod = "RETCloneSkipped" ;
         AV114SCETp = "I" ;
         AV61SCETex = "RFN ignorado|" + AV15CRS + "|CRS n�o autorizado" ;
         /* Execute user subroutine: S211 */
         S211 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S191( )
   {
      /* 'RFND_IT03' Routine */
      AV55RCPN = GXutil.trim( GXutil.substring( AV40Linha, 8, 4)) ;
      AV59RTDN = GXutil.trim( GXutil.substring( AV40Linha, 15, 10)) ;
      AV213DIRD = GXutil.substring( AV40Linha, 148, 6) ;
      /* Execute user subroutine: S221 */
      S221 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( AV57RFNSki == 0 ) )
      {
         /* Execute user subroutine: S231 */
         S231 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      if ( ( AV57RFNSki == 1 ) )
      {
         AV10AppCod = "RETCloneSkipped" ;
         GXv_char11[0] = AV36IT02po ;
         GXv_char10[0] = AV10AppCod ;
         new pretchangeapp(remoteHandle, context).execute( GXv_char11, GXv_char10) ;
         prethandling.this.AV36IT02po = GXv_char11[0] ;
         prethandling.this.AV10AppCod = GXv_char10[0] ;
      }
      else if ( ( AV57RFNSki == 2 ) )
      {
         AV10AppCod = "RETCloneHeld" ;
         GXv_char11[0] = AV36IT02po ;
         GXv_char10[0] = AV10AppCod ;
         new pretchangeapp(remoteHandle, context).execute( GXv_char11, GXv_char10) ;
         prethandling.this.AV36IT02po = GXv_char11[0] ;
         prethandling.this.AV10AppCod = GXv_char10[0] ;
      }
      else
      {
         AV114SCETp = "A" ;
         AV61SCETex = "RFN aceito|" + AV56rffopd ;
      }
      /* Execute user subroutine: S211 */
      S211 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S231( )
   {
      /* 'BUSCAFOPS' Routine */
      if ( ( GXutil.strcmp(AV75TKT_TR, "TKTA") == 0 ) )
      {
         AV56rffopd = "OPTAT" ;
      }
      else if ( ( GXutil.strcmp(AV75TKT_TR, "TKTB") == 0 ) )
      {
         AV56rffopd = "OPTAT" ;
      }
      else if ( ( GXutil.strcmp(AV75TKT_TR, "TKTT") == 0 ) )
      {
         AV56rffopd = "ETICKET" ;
      }
      else
      {
         AV56rffopd = AV75TKT_TR ;
      }
      AV216fOK = (byte)(1) ;
      AV197k = (byte)(1) ;
      while ( ( AV197k <= AV196TKT_f ) )
      {
         AV31fopid = GXutil.trim( AV73TKT_fo[AV197k-1]) ;
         AV230GXLvl = (byte)(0) ;
         /* Using cursor P005U9 */
         pr_default.execute(7, new Object[] {AV67TACN, AV56rffopd, AV31fopid});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1450rfFop = P005U9_A1450rfFop[0] ;
            n1450rfFop = P005U9_n1450rfFop[0] ;
            A1435fopID = P005U9_A1435fopID[0] ;
            A1455rfFop = P005U9_A1455rfFop[0] ;
            A1233EmpCo = P005U9_A1233EmpCo[0] ;
            AV230GXLvl = (byte)(1) ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
         if ( ( AV230GXLvl == 0 ) )
         {
            AV216fOK = (byte)(0) ;
         }
         if ( ( AV216fOK == 0 ) )
         {
            AV114SCETp = "I" ;
            AV61SCETex = "RFN ignorado|" + AV56rffopd + "/" + AV31fopid + "|Documento n�o autorizado" ;
            if (true) break;
         }
         AV197k = (byte)(AV197k+1) ;
      }
      if ( ( AV196TKT_f == 0 ) )
      {
         AV114SCETp = "I" ;
         AV61SCETex = "RFN ignorado|Documento sem formas de pagamento no hist�rico" ;
         AV216fOK = (byte)(0) ;
      }
      if ( ( AV216fOK == 1 ) )
      {
         /* Execute user subroutine: S241 */
         S241 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         AV57RFNSki = (byte)(1) ;
      }
   }

   public void S241( )
   {
      /* 'GETFOPINFO' Routine */
      AV57RFNSki = (byte)(1) ;
      AV109rfFop = "1" ;
      AV31fopid = GXutil.trim( AV73TKT_fo[1-1]) ;
      AV231GXLvl = (byte)(0) ;
      /* Using cursor P005U10 */
      pr_default.execute(8, new Object[] {AV67TACN, AV56rffopd, AV31fopid});
      while ( (pr_default.getStatus(8) != 101) )
      {
         A1450rfFop = P005U10_A1450rfFop[0] ;
         n1450rfFop = P005U10_n1450rfFop[0] ;
         A1435fopID = P005U10_A1435fopID[0] ;
         A1455rfFop = P005U10_A1455rfFop[0] ;
         A1233EmpCo = P005U10_A1233EmpCo[0] ;
         A1454rfFop = P005U10_A1454rfFop[0] ;
         n1454rfFop = P005U10_n1454rfFop[0] ;
         A1451rfFop = P005U10_A1451rfFop[0] ;
         n1451rfFop = P005U10_n1451rfFop[0] ;
         A1453rfFop = P005U10_A1453rfFop[0] ;
         n1453rfFop = P005U10_n1453rfFop[0] ;
         AV231GXLvl = (byte)(1) ;
         AV109rfFop = A1454rfFop ;
         GXt_date13 = AV87dt1 ;
         GXv_date14[0] = GXt_date13 ;
         new pr2string2date(remoteHandle, context).execute( AV16RFN_DA, GXv_date14) ;
         prethandling.this.GXt_date13 = GXv_date14[0] ;
         AV87dt1 = GXt_date13 ;
         GXt_date13 = AV88dt2 ;
         GXv_date14[0] = GXt_date13 ;
         new pr2string2date(remoteHandle, context).execute( AV72TKT_DA, GXv_date14) ;
         prethandling.this.GXt_date13 = GXv_date14[0] ;
         AV88dt2 = GXt_date13 ;
         AV89RFN_De = (short)(GXutil.ddiff(AV87dt1,AV88dt2)) ;
         AV62sdb = "Decorridos " + GXutil.trim( GXutil.str( AV89RFN_De, 10, 0)) + " dias para FOP=" + AV31fopid ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( AV89RFN_De > A1451rfFop ) )
         {
            AV57RFNSki = (byte)(1) ;
            AV114SCETp = "I" ;
            AV61SCETex = "RFN ignorado|" + AV16RFN_DA + "/" + AV72TKT_DA + "|Emiss�o fora do prazo de " + GXutil.trim( GXutil.str( A1451rfFop, 10, 0)) + " dias" ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         GXt_int15 = AV103n1 ;
         GXv_char11[0] = AV55RCPN ;
         GXv_char10[0] = "0" ;
         GXv_int16[0] = GXt_int15 ;
         new pr2countchar(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_int16) ;
         prethandling.this.AV55RCPN = GXv_char11[0] ;
         prethandling.this.GXt_int15 = GXv_int16[0] ;
         AV103n1 = (int)(4-GXt_int15) ;
         GXt_int15 = AV104n2 ;
         GXv_char11[0] = AV71TKT_Co ;
         GXv_char10[0] = "F" ;
         GXv_int16[0] = GXt_int15 ;
         new pr2countchar(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_int16) ;
         prethandling.this.AV71TKT_Co = GXv_char11[0] ;
         prethandling.this.GXt_int15 = GXv_int16[0] ;
         AV104n2 = GXt_int15 ;
         AV62sdb = "Cupons originais: [" + AV71TKT_Co + "], pedidos: [" + AV55RCPN + "], configurado Total/Parcial=[" + A1453rfFop + "]" ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( GXutil.strcmp(A1453rfFop, "T") == 0 ) && ( AV103n1 != AV104n2 ) )
         {
            AV57RFNSki = (byte)(1) ;
            AV114SCETp = "I" ;
            AV61SCETex = "RFN ignorado|" + AV55RCPN + "/" + AV71TKT_Co + "|Apenas Reembolso total permitido" ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         if ( ( GXutil.strcmp(GXutil.trim( A1435fopID), "GR") == 0 ) )
         {
            AV87dt1 = Gx_date ;
            if ( ( GXutil.strcmp(AV213DIRD, AV72TKT_DA) != 0 ) )
            {
               GXt_date13 = AV88dt2 ;
               GXv_date14[0] = GXt_date13 ;
               new pr2string2date(remoteHandle, context).execute( AV213DIRD, GXv_date14) ;
               prethandling.this.GXt_date13 = GXv_date14[0] ;
               AV88dt2 = GXt_date13 ;
            }
            AV89RFN_De = (short)(GXutil.ddiff(AV87dt1,AV88dt2)) ;
            if ( ( AV89RFN_De < 20 ) )
            {
               AV57RFNSki = (byte)(2) ;
               AV114SCETp = "I" ;
               AV61SCETex = "RFN colocado em espera|" + AV16RFN_DA + "/" + AV72TKT_DA + "/" + AV213DIRD + "|GR fora do prazo m�nimo" ;
               AV201cnt_C = (int)(AV201cnt_C+1) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            else
            {
               AV200cnt_C = (int)(AV200cnt_C+1) ;
               AV57RFNSki = (byte)(0) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
         }
         else
         {
            AV97RFN_Mu = "" ;
            /* Using cursor P005U11 */
            pr_default.execute(9, new Object[] {AV67TACN});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1447rfBas = P005U11_A1447rfBas[0] ;
               A1446rfBas = P005U11_A1446rfBas[0] ;
               A1233EmpCo = P005U11_A1233EmpCo[0] ;
               A1449rfBas = P005U11_A1449rfBas[0] ;
               A1442rfBas = P005U11_A1442rfBas[0] ;
               n1442rfBas = P005U11_n1442rfBas[0] ;
               A1443rfBas = P005U11_A1443rfBas[0] ;
               n1443rfBas = P005U11_n1443rfBas[0] ;
               A1444rfBas = P005U11_A1444rfBas[0] ;
               n1444rfBas = P005U11_n1444rfBas[0] ;
               A1445rfBas = P005U11_A1445rfBas[0] ;
               n1445rfBas = P005U11_n1445rfBas[0] ;
               A1448rfBas = P005U11_A1448rfBas[0] ;
               AV60s = "," + GXutil.trim( A1447rfBas) + "," ;
               if ( ( GXutil.strSearch( AV90TKT_Fa, AV60s, 1) > 0 ) )
               {
                  AV62sdb = "Encontrou base tarif�ria espec�fica [" + AV99rfBase + "]" ;
                  GXv_char11[0] = AV62sdb ;
                  GXv_char10[0] = AV22FileNa ;
                  GXv_char4[0] = AV84Versao ;
                  GXv_char3[0] = AV17DebugM ;
                  new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
                  prethandling.this.AV62sdb = GXv_char11[0] ;
                  prethandling.this.AV22FileNa = GXv_char10[0] ;
                  prethandling.this.AV84Versao = GXv_char4[0] ;
                  prethandling.this.AV17DebugM = GXv_char3[0] ;
                  if ( ( AV89RFN_De <= A1449rfBas ) )
                  {
                     AV106rfBas = A1442rfBas ;
                     AV107rfBas = A1443rfBas ;
                     AV108rfBas = A1444rfBas ;
                     AV105rfBas = A1445rfBas ;
                     /* Execute user subroutine: S259 */
                     S259 ();
                     if ( returnInSub )
                     {
                        pr_default.close(9);
                        pr_default.close(8);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  else
                  {
                     AV62sdb = "Achou a base tarif�ria, mas o prazo decorrido n�o bate com o da tarifa:" + GXutil.trim( GXutil.str( AV89RFN_De, 10, 0)) + "/" + GXutil.trim( GXutil.str( A1449rfBas, 10, 0)) ;
                  }
               }
               else
               {
                  AV62sdb = "Fare Basis do TKT: [" + AV90TKT_Fa + "], configurado: [" + AV99rfBase + "]" ;
               }
               pr_default.readNext(9);
            }
            pr_default.close(9);
            if ( ( GXutil.strcmp(AV97RFN_Mu, "") == 0 ) )
            {
               AV234GXLvl = (byte)(0) ;
               /* Using cursor P005U12 */
               pr_default.execute(10, new Object[] {AV67TACN, new Short(AV89RFN_De)});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A1449rfBas = P005U12_A1449rfBas[0] ;
                  A1447rfBas = P005U12_A1447rfBas[0] ;
                  A1446rfBas = P005U12_A1446rfBas[0] ;
                  A1233EmpCo = P005U12_A1233EmpCo[0] ;
                  A1442rfBas = P005U12_A1442rfBas[0] ;
                  n1442rfBas = P005U12_n1442rfBas[0] ;
                  A1443rfBas = P005U12_A1443rfBas[0] ;
                  n1443rfBas = P005U12_n1443rfBas[0] ;
                  A1444rfBas = P005U12_A1444rfBas[0] ;
                  n1444rfBas = P005U12_n1444rfBas[0] ;
                  A1445rfBas = P005U12_A1445rfBas[0] ;
                  n1445rfBas = P005U12_n1445rfBas[0] ;
                  A1448rfBas = P005U12_A1448rfBas[0] ;
                  AV234GXLvl = (byte)(1) ;
                  AV62sdb = "Encontrou base tarif�ria padr�o" ;
                  GXv_char11[0] = AV62sdb ;
                  GXv_char10[0] = AV22FileNa ;
                  GXv_char4[0] = AV84Versao ;
                  GXv_char3[0] = AV17DebugM ;
                  new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
                  prethandling.this.AV62sdb = GXv_char11[0] ;
                  prethandling.this.AV22FileNa = GXv_char10[0] ;
                  prethandling.this.AV84Versao = GXv_char4[0] ;
                  prethandling.this.AV17DebugM = GXv_char3[0] ;
                  AV106rfBas = A1442rfBas ;
                  AV107rfBas = A1443rfBas ;
                  AV108rfBas = A1444rfBas ;
                  AV105rfBas = A1445rfBas ;
                  /* Execute user subroutine: S259 */
                  S259 ();
                  if ( returnInSub )
                  {
                     pr_default.close(10);
                     pr_default.close(8);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(10);
               }
               pr_default.close(10);
               if ( ( AV234GXLvl == 0 ) )
               {
                  AV97RFN_Mu = "" ;
                  AV62sdb = "N�o encontrou base tarif�ria padr�o, sem multa" ;
                  GXv_char11[0] = AV62sdb ;
                  GXv_char10[0] = AV22FileNa ;
                  GXv_char4[0] = AV84Versao ;
                  GXv_char3[0] = AV17DebugM ;
                  new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
                  prethandling.this.AV62sdb = GXv_char11[0] ;
                  prethandling.this.AV22FileNa = GXv_char10[0] ;
                  prethandling.this.AV84Versao = GXv_char4[0] ;
                  prethandling.this.AV17DebugM = GXv_char3[0] ;
               }
            }
            AV57RFNSki = (byte)(0) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(8);
      if ( ( AV231GXLvl == 0 ) )
      {
         AV114SCETp = "I" ;
         AV61SCETex = "RFN ignorado|" + AV56rffopd + "|Documento n�o autorizado (2)" ;
      }
   }

   public void S259( )
   {
      /* 'CALCMULTA' Routine */
      AV62sdb = "Percentagem=" + GXutil.trim( GXutil.str( AV106rfBas, 7, 4)) + ", Valor=" + AV105rfBas + " " + GXutil.trim( GXutil.str( AV107rfBas, 11, 2)) + ", menor=" + AV108rfBas ;
      GXv_char11[0] = AV62sdb ;
      GXv_char10[0] = AV22FileNa ;
      GXv_char4[0] = AV84Versao ;
      GXv_char3[0] = AV17DebugM ;
      new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
      prethandling.this.AV62sdb = GXv_char11[0] ;
      prethandling.this.AV22FileNa = GXv_char10[0] ;
      prethandling.this.AV84Versao = GXv_char4[0] ;
      prethandling.this.AV17DebugM = GXv_char3[0] ;
      GXt_date13 = AV87dt1 ;
      GXv_date14[0] = GXt_date13 ;
      new pr2string2date(remoteHandle, context).execute( AV16RFN_DA, GXv_date14) ;
      prethandling.this.GXt_date13 = GXv_date14[0] ;
      AV87dt1 = GXt_date13 ;
      AV112v = (double)(AV107rfBas*100) ;
      AV62sdb = "Valor=" + GXutil.str( AV112v, 10, 0) ;
      GXv_char11[0] = AV62sdb ;
      GXv_char10[0] = AV22FileNa ;
      GXv_char4[0] = AV84Versao ;
      GXv_char3[0] = AV17DebugM ;
      new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
      prethandling.this.AV62sdb = GXv_char11[0] ;
      prethandling.this.AV22FileNa = GXv_char10[0] ;
      prethandling.this.AV84Versao = GXv_char4[0] ;
      prethandling.this.AV17DebugM = GXv_char3[0] ;
      if ( ( GXutil.strcmp(AV105rfBas, "BRL") == 0 ) )
      {
         AV97RFN_Mu = GXutil.padl( GXutil.trim( GXutil.str( AV112v, 10, 0)), (short)(11), "0") ;
         AV62sdb = "Multa em BRL=" + AV97RFN_Mu ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
      }
      else
      {
         AV235GXLvl = (byte)(0) ;
         /* Using cursor P005U13 */
         pr_default.execute(11, new Object[] {AV105rfBas, AV87dt1});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A1431CaTip = P005U13_A1431CaTip[0] ;
            A1430CaMoe = P005U13_A1430CaMoe[0] ;
            A1429CaDat = P005U13_A1429CaDat[0] ;
            A1426CaVal = P005U13_A1426CaVal[0] ;
            n1426CaVal = P005U13_n1426CaVal[0] ;
            AV235GXLvl = (byte)(1) ;
            AV112v = (double)(DecimalUtil.decToDouble(GXutil.truncDecimal( DecimalUtil.doubleToDec(AV112v*A1426CaVal), 0))) ;
            AV97RFN_Mu = GXutil.trim( GXutil.str( AV112v, 10, 0)) ;
            AV97RFN_Mu = GXutil.padl( AV97RFN_Mu, (short)(11), "0") ;
            AV62sdb = "Multa em " + AV105rfBas + "=" + AV97RFN_Mu ;
            GXv_char11[0] = AV62sdb ;
            GXv_char10[0] = AV22FileNa ;
            GXv_char4[0] = AV84Versao ;
            GXv_char3[0] = AV17DebugM ;
            new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
            prethandling.this.AV62sdb = GXv_char11[0] ;
            prethandling.this.AV22FileNa = GXv_char10[0] ;
            prethandling.this.AV84Versao = GXv_char4[0] ;
            prethandling.this.AV17DebugM = GXv_char3[0] ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(11);
         }
         pr_default.close(11);
         if ( ( AV235GXLvl == 0 ) )
         {
            if ( ( AV189Verbo == 1 ) )
            {
               context.msgStatus( "     Exchange Rate for "+localUtil.dtoc( AV87dt1, 2, "/")+" ("+AV16RFN_DA+") not found" );
            }
            AV236GXLvl = (byte)(0) ;
            /* Using cursor P005U14 */
            pr_default.execute(12, new Object[] {AV105rfBas, AV87dt1});
            while ( (pr_default.getStatus(12) != 101) )
            {
               A1429CaDat = P005U14_A1429CaDat[0] ;
               A1431CaTip = P005U14_A1431CaTip[0] ;
               A1430CaMoe = P005U14_A1430CaMoe[0] ;
               A1426CaVal = P005U14_A1426CaVal[0] ;
               n1426CaVal = P005U14_n1426CaVal[0] ;
               AV236GXLvl = (byte)(1) ;
               AV112v = (double)(DecimalUtil.decToDouble(GXutil.truncDecimal( DecimalUtil.doubleToDec(AV112v*A1426CaVal), 0))) ;
               AV97RFN_Mu = GXutil.trim( GXutil.str( AV112v, 10, 0)) ;
               AV97RFN_Mu = GXutil.padl( AV97RFN_Mu, (short)(11), "0") ;
               AV62sdb = "Multa (Cambio aproximado) em " + AV105rfBas + "=" + AV97RFN_Mu ;
               GXv_char11[0] = AV62sdb ;
               GXv_char10[0] = AV22FileNa ;
               GXv_char4[0] = AV84Versao ;
               GXv_char3[0] = AV17DebugM ;
               new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
               prethandling.this.AV62sdb = GXv_char11[0] ;
               prethandling.this.AV22FileNa = GXv_char10[0] ;
               prethandling.this.AV84Versao = GXv_char4[0] ;
               prethandling.this.AV17DebugM = GXv_char3[0] ;
               AV186cnt_C = (int)(AV186cnt_C+1) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(12);
            }
            pr_default.close(12);
            if ( ( AV236GXLvl == 0 ) )
            {
               AV97RFN_Mu = GXutil.trim( GXutil.str( AV112v, 10, 0)) ;
               AV97RFN_Mu = GXutil.padl( AV97RFN_Mu, (short)(11), "0") ;
               AV114SCETp = "E" ;
               AV62sdb = "Erro: N�o achou o c�mbio para " + AV105rfBas + " em " + AV16RFN_DA + ". Multa ser� " + AV97RFN_Mu ;
               AV187cnt_C = (int)(AV187cnt_C+1) ;
               GXv_char11[0] = AV62sdb ;
               GXv_char10[0] = AV22FileNa ;
               GXv_char4[0] = AV84Versao ;
               GXv_char3[0] = AV17DebugM ;
               new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
               prethandling.this.AV62sdb = GXv_char11[0] ;
               prethandling.this.AV22FileNa = GXv_char10[0] ;
               prethandling.this.AV84Versao = GXv_char4[0] ;
               prethandling.this.AV17DebugM = GXv_char3[0] ;
            }
         }
      }
   }

   public void S221( )
   {
      /* 'BUSCAHOT' Routine */
      AV57RFNSki = (byte)(1) ;
      AV90TKT_Fa = "," ;
      AV94hot0_n = AV59RTDN ;
      AV169NUM_B = (long)(GXutil.val( AV94hot0_n, ".")) ;
      AV196TKT_f = (byte)(0) ;
      AV237GXLvl = (byte)(0) ;
      /* Using cursor P005U15 */
      pr_default.execute(13, new Object[] {new Long(AV169NUM_B)});
      while ( (pr_default.getStatus(13) != 101) )
      {
         A964CiaCod = P005U15_A964CiaCod[0] ;
         A963ISOC = P005U15_A963ISOC[0] ;
         A966CODE = P005U15_A966CODE[0] ;
         A902NUM_BI = P005U15_A902NUM_BI[0] ;
         n902NUM_BI = P005U15_n902NUM_BI[0] ;
         A907CPUI = P005U15_A907CPUI[0] ;
         n907CPUI = P005U15_n907CPUI[0] ;
         A889TRANS_ = P005U15_A889TRANS_[0] ;
         n889TRANS_ = P005U15_n889TRANS_[0] ;
         A970DATA = P005U15_A970DATA[0] ;
         A967IATA = P005U15_A967IATA[0] ;
         A968NUM_BI = P005U15_A968NUM_BI[0] ;
         A965PER_NA = P005U15_A965PER_NA[0] ;
         A969TIPO_V = P005U15_A969TIPO_V[0] ;
         A883TAX_IT = P005U15_A883TAX_IT[0] ;
         n883TAX_IT = P005U15_n883TAX_IT[0] ;
         A871COMMIS = P005U15_A871COMMIS[0] ;
         n871COMMIS = P005U15_n871COMMIS[0] ;
         if ( ( GXutil.strcmp(GXutil.left( A966CODE, 3), "TKT") == 0 ) )
         {
            AV237GXLvl = (byte)(1) ;
            AV75TKT_TR = GXutil.substring( A966CODE, 1, 4) ;
            if ( ( GXutil.strcmp(A907CPUI, "") != 0 ) )
            {
               AV71TKT_Co = A907CPUI ;
            }
            else
            {
               AV71TKT_Co = A889TRANS_ ;
            }
            GXt_char9 = AV72TKT_DA ;
            GXv_date14[0] = A970DATA ;
            GXv_int17[0] = (byte)(0) ;
            GXv_char11[0] = GXt_char9 ;
            new pr2date2string(remoteHandle, context).execute( GXv_date14, GXv_int17, GXv_char11) ;
            prethandling.this.A970DATA = GXv_date14[0] ;
            prethandling.this.GXt_char9 = GXv_char11[0] ;
            AV72TKT_DA = GXt_char9 ;
            AV57RFNSki = (byte)(0) ;
            AV92hot0_c = A966CODE ;
            AV96hot0_d = A970DATA ;
            AV93hot0_i = A967IATA ;
            AV94hot0_n = A968NUM_BI ;
            AV91hot0_p = A965PER_NA ;
            AV95hot0_t = A969TIPO_V ;
            if ( ( A883TAX_IT == 0 ) )
            {
               AV161COMMI = A871COMMIS ;
            }
            else
            {
               AV161COMMI = A883TAX_IT ;
            }
            AV238GXLvl = (byte)(0) ;
            /* Using cursor P005U16 */
            pr_default.execute(14, new Object[] {A963ISOC, A964CiaCod, AV91hot0_p, AV92hot0_c, AV93hot0_i, AV94hot0_n, AV95hot0_t, AV96hot0_d});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A970DATA = P005U16_A970DATA[0] ;
               A969TIPO_V = P005U16_A969TIPO_V[0] ;
               A968NUM_BI = P005U16_A968NUM_BI[0] ;
               A967IATA = P005U16_A967IATA[0] ;
               A966CODE = P005U16_A966CODE[0] ;
               A965PER_NA = P005U16_A965PER_NA[0] ;
               A972SeqPag = P005U16_A972SeqPag[0] ;
               A936PGTIP_ = P005U16_A936PGTIP_[0] ;
               n936PGTIP_ = P005U16_n936PGTIP_[0] ;
               A933PGCC_T = P005U16_A933PGCC_T[0] ;
               n933PGCC_T = P005U16_n933PGCC_T[0] ;
               AV238GXLvl = (byte)(1) ;
               if ( ( AV196TKT_f < 10 ) )
               {
                  AV196TKT_f = (byte)(AV196TKT_f+1) ;
                  if ( ( GXutil.strcmp(A933PGCC_T, "GR") == 0 ) || ( GXutil.strcmp(A936PGTIP_, "MS") == 0 ) )
                  {
                     AV73TKT_fo[AV196TKT_f-1] = A933PGCC_T ;
                  }
                  else
                  {
                     if ( ( GXutil.strcmp(GXutil.trim( A936PGTIP_), "") == 0 ) )
                     {
                        AV73TKT_fo[AV196TKT_f-1] = AV95hot0_t ;
                     }
                     else
                     {
                        AV73TKT_fo[AV196TKT_f-1] = A936PGTIP_ ;
                     }
                  }
               }
               pr_default.readNext(14);
            }
            pr_default.close(14);
            if ( ( AV238GXLvl == 0 ) )
            {
               AV196TKT_f = (byte)(1) ;
               if ( ( ( GXutil.strcmp(GXutil.substring( AV95hot0_t, 1, 2), "MS") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV95hot0_t, 3, 2), "GR") == 0 ) ) && ( GXutil.strcmp(GXutil.trim( GXutil.substring( AV95hot0_t, 3, 2)), "") != 0 ) )
               {
                  AV73TKT_fo[1-1] = GXutil.substring( AV95hot0_t, 3, 2) ;
               }
               else
               {
                  AV73TKT_fo[1-1] = GXutil.substring( AV95hot0_t, 1, 2) ;
               }
            }
            AV239GXLvl = (byte)(0) ;
            /* Using cursor P005U17 */
            pr_default.execute(15, new Object[] {A963ISOC, A964CiaCod, AV91hot0_p, AV92hot0_c, AV93hot0_i, AV94hot0_n, AV95hot0_t, AV96hot0_d});
            while ( (pr_default.getStatus(15) != 101) )
            {
               A970DATA = P005U17_A970DATA[0] ;
               A969TIPO_V = P005U17_A969TIPO_V[0] ;
               A968NUM_BI = P005U17_A968NUM_BI[0] ;
               A967IATA = P005U17_A967IATA[0] ;
               A966CODE = P005U17_A966CODE[0] ;
               A965PER_NA = P005U17_A965PER_NA[0] ;
               A971ISeq = P005U17_A971ISeq[0] ;
               A926Tariff = P005U17_A926Tariff[0] ;
               n926Tariff = P005U17_n926Tariff[0] ;
               AV239GXLvl = (byte)(1) ;
               AV90TKT_Fa = AV90TKT_Fa + GXutil.trim( A926Tariff) + "," ;
               pr_default.readNext(15);
            }
            pr_default.close(15);
            if ( ( AV239GXLvl == 0 ) )
            {
            }
            AV62sdb = "RFN solicitado|" + GXutil.trim( AV94hot0_n) ;
            GXv_char11[0] = AV62sdb ;
            GXv_char10[0] = AV22FileNa ;
            GXv_char4[0] = AV84Versao ;
            GXv_char3[0] = AV17DebugM ;
            new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
            prethandling.this.AV62sdb = GXv_char11[0] ;
            prethandling.this.AV22FileNa = GXv_char10[0] ;
            prethandling.this.AV84Versao = GXv_char4[0] ;
            prethandling.this.AV17DebugM = GXv_char3[0] ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         pr_default.readNext(13);
      }
      pr_default.close(13);
      if ( ( AV237GXLvl == 0 ) )
      {
         AV114SCETp = "I" ;
         AV61SCETex = "RFN ignorado|" + GXutil.trim( AV94hot0_n) + "|Documento original n�o encontrado no Hist�rico" ;
         AV188cnt_n = (int)(AV188cnt_n+1) ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     Ticket "+GXutil.trim( AV94hot0_n)+", issued on "+AV213DIRD+"(YYMMDD), not found on Historical Data" );
         }
      }
   }

   public void S131( )
   {
      /* 'CARREGAARRAY' Routine */
      context.msgStatus( "  Loading Auxiliary Tables..." );
      AV52qtdAir = 0 ;
      AV34i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 3 ) )
         {
            AV9AirptCo[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P005U18 */
      pr_default.execute(16);
      while ( (pr_default.getStatus(16) != 101) )
      {
         A1436ListC = P005U18_A1436ListC[0] ;
         n1436ListC = P005U18_n1436ListC[0] ;
         A1438ListA = P005U18_A1438ListA[0] ;
         A1437ListA = P005U18_A1437ListA[0] ;
         if ( ( AV52qtdAir < 20000 ) )
         {
            AV52qtdAir = (int)(AV52qtdAir+1) ;
            AV9AirptCo[AV52qtdAir-1][1-1] = A1437ListA ;
            AV9AirptCo[AV52qtdAir-1][2-1] = A1438ListA ;
            AV9AirptCo[AV52qtdAir-1][3-1] = GXutil.trim( GXutil.str( A1436ListC, 4, 2)) ;
         }
         AV34i = (int)(AV34i+1) ;
         pr_default.readNext(16);
      }
      pr_default.close(16);
      if ( ( AV34i != AV52qtdAir ) )
      {
         AV114SCETp = "E" ;
         AV61SCETex = "Alerta|Estouro da capacidade de Aeroportos|Carregados " + GXutil.trim( GXutil.str( AV52qtdAir, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV34i, 10, 0)) ;
         /* Execute user subroutine: S211 */
         S211 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV51qtdAir = 0 ;
      AV34i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 2 ) )
         {
            AV85vFareB[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P005U19 */
      pr_default.execute(17);
      while ( (pr_default.getStatus(17) != 101) )
      {
         A1433FareB = P005U19_A1433FareB[0] ;
         A1432FareA = P005U19_A1432FareA[0] ;
         if ( ( AV51qtdAir < 20000 ) )
         {
            AV51qtdAir = (int)(AV51qtdAir+1) ;
            AV85vFareB[AV51qtdAir-1][1-1] = A1432FareA ;
            AV85vFareB[AV51qtdAir-1][2-1] = A1433FareB ;
         }
         AV34i = (int)(AV34i+1) ;
         pr_default.readNext(17);
      }
      pr_default.close(17);
      if ( ( AV34i != AV51qtdAir ) )
      {
         AV114SCETp = "E" ;
         AV61SCETex = "Alerta|Estouro da capacidade de excess�es de FareBasis|Carregadas " + GXutil.trim( GXutil.str( AV51qtdAir, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV34i, 10, 0)) ;
         /* Execute user subroutine: S211 */
         S211 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV53qtdCRS = 0 ;
      AV34i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 1000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 2 ) )
         {
            AV83vCRSs[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P005U20 */
      pr_default.execute(18);
      while ( (pr_default.getStatus(18) != 101) )
      {
         A1458rfGDS = P005U20_A1458rfGDS[0] ;
         n1458rfGDS = P005U20_n1458rfGDS[0] ;
         A1233EmpCo = P005U20_A1233EmpCo[0] ;
         A1404GdsCo = P005U20_A1404GdsCo[0] ;
         if ( ( AV53qtdCRS < 1000 ) )
         {
            AV53qtdCRS = (int)(AV53qtdCRS+1) ;
            AV83vCRSs[AV53qtdCRS-1][1-1] = A1233EmpCo ;
            AV83vCRSs[AV53qtdCRS-1][2-1] = A1404GdsCo ;
         }
         AV34i = (int)(AV34i+1) ;
         pr_default.readNext(18);
      }
      pr_default.close(18);
      if ( ( AV34i != AV53qtdCRS ) )
      {
         AV114SCETp = "E" ;
         AV61SCETex = "Alerta|Estouro da capacidade de CRSs para Reembolso|Carregados " + GXutil.trim( GXutil.str( AV53qtdCRS, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV34i, 10, 0)) ;
         /* Execute user subroutine: S211 */
         S211 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S211( )
   {
      /* 'GRAVALOG' Routine */
      AV45now = GXutil.now(true, false) ;
      /*
         INSERT RECORD ON TABLE SCEVENTS

      */
      A1298SCETk = AV70TDNR ;
      n1298SCETk = false ;
      A1299SCEDa = AV45now ;
      n1299SCEDa = false ;
      A1300SCECR = AV15CRS ;
      n1300SCECR = false ;
      A1301SCEAi = AV67TACN ;
      n1301SCEAi = false ;
      A1303SCERE = AV22FileNa ;
      n1303SCERE = false ;
      A1304SCEAi = "" ;
      n1304SCEAi = false ;
      A1305SCETe = AV61SCETex ;
      n1305SCETe = false ;
      A1307SCEIa = AV8AGTN ;
      n1307SCEIa = false ;
      A1310SCETp = AV114SCETp ;
      n1310SCETp = false ;
      A1306SCEAP = "RETH" ;
      n1306SCEAP = false ;
      A1311SCEVe = AV84Versao ;
      n1311SCEVe = false ;
      /* Using cursor P005U21 */
      pr_default.execute(19, new Object[] {new Boolean(n1298SCETk), A1298SCETk, new Boolean(n1299SCEDa), A1299SCEDa, new Boolean(n1300SCECR), A1300SCECR, new Boolean(n1301SCEAi), A1301SCEAi, new Boolean(n1303SCERE), A1303SCERE, new Boolean(n1304SCEAi), A1304SCEAi, new Boolean(n1305SCETe), A1305SCETe, new Boolean(n1306SCEAP), A1306SCEAP, new Boolean(n1307SCEIa), A1307SCEIa, new Boolean(n1310SCETp), A1310SCETp, new Boolean(n1311SCEVe), A1311SCEVe});
      /* Retrieving last key number assigned */
      /* Using cursor P005U22 */
      pr_default.execute(20);
      A1312SCEId = P005U22_A1312SCEId[0] ;
      n1312SCEId = P005U22_n1312SCEId[0] ;
      pr_default.close(20);
      if ( (pr_default.getStatus(19) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S171( )
   {
      /* 'CHANGETDAM' Routine */
      AV207Tot_F = (double)(GXutil.val( AV111TDAM, ".")-AV202TAX_T+GXutil.val( AV76TMFA, ".")) ;
      AV62sdb = "Totais do RFN original=" + AV111TDAM + "-" + GXutil.trim( GXutil.str( AV202TAX_T, 14, 0)) + "+" + AV76TMFA + "=" + GXutil.trim( GXutil.str( AV207Tot_F, 14, 0)) ;
      GXv_char11[0] = AV62sdb ;
      GXv_char10[0] = AV22FileNa ;
      GXv_char4[0] = AV84Versao ;
      GXv_char3[0] = AV17DebugM ;
      new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
      prethandling.this.AV62sdb = GXv_char11[0] ;
      prethandling.this.AV22FileNa = GXv_char10[0] ;
      prethandling.this.AV84Versao = GXv_char4[0] ;
      prethandling.this.AV17DebugM = GXv_char3[0] ;
      AV112v = (double)(DecimalUtil.decToDouble(GXutil.truncDecimal( DecimalUtil.doubleToDec((AV106rfBas*AV207Tot_F)/ (double) (100)), 0))) ;
      if ( ( GXutil.strcmp(AV108rfBas, "1") == 0 ) && ( AV112v < GXutil.val( AV97RFN_Mu, ".") ) )
      {
         AV62sdb = "Usar multa menor, valor em moeda (" + AV97RFN_Mu ;
         AV97RFN_Mu = GXutil.padl( GXutil.trim( GXutil.str( AV112v, 10, 0)), (short)(11), "0") ;
      }
      else
      {
         AV62sdb = "Usar multa calculada, valor em moeda =(" + AV97RFN_Mu ;
      }
      AV62sdb = AV62sdb + "), em % (" + GXutil.trim( GXutil.str( AV106rfBas, 5, 2)) + "% = " + GXutil.trim( GXutil.str( AV112v, 14, 0)) + ")" ;
      GXv_char11[0] = AV62sdb ;
      GXv_char10[0] = AV22FileNa ;
      GXv_char4[0] = AV84Versao ;
      GXv_char3[0] = AV17DebugM ;
      new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
      prethandling.this.AV62sdb = GXv_char11[0] ;
      prethandling.this.AV22FileNa = GXv_char10[0] ;
      prethandling.this.AV84Versao = GXv_char4[0] ;
      prethandling.this.AV17DebugM = GXv_char3[0] ;
      if ( ( GXutil.strcmp(GXutil.trim( AV203IT05P), "") != 0 ) )
      {
         /* Execute user subroutine: S261 */
         S261 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV204IT05P), "") != 0 ) && ( GXutil.val( AV97RFN_Mu, ".") > 0 ) )
         {
            /* Execute user subroutine: S271 */
            S271 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
      if ( ( GXutil.val( AV97RFN_Mu, ".") > 0 ) )
      {
         /* Execute user subroutine: S281 */
         S281 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV208IT08p), "") != 0 ) || ( GXutil.strcmp(GXutil.trim( AV209IT08p), "") != 0 ) )
      {
         /* Execute user subroutine: S291 */
         S291 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
      }
      AV112v = (double)(GXutil.val( AV111TDAM, ".")+GXutil.val( AV76TMFA, ".")-GXutil.val( AV110NEW_T, ".")) ;
      if ( ( AV112v < 0 ) )
      {
         AV112v = 0 ;
         AV62sdb = "ALERTA: TDAM ficou negativo no doc. " + AV70TDNR + ", TDAM=" + AV111TDAM + ", TMFA=" + AV76TMFA + ", NEW TMFA=" + AV110NEW_T ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     "+AV62sdb );
         }
      }
      AV60s = GXutil.padl( GXutil.trim( GXutil.str( AV112v, 10, 0)), (short)(11), "0") ;
      AV243GXLvl = (byte)(0) ;
      /* Using cursor P005U23 */
      pr_default.execute(21, new Object[] {AV37IT05po, AV37IT05po});
      while ( (pr_default.getStatus(21) != 101) )
      {
         A993HntUsu = P005U23_A993HntUsu[0] ;
         n993HntUsu = P005U23_n993HntUsu[0] ;
         A997HntSeq = P005U23_A997HntSeq[0] ;
         A994HntLin = P005U23_A994HntLin[0] ;
         n994HntLin = P005U23_n994HntLin[0] ;
         A998HntSub = P005U23_A998HntSub[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A993HntUsu, 1, 8), "RETClone") == 0 ) )
         {
            AV243GXLvl = (byte)(1) ;
            AV138HntUs = A993HntUsu ;
            AV113AuxLi = A994HntLin ;
            GXt_char9 = AV113AuxLi ;
            GXv_char11[0] = AV113AuxLi ;
            GXv_int12[0] = (short)(87) ;
            GXv_char10[0] = AV60s ;
            GXv_char4[0] = GXt_char9 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
            prethandling.this.AV113AuxLi = GXv_char11[0] ;
            prethandling.this.AV60s = GXv_char10[0] ;
            prethandling.this.GXt_char9 = GXv_char4[0] ;
            AV113AuxLi = GXt_char9 ;
            if ( ( AV214FlagC == 1 ) && ( AV161COMMI <= 100 ) )
            {
               AV215ComVa = AV161COMMI ;
               /* Execute user subroutine: S3020 */
               S3020 ();
               if ( returnInSub )
               {
                  pr_default.close(21);
                  returnInSub = true;
                  if (true) return;
               }
               if ( ( GXutil.strcmp(AV14CORT, AV191new_C) == 0 ) )
               {
                  AV62sdb = "CORT mantido como " + AV14CORT ;
               }
               else
               {
                  AV62sdb = "CORT alterado de " + AV14CORT + " para " + AV191new_C ;
                  GXt_char9 = AV113AuxLi ;
                  GXv_char11[0] = AV113AuxLi ;
                  GXv_int12[0] = (short)(177) ;
                  GXv_char10[0] = AV191new_C ;
                  GXv_char4[0] = GXt_char9 ;
                  new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                  prethandling.this.AV113AuxLi = GXv_char11[0] ;
                  prethandling.this.AV191new_C = GXv_char10[0] ;
                  prethandling.this.GXt_char9 = GXv_char4[0] ;
                  AV113AuxLi = GXt_char9 ;
                  AV14CORT = AV191new_C ;
                  AV12COAM = "00000000000" ;
                  GXt_char9 = AV113AuxLi ;
                  GXv_char11[0] = AV113AuxLi ;
                  GXv_int12[0] = (short)(182) ;
                  GXv_char10[0] = AV12COAM ;
                  GXv_char4[0] = GXt_char9 ;
                  new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                  prethandling.this.AV113AuxLi = GXv_char11[0] ;
                  prethandling.this.AV12COAM = GXv_char10[0] ;
                  prethandling.this.GXt_char9 = GXv_char4[0] ;
                  AV113AuxLi = GXt_char9 ;
               }
               GXv_char11[0] = AV62sdb ;
               GXv_char10[0] = AV22FileNa ;
               GXv_char4[0] = AV84Versao ;
               GXv_char3[0] = AV17DebugM ;
               new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
               prethandling.this.AV62sdb = GXv_char11[0] ;
               prethandling.this.AV22FileNa = GXv_char10[0] ;
               prethandling.this.AV84Versao = GXv_char4[0] ;
               prethandling.this.AV17DebugM = GXv_char3[0] ;
            }
            A994HntLin = AV113AuxLi ;
            n994HntLin = false ;
            AV114SCETp = "A" ;
            AV61SCETex = "RFN Modificado|" + AV15CRS + "|Org.=" + AV111TDAM + "/CP=" + AV76TMFA + ", Mod.=" + AV60s + "/CP=" + AV110NEW_T ;
            /* Execute user subroutine: S211 */
            S211 ();
            if ( returnInSub )
            {
               pr_default.close(21);
               returnInSub = true;
               if (true) return;
            }
            AV62sdb = AV61SCETex ;
            GXv_char11[0] = AV62sdb ;
            GXv_char10[0] = AV22FileNa ;
            GXv_char4[0] = AV84Versao ;
            GXv_char3[0] = AV17DebugM ;
            new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
            prethandling.this.AV62sdb = GXv_char11[0] ;
            prethandling.this.AV22FileNa = GXv_char10[0] ;
            prethandling.this.AV84Versao = GXv_char4[0] ;
            prethandling.this.AV17DebugM = GXv_char3[0] ;
            /* Using cursor P005U24 */
            pr_default.execute(22, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(21);
      }
      pr_default.close(21);
      if ( ( AV243GXLvl == 0 ) )
      {
         AV62sdb = "Erro: IT05 n�o encontrado em ChangeTDAM (" + AV70TDNR + ")" ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     "+AV62sdb );
         }
      }
   }

   public void S3020( )
   {
      /* 'MAKECORT' Routine */
      if ( ( AV215ComVa > 100 ) )
      {
         AV215ComVa = 0 ;
      }
      AV191new_C = GXutil.str( AV215ComVa*100, 5, 0) ;
      AV191new_C = GXutil.strReplace( AV191new_C, " ", "0") ;
      AV191new_C = GXutil.left( AV191new_C, 5) ;
   }

   public void S261( )
   {
      /* 'REDOCP' Routine */
      AV244GXLvl = (byte)(0) ;
      /* Using cursor P005U25 */
      pr_default.execute(23, new Object[] {AV203IT05P, AV203IT05P});
      while ( (pr_default.getStatus(23) != 101) )
      {
         A993HntUsu = P005U25_A993HntUsu[0] ;
         n993HntUsu = P005U25_n993HntUsu[0] ;
         A997HntSeq = P005U25_A997HntSeq[0] ;
         A994HntLin = P005U25_A994HntLin[0] ;
         n994HntLin = P005U25_n994HntLin[0] ;
         A998HntSub = P005U25_A998HntSub[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A993HntUsu, 1, 8), "RETClone") == 0 ) )
         {
            AV244GXLvl = (byte)(1) ;
            AV113AuxLi = A994HntLin ;
            AV205iAdd = (short)(0) ;
            while ( ( AV205iAdd <= 84 ) )
            {
               AV206iCoun = (short)(30) ;
               while ( ( AV206iCoun <= 68 ) )
               {
                  AV34i = (int)(AV206iCoun+AV205iAdd) ;
                  AV39j = (int)(AV34i+8) ;
                  if ( ( GXutil.strcmp(GXutil.substring( AV113AuxLi, AV34i, 2), "CP") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(AV97RFN_Mu, "00000000000") == 0 ) )
                     {
                        GXt_char9 = AV113AuxLi ;
                        GXv_char11[0] = AV113AuxLi ;
                        GXv_int12[0] = (short)(AV34i) ;
                        GXv_char10[0] = "  " ;
                        GXv_char4[0] = GXt_char9 ;
                        new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                        prethandling.this.AV113AuxLi = GXv_char11[0] ;
                        prethandling.this.AV34i = GXv_int12[0] ;
                        prethandling.this.GXt_char9 = GXv_char4[0] ;
                        AV113AuxLi = GXt_char9 ;
                     }
                     if ( ( GXutil.strcmp(AV109rfFop, "0") == 0 ) )
                     {
                        GXt_char9 = AV113AuxLi ;
                        GXv_char11[0] = AV113AuxLi ;
                        GXv_int12[0] = (short)(AV39j) ;
                        GXv_char10[0] = AV97RFN_Mu ;
                        GXv_char4[0] = GXt_char9 ;
                        new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                        prethandling.this.AV113AuxLi = GXv_char11[0] ;
                        prethandling.this.AV39j = GXv_int12[0] ;
                        prethandling.this.AV97RFN_Mu = GXv_char10[0] ;
                        prethandling.this.GXt_char9 = GXv_char4[0] ;
                        AV113AuxLi = GXt_char9 ;
                        AV110NEW_T = AV97RFN_Mu ;
                        AV62sdb = "Recalculada a multa: " + AV110NEW_T ;
                        GXv_char11[0] = AV62sdb ;
                        GXv_char10[0] = AV22FileNa ;
                        GXv_char4[0] = AV84Versao ;
                        GXv_char3[0] = AV17DebugM ;
                        new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
                        prethandling.this.AV62sdb = GXv_char11[0] ;
                        prethandling.this.AV22FileNa = GXv_char10[0] ;
                        prethandling.this.AV84Versao = GXv_char4[0] ;
                        prethandling.this.AV17DebugM = GXv_char3[0] ;
                     }
                     A994HntLin = AV113AuxLi ;
                     n994HntLin = false ;
                     AV97RFN_Mu = "" ;
                     if (true) break;
                  }
                  AV206iCoun = (short)(AV206iCoun+19) ;
               }
               if ( ( GXutil.strcmp(AV97RFN_Mu, "") == 0 ) )
               {
                  if (true) break;
               }
               AV205iAdd = (short)(AV205iAdd+84) ;
            }
            /* Using cursor P005U26 */
            pr_default.execute(24, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(23);
      }
      pr_default.close(23);
      if ( ( AV244GXLvl == 0 ) )
      {
         AV62sdb = "Erro: IT05 (CP) n�o encontrado em ChangeTDAM" ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     "+AV62sdb );
         }
         AV97RFN_Mu = "" ;
      }
   }

   public void S271( )
   {
      /* 'REDOBLANK' Routine */
      AV245GXLvl = (byte)(0) ;
      /* Using cursor P005U27 */
      pr_default.execute(25, new Object[] {AV204IT05P, AV204IT05P});
      while ( (pr_default.getStatus(25) != 101) )
      {
         A993HntUsu = P005U27_A993HntUsu[0] ;
         n993HntUsu = P005U27_n993HntUsu[0] ;
         A997HntSeq = P005U27_A997HntSeq[0] ;
         A994HntLin = P005U27_A994HntLin[0] ;
         n994HntLin = P005U27_n994HntLin[0] ;
         A998HntSub = P005U27_A998HntSub[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A993HntUsu, 1, 8), "RETClone") == 0 ) )
         {
            AV245GXLvl = (byte)(1) ;
            AV113AuxLi = A994HntLin ;
            AV205iAdd = (short)(0) ;
            while ( ( AV205iAdd <= 84 ) )
            {
               AV206iCoun = (short)(30) ;
               while ( ( AV206iCoun <= 68 ) )
               {
                  AV34i = (int)(AV206iCoun+AV205iAdd) ;
                  AV39j = (int)(AV34i+8) ;
                  if ( ( GXutil.strcmp(GXutil.substring( AV113AuxLi, AV34i, 2), "  ") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(AV97RFN_Mu, "00000000000") == 0 ) )
                     {
                        GXt_char9 = AV113AuxLi ;
                        GXv_char11[0] = AV113AuxLi ;
                        GXv_int12[0] = (short)(AV34i) ;
                        GXv_char10[0] = "  " ;
                        GXv_char4[0] = GXt_char9 ;
                        new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                        prethandling.this.AV113AuxLi = GXv_char11[0] ;
                        prethandling.this.AV34i = GXv_int12[0] ;
                        prethandling.this.GXt_char9 = GXv_char4[0] ;
                        AV113AuxLi = GXt_char9 ;
                     }
                     else
                     {
                        GXt_char9 = AV113AuxLi ;
                        GXv_char11[0] = AV113AuxLi ;
                        GXv_int12[0] = (short)(AV34i) ;
                        GXv_char10[0] = "CP" ;
                        GXv_char4[0] = GXt_char9 ;
                        new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                        prethandling.this.AV113AuxLi = GXv_char11[0] ;
                        prethandling.this.AV34i = GXv_int12[0] ;
                        prethandling.this.GXt_char9 = GXv_char4[0] ;
                        AV113AuxLi = GXt_char9 ;
                     }
                     GXt_char9 = AV113AuxLi ;
                     GXv_char11[0] = AV113AuxLi ;
                     GXv_int12[0] = (short)(AV39j) ;
                     GXv_char10[0] = AV97RFN_Mu ;
                     GXv_char4[0] = GXt_char9 ;
                     new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
                     prethandling.this.AV113AuxLi = GXv_char11[0] ;
                     prethandling.this.AV39j = GXv_int12[0] ;
                     prethandling.this.AV97RFN_Mu = GXv_char10[0] ;
                     prethandling.this.GXt_char9 = GXv_char4[0] ;
                     AV113AuxLi = GXt_char9 ;
                     AV110NEW_T = AV97RFN_Mu ;
                     AV97RFN_Mu = "" ;
                     AV62sdb = "Inserida a multa: " + AV110NEW_T ;
                     GXv_char11[0] = AV62sdb ;
                     GXv_char10[0] = AV22FileNa ;
                     GXv_char4[0] = AV84Versao ;
                     GXv_char3[0] = AV17DebugM ;
                     new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
                     prethandling.this.AV62sdb = GXv_char11[0] ;
                     prethandling.this.AV22FileNa = GXv_char10[0] ;
                     prethandling.this.AV84Versao = GXv_char4[0] ;
                     prethandling.this.AV17DebugM = GXv_char3[0] ;
                     A994HntLin = AV113AuxLi ;
                     n994HntLin = false ;
                     if (true) break;
                  }
                  AV206iCoun = (short)(AV206iCoun+19) ;
               }
               if ( ( GXutil.strcmp(AV97RFN_Mu, "") == 0 ) )
               {
                  if (true) break;
               }
               AV205iAdd = (short)(AV205iAdd+84) ;
            }
            /* Using cursor P005U28 */
            pr_default.execute(26, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(25);
      }
      pr_default.close(25);
      if ( ( AV245GXLvl == 0 ) )
      {
         AV62sdb = "Erro: IT05 (Blank) n�o encontrado em ChangeTDAM" ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     "+AV62sdb );
         }
         AV97RFN_Mu = "" ;
      }
   }

   public void S281( )
   {
      /* 'REDONEW' Routine */
      AV113AuxLi = GXutil.substring( "500000000000000000                   00000000000        00000000000        0000000000000000000000BRL200000000000         00000000000        00000000000        00000000000      0000000000000000      0000000000000000      0000000000000000  00000000000      ", 1, 255) ;
      GXt_char9 = AV113AuxLi ;
      GXv_char11[0] = AV113AuxLi ;
      GXv_int12[0] = (short)(38) ;
      GXv_char10[0] = AV97RFN_Mu ;
      GXv_char4[0] = GXt_char9 ;
      new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
      prethandling.this.AV113AuxLi = GXv_char11[0] ;
      prethandling.this.AV97RFN_Mu = GXv_char10[0] ;
      prethandling.this.GXt_char9 = GXv_char4[0] ;
      AV113AuxLi = GXt_char9 ;
      GXt_char9 = AV113AuxLi ;
      GXv_char11[0] = AV113AuxLi ;
      GXv_int12[0] = (short)(2) ;
      GXv_char10[0] = AV81TRNN_O ;
      GXv_char4[0] = GXt_char9 ;
      new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
      prethandling.this.AV113AuxLi = GXv_char11[0] ;
      prethandling.this.AV81TRNN_O = GXv_char10[0] ;
      prethandling.this.GXt_char9 = GXv_char4[0] ;
      AV113AuxLi = GXt_char9 ;
      GXt_char9 = AV113AuxLi ;
      GXv_char11[0] = AV113AuxLi ;
      GXv_int12[0] = (short)(30) ;
      GXv_char10[0] = "CP      " ;
      GXv_char4[0] = GXt_char9 ;
      new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
      prethandling.this.AV113AuxLi = GXv_char11[0] ;
      prethandling.this.GXt_char9 = GXv_char4[0] ;
      AV113AuxLi = GXt_char9 ;
      AV137Last_ = GXutil.substring( AV137Last_, 1, 7) + "6" ;
      /*
         INSERT RECORD ON TABLE RETSPECTEMP

      */
      A997HntSeq = AV137Last_ ;
      A993HntUsu = AV138HntUs ;
      n993HntUsu = false ;
      A994HntLin = AV113AuxLi ;
      n994HntLin = false ;
      AV62sdb = "Inserida linha adicional para comportar CP=" + AV97RFN_Mu ;
      /* Using cursor P005U29 */
      pr_default.execute(27, new Object[] {A997HntSeq, new Byte(A998HntSub), new Boolean(n993HntUsu), A993HntUsu, new Boolean(n994HntLin), A994HntLin});
      if ( (pr_default.getStatus(27) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         AV62sdb = "Erro: duplica��o da linha de CP adicional! (" + AV70TDNR + ")" ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     "+AV62sdb );
         }
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      GXv_char11[0] = AV62sdb ;
      GXv_char10[0] = AV22FileNa ;
      GXv_char4[0] = AV84Versao ;
      GXv_char3[0] = AV17DebugM ;
      new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
      prethandling.this.AV62sdb = GXv_char11[0] ;
      prethandling.this.AV22FileNa = GXv_char10[0] ;
      prethandling.this.AV84Versao = GXv_char4[0] ;
      prethandling.this.AV17DebugM = GXv_char3[0] ;
      AV110NEW_T = AV97RFN_Mu ;
      AV97RFN_Mu = "" ;
   }

   public void S291( )
   {
      /* 'REDOFPAM' Routine */
      if ( ( GXutil.strcmp(AV208IT08p, "") != 0 ) )
      {
         AV60s = AV208IT08p ;
         AV39j = AV210IT08S ;
      }
      else
      {
         AV60s = AV209IT08p ;
         AV39j = AV211IT08S ;
      }
      AV246GXLvl = (byte)(0) ;
      /* Using cursor P005U30 */
      pr_default.execute(28, new Object[] {AV60s, AV60s});
      while ( (pr_default.getStatus(28) != 101) )
      {
         A993HntUsu = P005U30_A993HntUsu[0] ;
         n993HntUsu = P005U30_n993HntUsu[0] ;
         A997HntSeq = P005U30_A997HntSeq[0] ;
         A994HntLin = P005U30_A994HntLin[0] ;
         n994HntLin = P005U30_n994HntLin[0] ;
         A998HntSub = P005U30_A998HntSub[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A993HntUsu, 1, 8), "RETClone") == 0 ) )
         {
            AV246GXLvl = (byte)(1) ;
            AV113AuxLi = A994HntLin ;
            if ( ( AV39j == 1 ) )
            {
               AV212FPAM = GXutil.substring( AV113AuxLi, 27, 11) ;
            }
            else
            {
               AV212FPAM = GXutil.substring( AV113AuxLi, 150, 11) ;
            }
            AV112v = (double)(GXutil.val( AV212FPAM, ".")+GXutil.val( AV76TMFA, ".")-GXutil.val( AV110NEW_T, ".")) ;
            if ( ( AV112v < 0 ) )
            {
               AV112v = 0 ;
               AV62sdb = "ALERTA: FPAM ficou negativo no doc. " + AV70TDNR + ", FPAM=" + AV212FPAM + ", TMFA=" + AV76TMFA + ", NEW TMFA=" + AV110NEW_T ;
               GXv_char11[0] = AV62sdb ;
               GXv_char10[0] = AV22FileNa ;
               GXv_char4[0] = AV84Versao ;
               GXv_char3[0] = AV17DebugM ;
               new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
               prethandling.this.AV62sdb = GXv_char11[0] ;
               prethandling.this.AV22FileNa = GXv_char10[0] ;
               prethandling.this.AV84Versao = GXv_char4[0] ;
               prethandling.this.AV17DebugM = GXv_char3[0] ;
               if ( ( AV189Verbo == 1 ) )
               {
                  context.msgStatus( "     "+AV62sdb );
               }
            }
            AV60s = GXutil.padl( GXutil.trim( GXutil.str( AV112v, 10, 0)), (short)(11), "0") ;
            if ( ( AV39j == 1 ) )
            {
               GXt_char9 = AV113AuxLi ;
               GXv_char11[0] = AV113AuxLi ;
               GXv_int12[0] = (short)(27) ;
               GXv_char10[0] = AV60s ;
               GXv_char4[0] = GXt_char9 ;
               new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
               prethandling.this.AV113AuxLi = GXv_char11[0] ;
               prethandling.this.AV60s = GXv_char10[0] ;
               prethandling.this.GXt_char9 = GXv_char4[0] ;
               AV113AuxLi = GXt_char9 ;
            }
            else
            {
               GXt_char9 = AV113AuxLi ;
               GXv_char11[0] = AV113AuxLi ;
               GXv_int12[0] = (short)(150) ;
               GXv_char10[0] = AV60s ;
               GXv_char4[0] = GXt_char9 ;
               new pr2insertintostring(remoteHandle, context).execute( GXv_char11, GXv_int12, GXv_char10, GXv_char4) ;
               prethandling.this.AV113AuxLi = GXv_char11[0] ;
               prethandling.this.AV60s = GXv_char10[0] ;
               prethandling.this.GXt_char9 = GXv_char4[0] ;
               AV113AuxLi = GXt_char9 ;
            }
            A994HntLin = AV113AuxLi ;
            n994HntLin = false ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P005U31 */
            pr_default.execute(29, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
            if (true) break;
            /* Using cursor P005U32 */
            pr_default.execute(30, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(28);
      }
      pr_default.close(28);
      if ( ( AV246GXLvl == 0 ) )
      {
         AV62sdb = "Erro: IT08 n�o reencontrado em RedoFOPAM" ;
         GXv_char11[0] = AV62sdb ;
         GXv_char10[0] = AV22FileNa ;
         GXv_char4[0] = AV84Versao ;
         GXv_char3[0] = AV17DebugM ;
         new pretdbg(remoteHandle, context).execute( GXv_char11, GXv_char10, GXv_char4, GXv_char3) ;
         prethandling.this.AV62sdb = GXv_char11[0] ;
         prethandling.this.AV22FileNa = GXv_char10[0] ;
         prethandling.this.AV84Versao = GXv_char4[0] ;
         prethandling.this.AV17DebugM = GXv_char3[0] ;
         if ( ( AV189Verbo == 1 ) )
         {
            context.msgStatus( "     "+AV62sdb );
         }
      }
   }

   protected void cleanup( )
   {
      this.aP0[0] = prethandling.this.AV17DebugM;
      this.aP1[0] = prethandling.this.AV24FileSo;
      this.aP2[0] = prethandling.this.AV223Menu;
      Application.commit(context, remoteHandle, "DEFAULT", "prethandling");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV84Versao = "" ;
      AV64SplitR = (byte)(0) ;
      AV189Verbo = (byte)(0) ;
      AV214FlagC = (byte)(0) ;
      returnInSub = false ;
      AV224REVN = "" ;
      AV22FileNa = "" ;
      AV23FileNo = 0 ;
      AV10AppCod = "" ;
      AV86vHntSe = 0 ;
      AV28Flag_I = (byte)(0) ;
      AV81TRNN_O = "" ;
      AV40Linha = "" ;
      GXt_int5 = (short)(0) ;
      AV26FileNo = 0 ;
      AV199cnt_H = 0 ;
      scmdbuf = "" ;
      AV15CRS = "" ;
      P005U3_A1440RETHe = new String[] {""} ;
      P005U3_A1439RETHe = new String[] {""} ;
      P005U3_n1439RETHe = new boolean[] {false} ;
      P005U3_A1441RETHe = new int[1] ;
      A1440RETHe = "" ;
      A1439RETHe = "" ;
      n1439RETHe = false ;
      A1441RETHe = 0 ;
      AV63sHntSe = "" ;
      AV38IT0Zpo = "" ;
      GX_INS207 = 0 ;
      A997HntSeq = "" ;
      A989HntRec = "" ;
      AV198IT0Z_ = "" ;
      n989HntRec = false ;
      A993HntUsu = "" ;
      n993HntUsu = false ;
      A994HntLin = "" ;
      n994HntLin = false ;
      A998HntSub = (byte)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV183Commi = (short)(0) ;
      AV188cnt_n = 0 ;
      GXt_char2 = "" ;
      AV186cnt_C = 0 ;
      GXt_char1 = "" ;
      AV187cnt_C = 0 ;
      GXt_char6 = "" ;
      AV185cnt_P = 0 ;
      AV184cnt_P = 0 ;
      AV194cnt_S = 0 ;
      AV200cnt_C = 0 ;
      GXt_char8 = "" ;
      AV201cnt_C = 0 ;
      GXt_char7 = "" ;
      AV77TPST = "" ;
      AV50FileDe = "" ;
      AV62sdb = "" ;
      AV80TRNN_C = "" ;
      AV97RFN_Mu = "" ;
      AV29Flag_I = (byte)(0) ;
      AV30Flag_I = (byte)(0) ;
      AV76TMFA = "" ;
      AV110NEW_T = "" ;
      AV204IT05P = "" ;
      AV203IT05P = "" ;
      AV208IT08p = "" ;
      AV209IT08p = "" ;
      AV210IT08S = (byte)(0) ;
      AV211IT08S = (byte)(0) ;
      AV54RCID = "" ;
      AV35IT01po = "" ;
      AV8AGTN = "" ;
      AV78TRNC = "" ;
      AV70TDNR = "" ;
      AV67TACN = "" ;
      AV144DAIS = "" ;
      AV190STAT = "" ;
      AV48ORAC = "" ;
      AV36IT02po = "" ;
      AV57RFNSki = (byte)(0) ;
      AV14CORT = "" ;
      AV12COAM = "" ;
      AV37IT05po = "" ;
      AV191new_C = "" ;
      AV137Last_ = "" ;
      AV202TAX_T = 0 ;
      AV111TDAM = "" ;
      AV205iAdd = (short)(0) ;
      AV206iCoun = (short)(0) ;
      AV34i = 0 ;
      AV39j = 0 ;
      AV21FBTD = "" ;
      AV52qtdAir = 0 ;
      AV41ListAi = "" ;
      AV9AirptCo = new String [20000][3] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 3 ) )
         {
            AV9AirptCo[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      AV42ListAi = "" ;
      AV43ListCo = "" ;
      AV27Flag_F = (short)(0) ;
      AV51qtdAir = 0 ;
      AV19FareAi = "" ;
      AV85vFareB = new String [20000][2] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 2 ) )
         {
            AV85vFareB[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      AV20FareBa = "" ;
      AV69TamFar = (short)(0) ;
      AV82uFBTD = "" ;
      AV114SCETp = "" ;
      AV61SCETex = "" ;
      AV65SubstS = "" ;
      AV66SubstS = "" ;
      AV11Baseco = 0 ;
      P005U7_A993HntUsu = new String[] {""} ;
      P005U7_n993HntUsu = new boolean[] {false} ;
      P005U7_A997HntSeq = new String[] {""} ;
      P005U7_A994HntLin = new String[] {""} ;
      P005U7_n994HntLin = new boolean[] {false} ;
      P005U7_A998HntSub = new byte[1] ;
      AV53qtdCRS = 0 ;
      AV83vCRSs = new String [1000][2] ;
      GX_I = 1 ;
      while ( ( GX_I <= 1000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 2 ) )
         {
            AV83vCRSs[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      AV16RFN_DA = "" ;
      AV55RCPN = "" ;
      AV59RTDN = "" ;
      AV213DIRD = "" ;
      AV56rffopd = "" ;
      AV75TKT_TR = "" ;
      AV216fOK = (byte)(0) ;
      AV197k = (byte)(0) ;
      AV196TKT_f = (byte)(0) ;
      AV31fopid = "" ;
      AV73TKT_fo = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV73TKT_fo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV230GXLvl = (byte)(0) ;
      P005U9_A1450rfFop = new String[] {""} ;
      P005U9_n1450rfFop = new boolean[] {false} ;
      P005U9_A1435fopID = new String[] {""} ;
      P005U9_A1455rfFop = new String[] {""} ;
      P005U9_A1233EmpCo = new String[] {""} ;
      A1450rfFop = "" ;
      n1450rfFop = false ;
      A1435fopID = "" ;
      A1455rfFop = "" ;
      A1233EmpCo = "" ;
      AV109rfFop = "" ;
      AV231GXLvl = (byte)(0) ;
      P005U10_A1450rfFop = new String[] {""} ;
      P005U10_n1450rfFop = new boolean[] {false} ;
      P005U10_A1435fopID = new String[] {""} ;
      P005U10_A1455rfFop = new String[] {""} ;
      P005U10_A1233EmpCo = new String[] {""} ;
      P005U10_A1454rfFop = new String[] {""} ;
      P005U10_n1454rfFop = new boolean[] {false} ;
      P005U10_A1451rfFop = new short[1] ;
      P005U10_n1451rfFop = new boolean[] {false} ;
      P005U10_A1453rfFop = new String[] {""} ;
      P005U10_n1453rfFop = new boolean[] {false} ;
      A1454rfFop = "" ;
      n1454rfFop = false ;
      A1451rfFop = (short)(0) ;
      n1451rfFop = false ;
      A1453rfFop = "" ;
      n1453rfFop = false ;
      AV87dt1 = GXutil.nullDate() ;
      AV88dt2 = GXutil.nullDate() ;
      AV72TKT_DA = "" ;
      AV89RFN_De = (short)(0) ;
      AV103n1 = 0 ;
      AV104n2 = 0 ;
      GXt_int15 = 0 ;
      AV71TKT_Co = "" ;
      GXv_int16 = new int [1] ;
      Gx_date = GXutil.nullDate() ;
      P005U11_A1447rfBas = new String[] {""} ;
      P005U11_A1446rfBas = new String[] {""} ;
      P005U11_A1233EmpCo = new String[] {""} ;
      P005U11_A1449rfBas = new short[1] ;
      P005U11_A1442rfBas = new double[1] ;
      P005U11_n1442rfBas = new boolean[] {false} ;
      P005U11_A1443rfBas = new double[1] ;
      P005U11_n1443rfBas = new boolean[] {false} ;
      P005U11_A1444rfBas = new String[] {""} ;
      P005U11_n1444rfBas = new boolean[] {false} ;
      P005U11_A1445rfBas = new String[] {""} ;
      P005U11_n1445rfBas = new boolean[] {false} ;
      P005U11_A1448rfBas = new String[] {""} ;
      A1447rfBas = "" ;
      A1446rfBas = "" ;
      A1449rfBas = (short)(0) ;
      A1442rfBas = 0 ;
      n1442rfBas = false ;
      A1443rfBas = 0 ;
      n1443rfBas = false ;
      A1444rfBas = "" ;
      n1444rfBas = false ;
      A1445rfBas = "" ;
      n1445rfBas = false ;
      A1448rfBas = "" ;
      AV60s = "" ;
      AV90TKT_Fa = "" ;
      AV99rfBase = "" ;
      AV106rfBas = 0 ;
      AV107rfBas = 0 ;
      AV108rfBas = "" ;
      AV105rfBas = "" ;
      AV234GXLvl = (byte)(0) ;
      P005U12_A1449rfBas = new short[1] ;
      P005U12_A1447rfBas = new String[] {""} ;
      P005U12_A1446rfBas = new String[] {""} ;
      P005U12_A1233EmpCo = new String[] {""} ;
      P005U12_A1442rfBas = new double[1] ;
      P005U12_n1442rfBas = new boolean[] {false} ;
      P005U12_A1443rfBas = new double[1] ;
      P005U12_n1443rfBas = new boolean[] {false} ;
      P005U12_A1444rfBas = new String[] {""} ;
      P005U12_n1444rfBas = new boolean[] {false} ;
      P005U12_A1445rfBas = new String[] {""} ;
      P005U12_n1445rfBas = new boolean[] {false} ;
      P005U12_A1448rfBas = new String[] {""} ;
      GXt_date13 = GXutil.nullDate() ;
      AV112v = 0 ;
      AV235GXLvl = (byte)(0) ;
      P005U13_A1431CaTip = new String[] {""} ;
      P005U13_A1430CaMoe = new String[] {""} ;
      P005U13_A1429CaDat = new java.util.Date[] {GXutil.nullDate()} ;
      P005U13_A1426CaVal = new double[1] ;
      P005U13_n1426CaVal = new boolean[] {false} ;
      A1431CaTip = "" ;
      A1430CaMoe = "" ;
      A1429CaDat = GXutil.nullDate() ;
      A1426CaVal = 0 ;
      n1426CaVal = false ;
      AV236GXLvl = (byte)(0) ;
      P005U14_A1429CaDat = new java.util.Date[] {GXutil.nullDate()} ;
      P005U14_A1431CaTip = new String[] {""} ;
      P005U14_A1430CaMoe = new String[] {""} ;
      P005U14_A1426CaVal = new double[1] ;
      P005U14_n1426CaVal = new boolean[] {false} ;
      AV94hot0_n = "" ;
      AV169NUM_B = 0 ;
      AV237GXLvl = (byte)(0) ;
      P005U15_A964CiaCod = new String[] {""} ;
      P005U15_A963ISOC = new String[] {""} ;
      P005U15_A966CODE = new String[] {""} ;
      P005U15_A902NUM_BI = new long[1] ;
      P005U15_n902NUM_BI = new boolean[] {false} ;
      P005U15_A907CPUI = new String[] {""} ;
      P005U15_n907CPUI = new boolean[] {false} ;
      P005U15_A889TRANS_ = new String[] {""} ;
      P005U15_n889TRANS_ = new boolean[] {false} ;
      P005U15_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005U15_A967IATA = new String[] {""} ;
      P005U15_A968NUM_BI = new String[] {""} ;
      P005U15_A965PER_NA = new String[] {""} ;
      P005U15_A969TIPO_V = new String[] {""} ;
      P005U15_A883TAX_IT = new double[1] ;
      P005U15_n883TAX_IT = new boolean[] {false} ;
      P005U15_A871COMMIS = new double[1] ;
      P005U15_n871COMMIS = new boolean[] {false} ;
      A964CiaCod = "" ;
      A963ISOC = "" ;
      A966CODE = "" ;
      A902NUM_BI = 0 ;
      n902NUM_BI = false ;
      A907CPUI = "" ;
      n907CPUI = false ;
      A889TRANS_ = "" ;
      n889TRANS_ = false ;
      A970DATA = GXutil.nullDate() ;
      A967IATA = "" ;
      A968NUM_BI = "" ;
      A965PER_NA = "" ;
      A969TIPO_V = "" ;
      A883TAX_IT = 0 ;
      n883TAX_IT = false ;
      A871COMMIS = 0 ;
      n871COMMIS = false ;
      GXv_date14 = new java.util.Date [1] ;
      GXv_int17 = new byte [1] ;
      AV92hot0_c = "" ;
      AV96hot0_d = GXutil.nullDate() ;
      AV93hot0_i = "" ;
      AV91hot0_p = "" ;
      AV95hot0_t = "" ;
      AV161COMMI = 0 ;
      AV238GXLvl = (byte)(0) ;
      P005U16_A963ISOC = new String[] {""} ;
      P005U16_A964CiaCod = new String[] {""} ;
      P005U16_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005U16_A969TIPO_V = new String[] {""} ;
      P005U16_A968NUM_BI = new String[] {""} ;
      P005U16_A967IATA = new String[] {""} ;
      P005U16_A966CODE = new String[] {""} ;
      P005U16_A965PER_NA = new String[] {""} ;
      P005U16_A972SeqPag = new int[1] ;
      P005U16_A936PGTIP_ = new String[] {""} ;
      P005U16_n936PGTIP_ = new boolean[] {false} ;
      P005U16_A933PGCC_T = new String[] {""} ;
      P005U16_n933PGCC_T = new boolean[] {false} ;
      A972SeqPag = 0 ;
      A936PGTIP_ = "" ;
      n936PGTIP_ = false ;
      A933PGCC_T = "" ;
      n933PGCC_T = false ;
      AV239GXLvl = (byte)(0) ;
      P005U17_A963ISOC = new String[] {""} ;
      P005U17_A964CiaCod = new String[] {""} ;
      P005U17_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005U17_A969TIPO_V = new String[] {""} ;
      P005U17_A968NUM_BI = new String[] {""} ;
      P005U17_A967IATA = new String[] {""} ;
      P005U17_A966CODE = new String[] {""} ;
      P005U17_A965PER_NA = new String[] {""} ;
      P005U17_A971ISeq = new int[1] ;
      P005U17_A926Tariff = new String[] {""} ;
      P005U17_n926Tariff = new boolean[] {false} ;
      A971ISeq = 0 ;
      A926Tariff = "" ;
      n926Tariff = false ;
      GX_I = 0 ;
      GX_J = 0 ;
      P005U18_A1436ListC = new double[1] ;
      P005U18_n1436ListC = new boolean[] {false} ;
      P005U18_A1438ListA = new String[] {""} ;
      P005U18_A1437ListA = new String[] {""} ;
      A1436ListC = 0 ;
      n1436ListC = false ;
      A1438ListA = "" ;
      A1437ListA = "" ;
      P005U19_A1433FareB = new String[] {""} ;
      P005U19_A1432FareA = new String[] {""} ;
      A1433FareB = "" ;
      A1432FareA = "" ;
      P005U20_A1458rfGDS = new String[] {""} ;
      P005U20_n1458rfGDS = new boolean[] {false} ;
      P005U20_A1233EmpCo = new String[] {""} ;
      P005U20_A1404GdsCo = new String[] {""} ;
      A1458rfGDS = "" ;
      n1458rfGDS = false ;
      A1404GdsCo = "" ;
      AV45now = GXutil.resetTime( GXutil.nullDate() );
      GX_INS248 = 0 ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1303SCERE = "" ;
      n1303SCERE = false ;
      A1304SCEAi = "" ;
      n1304SCEAi = false ;
      A1305SCETe = "" ;
      n1305SCETe = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "" ;
      n1310SCETp = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      A1311SCEVe = "" ;
      n1311SCEVe = false ;
      P005U22_A1312SCEId = new long[1] ;
      P005U22_n1312SCEId = new boolean[] {false} ;
      A1312SCEId = 0 ;
      n1312SCEId = false ;
      AV207Tot_F = 0 ;
      AV243GXLvl = (byte)(0) ;
      P005U23_A993HntUsu = new String[] {""} ;
      P005U23_n993HntUsu = new boolean[] {false} ;
      P005U23_A997HntSeq = new String[] {""} ;
      P005U23_A994HntLin = new String[] {""} ;
      P005U23_n994HntLin = new boolean[] {false} ;
      P005U23_A998HntSub = new byte[1] ;
      AV138HntUs = "" ;
      AV113AuxLi = "" ;
      AV215ComVa = 0 ;
      AV244GXLvl = (byte)(0) ;
      P005U25_A993HntUsu = new String[] {""} ;
      P005U25_n993HntUsu = new boolean[] {false} ;
      P005U25_A997HntSeq = new String[] {""} ;
      P005U25_A994HntLin = new String[] {""} ;
      P005U25_n994HntLin = new boolean[] {false} ;
      P005U25_A998HntSub = new byte[1] ;
      AV245GXLvl = (byte)(0) ;
      P005U27_A993HntUsu = new String[] {""} ;
      P005U27_n993HntUsu = new boolean[] {false} ;
      P005U27_A997HntSeq = new String[] {""} ;
      P005U27_A994HntLin = new String[] {""} ;
      P005U27_n994HntLin = new boolean[] {false} ;
      P005U27_A998HntSub = new byte[1] ;
      AV246GXLvl = (byte)(0) ;
      P005U30_A993HntUsu = new String[] {""} ;
      P005U30_n993HntUsu = new boolean[] {false} ;
      P005U30_A997HntSeq = new String[] {""} ;
      P005U30_A994HntLin = new String[] {""} ;
      P005U30_n994HntLin = new boolean[] {false} ;
      P005U30_A998HntSub = new byte[1] ;
      AV212FPAM = "" ;
      GXv_int12 = new short [1] ;
      GXv_char11 = new String [1] ;
      GXv_char10 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_char3 = new String [1] ;
      GXt_char9 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new prethandling__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            P005U3_A1440RETHe, P005U3_A1439RETHe, P005U3_n1439RETHe, P005U3_A1441RETHe
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P005U7_A993HntUsu, P005U7_n993HntUsu, P005U7_A997HntSeq, P005U7_A994HntLin, P005U7_n994HntLin, P005U7_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P005U9_A1450rfFop, P005U9_n1450rfFop, P005U9_A1435fopID, P005U9_A1455rfFop, P005U9_A1233EmpCo
            }
            , new Object[] {
            P005U10_A1450rfFop, P005U10_n1450rfFop, P005U10_A1435fopID, P005U10_A1455rfFop, P005U10_A1233EmpCo, P005U10_A1454rfFop, P005U10_n1454rfFop, P005U10_A1451rfFop, P005U10_n1451rfFop, P005U10_A1453rfFop,
            P005U10_n1453rfFop
            }
            , new Object[] {
            P005U11_A1447rfBas, P005U11_A1446rfBas, P005U11_A1233EmpCo, P005U11_A1449rfBas, P005U11_A1442rfBas, P005U11_n1442rfBas, P005U11_A1443rfBas, P005U11_n1443rfBas, P005U11_A1444rfBas, P005U11_n1444rfBas,
            P005U11_A1445rfBas, P005U11_n1445rfBas, P005U11_A1448rfBas
            }
            , new Object[] {
            P005U12_A1449rfBas, P005U12_A1447rfBas, P005U12_A1446rfBas, P005U12_A1233EmpCo, P005U12_A1442rfBas, P005U12_n1442rfBas, P005U12_A1443rfBas, P005U12_n1443rfBas, P005U12_A1444rfBas, P005U12_n1444rfBas,
            P005U12_A1445rfBas, P005U12_n1445rfBas, P005U12_A1448rfBas
            }
            , new Object[] {
            P005U13_A1431CaTip, P005U13_A1430CaMoe, P005U13_A1429CaDat, P005U13_A1426CaVal, P005U13_n1426CaVal
            }
            , new Object[] {
            P005U14_A1429CaDat, P005U14_A1431CaTip, P005U14_A1430CaMoe, P005U14_A1426CaVal, P005U14_n1426CaVal
            }
            , new Object[] {
            P005U15_A964CiaCod, P005U15_A963ISOC, P005U15_A966CODE, P005U15_A902NUM_BI, P005U15_n902NUM_BI, P005U15_A907CPUI, P005U15_n907CPUI, P005U15_A889TRANS_, P005U15_n889TRANS_, P005U15_A970DATA,
            P005U15_A967IATA, P005U15_A968NUM_BI, P005U15_A965PER_NA, P005U15_A969TIPO_V, P005U15_A883TAX_IT, P005U15_n883TAX_IT, P005U15_A871COMMIS, P005U15_n871COMMIS
            }
            , new Object[] {
            P005U16_A963ISOC, P005U16_A964CiaCod, P005U16_A970DATA, P005U16_A969TIPO_V, P005U16_A968NUM_BI, P005U16_A967IATA, P005U16_A966CODE, P005U16_A965PER_NA, P005U16_A972SeqPag, P005U16_A936PGTIP_,
            P005U16_n936PGTIP_, P005U16_A933PGCC_T, P005U16_n933PGCC_T
            }
            , new Object[] {
            P005U17_A963ISOC, P005U17_A964CiaCod, P005U17_A970DATA, P005U17_A969TIPO_V, P005U17_A968NUM_BI, P005U17_A967IATA, P005U17_A966CODE, P005U17_A965PER_NA, P005U17_A971ISeq, P005U17_A926Tariff,
            P005U17_n926Tariff
            }
            , new Object[] {
            P005U18_A1436ListC, P005U18_n1436ListC, P005U18_A1438ListA, P005U18_A1437ListA
            }
            , new Object[] {
            P005U19_A1433FareB, P005U19_A1432FareA
            }
            , new Object[] {
            P005U20_A1458rfGDS, P005U20_n1458rfGDS, P005U20_A1233EmpCo, P005U20_A1404GdsCo
            }
            , new Object[] {
            }
            , new Object[] {
            P005U22_A1312SCEId
            }
            , new Object[] {
            P005U23_A993HntUsu, P005U23_n993HntUsu, P005U23_A997HntSeq, P005U23_A994HntLin, P005U23_n994HntLin, P005U23_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P005U25_A993HntUsu, P005U25_n993HntUsu, P005U25_A997HntSeq, P005U25_A994HntLin, P005U25_n994HntLin, P005U25_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P005U27_A993HntUsu, P005U27_n993HntUsu, P005U27_A997HntSeq, P005U27_A994HntLin, P005U27_n994HntLin, P005U27_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P005U30_A993HntUsu, P005U30_n993HntUsu, P005U30_A997HntSeq, P005U30_A994HntLin, P005U30_n994HntLin, P005U30_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV64SplitR ;
   private byte AV189Verbo ;
   private byte AV214FlagC ;
   private byte AV28Flag_I ;
   private byte A998HntSub ;
   private byte AV29Flag_I ;
   private byte AV30Flag_I ;
   private byte AV210IT08S ;
   private byte AV211IT08S ;
   private byte AV57RFNSki ;
   private byte AV216fOK ;
   private byte AV197k ;
   private byte AV196TKT_f ;
   private byte AV230GXLvl ;
   private byte AV231GXLvl ;
   private byte AV234GXLvl ;
   private byte AV235GXLvl ;
   private byte AV236GXLvl ;
   private byte AV237GXLvl ;
   private byte GXv_int17[] ;
   private byte AV238GXLvl ;
   private byte AV239GXLvl ;
   private byte AV243GXLvl ;
   private byte AV244GXLvl ;
   private byte AV245GXLvl ;
   private byte AV246GXLvl ;
   private short GXt_int5 ;
   private short Gx_err ;
   private short AV183Commi ;
   private short AV205iAdd ;
   private short AV206iCoun ;
   private short AV27Flag_F ;
   private short AV69TamFar ;
   private short A1451rfFop ;
   private short AV89RFN_De ;
   private short A1449rfBas ;
   private short GXv_int12[] ;
   private int AV23FileNo ;
   private int AV86vHntSe ;
   private int AV199cnt_H ;
   private int A1441RETHe ;
   private int GX_INS207 ;
   private int AV188cnt_n ;
   private int AV186cnt_C ;
   private int AV187cnt_C ;
   private int AV185cnt_P ;
   private int AV184cnt_P ;
   private int AV194cnt_S ;
   private int AV200cnt_C ;
   private int AV201cnt_C ;
   private int AV34i ;
   private int AV39j ;
   private int AV52qtdAir ;
   private int AV51qtdAir ;
   private int AV53qtdCRS ;
   private int AV103n1 ;
   private int AV104n2 ;
   private int GXt_int15 ;
   private int GXv_int16[] ;
   private int A972SeqPag ;
   private int A971ISeq ;
   private int GX_I ;
   private int GX_J ;
   private int GX_INS248 ;
   private long AV26FileNo ;
   private long AV169NUM_B ;
   private long A902NUM_BI ;
   private long A1312SCEId ;
   private double AV202TAX_T ;
   private double AV11Baseco ;
   private double A1442rfBas ;
   private double A1443rfBas ;
   private double AV106rfBas ;
   private double AV107rfBas ;
   private double AV112v ;
   private double A1426CaVal ;
   private double A883TAX_IT ;
   private double A871COMMIS ;
   private double AV161COMMI ;
   private double A1436ListC ;
   private double AV207Tot_F ;
   private double AV215ComVa ;
   private String AV17DebugM ;
   private String AV24FileSo ;
   private String AV223Menu ;
   private String AV84Versao ;
   private String AV224REVN ;
   private String AV22FileNa ;
   private String AV81TRNN_O ;
   private String AV40Linha ;
   private String scmdbuf ;
   private String AV15CRS ;
   private String A1440RETHe ;
   private String AV63sHntSe ;
   private String AV38IT0Zpo ;
   private String A997HntSeq ;
   private String A989HntRec ;
   private String AV198IT0Z_ ;
   private String Gx_emsg ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXt_char6 ;
   private String GXt_char8 ;
   private String GXt_char7 ;
   private String AV77TPST ;
   private String AV50FileDe ;
   private String AV62sdb ;
   private String AV80TRNN_C ;
   private String AV97RFN_Mu ;
   private String AV76TMFA ;
   private String AV110NEW_T ;
   private String AV204IT05P ;
   private String AV203IT05P ;
   private String AV208IT08p ;
   private String AV209IT08p ;
   private String AV54RCID ;
   private String AV35IT01po ;
   private String AV8AGTN ;
   private String AV78TRNC ;
   private String AV70TDNR ;
   private String AV67TACN ;
   private String AV144DAIS ;
   private String AV190STAT ;
   private String AV48ORAC ;
   private String AV36IT02po ;
   private String AV14CORT ;
   private String AV12COAM ;
   private String AV37IT05po ;
   private String AV191new_C ;
   private String AV137Last_ ;
   private String AV111TDAM ;
   private String AV21FBTD ;
   private String AV41ListAi ;
   private String AV9AirptCo[][] ;
   private String AV42ListAi ;
   private String AV43ListCo ;
   private String AV19FareAi ;
   private String AV85vFareB[][] ;
   private String AV20FareBa ;
   private String AV82uFBTD ;
   private String AV114SCETp ;
   private String AV61SCETex ;
   private String AV65SubstS ;
   private String AV66SubstS ;
   private String AV83vCRSs[][] ;
   private String AV16RFN_DA ;
   private String AV55RCPN ;
   private String AV59RTDN ;
   private String AV213DIRD ;
   private String AV56rffopd ;
   private String AV75TKT_TR ;
   private String AV31fopid ;
   private String AV73TKT_fo[] ;
   private String A1450rfFop ;
   private String A1435fopID ;
   private String A1455rfFop ;
   private String A1233EmpCo ;
   private String AV109rfFop ;
   private String A1454rfFop ;
   private String A1453rfFop ;
   private String AV72TKT_DA ;
   private String A1447rfBas ;
   private String A1446rfBas ;
   private String A1444rfBas ;
   private String A1445rfBas ;
   private String A1448rfBas ;
   private String AV60s ;
   private String AV90TKT_Fa ;
   private String AV99rfBase ;
   private String AV108rfBas ;
   private String AV105rfBas ;
   private String A1431CaTip ;
   private String A1430CaMoe ;
   private String AV94hot0_n ;
   private String A964CiaCod ;
   private String A963ISOC ;
   private String A966CODE ;
   private String A907CPUI ;
   private String A967IATA ;
   private String A968NUM_BI ;
   private String A965PER_NA ;
   private String A969TIPO_V ;
   private String AV92hot0_c ;
   private String AV93hot0_i ;
   private String AV91hot0_p ;
   private String AV95hot0_t ;
   private String A936PGTIP_ ;
   private String A933PGCC_T ;
   private String A1438ListA ;
   private String A1437ListA ;
   private String A1433FareB ;
   private String A1432FareA ;
   private String A1458rfGDS ;
   private String A1404GdsCo ;
   private String A1298SCETk ;
   private String A1300SCECR ;
   private String A1301SCEAi ;
   private String A1303SCERE ;
   private String A1304SCEAi ;
   private String A1305SCETe ;
   private String A1307SCEIa ;
   private String A1310SCETp ;
   private String A1306SCEAP ;
   private String A1311SCEVe ;
   private String AV212FPAM ;
   private String GXv_char11[] ;
   private String GXv_char10[] ;
   private String GXv_char4[] ;
   private String GXv_char3[] ;
   private String GXt_char9 ;
   private java.util.Date AV45now ;
   private java.util.Date A1299SCEDa ;
   private java.util.Date AV87dt1 ;
   private java.util.Date AV88dt2 ;
   private java.util.Date Gx_date ;
   private java.util.Date GXt_date13 ;
   private java.util.Date A1429CaDat ;
   private java.util.Date A970DATA ;
   private java.util.Date GXv_date14[] ;
   private java.util.Date AV96hot0_d ;
   private boolean returnInSub ;
   private boolean n1439RETHe ;
   private boolean n989HntRec ;
   private boolean n993HntUsu ;
   private boolean n994HntLin ;
   private boolean n1450rfFop ;
   private boolean n1454rfFop ;
   private boolean n1451rfFop ;
   private boolean n1453rfFop ;
   private boolean n1442rfBas ;
   private boolean n1443rfBas ;
   private boolean n1444rfBas ;
   private boolean n1445rfBas ;
   private boolean n1426CaVal ;
   private boolean n902NUM_BI ;
   private boolean n907CPUI ;
   private boolean n889TRANS_ ;
   private boolean n883TAX_IT ;
   private boolean n871COMMIS ;
   private boolean n936PGTIP_ ;
   private boolean n933PGCC_T ;
   private boolean n926Tariff ;
   private boolean n1436ListC ;
   private boolean n1458rfGDS ;
   private boolean n1298SCETk ;
   private boolean n1299SCEDa ;
   private boolean n1300SCECR ;
   private boolean n1301SCEAi ;
   private boolean n1303SCERE ;
   private boolean n1304SCEAi ;
   private boolean n1305SCETe ;
   private boolean n1307SCEIa ;
   private boolean n1310SCETp ;
   private boolean n1306SCEAP ;
   private boolean n1311SCEVe ;
   private boolean n1312SCEId ;
   private String A994HntLin ;
   private String AV113AuxLi ;
   private String AV10AppCod ;
   private String A1439RETHe ;
   private String A993HntUsu ;
   private String AV71TKT_Co ;
   private String A889TRANS_ ;
   private String A926Tariff ;
   private String AV138HntUs ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P005U3_A1440RETHe ;
   private String[] P005U3_A1439RETHe ;
   private boolean[] P005U3_n1439RETHe ;
   private int[] P005U3_A1441RETHe ;
   private String[] P005U7_A993HntUsu ;
   private boolean[] P005U7_n993HntUsu ;
   private String[] P005U7_A997HntSeq ;
   private String[] P005U7_A994HntLin ;
   private boolean[] P005U7_n994HntLin ;
   private byte[] P005U7_A998HntSub ;
   private String[] P005U9_A1450rfFop ;
   private boolean[] P005U9_n1450rfFop ;
   private String[] P005U9_A1435fopID ;
   private String[] P005U9_A1455rfFop ;
   private String[] P005U9_A1233EmpCo ;
   private String[] P005U10_A1450rfFop ;
   private boolean[] P005U10_n1450rfFop ;
   private String[] P005U10_A1435fopID ;
   private String[] P005U10_A1455rfFop ;
   private String[] P005U10_A1233EmpCo ;
   private String[] P005U10_A1454rfFop ;
   private boolean[] P005U10_n1454rfFop ;
   private short[] P005U10_A1451rfFop ;
   private boolean[] P005U10_n1451rfFop ;
   private String[] P005U10_A1453rfFop ;
   private boolean[] P005U10_n1453rfFop ;
   private String[] P005U11_A1447rfBas ;
   private String[] P005U11_A1446rfBas ;
   private String[] P005U11_A1233EmpCo ;
   private short[] P005U11_A1449rfBas ;
   private double[] P005U11_A1442rfBas ;
   private boolean[] P005U11_n1442rfBas ;
   private double[] P005U11_A1443rfBas ;
   private boolean[] P005U11_n1443rfBas ;
   private String[] P005U11_A1444rfBas ;
   private boolean[] P005U11_n1444rfBas ;
   private String[] P005U11_A1445rfBas ;
   private boolean[] P005U11_n1445rfBas ;
   private String[] P005U11_A1448rfBas ;
   private short[] P005U12_A1449rfBas ;
   private String[] P005U12_A1447rfBas ;
   private String[] P005U12_A1446rfBas ;
   private String[] P005U12_A1233EmpCo ;
   private double[] P005U12_A1442rfBas ;
   private boolean[] P005U12_n1442rfBas ;
   private double[] P005U12_A1443rfBas ;
   private boolean[] P005U12_n1443rfBas ;
   private String[] P005U12_A1444rfBas ;
   private boolean[] P005U12_n1444rfBas ;
   private String[] P005U12_A1445rfBas ;
   private boolean[] P005U12_n1445rfBas ;
   private String[] P005U12_A1448rfBas ;
   private String[] P005U13_A1431CaTip ;
   private String[] P005U13_A1430CaMoe ;
   private java.util.Date[] P005U13_A1429CaDat ;
   private double[] P005U13_A1426CaVal ;
   private boolean[] P005U13_n1426CaVal ;
   private java.util.Date[] P005U14_A1429CaDat ;
   private String[] P005U14_A1431CaTip ;
   private String[] P005U14_A1430CaMoe ;
   private double[] P005U14_A1426CaVal ;
   private boolean[] P005U14_n1426CaVal ;
   private String[] P005U15_A964CiaCod ;
   private String[] P005U15_A963ISOC ;
   private String[] P005U15_A966CODE ;
   private long[] P005U15_A902NUM_BI ;
   private boolean[] P005U15_n902NUM_BI ;
   private String[] P005U15_A907CPUI ;
   private boolean[] P005U15_n907CPUI ;
   private String[] P005U15_A889TRANS_ ;
   private boolean[] P005U15_n889TRANS_ ;
   private java.util.Date[] P005U15_A970DATA ;
   private String[] P005U15_A967IATA ;
   private String[] P005U15_A968NUM_BI ;
   private String[] P005U15_A965PER_NA ;
   private String[] P005U15_A969TIPO_V ;
   private double[] P005U15_A883TAX_IT ;
   private boolean[] P005U15_n883TAX_IT ;
   private double[] P005U15_A871COMMIS ;
   private boolean[] P005U15_n871COMMIS ;
   private String[] P005U16_A963ISOC ;
   private String[] P005U16_A964CiaCod ;
   private java.util.Date[] P005U16_A970DATA ;
   private String[] P005U16_A969TIPO_V ;
   private String[] P005U16_A968NUM_BI ;
   private String[] P005U16_A967IATA ;
   private String[] P005U16_A966CODE ;
   private String[] P005U16_A965PER_NA ;
   private int[] P005U16_A972SeqPag ;
   private String[] P005U16_A936PGTIP_ ;
   private boolean[] P005U16_n936PGTIP_ ;
   private String[] P005U16_A933PGCC_T ;
   private boolean[] P005U16_n933PGCC_T ;
   private String[] P005U17_A963ISOC ;
   private String[] P005U17_A964CiaCod ;
   private java.util.Date[] P005U17_A970DATA ;
   private String[] P005U17_A969TIPO_V ;
   private String[] P005U17_A968NUM_BI ;
   private String[] P005U17_A967IATA ;
   private String[] P005U17_A966CODE ;
   private String[] P005U17_A965PER_NA ;
   private int[] P005U17_A971ISeq ;
   private String[] P005U17_A926Tariff ;
   private boolean[] P005U17_n926Tariff ;
   private double[] P005U18_A1436ListC ;
   private boolean[] P005U18_n1436ListC ;
   private String[] P005U18_A1438ListA ;
   private String[] P005U18_A1437ListA ;
   private String[] P005U19_A1433FareB ;
   private String[] P005U19_A1432FareA ;
   private String[] P005U20_A1458rfGDS ;
   private boolean[] P005U20_n1458rfGDS ;
   private String[] P005U20_A1233EmpCo ;
   private String[] P005U20_A1404GdsCo ;
   private long[] P005U22_A1312SCEId ;
   private boolean[] P005U22_n1312SCEId ;
   private String[] P005U23_A993HntUsu ;
   private boolean[] P005U23_n993HntUsu ;
   private String[] P005U23_A997HntSeq ;
   private String[] P005U23_A994HntLin ;
   private boolean[] P005U23_n994HntLin ;
   private byte[] P005U23_A998HntSub ;
   private String[] P005U25_A993HntUsu ;
   private boolean[] P005U25_n993HntUsu ;
   private String[] P005U25_A997HntSeq ;
   private String[] P005U25_A994HntLin ;
   private boolean[] P005U25_n994HntLin ;
   private byte[] P005U25_A998HntSub ;
   private String[] P005U27_A993HntUsu ;
   private boolean[] P005U27_n993HntUsu ;
   private String[] P005U27_A997HntSeq ;
   private String[] P005U27_A994HntLin ;
   private boolean[] P005U27_n994HntLin ;
   private byte[] P005U27_A998HntSub ;
   private String[] P005U30_A993HntUsu ;
   private boolean[] P005U30_n993HntUsu ;
   private String[] P005U30_A997HntSeq ;
   private String[] P005U30_A994HntLin ;
   private boolean[] P005U30_n994HntLin ;
   private byte[] P005U30_A998HntSub ;
}

final  class prethandling__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P005U2", "DELETE FROM [RETSPECTEMP]  WHERE [HntUsuCod] like 'RETClone%'", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U3", "SELECT [RETHeldGDS], [RETHeldLine], [RETHeldSeq] FROM [RETHELD] WITH (UPDLOCK) WHERE ([RETHeldGDS] = ?) AND ([RETHeldGDS] = ?) ORDER BY [RETHeldGDS], [RETHeldSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005U4", "DELETE FROM [RETHELD]  WHERE [RETHeldGDS] = ? AND [RETHeldSeq] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005U5", "INSERT INTO [RETSPECTEMP] ([HntSeq], [HntSub], [HntRecId], [HntUsuCod], [HntLine], [HntTypId], [HntIata], [HntStd], [HntTDNR], [HntTRNC]) VALUES (?, ?, ?, ?, ?, '', '', '', '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005U6", "INSERT INTO [RETSPECTEMP] ([HntSeq], [HntSub], [HntRecId], [HntUsuCod], [HntLine], [HntTypId], [HntIata], [HntStd], [HntTDNR], [HntTRNC]) VALUES (?, ?, ?, ?, ?, '', '', '', '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U7", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND (SUBSTRING([HntUsuCod], 1, 8) = 'RETClone')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005U8", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U9", "SELECT [rfFopStatus], [fopID], [rfFopDoc], [EmpCod] FROM [RFFORMAPAGAMENTO] WITH (NOLOCK) WHERE ([EmpCod] = ? and [rfFopDoc] = ? and [fopID] = ?) AND ([rfFopStatus] = '1') ORDER BY [EmpCod], [rfFopDoc], [fopID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005U10", "SELECT [rfFopStatus], [fopID], [rfFopDoc], [EmpCod], [rfFopCalcAge], [rfFoplAte], [rfFopTipoReembolso] FROM [RFFORMAPAGAMENTO] WITH (NOLOCK) WHERE ([EmpCod] = ? and [rfFopDoc] = ? and [fopID] = ?) AND ([rfFopStatus] = '1') ORDER BY [EmpCod], [rfFopDoc], [fopID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005U11", "SELECT [rfBaseTarifaria], [rfBaseStatus], [EmpCod], [rfBaseAte], [rfBasePorcentagem], [rfBaseValor], [rfBaseMenor], [rfBaseMoeda], [rfBaseTipoTrafego] FROM [RFBASETARIFARIA] WITH (NOLOCK) WHERE ([EmpCod] = ? and [rfBaseStatus] = 'A') AND ([rfBaseTarifaria] <> '*') ORDER BY [EmpCod], [rfBaseStatus] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005U12", "SELECT TOP 1 [rfBaseAte], [rfBaseTarifaria], [rfBaseStatus], [EmpCod], [rfBasePorcentagem], [rfBaseValor], [rfBaseMenor], [rfBaseMoeda], [rfBaseTipoTrafego] FROM [RFBASETARIFARIA] WITH (NOLOCK) WHERE ([EmpCod] = ? and [rfBaseStatus] = 'A' and [rfBaseTarifaria] = '*') AND ([rfBaseAte] >= ?) ORDER BY [EmpCod], [rfBaseStatus], [rfBaseTarifaria] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005U13", "SELECT TOP 1 [CaTipo], [CaMoeda], [CaData], [CaValor] FROM [CAMBIO] WITH (NOLOCK) WHERE ([CaMoeda] = ? and [CaTipo] = 'CA') AND ([CaData] >= ?) ORDER BY [CaMoeda], [CaTipo] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005U14", "SELECT TOP 1 [CaData], [CaTipo], [CaMoeda], [CaValor] FROM [CAMBIO] WITH (NOLOCK) WHERE [CaMoeda] = ? and [CaTipo] = 'CA' and [CaData] <= ? ORDER BY [CaMoeda], [CaTipo], [CaData] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005U15", "SELECT [CiaCod], [ISOC], [CODE], [NUM_BIL2], [CPUI], [TRANS_NO], [DATA], [IATA], [NUM_BIL], [PER_NAME], [TIPO_VEND], [TAX_IT], [COMMISSION] FROM [HOT] WITH (NOLOCK) WHERE [NUM_BIL2] = ? ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005U16", "SELECT [ISOC], [CiaCod], [DATA], [TIPO_VEND], [NUM_BIL], [IATA], [CODE], [PER_NAME], [SeqPag], [PGTIP_VEND], [PGCC_TYPE] FROM [HOT2] WITH (NOLOCK) WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005U17", "SELECT [ISOC], [CiaCod], [DATA], [TIPO_VEND], [NUM_BIL], [IATA], [CODE], [PER_NAME], [ISeq], [Tariff] FROM [HOT1] WITH (NOLOCK) WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005U18", "SELECT [ListCommission], [ListAirptCode], [ListAirLine] FROM [AIRPORTCODELIST] WITH (NOLOCK) ORDER BY [ListAirLine], [ListAirptCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005U19", "SELECT [FareBasis], [FareAirLine] FROM [FAREBASISEXCEPTIONS] WITH (NOLOCK) ORDER BY [FareAirLine], [FareBasis] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005U20", "SELECT [rfGDSStatus], [EmpCod], [GdsCod] FROM [RFGDSS] WITH (NOLOCK) WHERE [rfGDSStatus] = '1' ORDER BY [EmpCod], [GdsCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005U21", "INSERT INTO [SCEVENTS] ([SCETkt], [SCEDate], [SCECRS], [SCEAirLine], [SCERETName], [SCEAirportCode], [SCEText], [SCEAPP], [SCEIata], [SCETpEvento], [SCEVersao], [SCEFopID], [SCEFPAM], [SCEParcela]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U22", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
         ,new ForEachCursor("P005U23", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND (SUBSTRING([HntUsuCod], 1, 8) = 'RETClone')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005U24", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U25", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND (SUBSTRING([HntUsuCod], 1, 8) = 'RETClone')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005U26", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U27", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND (SUBSTRING([HntUsuCod], 1, 8) = 'RETClone')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P005U28", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005U29", "INSERT INTO [RETSPECTEMP] ([HntSeq], [HntSub], [HntUsuCod], [HntLine], [HntRecId], [HntTypId], [HntIata], [HntStd], [HntTDNR], [HntTRNC]) VALUES (?, ?, ?, ?, '', '', '', '', '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005U30", "SELECT TOP 1 [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND (SUBSTRING([HntUsuCod], 1, 8) = 'RETClone')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P005U31", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005U32", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((int[]) buf[3])[0] = rslt.getInt(3) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 5) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 5) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(6) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               ((double[]) buf[4])[0] = rslt.getDouble(5) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((double[]) buf[6])[0] = rslt.getDouble(6) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(8, 3) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
               break;
            case 10 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
               ((double[]) buf[4])[0] = rslt.getDouble(5) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((double[]) buf[6])[0] = rslt.getDouble(6) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(8, 3) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((double[]) buf[3])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 12 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
               ((double[]) buf[3])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((long[]) buf[3])[0] = rslt.getLong(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getString(5, 4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[9])[0] = rslt.getGXDate(7) ;
               ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[11])[0] = rslt.getString(9, 20) ;
               ((String[]) buf[12])[0] = rslt.getString(10, 20) ;
               ((String[]) buf[13])[0] = rslt.getString(11, 20) ;
               ((double[]) buf[14])[0] = rslt.getDouble(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((double[]) buf[16])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               break;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 20) ;
               ((int[]) buf[8])[0] = rslt.getInt(9) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 2) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(11, 2) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               break;
            case 15 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 20) ;
               ((int[]) buf[8])[0] = rslt.getInt(9) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(10) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               break;
            case 16 :
               ((double[]) buf[0])[0] = rslt.getDouble(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
               break;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               break;
            case 18 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 4) ;
               break;
            case 20 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 21 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 23 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 25 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 28 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 5);
               stmt.setString(2, (String)parms[1], 5);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 4);
               stmt.setInt(2, ((Number) parms[1]).intValue());
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setByte(2, ((Number) parms[1]).byteValue());
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 3);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(5, (String)parms[7]);
               }
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setByte(2, ((Number) parms[1]).byteValue());
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 3);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(5, (String)parms[7]);
               }
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 7 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 5);
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 5);
               break;
            case 9 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 10 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 11 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setDate(2, (java.util.Date)parms[1]);
               break;
            case 12 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setDate(2, (java.util.Date)parms[1]);
               break;
            case 13 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               break;
            case 14 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               break;
            case 15 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               break;
            case 19 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 5);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 3);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 3);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 150);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 4);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 11);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 3);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[21], 5);
               }
               break;
            case 21 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 22 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 23 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 24 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 25 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 26 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 27 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setByte(2, ((Number) parms[1]).byteValue());
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[3], 20);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(4, (String)parms[5]);
               }
               break;
            case 28 :
               stmt.setString(1, (String)parms[0], 21);
               stmt.setString(2, (String)parms[1], 21);
               break;
            case 29 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 30 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
      }
   }

}


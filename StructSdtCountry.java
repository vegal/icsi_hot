
public final  class StructSdtCountry implements Cloneable, java.io.Serializable
{
   public StructSdtCountry( )
   {
      gxTv_SdtCountry_Isocod = "" ;
      gxTv_SdtCountry_Isodes = "" ;
      gxTv_SdtCountry_Level1 = new java.util.Vector();
      gxTv_SdtCountry_Contacttypes = new java.util.Vector();
      gxTv_SdtCountry_Currencies = new java.util.Vector();
      gxTv_SdtCountry_Mode = "" ;
      gxTv_SdtCountry_Isocod_Z = "" ;
      gxTv_SdtCountry_Isodes_Z = "" ;
      gxTv_SdtCountry_Isocod_N = (byte)(0) ;
      gxTv_SdtCountry_Isodes_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIsocod( )
   {
      return gxTv_SdtCountry_Isocod ;
   }

   public void setIsocod( String value )
   {
      gxTv_SdtCountry_Isocod = value ;
      return  ;
   }

   public String getIsodes( )
   {
      return gxTv_SdtCountry_Isodes ;
   }

   public void setIsodes( String value )
   {
      gxTv_SdtCountry_Isodes = value ;
      return  ;
   }

   public java.util.Vector getLevel1( )
   {
      return gxTv_SdtCountry_Level1 ;
   }

   public void setLevel1( java.util.Vector value )
   {
      gxTv_SdtCountry_Level1 = value ;
      return  ;
   }

   public java.util.Vector getContacttypes( )
   {
      return gxTv_SdtCountry_Contacttypes ;
   }

   public void setContacttypes( java.util.Vector value )
   {
      gxTv_SdtCountry_Contacttypes = value ;
      return  ;
   }

   public java.util.Vector getCurrencies( )
   {
      return gxTv_SdtCountry_Currencies ;
   }

   public void setCurrencies( java.util.Vector value )
   {
      gxTv_SdtCountry_Currencies = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtCountry_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCountry_Mode = value ;
      return  ;
   }

   public String getIsocod_Z( )
   {
      return gxTv_SdtCountry_Isocod_Z ;
   }

   public void setIsocod_Z( String value )
   {
      gxTv_SdtCountry_Isocod_Z = value ;
      return  ;
   }

   public String getIsodes_Z( )
   {
      return gxTv_SdtCountry_Isodes_Z ;
   }

   public void setIsodes_Z( String value )
   {
      gxTv_SdtCountry_Isodes_Z = value ;
      return  ;
   }

   public byte getIsocod_N( )
   {
      return gxTv_SdtCountry_Isocod_N ;
   }

   public void setIsocod_N( byte value )
   {
      gxTv_SdtCountry_Isocod_N = value ;
      return  ;
   }

   public byte getIsodes_N( )
   {
      return gxTv_SdtCountry_Isodes_N ;
   }

   public void setIsodes_N( byte value )
   {
      gxTv_SdtCountry_Isodes_N = value ;
      return  ;
   }

   protected byte gxTv_SdtCountry_Isocod_N ;
   protected byte gxTv_SdtCountry_Isodes_N ;
   protected String gxTv_SdtCountry_Mode ;
   protected String gxTv_SdtCountry_Isocod ;
   protected String gxTv_SdtCountry_Isodes ;
   protected String gxTv_SdtCountry_Isocod_Z ;
   protected String gxTv_SdtCountry_Isodes_Z ;
   protected java.util.Vector gxTv_SdtCountry_Level1 ;
   protected java.util.Vector gxTv_SdtCountry_Contacttypes ;
   protected java.util.Vector gxTv_SdtCountry_Currencies ;
}


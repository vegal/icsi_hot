/*
               File: Gx06M0
        Description: Selection List ICSI CCConf
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:16.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx06m0 extends GXWorkpanel
{
   public wgx06m0( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx06m0.class ));
   }

   public wgx06m0( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx06M0" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List ICSI CCConf" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 675 ;
   }

   protected int getFrmHeight( )
   {
      return 306 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx06M0.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      wgx06m0.this.aP0 = aP0;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load12( ) throws GXLoadInterruptException
   {
      subwgx06m012 = new subwgx06m012 ();
      lV5cICSI_C = GXutil.padr( GXutil.rtrim( AV5cICSI_C), (short)(2), "%") ;
      lV7cICSI_C = GXutil.padr( GXutil.rtrim( AV7cICSI_C), (short)(40), "%") ;
      /* Using cursor W00152 */
      pr_default.execute(0, new Object[] {lV5cICSI_C, new Byte(AV6cICSI_C), lV7cICSI_C});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A1421ICSI_ = W00152_A1421ICSI_[0] ;
         n1421ICSI_ = W00152_n1421ICSI_[0] ;
         A1235ICSI_ = W00152_A1235ICSI_[0] ;
         n1235ICSI_ = W00152_n1235ICSI_[0] ;
         A1237ICSI_ = W00152_A1237ICSI_[0] ;
         /* Execute user event: e11V152 */
         e11V152 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx06M0_load12 extends GXLoadProducer
   {
      wgx06m0 _sf ;

      public Gx06M0_load12( wgx06m0 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer12();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load12();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors12();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow12( )
   {
      return true;
   }

   public void autoRefresh_flow12( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cICSI_C, cV5cICSI_C)!=0)||(AV6cICSI_C!=cV6cICSI_C)||(GXutil.strcmp(AV7cICSI_C, cV7cICSI_C)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow12() )) {
         subfile.refresh();
         resetSubfileConditions_flow12() ;
      }
   }

   public boolean getSearch_flow12( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow12( )
   {
      cV5cICSI_C = AV5cICSI_C ;
      cV6cICSI_C = AV6cICSI_C ;
      cV7cICSI_C = AV7cICSI_C ;
   }

   public void resetSearchConditions_flow12( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow12( )
   {
      return new subwgx06m012 ();
   }

   public boolean getSearch_flow12( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow12( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow12( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow12( )
   {
      GXRefreshCommand12 ();
   }

   public final  class Gx06M0_flow12 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx06m0 _sf ;

      public Gx06M0_flow12( wgx06m0 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow12();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow12(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow12();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow12();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow12(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow12();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow12(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow12(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow12(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow12();
      }

   }

   protected void GXRefreshCommand12( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V152 */
      e12V152 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V152( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV8pICSI_C = A1237ICSI_ ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer12( )
   {
      subwgx06m012 oAux = subwgx06m012 ;
      subwgx06m012 = new subwgx06m012 ();
      variablesToSubfile12 ();
      subGrd_1.addElement(subwgx06m012);
      subwgx06m012 = oAux;
   }

   private void e11V152( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors12( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 675 , 306 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtavCicsi_ccconfcod = new GUIObjectString ( new GXEdit(2, "XX", UIFactory.getFont( "Courier New", 0, 9),259, 24, 24, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.CHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 259 , 24 , 24 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cICSI_C" );
      ((GXEdit) edtavCicsi_ccconfcod.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCicsi_ccconfcod.addFocusListener(this);
      edtavCicsi_ccconfcod.getGXComponent().setHelpId("HLP_WGx06M0.htm");
      edtavCicsi_ccconfenab = new GUIObjectByte ( new GXEdit(1, "9", UIFactory.getFont( "Courier New", 0, 9),259, 48, 17, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.NUMERIC, false, true, UIFactory.getColor(5), false) , GXPanel1 , 259 , 48 , 17 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6cICSI_C" );
      ((GXEdit) edtavCicsi_ccconfenab.getGXComponent()).setAlignment(ILabel.RIGHT);
      edtavCicsi_ccconfenab.addFocusListener(this);
      edtavCicsi_ccconfenab.getGXComponent().setHelpId("HLP_WGx06M0.htm");
      edtavCicsi_ccconfname = new GUIObjectString ( new GXEdit(40, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),259, 72, 290, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 259 , 72 , 290 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV7cICSI_C" );
      ((GXEdit) edtavCicsi_ccconfname.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCicsi_ccconfname.addFocusListener(this);
      edtavCicsi_ccconfname.getGXComponent().setHelpId("HLP_WGx06M0.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx06M0_load12(this), new Gx06M0_flow12(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(2, "XX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 128, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.CHAR, false, false, 0, false) , null ,  0 , 0 , 127 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1237ICSI_" ), "Company Credit Card"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 127 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectByte ( new GXEdit(1, "9", UIFactory.getFont( "Courier New", 0, 9),0, 0, 221, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.NUMERIC, false, false, 0, false) , null ,  0 , 0 , 220 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1235ICSI_" ), "Indicador de habilitado (1) ou n�o (0)"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 220 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 96 , 404 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  567 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  567 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  567 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  567 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Company Credit Card", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 119 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "Indicador de habilitado (1) ou n�o (0)", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 212 , 13 );
      lbllbl11 = UIFactory.getLabel(GXPanel1, "Processor Name", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 72 , 93 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                edtavCicsi_ccconfcod ,
                edtavCicsi_ccconfenab ,
                edtavCicsi_ccconfname ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   protected void variablesToSubfile12( )
   {
      subwgx06m012.setICSI_CCConfCod(A1237ICSI_);
      subwgx06m012.setICSI_CCConfEnab(A1235ICSI_);
   }

   protected void subfileToVariables12( )
   {
      A1237ICSI_ = subwgx06m012.getICSI_CCConfCod();
      A1235ICSI_ = subwgx06m012.getICSI_CCConfEnab();
      if ( ( A1235ICSI_ != 0 ) )
      {
         n1235ICSI_ = false ;
      }
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      edtavCicsi_ccconfcod.setValue( AV5cICSI_C );
      edtavCicsi_ccconfenab.setValue( AV6cICSI_C );
      edtavCicsi_ccconfname.setValue( AV7cICSI_C );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      AV5cICSI_C = edtavCicsi_ccconfcod.getValue() ;
      AV6cICSI_C = edtavCicsi_ccconfenab.getValue() ;
      AV7cICSI_C = edtavCicsi_ccconfname.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx06m012 = ( subwgx06m012 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06m012 = new subwgx06m012 ();
      }
      subfileToVariables12 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile12 ();
      subGrd_1.refreshLineValue(subwgx06m012);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx06m012 = ( subwgx06m012 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06m012 = new subwgx06m012 ();
      }
      subfileToVariables12 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V152 */
         e12V152 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V152 */
         e12V152 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtavCicsi_ccconfcod.isEventSource(eventSource) ) {
         setGXCursor( edtavCicsi_ccconfcod.getGXCursor() );
         return;
      }
      if ( edtavCicsi_ccconfenab.isEventSource(eventSource) ) {
         setGXCursor( edtavCicsi_ccconfenab.getGXCursor() );
         return;
      }
      if ( edtavCicsi_ccconfname.isEventSource(eventSource) ) {
         setGXCursor( edtavCicsi_ccconfname.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtavCicsi_ccconfcod.isEventSource(eventSource) ) {
         AV5cICSI_C = edtavCicsi_ccconfcod.getValue() ;
         return;
      }
      if ( edtavCicsi_ccconfenab.isEventSource(eventSource) ) {
         AV6cICSI_C = edtavCicsi_ccconfenab.getValue() ;
         return;
      }
      if ( edtavCicsi_ccconfname.isEventSource(eventSource) ) {
         AV7cICSI_C = edtavCicsi_ccconfname.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V152 */
         e12V152 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP0[0] = wgx06m0.this.AV8pICSI_C;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV8pICSI_C = "" ;
      subwgx06m012 = new subwgx06m012();
      scmdbuf = "" ;
      lV5cICSI_C = "" ;
      AV5cICSI_C = "" ;
      lV7cICSI_C = "" ;
      AV7cICSI_C = "" ;
      AV6cICSI_C = (byte)(0) ;
      W00152_A1421ICSI_ = new String[] {""} ;
      W00152_n1421ICSI_ = new boolean[] {false} ;
      W00152_A1235ICSI_ = new byte[1] ;
      W00152_n1235ICSI_ = new boolean[] {false} ;
      W00152_A1237ICSI_ = new String[] {""} ;
      A1421ICSI_ = "" ;
      n1421ICSI_ = false ;
      A1235ICSI_ = (byte)(0) ;
      n1235ICSI_ = false ;
      A1237ICSI_ = "" ;
      gxIsRefreshing = false ;
      cV5cICSI_C = "" ;
      cV6cICSI_C = (byte)(0) ;
      cV7cICSI_C = "" ;
      returnInSub = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx06m0__default(),
         new Object[] {
             new Object[] {
            W00152_A1421ICSI_, W00152_n1421ICSI_, W00152_A1235ICSI_, W00152_n1235ICSI_, W00152_A1237ICSI_
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected byte AV6cICSI_C ;
   protected byte A1235ICSI_ ;
   protected byte cV6cICSI_C ;
   protected short Gx_err ;
   protected String AV8pICSI_C ;
   protected String scmdbuf ;
   protected String lV5cICSI_C ;
   protected String AV5cICSI_C ;
   protected String A1237ICSI_ ;
   protected String cV5cICSI_C ;
   protected boolean n1421ICSI_ ;
   protected boolean n1235ICSI_ ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String lV7cICSI_C ;
   protected String AV7cICSI_C ;
   protected String A1421ICSI_ ;
   protected String cV7cICSI_C ;
   protected String[] aP0 ;
   protected subwgx06m012 subwgx06m012 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00152_A1421ICSI_ ;
   protected boolean[] W00152_n1421ICSI_ ;
   protected byte[] W00152_A1235ICSI_ ;
   protected boolean[] W00152_n1235ICSI_ ;
   protected String[] W00152_A1237ICSI_ ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtavCicsi_ccconfcod ;
   protected GUIObjectByte edtavCicsi_ccconfenab ;
   protected GUIObjectString edtavCicsi_ccconfname ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
   protected ILabel lbllbl11 ;
}

final  class wgx06m0__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00152", "SELECT [ICSI_CCConfName], [ICSI_CCConfEnab], [ICSI_CCConfCod] FROM [ICSI_CCCONF] WITH (FASTFIRSTROW NOLOCK) WHERE ([ICSI_CCConfCod] like ?) AND ([ICSI_CCConfEnab] >= ?) AND ([ICSI_CCConfName] like ?) ORDER BY [ICSI_CCConfCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((byte[]) buf[2])[0] = rslt.getByte(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setByte(2, ((Number) parms[1]).byteValue());
               stmt.setVarchar(3, (String)parms[2], 40);
               break;
      }
   }

}


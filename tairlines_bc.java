/*
               File: tairlines_bc
        Description: Airline Data
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:55.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tairlines_bc extends GXWebPanel implements IGxSilentTrn
{
   public tairlines_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tairlines_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tairlines_bc.class ));
   }

   public tairlines_bc( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z23ISOCod = A23ISOCod ;
            Z1136AirLi = A1136AirLi ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_2B0( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2B226( ) ;
         }
         else
         {
            checkExtendedTable2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               zm2B226( 15) ;
            }
            closeExtendedTableCursors2B226( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         /* Save parent mode. */
         sMode226 = Gx_mode ;
         confirm_2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            confirm_2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Restore parent mode. */
               Gx_mode = sMode226 ;
               IsConfirmed = (short)(1) ;
            }
         }
         /* Restore parent mode. */
         Gx_mode = sMode226 ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues2B0( ) ;
      }
   }

   public void confirm_2B228( )
   {
      nGXsfl_228_idx = (short)(0) ;
      while ( ( nGXsfl_228_idx < bcAirlines.getgxTv_SdtAirlines_Contacts().size() ) )
      {
         readRow2B228( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound228 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_228 != 0 ) )
         {
            getKey2B228( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound228 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate2B228( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable2B228( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        zm2B228( 18) ;
                     }
                     closeExtendedTableCursors2B228( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound228 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey2B228( ) ;
                     load2B228( ) ;
                     beforeValidate2B228( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls2B228( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_228 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate2B228( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable2B228( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              zm2B228( 18) ;
                           }
                           closeExtendedTableCursors2B228( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void confirm_2B227( )
   {
      nGXsfl_227_idx = (short)(0) ;
      while ( ( nGXsfl_227_idx < bcAirlines.getgxTv_SdtAirlines_Level1().size() ) )
      {
         readRow2B227( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound227 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_227 != 0 ) )
         {
            getKey2B227( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound227 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate2B227( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable2B227( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                     }
                     closeExtendedTableCursors2B227( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound227 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey2B227( ) ;
                     load2B227( ) ;
                     beforeValidate2B227( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls2B227( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_227 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate2B227( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable2B227( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                           }
                           closeExtendedTableCursors2B227( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void e112B2( )
   {
      /* Start Routine */
   }

   public void e122B2( )
   {
      /* 'back' Routine */
   }

   public void zm2B226( int GX_JID )
   {
      if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
      {
         Z1126AirLi = A1126AirLi ;
         Z1125AirLi = A1125AirLi ;
         Z1130Airli = A1130Airli ;
         Z1118AirLi = A1118AirLi ;
         Z1119AirLi = A1119AirLi ;
         Z1120Airli = A1120Airli ;
         Z1121AirLi = A1121AirLi ;
         Z1122AirLi = A1122AirLi ;
         Z1123AirLi = A1123AirLi ;
         Z1124AirLi = A1124AirLi ;
         Z1127AirLi = A1127AirLi ;
         Z1128AirLi = A1128AirLi ;
         Z1129AirLi = A1129AirLi ;
         Z1137AirLi = A1137AirLi ;
         Z1131AirLI = A1131AirLI ;
         Z1132AirLi = A1132AirLi ;
         Z365Contac = A365Contac ;
         Z1138AirLi = A1138AirLi ;
         Z1133AirLi = A1133AirLi ;
         Z1134AirLi = A1134AirLi ;
         Z1135AirLi = A1135AirLi ;
         Z366Contac = A366Contac ;
      }
      if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
      {
         Z20ISODes = A20ISODes ;
         Z1137AirLi = A1137AirLi ;
         Z1131AirLI = A1131AirLI ;
         Z1132AirLi = A1132AirLi ;
         Z365Contac = A365Contac ;
         Z1138AirLi = A1138AirLi ;
         Z1133AirLi = A1133AirLi ;
         Z1134AirLi = A1134AirLi ;
         Z1135AirLi = A1135AirLi ;
         Z366Contac = A366Contac ;
      }
      if ( ( GX_JID == -14 ) )
      {
         Z1136AirLi = A1136AirLi ;
         Z1126AirLi = A1126AirLi ;
         Z1125AirLi = A1125AirLi ;
         Z1130Airli = A1130Airli ;
         Z1118AirLi = A1118AirLi ;
         Z1119AirLi = A1119AirLi ;
         Z1120Airli = A1120Airli ;
         Z1121AirLi = A1121AirLi ;
         Z1122AirLi = A1122AirLi ;
         Z1123AirLi = A1123AirLi ;
         Z1124AirLi = A1124AirLi ;
         Z1127AirLi = A1127AirLi ;
         Z1128AirLi = A1128AirLi ;
         Z1129AirLi = A1129AirLi ;
         Z23ISOCod = A23ISOCod ;
      }
   }

   public void standaloneNotModal( )
   {
      Gx_BScreen = (byte)(0) ;
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         A1125AirLi = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  && ((GXutil.strcmp("", GXutil.rtrim( A1128AirLi))==0)) && ( Gx_BScreen == 0 ) )
      {
         A1128AirLi = "S" ;
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
      {
         if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
         {
            A1130Airli = "" ;
            n1130Airli = false ;
         }
      }
   }

   public void load2B226( )
   {
      /* Using cursor BC002B10 */
      pr_default.execute(8, new Object[] {A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound226 = (short)(1) ;
         A1126AirLi = BC002B10_A1126AirLi[0] ;
         A1125AirLi = BC002B10_A1125AirLi[0] ;
         A1130Airli = BC002B10_A1130Airli[0] ;
         n1130Airli = BC002B10_n1130Airli[0] ;
         A1118AirLi = BC002B10_A1118AirLi[0] ;
         A20ISODes = BC002B10_A20ISODes[0] ;
         n20ISODes = BC002B10_n20ISODes[0] ;
         A1119AirLi = BC002B10_A1119AirLi[0] ;
         A1120Airli = BC002B10_A1120Airli[0] ;
         A1121AirLi = BC002B10_A1121AirLi[0] ;
         A1122AirLi = BC002B10_A1122AirLi[0] ;
         A1123AirLi = BC002B10_A1123AirLi[0] ;
         A1124AirLi = BC002B10_A1124AirLi[0] ;
         A1127AirLi = BC002B10_A1127AirLi[0] ;
         n1127AirLi = BC002B10_n1127AirLi[0] ;
         A1128AirLi = BC002B10_A1128AirLi[0] ;
         A1129AirLi = BC002B10_A1129AirLi[0] ;
         n1129AirLi = BC002B10_n1129AirLi[0] ;
         zm2B226( -14) ;
      }
      pr_default.close(8);
      onLoadActions2B226( ) ;
   }

   public void onLoadActions2B226( )
   {
      if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
      {
         A1130Airli = "" ;
         n1130Airli = false ;
      }
   }

   public void checkExtendedTable2B226( )
   {
      standaloneModal( ) ;
      /* Using cursor BC002B9 */
      pr_default.execute(7, new Object[] {A23ISOCod});
      if ( (pr_default.getStatus(7) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A20ISODes = BC002B9_A20ISODes[0] ;
      n20ISODes = BC002B9_n20ISODes[0] ;
      pr_default.close(7);
      if ( ! ( (GXutil.nullDate().equals(A1125AirLi)) || (( A1125AirLi.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1125AirLi.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Record Creation Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1126AirLi)) || (( A1126AirLi.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1126AirLi.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Record Modification Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( ( GXutil.strcmp(A1128AirLi, "C") == 0 ) || ( GXutil.strcmp(A1128AirLi, "I") == 0 ) || ( GXutil.strcmp(A1128AirLi, "N") == 0 ) )
      {
         A1130Airli = "" ;
         n1130Airli = false ;
      }
   }

   public void closeExtendedTableCursors2B226( )
   {
      pr_default.close(7);
   }

   public void enableDisable( )
   {
   }

   public void getKey2B226( )
   {
      /* Using cursor BC002B11 */
      pr_default.execute(9, new Object[] {A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound226 = (short)(1) ;
      }
      else
      {
         RcdFound226 = (short)(0) ;
      }
      pr_default.close(9);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC002B8 */
      pr_default.execute(6, new Object[] {A23ISOCod, A1136AirLi});
      if ( (pr_default.getStatus(6) != 101) )
      {
         zm2B226( 14) ;
         RcdFound226 = (short)(1) ;
         A1136AirLi = BC002B8_A1136AirLi[0] ;
         A1126AirLi = BC002B8_A1126AirLi[0] ;
         A1125AirLi = BC002B8_A1125AirLi[0] ;
         A1130Airli = BC002B8_A1130Airli[0] ;
         n1130Airli = BC002B8_n1130Airli[0] ;
         A1118AirLi = BC002B8_A1118AirLi[0] ;
         A1119AirLi = BC002B8_A1119AirLi[0] ;
         A1120Airli = BC002B8_A1120Airli[0] ;
         A1121AirLi = BC002B8_A1121AirLi[0] ;
         A1122AirLi = BC002B8_A1122AirLi[0] ;
         A1123AirLi = BC002B8_A1123AirLi[0] ;
         A1124AirLi = BC002B8_A1124AirLi[0] ;
         A1127AirLi = BC002B8_A1127AirLi[0] ;
         n1127AirLi = BC002B8_n1127AirLi[0] ;
         A1128AirLi = BC002B8_A1128AirLi[0] ;
         A1129AirLi = BC002B8_A1129AirLi[0] ;
         n1129AirLi = BC002B8_n1129AirLi[0] ;
         A23ISOCod = BC002B8_A23ISOCod[0] ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         sMode226 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load2B226( ) ;
         Gx_mode = sMode226 ;
      }
      else
      {
         RcdFound226 = (short)(0) ;
         initializeNonKey2B226( ) ;
         sMode226 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode226 ;
      }
      pr_default.close(6);
   }

   public void getEqualNoModal( )
   {
      getKey2B226( ) ;
      if ( ( RcdFound226 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_2B0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency2B226( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002B7 */
         pr_default.execute(5, new Object[] {A23ISOCod, A1136AirLi});
         if ( ! (pr_default.getStatus(5) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"AIRLINES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         Gx_longc = false ;
         if ( (pr_default.getStatus(5) == 101) || !( Z1126AirLi.equals( BC002B7_A1126AirLi[0] ) ) || !( Z1125AirLi.equals( BC002B7_A1125AirLi[0] ) ) || ( GXutil.strcmp(Z1130Airli, BC002B7_A1130Airli[0]) != 0 ) || ( GXutil.strcmp(Z1118AirLi, BC002B7_A1118AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1119AirLi, BC002B7_A1119AirLi[0]) != 0 ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || ( GXutil.strcmp(Z1120Airli, BC002B7_A1120Airli[0]) != 0 ) || ( GXutil.strcmp(Z1121AirLi, BC002B7_A1121AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1122AirLi, BC002B7_A1122AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1123AirLi, BC002B7_A1123AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1124AirLi, BC002B7_A1124AirLi[0]) != 0 ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || ( GXutil.strcmp(Z1127AirLi, BC002B7_A1127AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1128AirLi, BC002B7_A1128AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1129AirLi, BC002B7_A1129AirLi[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"AIRLINES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2B226( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2B226( 0) ;
         checkOptimisticConcurrency2B226( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2B226( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B12 */
                  pr_default.execute(10, new Object[] {A1136AirLi, A1126AirLi, A1125AirLi, new Boolean(n1130Airli), A1130Airli, A1118AirLi, A1119AirLi, A1120Airli, A1121AirLi, A1122AirLi, A1123AirLi, A1124AirLi, new Boolean(n1127AirLi), A1127AirLi, A1128AirLi, new Boolean(n1129AirLi), A1129AirLi, A23ISOCod});
                  if ( (pr_default.getStatus(10) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2B226( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2B226( ) ;
         }
         endLevel2B226( ) ;
      }
      closeExtendedTableCursors2B226( ) ;
   }

   public void update2B226( )
   {
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B226( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2B226( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B13 */
                  pr_default.execute(11, new Object[] {A1126AirLi, A1125AirLi, new Boolean(n1130Airli), A1130Airli, A1118AirLi, A1119AirLi, A1120Airli, A1121AirLi, A1122AirLi, A1123AirLi, A1124AirLi, new Boolean(n1127AirLi), A1127AirLi, A1128AirLi, new Boolean(n1129AirLi), A1129AirLi, A23ISOCod, A1136AirLi});
                  deferredUpdate2B226( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel2B226( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           getByPrimaryKey( ) ;
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2B226( ) ;
      }
      closeExtendedTableCursors2B226( ) ;
   }

   public void deferredUpdate2B226( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2B226( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2B226( ) ;
         scanStart2B228( ) ;
         while ( ( RcdFound228 != 0 ) )
         {
            getByPrimaryKey2B228( ) ;
            delete2B228( ) ;
            scanNext2B228( ) ;
         }
         scanEnd2B228( ) ;
         scanStart2B227( ) ;
         while ( ( RcdFound227 != 0 ) )
         {
            getByPrimaryKey2B227( ) ;
            delete2B227( ) ;
            scanNext2B227( ) ;
         }
         scanEnd2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B226( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete2B226( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B14 */
                  pr_default.execute(12, new Object[] {A23ISOCod, A1136AirLi});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      sMode226 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2B226( ) ;
      Gx_mode = sMode226 ;
   }

   public void onDeleteControls2B226( )
   {
      standaloneModal( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor BC002B15 */
         pr_default.execute(13, new Object[] {A23ISOCod});
         A20ISODes = BC002B15_A20ISODes[0] ;
         n20ISODes = BC002B15_n20ISODes[0] ;
         pr_default.close(13);
      }
   }

   public void processNestedLevel2B227( )
   {
      nGXsfl_227_idx = (short)(0) ;
      while ( ( nGXsfl_227_idx < bcAirlines.getgxTv_SdtAirlines_Level1().size() ) )
      {
         readRow2B227( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound227 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_227 != 0 ) )
         {
            standaloneNotModal2B227( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert2B227( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete2B227( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update2B227( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_227 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_227 = (short)(1) ;
               Gxremove227 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_227 = (short)(0) ;
               Gxremove227 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcAirlines.getgxTv_SdtAirlines_Level1().removeElement(nGXsfl_227_idx);
            nGXsfl_227_idx = (short)(nGXsfl_227_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow227( ((SdtAirlines_Level1Item)bcAirlines.getgxTv_SdtAirlines_Level1().elementAt(-1+nGXsfl_227_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll2B227( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processNestedLevel2B228( )
   {
      nGXsfl_228_idx = (short)(0) ;
      while ( ( nGXsfl_228_idx < bcAirlines.getgxTv_SdtAirlines_Contacts().size() ) )
      {
         readRow2B228( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound228 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_228 != 0 ) )
         {
            standaloneNotModal2B228( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert2B228( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete2B228( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update2B228( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_228 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_228 = (short)(1) ;
               Gxremove228 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_228 = (short)(0) ;
               Gxremove228 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcAirlines.getgxTv_SdtAirlines_Contacts().removeElement(nGXsfl_228_idx);
            nGXsfl_228_idx = (short)(nGXsfl_228_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow228( ((SdtAirlines_Contacts)bcAirlines.getgxTv_SdtAirlines_Contacts().elementAt(-1+nGXsfl_228_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll2B228( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processLevel2B226( )
   {
      /* Save parent mode. */
      sMode226 = Gx_mode ;
      processNestedLevel2B227( ) ;
      processNestedLevel2B228( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode226 ;
      /* ' Update level parameters */
   }

   public void endLevel2B226( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(5);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2B226( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         pr_default.close(4);
         pr_default.close(3);
         pr_default.close(1);
         pr_default.close(0);
         pr_default.close(13);
         pr_default.close(2);
         Application.commit(context, remoteHandle, "DEFAULT", "tairlines_bc");
         if ( ( AnyError == 0 ) )
         {
            confirmValues2B0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         pr_default.close(4);
         pr_default.close(3);
         pr_default.close(1);
         pr_default.close(0);
         pr_default.close(13);
         pr_default.close(2);
         Application.rollback(context, remoteHandle, "DEFAULT", "tairlines_bc");
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2B226( )
   {
      /* Using cursor BC002B16 */
      pr_default.execute(14, new Object[] {A23ISOCod, A1136AirLi});
      RcdFound226 = (short)(0) ;
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound226 = (short)(1) ;
         A1136AirLi = BC002B16_A1136AirLi[0] ;
         A1126AirLi = BC002B16_A1126AirLi[0] ;
         A1125AirLi = BC002B16_A1125AirLi[0] ;
         A1130Airli = BC002B16_A1130Airli[0] ;
         n1130Airli = BC002B16_n1130Airli[0] ;
         A1118AirLi = BC002B16_A1118AirLi[0] ;
         A20ISODes = BC002B16_A20ISODes[0] ;
         n20ISODes = BC002B16_n20ISODes[0] ;
         A1119AirLi = BC002B16_A1119AirLi[0] ;
         A1120Airli = BC002B16_A1120Airli[0] ;
         A1121AirLi = BC002B16_A1121AirLi[0] ;
         A1122AirLi = BC002B16_A1122AirLi[0] ;
         A1123AirLi = BC002B16_A1123AirLi[0] ;
         A1124AirLi = BC002B16_A1124AirLi[0] ;
         A1127AirLi = BC002B16_A1127AirLi[0] ;
         n1127AirLi = BC002B16_n1127AirLi[0] ;
         A1128AirLi = BC002B16_A1128AirLi[0] ;
         A1129AirLi = BC002B16_A1129AirLi[0] ;
         n1129AirLi = BC002B16_n1129AirLi[0] ;
         A23ISOCod = BC002B16_A23ISOCod[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2B226( )
   {
      pr_default.readNext(14);
      RcdFound226 = (short)(0) ;
      scanLoad2B226( ) ;
   }

   public void scanLoad2B226( )
   {
      sMode226 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound226 = (short)(1) ;
         A1136AirLi = BC002B16_A1136AirLi[0] ;
         A1126AirLi = BC002B16_A1126AirLi[0] ;
         A1125AirLi = BC002B16_A1125AirLi[0] ;
         A1130Airli = BC002B16_A1130Airli[0] ;
         n1130Airli = BC002B16_n1130Airli[0] ;
         A1118AirLi = BC002B16_A1118AirLi[0] ;
         A20ISODes = BC002B16_A20ISODes[0] ;
         n20ISODes = BC002B16_n20ISODes[0] ;
         A1119AirLi = BC002B16_A1119AirLi[0] ;
         A1120Airli = BC002B16_A1120Airli[0] ;
         A1121AirLi = BC002B16_A1121AirLi[0] ;
         A1122AirLi = BC002B16_A1122AirLi[0] ;
         A1123AirLi = BC002B16_A1123AirLi[0] ;
         A1124AirLi = BC002B16_A1124AirLi[0] ;
         A1127AirLi = BC002B16_A1127AirLi[0] ;
         n1127AirLi = BC002B16_n1127AirLi[0] ;
         A1128AirLi = BC002B16_A1128AirLi[0] ;
         A1129AirLi = BC002B16_A1129AirLi[0] ;
         n1129AirLi = BC002B16_n1129AirLi[0] ;
         A23ISOCod = BC002B16_A23ISOCod[0] ;
      }
      Gx_mode = sMode226 ;
   }

   public void scanEnd2B226( )
   {
      pr_default.close(14);
   }

   public void afterConfirm2B226( )
   {
      /* After Confirm Rules */
      A1126AirLi = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1136AirLi))==0)) )
      {
         httpContext.GX_msglist.addItem("Airline Code cannot be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1118AirLi))==0)) )
      {
         httpContext.GX_msglist.addItem("Airline Description cannot be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
   }

   public void beforeInsert2B226( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2B226( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2B226( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2B226( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2B226( )
   {
      /* Before Validate Rules */
   }

   public void addRow2B226( )
   {
      VarsToRow226( bcAirlines) ;
   }

   public void sendRow2B226( )
   {
   }

   public void readRow2B226( )
   {
      RowToVars226( bcAirlines, 0) ;
   }

   public void zm2B227( int GX_JID )
   {
      if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
      {
         Z1132AirLi = A1132AirLi ;
         Z1118AirLi = A1118AirLi ;
         Z20ISODes = A20ISODes ;
         Z1119AirLi = A1119AirLi ;
         Z1120Airli = A1120Airli ;
         Z1121AirLi = A1121AirLi ;
         Z1122AirLi = A1122AirLi ;
         Z1123AirLi = A1123AirLi ;
         Z1124AirLi = A1124AirLi ;
         Z1125AirLi = A1125AirLi ;
         Z1126AirLi = A1126AirLi ;
         Z1127AirLi = A1127AirLi ;
         Z1128AirLi = A1128AirLi ;
         Z1129AirLi = A1129AirLi ;
         Z1130Airli = A1130Airli ;
         Z365Contac = A365Contac ;
         Z1138AirLi = A1138AirLi ;
         Z1133AirLi = A1133AirLi ;
         Z1134AirLi = A1134AirLi ;
         Z1135AirLi = A1135AirLi ;
         Z366Contac = A366Contac ;
      }
      if ( ( GX_JID == -16 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z1137AirLi = A1137AirLi ;
         Z1131AirLI = A1131AirLI ;
         Z1132AirLi = A1132AirLi ;
      }
   }

   public void standaloneNotModal2B227( )
   {
   }

   public void standaloneModal2B227( )
   {
   }

   public void load2B227( )
   {
      /* Using cursor BC002B17 */
      pr_default.execute(15, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
      if ( (pr_default.getStatus(15) != 101) )
      {
         RcdFound227 = (short)(1) ;
         A1131AirLI = BC002B17_A1131AirLI[0] ;
         A1132AirLi = BC002B17_A1132AirLi[0] ;
         zm2B227( -16) ;
      }
      pr_default.close(15);
      onLoadActions2B227( ) ;
   }

   public void onLoadActions2B227( )
   {
   }

   public void checkExtendedTable2B227( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal2B227( ) ;
      Gx_BScreen = (byte)(0) ;
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2B227( )
   {
   }

   public void enableDisable2B227( )
   {
   }

   public void getKey2B227( )
   {
      /* Using cursor BC002B18 */
      pr_default.execute(16, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
      if ( (pr_default.getStatus(16) != 101) )
      {
         RcdFound227 = (short)(1) ;
      }
      else
      {
         RcdFound227 = (short)(0) ;
      }
      pr_default.close(16);
   }

   public void getByPrimaryKey2B227( )
   {
      /* Using cursor BC002B6 */
      pr_default.execute(4, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
      if ( (pr_default.getStatus(4) != 101) )
      {
         zm2B227( 16) ;
         RcdFound227 = (short)(1) ;
         initializeNonKey2B227( ) ;
         A1137AirLi = BC002B6_A1137AirLi[0] ;
         A1131AirLI = BC002B6_A1131AirLI[0] ;
         A1132AirLi = BC002B6_A1132AirLi[0] ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z1137AirLi = A1137AirLi ;
         sMode227 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2B227( ) ;
         load2B227( ) ;
         Gx_mode = sMode227 ;
      }
      else
      {
         RcdFound227 = (short)(0) ;
         initializeNonKey2B227( ) ;
         sMode227 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2B227( ) ;
         Gx_mode = sMode227 ;
      }
      pr_default.close(4);
   }

   public void checkOptimisticConcurrency2B227( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002B5 */
         pr_default.execute(3, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
         if ( ! (pr_default.getStatus(3) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"AIRLINESBANK"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(3) == 101) || ( GXutil.strcmp(Z1132AirLi, BC002B5_A1132AirLi[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"AIRLINESBANK"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2B227( )
   {
      beforeValidate2B227( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B227( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2B227( 0) ;
         checkOptimisticConcurrency2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B227( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2B227( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B19 */
                  pr_default.execute(17, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi, A1131AirLI, A1132AirLi});
                  if ( (pr_default.getStatus(17) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2B227( ) ;
         }
         endLevel2B227( ) ;
      }
      closeExtendedTableCursors2B227( ) ;
   }

   public void update2B227( )
   {
      beforeValidate2B227( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B227( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B227( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2B227( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B20 */
                  pr_default.execute(18, new Object[] {A1131AirLI, A1132AirLi, A23ISOCod, A1136AirLi, A1137AirLi});
                  deferredUpdate2B227( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey2B227( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2B227( ) ;
      }
      closeExtendedTableCursors2B227( ) ;
   }

   public void deferredUpdate2B227( )
   {
   }

   public void delete2B227( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2B227( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B227( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2B227( ) ;
         /* No cascading delete specified. */
         afterConfirm2B227( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2B227( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC002B21 */
               pr_default.execute(19, new Object[] {A23ISOCod, A1136AirLi, A1137AirLi});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode227 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2B227( ) ;
      Gx_mode = sMode227 ;
   }

   public void onDeleteControls2B227( )
   {
      standaloneModal2B227( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel2B227( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(3);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2B227( )
   {
      /* Using cursor BC002B22 */
      pr_default.execute(20, new Object[] {A23ISOCod, A1136AirLi});
      RcdFound227 = (short)(0) ;
      if ( (pr_default.getStatus(20) != 101) )
      {
         RcdFound227 = (short)(1) ;
         A1137AirLi = BC002B22_A1137AirLi[0] ;
         A1131AirLI = BC002B22_A1131AirLI[0] ;
         A1132AirLi = BC002B22_A1132AirLi[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2B227( )
   {
      pr_default.readNext(20);
      RcdFound227 = (short)(0) ;
      scanLoad2B227( ) ;
   }

   public void scanLoad2B227( )
   {
      sMode227 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(20) != 101) )
      {
         RcdFound227 = (short)(1) ;
         A1137AirLi = BC002B22_A1137AirLi[0] ;
         A1131AirLI = BC002B22_A1131AirLI[0] ;
         A1132AirLi = BC002B22_A1132AirLi[0] ;
      }
      Gx_mode = sMode227 ;
   }

   public void scanEnd2B227( )
   {
      pr_default.close(20);
   }

   public void afterConfirm2B227( )
   {
      /* After Confirm Rules */
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  && true /* Level */ && ( AV26AlterB == 0 ) )
      {
         httpContext.GX_msglist.addItem("User cannot delete data bank", 1);
         AnyError = (short)(1) ;
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1137AirLi))==0)) )
      {
         httpContext.GX_msglist.addItem("Currency cannot be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1131AirLI))==0)) )
      {
         httpContext.GX_msglist.addItem("Bank Data cannot be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( A1132AirLi))==0)) )
      {
         httpContext.GX_msglist.addItem("Transf. Currency cannot be empty", 1);
         AnyError = (short)(1) ;
         return  ;
      }
   }

   public void beforeInsert2B227( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2B227( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2B227( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2B227( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2B227( )
   {
      /* Before Validate Rules */
   }

   public void addRow2B227( )
   {
      SdtAirlines_Level1Item obj227 ;
      obj227 = new SdtAirlines_Level1Item(remoteHandle);
      VarsToRow227( obj227) ;
      bcAirlines.getgxTv_SdtAirlines_Level1().add(obj227, 0);
      obj227.setgxTv_SdtAirlines_Level1Item_Mode( "UPD" );
      obj227.setgxTv_SdtAirlines_Level1Item_Modified( (short)(0) );
   }

   public void sendRow2B227( )
   {
   }

   public void readRow2B227( )
   {
      nGXsfl_227_idx = (short)(nGXsfl_227_idx+1) ;
      RowToVars227( ((SdtAirlines_Level1Item)bcAirlines.getgxTv_SdtAirlines_Level1().elementAt(-1+nGXsfl_227_idx)), 0) ;
   }

   public void zm2B228( int GX_JID )
   {
      if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
      {
         Z1133AirLi = A1133AirLi ;
         Z1134AirLi = A1134AirLi ;
         Z1135AirLi = A1135AirLi ;
         Z1118AirLi = A1118AirLi ;
         Z20ISODes = A20ISODes ;
         Z1119AirLi = A1119AirLi ;
         Z1120Airli = A1120Airli ;
         Z1121AirLi = A1121AirLi ;
         Z1122AirLi = A1122AirLi ;
         Z1123AirLi = A1123AirLi ;
         Z1124AirLi = A1124AirLi ;
         Z1125AirLi = A1125AirLi ;
         Z1126AirLi = A1126AirLi ;
         Z1127AirLi = A1127AirLi ;
         Z1128AirLi = A1128AirLi ;
         Z1129AirLi = A1129AirLi ;
         Z1130Airli = A1130Airli ;
         Z1137AirLi = A1137AirLi ;
         Z1131AirLI = A1131AirLI ;
         Z1132AirLi = A1132AirLi ;
      }
      if ( ( GX_JID == 18 ) || ( GX_JID == 0 ) )
      {
         Z366Contac = A366Contac ;
         Z1118AirLi = A1118AirLi ;
         Z20ISODes = A20ISODes ;
         Z1119AirLi = A1119AirLi ;
         Z1120Airli = A1120Airli ;
         Z1121AirLi = A1121AirLi ;
         Z1122AirLi = A1122AirLi ;
         Z1123AirLi = A1123AirLi ;
         Z1124AirLi = A1124AirLi ;
         Z1125AirLi = A1125AirLi ;
         Z1126AirLi = A1126AirLi ;
         Z1127AirLi = A1127AirLi ;
         Z1128AirLi = A1128AirLi ;
         Z1129AirLi = A1129AirLi ;
         Z1130Airli = A1130Airli ;
         Z1137AirLi = A1137AirLi ;
         Z1131AirLI = A1131AirLI ;
         Z1132AirLi = A1132AirLi ;
      }
      if ( ( GX_JID == -17 ) )
      {
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z1138AirLi = A1138AirLi ;
         Z1133AirLi = A1133AirLi ;
         Z1134AirLi = A1134AirLi ;
         Z1135AirLi = A1135AirLi ;
         Z365Contac = A365Contac ;
      }
   }

   public void standaloneNotModal2B228( )
   {
   }

   public void standaloneModal2B228( )
   {
   }

   public void load2B228( )
   {
      /* Using cursor BC002B23 */
      pr_default.execute(21, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
      if ( (pr_default.getStatus(21) != 101) )
      {
         RcdFound228 = (short)(1) ;
         A1133AirLi = BC002B23_A1133AirLi[0] ;
         A1134AirLi = BC002B23_A1134AirLi[0] ;
         A1135AirLi = BC002B23_A1135AirLi[0] ;
         A366Contac = BC002B23_A366Contac[0] ;
         zm2B228( -17) ;
      }
      pr_default.close(21);
      onLoadActions2B228( ) ;
   }

   public void onLoadActions2B228( )
   {
   }

   public void checkExtendedTable2B228( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal2B228( ) ;
      Gx_BScreen = (byte)(0) ;
      /* Using cursor BC002B4 */
      pr_default.execute(2, new Object[] {A365Contac});
      if ( (pr_default.getStatus(2) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Contact Types'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A366Contac = BC002B4_A366Contac[0] ;
      pr_default.close(2);
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors2B228( )
   {
      pr_default.close(2);
   }

   public void enableDisable2B228( )
   {
   }

   public void getKey2B228( )
   {
      /* Using cursor BC002B24 */
      pr_default.execute(22, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
      if ( (pr_default.getStatus(22) != 101) )
      {
         RcdFound228 = (short)(1) ;
      }
      else
      {
         RcdFound228 = (short)(0) ;
      }
      pr_default.close(22);
   }

   public void getByPrimaryKey2B228( )
   {
      /* Using cursor BC002B3 */
      pr_default.execute(1, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm2B228( 17) ;
         RcdFound228 = (short)(1) ;
         initializeNonKey2B228( ) ;
         A1138AirLi = BC002B3_A1138AirLi[0] ;
         A1133AirLi = BC002B3_A1133AirLi[0] ;
         A1134AirLi = BC002B3_A1134AirLi[0] ;
         A1135AirLi = BC002B3_A1135AirLi[0] ;
         A365Contac = BC002B3_A365Contac[0] ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
         Z365Contac = A365Contac ;
         Z1138AirLi = A1138AirLi ;
         sMode228 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2B228( ) ;
         load2B228( ) ;
         Gx_mode = sMode228 ;
      }
      else
      {
         RcdFound228 = (short)(0) ;
         initializeNonKey2B228( ) ;
         sMode228 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal2B228( ) ;
         Gx_mode = sMode228 ;
      }
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency2B228( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002B2 */
         pr_default.execute(0, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"AIRLINESCONTACTS"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1133AirLi, BC002B2_A1133AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1134AirLi, BC002B2_A1134AirLi[0]) != 0 ) || ( GXutil.strcmp(Z1135AirLi, BC002B2_A1135AirLi[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"AIRLINESCONTACTS"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2B228( )
   {
      beforeValidate2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B228( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2B228( 0) ;
         checkOptimisticConcurrency2B228( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2B228( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B25 */
                  pr_default.execute(23, new Object[] {A23ISOCod, A1136AirLi, new Short(A1138AirLi), A1133AirLi, A1134AirLi, A1135AirLi, A365Contac});
                  if ( (pr_default.getStatus(23) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2B228( ) ;
         }
         endLevel2B228( ) ;
      }
      closeExtendedTableCursors2B228( ) ;
   }

   public void update2B228( )
   {
      beforeValidate2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2B228( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B228( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2B228( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002B26 */
                  pr_default.execute(24, new Object[] {A1133AirLi, A1134AirLi, A1135AirLi, A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
                  deferredUpdate2B228( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey2B228( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2B228( ) ;
      }
      closeExtendedTableCursors2B228( ) ;
   }

   public void deferredUpdate2B228( )
   {
   }

   public void delete2B228( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2B228( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2B228( ) ;
         /* No cascading delete specified. */
         afterConfirm2B228( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2B228( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC002B27 */
               pr_default.execute(25, new Object[] {A23ISOCod, A1136AirLi, A365Contac, new Short(A1138AirLi)});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode228 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2B228( ) ;
      Gx_mode = sMode228 ;
   }

   public void onDeleteControls2B228( )
   {
      standaloneModal2B228( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor BC002B28 */
         pr_default.execute(26, new Object[] {A365Contac});
         A366Contac = BC002B28_A366Contac[0] ;
         pr_default.close(26);
      }
   }

   public void endLevel2B228( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2B228( )
   {
      /* Using cursor BC002B29 */
      pr_default.execute(27, new Object[] {A23ISOCod, A1136AirLi});
      RcdFound228 = (short)(0) ;
      if ( (pr_default.getStatus(27) != 101) )
      {
         RcdFound228 = (short)(1) ;
         A1138AirLi = BC002B29_A1138AirLi[0] ;
         A1133AirLi = BC002B29_A1133AirLi[0] ;
         A1134AirLi = BC002B29_A1134AirLi[0] ;
         A1135AirLi = BC002B29_A1135AirLi[0] ;
         A366Contac = BC002B29_A366Contac[0] ;
         A365Contac = BC002B29_A365Contac[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2B228( )
   {
      pr_default.readNext(27);
      RcdFound228 = (short)(0) ;
      scanLoad2B228( ) ;
   }

   public void scanLoad2B228( )
   {
      sMode228 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(27) != 101) )
      {
         RcdFound228 = (short)(1) ;
         A1138AirLi = BC002B29_A1138AirLi[0] ;
         A1133AirLi = BC002B29_A1133AirLi[0] ;
         A1134AirLi = BC002B29_A1134AirLi[0] ;
         A1135AirLi = BC002B29_A1135AirLi[0] ;
         A366Contac = BC002B29_A366Contac[0] ;
         A365Contac = BC002B29_A365Contac[0] ;
      }
      Gx_mode = sMode228 ;
   }

   public void scanEnd2B228( )
   {
      pr_default.close(27);
   }

   public void afterConfirm2B228( )
   {
      /* After Confirm Rules */
      if ( true /* Level */ && ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         GXt_int1 = A1138AirLi ;
         GXv_int2[0] = GXt_int1 ;
         new pairlinecttseq(remoteHandle, context).execute( A23ISOCod, A1136AirLi, A365Contac, GXv_int2) ;
         tairlines_bc.this.GXt_int1 = GXv_int2[0] ;
         A1138AirLi = GXt_int1 ;
      }
   }

   public void beforeInsert2B228( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2B228( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2B228( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2B228( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2B228( )
   {
      /* Before Validate Rules */
   }

   public void addRow2B228( )
   {
      SdtAirlines_Contacts obj228 ;
      obj228 = new SdtAirlines_Contacts(remoteHandle);
      VarsToRow228( obj228) ;
      bcAirlines.getgxTv_SdtAirlines_Contacts().add(obj228, 0);
      obj228.setgxTv_SdtAirlines_Contacts_Mode( "UPD" );
      obj228.setgxTv_SdtAirlines_Contacts_Modified( (short)(0) );
   }

   public void sendRow2B228( )
   {
   }

   public void readRow2B228( )
   {
      nGXsfl_228_idx = (short)(nGXsfl_228_idx+1) ;
      RowToVars228( ((SdtAirlines_Contacts)bcAirlines.getgxTv_SdtAirlines_Contacts().elementAt(-1+nGXsfl_228_idx)), 0) ;
   }

   public void confirmValues2B0( )
   {
   }

   public void initializeNonKey2B226( )
   {
      A1126AirLi = GXutil.resetTime( GXutil.nullDate() );
      A1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      A1130Airli = "" ;
      n1130Airli = false ;
      A1118AirLi = "" ;
      A20ISODes = "" ;
      n20ISODes = false ;
      A1119AirLi = "" ;
      A1120Airli = "" ;
      A1121AirLi = "" ;
      A1122AirLi = "" ;
      A1123AirLi = "" ;
      A1124AirLi = "" ;
      A1127AirLi = "" ;
      n1127AirLi = false ;
      A1128AirLi = "S" ;
      A1129AirLi = "" ;
      n1129AirLi = false ;
   }

   public void initAll2B226( )
   {
      A23ISOCod = "" ;
      A1136AirLi = "" ;
      initializeNonKey2B226( ) ;
   }

   public void standaloneModalInsert( )
   {
      A1125AirLi = i1125AirLi ;
      A1128AirLi = i1128AirLi ;
   }

   public void initializeNonKey2B227( )
   {
      AV26AlterB = (byte)(0) ;
      A1131AirLI = "" ;
      A1132AirLi = "" ;
   }

   public void initAll2B227( )
   {
      A1137AirLi = "" ;
      initializeNonKey2B227( ) ;
   }

   public void standaloneModalInsert2B227( )
   {
   }

   public void initializeNonKey2B228( )
   {
      A1133AirLi = "" ;
      A1134AirLi = "" ;
      A1135AirLi = "" ;
      A366Contac = "" ;
   }

   public void initAll2B228( )
   {
      A365Contac = "" ;
      A1138AirLi = (short)(0) ;
      initializeNonKey2B228( ) ;
   }

   public void standaloneModalInsert2B228( )
   {
   }

   public void VarsToRow226( SdtAirlines obj226 )
   {
      obj226.setgxTv_SdtAirlines_Mode( Gx_mode );
      obj226.setgxTv_SdtAirlines_Airlinedatmod( A1126AirLi );
      obj226.setgxTv_SdtAirlines_Airlinedatins( A1125AirLi );
      obj226.setgxTv_SdtAirlines_Airlinebanktransfermode( A1130Airli );
      obj226.setgxTv_SdtAirlines_Airlinedescription( A1118AirLi );
      obj226.setgxTv_SdtAirlines_Isodes( A20ISODes );
      obj226.setgxTv_SdtAirlines_Airlineabbreviation( A1119AirLi );
      obj226.setgxTv_SdtAirlines_Airlineaddress( A1120Airli );
      obj226.setgxTv_SdtAirlines_Airlineaddresscompl( A1121AirLi );
      obj226.setgxTv_SdtAirlines_Airlinecity( A1122AirLi );
      obj226.setgxTv_SdtAirlines_Airlinestate( A1123AirLi );
      obj226.setgxTv_SdtAirlines_Airlinezip( A1124AirLi );
      obj226.setgxTv_SdtAirlines_Airlinecompleteaddress( A1127AirLi );
      obj226.setgxTv_SdtAirlines_Airlinetypesettlement( A1128AirLi );
      obj226.setgxTv_SdtAirlines_Airlinesta( A1129AirLi );
      obj226.setgxTv_SdtAirlines_Isocod( A23ISOCod );
      obj226.setgxTv_SdtAirlines_Airlinecode( A1136AirLi );
      obj226.setgxTv_SdtAirlines_Isocod_Z( Z23ISOCod );
      obj226.setgxTv_SdtAirlines_Airlinecode_Z( Z1136AirLi );
      obj226.setgxTv_SdtAirlines_Airlinedescription_Z( Z1118AirLi );
      obj226.setgxTv_SdtAirlines_Isodes_Z( Z20ISODes );
      obj226.setgxTv_SdtAirlines_Airlineabbreviation_Z( Z1119AirLi );
      obj226.setgxTv_SdtAirlines_Airlineaddress_Z( Z1120Airli );
      obj226.setgxTv_SdtAirlines_Airlineaddresscompl_Z( Z1121AirLi );
      obj226.setgxTv_SdtAirlines_Airlinecity_Z( Z1122AirLi );
      obj226.setgxTv_SdtAirlines_Airlinestate_Z( Z1123AirLi );
      obj226.setgxTv_SdtAirlines_Airlinezip_Z( Z1124AirLi );
      obj226.setgxTv_SdtAirlines_Airlinedatins_Z( Z1125AirLi );
      obj226.setgxTv_SdtAirlines_Airlinedatmod_Z( Z1126AirLi );
      obj226.setgxTv_SdtAirlines_Airlinecompleteaddress_Z( Z1127AirLi );
      obj226.setgxTv_SdtAirlines_Airlinetypesettlement_Z( Z1128AirLi );
      obj226.setgxTv_SdtAirlines_Airlinesta_Z( Z1129AirLi );
      obj226.setgxTv_SdtAirlines_Airlinebanktransfermode_Z( Z1130Airli );
      obj226.setgxTv_SdtAirlines_Isodes_N( (byte)((byte)((n20ISODes)?1:0)) );
      obj226.setgxTv_SdtAirlines_Airlinecompleteaddress_N( (byte)((byte)((n1127AirLi)?1:0)) );
      obj226.setgxTv_SdtAirlines_Airlinesta_N( (byte)((byte)((n1129AirLi)?1:0)) );
      obj226.setgxTv_SdtAirlines_Airlinebanktransfermode_N( (byte)((byte)((n1130Airli)?1:0)) );
      obj226.setgxTv_SdtAirlines_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars226( SdtAirlines obj226 ,
                             int forceLoad )
   {
      Gx_mode = obj226.getgxTv_SdtAirlines_Mode() ;
      A1126AirLi = obj226.getgxTv_SdtAirlines_Airlinedatmod() ;
      A1125AirLi = obj226.getgxTv_SdtAirlines_Airlinedatins() ;
      A1130Airli = obj226.getgxTv_SdtAirlines_Airlinebanktransfermode() ;
      A1118AirLi = obj226.getgxTv_SdtAirlines_Airlinedescription() ;
      A20ISODes = obj226.getgxTv_SdtAirlines_Isodes() ;
      A1119AirLi = obj226.getgxTv_SdtAirlines_Airlineabbreviation() ;
      A1120Airli = obj226.getgxTv_SdtAirlines_Airlineaddress() ;
      A1121AirLi = obj226.getgxTv_SdtAirlines_Airlineaddresscompl() ;
      A1122AirLi = obj226.getgxTv_SdtAirlines_Airlinecity() ;
      A1123AirLi = obj226.getgxTv_SdtAirlines_Airlinestate() ;
      A1124AirLi = obj226.getgxTv_SdtAirlines_Airlinezip() ;
      A1127AirLi = obj226.getgxTv_SdtAirlines_Airlinecompleteaddress() ;
      A1128AirLi = obj226.getgxTv_SdtAirlines_Airlinetypesettlement() ;
      A1129AirLi = obj226.getgxTv_SdtAirlines_Airlinesta() ;
      if ( ( forceLoad == 1 ) )
      {
         A23ISOCod = obj226.getgxTv_SdtAirlines_Isocod() ;
      }
      A1136AirLi = obj226.getgxTv_SdtAirlines_Airlinecode() ;
      Z23ISOCod = obj226.getgxTv_SdtAirlines_Isocod_Z() ;
      Z1136AirLi = obj226.getgxTv_SdtAirlines_Airlinecode_Z() ;
      Z1118AirLi = obj226.getgxTv_SdtAirlines_Airlinedescription_Z() ;
      Z20ISODes = obj226.getgxTv_SdtAirlines_Isodes_Z() ;
      Z1119AirLi = obj226.getgxTv_SdtAirlines_Airlineabbreviation_Z() ;
      Z1120Airli = obj226.getgxTv_SdtAirlines_Airlineaddress_Z() ;
      Z1121AirLi = obj226.getgxTv_SdtAirlines_Airlineaddresscompl_Z() ;
      Z1122AirLi = obj226.getgxTv_SdtAirlines_Airlinecity_Z() ;
      Z1123AirLi = obj226.getgxTv_SdtAirlines_Airlinestate_Z() ;
      Z1124AirLi = obj226.getgxTv_SdtAirlines_Airlinezip_Z() ;
      Z1125AirLi = obj226.getgxTv_SdtAirlines_Airlinedatins_Z() ;
      Z1126AirLi = obj226.getgxTv_SdtAirlines_Airlinedatmod_Z() ;
      Z1127AirLi = obj226.getgxTv_SdtAirlines_Airlinecompleteaddress_Z() ;
      Z1128AirLi = obj226.getgxTv_SdtAirlines_Airlinetypesettlement_Z() ;
      Z1129AirLi = obj226.getgxTv_SdtAirlines_Airlinesta_Z() ;
      Z1130Airli = obj226.getgxTv_SdtAirlines_Airlinebanktransfermode_Z() ;
      n20ISODes = (boolean)((obj226.getgxTv_SdtAirlines_Isodes_N()==0)?false:true) ;
      n1127AirLi = (boolean)((obj226.getgxTv_SdtAirlines_Airlinecompleteaddress_N()==0)?false:true) ;
      n1129AirLi = (boolean)((obj226.getgxTv_SdtAirlines_Airlinesta_N()==0)?false:true) ;
      n1130Airli = (boolean)((obj226.getgxTv_SdtAirlines_Airlinebanktransfermode_N()==0)?false:true) ;
      Gx_mode = obj226.getgxTv_SdtAirlines_Mode() ;
      return  ;
   }

   public void VarsToRow227( SdtAirlines_Level1Item obj227 )
   {
      obj227.setgxTv_SdtAirlines_Level1Item_Mode( Gx_mode );
      obj227.setgxTv_SdtAirlines_Level1Item_Airlinedatabank( A1131AirLI );
      obj227.setgxTv_SdtAirlines_Level1Item_Airlinecurtrans( A1132AirLi );
      obj227.setgxTv_SdtAirlines_Level1Item_Airlinecurcode( A1137AirLi );
      obj227.setgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z( Z1137AirLi );
      obj227.setgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z( Z1131AirLI );
      obj227.setgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z( Z1132AirLi );
      obj227.setgxTv_SdtAirlines_Level1Item_Modified( nIsMod_227 );
      return  ;
   }

   public void RowToVars227( SdtAirlines_Level1Item obj227 ,
                             int forceLoad )
   {
      Gx_mode = obj227.getgxTv_SdtAirlines_Level1Item_Mode() ;
      A1131AirLI = obj227.getgxTv_SdtAirlines_Level1Item_Airlinedatabank() ;
      A1132AirLi = obj227.getgxTv_SdtAirlines_Level1Item_Airlinecurtrans() ;
      A1137AirLi = obj227.getgxTv_SdtAirlines_Level1Item_Airlinecurcode() ;
      Z1137AirLi = obj227.getgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z() ;
      Z1131AirLI = obj227.getgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z() ;
      Z1132AirLi = obj227.getgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z() ;
      nIsMod_227 = obj227.getgxTv_SdtAirlines_Level1Item_Modified() ;
      return  ;
   }

   public void VarsToRow228( SdtAirlines_Contacts obj228 )
   {
      obj228.setgxTv_SdtAirlines_Contacts_Mode( Gx_mode );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttname( A1133AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttphone( A1134AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttemail( A1135AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Contacttypesdescription( A366Contac );
      obj228.setgxTv_SdtAirlines_Contacts_Contacttypescode( A365Contac );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttseq( A1138AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Contacttypescode_Z( Z365Contac );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttseq_Z( Z1138AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttname_Z( Z1133AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttphone_Z( Z1134AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Airlinecttemail_Z( Z1135AirLi );
      obj228.setgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z( Z366Contac );
      obj228.setgxTv_SdtAirlines_Contacts_Modified( nIsMod_228 );
      return  ;
   }

   public void RowToVars228( SdtAirlines_Contacts obj228 ,
                             int forceLoad )
   {
      Gx_mode = obj228.getgxTv_SdtAirlines_Contacts_Mode() ;
      A1133AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttname() ;
      A1134AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttphone() ;
      A1135AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttemail() ;
      A366Contac = obj228.getgxTv_SdtAirlines_Contacts_Contacttypesdescription() ;
      A365Contac = obj228.getgxTv_SdtAirlines_Contacts_Contacttypescode() ;
      A1138AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttseq() ;
      Z365Contac = obj228.getgxTv_SdtAirlines_Contacts_Contacttypescode_Z() ;
      Z1138AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttseq_Z() ;
      Z1133AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttname_Z() ;
      Z1134AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttphone_Z() ;
      Z1135AirLi = obj228.getgxTv_SdtAirlines_Contacts_Airlinecttemail_Z() ;
      Z366Contac = obj228.getgxTv_SdtAirlines_Contacts_Contacttypesdescription_Z() ;
      nIsMod_228 = obj228.getgxTv_SdtAirlines_Contacts_Modified() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A23ISOCod = (String)obj[0] ;
      A1136AirLi = (String)obj[1] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey2B226( ) ;
      scanStart2B226( ) ;
      if ( ( RcdFound226 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z23ISOCod = A23ISOCod ;
         Z1136AirLi = A1136AirLi ;
      }
      onLoadActions2B226( ) ;
      zm2B226( 0) ;
      addRow2B226( ) ;
      bcAirlines.getgxTv_SdtAirlines_Level1().clearCollection();
      if ( ( RcdFound226 == 1 ) )
      {
         scanStart2B227( ) ;
         nGXsfl_227_idx = (short)(1) ;
         while ( ( RcdFound227 != 0 ) )
         {
            onLoadActions2B227( ) ;
            Z23ISOCod = A23ISOCod ;
            Z1136AirLi = A1136AirLi ;
            Z1137AirLi = A1137AirLi ;
            zm2B227( 0) ;
            nRcdExists_227 = (short)(1) ;
            nIsMod_227 = (short)(0) ;
            addRow2B227( ) ;
            nGXsfl_227_idx = (short)(nGXsfl_227_idx+1) ;
            scanNext2B227( ) ;
         }
         scanEnd2B227( ) ;
      }
      bcAirlines.getgxTv_SdtAirlines_Contacts().clearCollection();
      if ( ( RcdFound226 == 1 ) )
      {
         scanStart2B228( ) ;
         nGXsfl_228_idx = (short)(1) ;
         while ( ( RcdFound228 != 0 ) )
         {
            onLoadActions2B228( ) ;
            Z23ISOCod = A23ISOCod ;
            Z1136AirLi = A1136AirLi ;
            Z365Contac = A365Contac ;
            Z1138AirLi = A1138AirLi ;
            zm2B228( 0) ;
            nRcdExists_228 = (short)(1) ;
            nIsMod_228 = (short)(0) ;
            addRow2B228( ) ;
            nGXsfl_228_idx = (short)(nGXsfl_228_idx+1) ;
            scanNext2B228( ) ;
         }
         scanEnd2B228( ) ;
      }
      scanEnd2B226( ) ;
      if ( ( RcdFound226 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars226( bcAirlines, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey2B226( ) ;
      if ( ( RcdFound226 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            A1136AirLi = Z1136AirLi ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update2B226( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2B226( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2B226( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow226( bcAirlines) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars226( bcAirlines, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey2B226( ) ;
      if ( ( RcdFound226 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            A1136AirLi = Z1136AirLi ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A1136AirLi, Z1136AirLi) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      pr_default.close(13);
      pr_default.close(26);
      Application.rollback(context, remoteHandle, "DEFAULT", "tairlines_bc");
      VarsToRow226( bcAirlines) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcAirlines.getgxTv_SdtAirlines_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcAirlines.setgxTv_SdtAirlines_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtAirlines sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcAirlines ) )
      {
         bcAirlines = sdt ;
         if ( ( GXutil.strcmp(bcAirlines.getgxTv_SdtAirlines_Mode(), "") == 0 ) )
         {
            bcAirlines.setgxTv_SdtAirlines_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow226( bcAirlines) ;
         }
         else
         {
            RowToVars226( bcAirlines, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcAirlines.getgxTv_SdtAirlines_Mode(), "") == 0 ) )
         {
            bcAirlines.setgxTv_SdtAirlines_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars226( bcAirlines, 1) ;
      return  ;
   }

   public SdtAirlines getAirlines_BC( )
   {
      return bcAirlines ;
   }


   public void webExecute( )
   {
      com.genexus.internet.HttpClient GXSoapHTTPClient ;
      GXSoapHTTPClient = new com.genexus.internet.HttpClient();
      com.genexus.internet.HttpRequest GXSoapHTTPRequest ;
      GXSoapHTTPRequest = httpContext.getHttpRequest();
      com.genexus.xml.XMLReader GXSoapXMLReader ;
      GXSoapXMLReader = new com.genexus.xml.XMLReader();
      short GXSoapError ;
      GXSoapError = 0;
      com.genexus.internet.HttpResponse GXSoapHTTPResponse ;
      GXSoapHTTPResponse = httpContext.getHttpResponse();
      com.genexus.xml.XMLWriter GXSoapXMLWriter ;
      GXSoapXMLWriter = new com.genexus.xml.XMLWriter();
      GXSoapHTTPResponse.addHeader("Content-type", "text/xml;charset=utf-8");
      if ( ( GXutil.strcmp(GXutil.lower( GXSoapHTTPRequest.getMethod()), "get") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.lower( GXSoapHTTPRequest.getQuerystring()), "wsdl") == 0 ) )
         {
            GXSoapXMLWriter.openResponse(GXSoapHTTPResponse);
            GXSoapXMLWriter.writeStartDocument("utf-8", (byte)(0));
            GXSoapXMLWriter.writeStartElement("definitions");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC");
            GXSoapXMLWriter.writeAttribute("targetNamespace", "IataICSI");
            GXSoapXMLWriter.writeAttribute("xmlns:tns", "IataICSI");
            GXSoapXMLWriter.writeAttribute("xmlns:wsdlns", "IataICSI");
            GXSoapXMLWriter.writeAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
            GXSoapXMLWriter.writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            GXSoapXMLWriter.writeAttribute("xmlns", "http://schemas.xmlsoap.org/wsdl/");
            GXSoapXMLWriter.writeAttribute("xmlns:tns2", "IataICSI");
            GXSoapXMLWriter.writeAttribute("xmlns:tns1", "Genexus");
            GXSoapXMLWriter.writeStartElement("document");
            GXSoapXMLWriter.writeElement("URL", ((httpContext.getHttpSecure( )==1) ? "https://" : "http://")+httpContext.getServerName( )+((httpContext.getServerPort( )>0)&&(httpContext.getServerPort( )!=80)&&(httpContext.getServerPort( )!=443) ? ":"+GXutil.ltrim( GXutil.str( httpContext.getServerPort( ), 6, 0)) : "")+httpContext.getScriptPath( )+""+"HLP_TAirlines_BC.htm");
            GXSoapXMLWriter.writeElement("line", " ");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("types");
            GXSoapXMLWriter.writeStartElement("schema");
            GXSoapXMLWriter.writeAttribute("targetNamespace", "IataICSI");
            GXSoapXMLWriter.writeAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
            GXSoapXMLWriter.writeAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
            GXSoapXMLWriter.writeAttribute("elementFormDefault", "qualified");
            GXSoapXMLWriter.writeElement("import", "");
            GXSoapXMLWriter.writeAttribute("namespace", "Genexus");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "Airlines");
            GXSoapXMLWriter.writeStartElement("all");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ISOCod");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineDescription");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ISODes");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineAbbreviation");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirlineAddress");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineAddressCompl");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCity");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineState");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineZIP");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineDatIns");
            GXSoapXMLWriter.writeAttribute("type", "xsd:dateTime");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineDatMod");
            GXSoapXMLWriter.writeAttribute("type", "xsd:dateTime");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCompleteAddress");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineTypeSettlement");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineSta");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirlineBankTransferMode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Level1");
            GXSoapXMLWriter.writeAttribute("type", "tns2:ArrayOfAirlines.Level1Item");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Contacts");
            GXSoapXMLWriter.writeAttribute("type", "tns2:ArrayOfAirlines.Contacts");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Mode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ISOCod_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCode_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineDescription_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ISODes_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineAbbreviation_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirlineAddress_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineAddressCompl_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCity_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineState_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineZIP_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineDatIns_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:dateTime");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineDatMod_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:dateTime");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCompleteAddress_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineTypeSettlement_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineSta_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirlineBankTransferMode_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ISODes_N");
            GXSoapXMLWriter.writeAttribute("type", "xsd:byte");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCompleteAddress_N");
            GXSoapXMLWriter.writeAttribute("type", "xsd:byte");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineSta_N");
            GXSoapXMLWriter.writeAttribute("type", "xsd:byte");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirlineBankTransferMode_N");
            GXSoapXMLWriter.writeAttribute("type", "xsd:byte");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "ArrayOfAirlines.Level1Item");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("minOccurs", "0");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "unbounded");
            GXSoapXMLWriter.writeAttribute("name", "Airlines.Level1Item");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines.Level1Item");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "Airlines.Level1Item");
            GXSoapXMLWriter.writeStartElement("all");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCurCode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLIneDataBank");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCurTrans");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Mode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Modified");
            GXSoapXMLWriter.writeAttribute("type", "xsd:short");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCurCode_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLIneDataBank_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCurTrans_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "ArrayOfAirlines.Contacts");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("minOccurs", "0");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "unbounded");
            GXSoapXMLWriter.writeAttribute("name", "Airlines.Contacts");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines.Contacts");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "Airlines.Contacts");
            GXSoapXMLWriter.writeStartElement("all");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ContactTypesCode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttSeq");
            GXSoapXMLWriter.writeAttribute("type", "xsd:short");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttName");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttPhone");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttEmail");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ContactTypesDescription");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Mode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Modified");
            GXSoapXMLWriter.writeAttribute("type", "xsd:short");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ContactTypesCode_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttSeq_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:short");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttName_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttPhone_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "AirLineCttEmail_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "ContactTypesDescription_Z");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.LoadKeySvc");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "Isocod");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "Airlinecode");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.LoadKeySvcResponse");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlinesMessages");
            GXSoapXMLWriter.writeAttribute("type", "tns1:Messages");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.SaveSvc");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.SaveSvcResponse");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlinesMessages");
            GXSoapXMLWriter.writeAttribute("type", "tns1:Messages");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.CheckSvc");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.CheckSvcResponse");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlinesMessages");
            GXSoapXMLWriter.writeAttribute("type", "tns1:Messages");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.DeleteSvc");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.DeleteSvcResponse");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlines");
            GXSoapXMLWriter.writeAttribute("type", "tns2:Airlines");
            GXSoapXMLWriter.writeElement("element", "");
            GXSoapXMLWriter.writeAttribute("minOccurs", "1");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "1");
            GXSoapXMLWriter.writeAttribute("name", "bcAirlinesMessages");
            GXSoapXMLWriter.writeAttribute("type", "tns1:Messages");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("schema");
            GXSoapXMLWriter.writeAttribute("targetNamespace", "Genexus");
            GXSoapXMLWriter.writeAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
            GXSoapXMLWriter.writeAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
            GXSoapXMLWriter.writeAttribute("elementFormDefault", "qualified");
            GXSoapXMLWriter.writeElement("import", "");
            GXSoapXMLWriter.writeAttribute("namespace", "IataICSI");
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "Messages");
            GXSoapXMLWriter.writeStartElement("sequence");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("minOccurs", "0");
            GXSoapXMLWriter.writeAttribute("maxOccurs", "unbounded");
            GXSoapXMLWriter.writeAttribute("name", "Messages.Message");
            GXSoapXMLWriter.writeAttribute("type", "tns1:Messages.Message");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("complexType");
            GXSoapXMLWriter.writeAttribute("name", "Messages.Message");
            GXSoapXMLWriter.writeStartElement("all");
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Id");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Type");
            GXSoapXMLWriter.writeAttribute("type", "xsd:byte");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("element");
            GXSoapXMLWriter.writeAttribute("name", "Description");
            GXSoapXMLWriter.writeAttribute("type", "xsd:string");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.LoadKeySvcSoapIn");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.LoadKeySvc");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.LoadKeySvcSoapOut");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.LoadKeySvcResponse");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.SaveSvcSoapIn");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.SaveSvc");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.SaveSvcSoapOut");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.SaveSvcResponse");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.CheckSvcSoapIn");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.CheckSvc");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.CheckSvcSoapOut");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.CheckSvcResponse");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.DeleteSvcSoapIn");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.DeleteSvc");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("message");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC.DeleteSvcSoapOut");
            GXSoapXMLWriter.writeElement("part", "");
            GXSoapXMLWriter.writeAttribute("name", "parameters");
            GXSoapXMLWriter.writeAttribute("element", "tns:Airlines_BCBC.DeleteSvcResponse");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("portType");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBCSoapPort");
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "LoadKeySvc");
            GXSoapXMLWriter.writeElement("input", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.LoadKeySvcSoapIn");
            GXSoapXMLWriter.writeElement("output", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.LoadKeySvcSoapOut");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "SaveSvc");
            GXSoapXMLWriter.writeElement("input", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.SaveSvcSoapIn");
            GXSoapXMLWriter.writeElement("output", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.SaveSvcSoapOut");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "CheckSvc");
            GXSoapXMLWriter.writeElement("input", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.CheckSvcSoapIn");
            GXSoapXMLWriter.writeElement("output", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.CheckSvcSoapOut");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "DeleteSvc");
            GXSoapXMLWriter.writeElement("input", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.DeleteSvcSoapIn");
            GXSoapXMLWriter.writeElement("output", "");
            GXSoapXMLWriter.writeAttribute("message", "wsdlns:"+"Airlines_BCBC.DeleteSvcSoapOut");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("binding");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBCSoapBinding");
            GXSoapXMLWriter.writeAttribute("type", "wsdlns:"+"Airlines_BCBCSoapPort");
            GXSoapXMLWriter.writeElement("soap:binding", "");
            GXSoapXMLWriter.writeAttribute("style", "document");
            GXSoapXMLWriter.writeAttribute("transport", "http://schemas.xmlsoap.org/soap/http");
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "LoadKeySvc");
            GXSoapXMLWriter.writeElement("soap:operation", "");
            GXSoapXMLWriter.writeAttribute("soapAction", "IataICSIaction/"+"TAIRLINES_BC.LoadKeySvc");
            GXSoapXMLWriter.writeStartElement("input");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("output");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "SaveSvc");
            GXSoapXMLWriter.writeElement("soap:operation", "");
            GXSoapXMLWriter.writeAttribute("soapAction", "IataICSIaction/"+"TAIRLINES_BC.SaveSvc");
            GXSoapXMLWriter.writeStartElement("input");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("output");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "CheckSvc");
            GXSoapXMLWriter.writeElement("soap:operation", "");
            GXSoapXMLWriter.writeAttribute("soapAction", "IataICSIaction/"+"TAIRLINES_BC.CheckSvc");
            GXSoapXMLWriter.writeStartElement("input");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("output");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("operation");
            GXSoapXMLWriter.writeAttribute("name", "DeleteSvc");
            GXSoapXMLWriter.writeElement("soap:operation", "");
            GXSoapXMLWriter.writeAttribute("soapAction", "IataICSIaction/"+"TAIRLINES_BC.DeleteSvc");
            GXSoapXMLWriter.writeStartElement("input");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("output");
            GXSoapXMLWriter.writeElement("soap:body", "");
            GXSoapXMLWriter.writeAttribute("use", "literal");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeStartElement("service");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBC");
            GXSoapXMLWriter.writeStartElement("port");
            GXSoapXMLWriter.writeAttribute("name", "Airlines_BCBCSoapPort");
            GXSoapXMLWriter.writeAttribute("binding", "wsdlns:"+"Airlines_BCBCSoapBinding");
            GXSoapXMLWriter.writeElement("soap:address", "");
            GXSoapXMLWriter.writeAttribute("location", ((httpContext.getHttpSecure( )==1) ? "https://" : "http://")+httpContext.getServerName( )+((httpContext.getServerPort( )>0)&&(httpContext.getServerPort( )!=80)&&(httpContext.getServerPort( )!=443) ? ":"+GXutil.ltrim( GXutil.str( httpContext.getServerPort( ), 6, 0)) : "")+httpContext.getScriptPath( )+"tairlines_bc_ws");
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.writeEndElement();
            GXSoapXMLWriter.close();
            return  ;
         }
         else
         {
            currSoapErr = (short)(-20000) ;
            currSoapErrmsg = "No SOAP request found. Call " + ((httpContext.getHttpSecure( )==1) ? "https://" : "http://") + httpContext.getServerName( ) + ((httpContext.getServerPort( )>0)&&(httpContext.getServerPort( )!=80)&&(httpContext.getServerPort( )!=443) ? ":"+GXutil.ltrim( GXutil.str( httpContext.getServerPort( ), 6, 0)) : "") + httpContext.getScriptPath( ) + "tairlines_bc_ws" + "?wsdl to get the WSDL." ;
         }
      }
      if ( ( currSoapErr == 0 ) )
      {
         GXSoapXMLReader.openRequest(GXSoapHTTPRequest);
         GXSoapError = GXSoapXMLReader.read() ;
         while ( ( GXSoapError > 0 ) )
         {
            if ( ( GXutil.strSearch( GXSoapXMLReader.getName(), "Body", 1) > 0 ) )
            {
               if (true) break;
            }
            GXSoapError = GXSoapXMLReader.read() ;
         }
         if ( ( GXSoapError > 0 ) )
         {
            GXSoapError = GXSoapXMLReader.read() ;
            if ( ( GXSoapError > 0 ) )
            {
               currMethod = GXSoapXMLReader.getName() ;
               if ( ( GXutil.strSearch( currMethod, "LoadKeySvc", 1) > 0 ) )
               {
                  if ( ( currSoapErr == 0 ) )
                  {
                     bcAirlines = new SdtAirlines(remoteHandle);
                     bcAirlinesMessages = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus");
                     sTagName = GXSoapXMLReader.getName() ;
                     GXSoapError = GXSoapXMLReader.read() ;
                     nOutParmCount = (short)(0) ;
                     while ( ( ( GXutil.strcmp(GXSoapXMLReader.getName(), sTagName) != 0 ) || ( GXSoapXMLReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
                     {
                        readOk = (short)(0) ;
                        if ( ( GXutil.strcmp(GXSoapXMLReader.getLocalName(), "Isocod") == 0 ) && ( ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "") == 0 ) ) )
                        {
                           A23ISOCod = GXSoapXMLReader.getValue() ;
                           if ( ( GXSoapError > 0 ) )
                           {
                              readOk = (short)(1) ;
                           }
                        }
                        if ( ( GXutil.strcmp(GXSoapXMLReader.getLocalName(), "Airlinecode") == 0 ) && ( ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "") == 0 ) ) )
                        {
                           A1136AirLi = GXSoapXMLReader.getValue() ;
                           if ( ( GXSoapError > 0 ) )
                           {
                              readOk = (short)(1) ;
                           }
                        }
                        if ( ( GXutil.strcmp(GXSoapXMLReader.getLocalName(), "bcAirlines") == 0 ) && ( ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "") == 0 ) ) )
                        {
                           if ( ( GXSoapXMLReader.getIsSimple() == 0 ) )
                           {
                              GXSoapError = bcAirlines.readxml(GXSoapXMLReader, "bcAirlines") ;
                           }
                           if ( ( GXSoapError > 0 ) )
                           {
                              readOk = (short)(1) ;
                           }
                        }
                        GXSoapError = GXSoapXMLReader.read() ;
                        nOutParmCount = (short)(nOutParmCount+1) ;
                        if ( ( readOk == 0 ) )
                        {
                           context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
                           GXSoapError = (short)(nOutParmCount*-1) ;
                        }
                     }
                  }
               }
               else if ( ( GXutil.strSearch( currMethod, "SaveSvc", 1) > 0 ) )
               {
                  if ( ( currSoapErr == 0 ) )
                  {
                     bcAirlines = new SdtAirlines(remoteHandle);
                     bcAirlinesMessages = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus");
                     sTagName = GXSoapXMLReader.getName() ;
                     GXSoapError = GXSoapXMLReader.read() ;
                     nOutParmCount = (short)(0) ;
                     while ( ( ( GXutil.strcmp(GXSoapXMLReader.getName(), sTagName) != 0 ) || ( GXSoapXMLReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
                     {
                        readOk = (short)(0) ;
                        if ( ( GXutil.strcmp(GXSoapXMLReader.getLocalName(), "bcAirlines") == 0 ) && ( ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "") == 0 ) ) )
                        {
                           if ( ( GXSoapXMLReader.getIsSimple() == 0 ) )
                           {
                              GXSoapError = bcAirlines.readxml(GXSoapXMLReader, "bcAirlines") ;
                           }
                           if ( ( GXSoapError > 0 ) )
                           {
                              readOk = (short)(1) ;
                           }
                        }
                        GXSoapError = GXSoapXMLReader.read() ;
                        nOutParmCount = (short)(nOutParmCount+1) ;
                        if ( ( readOk == 0 ) )
                        {
                           context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
                           GXSoapError = (short)(nOutParmCount*-1) ;
                        }
                     }
                  }
               }
               else if ( ( GXutil.strSearch( currMethod, "CheckSvc", 1) > 0 ) )
               {
                  if ( ( currSoapErr == 0 ) )
                  {
                     bcAirlines = new SdtAirlines(remoteHandle);
                     bcAirlinesMessages = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus");
                     sTagName = GXSoapXMLReader.getName() ;
                     GXSoapError = GXSoapXMLReader.read() ;
                     nOutParmCount = (short)(0) ;
                     while ( ( ( GXutil.strcmp(GXSoapXMLReader.getName(), sTagName) != 0 ) || ( GXSoapXMLReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
                     {
                        readOk = (short)(0) ;
                        if ( ( GXutil.strcmp(GXSoapXMLReader.getLocalName(), "bcAirlines") == 0 ) && ( ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "") == 0 ) ) )
                        {
                           if ( ( GXSoapXMLReader.getIsSimple() == 0 ) )
                           {
                              GXSoapError = bcAirlines.readxml(GXSoapXMLReader, "bcAirlines") ;
                           }
                           if ( ( GXSoapError > 0 ) )
                           {
                              readOk = (short)(1) ;
                           }
                        }
                        GXSoapError = GXSoapXMLReader.read() ;
                        nOutParmCount = (short)(nOutParmCount+1) ;
                        if ( ( readOk == 0 ) )
                        {
                           context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
                           GXSoapError = (short)(nOutParmCount*-1) ;
                        }
                     }
                  }
               }
               else if ( ( GXutil.strSearch( currMethod, "DeleteSvc", 1) > 0 ) )
               {
                  if ( ( currSoapErr == 0 ) )
                  {
                     bcAirlines = new SdtAirlines(remoteHandle);
                     bcAirlinesMessages = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus");
                     sTagName = GXSoapXMLReader.getName() ;
                     GXSoapError = GXSoapXMLReader.read() ;
                     nOutParmCount = (short)(0) ;
                     while ( ( ( GXutil.strcmp(GXSoapXMLReader.getName(), sTagName) != 0 ) || ( GXSoapXMLReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
                     {
                        readOk = (short)(0) ;
                        if ( ( GXutil.strcmp(GXSoapXMLReader.getLocalName(), "bcAirlines") == 0 ) && ( ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(GXSoapXMLReader.getNamespaceURI(), "") == 0 ) ) )
                        {
                           if ( ( GXSoapXMLReader.getIsSimple() == 0 ) )
                           {
                              GXSoapError = bcAirlines.readxml(GXSoapXMLReader, "bcAirlines") ;
                           }
                           if ( ( GXSoapError > 0 ) )
                           {
                              readOk = (short)(1) ;
                           }
                        }
                        GXSoapError = GXSoapXMLReader.read() ;
                        nOutParmCount = (short)(nOutParmCount+1) ;
                        if ( ( readOk == 0 ) )
                        {
                           context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
                           GXSoapError = (short)(nOutParmCount*-1) ;
                        }
                     }
                  }
               }
               else
               {
                  currSoapErr = (short)(-20002) ;
                  currSoapErrmsg = "Wrong method called. Expected method: " + "LoadKeySvc,SaveSvc,CheckSvc,DeleteSvc" ;
               }
            }
         }
         GXSoapXMLReader.close();
      }
      if ( ( currSoapErr == 0 ) )
      {
         if ( ( GXSoapError < 0 ) )
         {
            currSoapErr = (short)(GXSoapError*-1) ;
            currSoapErrmsg = context.globals.sSOAPErrMsg ;
         }
         else
         {
            if ( ( GXSoapXMLReader.getErrCode() > 0 ) )
            {
               currSoapErr = (short)(GXSoapXMLReader.getErrCode()*-1) ;
               currSoapErrmsg = GXSoapXMLReader.getErrDescription() ;
            }
            else
            {
               if ( ( GXSoapError == 0 ) )
               {
                  currSoapErr = (short)(-20001) ;
                  currSoapErrmsg = "Malformed SOAP message." ;
               }
               else
               {
                  currSoapErr = (short)(0) ;
                  currSoapErrmsg = "No error." ;
               }
            }
         }
      }
      if ( ( currSoapErr == 0 ) )
      {
         if ( ( GXutil.strSearch( currMethod, "LoadKeySvc", 1) > 0 ) )
         {
            bcAirlines.Load(A23ISOCod, A1136AirLi);
            bcAirlinesMessages = bcAirlines.GetMessages() ;
         }
         else if ( ( GXutil.strSearch( currMethod, "SaveSvc", 1) > 0 ) )
         {
            bcAirlines.Save();
            bcAirlinesMessages = bcAirlines.GetMessages() ;
         }
         else if ( ( GXutil.strSearch( currMethod, "CheckSvc", 1) > 0 ) )
         {
            bcAirlines.Check();
            bcAirlinesMessages = bcAirlines.GetMessages() ;
         }
         else if ( ( GXutil.strSearch( currMethod, "DeleteSvc", 1) > 0 ) )
         {
            bcAirlines.Delete();
            bcAirlinesMessages = bcAirlines.GetMessages() ;
         }
      }
      GXSoapXMLWriter.openResponse(GXSoapHTTPResponse);
      GXSoapXMLWriter.writeStartDocument("utf-8", (byte)(0));
      GXSoapXMLWriter.writeStartElement("SOAP-ENV:Envelope");
      GXSoapXMLWriter.writeAttribute("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
      GXSoapXMLWriter.writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
      GXSoapXMLWriter.writeAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
      GXSoapXMLWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
      if ( ( GXutil.strSearch( currMethod, "LoadKeySvc", 1) > 0 ) )
      {
         GXSoapXMLWriter.writeStartElement("SOAP-ENV:Body");
         GXSoapXMLWriter.writeStartElement("Airlines_BCBC.LoadKeySvcResponse");
         GXSoapXMLWriter.writeAttribute("xmlns", "IataICSI");
         if ( ( currSoapErr == 0 ) )
         {
            bcAirlines.writexml(GXSoapXMLWriter, "bcAirlines", "IataICSI");
            bcAirlinesMessages.writexml(GXSoapXMLWriter, "bcAirlinesMessages", "IataICSI");
         }
         else
         {
            GXSoapXMLWriter.writeStartElement("SOAP-ENV:Fault");
            GXSoapXMLWriter.writeElement("faultcode", "SOAP-ENV:Client");
            GXSoapXMLWriter.writeElement("faultstring", currSoapErrmsg);
            GXSoapXMLWriter.writeElement("detail", GXutil.trim( GXutil.str( currSoapErr, 10, 0)));
            GXSoapXMLWriter.writeEndElement();
         }
         GXSoapXMLWriter.writeEndElement();
         GXSoapXMLWriter.writeEndElement();
      }
      else if ( ( GXutil.strSearch( currMethod, "SaveSvc", 1) > 0 ) )
      {
         GXSoapXMLWriter.writeStartElement("SOAP-ENV:Body");
         GXSoapXMLWriter.writeStartElement("Airlines_BCBC.SaveSvcResponse");
         GXSoapXMLWriter.writeAttribute("xmlns", "IataICSI");
         if ( ( currSoapErr == 0 ) )
         {
            bcAirlines.writexml(GXSoapXMLWriter, "bcAirlines", "IataICSI");
            bcAirlinesMessages.writexml(GXSoapXMLWriter, "bcAirlinesMessages", "IataICSI");
         }
         else
         {
            GXSoapXMLWriter.writeStartElement("SOAP-ENV:Fault");
            GXSoapXMLWriter.writeElement("faultcode", "SOAP-ENV:Client");
            GXSoapXMLWriter.writeElement("faultstring", currSoapErrmsg);
            GXSoapXMLWriter.writeElement("detail", GXutil.trim( GXutil.str( currSoapErr, 10, 0)));
            GXSoapXMLWriter.writeEndElement();
         }
         GXSoapXMLWriter.writeEndElement();
         GXSoapXMLWriter.writeEndElement();
      }
      else if ( ( GXutil.strSearch( currMethod, "CheckSvc", 1) > 0 ) )
      {
         GXSoapXMLWriter.writeStartElement("SOAP-ENV:Body");
         GXSoapXMLWriter.writeStartElement("Airlines_BCBC.CheckSvcResponse");
         GXSoapXMLWriter.writeAttribute("xmlns", "IataICSI");
         if ( ( currSoapErr == 0 ) )
         {
            bcAirlines.writexml(GXSoapXMLWriter, "bcAirlines", "IataICSI");
            bcAirlinesMessages.writexml(GXSoapXMLWriter, "bcAirlinesMessages", "IataICSI");
         }
         else
         {
            GXSoapXMLWriter.writeStartElement("SOAP-ENV:Fault");
            GXSoapXMLWriter.writeElement("faultcode", "SOAP-ENV:Client");
            GXSoapXMLWriter.writeElement("faultstring", currSoapErrmsg);
            GXSoapXMLWriter.writeElement("detail", GXutil.trim( GXutil.str( currSoapErr, 10, 0)));
            GXSoapXMLWriter.writeEndElement();
         }
         GXSoapXMLWriter.writeEndElement();
         GXSoapXMLWriter.writeEndElement();
      }
      else if ( ( GXutil.strSearch( currMethod, "DeleteSvc", 1) > 0 ) )
      {
         GXSoapXMLWriter.writeStartElement("SOAP-ENV:Body");
         GXSoapXMLWriter.writeStartElement("Airlines_BCBC.DeleteSvcResponse");
         GXSoapXMLWriter.writeAttribute("xmlns", "IataICSI");
         if ( ( currSoapErr == 0 ) )
         {
            bcAirlines.writexml(GXSoapXMLWriter, "bcAirlines", "IataICSI");
            bcAirlinesMessages.writexml(GXSoapXMLWriter, "bcAirlinesMessages", "IataICSI");
         }
         else
         {
            GXSoapXMLWriter.writeStartElement("SOAP-ENV:Fault");
            GXSoapXMLWriter.writeElement("faultcode", "SOAP-ENV:Client");
            GXSoapXMLWriter.writeElement("faultstring", currSoapErrmsg);
            GXSoapXMLWriter.writeElement("detail", GXutil.trim( GXutil.str( currSoapErr, 10, 0)));
            GXSoapXMLWriter.writeEndElement();
         }
         GXSoapXMLWriter.writeEndElement();
         GXSoapXMLWriter.writeEndElement();
      }
      GXSoapXMLWriter.writeEndElement();
      GXSoapXMLWriter.close();
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(26);
      pr_default.close(13);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z23ISOCod = "" ;
      A23ISOCod = "" ;
      Z1136AirLi = "" ;
      A1136AirLi = "" ;
      sMode226 = "" ;
      nIsMod_228 = (short)(0) ;
      RcdFound228 = (short)(0) ;
      nIsMod_227 = (short)(0) ;
      RcdFound227 = (short)(0) ;
      gxTv_SdtAirlines_Isocod_Z = "" ;
      gxTv_SdtAirlines_Airlinecode_Z = "" ;
      gxTv_SdtAirlines_Airlinedescription_Z = "" ;
      gxTv_SdtAirlines_Isodes_Z = "" ;
      gxTv_SdtAirlines_Airlineabbreviation_Z = "" ;
      gxTv_SdtAirlines_Airlineaddress_Z = "" ;
      gxTv_SdtAirlines_Airlineaddresscompl_Z = "" ;
      gxTv_SdtAirlines_Airlinecity_Z = "" ;
      gxTv_SdtAirlines_Airlinestate_Z = "" ;
      gxTv_SdtAirlines_Airlinezip_Z = "" ;
      gxTv_SdtAirlines_Airlinedatins_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtAirlines_Airlinedatmod_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtAirlines_Airlinecompleteaddress_Z = "" ;
      gxTv_SdtAirlines_Airlinetypesettlement_Z = "" ;
      gxTv_SdtAirlines_Airlinesta_Z = "" ;
      gxTv_SdtAirlines_Airlinebanktransfermode_Z = "" ;
      gxTv_SdtAirlines_Isodes_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinecompleteaddress_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinesta_N = (byte)(0) ;
      gxTv_SdtAirlines_Airlinebanktransfermode_N = (byte)(0) ;
      gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = "" ;
      gxTv_SdtAirlines_Contacts_Contacttypescode_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Airlinecttname_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = "" ;
      gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = "" ;
      GX_JID = 0 ;
      Z1126AirLi = GXutil.resetTime( GXutil.nullDate() );
      A1126AirLi = GXutil.resetTime( GXutil.nullDate() );
      Z1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      A1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      Z1130Airli = "" ;
      A1130Airli = "" ;
      Z1118AirLi = "" ;
      A1118AirLi = "" ;
      Z1119AirLi = "" ;
      A1119AirLi = "" ;
      Z1120Airli = "" ;
      A1120Airli = "" ;
      Z1121AirLi = "" ;
      A1121AirLi = "" ;
      Z1122AirLi = "" ;
      A1122AirLi = "" ;
      Z1123AirLi = "" ;
      A1123AirLi = "" ;
      Z1124AirLi = "" ;
      A1124AirLi = "" ;
      Z1127AirLi = "" ;
      A1127AirLi = "" ;
      Z1128AirLi = "" ;
      A1128AirLi = "" ;
      Z1129AirLi = "" ;
      A1129AirLi = "" ;
      Z1137AirLi = "" ;
      A1137AirLi = "" ;
      Z1131AirLI = "" ;
      A1131AirLI = "" ;
      Z1132AirLi = "" ;
      A1132AirLi = "" ;
      Z365Contac = "" ;
      A365Contac = "" ;
      Z1138AirLi = (short)(0) ;
      A1138AirLi = (short)(0) ;
      Z1133AirLi = "" ;
      A1133AirLi = "" ;
      Z1134AirLi = "" ;
      A1134AirLi = "" ;
      Z1135AirLi = "" ;
      A1135AirLi = "" ;
      Z366Contac = "" ;
      A366Contac = "" ;
      Z20ISODes = "" ;
      A20ISODes = "" ;
      Gx_BScreen = (byte)(0) ;
      n1130Airli = false ;
      BC002B10_A1136AirLi = new String[] {""} ;
      BC002B10_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B10_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B10_A1130Airli = new String[] {""} ;
      BC002B10_n1130Airli = new boolean[] {false} ;
      BC002B10_A1118AirLi = new String[] {""} ;
      BC002B10_A20ISODes = new String[] {""} ;
      BC002B10_n20ISODes = new boolean[] {false} ;
      BC002B10_A1119AirLi = new String[] {""} ;
      BC002B10_A1120Airli = new String[] {""} ;
      BC002B10_A1121AirLi = new String[] {""} ;
      BC002B10_A1122AirLi = new String[] {""} ;
      BC002B10_A1123AirLi = new String[] {""} ;
      BC002B10_A1124AirLi = new String[] {""} ;
      BC002B10_A1127AirLi = new String[] {""} ;
      BC002B10_n1127AirLi = new boolean[] {false} ;
      BC002B10_A1128AirLi = new String[] {""} ;
      BC002B10_A1129AirLi = new String[] {""} ;
      BC002B10_n1129AirLi = new boolean[] {false} ;
      BC002B10_A23ISOCod = new String[] {""} ;
      RcdFound226 = (short)(0) ;
      n20ISODes = false ;
      n1127AirLi = false ;
      n1129AirLi = false ;
      BC002B9_A20ISODes = new String[] {""} ;
      BC002B9_n20ISODes = new boolean[] {false} ;
      BC002B11_A23ISOCod = new String[] {""} ;
      BC002B11_A1136AirLi = new String[] {""} ;
      BC002B8_A1136AirLi = new String[] {""} ;
      BC002B8_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B8_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B8_A1130Airli = new String[] {""} ;
      BC002B8_n1130Airli = new boolean[] {false} ;
      BC002B8_A1118AirLi = new String[] {""} ;
      BC002B8_A1119AirLi = new String[] {""} ;
      BC002B8_A1120Airli = new String[] {""} ;
      BC002B8_A1121AirLi = new String[] {""} ;
      BC002B8_A1122AirLi = new String[] {""} ;
      BC002B8_A1123AirLi = new String[] {""} ;
      BC002B8_A1124AirLi = new String[] {""} ;
      BC002B8_A1127AirLi = new String[] {""} ;
      BC002B8_n1127AirLi = new boolean[] {false} ;
      BC002B8_A1128AirLi = new String[] {""} ;
      BC002B8_A1129AirLi = new String[] {""} ;
      BC002B8_n1129AirLi = new boolean[] {false} ;
      BC002B8_A23ISOCod = new String[] {""} ;
      BC002B7_A1136AirLi = new String[] {""} ;
      BC002B7_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B7_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B7_A1130Airli = new String[] {""} ;
      BC002B7_n1130Airli = new boolean[] {false} ;
      BC002B7_A1118AirLi = new String[] {""} ;
      BC002B7_A1119AirLi = new String[] {""} ;
      BC002B7_A1120Airli = new String[] {""} ;
      BC002B7_A1121AirLi = new String[] {""} ;
      BC002B7_A1122AirLi = new String[] {""} ;
      BC002B7_A1123AirLi = new String[] {""} ;
      BC002B7_A1124AirLi = new String[] {""} ;
      BC002B7_A1127AirLi = new String[] {""} ;
      BC002B7_n1127AirLi = new boolean[] {false} ;
      BC002B7_A1128AirLi = new String[] {""} ;
      BC002B7_A1129AirLi = new String[] {""} ;
      BC002B7_n1129AirLi = new boolean[] {false} ;
      BC002B7_A23ISOCod = new String[] {""} ;
      Gx_longc = false ;
      BC002B15_A20ISODes = new String[] {""} ;
      BC002B15_n20ISODes = new boolean[] {false} ;
      nRcdExists_227 = (short)(0) ;
      Gxremove227 = (byte)(0) ;
      nRcdExists_228 = (short)(0) ;
      Gxremove228 = (byte)(0) ;
      BC002B16_A1136AirLi = new String[] {""} ;
      BC002B16_A1126AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B16_A1125AirLi = new java.util.Date[] {GXutil.nullDate()} ;
      BC002B16_A1130Airli = new String[] {""} ;
      BC002B16_n1130Airli = new boolean[] {false} ;
      BC002B16_A1118AirLi = new String[] {""} ;
      BC002B16_A20ISODes = new String[] {""} ;
      BC002B16_n20ISODes = new boolean[] {false} ;
      BC002B16_A1119AirLi = new String[] {""} ;
      BC002B16_A1120Airli = new String[] {""} ;
      BC002B16_A1121AirLi = new String[] {""} ;
      BC002B16_A1122AirLi = new String[] {""} ;
      BC002B16_A1123AirLi = new String[] {""} ;
      BC002B16_A1124AirLi = new String[] {""} ;
      BC002B16_A1127AirLi = new String[] {""} ;
      BC002B16_n1127AirLi = new boolean[] {false} ;
      BC002B16_A1128AirLi = new String[] {""} ;
      BC002B16_A1129AirLi = new String[] {""} ;
      BC002B16_n1129AirLi = new boolean[] {false} ;
      BC002B16_A23ISOCod = new String[] {""} ;
      BC002B17_A23ISOCod = new String[] {""} ;
      BC002B17_A1136AirLi = new String[] {""} ;
      BC002B17_A1137AirLi = new String[] {""} ;
      BC002B17_A1131AirLI = new String[] {""} ;
      BC002B17_A1132AirLi = new String[] {""} ;
      BC002B18_A23ISOCod = new String[] {""} ;
      BC002B18_A1136AirLi = new String[] {""} ;
      BC002B18_A1137AirLi = new String[] {""} ;
      BC002B6_A23ISOCod = new String[] {""} ;
      BC002B6_A1136AirLi = new String[] {""} ;
      BC002B6_A1137AirLi = new String[] {""} ;
      BC002B6_A1131AirLI = new String[] {""} ;
      BC002B6_A1132AirLi = new String[] {""} ;
      sMode227 = "" ;
      BC002B5_A23ISOCod = new String[] {""} ;
      BC002B5_A1136AirLi = new String[] {""} ;
      BC002B5_A1137AirLi = new String[] {""} ;
      BC002B5_A1131AirLI = new String[] {""} ;
      BC002B5_A1132AirLi = new String[] {""} ;
      BC002B22_A23ISOCod = new String[] {""} ;
      BC002B22_A1136AirLi = new String[] {""} ;
      BC002B22_A1137AirLi = new String[] {""} ;
      BC002B22_A1131AirLI = new String[] {""} ;
      BC002B22_A1132AirLi = new String[] {""} ;
      AV26AlterB = (byte)(0) ;
      BC002B23_A23ISOCod = new String[] {""} ;
      BC002B23_A1136AirLi = new String[] {""} ;
      BC002B23_A1138AirLi = new short[1] ;
      BC002B23_A1133AirLi = new String[] {""} ;
      BC002B23_A1134AirLi = new String[] {""} ;
      BC002B23_A1135AirLi = new String[] {""} ;
      BC002B23_A366Contac = new String[] {""} ;
      BC002B23_A365Contac = new String[] {""} ;
      BC002B4_A366Contac = new String[] {""} ;
      BC002B24_A23ISOCod = new String[] {""} ;
      BC002B24_A1136AirLi = new String[] {""} ;
      BC002B24_A365Contac = new String[] {""} ;
      BC002B24_A1138AirLi = new short[1] ;
      BC002B3_A23ISOCod = new String[] {""} ;
      BC002B3_A1136AirLi = new String[] {""} ;
      BC002B3_A1138AirLi = new short[1] ;
      BC002B3_A1133AirLi = new String[] {""} ;
      BC002B3_A1134AirLi = new String[] {""} ;
      BC002B3_A1135AirLi = new String[] {""} ;
      BC002B3_A365Contac = new String[] {""} ;
      sMode228 = "" ;
      BC002B2_A23ISOCod = new String[] {""} ;
      BC002B2_A1136AirLi = new String[] {""} ;
      BC002B2_A1138AirLi = new short[1] ;
      BC002B2_A1133AirLi = new String[] {""} ;
      BC002B2_A1134AirLi = new String[] {""} ;
      BC002B2_A1135AirLi = new String[] {""} ;
      BC002B2_A365Contac = new String[] {""} ;
      BC002B28_A366Contac = new String[] {""} ;
      BC002B29_A23ISOCod = new String[] {""} ;
      BC002B29_A1136AirLi = new String[] {""} ;
      BC002B29_A1138AirLi = new short[1] ;
      BC002B29_A1133AirLi = new String[] {""} ;
      BC002B29_A1134AirLi = new String[] {""} ;
      BC002B29_A1135AirLi = new String[] {""} ;
      BC002B29_A366Contac = new String[] {""} ;
      BC002B29_A365Contac = new String[] {""} ;
      GXt_int1 = (short)(0) ;
      GXv_int2 = new short [1] ;
      i1125AirLi = GXutil.resetTime( GXutil.nullDate() );
      i1128AirLi = "" ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      currSoapErr = (short)(0) ;
      currSoapErrmsg = "" ;
      currMethod = "" ;
      GXt_char3 = "" ;
      GXt_char4 = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tairlines_bc__default(),
         new Object[] {
             new Object[] {
            BC002B2_A23ISOCod, BC002B2_A1136AirLi, BC002B2_A1138AirLi, BC002B2_A1133AirLi, BC002B2_A1134AirLi, BC002B2_A1135AirLi, BC002B2_A365Contac
            }
            , new Object[] {
            BC002B3_A23ISOCod, BC002B3_A1136AirLi, BC002B3_A1138AirLi, BC002B3_A1133AirLi, BC002B3_A1134AirLi, BC002B3_A1135AirLi, BC002B3_A365Contac
            }
            , new Object[] {
            BC002B4_A366Contac
            }
            , new Object[] {
            BC002B5_A23ISOCod, BC002B5_A1136AirLi, BC002B5_A1137AirLi, BC002B5_A1131AirLI, BC002B5_A1132AirLi
            }
            , new Object[] {
            BC002B6_A23ISOCod, BC002B6_A1136AirLi, BC002B6_A1137AirLi, BC002B6_A1131AirLI, BC002B6_A1132AirLi
            }
            , new Object[] {
            BC002B7_A1136AirLi, BC002B7_A1126AirLi, BC002B7_A1125AirLi, BC002B7_A1130Airli, BC002B7_n1130Airli, BC002B7_A1118AirLi, BC002B7_A1119AirLi, BC002B7_A1120Airli, BC002B7_A1121AirLi, BC002B7_A1122AirLi,
            BC002B7_A1123AirLi, BC002B7_A1124AirLi, BC002B7_A1127AirLi, BC002B7_n1127AirLi, BC002B7_A1128AirLi, BC002B7_A1129AirLi, BC002B7_n1129AirLi, BC002B7_A23ISOCod
            }
            , new Object[] {
            BC002B8_A1136AirLi, BC002B8_A1126AirLi, BC002B8_A1125AirLi, BC002B8_A1130Airli, BC002B8_n1130Airli, BC002B8_A1118AirLi, BC002B8_A1119AirLi, BC002B8_A1120Airli, BC002B8_A1121AirLi, BC002B8_A1122AirLi,
            BC002B8_A1123AirLi, BC002B8_A1124AirLi, BC002B8_A1127AirLi, BC002B8_n1127AirLi, BC002B8_A1128AirLi, BC002B8_A1129AirLi, BC002B8_n1129AirLi, BC002B8_A23ISOCod
            }
            , new Object[] {
            BC002B9_A20ISODes, BC002B9_n20ISODes
            }
            , new Object[] {
            BC002B10_A1136AirLi, BC002B10_A1126AirLi, BC002B10_A1125AirLi, BC002B10_A1130Airli, BC002B10_n1130Airli, BC002B10_A1118AirLi, BC002B10_A20ISODes, BC002B10_n20ISODes, BC002B10_A1119AirLi, BC002B10_A1120Airli,
            BC002B10_A1121AirLi, BC002B10_A1122AirLi, BC002B10_A1123AirLi, BC002B10_A1124AirLi, BC002B10_A1127AirLi, BC002B10_n1127AirLi, BC002B10_A1128AirLi, BC002B10_A1129AirLi, BC002B10_n1129AirLi, BC002B10_A23ISOCod
            }
            , new Object[] {
            BC002B11_A23ISOCod, BC002B11_A1136AirLi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002B15_A20ISODes, BC002B15_n20ISODes
            }
            , new Object[] {
            BC002B16_A1136AirLi, BC002B16_A1126AirLi, BC002B16_A1125AirLi, BC002B16_A1130Airli, BC002B16_n1130Airli, BC002B16_A1118AirLi, BC002B16_A20ISODes, BC002B16_n20ISODes, BC002B16_A1119AirLi, BC002B16_A1120Airli,
            BC002B16_A1121AirLi, BC002B16_A1122AirLi, BC002B16_A1123AirLi, BC002B16_A1124AirLi, BC002B16_A1127AirLi, BC002B16_n1127AirLi, BC002B16_A1128AirLi, BC002B16_A1129AirLi, BC002B16_n1129AirLi, BC002B16_A23ISOCod
            }
            , new Object[] {
            BC002B17_A23ISOCod, BC002B17_A1136AirLi, BC002B17_A1137AirLi, BC002B17_A1131AirLI, BC002B17_A1132AirLi
            }
            , new Object[] {
            BC002B18_A23ISOCod, BC002B18_A1136AirLi, BC002B18_A1137AirLi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002B22_A23ISOCod, BC002B22_A1136AirLi, BC002B22_A1137AirLi, BC002B22_A1131AirLI, BC002B22_A1132AirLi
            }
            , new Object[] {
            BC002B23_A23ISOCod, BC002B23_A1136AirLi, BC002B23_A1138AirLi, BC002B23_A1133AirLi, BC002B23_A1134AirLi, BC002B23_A1135AirLi, BC002B23_A366Contac, BC002B23_A365Contac
            }
            , new Object[] {
            BC002B24_A23ISOCod, BC002B24_A1136AirLi, BC002B24_A365Contac, BC002B24_A1138AirLi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002B28_A366Contac
            }
            , new Object[] {
            BC002B29_A23ISOCod, BC002B29_A1136AirLi, BC002B29_A1138AirLi, BC002B29_A1133AirLi, BC002B29_A1134AirLi, BC002B29_A1135AirLi, BC002B29_A366Contac, BC002B29_A365Contac
            }
         }
      );
      Z1128AirLi = "S" ;
      /* Execute Start event if defined. */
      /* Execute user event: e112B2 */
      e112B2 ();
   }

   private byte nKeyPressed ;
   private byte gxTv_SdtAirlines_Isodes_N ;
   private byte gxTv_SdtAirlines_Airlinecompleteaddress_N ;
   private byte gxTv_SdtAirlines_Airlinesta_N ;
   private byte gxTv_SdtAirlines_Airlinebanktransfermode_N ;
   private byte Gx_BScreen ;
   private byte Gxremove227 ;
   private byte Gxremove228 ;
   private byte AV26AlterB ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short nGXsfl_228_idx=1 ;
   private short nIsMod_228 ;
   private short RcdFound228 ;
   private short nGXsfl_227_idx=1 ;
   private short nIsMod_227 ;
   private short RcdFound227 ;
   private short gxTv_SdtAirlines_Contacts_Airlinecttseq_Z ;
   private short Z1138AirLi ;
   private short A1138AirLi ;
   private short RcdFound226 ;
   private short nRcdExists_227 ;
   private short nRcdExists_228 ;
   private short GXt_int1 ;
   private short GXv_int2[] ;
   private short currSoapErr ;
   private short nOutParmCount ;
   private short readOk ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode226 ;
   private String gxTv_SdtAirlines_Airlinetypesettlement_Z ;
   private String gxTv_SdtAirlines_Airlinesta_Z ;
   private String gxTv_SdtAirlines_Airlinebanktransfermode_Z ;
   private String Z1130Airli ;
   private String A1130Airli ;
   private String Z1128AirLi ;
   private String A1128AirLi ;
   private String Z1129AirLi ;
   private String A1129AirLi ;
   private String sMode227 ;
   private String sMode228 ;
   private String i1128AirLi ;
   private String currSoapErrmsg ;
   private String currMethod ;
   private String GXt_char3 ;
   private String GXt_char4 ;
   private String sTagName ;
   private java.util.Date gxTv_SdtAirlines_Airlinedatins_Z ;
   private java.util.Date gxTv_SdtAirlines_Airlinedatmod_Z ;
   private java.util.Date Z1126AirLi ;
   private java.util.Date A1126AirLi ;
   private java.util.Date Z1125AirLi ;
   private java.util.Date A1125AirLi ;
   private java.util.Date i1125AirLi ;
   private boolean n1130Airli ;
   private boolean n20ISODes ;
   private boolean n1127AirLi ;
   private boolean n1129AirLi ;
   private boolean Gx_longc ;
   private String gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z ;
   private String Z1131AirLI ;
   private String A1131AirLI ;
   private String Z23ISOCod ;
   private String A23ISOCod ;
   private String Z1136AirLi ;
   private String A1136AirLi ;
   private String gxTv_SdtAirlines_Isocod_Z ;
   private String gxTv_SdtAirlines_Airlinecode_Z ;
   private String gxTv_SdtAirlines_Airlinedescription_Z ;
   private String gxTv_SdtAirlines_Isodes_Z ;
   private String gxTv_SdtAirlines_Airlineabbreviation_Z ;
   private String gxTv_SdtAirlines_Airlineaddress_Z ;
   private String gxTv_SdtAirlines_Airlineaddresscompl_Z ;
   private String gxTv_SdtAirlines_Airlinecity_Z ;
   private String gxTv_SdtAirlines_Airlinestate_Z ;
   private String gxTv_SdtAirlines_Airlinezip_Z ;
   private String gxTv_SdtAirlines_Airlinecompleteaddress_Z ;
   private String gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z ;
   private String gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z ;
   private String gxTv_SdtAirlines_Contacts_Contacttypescode_Z ;
   private String gxTv_SdtAirlines_Contacts_Airlinecttname_Z ;
   private String gxTv_SdtAirlines_Contacts_Airlinecttphone_Z ;
   private String gxTv_SdtAirlines_Contacts_Airlinecttemail_Z ;
   private String gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z ;
   private String Z1118AirLi ;
   private String A1118AirLi ;
   private String Z1119AirLi ;
   private String A1119AirLi ;
   private String Z1120Airli ;
   private String A1120Airli ;
   private String Z1121AirLi ;
   private String A1121AirLi ;
   private String Z1122AirLi ;
   private String A1122AirLi ;
   private String Z1123AirLi ;
   private String A1123AirLi ;
   private String Z1124AirLi ;
   private String A1124AirLi ;
   private String Z1127AirLi ;
   private String A1127AirLi ;
   private String Z1137AirLi ;
   private String A1137AirLi ;
   private String Z1132AirLi ;
   private String A1132AirLi ;
   private String Z365Contac ;
   private String A365Contac ;
   private String Z1133AirLi ;
   private String A1133AirLi ;
   private String Z1134AirLi ;
   private String A1134AirLi ;
   private String Z1135AirLi ;
   private String A1135AirLi ;
   private String Z366Contac ;
   private String A366Contac ;
   private String Z20ISODes ;
   private String A20ISODes ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private GxObjectCollection bcAirlinesMessages ;
   private SdtAirlines bcAirlines ;
   private IDataStoreProvider pr_default ;
   private String[] BC002B10_A1136AirLi ;
   private java.util.Date[] BC002B10_A1126AirLi ;
   private java.util.Date[] BC002B10_A1125AirLi ;
   private String[] BC002B10_A1130Airli ;
   private boolean[] BC002B10_n1130Airli ;
   private String[] BC002B10_A1118AirLi ;
   private String[] BC002B10_A20ISODes ;
   private boolean[] BC002B10_n20ISODes ;
   private String[] BC002B10_A1119AirLi ;
   private String[] BC002B10_A1120Airli ;
   private String[] BC002B10_A1121AirLi ;
   private String[] BC002B10_A1122AirLi ;
   private String[] BC002B10_A1123AirLi ;
   private String[] BC002B10_A1124AirLi ;
   private String[] BC002B10_A1127AirLi ;
   private boolean[] BC002B10_n1127AirLi ;
   private String[] BC002B10_A1128AirLi ;
   private String[] BC002B10_A1129AirLi ;
   private boolean[] BC002B10_n1129AirLi ;
   private String[] BC002B10_A23ISOCod ;
   private String[] BC002B9_A20ISODes ;
   private boolean[] BC002B9_n20ISODes ;
   private String[] BC002B11_A23ISOCod ;
   private String[] BC002B11_A1136AirLi ;
   private String[] BC002B8_A1136AirLi ;
   private java.util.Date[] BC002B8_A1126AirLi ;
   private java.util.Date[] BC002B8_A1125AirLi ;
   private String[] BC002B8_A1130Airli ;
   private boolean[] BC002B8_n1130Airli ;
   private String[] BC002B8_A1118AirLi ;
   private String[] BC002B8_A1119AirLi ;
   private String[] BC002B8_A1120Airli ;
   private String[] BC002B8_A1121AirLi ;
   private String[] BC002B8_A1122AirLi ;
   private String[] BC002B8_A1123AirLi ;
   private String[] BC002B8_A1124AirLi ;
   private String[] BC002B8_A1127AirLi ;
   private boolean[] BC002B8_n1127AirLi ;
   private String[] BC002B8_A1128AirLi ;
   private String[] BC002B8_A1129AirLi ;
   private boolean[] BC002B8_n1129AirLi ;
   private String[] BC002B8_A23ISOCod ;
   private String[] BC002B7_A1136AirLi ;
   private java.util.Date[] BC002B7_A1126AirLi ;
   private java.util.Date[] BC002B7_A1125AirLi ;
   private String[] BC002B7_A1130Airli ;
   private boolean[] BC002B7_n1130Airli ;
   private String[] BC002B7_A1118AirLi ;
   private String[] BC002B7_A1119AirLi ;
   private String[] BC002B7_A1120Airli ;
   private String[] BC002B7_A1121AirLi ;
   private String[] BC002B7_A1122AirLi ;
   private String[] BC002B7_A1123AirLi ;
   private String[] BC002B7_A1124AirLi ;
   private String[] BC002B7_A1127AirLi ;
   private boolean[] BC002B7_n1127AirLi ;
   private String[] BC002B7_A1128AirLi ;
   private String[] BC002B7_A1129AirLi ;
   private boolean[] BC002B7_n1129AirLi ;
   private String[] BC002B7_A23ISOCod ;
   private String[] BC002B15_A20ISODes ;
   private boolean[] BC002B15_n20ISODes ;
   private String[] BC002B16_A1136AirLi ;
   private java.util.Date[] BC002B16_A1126AirLi ;
   private java.util.Date[] BC002B16_A1125AirLi ;
   private String[] BC002B16_A1130Airli ;
   private boolean[] BC002B16_n1130Airli ;
   private String[] BC002B16_A1118AirLi ;
   private String[] BC002B16_A20ISODes ;
   private boolean[] BC002B16_n20ISODes ;
   private String[] BC002B16_A1119AirLi ;
   private String[] BC002B16_A1120Airli ;
   private String[] BC002B16_A1121AirLi ;
   private String[] BC002B16_A1122AirLi ;
   private String[] BC002B16_A1123AirLi ;
   private String[] BC002B16_A1124AirLi ;
   private String[] BC002B16_A1127AirLi ;
   private boolean[] BC002B16_n1127AirLi ;
   private String[] BC002B16_A1128AirLi ;
   private String[] BC002B16_A1129AirLi ;
   private boolean[] BC002B16_n1129AirLi ;
   private String[] BC002B16_A23ISOCod ;
   private String[] BC002B17_A23ISOCod ;
   private String[] BC002B17_A1136AirLi ;
   private String[] BC002B17_A1137AirLi ;
   private String[] BC002B17_A1131AirLI ;
   private String[] BC002B17_A1132AirLi ;
   private String[] BC002B18_A23ISOCod ;
   private String[] BC002B18_A1136AirLi ;
   private String[] BC002B18_A1137AirLi ;
   private String[] BC002B6_A23ISOCod ;
   private String[] BC002B6_A1136AirLi ;
   private String[] BC002B6_A1137AirLi ;
   private String[] BC002B6_A1131AirLI ;
   private String[] BC002B6_A1132AirLi ;
   private String[] BC002B5_A23ISOCod ;
   private String[] BC002B5_A1136AirLi ;
   private String[] BC002B5_A1137AirLi ;
   private String[] BC002B5_A1131AirLI ;
   private String[] BC002B5_A1132AirLi ;
   private String[] BC002B22_A23ISOCod ;
   private String[] BC002B22_A1136AirLi ;
   private String[] BC002B22_A1137AirLi ;
   private String[] BC002B22_A1131AirLI ;
   private String[] BC002B22_A1132AirLi ;
   private String[] BC002B23_A23ISOCod ;
   private String[] BC002B23_A1136AirLi ;
   private short[] BC002B23_A1138AirLi ;
   private String[] BC002B23_A1133AirLi ;
   private String[] BC002B23_A1134AirLi ;
   private String[] BC002B23_A1135AirLi ;
   private String[] BC002B23_A366Contac ;
   private String[] BC002B23_A365Contac ;
   private String[] BC002B4_A366Contac ;
   private String[] BC002B24_A23ISOCod ;
   private String[] BC002B24_A1136AirLi ;
   private String[] BC002B24_A365Contac ;
   private short[] BC002B24_A1138AirLi ;
   private String[] BC002B3_A23ISOCod ;
   private String[] BC002B3_A1136AirLi ;
   private short[] BC002B3_A1138AirLi ;
   private String[] BC002B3_A1133AirLi ;
   private String[] BC002B3_A1134AirLi ;
   private String[] BC002B3_A1135AirLi ;
   private String[] BC002B3_A365Contac ;
   private String[] BC002B2_A23ISOCod ;
   private String[] BC002B2_A1136AirLi ;
   private short[] BC002B2_A1138AirLi ;
   private String[] BC002B2_A1133AirLi ;
   private String[] BC002B2_A1134AirLi ;
   private String[] BC002B2_A1135AirLi ;
   private String[] BC002B2_A365Contac ;
   private String[] BC002B28_A366Contac ;
   private String[] BC002B29_A23ISOCod ;
   private String[] BC002B29_A1136AirLi ;
   private short[] BC002B29_A1138AirLi ;
   private String[] BC002B29_A1133AirLi ;
   private String[] BC002B29_A1134AirLi ;
   private String[] BC002B29_A1135AirLi ;
   private String[] BC002B29_A366Contac ;
   private String[] BC002B29_A365Contac ;
}

final  class tairlines_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC002B2", "SELECT [ISOCod], [AirLineCode], [AirLineCttSeq], [AirLineCttName], [AirLineCttPhone], [AirLineCttEmail], [ContactTypesCode] FROM [AIRLINESCONTACTS] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B3", "SELECT [ISOCod], [AirLineCode], [AirLineCttSeq], [AirLineCttName], [AirLineCttPhone], [AirLineCttEmail], [ContactTypesCode] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B4", "SELECT [ContactTypesDescription] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B5", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B6", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B7", "SELECT [AirLineCode], [AirLineDatMod], [AirLineDatIns], [AirlineBankTransferMode], [AirLineDescription], [AirLineAbbreviation], [AirlineAddress], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineZIP], [AirLineCompleteAddress], [AirLineTypeSettlement], [AirLineSta], [ISOCod] FROM [AIRLINES] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B8", "SELECT [AirLineCode], [AirLineDatMod], [AirLineDatIns], [AirlineBankTransferMode], [AirLineDescription], [AirLineAbbreviation], [AirlineAddress], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineZIP], [AirLineCompleteAddress], [AirLineTypeSettlement], [AirLineSta], [ISOCod] FROM [AIRLINES] WITH (NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B9", "SELECT [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B10", "SELECT TM1.[AirLineCode], TM1.[AirLineDatMod], TM1.[AirLineDatIns], TM1.[AirlineBankTransferMode], TM1.[AirLineDescription], T2.[ISODes], TM1.[AirLineAbbreviation], TM1.[AirlineAddress], TM1.[AirLineAddressCompl], TM1.[AirLineCity], TM1.[AirLineState], TM1.[AirLineZIP], TM1.[AirLineCompleteAddress], TM1.[AirLineTypeSettlement], TM1.[AirLineSta], TM1.[ISOCod] FROM ([AIRLINES] TM1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [COUNTRY] T2 WITH (NOLOCK) ON T2.[ISOCod] = TM1.[ISOCod]) WHERE TM1.[ISOCod] = ? and TM1.[AirLineCode] = ? ORDER BY TM1.[ISOCod], TM1.[AirLineCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B11", "SELECT [ISOCod], [AirLineCode] FROM [AIRLINES] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002B12", "INSERT INTO [AIRLINES] ([AirLineCode], [AirLineDatMod], [AirLineDatIns], [AirlineBankTransferMode], [AirLineDescription], [AirLineAbbreviation], [AirlineAddress], [AirLineAddressCompl], [AirLineCity], [AirLineState], [AirLineZIP], [AirLineCompleteAddress], [AirLineTypeSettlement], [AirLineSta], [ISOCod]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002B13", "UPDATE [AIRLINES] SET [AirLineDatMod]=?, [AirLineDatIns]=?, [AirlineBankTransferMode]=?, [AirLineDescription]=?, [AirLineAbbreviation]=?, [AirlineAddress]=?, [AirLineAddressCompl]=?, [AirLineCity]=?, [AirLineState]=?, [AirLineZIP]=?, [AirLineCompleteAddress]=?, [AirLineTypeSettlement]=?, [AirLineSta]=?  WHERE [ISOCod] = ? AND [AirLineCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002B14", "DELETE FROM [AIRLINES]  WHERE [ISOCod] = ? AND [AirLineCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002B15", "SELECT [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B16", "SELECT TM1.[AirLineCode], TM1.[AirLineDatMod], TM1.[AirLineDatIns], TM1.[AirlineBankTransferMode], TM1.[AirLineDescription], T2.[ISODes], TM1.[AirLineAbbreviation], TM1.[AirlineAddress], TM1.[AirLineAddressCompl], TM1.[AirLineCity], TM1.[AirLineState], TM1.[AirLineZIP], TM1.[AirLineCompleteAddress], TM1.[AirLineTypeSettlement], TM1.[AirLineSta], TM1.[ISOCod] FROM ([AIRLINES] TM1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [COUNTRY] T2 WITH (NOLOCK) ON T2.[ISOCod] = TM1.[ISOCod]) WHERE TM1.[ISOCod] = ? and TM1.[AirLineCode] = ? ORDER BY TM1.[ISOCod], TM1.[AirLineCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B17", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? and [AirLineCode] = ? and [AirLineCurCode] = ? ORDER BY [ISOCod], [AirLineCode], [AirLineCurCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B18", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode] FROM [AIRLINESBANK] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002B19", "INSERT INTO [AIRLINESBANK] ([ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002B20", "UPDATE [AIRLINESBANK] SET [AirLIneDataBank]=?, [AirLineCurTrans]=?  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002B21", "DELETE FROM [AIRLINESBANK]  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [AirLineCurCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002B22", "SELECT [ISOCod], [AirLineCode], [AirLineCurCode], [AirLIneDataBank], [AirLineCurTrans] FROM [AIRLINESBANK] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? and [AirLineCode] = ? ORDER BY [ISOCod], [AirLineCode], [AirLineCurCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B23", "SELECT T1.[ISOCod], T1.[AirLineCode], T1.[AirLineCttSeq], T1.[AirLineCttName], T1.[AirLineCttPhone], T1.[AirLineCttEmail], T2.[ContactTypesDescription], T1.[ContactTypesCode] FROM ([AIRLINESCONTACTS] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [CONTACTTYPES] T2 WITH (NOLOCK) ON T2.[ContactTypesCode] = T1.[ContactTypesCode]) WHERE T1.[ISOCod] = ? and T1.[AirLineCode] = ? and T1.[ContactTypesCode] = ? and T1.[AirLineCttSeq] = ? ORDER BY T1.[ISOCod], T1.[AirLineCode], T1.[ContactTypesCode], T1.[AirLineCttSeq] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B24", "SELECT [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] FROM [AIRLINESCONTACTS] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002B25", "INSERT INTO [AIRLINESCONTACTS] ([ISOCod], [AirLineCode], [AirLineCttSeq], [AirLineCttName], [AirLineCttPhone], [AirLineCttEmail], [ContactTypesCode]) VALUES (?, ?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002B26", "UPDATE [AIRLINESCONTACTS] SET [AirLineCttName]=?, [AirLineCttPhone]=?, [AirLineCttEmail]=?  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002B27", "DELETE FROM [AIRLINESCONTACTS]  WHERE [ISOCod] = ? AND [AirLineCode] = ? AND [ContactTypesCode] = ? AND [AirLineCttSeq] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002B28", "SELECT [ContactTypesDescription] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002B29", "SELECT T1.[ISOCod], T1.[AirLineCode], T1.[AirLineCttSeq], T1.[AirLineCttName], T1.[AirLineCttPhone], T1.[AirLineCttEmail], T2.[ContactTypesDescription], T1.[ContactTypesCode] FROM ([AIRLINESCONTACTS] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [CONTACTTYPES] T2 WITH (NOLOCK) ON T2.[ContactTypesCode] = T1.[ContactTypesCode]) WHERE T1.[ISOCod] = ? and T1.[AirLineCode] = ? ORDER BY T1.[ISOCod], T1.[AirLineCode], T1.[ContactTypesCode], T1.[AirLineCttSeq] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 1) ;
               ((String[]) buf[15])[0] = rslt.getString(14, 1) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getVarchar(15) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 1) ;
               ((String[]) buf[15])[0] = rslt.getString(14, 1) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getVarchar(15) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(14, 1) ;
               ((String[]) buf[17])[0] = rslt.getString(15, 1) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getVarchar(16) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(10) ;
               ((String[]) buf[12])[0] = rslt.getVarchar(11) ;
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(14, 1) ;
               ((String[]) buf[17])[0] = rslt.getString(15, 1) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getVarchar(16) ;
               break;
            case 15 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 20 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               break;
            case 21 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
               break;
            case 22 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
            case 26 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 27 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
               ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               stmt.setDateTime(3, (java.util.Date)parms[2], false);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[4], 1);
               }
               stmt.setVarchar(5, (String)parms[5], 30, false);
               stmt.setVarchar(6, (String)parms[6], 5, false);
               stmt.setVarchar(7, (String)parms[7], 200, false);
               stmt.setVarchar(8, (String)parms[8], 30, false);
               stmt.setVarchar(9, (String)parms[9], 30, false);
               stmt.setVarchar(10, (String)parms[10], 5, false);
               stmt.setVarchar(11, (String)parms[11], 10, false);
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 200);
               }
               stmt.setString(13, (String)parms[14], 1);
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(14, (String)parms[16], 1);
               }
               stmt.setVarchar(15, (String)parms[17], 3, false);
               break;
            case 11 :
               stmt.setDateTime(1, (java.util.Date)parms[0], false);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[3], 1);
               }
               stmt.setVarchar(4, (String)parms[4], 30, false);
               stmt.setVarchar(5, (String)parms[5], 5, false);
               stmt.setVarchar(6, (String)parms[6], 200, false);
               stmt.setVarchar(7, (String)parms[7], 30, false);
               stmt.setVarchar(8, (String)parms[8], 30, false);
               stmt.setVarchar(9, (String)parms[9], 5, false);
               stmt.setVarchar(10, (String)parms[10], 10, false);
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 200);
               }
               stmt.setString(12, (String)parms[13], 1);
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[15], 1);
               }
               stmt.setVarchar(14, (String)parms[16], 3, false);
               stmt.setVarchar(15, (String)parms[17], 20, false);
               break;
            case 12 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 13 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               break;
            case 14 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 15 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 16 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 17 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               stmt.setLongVarchar(4, (String)parms[3], false);
               stmt.setVarchar(5, (String)parms[4], 3, false);
               break;
            case 18 :
               stmt.setLongVarchar(1, (String)parms[0], false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               stmt.setVarchar(5, (String)parms[4], 3, false);
               break;
            case 19 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3, false);
               break;
            case 20 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 21 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 22 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 23 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setVarchar(4, (String)parms[3], 30, false);
               stmt.setVarchar(5, (String)parms[4], 25, false);
               stmt.setVarchar(6, (String)parms[5], 40, false);
               stmt.setVarchar(7, (String)parms[6], 5, false);
               break;
            case 24 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setVarchar(2, (String)parms[1], 25, false);
               stmt.setVarchar(3, (String)parms[2], 40, false);
               stmt.setVarchar(4, (String)parms[3], 3, false);
               stmt.setVarchar(5, (String)parms[4], 20, false);
               stmt.setVarchar(6, (String)parms[5], 5, false);
               stmt.setShort(7, ((Number) parms[6]).shortValue());
               break;
            case 25 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 26 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 27 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
      }
   }

}


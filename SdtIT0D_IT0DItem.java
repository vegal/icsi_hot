import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtIT0D_IT0DItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtIT0D_IT0DItem( )
   {
      this(  new ModelContext(SdtIT0D_IT0DItem.class));
   }

   public SdtIT0D_IT0DItem( ModelContext context )
   {
      super( context, "SdtIT0D_IT0DItem");
   }

   public SdtIT0D_IT0DItem( StructSdtIT0D_IT0DItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PLID_IT0D") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0D_IT0DItem_Plid_it0d = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PLTX_IT0D") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0D_IT0DItem_Pltx_it0d = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CPNR_IT0D") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0D_IT0DItem_Cpnr_it0d = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "IT0D.IT0DItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("PLID_IT0D", GXutil.rtrim( gxTv_SdtIT0D_IT0DItem_Plid_it0d));
      oWriter.writeElement("PLTX_IT0D", GXutil.rtrim( gxTv_SdtIT0D_IT0DItem_Pltx_it0d));
      oWriter.writeElement("CPNR_IT0D", GXutil.trim( GXutil.str( gxTv_SdtIT0D_IT0DItem_Cpnr_it0d, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtIT0D_IT0DItem_Plid_it0d( )
   {
      return gxTv_SdtIT0D_IT0DItem_Plid_it0d ;
   }

   public void setgxTv_SdtIT0D_IT0DItem_Plid_it0d( String value )
   {
      gxTv_SdtIT0D_IT0DItem_Plid_it0d = value ;
      return  ;
   }

   public void setgxTv_SdtIT0D_IT0DItem_Plid_it0d_SetNull( )
   {
      gxTv_SdtIT0D_IT0DItem_Plid_it0d = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0D_IT0DItem_Pltx_it0d( )
   {
      return gxTv_SdtIT0D_IT0DItem_Pltx_it0d ;
   }

   public void setgxTv_SdtIT0D_IT0DItem_Pltx_it0d( String value )
   {
      gxTv_SdtIT0D_IT0DItem_Pltx_it0d = value ;
      return  ;
   }

   public void setgxTv_SdtIT0D_IT0DItem_Pltx_it0d_SetNull( )
   {
      gxTv_SdtIT0D_IT0DItem_Pltx_it0d = "" ;
      return  ;
   }

   public byte getgxTv_SdtIT0D_IT0DItem_Cpnr_it0d( )
   {
      return gxTv_SdtIT0D_IT0DItem_Cpnr_it0d ;
   }

   public void setgxTv_SdtIT0D_IT0DItem_Cpnr_it0d( byte value )
   {
      gxTv_SdtIT0D_IT0DItem_Cpnr_it0d = value ;
      return  ;
   }

   public void setgxTv_SdtIT0D_IT0DItem_Cpnr_it0d_SetNull( )
   {
      gxTv_SdtIT0D_IT0DItem_Cpnr_it0d = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtIT0D_IT0DItem_Plid_it0d = "" ;
      gxTv_SdtIT0D_IT0DItem_Pltx_it0d = "" ;
      gxTv_SdtIT0D_IT0DItem_Cpnr_it0d = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char3 = "" ;
      return  ;
   }

   public SdtIT0D_IT0DItem Clone( )
   {
      return (SdtIT0D_IT0DItem)(clone()) ;
   }

   public void setStruct( StructSdtIT0D_IT0DItem struct )
   {
      setgxTv_SdtIT0D_IT0DItem_Plid_it0d(struct.getPlid_it0d());
      setgxTv_SdtIT0D_IT0DItem_Pltx_it0d(struct.getPltx_it0d());
      setgxTv_SdtIT0D_IT0DItem_Cpnr_it0d(struct.getCpnr_it0d());
   }

   public StructSdtIT0D_IT0DItem getStruct( )
   {
      StructSdtIT0D_IT0DItem struct = new StructSdtIT0D_IT0DItem ();
      struct.setPlid_it0d(getgxTv_SdtIT0D_IT0DItem_Plid_it0d());
      struct.setPltx_it0d(getgxTv_SdtIT0D_IT0DItem_Pltx_it0d());
      struct.setCpnr_it0d(getgxTv_SdtIT0D_IT0DItem_Cpnr_it0d());
      return struct ;
   }

   private byte gxTv_SdtIT0D_IT0DItem_Cpnr_it0d ;
   private short nOutParmCount ;
   private short readOk ;
   private String sTagName ;
   private String GXt_char3 ;
   private String gxTv_SdtIT0D_IT0DItem_Plid_it0d ;
   private String gxTv_SdtIT0D_IT0DItem_Pltx_it0d ;
}


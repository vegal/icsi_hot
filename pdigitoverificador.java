/*
               File: DigitoVerificador
        Description: Digito Verificador do IATA
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:57.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pdigitoverificador extends GXProcedure
{
   public pdigitoverificador( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pdigitoverificador.class ), "" );
   }

   public pdigitoverificador( int remoteHandle ,
                              ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String[] aP1 )
   {
      pdigitoverificador.this.AV8CadIATA = aP0;
      pdigitoverificador.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV10vCadIA = (long)(GXutil.val( AV8CadIATA, ".")) ;
      AV9CadDigI = GXutil.trim( GXutil.str( AV10vCadIA-(GXutil.Int( AV10vCadIA/ (double) (7))*7), 10, 0)) ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP1[0] = pdigitoverificador.this.AV9CadDigI;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9CadDigI = "" ;
      AV10vCadIA = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private long AV10vCadIA ;
   private String AV8CadIATA ;
   private String AV9CadDigI ;
   private String[] aP1 ;
}


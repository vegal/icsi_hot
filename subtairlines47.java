import com.genexus.*;
import com.genexus.ui.*;

public final  class subtairlines47 extends GXSubfileElement
{
   private String ContactTypesCode ;
   private short AirLineCttSeq ;
   private String AirLineCttName ;
   private String AirLineCttPhone ;
   private String AirLineCttEmail ;
   private String ContactTypesDescription ;
   private String ZAirLineCttName ;
   private String ZAirLineCttPhone ;
   private String ZAirLineCttEmail ;
   public String getContactTypesCode( )
   {
      return ContactTypesCode ;
   }

   public void setContactTypesCode( String value )
   {
      ContactTypesCode = value;
   }

   public short getAirLineCttSeq( )
   {
      return AirLineCttSeq ;
   }

   public void setAirLineCttSeq( short value )
   {
      AirLineCttSeq = value;
   }

   public String getAirLineCttName( )
   {
      return AirLineCttName ;
   }

   public void setAirLineCttName( String value )
   {
      AirLineCttName = value;
   }

   public String getAirLineCttPhone( )
   {
      return AirLineCttPhone ;
   }

   public void setAirLineCttPhone( String value )
   {
      AirLineCttPhone = value;
   }

   public String getAirLineCttEmail( )
   {
      return AirLineCttEmail ;
   }

   public void setAirLineCttEmail( String value )
   {
      AirLineCttEmail = value;
   }

   public String getContactTypesDescription( )
   {
      return ContactTypesDescription ;
   }

   public void setContactTypesDescription( String value )
   {
      ContactTypesDescription = value;
   }

   public String getZAirLineCttName( )
   {
      return ZAirLineCttName ;
   }

   public void setZAirLineCttName( String value )
   {
      ZAirLineCttName = value;
   }

   public String getZAirLineCttPhone( )
   {
      return ZAirLineCttPhone ;
   }

   public void setZAirLineCttPhone( String value )
   {
      ZAirLineCttPhone = value;
   }

   public String getZAirLineCttEmail( )
   {
      return ZAirLineCttEmail ;
   }

   public void setZAirLineCttEmail( String value )
   {
      ZAirLineCttEmail = value;
   }

   public void clear( )
   {
      ContactTypesCode = "" ;
      AirLineCttSeq = 0 ;
      AirLineCttName = "" ;
      AirLineCttPhone = "" ;
      AirLineCttEmail = "" ;
      ContactTypesDescription = "" ;
      ZAirLineCttName = "" ;
      ZAirLineCttPhone = "" ;
      ZAirLineCttEmail = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getContactTypesCode().compareTo(((subtairlines47) element).getContactTypesCode()) ;
            case 1 :
               if ( getAirLineCttSeq() > ((subtairlines47) element).getAirLineCttSeq() ) return 1;
               if ( getAirLineCttSeq() < ((subtairlines47) element).getAirLineCttSeq() ) return -1;
               return 0;
            case 2 :
               return  getAirLineCttName().compareTo(((subtairlines47) element).getAirLineCttName()) ;
            case 3 :
               return  getAirLineCttPhone().compareTo(((subtairlines47) element).getAirLineCttPhone()) ;
            case 4 :
               return  getAirLineCttEmail().compareTo(((subtairlines47) element).getAirLineCttEmail()) ;
            case 5 :
               return  getContactTypesDescription().compareTo(((subtairlines47) element).getContactTypesDescription()) ;
      }
      return 0;
   }

   public int isChanged( )
   {
      return (!userModified && (!inserted
      && ( GXutil.strcmp(ZAirLineCttName,AirLineCttName) == 0)
      && ( GXutil.strcmp(ZAirLineCttPhone,AirLineCttPhone) == 0)
      && ( GXutil.strcmp(ZAirLineCttEmail,AirLineCttEmail) == 0)
      ))?0:1;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getContactTypesCode(), "") == 0 ) && ( getAirLineCttSeq() == 0 ) && ( GXutil.strcmp(getAirLineCttName(), "") == 0 ) && ( GXutil.strcmp(getAirLineCttPhone(), "") == 0 ) && ( GXutil.strcmp(getAirLineCttEmail(), "") == 0 ) && ( GXutil.strcmp(getContactTypesDescription(), "") == 0 ) || insertedNotUserModified() )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getContactTypesCode() );
            break;
         case 1 :
            cell.setValue( getAirLineCttSeq() );
            break;
         case 2 :
            cell.setValue( getAirLineCttName() );
            break;
         case 3 :
            cell.setValue( getAirLineCttPhone() );
            break;
         case 4 :
            cell.setValue( getAirLineCttEmail() );
            break;
         case 5 :
            cell.setValue( getContactTypesDescription() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getContactTypesCode()) == 0) );
         case 1 :
            return ( (((GUIObjectShort) cell).getValue() == getAirLineCttSeq()) );
         case 2 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCttName()) == 0) );
         case 3 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCttPhone()) == 0) );
         case 4 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCttEmail()) == 0) );
         case 5 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getContactTypesDescription()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setContactTypesCode(value.getStringValue());
               break;
            case 1 :
               setAirLineCttSeq(value.getShortValue());
               break;
            case 2 :
               setAirLineCttName(value.getStringValue());
               break;
            case 3 :
               setAirLineCttPhone(value.getStringValue());
               break;
            case 4 :
               setAirLineCttEmail(value.getStringValue());
               break;
            case 5 :
               setContactTypesDescription(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setContactTypesCode(((subtairlines47) element).getContactTypesCode());
               return;
            case 1 :
               setAirLineCttSeq(((subtairlines47) element).getAirLineCttSeq());
               return;
            case 2 :
               setAirLineCttName(((subtairlines47) element).getAirLineCttName());
               return;
            case 3 :
               setAirLineCttPhone(((subtairlines47) element).getAirLineCttPhone());
               return;
            case 4 :
               setAirLineCttEmail(((subtairlines47) element).getAirLineCttEmail());
               return;
            case 5 :
               setContactTypesDescription(((subtairlines47) element).getContactTypesDescription());
               return;
      }
   }

}


/*
               File: tcontacttypes_bc
        Description: Contact Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:2.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tcontacttypes_bc extends GXWebPanel implements IGxSilentTrn
{
   public tcontacttypes_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tcontacttypes_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcontacttypes_bc.class ));
   }

   public tcontacttypes_bc( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         /* Execute user event: e11282 */
         e11282 ();
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z365Contac = A365Contac ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_280( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls28223( ) ;
         }
         else
         {
            checkExtendedTable28223( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors28223( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues280( ) ;
      }
   }

   public void e12282( )
   {
      /* Start Routine */
      AV13LgnLog = AV11Sessio.getValue("LOGGED") ;
      if ( ( GXutil.strcmp(AV13LgnLog, "") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hlogin") );
      }
   }

   public void e13282( )
   {
      /* 'Back' Routine */
      httpContext.setLinkToRedirect(formatLink("hcontacttypes") );
   }

   public void e11282( )
   {
      /* After Trn Routine */
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hcontacttypes") );
      }
   }

   public void zm28223( int GX_JID )
   {
      if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
      {
         Z366Contac = A366Contac ;
         Z1104Conta = A1104Conta ;
      }
      if ( ( GX_JID == -4 ) )
      {
         Z365Contac = A365Contac ;
         Z366Contac = A366Contac ;
         Z1104Conta = A1104Conta ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load28223( )
   {
      /* Using cursor BC00284 */
      pr_default.execute(2, new Object[] {A365Contac});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound223 = (short)(1) ;
         A366Contac = BC00284_A366Contac[0] ;
         A1104Conta = BC00284_A1104Conta[0] ;
         zm28223( -4) ;
      }
      pr_default.close(2);
      onLoadActions28223( ) ;
   }

   public void onLoadActions28223( )
   {
   }

   public void checkExtendedTable28223( )
   {
      standaloneModal( ) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A365Contac), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Code can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A366Contac), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Description can not be null", 1);
         AnyError = (short)(1) ;
      }
   }

   public void closeExtendedTableCursors28223( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey28223( )
   {
      /* Using cursor BC00285 */
      pr_default.execute(3, new Object[] {A365Contac});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound223 = (short)(1) ;
      }
      else
      {
         RcdFound223 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC00283 */
      pr_default.execute(1, new Object[] {A365Contac});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm28223( 4) ;
         RcdFound223 = (short)(1) ;
         A365Contac = BC00283_A365Contac[0] ;
         A366Contac = BC00283_A366Contac[0] ;
         A1104Conta = BC00283_A1104Conta[0] ;
         Z365Contac = A365Contac ;
         sMode223 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load28223( ) ;
         Gx_mode = sMode223 ;
      }
      else
      {
         RcdFound223 = (short)(0) ;
         initializeNonKey28223( ) ;
         sMode223 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode223 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey28223( ) ;
      if ( ( RcdFound223 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_280( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency28223( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00282 */
         pr_default.execute(0, new Object[] {A365Contac});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"CONTACTTYPES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z366Contac, BC00282_A366Contac[0]) != 0 ) || ( GXutil.strcmp(Z1104Conta, BC00282_A1104Conta[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"CONTACTTYPES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert28223( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm28223( 0) ;
         checkOptimisticConcurrency28223( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm28223( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert28223( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00286 */
                  pr_default.execute(4, new Object[] {A365Contac, A366Contac, A1104Conta});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load28223( ) ;
         }
         endLevel28223( ) ;
      }
      closeExtendedTableCursors28223( ) ;
   }

   public void update28223( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency28223( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm28223( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate28223( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00287 */
                  pr_default.execute(5, new Object[] {A366Contac, A1104Conta, A365Contac});
                  deferredUpdate28223( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel28223( ) ;
      }
      closeExtendedTableCursors28223( ) ;
   }

   public void deferredUpdate28223( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls28223( ) ;
         /* No cascading delete specified. */
         afterConfirm28223( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete28223( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC00288 */
               pr_default.execute(6, new Object[] {A365Contac});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode223 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel28223( ) ;
      Gx_mode = sMode223 ;
   }

   public void onDeleteControls28223( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC00289 */
         pr_default.execute(7, new Object[] {A365Contac});
         if ( (pr_default.getStatus(7) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Contacts"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(7);
         /* Using cursor BC002810 */
         pr_default.execute(8, new Object[] {A365Contac});
         if ( (pr_default.getStatus(8) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Contact Types"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(8);
      }
   }

   public void endLevel28223( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues280( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart28223( )
   {
      /* Using cursor BC002811 */
      pr_default.execute(9, new Object[] {A365Contac});
      RcdFound223 = (short)(0) ;
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound223 = (short)(1) ;
         A365Contac = BC002811_A365Contac[0] ;
         A366Contac = BC002811_A366Contac[0] ;
         A1104Conta = BC002811_A1104Conta[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext28223( )
   {
      pr_default.readNext(9);
      RcdFound223 = (short)(0) ;
      scanLoad28223( ) ;
   }

   public void scanLoad28223( )
   {
      sMode223 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound223 = (short)(1) ;
         A365Contac = BC002811_A365Contac[0] ;
         A366Contac = BC002811_A366Contac[0] ;
         A1104Conta = BC002811_A1104Conta[0] ;
      }
      Gx_mode = sMode223 ;
   }

   public void scanEnd28223( )
   {
      pr_default.close(9);
   }

   public void afterConfirm28223( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert28223( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate28223( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete28223( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete28223( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate28223( )
   {
      /* Before Validate Rules */
   }

   public void addRow28223( )
   {
      VarsToRow223( bcContactTypes) ;
   }

   public void sendRow28223( )
   {
   }

   public void readRow28223( )
   {
      RowToVars223( bcContactTypes, 0) ;
   }

   public void confirmValues280( )
   {
   }

   public void initializeNonKey28223( )
   {
      A366Contac = "" ;
      A1104Conta = "" ;
   }

   public void initAll28223( )
   {
      A365Contac = "" ;
      initializeNonKey28223( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow223( SdtContactTypes obj223 )
   {
      obj223.setgxTv_SdtContactTypes_Mode( Gx_mode );
      obj223.setgxTv_SdtContactTypes_Contacttypesdescription( A366Contac );
      obj223.setgxTv_SdtContactTypes_Contacttypesstatus( A1104Conta );
      obj223.setgxTv_SdtContactTypes_Contacttypescode( A365Contac );
      obj223.setgxTv_SdtContactTypes_Contacttypescode_Z( Z365Contac );
      obj223.setgxTv_SdtContactTypes_Contacttypesdescription_Z( Z366Contac );
      obj223.setgxTv_SdtContactTypes_Contacttypesstatus_Z( Z1104Conta );
      obj223.setgxTv_SdtContactTypes_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars223( SdtContactTypes obj223 ,
                             int forceLoad )
   {
      Gx_mode = obj223.getgxTv_SdtContactTypes_Mode() ;
      A366Contac = obj223.getgxTv_SdtContactTypes_Contacttypesdescription() ;
      A1104Conta = obj223.getgxTv_SdtContactTypes_Contacttypesstatus() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A365Contac = obj223.getgxTv_SdtContactTypes_Contacttypescode() ;
      }
      Z365Contac = obj223.getgxTv_SdtContactTypes_Contacttypescode_Z() ;
      Z366Contac = obj223.getgxTv_SdtContactTypes_Contacttypesdescription_Z() ;
      Z1104Conta = obj223.getgxTv_SdtContactTypes_Contacttypesstatus_Z() ;
      Gx_mode = obj223.getgxTv_SdtContactTypes_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A365Contac = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey28223( ) ;
      scanStart28223( ) ;
      if ( ( RcdFound223 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z365Contac = A365Contac ;
      }
      onLoadActions28223( ) ;
      zm28223( 0) ;
      addRow28223( ) ;
      scanEnd28223( ) ;
      if ( ( RcdFound223 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars223( bcContactTypes, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey28223( ) ;
      if ( ( RcdFound223 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
         {
            A365Contac = Z365Contac ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update28223( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert28223( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert28223( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow223( bcContactTypes) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars223( bcContactTypes, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey28223( ) ;
      if ( ( RcdFound223 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
         {
            A365Contac = Z365Contac ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tcontacttypes_bc");
      VarsToRow223( bcContactTypes) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcContactTypes.getgxTv_SdtContactTypes_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcContactTypes.setgxTv_SdtContactTypes_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtContactTypes sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcContactTypes ) )
      {
         bcContactTypes = sdt ;
         if ( ( GXutil.strcmp(bcContactTypes.getgxTv_SdtContactTypes_Mode(), "") == 0 ) )
         {
            bcContactTypes.setgxTv_SdtContactTypes_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow223( bcContactTypes) ;
         }
         else
         {
            RowToVars223( bcContactTypes, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcContactTypes.getgxTv_SdtContactTypes_Mode(), "") == 0 ) )
         {
            bcContactTypes.setgxTv_SdtContactTypes_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars223( bcContactTypes, 1) ;
      return  ;
   }

   public SdtContactTypes getContactTypes_BC( )
   {
      return bcContactTypes ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z365Contac = "" ;
      A365Contac = "" ;
      AV13LgnLog = "" ;
      AV11Sessio = httpContext.getWebSession();
      gxTv_SdtContactTypes_Contacttypescode_Z = "" ;
      gxTv_SdtContactTypes_Contacttypesdescription_Z = "" ;
      gxTv_SdtContactTypes_Contacttypesstatus_Z = "" ;
      GX_JID = 0 ;
      Z366Contac = "" ;
      A366Contac = "" ;
      Z1104Conta = "" ;
      A1104Conta = "" ;
      BC00284_A365Contac = new String[] {""} ;
      BC00284_A366Contac = new String[] {""} ;
      BC00284_A1104Conta = new String[] {""} ;
      RcdFound223 = (short)(0) ;
      BC00285_A365Contac = new String[] {""} ;
      BC00283_A365Contac = new String[] {""} ;
      BC00283_A366Contac = new String[] {""} ;
      BC00283_A1104Conta = new String[] {""} ;
      sMode223 = "" ;
      BC00282_A365Contac = new String[] {""} ;
      BC00282_A366Contac = new String[] {""} ;
      BC00282_A1104Conta = new String[] {""} ;
      BC00289_A23ISOCod = new String[] {""} ;
      BC00289_A1136AirLi = new String[] {""} ;
      BC00289_A365Contac = new String[] {""} ;
      BC00289_A1138AirLi = new short[1] ;
      BC002810_A23ISOCod = new String[] {""} ;
      BC002810_A365Contac = new String[] {""} ;
      BC002810_A406ISOCtt = new short[1] ;
      BC002811_A365Contac = new String[] {""} ;
      BC002811_A366Contac = new String[] {""} ;
      BC002811_A1104Conta = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tcontacttypes_bc__default(),
         new Object[] {
             new Object[] {
            BC00282_A365Contac, BC00282_A366Contac, BC00282_A1104Conta
            }
            , new Object[] {
            BC00283_A365Contac, BC00283_A366Contac, BC00283_A1104Conta
            }
            , new Object[] {
            BC00284_A365Contac, BC00284_A366Contac, BC00284_A1104Conta
            }
            , new Object[] {
            BC00285_A365Contac
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC00289_A23ISOCod, BC00289_A1136AirLi, BC00289_A365Contac, BC00289_A1138AirLi
            }
            , new Object[] {
            BC002810_A23ISOCod, BC002810_A365Contac, BC002810_A406ISOCtt
            }
            , new Object[] {
            BC002811_A365Contac, BC002811_A366Contac, BC002811_A1104Conta
            }
         }
      );
      /* Execute Start event if defined. */
      /* Execute user event: e12282 */
      e12282 ();
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound223 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String gxTv_SdtContactTypes_Contacttypesstatus_Z ;
   private String Z1104Conta ;
   private String A1104Conta ;
   private String sMode223 ;
   private String Z365Contac ;
   private String A365Contac ;
   private String AV13LgnLog ;
   private String gxTv_SdtContactTypes_Contacttypescode_Z ;
   private String gxTv_SdtContactTypes_Contacttypesdescription_Z ;
   private String Z366Contac ;
   private String A366Contac ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.WebSession AV11Sessio ;
   private SdtContactTypes bcContactTypes ;
   private IDataStoreProvider pr_default ;
   private String[] BC00284_A365Contac ;
   private String[] BC00284_A366Contac ;
   private String[] BC00284_A1104Conta ;
   private String[] BC00285_A365Contac ;
   private String[] BC00283_A365Contac ;
   private String[] BC00283_A366Contac ;
   private String[] BC00283_A1104Conta ;
   private String[] BC00282_A365Contac ;
   private String[] BC00282_A366Contac ;
   private String[] BC00282_A1104Conta ;
   private String[] BC00289_A23ISOCod ;
   private String[] BC00289_A1136AirLi ;
   private String[] BC00289_A365Contac ;
   private short[] BC00289_A1138AirLi ;
   private String[] BC002810_A23ISOCod ;
   private String[] BC002810_A365Contac ;
   private short[] BC002810_A406ISOCtt ;
   private String[] BC002811_A365Contac ;
   private String[] BC002811_A366Contac ;
   private String[] BC002811_A1104Conta ;
}

final  class tcontacttypes_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00282", "SELECT [ContactTypesCode], [ContactTypesDescription], [ContactTypesStatus] FROM [CONTACTTYPES] WITH (UPDLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00283", "SELECT [ContactTypesCode], [ContactTypesDescription], [ContactTypesStatus] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00284", "SELECT TM1.[ContactTypesCode], TM1.[ContactTypesDescription], TM1.[ContactTypesStatus] FROM [CONTACTTYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ContactTypesCode] = ? ORDER BY TM1.[ContactTypesCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00285", "SELECT [ContactTypesCode] FROM [CONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC00286", "INSERT INTO [CONTACTTYPES] ([ContactTypesCode], [ContactTypesDescription], [ContactTypesStatus]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC00287", "UPDATE [CONTACTTYPES] SET [ContactTypesDescription]=?, [ContactTypesStatus]=?  WHERE [ContactTypesCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC00288", "DELETE FROM [CONTACTTYPES]  WHERE [ContactTypesCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC00289", "SELECT TOP 1 [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC002810", "SELECT TOP 1 [ISOCod], [ContactTypesCode], [ISOCttSeq] FROM [COUNTRYCONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC002811", "SELECT TM1.[ContactTypesCode], TM1.[ContactTypesDescription], TM1.[ContactTypesStatus] FROM [CONTACTTYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ContactTypesCode] = ? ORDER BY TM1.[ContactTypesCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               stmt.setVarchar(2, (String)parms[1], 30, false);
               stmt.setString(3, (String)parms[2], 1);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setString(2, (String)parms[1], 1);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
      }
   }

}


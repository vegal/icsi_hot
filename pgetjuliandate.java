/*
               File: getJulianDate
        Description: get Julian Date
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.31
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pgetjuliandate extends GXProcedure
{
   public pgetjuliandate( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pgetjuliandate.class ), "" );
   }

   public pgetjuliandate( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( java.util.Date aP0 ,
                        short[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( java.util.Date aP0 ,
                             short[] aP1 )
   {
      pgetjuliandate.this.AV8inDate = aP0;
      pgetjuliandate.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV11finalD = "01/01/" + GXutil.trim( GXutil.str( GXutil.year( AV8inDate), 10, 0)) ;
      AV10finalD = localUtil.ctod( AV11finalD, 2) ;
      AV9outDate = (short)(1) ;
      while ( AV10finalD.before( AV8inDate ) )
      {
         AV10finalD = GXutil.dadd(AV10finalD,+(1)) ;
         AV9outDate = (short)(AV9outDate+1) ;
      }
      cleanup();
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(pgetjuliandate.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP1[0] = pgetjuliandate.this.AV9outDate;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9outDate = (short)(0) ;
      AV11finalD = "" ;
      AV10finalD = GXutil.nullDate() ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short AV9outDate ;
   private short Gx_err ;
   private String AV11finalD ;
   private java.util.Date AV8inDate ;
   private java.util.Date AV10finalD ;
   private short[] aP1 ;
}


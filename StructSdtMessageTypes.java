
public final  class StructSdtMessageTypes implements Cloneable, java.io.Serializable
{
   public StructSdtMessageTypes( )
   {
      gxTv_SdtMessageTypes_Msgcode = "" ;
      gxTv_SdtMessageTypes_Msgdescription = "" ;
      gxTv_SdtMessageTypes_Msgsubject = "" ;
      gxTv_SdtMessageTypes_Msgbody = "" ;
      gxTv_SdtMessageTypes_Msgstatus = (byte)(0) ;
      gxTv_SdtMessageTypes_Mode = "" ;
      gxTv_SdtMessageTypes_Msgcode_Z = "" ;
      gxTv_SdtMessageTypes_Msgdescription_Z = "" ;
      gxTv_SdtMessageTypes_Msgsubject_Z = "" ;
      gxTv_SdtMessageTypes_Msgbody_Z = "" ;
      gxTv_SdtMessageTypes_Msgstatus_Z = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgdescription_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgsubject_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgbody_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgstatus_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getMsgcode( )
   {
      return gxTv_SdtMessageTypes_Msgcode ;
   }

   public void setMsgcode( String value )
   {
      gxTv_SdtMessageTypes_Msgcode = value ;
      return  ;
   }

   public String getMsgdescription( )
   {
      return gxTv_SdtMessageTypes_Msgdescription ;
   }

   public void setMsgdescription( String value )
   {
      gxTv_SdtMessageTypes_Msgdescription = value ;
      return  ;
   }

   public String getMsgsubject( )
   {
      return gxTv_SdtMessageTypes_Msgsubject ;
   }

   public void setMsgsubject( String value )
   {
      gxTv_SdtMessageTypes_Msgsubject = value ;
      return  ;
   }

   public String getMsgbody( )
   {
      return gxTv_SdtMessageTypes_Msgbody ;
   }

   public void setMsgbody( String value )
   {
      gxTv_SdtMessageTypes_Msgbody = value ;
      return  ;
   }

   public byte getMsgstatus( )
   {
      return gxTv_SdtMessageTypes_Msgstatus ;
   }

   public void setMsgstatus( byte value )
   {
      gxTv_SdtMessageTypes_Msgstatus = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtMessageTypes_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtMessageTypes_Mode = value ;
      return  ;
   }

   public String getMsgcode_Z( )
   {
      return gxTv_SdtMessageTypes_Msgcode_Z ;
   }

   public void setMsgcode_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgcode_Z = value ;
      return  ;
   }

   public String getMsgdescription_Z( )
   {
      return gxTv_SdtMessageTypes_Msgdescription_Z ;
   }

   public void setMsgdescription_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgdescription_Z = value ;
      return  ;
   }

   public String getMsgsubject_Z( )
   {
      return gxTv_SdtMessageTypes_Msgsubject_Z ;
   }

   public void setMsgsubject_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgsubject_Z = value ;
      return  ;
   }

   public String getMsgbody_Z( )
   {
      return gxTv_SdtMessageTypes_Msgbody_Z ;
   }

   public void setMsgbody_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgbody_Z = value ;
      return  ;
   }

   public byte getMsgstatus_Z( )
   {
      return gxTv_SdtMessageTypes_Msgstatus_Z ;
   }

   public void setMsgstatus_Z( byte value )
   {
      gxTv_SdtMessageTypes_Msgstatus_Z = value ;
      return  ;
   }

   public byte getMsgdescription_N( )
   {
      return gxTv_SdtMessageTypes_Msgdescription_N ;
   }

   public void setMsgdescription_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgdescription_N = value ;
      return  ;
   }

   public byte getMsgsubject_N( )
   {
      return gxTv_SdtMessageTypes_Msgsubject_N ;
   }

   public void setMsgsubject_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgsubject_N = value ;
      return  ;
   }

   public byte getMsgbody_N( )
   {
      return gxTv_SdtMessageTypes_Msgbody_N ;
   }

   public void setMsgbody_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgbody_N = value ;
      return  ;
   }

   public byte getMsgstatus_N( )
   {
      return gxTv_SdtMessageTypes_Msgstatus_N ;
   }

   public void setMsgstatus_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgstatus_N = value ;
      return  ;
   }

   protected byte gxTv_SdtMessageTypes_Msgstatus ;
   protected byte gxTv_SdtMessageTypes_Msgstatus_Z ;
   protected byte gxTv_SdtMessageTypes_Msgdescription_N ;
   protected byte gxTv_SdtMessageTypes_Msgsubject_N ;
   protected byte gxTv_SdtMessageTypes_Msgbody_N ;
   protected byte gxTv_SdtMessageTypes_Msgstatus_N ;
   protected String gxTv_SdtMessageTypes_Mode ;
   protected String gxTv_SdtMessageTypes_Msgbody ;
   protected String gxTv_SdtMessageTypes_Msgbody_Z ;
   protected String gxTv_SdtMessageTypes_Msgcode ;
   protected String gxTv_SdtMessageTypes_Msgdescription ;
   protected String gxTv_SdtMessageTypes_Msgsubject ;
   protected String gxTv_SdtMessageTypes_Msgcode_Z ;
   protected String gxTv_SdtMessageTypes_Msgdescription_Z ;
   protected String gxTv_SdtMessageTypes_Msgsubject_Z ;
}


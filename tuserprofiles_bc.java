/*
               File: tuserprofiles_bc
        Description: User Profiles
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:14.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tuserprofiles_bc extends GXWebPanel implements IGxSilentTrn
{
   public tuserprofiles_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tuserprofiles_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tuserprofiles_bc.class ));
   }

   public tuserprofiles_bc( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z64ustCode = A64ustCode ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_0E0( )
   {
      beforeValidate0E22( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls0E22( ) ;
         }
         else
         {
            checkExtendedTable0E22( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors0E22( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         /* Save parent mode. */
         sMode22 = Gx_mode ;
         confirm_0E47( ) ;
         if ( ( AnyError == 0 ) )
         {
            /* Restore parent mode. */
            Gx_mode = sMode22 ;
            IsConfirmed = (short)(1) ;
         }
         /* Restore parent mode. */
         Gx_mode = sMode22 ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues0E0( ) ;
      }
   }

   public void confirm_0E47( )
   {
      nGXsfl_47_idx = (short)(0) ;
      while ( ( nGXsfl_47_idx < bcUserProfiles.getgxTv_SdtUserProfiles_Functions().size() ) )
      {
         readRow0E47( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound47 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_47 != 0 ) )
         {
            getKey0E47( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound47 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate0E47( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable0E47( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        zm0E47( 4) ;
                     }
                     closeExtendedTableCursors0E47( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound47 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey0E47( ) ;
                     load0E47( ) ;
                     beforeValidate0E47( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls0E47( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_47 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate0E47( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable0E47( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              zm0E47( 4) ;
                           }
                           closeExtendedTableCursors0E47( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void e110E2( )
   {
      /* 'Back' Routine */
   }

   public void zm0E22( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         Z65ustDesc = A65ustDesc ;
         Z47uspCode = A47uspCode ;
         Z51uspDesc = A51uspDesc ;
      }
      if ( ( GX_JID == -2 ) )
      {
         Z64ustCode = A64ustCode ;
         Z65ustDesc = A65ustDesc ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load0E22( )
   {
      /* Using cursor BC000E7 */
      pr_default.execute(5, new Object[] {A64ustCode});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound22 = (short)(1) ;
         A65ustDesc = BC000E7_A65ustDesc[0] ;
         n65ustDesc = BC000E7_n65ustDesc[0] ;
         zm0E22( -2) ;
      }
      pr_default.close(5);
      onLoadActions0E22( ) ;
   }

   public void onLoadActions0E22( )
   {
   }

   public void checkExtendedTable0E22( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors0E22( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey0E22( )
   {
      /* Using cursor BC000E8 */
      pr_default.execute(6, new Object[] {A64ustCode});
      if ( (pr_default.getStatus(6) != 101) )
      {
         RcdFound22 = (short)(1) ;
      }
      else
      {
         RcdFound22 = (short)(0) ;
      }
      pr_default.close(6);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC000E6 */
      pr_default.execute(4, new Object[] {A64ustCode});
      if ( (pr_default.getStatus(4) != 101) )
      {
         zm0E22( 2) ;
         RcdFound22 = (short)(1) ;
         A64ustCode = BC000E6_A64ustCode[0] ;
         A65ustDesc = BC000E6_A65ustDesc[0] ;
         n65ustDesc = BC000E6_n65ustDesc[0] ;
         Z64ustCode = A64ustCode ;
         sMode22 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load0E22( ) ;
         Gx_mode = sMode22 ;
      }
      else
      {
         RcdFound22 = (short)(0) ;
         initializeNonKey0E22( ) ;
         sMode22 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode22 ;
      }
      pr_default.close(4);
   }

   public void getEqualNoModal( )
   {
      getKey0E22( ) ;
      if ( ( RcdFound22 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_0E0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency0E22( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC000E5 */
         pr_default.execute(3, new Object[] {A64ustCode});
         if ( ! (pr_default.getStatus(3) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"USERPROFILES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(3) == 101) || ( GXutil.strcmp(Z65ustDesc, BC000E5_A65ustDesc[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"USERPROFILES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0E22( )
   {
      beforeValidate0E22( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0E22( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0E22( 0) ;
         checkOptimisticConcurrency0E22( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0E22( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0E22( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000E9 */
                  pr_default.execute(7, new Object[] {A64ustCode, new Boolean(n65ustDesc), A65ustDesc});
                  if ( (pr_default.getStatus(7) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel0E22( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0E22( ) ;
         }
         endLevel0E22( ) ;
      }
      closeExtendedTableCursors0E22( ) ;
   }

   public void update0E22( )
   {
      beforeValidate0E22( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0E22( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0E22( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0E22( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0E22( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000E10 */
                  pr_default.execute(8, new Object[] {new Boolean(n65ustDesc), A65ustDesc, A64ustCode});
                  deferredUpdate0E22( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel0E22( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           getByPrimaryKey( ) ;
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0E22( ) ;
      }
      closeExtendedTableCursors0E22( ) ;
   }

   public void deferredUpdate0E22( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0E22( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0E22( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0E22( ) ;
         scanStart0E47( ) ;
         while ( ( RcdFound47 != 0 ) )
         {
            getByPrimaryKey0E47( ) ;
            delete0E47( ) ;
            scanNext0E47( ) ;
         }
         scanEnd0E47( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0E22( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete0E22( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000E11 */
                  pr_default.execute(9, new Object[] {A64ustCode});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      sMode22 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0E22( ) ;
      Gx_mode = sMode22 ;
   }

   public void onDeleteControls0E22( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC000E12 */
         pr_default.execute(10, new Object[] {A64ustCode});
         if ( (pr_default.getStatus(10) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Profiles"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(10);
      }
   }

   public void processNestedLevel0E47( )
   {
      nGXsfl_47_idx = (short)(0) ;
      while ( ( nGXsfl_47_idx < bcUserProfiles.getgxTv_SdtUserProfiles_Functions().size() ) )
      {
         readRow0E47( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound47 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_47 != 0 ) )
         {
            standaloneNotModal0E47( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert0E47( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete0E47( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update0E47( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_47 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_47 = (short)(1) ;
               Gxremove47 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_47 = (short)(0) ;
               Gxremove47 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcUserProfiles.getgxTv_SdtUserProfiles_Functions().removeElement(nGXsfl_47_idx);
            nGXsfl_47_idx = (short)(nGXsfl_47_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow47( ((SdtUserProfiles_Level1Item)bcUserProfiles.getgxTv_SdtUserProfiles_Functions().elementAt(-1+nGXsfl_47_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll0E47( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processLevel0E22( )
   {
      /* Save parent mode. */
      sMode22 = Gx_mode ;
      processNestedLevel0E47( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode22 ;
      /* ' Update level parameters */
   }

   public void endLevel0E22( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(3);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete0E22( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues0E0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0E22( )
   {
      /* Using cursor BC000E13 */
      pr_default.execute(11, new Object[] {A64ustCode});
      RcdFound22 = (short)(0) ;
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound22 = (short)(1) ;
         A64ustCode = BC000E13_A64ustCode[0] ;
         A65ustDesc = BC000E13_A65ustDesc[0] ;
         n65ustDesc = BC000E13_n65ustDesc[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0E22( )
   {
      pr_default.readNext(11);
      RcdFound22 = (short)(0) ;
      scanLoad0E22( ) ;
   }

   public void scanLoad0E22( )
   {
      sMode22 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound22 = (short)(1) ;
         A64ustCode = BC000E13_A64ustCode[0] ;
         A65ustDesc = BC000E13_A65ustDesc[0] ;
         n65ustDesc = BC000E13_n65ustDesc[0] ;
      }
      Gx_mode = sMode22 ;
   }

   public void scanEnd0E22( )
   {
      pr_default.close(11);
   }

   public void afterConfirm0E22( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0E22( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0E22( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0E22( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0E22( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0E22( )
   {
      /* Before Validate Rules */
   }

   public void addRow0E22( )
   {
      VarsToRow22( bcUserProfiles) ;
   }

   public void sendRow0E22( )
   {
   }

   public void readRow0E22( )
   {
      RowToVars22( bcUserProfiles, 0) ;
   }

   public void zm0E47( int GX_JID )
   {
      if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
      {
         Z65ustDesc = A65ustDesc ;
      }
      if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
      {
         Z51uspDesc = A51uspDesc ;
         Z65ustDesc = A65ustDesc ;
      }
      if ( ( GX_JID == -3 ) )
      {
         Z64ustCode = A64ustCode ;
         Z47uspCode = A47uspCode ;
      }
   }

   public void standaloneNotModal0E47( )
   {
   }

   public void standaloneModal0E47( )
   {
   }

   public void load0E47( )
   {
      /* Using cursor BC000E14 */
      pr_default.execute(12, new Object[] {A64ustCode, A47uspCode});
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound47 = (short)(1) ;
         A51uspDesc = BC000E14_A51uspDesc[0] ;
         zm0E47( -3) ;
      }
      pr_default.close(12);
      onLoadActions0E47( ) ;
   }

   public void onLoadActions0E47( )
   {
   }

   public void checkExtendedTable0E47( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal0E47( ) ;
      Gx_BScreen = (byte)(0) ;
      /* Using cursor BC000E4 */
      pr_default.execute(2, new Object[] {A47uspCode});
      if ( (pr_default.getStatus(2) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'User Functions'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A51uspDesc = BC000E4_A51uspDesc[0] ;
      pr_default.close(2);
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors0E47( )
   {
      pr_default.close(2);
   }

   public void enableDisable0E47( )
   {
   }

   public void getKey0E47( )
   {
      /* Using cursor BC000E15 */
      pr_default.execute(13, new Object[] {A64ustCode, A47uspCode});
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound47 = (short)(1) ;
      }
      else
      {
         RcdFound47 = (short)(0) ;
      }
      pr_default.close(13);
   }

   public void getByPrimaryKey0E47( )
   {
      /* Using cursor BC000E3 */
      pr_default.execute(1, new Object[] {A64ustCode, A47uspCode});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0E47( 3) ;
         RcdFound47 = (short)(1) ;
         initializeNonKey0E47( ) ;
         A47uspCode = BC000E3_A47uspCode[0] ;
         Z64ustCode = A64ustCode ;
         Z47uspCode = A47uspCode ;
         sMode47 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0E47( ) ;
         load0E47( ) ;
         Gx_mode = sMode47 ;
      }
      else
      {
         RcdFound47 = (short)(0) ;
         initializeNonKey0E47( ) ;
         sMode47 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal0E47( ) ;
         Gx_mode = sMode47 ;
      }
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency0E47( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC000E2 */
         pr_default.execute(0, new Object[] {A64ustCode, A47uspCode});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"USERPROFILESLEVEL1"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"USERPROFILESLEVEL1"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0E47( )
   {
      beforeValidate0E47( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0E47( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0E47( 0) ;
         checkOptimisticConcurrency0E47( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0E47( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0E47( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000E16 */
                  pr_default.execute(14, new Object[] {A64ustCode, A47uspCode});
                  if ( (pr_default.getStatus(14) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0E47( ) ;
         }
         endLevel0E47( ) ;
      }
      closeExtendedTableCursors0E47( ) ;
   }

   public void update0E47( )
   {
      beforeValidate0E47( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0E47( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0E47( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0E47( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0E47( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* No attributes to update on table [USERPROFILESLEVEL1] */
                  deferredUpdate0E47( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey0E47( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0E47( ) ;
      }
      closeExtendedTableCursors0E47( ) ;
   }

   public void deferredUpdate0E47( )
   {
   }

   public void delete0E47( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0E47( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0E47( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0E47( ) ;
         /* No cascading delete specified. */
         afterConfirm0E47( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0E47( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000E17 */
               pr_default.execute(15, new Object[] {A64ustCode, A47uspCode});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode47 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0E47( ) ;
      Gx_mode = sMode47 ;
   }

   public void onDeleteControls0E47( )
   {
      standaloneModal0E47( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor BC000E18 */
         pr_default.execute(16, new Object[] {A47uspCode});
         A51uspDesc = BC000E18_A51uspDesc[0] ;
         pr_default.close(16);
      }
   }

   public void endLevel0E47( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0E47( )
   {
      /* Using cursor BC000E19 */
      pr_default.execute(17, new Object[] {A64ustCode});
      RcdFound47 = (short)(0) ;
      if ( (pr_default.getStatus(17) != 101) )
      {
         RcdFound47 = (short)(1) ;
         A51uspDesc = BC000E19_A51uspDesc[0] ;
         A47uspCode = BC000E19_A47uspCode[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0E47( )
   {
      pr_default.readNext(17);
      RcdFound47 = (short)(0) ;
      scanLoad0E47( ) ;
   }

   public void scanLoad0E47( )
   {
      sMode47 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(17) != 101) )
      {
         RcdFound47 = (short)(1) ;
         A51uspDesc = BC000E19_A51uspDesc[0] ;
         A47uspCode = BC000E19_A47uspCode[0] ;
      }
      Gx_mode = sMode47 ;
   }

   public void scanEnd0E47( )
   {
      pr_default.close(17);
   }

   public void afterConfirm0E47( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0E47( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0E47( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0E47( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0E47( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0E47( )
   {
      /* Before Validate Rules */
   }

   public void addRow0E47( )
   {
      SdtUserProfiles_Level1Item obj47 ;
      obj47 = new SdtUserProfiles_Level1Item(remoteHandle);
      VarsToRow47( obj47) ;
      bcUserProfiles.getgxTv_SdtUserProfiles_Functions().add(obj47, 0);
      obj47.setgxTv_SdtUserProfiles_Level1Item_Mode( "UPD" );
      obj47.setgxTv_SdtUserProfiles_Level1Item_Modified( (short)(0) );
   }

   public void sendRow0E47( )
   {
   }

   public void readRow0E47( )
   {
      nGXsfl_47_idx = (short)(nGXsfl_47_idx+1) ;
      RowToVars47( ((SdtUserProfiles_Level1Item)bcUserProfiles.getgxTv_SdtUserProfiles_Functions().elementAt(-1+nGXsfl_47_idx)), 0) ;
   }

   public void confirmValues0E0( )
   {
   }

   public void initializeNonKey0E22( )
   {
      A65ustDesc = "" ;
      n65ustDesc = false ;
   }

   public void initAll0E22( )
   {
      A64ustCode = "" ;
      initializeNonKey0E22( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void initializeNonKey0E47( )
   {
      A51uspDesc = "" ;
   }

   public void initAll0E47( )
   {
      A47uspCode = "" ;
      initializeNonKey0E47( ) ;
   }

   public void standaloneModalInsert0E47( )
   {
   }

   public void VarsToRow22( SdtUserProfiles obj22 )
   {
      obj22.setgxTv_SdtUserProfiles_Mode( Gx_mode );
      obj22.setgxTv_SdtUserProfiles_Ustdescription( A65ustDesc );
      obj22.setgxTv_SdtUserProfiles_Ustcode( A64ustCode );
      obj22.setgxTv_SdtUserProfiles_Ustcode_Z( Z64ustCode );
      obj22.setgxTv_SdtUserProfiles_Ustdescription_Z( Z65ustDesc );
      obj22.setgxTv_SdtUserProfiles_Ustdescription_N( (byte)((byte)((n65ustDesc)?1:0)) );
      obj22.setgxTv_SdtUserProfiles_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars22( SdtUserProfiles obj22 ,
                            int forceLoad )
   {
      Gx_mode = obj22.getgxTv_SdtUserProfiles_Mode() ;
      A65ustDesc = obj22.getgxTv_SdtUserProfiles_Ustdescription() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A64ustCode = obj22.getgxTv_SdtUserProfiles_Ustcode() ;
      }
      Z64ustCode = obj22.getgxTv_SdtUserProfiles_Ustcode_Z() ;
      Z65ustDesc = obj22.getgxTv_SdtUserProfiles_Ustdescription_Z() ;
      n65ustDesc = (boolean)((obj22.getgxTv_SdtUserProfiles_Ustdescription_N()==0)?false:true) ;
      Gx_mode = obj22.getgxTv_SdtUserProfiles_Mode() ;
      return  ;
   }

   public void VarsToRow47( SdtUserProfiles_Level1Item obj47 )
   {
      obj47.setgxTv_SdtUserProfiles_Level1Item_Mode( Gx_mode );
      obj47.setgxTv_SdtUserProfiles_Level1Item_Uspdescription( A51uspDesc );
      obj47.setgxTv_SdtUserProfiles_Level1Item_Uspcode( A47uspCode );
      obj47.setgxTv_SdtUserProfiles_Level1Item_Uspcode_Z( Z47uspCode );
      obj47.setgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z( Z51uspDesc );
      obj47.setgxTv_SdtUserProfiles_Level1Item_Modified( nIsMod_47 );
      return  ;
   }

   public void RowToVars47( SdtUserProfiles_Level1Item obj47 ,
                            int forceLoad )
   {
      Gx_mode = obj47.getgxTv_SdtUserProfiles_Level1Item_Mode() ;
      A51uspDesc = obj47.getgxTv_SdtUserProfiles_Level1Item_Uspdescription() ;
      A47uspCode = obj47.getgxTv_SdtUserProfiles_Level1Item_Uspcode() ;
      Z47uspCode = obj47.getgxTv_SdtUserProfiles_Level1Item_Uspcode_Z() ;
      Z51uspDesc = obj47.getgxTv_SdtUserProfiles_Level1Item_Uspdescription_Z() ;
      nIsMod_47 = obj47.getgxTv_SdtUserProfiles_Level1Item_Modified() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A64ustCode = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey0E22( ) ;
      scanStart0E22( ) ;
      if ( ( RcdFound22 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z64ustCode = A64ustCode ;
      }
      onLoadActions0E22( ) ;
      zm0E22( 0) ;
      addRow0E22( ) ;
      bcUserProfiles.getgxTv_SdtUserProfiles_Functions().clearCollection();
      if ( ( RcdFound22 == 1 ) )
      {
         scanStart0E47( ) ;
         nGXsfl_47_idx = (short)(1) ;
         while ( ( RcdFound47 != 0 ) )
         {
            onLoadActions0E47( ) ;
            Z64ustCode = A64ustCode ;
            Z47uspCode = A47uspCode ;
            zm0E47( 0) ;
            nRcdExists_47 = (short)(1) ;
            nIsMod_47 = (short)(0) ;
            addRow0E47( ) ;
            nGXsfl_47_idx = (short)(nGXsfl_47_idx+1) ;
            scanNext0E47( ) ;
         }
         scanEnd0E47( ) ;
      }
      scanEnd0E22( ) ;
      if ( ( RcdFound22 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars22( bcUserProfiles, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey0E22( ) ;
      if ( ( RcdFound22 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A64ustCode, Z64ustCode) != 0 ) )
         {
            A64ustCode = Z64ustCode ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update0E22( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A64ustCode, Z64ustCode) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert0E22( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert0E22( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow22( bcUserProfiles) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars22( bcUserProfiles, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey0E22( ) ;
      if ( ( RcdFound22 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A64ustCode, Z64ustCode) != 0 ) )
         {
            A64ustCode = Z64ustCode ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A64ustCode, Z64ustCode) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      pr_default.close(16);
      Application.rollback(context, remoteHandle, "DEFAULT", "tuserprofiles_bc");
      VarsToRow22( bcUserProfiles) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcUserProfiles.getgxTv_SdtUserProfiles_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcUserProfiles.setgxTv_SdtUserProfiles_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtUserProfiles sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcUserProfiles ) )
      {
         bcUserProfiles = sdt ;
         if ( ( GXutil.strcmp(bcUserProfiles.getgxTv_SdtUserProfiles_Mode(), "") == 0 ) )
         {
            bcUserProfiles.setgxTv_SdtUserProfiles_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow22( bcUserProfiles) ;
         }
         else
         {
            RowToVars22( bcUserProfiles, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcUserProfiles.getgxTv_SdtUserProfiles_Mode(), "") == 0 ) )
         {
            bcUserProfiles.setgxTv_SdtUserProfiles_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars22( bcUserProfiles, 1) ;
      return  ;
   }

   public SdtUserProfiles getUserProfiles_BC( )
   {
      return bcUserProfiles ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(16);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z64ustCode = "" ;
      A64ustCode = "" ;
      sMode22 = "" ;
      nIsMod_47 = (short)(0) ;
      RcdFound47 = (short)(0) ;
      gxTv_SdtUserProfiles_Ustcode_Z = "" ;
      gxTv_SdtUserProfiles_Ustdescription_Z = "" ;
      gxTv_SdtUserProfiles_Ustdescription_N = (byte)(0) ;
      gxTv_SdtUserProfiles_Level1Item_Uspcode_Z = "" ;
      gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z = "" ;
      GX_JID = 0 ;
      Z65ustDesc = "" ;
      A65ustDesc = "" ;
      Z47uspCode = "" ;
      A47uspCode = "" ;
      Z51uspDesc = "" ;
      A51uspDesc = "" ;
      BC000E7_A64ustCode = new String[] {""} ;
      BC000E7_A65ustDesc = new String[] {""} ;
      BC000E7_n65ustDesc = new boolean[] {false} ;
      RcdFound22 = (short)(0) ;
      n65ustDesc = false ;
      BC000E8_A64ustCode = new String[] {""} ;
      BC000E6_A64ustCode = new String[] {""} ;
      BC000E6_A65ustDesc = new String[] {""} ;
      BC000E6_n65ustDesc = new boolean[] {false} ;
      BC000E5_A64ustCode = new String[] {""} ;
      BC000E5_A65ustDesc = new String[] {""} ;
      BC000E5_n65ustDesc = new boolean[] {false} ;
      BC000E12_A30lgnLogi = new String[] {""} ;
      BC000E12_A64ustCode = new String[] {""} ;
      nRcdExists_47 = (short)(0) ;
      Gxremove47 = (byte)(0) ;
      BC000E13_A64ustCode = new String[] {""} ;
      BC000E13_A65ustDesc = new String[] {""} ;
      BC000E13_n65ustDesc = new boolean[] {false} ;
      BC000E14_A64ustCode = new String[] {""} ;
      BC000E14_A51uspDesc = new String[] {""} ;
      BC000E14_A47uspCode = new String[] {""} ;
      Gx_BScreen = (byte)(0) ;
      BC000E4_A51uspDesc = new String[] {""} ;
      BC000E15_A64ustCode = new String[] {""} ;
      BC000E15_A47uspCode = new String[] {""} ;
      BC000E3_A64ustCode = new String[] {""} ;
      BC000E3_A47uspCode = new String[] {""} ;
      sMode47 = "" ;
      BC000E2_A64ustCode = new String[] {""} ;
      BC000E2_A47uspCode = new String[] {""} ;
      BC000E18_A51uspDesc = new String[] {""} ;
      BC000E19_A64ustCode = new String[] {""} ;
      BC000E19_A51uspDesc = new String[] {""} ;
      BC000E19_A47uspCode = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tuserprofiles_bc__default(),
         new Object[] {
             new Object[] {
            BC000E2_A64ustCode, BC000E2_A47uspCode
            }
            , new Object[] {
            BC000E3_A64ustCode, BC000E3_A47uspCode
            }
            , new Object[] {
            BC000E4_A51uspDesc
            }
            , new Object[] {
            BC000E5_A64ustCode, BC000E5_A65ustDesc, BC000E5_n65ustDesc
            }
            , new Object[] {
            BC000E6_A64ustCode, BC000E6_A65ustDesc, BC000E6_n65ustDesc
            }
            , new Object[] {
            BC000E7_A64ustCode, BC000E7_A65ustDesc, BC000E7_n65ustDesc
            }
            , new Object[] {
            BC000E8_A64ustCode
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000E12_A30lgnLogi, BC000E12_A64ustCode
            }
            , new Object[] {
            BC000E13_A64ustCode, BC000E13_A65ustDesc, BC000E13_n65ustDesc
            }
            , new Object[] {
            BC000E14_A64ustCode, BC000E14_A51uspDesc, BC000E14_A47uspCode
            }
            , new Object[] {
            BC000E15_A64ustCode, BC000E15_A47uspCode
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000E18_A51uspDesc
            }
            , new Object[] {
            BC000E19_A64ustCode, BC000E19_A51uspDesc, BC000E19_A47uspCode
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private byte gxTv_SdtUserProfiles_Ustdescription_N ;
   private byte Gxremove47 ;
   private byte Gx_BScreen ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short nGXsfl_47_idx=1 ;
   private short nIsMod_47 ;
   private short RcdFound47 ;
   private short RcdFound22 ;
   private short nRcdExists_47 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode22 ;
   private String sMode47 ;
   private boolean n65ustDesc ;
   private String Z64ustCode ;
   private String A64ustCode ;
   private String gxTv_SdtUserProfiles_Ustcode_Z ;
   private String gxTv_SdtUserProfiles_Ustdescription_Z ;
   private String gxTv_SdtUserProfiles_Level1Item_Uspcode_Z ;
   private String gxTv_SdtUserProfiles_Level1Item_Uspdescription_Z ;
   private String Z65ustDesc ;
   private String A65ustDesc ;
   private String Z47uspCode ;
   private String A47uspCode ;
   private String Z51uspDesc ;
   private String A51uspDesc ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtUserProfiles bcUserProfiles ;
   private IDataStoreProvider pr_default ;
   private String[] BC000E7_A64ustCode ;
   private String[] BC000E7_A65ustDesc ;
   private boolean[] BC000E7_n65ustDesc ;
   private String[] BC000E8_A64ustCode ;
   private String[] BC000E6_A64ustCode ;
   private String[] BC000E6_A65ustDesc ;
   private boolean[] BC000E6_n65ustDesc ;
   private String[] BC000E5_A64ustCode ;
   private String[] BC000E5_A65ustDesc ;
   private boolean[] BC000E5_n65ustDesc ;
   private String[] BC000E12_A30lgnLogi ;
   private String[] BC000E12_A64ustCode ;
   private String[] BC000E13_A64ustCode ;
   private String[] BC000E13_A65ustDesc ;
   private boolean[] BC000E13_n65ustDesc ;
   private String[] BC000E14_A64ustCode ;
   private String[] BC000E14_A51uspDesc ;
   private String[] BC000E14_A47uspCode ;
   private String[] BC000E4_A51uspDesc ;
   private String[] BC000E15_A64ustCode ;
   private String[] BC000E15_A47uspCode ;
   private String[] BC000E3_A64ustCode ;
   private String[] BC000E3_A47uspCode ;
   private String[] BC000E2_A64ustCode ;
   private String[] BC000E2_A47uspCode ;
   private String[] BC000E18_A51uspDesc ;
   private String[] BC000E19_A64ustCode ;
   private String[] BC000E19_A51uspDesc ;
   private String[] BC000E19_A47uspCode ;
}

final  class tuserprofiles_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC000E2", "SELECT [ustCode], [uspCode] FROM [USERPROFILESLEVEL1] WITH (UPDLOCK) WHERE [ustCode] = ? AND [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E3", "SELECT [ustCode], [uspCode] FROM [USERPROFILESLEVEL1] WITH (NOLOCK) WHERE [ustCode] = ? AND [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E4", "SELECT [uspDescription] FROM [USERFUNCTIONS] WITH (NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E5", "SELECT [ustCode], [ustDescription] FROM [USERPROFILES] WITH (UPDLOCK) WHERE [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E6", "SELECT [ustCode], [ustDescription] FROM [USERPROFILES] WITH (NOLOCK) WHERE [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E7", "SELECT TM1.[ustCode], TM1.[ustDescription] FROM [USERPROFILES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ustCode] = ? ORDER BY TM1.[ustCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E8", "SELECT [ustCode] FROM [USERPROFILES] WITH (FASTFIRSTROW NOLOCK) WHERE [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000E9", "INSERT INTO [USERPROFILES] ([ustCode], [ustDescription]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000E10", "UPDATE [USERPROFILES] SET [ustDescription]=?  WHERE [ustCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000E11", "DELETE FROM [USERPROFILES]  WHERE [ustCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000E12", "SELECT TOP 1 [lgnLogin], [ustCode] FROM [LOGINSPROFILES] WITH (NOLOCK) WHERE [ustCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000E13", "SELECT TM1.[ustCode], TM1.[ustDescription] FROM [USERPROFILES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ustCode] = ? ORDER BY TM1.[ustCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E14", "SELECT T1.[ustCode], T2.[uspDescription], T1.[uspCode] FROM ([USERPROFILESLEVEL1] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [USERFUNCTIONS] T2 WITH (NOLOCK) ON T2.[uspCode] = T1.[uspCode]) WHERE T1.[ustCode] = ? and T1.[uspCode] = ? ORDER BY T1.[ustCode], T1.[uspCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E15", "SELECT [ustCode], [uspCode] FROM [USERPROFILESLEVEL1] WITH (FASTFIRSTROW NOLOCK) WHERE [ustCode] = ? AND [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000E16", "INSERT INTO [USERPROFILESLEVEL1] ([ustCode], [uspCode]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000E17", "DELETE FROM [USERPROFILESLEVEL1]  WHERE [ustCode] = ? AND [uspCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000E18", "SELECT [uspDescription] FROM [USERFUNCTIONS] WITH (NOLOCK) WHERE [uspCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000E19", "SELECT T1.[ustCode], T2.[uspDescription], T1.[uspCode] FROM ([USERPROFILESLEVEL1] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [USERFUNCTIONS] T2 WITH (NOLOCK) ON T2.[uspCode] = T1.[uspCode]) WHERE T1.[ustCode] = ? ORDER BY T1.[ustCode], T1.[uspCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 40);
               }
               break;
            case 8 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 40);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 11 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 12 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 13 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 14 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 15 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
            case 16 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 17 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


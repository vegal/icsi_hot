/*
               File: tperiods_bc
        Description: Period Configuration
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:10.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tperiods_bc extends GXWebPanel implements IGxSilentTrn
{
   public tperiods_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tperiods_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tperiods_bc.class ));
   }

   public tperiods_bc( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         /* Execute user event: e11292 */
         e11292 ();
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z23ISOCod = A23ISOCod ;
            Z349CurCod = A349CurCod ;
            Z387Period = A387Period ;
            Z263PerID = A263PerID ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_290( )
   {
      beforeValidate29224( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls29224( ) ;
         }
         else
         {
            checkExtendedTable29224( ) ;
            if ( ( AnyError == 0 ) )
            {
               zm29224( 31) ;
               zm29224( 32) ;
               zm29224( 33) ;
            }
            closeExtendedTableCursors29224( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues290( ) ;
      }
   }

   public void e12292( )
   {
      /* Start Routine */
      AV12lgnLog = AV11Sessio.getValue("LOGGED") ;
      if ( ( GXutil.strcmp(AV12lgnLog, "") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hlogin") );
      }
      GXt_int1 = AV19Acessa ;
      GXv_svchar2[0] = "GeneralManager" ;
      GXv_svchar3[0] = "User can access All Countries in Purgatory" ;
      GXv_char4[0] = "" ;
      GXv_int5[0] = GXt_int1 ;
      new pcheckprofile(remoteHandle, context).execute( GXv_svchar2, GXv_svchar3, GXv_char4, GXv_int5) ;
      tperiods_bc.this.GXt_int1 = GXv_int5[0] ;
      AV19Acessa = GXt_int1 ;
   }

   public void e13292( )
   {
      /* 'Back' Routine */
      httpContext.setLinkToRedirect(formatLink("hperiods") );
   }

   public void e11292( )
   {
      /* After Trn Routine */
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hperiods") );
      }
   }

   public void zm29224( int GX_JID )
   {
      if ( ( GX_JID == 30 ) || ( GX_JID == 0 ) )
      {
         Z1113PerDa = A1113PerDa ;
         Z1114PerDa = A1114PerDa ;
         Z1105PerDe = A1105PerDe ;
         Z1106PerDa = A1106PerDa ;
         Z1107PerDa = A1107PerDa ;
         Z1108PerDa = A1108PerDa ;
         Z1109PerDa = A1109PerDa ;
         Z1110PerDa = A1110PerDa ;
         Z1111PerDa = A1111PerDa ;
         Z1112PerCl = A1112PerCl ;
         Z1115PerID = A1115PerID ;
      }
      if ( ( GX_JID == 31 ) || ( GX_JID == 0 ) )
      {
         Z20ISODes = A20ISODes ;
      }
      if ( ( GX_JID == 32 ) || ( GX_JID == 0 ) )
      {
         Z351CurDes = A351CurDes ;
      }
      if ( ( GX_JID == 33 ) || ( GX_JID == 0 ) )
      {
      }
      if ( ( GX_JID == -30 ) )
      {
         Z263PerID = A263PerID ;
         Z1113PerDa = A1113PerDa ;
         Z1114PerDa = A1114PerDa ;
         Z1105PerDe = A1105PerDe ;
         Z1106PerDa = A1106PerDa ;
         Z1107PerDa = A1107PerDa ;
         Z1108PerDa = A1108PerDa ;
         Z1109PerDa = A1109PerDa ;
         Z1110PerDa = A1110PerDa ;
         Z1111PerDa = A1111PerDa ;
         Z1112PerCl = A1112PerCl ;
         Z1115PerID = A1115PerID ;
         Z23ISOCod = A23ISOCod ;
         Z349CurCod = A349CurCod ;
         Z387Period = A387Period ;
      }
   }

   public void standaloneNotModal( )
   {
      A23ISOCod_Enabled = 0 ;
   }

   public void standaloneModal( )
   {
   }

   public void load29224( )
   {
      /* Using cursor BC00297 */
      pr_default.execute(5, new Object[] {A23ISOCod, A349CurCod, A387Period, A263PerID});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound224 = (short)(1) ;
         A1113PerDa = BC00297_A1113PerDa[0] ;
         A1114PerDa = BC00297_A1114PerDa[0] ;
         A20ISODes = BC00297_A20ISODes[0] ;
         n20ISODes = BC00297_n20ISODes[0] ;
         A351CurDes = BC00297_A351CurDes[0] ;
         n351CurDes = BC00297_n351CurDes[0] ;
         A1105PerDe = BC00297_A1105PerDe[0] ;
         A1106PerDa = BC00297_A1106PerDa[0] ;
         A1107PerDa = BC00297_A1107PerDa[0] ;
         A1108PerDa = BC00297_A1108PerDa[0] ;
         n1108PerDa = BC00297_n1108PerDa[0] ;
         A1109PerDa = BC00297_A1109PerDa[0] ;
         n1109PerDa = BC00297_n1109PerDa[0] ;
         A1110PerDa = BC00297_A1110PerDa[0] ;
         A1111PerDa = BC00297_A1111PerDa[0] ;
         A1112PerCl = BC00297_A1112PerCl[0] ;
         A1115PerID = BC00297_A1115PerID[0] ;
         zm29224( -30) ;
      }
      pr_default.close(5);
      onLoadActions29224( ) ;
   }

   public void onLoadActions29224( )
   {
   }

   public void checkExtendedTable29224( )
   {
      standaloneModal( ) ;
      /* Using cursor BC00294 */
      pr_default.execute(2, new Object[] {A23ISOCod});
      if ( (pr_default.getStatus(2) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A20ISODes = BC00294_A20ISODes[0] ;
      n20ISODes = BC00294_n20ISODes[0] ;
      pr_default.close(2);
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A23ISOCod), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Country can not be null", 1);
         AnyError = (short)(1) ;
      }
      /* Using cursor BC00295 */
      pr_default.execute(3, new Object[] {A23ISOCod, A349CurCod});
      if ( (pr_default.getStatus(3) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Currencies'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A351CurDes = BC00295_A351CurDes[0] ;
      n351CurDes = BC00295_n351CurDes[0] ;
      pr_default.close(3);
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A349CurCod), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Currency can not be null", 1);
         AnyError = (short)(1) ;
      }
      /* Using cursor BC00296 */
      pr_default.execute(4, new Object[] {A387Period});
      if ( (pr_default.getStatus(4) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'Period Types'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      pr_default.close(4);
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A387Period), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Period Types can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A263PerID), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("ID can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A263PerID), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Period ID can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A1105PerDe), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Description can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1106PerDa)) || (( A1106PerDa.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A1106PerDa.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Sales Initial Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && (GXutil.nullDate().equals(A1106PerDa)) )
      {
         httpContext.GX_msglist.addItem("Sales Initial Date can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1107PerDa)) || (( A1107PerDa.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A1107PerDa.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Sales End Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && (GXutil.nullDate().equals(A1107PerDa)) )
      {
         httpContext.GX_msglist.addItem("Sales End Date can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && A1106PerDa.after( A1107PerDa ) )
      {
         httpContext.GX_msglist.addItem("Sales End Date must be bigger than Sales Initial Date", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1108PerDa)) || (( A1108PerDa.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A1108PerDa.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Dispute Initial Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1109PerDa)) || (( A1109PerDa.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A1109PerDa.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Dispute End Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && A1108PerDa.after( A1109PerDa ) )
      {
         httpContext.GX_msglist.addItem("Dispute End Date must be bigger than Dispute Initial Date", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1110PerDa)) || (( A1110PerDa.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A1110PerDa.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Initial Due Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && (GXutil.nullDate().equals(A1110PerDa)) )
      {
         httpContext.GX_msglist.addItem("Initial Due Date can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1111PerDa)) || (( A1111PerDa.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A1111PerDa.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Final Due Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && (GXutil.nullDate().equals(A1111PerDa)) )
      {
         httpContext.GX_msglist.addItem("Final Due Date can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && A1110PerDa.after( A1111PerDa ) )
      {
         httpContext.GX_msglist.addItem("Due End Date must be bigger than Initial Due Date", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1113PerDa)) || (( A1113PerDa.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1113PerDa.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Record Creation Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A1114PerDa)) || (( A1114PerDa.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A1114PerDa.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Record Modification Date is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A1115PerID), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("ID Period Administration can not be null", 1);
         AnyError = (short)(1) ;
      }
   }

   public void closeExtendedTableCursors29224( )
   {
      pr_default.close(2);
      pr_default.close(3);
      pr_default.close(4);
   }

   public void enableDisable( )
   {
   }

   public void getKey29224( )
   {
      /* Using cursor BC00298 */
      pr_default.execute(6, new Object[] {A23ISOCod, A349CurCod, A387Period, A263PerID});
      if ( (pr_default.getStatus(6) != 101) )
      {
         RcdFound224 = (short)(1) ;
      }
      else
      {
         RcdFound224 = (short)(0) ;
      }
      pr_default.close(6);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC00293 */
      pr_default.execute(1, new Object[] {A23ISOCod, A349CurCod, A387Period, A263PerID});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm29224( 30) ;
         RcdFound224 = (short)(1) ;
         A263PerID = BC00293_A263PerID[0] ;
         A1113PerDa = BC00293_A1113PerDa[0] ;
         A1114PerDa = BC00293_A1114PerDa[0] ;
         A1105PerDe = BC00293_A1105PerDe[0] ;
         A1106PerDa = BC00293_A1106PerDa[0] ;
         A1107PerDa = BC00293_A1107PerDa[0] ;
         A1108PerDa = BC00293_A1108PerDa[0] ;
         n1108PerDa = BC00293_n1108PerDa[0] ;
         A1109PerDa = BC00293_A1109PerDa[0] ;
         n1109PerDa = BC00293_n1109PerDa[0] ;
         A1110PerDa = BC00293_A1110PerDa[0] ;
         A1111PerDa = BC00293_A1111PerDa[0] ;
         A1112PerCl = BC00293_A1112PerCl[0] ;
         A1115PerID = BC00293_A1115PerID[0] ;
         A23ISOCod = BC00293_A23ISOCod[0] ;
         A349CurCod = BC00293_A349CurCod[0] ;
         A387Period = BC00293_A387Period[0] ;
         Z23ISOCod = A23ISOCod ;
         Z349CurCod = A349CurCod ;
         Z387Period = A387Period ;
         Z263PerID = A263PerID ;
         sMode224 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load29224( ) ;
         Gx_mode = sMode224 ;
      }
      else
      {
         RcdFound224 = (short)(0) ;
         initializeNonKey29224( ) ;
         sMode224 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode224 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey29224( ) ;
      if ( ( RcdFound224 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_290( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency29224( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00292 */
         pr_default.execute(0, new Object[] {A23ISOCod, A349CurCod, A387Period, A263PerID});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"PERIODS"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         Gx_longc = false ;
         if ( (pr_default.getStatus(0) == 101) || !( Z1113PerDa.equals( BC00292_A1113PerDa[0] ) ) || !( Z1114PerDa.equals( BC00292_A1114PerDa[0] ) ) || ( GXutil.strcmp(Z1105PerDe, BC00292_A1105PerDe[0]) != 0 ) || !( Z1106PerDa.equals( BC00292_A1106PerDa[0] ) ) || !( Z1107PerDa.equals( BC00292_A1107PerDa[0] ) ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || !( Z1108PerDa.equals( BC00292_A1108PerDa[0] ) ) || !( Z1109PerDa.equals( BC00292_A1109PerDa[0] ) ) || !( Z1110PerDa.equals( BC00292_A1110PerDa[0] ) ) || !( Z1111PerDa.equals( BC00292_A1111PerDa[0] ) ) || ( GXutil.strcmp(Z1112PerCl, BC00292_A1112PerCl[0]) != 0 ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || ( GXutil.strcmp(Z1115PerID, BC00292_A1115PerID[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"PERIODS"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert29224( )
   {
      beforeValidate29224( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable29224( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm29224( 0) ;
         checkOptimisticConcurrency29224( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm29224( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert29224( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00299 */
                  pr_default.execute(7, new Object[] {A263PerID, A1113PerDa, A1114PerDa, A1105PerDe, A1106PerDa, A1107PerDa, new Boolean(n1108PerDa), A1108PerDa, new Boolean(n1109PerDa), A1109PerDa, A1110PerDa, A1111PerDa, A1112PerCl, A1115PerID, A23ISOCod, A349CurCod, A387Period});
                  if ( (pr_default.getStatus(7) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load29224( ) ;
         }
         endLevel29224( ) ;
      }
      closeExtendedTableCursors29224( ) ;
   }

   public void update29224( )
   {
      beforeValidate29224( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable29224( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency29224( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm29224( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate29224( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002910 */
                  pr_default.execute(8, new Object[] {A1113PerDa, A1114PerDa, A1105PerDe, A1106PerDa, A1107PerDa, new Boolean(n1108PerDa), A1108PerDa, new Boolean(n1109PerDa), A1109PerDa, A1110PerDa, A1111PerDa, A1112PerCl, A1115PerID, A23ISOCod, A349CurCod, A387Period, A263PerID});
                  deferredUpdate29224( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel29224( ) ;
      }
      closeExtendedTableCursors29224( ) ;
   }

   public void deferredUpdate29224( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate29224( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency29224( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls29224( ) ;
         /* No cascading delete specified. */
         afterConfirm29224( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete29224( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC002911 */
               pr_default.execute(9, new Object[] {A23ISOCod, A349CurCod, A387Period, A263PerID});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode224 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel29224( ) ;
      Gx_mode = sMode224 ;
   }

   public void onDeleteControls29224( )
   {
      standaloneModal( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor BC002912 */
         pr_default.execute(10, new Object[] {A23ISOCod});
         A20ISODes = BC002912_A20ISODes[0] ;
         n20ISODes = BC002912_n20ISODes[0] ;
         pr_default.close(10);
         /* Using cursor BC002913 */
         pr_default.execute(11, new Object[] {A23ISOCod, A349CurCod});
         A351CurDes = BC002913_A351CurDes[0] ;
         n351CurDes = BC002913_n351CurDes[0] ;
         pr_default.close(11);
      }
   }

   public void endLevel29224( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete29224( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues290( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart29224( )
   {
      /* Using cursor BC002914 */
      pr_default.execute(12, new Object[] {A23ISOCod, A349CurCod, A387Period, A263PerID});
      RcdFound224 = (short)(0) ;
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound224 = (short)(1) ;
         A263PerID = BC002914_A263PerID[0] ;
         A1113PerDa = BC002914_A1113PerDa[0] ;
         A1114PerDa = BC002914_A1114PerDa[0] ;
         A20ISODes = BC002914_A20ISODes[0] ;
         n20ISODes = BC002914_n20ISODes[0] ;
         A351CurDes = BC002914_A351CurDes[0] ;
         n351CurDes = BC002914_n351CurDes[0] ;
         A1105PerDe = BC002914_A1105PerDe[0] ;
         A1106PerDa = BC002914_A1106PerDa[0] ;
         A1107PerDa = BC002914_A1107PerDa[0] ;
         A1108PerDa = BC002914_A1108PerDa[0] ;
         n1108PerDa = BC002914_n1108PerDa[0] ;
         A1109PerDa = BC002914_A1109PerDa[0] ;
         n1109PerDa = BC002914_n1109PerDa[0] ;
         A1110PerDa = BC002914_A1110PerDa[0] ;
         A1111PerDa = BC002914_A1111PerDa[0] ;
         A1112PerCl = BC002914_A1112PerCl[0] ;
         A1115PerID = BC002914_A1115PerID[0] ;
         A23ISOCod = BC002914_A23ISOCod[0] ;
         A349CurCod = BC002914_A349CurCod[0] ;
         A387Period = BC002914_A387Period[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext29224( )
   {
      pr_default.readNext(12);
      RcdFound224 = (short)(0) ;
      scanLoad29224( ) ;
   }

   public void scanLoad29224( )
   {
      sMode224 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound224 = (short)(1) ;
         A263PerID = BC002914_A263PerID[0] ;
         A1113PerDa = BC002914_A1113PerDa[0] ;
         A1114PerDa = BC002914_A1114PerDa[0] ;
         A20ISODes = BC002914_A20ISODes[0] ;
         n20ISODes = BC002914_n20ISODes[0] ;
         A351CurDes = BC002914_A351CurDes[0] ;
         n351CurDes = BC002914_n351CurDes[0] ;
         A1105PerDe = BC002914_A1105PerDe[0] ;
         A1106PerDa = BC002914_A1106PerDa[0] ;
         A1107PerDa = BC002914_A1107PerDa[0] ;
         A1108PerDa = BC002914_A1108PerDa[0] ;
         n1108PerDa = BC002914_n1108PerDa[0] ;
         A1109PerDa = BC002914_A1109PerDa[0] ;
         n1109PerDa = BC002914_n1109PerDa[0] ;
         A1110PerDa = BC002914_A1110PerDa[0] ;
         A1111PerDa = BC002914_A1111PerDa[0] ;
         A1112PerCl = BC002914_A1112PerCl[0] ;
         A1115PerID = BC002914_A1115PerID[0] ;
         A23ISOCod = BC002914_A23ISOCod[0] ;
         A349CurCod = BC002914_A349CurCod[0] ;
         A387Period = BC002914_A387Period[0] ;
      }
      Gx_mode = sMode224 ;
   }

   public void scanEnd29224( )
   {
      pr_default.close(12);
   }

   public void afterConfirm29224( )
   {
      /* After Confirm Rules */
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  && true /* After */ )
      {
         A1113PerDa = GXutil.now(true, false) ;
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  || ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  && true /* After */ )
      {
         A1114PerDa = GXutil.now(true, false) ;
      }
   }

   public void beforeInsert29224( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate29224( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete29224( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete29224( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate29224( )
   {
      /* Before Validate Rules */
   }

   public void addRow29224( )
   {
      VarsToRow224( bcPeriods) ;
   }

   public void sendRow29224( )
   {
   }

   public void readRow29224( )
   {
      RowToVars224( bcPeriods, 0) ;
   }

   public void confirmValues290( )
   {
   }

   public void initializeNonKey29224( )
   {
      A1113PerDa = GXutil.resetTime( GXutil.nullDate() );
      A1114PerDa = GXutil.resetTime( GXutil.nullDate() );
      A20ISODes = "" ;
      n20ISODes = false ;
      A351CurDes = "" ;
      n351CurDes = false ;
      A1105PerDe = "" ;
      A1106PerDa = GXutil.nullDate() ;
      A1107PerDa = GXutil.nullDate() ;
      A1108PerDa = GXutil.nullDate() ;
      n1108PerDa = false ;
      A1109PerDa = GXutil.nullDate() ;
      n1109PerDa = false ;
      A1110PerDa = GXutil.nullDate() ;
      A1111PerDa = GXutil.nullDate() ;
      A1112PerCl = "" ;
      A1115PerID = "" ;
   }

   public void initAll29224( )
   {
      A23ISOCod = "" ;
      A349CurCod = "" ;
      A387Period = "" ;
      A263PerID = "" ;
      initializeNonKey29224( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow224( SdtPeriods obj224 )
   {
      obj224.setgxTv_SdtPeriods_Mode( Gx_mode );
      obj224.setgxTv_SdtPeriods_Perdatins( A1113PerDa );
      obj224.setgxTv_SdtPeriods_Perdatupd( A1114PerDa );
      obj224.setgxTv_SdtPeriods_Isodes( A20ISODes );
      obj224.setgxTv_SdtPeriods_Curdescription( A351CurDes );
      obj224.setgxTv_SdtPeriods_Perdescr( A1105PerDe );
      obj224.setgxTv_SdtPeriods_Perdatini( A1106PerDa );
      obj224.setgxTv_SdtPeriods_Perdatend( A1107PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdisini( A1108PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdisfin( A1109PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdueini( A1110PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdueend( A1111PerDa );
      obj224.setgxTv_SdtPeriods_Perclose( A1112PerCl );
      obj224.setgxTv_SdtPeriods_Peridadm( A1115PerID );
      obj224.setgxTv_SdtPeriods_Isocod( A23ISOCod );
      obj224.setgxTv_SdtPeriods_Curcode( A349CurCod );
      obj224.setgxTv_SdtPeriods_Periodtypescode( A387Period );
      obj224.setgxTv_SdtPeriods_Perid( A263PerID );
      obj224.setgxTv_SdtPeriods_Isocod_Z( Z23ISOCod );
      obj224.setgxTv_SdtPeriods_Curcode_Z( Z349CurCod );
      obj224.setgxTv_SdtPeriods_Periodtypescode_Z( Z387Period );
      obj224.setgxTv_SdtPeriods_Perid_Z( Z263PerID );
      obj224.setgxTv_SdtPeriods_Isodes_Z( Z20ISODes );
      obj224.setgxTv_SdtPeriods_Curdescription_Z( Z351CurDes );
      obj224.setgxTv_SdtPeriods_Perdescr_Z( Z1105PerDe );
      obj224.setgxTv_SdtPeriods_Perdatini_Z( Z1106PerDa );
      obj224.setgxTv_SdtPeriods_Perdatend_Z( Z1107PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdisini_Z( Z1108PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdisfin_Z( Z1109PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdueini_Z( Z1110PerDa );
      obj224.setgxTv_SdtPeriods_Perdatdueend_Z( Z1111PerDa );
      obj224.setgxTv_SdtPeriods_Perclose_Z( Z1112PerCl );
      obj224.setgxTv_SdtPeriods_Perdatins_Z( Z1113PerDa );
      obj224.setgxTv_SdtPeriods_Perdatupd_Z( Z1114PerDa );
      obj224.setgxTv_SdtPeriods_Peridadm_Z( Z1115PerID );
      obj224.setgxTv_SdtPeriods_Isodes_N( (byte)((byte)((n20ISODes)?1:0)) );
      obj224.setgxTv_SdtPeriods_Curdescription_N( (byte)((byte)((n351CurDes)?1:0)) );
      obj224.setgxTv_SdtPeriods_Perdatdisini_N( (byte)((byte)((n1108PerDa)?1:0)) );
      obj224.setgxTv_SdtPeriods_Perdatdisfin_N( (byte)((byte)((n1109PerDa)?1:0)) );
      obj224.setgxTv_SdtPeriods_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars224( SdtPeriods obj224 ,
                             int forceLoad )
   {
      Gx_mode = obj224.getgxTv_SdtPeriods_Mode() ;
      A1113PerDa = obj224.getgxTv_SdtPeriods_Perdatins() ;
      A1114PerDa = obj224.getgxTv_SdtPeriods_Perdatupd() ;
      A20ISODes = obj224.getgxTv_SdtPeriods_Isodes() ;
      A351CurDes = obj224.getgxTv_SdtPeriods_Curdescription() ;
      A1105PerDe = obj224.getgxTv_SdtPeriods_Perdescr() ;
      A1106PerDa = obj224.getgxTv_SdtPeriods_Perdatini() ;
      A1107PerDa = obj224.getgxTv_SdtPeriods_Perdatend() ;
      A1108PerDa = obj224.getgxTv_SdtPeriods_Perdatdisini() ;
      A1109PerDa = obj224.getgxTv_SdtPeriods_Perdatdisfin() ;
      A1110PerDa = obj224.getgxTv_SdtPeriods_Perdatdueini() ;
      A1111PerDa = obj224.getgxTv_SdtPeriods_Perdatdueend() ;
      A1112PerCl = obj224.getgxTv_SdtPeriods_Perclose() ;
      A1115PerID = obj224.getgxTv_SdtPeriods_Peridadm() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A23ISOCod = obj224.getgxTv_SdtPeriods_Isocod() ;
      }
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A349CurCod = obj224.getgxTv_SdtPeriods_Curcode() ;
      }
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A387Period = obj224.getgxTv_SdtPeriods_Periodtypescode() ;
      }
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A263PerID = obj224.getgxTv_SdtPeriods_Perid() ;
      }
      Z23ISOCod = obj224.getgxTv_SdtPeriods_Isocod_Z() ;
      Z349CurCod = obj224.getgxTv_SdtPeriods_Curcode_Z() ;
      Z387Period = obj224.getgxTv_SdtPeriods_Periodtypescode_Z() ;
      Z263PerID = obj224.getgxTv_SdtPeriods_Perid_Z() ;
      Z20ISODes = obj224.getgxTv_SdtPeriods_Isodes_Z() ;
      Z351CurDes = obj224.getgxTv_SdtPeriods_Curdescription_Z() ;
      Z1105PerDe = obj224.getgxTv_SdtPeriods_Perdescr_Z() ;
      Z1106PerDa = obj224.getgxTv_SdtPeriods_Perdatini_Z() ;
      Z1107PerDa = obj224.getgxTv_SdtPeriods_Perdatend_Z() ;
      Z1108PerDa = obj224.getgxTv_SdtPeriods_Perdatdisini_Z() ;
      Z1109PerDa = obj224.getgxTv_SdtPeriods_Perdatdisfin_Z() ;
      Z1110PerDa = obj224.getgxTv_SdtPeriods_Perdatdueini_Z() ;
      Z1111PerDa = obj224.getgxTv_SdtPeriods_Perdatdueend_Z() ;
      Z1112PerCl = obj224.getgxTv_SdtPeriods_Perclose_Z() ;
      Z1113PerDa = obj224.getgxTv_SdtPeriods_Perdatins_Z() ;
      Z1114PerDa = obj224.getgxTv_SdtPeriods_Perdatupd_Z() ;
      Z1115PerID = obj224.getgxTv_SdtPeriods_Peridadm_Z() ;
      n20ISODes = (boolean)((obj224.getgxTv_SdtPeriods_Isodes_N()==0)?false:true) ;
      n351CurDes = (boolean)((obj224.getgxTv_SdtPeriods_Curdescription_N()==0)?false:true) ;
      n1108PerDa = (boolean)((obj224.getgxTv_SdtPeriods_Perdatdisini_N()==0)?false:true) ;
      n1109PerDa = (boolean)((obj224.getgxTv_SdtPeriods_Perdatdisfin_N()==0)?false:true) ;
      Gx_mode = obj224.getgxTv_SdtPeriods_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A23ISOCod = (String)obj[0] ;
      A349CurCod = (String)obj[1] ;
      A387Period = (String)obj[2] ;
      A263PerID = (String)obj[3] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey29224( ) ;
      scanStart29224( ) ;
      if ( ( RcdFound224 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z23ISOCod = A23ISOCod ;
         Z349CurCod = A349CurCod ;
         Z387Period = A387Period ;
         Z263PerID = A263PerID ;
      }
      onLoadActions29224( ) ;
      zm29224( 0) ;
      addRow29224( ) ;
      scanEnd29224( ) ;
      if ( ( RcdFound224 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars224( bcPeriods, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey29224( ) ;
      if ( ( RcdFound224 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A349CurCod, Z349CurCod) != 0 ) || ( GXutil.strcmp(A387Period, Z387Period) != 0 ) || ( GXutil.strcmp(A263PerID, Z263PerID) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            A349CurCod = Z349CurCod ;
            A387Period = Z387Period ;
            A263PerID = Z263PerID ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update29224( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A349CurCod, Z349CurCod) != 0 ) || ( GXutil.strcmp(A387Period, Z387Period) != 0 ) || ( GXutil.strcmp(A263PerID, Z263PerID) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert29224( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert29224( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow224( bcPeriods) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars224( bcPeriods, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey29224( ) ;
      if ( ( RcdFound224 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A349CurCod, Z349CurCod) != 0 ) || ( GXutil.strcmp(A387Period, Z387Period) != 0 ) || ( GXutil.strcmp(A263PerID, Z263PerID) != 0 ) )
         {
            A23ISOCod = Z23ISOCod ;
            A349CurCod = Z349CurCod ;
            A387Period = Z387Period ;
            A263PerID = Z263PerID ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A23ISOCod, Z23ISOCod) != 0 ) || ( GXutil.strcmp(A349CurCod, Z349CurCod) != 0 ) || ( GXutil.strcmp(A387Period, Z387Period) != 0 ) || ( GXutil.strcmp(A263PerID, Z263PerID) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      pr_default.close(10);
      pr_default.close(11);
      Application.rollback(context, remoteHandle, "DEFAULT", "tperiods_bc");
      VarsToRow224( bcPeriods) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcPeriods.getgxTv_SdtPeriods_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcPeriods.setgxTv_SdtPeriods_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtPeriods sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcPeriods ) )
      {
         bcPeriods = sdt ;
         if ( ( GXutil.strcmp(bcPeriods.getgxTv_SdtPeriods_Mode(), "") == 0 ) )
         {
            bcPeriods.setgxTv_SdtPeriods_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow224( bcPeriods) ;
         }
         else
         {
            RowToVars224( bcPeriods, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcPeriods.getgxTv_SdtPeriods_Mode(), "") == 0 ) )
         {
            bcPeriods.setgxTv_SdtPeriods_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars224( bcPeriods, 1) ;
      return  ;
   }

   public SdtPeriods getPeriods_BC( )
   {
      return bcPeriods ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(10);
      pr_default.close(11);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z23ISOCod = "" ;
      A23ISOCod = "" ;
      Z349CurCod = "" ;
      A349CurCod = "" ;
      Z387Period = "" ;
      A387Period = "" ;
      Z263PerID = "" ;
      A263PerID = "" ;
      AV12lgnLog = "" ;
      AV11Sessio = httpContext.getWebSession();
      AV19Acessa = (byte)(0) ;
      GXt_int1 = (byte)(0) ;
      GXv_svchar2 = new String [1] ;
      GXv_svchar3 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_int5 = new byte [1] ;
      gxTv_SdtPeriods_Isocod_Z = "" ;
      gxTv_SdtPeriods_Curcode_Z = "" ;
      gxTv_SdtPeriods_Periodtypescode_Z = "" ;
      gxTv_SdtPeriods_Perid_Z = "" ;
      gxTv_SdtPeriods_Isodes_Z = "" ;
      gxTv_SdtPeriods_Curdescription_Z = "" ;
      gxTv_SdtPeriods_Perdescr_Z = "" ;
      gxTv_SdtPeriods_Perdatini_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatend_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdisini_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdisfin_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdueini_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdueend_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perclose_Z = "" ;
      gxTv_SdtPeriods_Perdatins_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtPeriods_Perdatupd_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtPeriods_Peridadm_Z = "" ;
      gxTv_SdtPeriods_Isodes_N = (byte)(0) ;
      gxTv_SdtPeriods_Curdescription_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisini_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisfin_N = (byte)(0) ;
      GX_JID = 0 ;
      Z1113PerDa = GXutil.resetTime( GXutil.nullDate() );
      A1113PerDa = GXutil.resetTime( GXutil.nullDate() );
      Z1114PerDa = GXutil.resetTime( GXutil.nullDate() );
      A1114PerDa = GXutil.resetTime( GXutil.nullDate() );
      Z1105PerDe = "" ;
      A1105PerDe = "" ;
      Z1106PerDa = GXutil.nullDate() ;
      A1106PerDa = GXutil.nullDate() ;
      Z1107PerDa = GXutil.nullDate() ;
      A1107PerDa = GXutil.nullDate() ;
      Z1108PerDa = GXutil.nullDate() ;
      A1108PerDa = GXutil.nullDate() ;
      Z1109PerDa = GXutil.nullDate() ;
      A1109PerDa = GXutil.nullDate() ;
      Z1110PerDa = GXutil.nullDate() ;
      A1110PerDa = GXutil.nullDate() ;
      Z1111PerDa = GXutil.nullDate() ;
      A1111PerDa = GXutil.nullDate() ;
      Z1112PerCl = "" ;
      A1112PerCl = "" ;
      Z1115PerID = "" ;
      A1115PerID = "" ;
      Z20ISODes = "" ;
      A20ISODes = "" ;
      Z351CurDes = "" ;
      A351CurDes = "" ;
      A23ISOCod_Enabled = 0 ;
      BC00297_A263PerID = new String[] {""} ;
      BC00297_A1113PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_A1114PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_A20ISODes = new String[] {""} ;
      BC00297_n20ISODes = new boolean[] {false} ;
      BC00297_A351CurDes = new String[] {""} ;
      BC00297_n351CurDes = new boolean[] {false} ;
      BC00297_A1105PerDe = new String[] {""} ;
      BC00297_A1106PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_A1107PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_A1108PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_n1108PerDa = new boolean[] {false} ;
      BC00297_A1109PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_n1109PerDa = new boolean[] {false} ;
      BC00297_A1110PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_A1111PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00297_A1112PerCl = new String[] {""} ;
      BC00297_A1115PerID = new String[] {""} ;
      BC00297_A23ISOCod = new String[] {""} ;
      BC00297_A349CurCod = new String[] {""} ;
      BC00297_A387Period = new String[] {""} ;
      RcdFound224 = (short)(0) ;
      n20ISODes = false ;
      n351CurDes = false ;
      n1108PerDa = false ;
      n1109PerDa = false ;
      BC00294_A20ISODes = new String[] {""} ;
      BC00294_n20ISODes = new boolean[] {false} ;
      BC00295_A351CurDes = new String[] {""} ;
      BC00295_n351CurDes = new boolean[] {false} ;
      BC00296_A387Period = new String[] {""} ;
      BC00298_A23ISOCod = new String[] {""} ;
      BC00298_A349CurCod = new String[] {""} ;
      BC00298_A387Period = new String[] {""} ;
      BC00298_A263PerID = new String[] {""} ;
      BC00293_A263PerID = new String[] {""} ;
      BC00293_A1113PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_A1114PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_A1105PerDe = new String[] {""} ;
      BC00293_A1106PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_A1107PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_A1108PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_n1108PerDa = new boolean[] {false} ;
      BC00293_A1109PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_n1109PerDa = new boolean[] {false} ;
      BC00293_A1110PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_A1111PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00293_A1112PerCl = new String[] {""} ;
      BC00293_A1115PerID = new String[] {""} ;
      BC00293_A23ISOCod = new String[] {""} ;
      BC00293_A349CurCod = new String[] {""} ;
      BC00293_A387Period = new String[] {""} ;
      sMode224 = "" ;
      BC00292_A263PerID = new String[] {""} ;
      BC00292_A1113PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_A1114PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_A1105PerDe = new String[] {""} ;
      BC00292_A1106PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_A1107PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_A1108PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_n1108PerDa = new boolean[] {false} ;
      BC00292_A1109PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_n1109PerDa = new boolean[] {false} ;
      BC00292_A1110PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_A1111PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC00292_A1112PerCl = new String[] {""} ;
      BC00292_A1115PerID = new String[] {""} ;
      BC00292_A23ISOCod = new String[] {""} ;
      BC00292_A349CurCod = new String[] {""} ;
      BC00292_A387Period = new String[] {""} ;
      Gx_longc = false ;
      BC002912_A20ISODes = new String[] {""} ;
      BC002912_n20ISODes = new boolean[] {false} ;
      BC002913_A351CurDes = new String[] {""} ;
      BC002913_n351CurDes = new boolean[] {false} ;
      BC002914_A263PerID = new String[] {""} ;
      BC002914_A1113PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_A1114PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_A20ISODes = new String[] {""} ;
      BC002914_n20ISODes = new boolean[] {false} ;
      BC002914_A351CurDes = new String[] {""} ;
      BC002914_n351CurDes = new boolean[] {false} ;
      BC002914_A1105PerDe = new String[] {""} ;
      BC002914_A1106PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_A1107PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_A1108PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_n1108PerDa = new boolean[] {false} ;
      BC002914_A1109PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_n1109PerDa = new boolean[] {false} ;
      BC002914_A1110PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_A1111PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      BC002914_A1112PerCl = new String[] {""} ;
      BC002914_A1115PerID = new String[] {""} ;
      BC002914_A23ISOCod = new String[] {""} ;
      BC002914_A349CurCod = new String[] {""} ;
      BC002914_A387Period = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tperiods_bc__default(),
         new Object[] {
             new Object[] {
            BC00292_A263PerID, BC00292_A1113PerDa, BC00292_A1114PerDa, BC00292_A1105PerDe, BC00292_A1106PerDa, BC00292_A1107PerDa, BC00292_A1108PerDa, BC00292_n1108PerDa, BC00292_A1109PerDa, BC00292_n1109PerDa,
            BC00292_A1110PerDa, BC00292_A1111PerDa, BC00292_A1112PerCl, BC00292_A1115PerID, BC00292_A23ISOCod, BC00292_A349CurCod, BC00292_A387Period
            }
            , new Object[] {
            BC00293_A263PerID, BC00293_A1113PerDa, BC00293_A1114PerDa, BC00293_A1105PerDe, BC00293_A1106PerDa, BC00293_A1107PerDa, BC00293_A1108PerDa, BC00293_n1108PerDa, BC00293_A1109PerDa, BC00293_n1109PerDa,
            BC00293_A1110PerDa, BC00293_A1111PerDa, BC00293_A1112PerCl, BC00293_A1115PerID, BC00293_A23ISOCod, BC00293_A349CurCod, BC00293_A387Period
            }
            , new Object[] {
            BC00294_A20ISODes, BC00294_n20ISODes
            }
            , new Object[] {
            BC00295_A351CurDes, BC00295_n351CurDes
            }
            , new Object[] {
            BC00296_A387Period
            }
            , new Object[] {
            BC00297_A263PerID, BC00297_A1113PerDa, BC00297_A1114PerDa, BC00297_A20ISODes, BC00297_n20ISODes, BC00297_A351CurDes, BC00297_n351CurDes, BC00297_A1105PerDe, BC00297_A1106PerDa, BC00297_A1107PerDa,
            BC00297_A1108PerDa, BC00297_n1108PerDa, BC00297_A1109PerDa, BC00297_n1109PerDa, BC00297_A1110PerDa, BC00297_A1111PerDa, BC00297_A1112PerCl, BC00297_A1115PerID, BC00297_A23ISOCod, BC00297_A349CurCod,
            BC00297_A387Period
            }
            , new Object[] {
            BC00298_A23ISOCod, BC00298_A349CurCod, BC00298_A387Period, BC00298_A263PerID
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002912_A20ISODes, BC002912_n20ISODes
            }
            , new Object[] {
            BC002913_A351CurDes, BC002913_n351CurDes
            }
            , new Object[] {
            BC002914_A263PerID, BC002914_A1113PerDa, BC002914_A1114PerDa, BC002914_A20ISODes, BC002914_n20ISODes, BC002914_A351CurDes, BC002914_n351CurDes, BC002914_A1105PerDe, BC002914_A1106PerDa, BC002914_A1107PerDa,
            BC002914_A1108PerDa, BC002914_n1108PerDa, BC002914_A1109PerDa, BC002914_n1109PerDa, BC002914_A1110PerDa, BC002914_A1111PerDa, BC002914_A1112PerCl, BC002914_A1115PerID, BC002914_A23ISOCod, BC002914_A349CurCod,
            BC002914_A387Period
            }
         }
      );
      /* Execute Start event if defined. */
      /* Execute user event: e12292 */
      e12292 ();
   }

   private byte nKeyPressed ;
   private byte AV19Acessa ;
   private byte GXt_int1 ;
   private byte GXv_int5[] ;
   private byte gxTv_SdtPeriods_Isodes_N ;
   private byte gxTv_SdtPeriods_Curdescription_N ;
   private byte gxTv_SdtPeriods_Perdatdisini_N ;
   private byte gxTv_SdtPeriods_Perdatdisfin_N ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound224 ;
   private int trnEnded ;
   private int GX_JID ;
   private int A23ISOCod_Enabled ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String GXv_char4[] ;
   private String gxTv_SdtPeriods_Perclose_Z ;
   private String Z1112PerCl ;
   private String A1112PerCl ;
   private String sMode224 ;
   private java.util.Date gxTv_SdtPeriods_Perdatins_Z ;
   private java.util.Date gxTv_SdtPeriods_Perdatupd_Z ;
   private java.util.Date Z1113PerDa ;
   private java.util.Date A1113PerDa ;
   private java.util.Date Z1114PerDa ;
   private java.util.Date A1114PerDa ;
   private java.util.Date gxTv_SdtPeriods_Perdatini_Z ;
   private java.util.Date gxTv_SdtPeriods_Perdatend_Z ;
   private java.util.Date gxTv_SdtPeriods_Perdatdisini_Z ;
   private java.util.Date gxTv_SdtPeriods_Perdatdisfin_Z ;
   private java.util.Date gxTv_SdtPeriods_Perdatdueini_Z ;
   private java.util.Date gxTv_SdtPeriods_Perdatdueend_Z ;
   private java.util.Date Z1106PerDa ;
   private java.util.Date A1106PerDa ;
   private java.util.Date Z1107PerDa ;
   private java.util.Date A1107PerDa ;
   private java.util.Date Z1108PerDa ;
   private java.util.Date A1108PerDa ;
   private java.util.Date Z1109PerDa ;
   private java.util.Date A1109PerDa ;
   private java.util.Date Z1110PerDa ;
   private java.util.Date A1110PerDa ;
   private java.util.Date Z1111PerDa ;
   private java.util.Date A1111PerDa ;
   private boolean n20ISODes ;
   private boolean n351CurDes ;
   private boolean n1108PerDa ;
   private boolean n1109PerDa ;
   private boolean Gx_longc ;
   private String Z23ISOCod ;
   private String A23ISOCod ;
   private String Z349CurCod ;
   private String A349CurCod ;
   private String Z387Period ;
   private String A387Period ;
   private String Z263PerID ;
   private String A263PerID ;
   private String AV12lgnLog ;
   private String GXv_svchar2[] ;
   private String GXv_svchar3[] ;
   private String gxTv_SdtPeriods_Isocod_Z ;
   private String gxTv_SdtPeriods_Curcode_Z ;
   private String gxTv_SdtPeriods_Periodtypescode_Z ;
   private String gxTv_SdtPeriods_Perid_Z ;
   private String gxTv_SdtPeriods_Isodes_Z ;
   private String gxTv_SdtPeriods_Curdescription_Z ;
   private String gxTv_SdtPeriods_Perdescr_Z ;
   private String gxTv_SdtPeriods_Peridadm_Z ;
   private String Z1105PerDe ;
   private String A1105PerDe ;
   private String Z1115PerID ;
   private String A1115PerID ;
   private String Z20ISODes ;
   private String A20ISODes ;
   private String Z351CurDes ;
   private String A351CurDes ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.WebSession AV11Sessio ;
   private SdtPeriods bcPeriods ;
   private IDataStoreProvider pr_default ;
   private String[] BC00297_A263PerID ;
   private java.util.Date[] BC00297_A1113PerDa ;
   private java.util.Date[] BC00297_A1114PerDa ;
   private String[] BC00297_A20ISODes ;
   private boolean[] BC00297_n20ISODes ;
   private String[] BC00297_A351CurDes ;
   private boolean[] BC00297_n351CurDes ;
   private String[] BC00297_A1105PerDe ;
   private java.util.Date[] BC00297_A1106PerDa ;
   private java.util.Date[] BC00297_A1107PerDa ;
   private java.util.Date[] BC00297_A1108PerDa ;
   private boolean[] BC00297_n1108PerDa ;
   private java.util.Date[] BC00297_A1109PerDa ;
   private boolean[] BC00297_n1109PerDa ;
   private java.util.Date[] BC00297_A1110PerDa ;
   private java.util.Date[] BC00297_A1111PerDa ;
   private String[] BC00297_A1112PerCl ;
   private String[] BC00297_A1115PerID ;
   private String[] BC00297_A23ISOCod ;
   private String[] BC00297_A349CurCod ;
   private String[] BC00297_A387Period ;
   private String[] BC00294_A20ISODes ;
   private boolean[] BC00294_n20ISODes ;
   private String[] BC00295_A351CurDes ;
   private boolean[] BC00295_n351CurDes ;
   private String[] BC00296_A387Period ;
   private String[] BC00298_A23ISOCod ;
   private String[] BC00298_A349CurCod ;
   private String[] BC00298_A387Period ;
   private String[] BC00298_A263PerID ;
   private String[] BC00293_A263PerID ;
   private java.util.Date[] BC00293_A1113PerDa ;
   private java.util.Date[] BC00293_A1114PerDa ;
   private String[] BC00293_A1105PerDe ;
   private java.util.Date[] BC00293_A1106PerDa ;
   private java.util.Date[] BC00293_A1107PerDa ;
   private java.util.Date[] BC00293_A1108PerDa ;
   private boolean[] BC00293_n1108PerDa ;
   private java.util.Date[] BC00293_A1109PerDa ;
   private boolean[] BC00293_n1109PerDa ;
   private java.util.Date[] BC00293_A1110PerDa ;
   private java.util.Date[] BC00293_A1111PerDa ;
   private String[] BC00293_A1112PerCl ;
   private String[] BC00293_A1115PerID ;
   private String[] BC00293_A23ISOCod ;
   private String[] BC00293_A349CurCod ;
   private String[] BC00293_A387Period ;
   private String[] BC00292_A263PerID ;
   private java.util.Date[] BC00292_A1113PerDa ;
   private java.util.Date[] BC00292_A1114PerDa ;
   private String[] BC00292_A1105PerDe ;
   private java.util.Date[] BC00292_A1106PerDa ;
   private java.util.Date[] BC00292_A1107PerDa ;
   private java.util.Date[] BC00292_A1108PerDa ;
   private boolean[] BC00292_n1108PerDa ;
   private java.util.Date[] BC00292_A1109PerDa ;
   private boolean[] BC00292_n1109PerDa ;
   private java.util.Date[] BC00292_A1110PerDa ;
   private java.util.Date[] BC00292_A1111PerDa ;
   private String[] BC00292_A1112PerCl ;
   private String[] BC00292_A1115PerID ;
   private String[] BC00292_A23ISOCod ;
   private String[] BC00292_A349CurCod ;
   private String[] BC00292_A387Period ;
   private String[] BC002912_A20ISODes ;
   private boolean[] BC002912_n20ISODes ;
   private String[] BC002913_A351CurDes ;
   private boolean[] BC002913_n351CurDes ;
   private String[] BC002914_A263PerID ;
   private java.util.Date[] BC002914_A1113PerDa ;
   private java.util.Date[] BC002914_A1114PerDa ;
   private String[] BC002914_A20ISODes ;
   private boolean[] BC002914_n20ISODes ;
   private String[] BC002914_A351CurDes ;
   private boolean[] BC002914_n351CurDes ;
   private String[] BC002914_A1105PerDe ;
   private java.util.Date[] BC002914_A1106PerDa ;
   private java.util.Date[] BC002914_A1107PerDa ;
   private java.util.Date[] BC002914_A1108PerDa ;
   private boolean[] BC002914_n1108PerDa ;
   private java.util.Date[] BC002914_A1109PerDa ;
   private boolean[] BC002914_n1109PerDa ;
   private java.util.Date[] BC002914_A1110PerDa ;
   private java.util.Date[] BC002914_A1111PerDa ;
   private String[] BC002914_A1112PerCl ;
   private String[] BC002914_A1115PerID ;
   private String[] BC002914_A23ISOCod ;
   private String[] BC002914_A349CurCod ;
   private String[] BC002914_A387Period ;
}

final  class tperiods_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00292", "SELECT [PerID], [PerDatIns], [PerDatUpd], [PerDescr], [PerDatIni], [PerDatEnd], [PerDatDisIni], [PerDatDisFin], [PerDatDueIni], [PerDatDueEnd], [PerClose], [PerIDAdm], [ISOCod], [CurCode], [PeriodTypesCode] FROM [PERIODS] WITH (UPDLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? AND [PeriodTypesCode] = ? AND [PerID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00293", "SELECT [PerID], [PerDatIns], [PerDatUpd], [PerDescr], [PerDatIni], [PerDatEnd], [PerDatDisIni], [PerDatDisFin], [PerDatDueIni], [PerDatDueEnd], [PerClose], [PerIDAdm], [ISOCod], [CurCode], [PeriodTypesCode] FROM [PERIODS] WITH (NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? AND [PeriodTypesCode] = ? AND [PerID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00294", "SELECT [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00295", "SELECT [CurDescription] FROM [COUNTRYCURRENCIES] WITH (NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00296", "SELECT [PeriodTypesCode] FROM [PERIODTYPES] WITH (NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00297", "SELECT TM1.[PerID], TM1.[PerDatIns], TM1.[PerDatUpd], T2.[ISODes], T3.[CurDescription], TM1.[PerDescr], TM1.[PerDatIni], TM1.[PerDatEnd], TM1.[PerDatDisIni], TM1.[PerDatDisFin], TM1.[PerDatDueIni], TM1.[PerDatDueEnd], TM1.[PerClose], TM1.[PerIDAdm], TM1.[ISOCod], TM1.[CurCode], TM1.[PeriodTypesCode] FROM (([PERIODS] TM1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [COUNTRY] T2 WITH (NOLOCK) ON T2.[ISOCod] = TM1.[ISOCod]) INNER JOIN [COUNTRYCURRENCIES] T3 WITH (NOLOCK) ON T3.[ISOCod] = TM1.[ISOCod] AND T3.[CurCode] = TM1.[CurCode]) WHERE TM1.[ISOCod] = ? and TM1.[CurCode] = ? and TM1.[PeriodTypesCode] = ? and TM1.[PerID] = ? ORDER BY TM1.[ISOCod], TM1.[CurCode], TM1.[PeriodTypesCode], TM1.[PerID] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00298", "SELECT [ISOCod], [CurCode], [PeriodTypesCode], [PerID] FROM [PERIODS] WITH (FASTFIRSTROW NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? AND [PeriodTypesCode] = ? AND [PerID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC00299", "INSERT INTO [PERIODS] ([PerID], [PerDatIns], [PerDatUpd], [PerDescr], [PerDatIni], [PerDatEnd], [PerDatDisIni], [PerDatDisFin], [PerDatDueIni], [PerDatDueEnd], [PerClose], [PerIDAdm], [ISOCod], [CurCode], [PeriodTypesCode]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002910", "UPDATE [PERIODS] SET [PerDatIns]=?, [PerDatUpd]=?, [PerDescr]=?, [PerDatIni]=?, [PerDatEnd]=?, [PerDatDisIni]=?, [PerDatDisFin]=?, [PerDatDueIni]=?, [PerDatDueEnd]=?, [PerClose]=?, [PerIDAdm]=?  WHERE [ISOCod] = ? AND [CurCode] = ? AND [PeriodTypesCode] = ? AND [PerID] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002911", "DELETE FROM [PERIODS]  WHERE [ISOCod] = ? AND [CurCode] = ? AND [PeriodTypesCode] = ? AND [PerID] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002912", "SELECT [ISODes] FROM [COUNTRY] WITH (NOLOCK) WHERE [ISOCod] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002913", "SELECT [CurDescription] FROM [COUNTRYCURRENCIES] WITH (NOLOCK) WHERE [ISOCod] = ? AND [CurCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002914", "SELECT TM1.[PerID], TM1.[PerDatIns], TM1.[PerDatUpd], T2.[ISODes], T3.[CurDescription], TM1.[PerDescr], TM1.[PerDatIni], TM1.[PerDatEnd], TM1.[PerDatDisIni], TM1.[PerDatDisFin], TM1.[PerDatDueIni], TM1.[PerDatDueEnd], TM1.[PerClose], TM1.[PerIDAdm], TM1.[ISOCod], TM1.[CurCode], TM1.[PeriodTypesCode] FROM (([PERIODS] TM1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [COUNTRY] T2 WITH (NOLOCK) ON T2.[ISOCod] = TM1.[ISOCod]) INNER JOIN [COUNTRYCURRENCIES] T3 WITH (NOLOCK) ON T3.[ISOCod] = TM1.[ISOCod] AND T3.[CurCode] = TM1.[CurCode]) WHERE TM1.[ISOCod] = ? and TM1.[CurCode] = ? and TM1.[PeriodTypesCode] = ? and TM1.[PerID] = ? ORDER BY TM1.[ISOCod], TM1.[CurCode], TM1.[PeriodTypesCode], TM1.[PerID] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(5) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(7) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDate(8) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(9) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               ((String[]) buf[12])[0] = rslt.getString(11, 1) ;
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
               ((String[]) buf[15])[0] = rslt.getVarchar(14) ;
               ((String[]) buf[16])[0] = rslt.getVarchar(15) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(5) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(7) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDate(8) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(9) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               ((String[]) buf[12])[0] = rslt.getString(11, 1) ;
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
               ((String[]) buf[15])[0] = rslt.getVarchar(14) ;
               ((String[]) buf[16])[0] = rslt.getVarchar(15) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDate(7) ;
               ((java.util.Date[]) buf[9])[0] = rslt.getGXDate(8) ;
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(9) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[12])[0] = rslt.getGXDate(10) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[14])[0] = rslt.getGXDate(11) ;
               ((java.util.Date[]) buf[15])[0] = rslt.getGXDate(12) ;
               ((String[]) buf[16])[0] = rslt.getString(13, 1) ;
               ((String[]) buf[17])[0] = rslt.getVarchar(14) ;
               ((String[]) buf[18])[0] = rslt.getVarchar(15) ;
               ((String[]) buf[19])[0] = rslt.getVarchar(16) ;
               ((String[]) buf[20])[0] = rslt.getVarchar(17) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDateTime(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDate(7) ;
               ((java.util.Date[]) buf[9])[0] = rslt.getGXDate(8) ;
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(9) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[12])[0] = rslt.getGXDate(10) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[14])[0] = rslt.getGXDate(11) ;
               ((java.util.Date[]) buf[15])[0] = rslt.getGXDate(12) ;
               ((String[]) buf[16])[0] = rslt.getString(13, 1) ;
               ((String[]) buf[17])[0] = rslt.getVarchar(14) ;
               ((String[]) buf[18])[0] = rslt.getVarchar(15) ;
               ((String[]) buf[19])[0] = rslt.getVarchar(16) ;
               ((String[]) buf[20])[0] = rslt.getVarchar(17) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               stmt.setDateTime(3, (java.util.Date)parms[2], false);
               stmt.setVarchar(4, (String)parms[3], 30, false);
               stmt.setDate(5, (java.util.Date)parms[4]);
               stmt.setDate(6, (java.util.Date)parms[5]);
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(7, (java.util.Date)parms[7]);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(8, (java.util.Date)parms[9]);
               }
               stmt.setDate(9, (java.util.Date)parms[10]);
               stmt.setDate(10, (java.util.Date)parms[11]);
               stmt.setString(11, (String)parms[12], 1);
               stmt.setVarchar(12, (String)parms[13], 4, false);
               stmt.setVarchar(13, (String)parms[14], 3, false);
               stmt.setVarchar(14, (String)parms[15], 3, false);
               stmt.setVarchar(15, (String)parms[16], 5, false);
               break;
            case 8 :
               stmt.setDateTime(1, (java.util.Date)parms[0], false);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               stmt.setVarchar(3, (String)parms[2], 30, false);
               stmt.setDate(4, (java.util.Date)parms[3]);
               stmt.setDate(5, (java.util.Date)parms[4]);
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(6, (java.util.Date)parms[6]);
               }
               if ( ((Boolean) parms[7]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(7, (java.util.Date)parms[8]);
               }
               stmt.setDate(8, (java.util.Date)parms[9]);
               stmt.setDate(9, (java.util.Date)parms[10]);
               stmt.setString(10, (String)parms[11], 1);
               stmt.setVarchar(11, (String)parms[12], 4, false);
               stmt.setVarchar(12, (String)parms[13], 3, false);
               stmt.setVarchar(13, (String)parms[14], 3, false);
               stmt.setVarchar(14, (String)parms[15], 5, false);
               stmt.setVarchar(15, (String)parms[16], 20, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               break;
            case 11 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               break;
            case 12 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 3, false);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               stmt.setVarchar(4, (String)parms[3], 20, false);
               break;
      }
   }

}


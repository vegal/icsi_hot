/*
               File: Gx06B2
        Description: Selection List Airlines Bank
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:16.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class wgx06b2 extends GXWorkpanel
{
   public wgx06b2( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wgx06b2.class ));
   }

   public wgx06b2( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected String getObjectName( )
   {
      return "Gx06B2" ;
   }

   protected String getFrmTitle( )
   {
      return "Selection List Airlines Bank" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 409 ;
   }

   protected int getFrmHeight( )
   {
      return 330 ;
   }

   protected String getHelpId( )
   {
      return "HLP_WGx06B2.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(15) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String[] aP2 )
   {
      wgx06b2.this.A23ISOCod = aP0;
      wgx06b2.this.A1136AirLi = aP1;
      wgx06b2.this.aP2 = aP2;
      start();
   }

   protected void standAlone( )
   {
   }

   public void runLoad_load14( ) throws GXLoadInterruptException
   {
      subwgx06b214 = new subwgx06b214 ();
      lV5cAirLin = GXutil.padr( GXutil.rtrim( AV5cAirLin), (short)(3), "%") ;
      lV6cAirLin = GXutil.padr( GXutil.rtrim( AV6cAirLin), (short)(3), "%") ;
      /* Using cursor W00132 */
      pr_default.execute(0, new Object[] {A23ISOCod, A1136AirLi, lV5cAirLin, lV6cAirLin});
      while ( ( (pr_default.getStatus(0) != 101) ) && ( ( subGrd_1.getSize() < 10000 ) ) )
      {
         A1132AirLi = W00132_A1132AirLi[0] ;
         A1137AirLi = W00132_A1137AirLi[0] ;
         /* Execute user event: e11V132 */
         e11V132 ();
         pr_default.readNext(0);
      }
      if ( subGrd_1.getSize() >= 10000 && !(pr_default.getStatus(0) == 101) )
      {
         GXutil.msg( me(), localUtil.getMessages().getMessage("mlmax", new Object[]{ new Integer(10000)}) );
      }
      pr_default.close(0);
   }

   public final  class Gx06B2_load14 extends GXLoadProducer
   {
      wgx06b2 _sf ;

      public Gx06B2_load14( wgx06b2 uType )
      {
         _sf = uType;
      }
      public void loadToBuffer( )
      {
         _sf.loadToBuffer14();
      }

      public void runLoad( ) throws GXLoadInterruptException
      {
         if(!UIFactory.isDisposed( _sf.getIPanel() )) {
            _sf.runLoad_load14();
         }
      }

      public void closeCursors( )
      {
         _sf.closeCursors14();
      }

   }

   protected void GXRefresh( )
   {
      ControlsToVariables();
      GXRefreshCommand();
   }

   protected void GXRefreshCommand( )
   {
      if ( ! gxIsRefreshing )
      {
         gxIsRefreshing = true ;
         ControlsToVariables();
         standAlone();
         VariablesToControls();
         subGrd_1.refresh();
         gxIsRefreshing = false ;
      }
   }

   protected void refreshEvent( )
   {
   }

   public boolean isLoadAtStartup_flow14( )
   {
      return true;
   }

   public void autoRefresh_flow14( GXSubfile subfile ,
                                   boolean loadedFirstTime )
   {
      if (( (GXutil.strcmp(AV5cAirLin, cV5cAirLin)!=0)||(GXutil.strcmp(AV6cAirLin, cV6cAirLin)!=0) ) || (!loadedFirstTime && ! isLoadAtStartup_flow14() )) {
         subfile.refresh();
         resetSubfileConditions_flow14() ;
      }
   }

   public boolean getSearch_flow14( )
   {
      return false ;
   }

   public void resetSubfileConditions_flow14( )
   {
      cV5cAirLin = AV5cAirLin ;
      cV6cAirLin = AV6cAirLin ;
   }

   public void resetSearchConditions_flow14( boolean defaults )
   {
   }

   public GXSubfileElement getNewSubfileElement_flow14( )
   {
      return new subwgx06b214 ();
   }

   public boolean getSearch_flow14( GXSubfileElement subfileElement )
   {
      return true;
   }

   public void setConditionalColor_flow14( GUIObject comp ,
                                           GXSubfileElement element )
   {

   }

   public boolean getNoaccept_flow14( int col ,
                                      GXSubfileElement element ,
                                      boolean enabled )
   {
      return !enabled;
   }

   public void refresh_flow14( )
   {
      GXRefreshCommand14 ();
   }

   public final  class Gx06B2_flow14 extends GXSubfileFlowBase implements GXSubfileFlow
   {
      wgx06b2 _sf ;

      public Gx06B2_flow14( wgx06b2 uType )
      {
         _sf = uType;
      }
      public boolean isLoadAtStartup( )
      {
         return _sf.isLoadAtStartup_flow14();
      }

      public void autoRefresh( GXSubfile subfile ,
                               boolean loadedFirstTime )
      {
         _sf.autoRefresh_flow14(subfile, loadedFirstTime);
      }

      public boolean getSearch( )
      {
         return _sf.getSearch_flow14();
      }

      public void resetSubfileConditions( )
      {
         _sf.resetSubfileConditions_flow14();
      }

      public void resetSearchConditions( boolean defaults )
      {
         _sf.resetSearchConditions_flow14(defaults);
      }

      public GXSubfileElement getNewSubfileElement( )
      {
         return _sf.getNewSubfileElement_flow14();
      }

      public void refreshScreen( )
      {
         _sf.VariablesToControls();
      }

      public boolean getSearch( GXSubfileElement subfileElement )
      {
         return _sf.getSearch_flow14(subfileElement);
      }

      public void setConditionalColor( GUIObject comp ,
                                       GXSubfileElement element )
      {
         _sf.setConditionalColor_flow14(comp, element);
      }

      public boolean getNoaccept( int col ,
                                  GXSubfileElement element ,
                                  boolean enabled )
      {
         return _sf.getNoaccept_flow14(col, element, enabled);
      }

      public void refresh( )
      {
         _sf.refresh_flow14();
      }

   }

   protected void GXRefreshCommand14( )
   {
      ControlsToVariables();
      /* End function GeneXus Refresh */
   }

   public void GXEnter( )
   {
      /* Execute user event: e12V132 */
      e12V132 ();
      if ( returnInSub )
      {
         if (canCleanup()) {
            returnInSub = true;
            cleanup();
         }
         if (true) return;
      }
   }

   public void e12V132( )
   {
      eventLevelContext();
      /* Enter Routine */
      AV7pAirLin = A1137AirLi ;
      if (canCleanup()) {
         returnInSub = true;
         cleanup();
      }
      if (true) return;
      eventLevelResetContext();
   }

   public void loadToBuffer14( )
   {
      subwgx06b214 oAux = subwgx06b214 ;
      subwgx06b214 = new subwgx06b214 ();
      variablesToSubfile14 ();
      subGrd_1.addElement(subwgx06b214);
      subwgx06b214 = oAux;
   }

   private void e11V132( ) throws GXLoadInterruptException
   {
      /* Load Routine */
      subGrd_1.loadCommand();
   }

   protected void closeCursors14( )
   {
      pr_default.close(0);
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 0 , 0 , 409 , 330 );
      this.setIBackground(UIFactory.getColor(15));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      dynISOCod = new GUIObjectString ( new GXComboBox(GXPanel1, this, 6) , GXPanel1 , 133 , 24 , 47 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A23ISOCod" );
      dynISOCod.addFocusListener(this);
      dynISOCod.addItemListener(this);
      dynISOCod.getGXComponent().setHelpId("HLP_WGx06B2.htm");
      edtAirLineCode = new GUIObjectString ( new GXEdit(20, "XXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),133, 48, 150, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), true) , GXPanel1 , 133 , 48 , 150 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1136AirLi" );
      ((GXEdit) edtAirLineCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtAirLineCode.addFocusListener(this);
      edtAirLineCode.getGXComponent().setHelpId("HLP_WGx06B2.htm");
      edtavCairlinecurcode = new GUIObjectString ( new GXEdit(3, "XXX", UIFactory.getFont( "Courier New", 0, 9),133, 72, 31, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 133 , 72 , 31 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV5cAirLin" );
      ((GXEdit) edtavCairlinecurcode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecurcode.addFocusListener(this);
      edtavCairlinecurcode.getGXComponent().setHelpId("HLP_WGx06B2.htm");
      edtavCairlinecurtrans = new GUIObjectString ( new GXEdit(3, "XXX", UIFactory.getFont( "Courier New", 0, 9),133, 96, 31, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 133 , 96 , 31 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "AV6cAirLin" );
      ((GXEdit) edtavCairlinecurtrans.getGXComponent()).setAlignment(ILabel.LEFT);
      edtavCairlinecurtrans.addFocusListener(this);
      edtavCairlinecurtrans.getGXComponent().setHelpId("HLP_WGx06B2.htm");
      addSubfile ( subGrd_1  = new GXSubfile ( new Gx06B2_load14(this), new Gx06B2_flow14(this), false , new GXColumnDefinition[] {
       new GXColumnDefinition( new GUIObjectString ( new GXEdit(3, "XXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 60, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 59 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1137AirLi" ), "Currency"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 59 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      , new GXColumnDefinition( new GUIObjectString ( new GXEdit(3, "XXX", UIFactory.getFont( "Courier New", 0, 9),0, 0, 98, 19, GXPanel1, false, ILabel.BORDER_NONE, GXTypeConstants.VARCHAR, false, false, 0, false) , null ,  0 , 0 , 97 , 18 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , false , "A1132AirLi" ), "Tranf. Currency"  , UIFactory.getColor(18) , UIFactory.getColor(15) , 97 , UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8) , false , true )
      }, 9 , 18 , GXPanel1 , 21 , 120 , 213 , 193 ,  18 ));
      subGrd_1.addActionListener(this);
      subGrd_1.addFocusListener(this);
      subGrd_1.setSortOnClick(true);
      bttBtn_enter = UIFactory.getGXButton( GXPanel1 , "Confirm" ,  301 ,  24 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_enter.setTooltip("Confirm");
      bttBtn_enter.addActionListener(this);
      bttBtn_cancel = UIFactory.getGXButton( GXPanel1 , "Close" ,  301 ,  52 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_cancel.setTooltip("Close");
      bttBtn_cancel.addActionListener(this);
      bttBtn_cancel.setFiresEvents(false);
      bttBtn_refresh = UIFactory.getGXButton( GXPanel1 , "Refresh" ,  301 ,  80 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_refresh.setTooltip("Refresh");
      bttBtn_refresh.addActionListener(this);
      bttBtn_refresh.setFiresEvents(false);
      bttBtn_help = UIFactory.getGXButton( GXPanel1 , "Help" ,  301 ,  118 ,  89 ,  23 , UIFactory.getFont( "MS Shell Dlg", 0, 8) );
      bttBtn_help.setTooltip("Help");
      bttBtn_help.addActionListener(this);
      bttBtn_help.setFiresEvents(false);
      lbllbl7 = UIFactory.getLabel(GXPanel1, "Country code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 24 , 76 , 13 );
      lbllbl9 = UIFactory.getLabel(GXPanel1, "AirLine Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 48 , 73 , 13 );
      lbllbl11 = UIFactory.getLabel(GXPanel1, "Currency", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 72 , 51 , 13 );
      lbllbl13 = UIFactory.getLabel(GXPanel1, "Tranf. Currency", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 96 , 89 , 13 );
      focusManager.setControlList(new IFocusableControl[] {
                dynISOCod ,
                edtAirLineCode ,
                edtavCairlinecurcode ,
                edtavCairlinecurtrans ,
                subGrd_1 ,
                bttBtn_enter ,
                bttBtn_cancel ,
                bttBtn_refresh ,
                bttBtn_help
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(subGrd_1, true);
   }

   public void reloadDynamicLists( int id )
   {
      if ( (id == 6) || (id == 0) )
      {
         /* Using cursor W00133 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            ((GXComboBox) dynISOCod.getGXComponent()).addItem(W00133_A23ISOCod[0], W00133_A23ISOCod[0], (short)(0));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }
   }

   protected void variablesToSubfile14( )
   {
      subwgx06b214.setAirLineCurCode(A1137AirLi);
      subwgx06b214.setAirLineCurTrans(A1132AirLi);
      subwgx06b214.setISOCod(A23ISOCod);
      subwgx06b214.setAirLineCode(A1136AirLi);
   }

   protected void subfileToVariables14( )
   {
      A1137AirLi = subwgx06b214.getAirLineCurCode();
      A1132AirLi = subwgx06b214.getAirLineCurTrans();
      A23ISOCod = subwgx06b214.getISOCod();
      A1136AirLi = subwgx06b214.getAirLineCode();
   }

   protected void VariablesToControls( )
   {
      if (cleanedUp) {
         return  ;
      }
      dynISOCod.setValue( A23ISOCod );
      edtAirLineCode.setValue( A1136AirLi );
      edtavCairlinecurcode.setValue( AV5cAirLin );
      edtavCairlinecurtrans.setValue( AV6cAirLin );
   }

   protected void ControlsToVariables( )
   {
      if (cleanedUp) {
         return  ;
      }
      A23ISOCod = dynISOCod.getValue() ;
      A1136AirLi = edtAirLineCode.getValue() ;
      AV5cAirLin = edtavCairlinecurcode.getValue() ;
      AV6cAirLin = edtavCairlinecurtrans.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
      if ( subGrd_1.inValidElement() )
      {
         subwgx06b214 = ( subwgx06b214 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06b214 = new subwgx06b214 ();
      }
      subfileToVariables14 ();
   }

   protected void eventLevelResetContext( )
   {
      variablesToSubfile14 ();
      subGrd_1.refreshLineValue(subwgx06b214);
   }

   protected void reloadGridRow( )
   {
      if ( subGrd_1.inValidElement() )
      {
         subwgx06b214 = ( subwgx06b214 ) subGrd_1.getCurrentElement() ;
      }
      else
      {
         subwgx06b214 = new subwgx06b214 ();
      }
      subfileToVariables14 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      if ( bttBtn_cancel.isEventSource(eventSource) ) {
         if (canCleanup())
            cleanup();

         return;
      }
      if ( bttBtn_refresh.isEventSource(eventSource) ) {
         GXRefresh();
         return;
      }
      if ( bttBtn_help.isEventSource(eventSource) ) {
         showHelp();
         return;
      }
      if ( bttBtn_enter.isEventSource(eventSource) ) {
         /* Execute user event: e12V132 */
         e12V132 ();
         return;
      }
      if ( subGrd_1.isEventSource(eventSource) ) {
         /* Execute user event: e12V132 */
         e12V132 ();
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( dynISOCod.isEventSource(eventSource) ) {
         setGXCursor( dynISOCod.getGXCursor() );
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         setGXCursor( edtAirLineCode.getGXCursor() );
         return;
      }
      if ( edtavCairlinecurcode.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecurcode.getGXCursor() );
         return;
      }
      if ( edtavCairlinecurtrans.isEventSource(eventSource) ) {
         setGXCursor( edtavCairlinecurtrans.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( dynISOCod.isEventSource(eventSource) ) {
         A23ISOCod = dynISOCod.getValue() ;
         return;
      }
      if ( edtAirLineCode.isEventSource(eventSource) ) {
         A1136AirLi = edtAirLineCode.getValue() ;
         return;
      }
      if ( edtavCairlinecurcode.isEventSource(eventSource) ) {
         AV5cAirLin = edtavCairlinecurcode.getValue() ;
         return;
      }
      if ( edtavCairlinecurtrans.isEventSource(eventSource) ) {
         AV6cAirLin = edtavCairlinecurtrans.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         /* Execute user event: e12V132 */
         e12V132 ();
         return true ;
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_cancel ;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      this.aP2[0] = wgx06b2.this.AV7pAirLin;
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV7pAirLin = "" ;
      subwgx06b214 = new subwgx06b214();
      scmdbuf = "" ;
      lV5cAirLin = "" ;
      AV5cAirLin = "" ;
      lV6cAirLin = "" ;
      AV6cAirLin = "" ;
      W00132_A23ISOCod = new String[] {""} ;
      W00132_A1136AirLi = new String[] {""} ;
      W00132_A1132AirLi = new String[] {""} ;
      W00132_A1137AirLi = new String[] {""} ;
      A1132AirLi = "" ;
      A1137AirLi = "" ;
      gxIsRefreshing = false ;
      cV5cAirLin = "" ;
      cV6cAirLin = "" ;
      returnInSub = false ;
      W00133_A23ISOCod = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new wgx06b2__default(),
         new Object[] {
             new Object[] {
            W00132_A23ISOCod, W00132_A1136AirLi, W00132_A1132AirLi, W00132_A1137AirLi
            }
            , new Object[] {
            W00133_A23ISOCod
            }
         }
      );
      reloadDynamicLists(0);
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   protected short Gx_err ;
   protected String scmdbuf ;
   protected boolean gxIsRefreshing ;
   protected boolean returnInSub ;
   protected String A23ISOCod ;
   protected String A1136AirLi ;
   protected String AV7pAirLin ;
   protected String lV5cAirLin ;
   protected String AV5cAirLin ;
   protected String lV6cAirLin ;
   protected String AV6cAirLin ;
   protected String A1132AirLi ;
   protected String A1137AirLi ;
   protected String cV5cAirLin ;
   protected String cV6cAirLin ;
   protected String[] aP2 ;
   protected subwgx06b214 subwgx06b214 ;
   protected IDataStoreProvider pr_default ;
   protected String[] W00132_A23ISOCod ;
   protected String[] W00132_A1136AirLi ;
   protected String[] W00132_A1132AirLi ;
   protected String[] W00132_A1137AirLi ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString dynISOCod ;
   protected GUIObjectString edtAirLineCode ;
   protected GUIObjectString edtavCairlinecurcode ;
   protected GUIObjectString edtavCairlinecurtrans ;
   protected GXSubfile subGrd_1 ;
   protected IGXButton bttBtn_enter ;
   protected IGXButton bttBtn_cancel ;
   protected IGXButton bttBtn_refresh ;
   protected IGXButton bttBtn_help ;
   protected ILabel lbllbl7 ;
   protected ILabel lbllbl9 ;
   protected ILabel lbllbl11 ;
   protected ILabel lbllbl13 ;
   protected String[] W00133_A23ISOCod ;
}

final  class wgx06b2__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("W00132", "SELECT [ISOCod], [AirLineCode], [AirLineCurTrans], [AirLineCurCode] FROM [AIRLINESBANK] WITH (FASTFIRSTROW NOLOCK) WHERE ([ISOCod] = ? and [AirLineCode] = ?) AND ([AirLineCurCode] like ?) AND ([AirLineCurTrans] like ?) ORDER BY [ISOCod], [AirLineCode], [AirLineCurCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("W00133", "SELECT [ISOCod] FROM [COUNTRY] WITH (NOLOCK) ORDER BY [ISOCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               stmt.setVarchar(3, (String)parms[2], 3);
               stmt.setVarchar(4, (String)parms[3], 3);
               break;
      }
   }

}


/*
               File: RETICSI_BKP
        Description: RETICSI_ BKP
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:55:42.36
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;

import java.sql.*;

public final  class areticsi_bkp extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      areticsi_bkp pgm = new areticsi_bkp (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};
      String[] aP1 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
         aP1[0] = (String) args[1];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0, aP1);
   }

   public areticsi_bkp( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( areticsi_bkp.class ), "" );
   }

   public areticsi_bkp( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      areticsi_bkp.this.AV176FileS = aP0[0];
      this.aP0 = aP0;
      areticsi_bkp.this.AV39DebugM = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV178Filen ;
      GXv_char2[0] = AV176FileS ;
      GXv_char3[0] = GXt_char1 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char2, GXv_char3) ;
      areticsi_bkp.this.AV176FileS = GXv_char2[0] ;
      areticsi_bkp.this.GXt_char1 = GXv_char3[0] ;
      AV178Filen = GXt_char1 ;
      AV326LogFi = "C:\\temp\\ICSI\\RET\\Log\\LogRetDuplicate_" + GXutil.trim( GXutil.str( GXutil.year( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.month( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.day( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.hour( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.minute( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.second( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + ".txt" ;
      GXt_char1 = AV332Difer ;
      GXv_char3[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "DISCREP_ACCEPTABLE", "Discrepancies up acceptable in RET loader", "C", "1.00", GXv_char3) ;
      areticsi_bkp.this.GXt_char1 = GXv_char3[0] ;
      AV332Difer = GXt_char1 ;
      AV153Versa = "00018" ;
      AV39DebugM = GXutil.trim( GXutil.upper( AV39DebugM)) ;
      AV361Count = 1 ;
      AV373SCEFP = 0 ;
      AV374SCEPa = 0 ;
      AV381Index = 0 ;
      AV267TRN_C = 0 ;
      context.msgStatus( "RETICSI - Version "+AV153Versa );
      context.msgStatus( "  Running mode: ["+AV39DebugM+"] - Started at "+GXutil.time( ) );
      /* Execute user subroutine: S1120 */
      S1120 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S1120( )
   {
      /* 'MAIN' Routine */
      AV274flagR = (byte)(0) ;
      AV282flagR = (byte)(0) ;
      AV327RetVa = context.getSessionInstances().getDelimitedFiles().dfwopen( AV326LogFi, "", "", (byte)(0), "") ;
      AV324LogLi = "Duplicate Ticket numbers: " + GXutil.newLine( ) ;
      /* Execute user subroutine: S121 */
      S121 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strSearch( AV39DebugM, "RETDUMP", 1) > 0 ) )
      {
         AV274flagR = (byte)(1) ;
      }
      else
      {
         if ( ( GXutil.strSearch( AV39DebugM, "RETMINI", 1) > 0 ) )
         {
            AV282flagR = (byte)(1) ;
         }
      }
      if ( ( GXutil.strSearch( AV39DebugM, "KEEPFPACGC", 1) > 0 ) )
      {
         AV298FlagK = (byte)(1) ;
      }
      else
      {
         AV298FlagK = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV39DebugM, "ICSI", 1) > 0 ) )
      {
         /* Using cursor P007N2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1228lccbF = P007N2_A1228lccbF[0] ;
            A1227lccbO = P007N2_A1227lccbO[0] ;
            A1226lccbA = P007N2_A1226lccbA[0] ;
            A1225lccbC = P007N2_A1225lccbC[0] ;
            A1224lccbC = P007N2_A1224lccbC[0] ;
            A1223lccbD = P007N2_A1223lccbD[0] ;
            A1222lccbI = P007N2_A1222lccbI[0] ;
            A1150lccbE = P007N2_A1150lccbE[0] ;
            A1184lccbS = P007N2_A1184lccbS[0] ;
            n1184lccbS = P007N2_n1184lccbS[0] ;
            A1167lccbG = P007N2_A1167lccbG[0] ;
            n1167lccbG = P007N2_n1167lccbG[0] ;
            /* Optimized DELETE. */
            /* Using cursor P007N3 */
            pr_default.execute(1, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P007N4 */
            pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            /* End optimized DELETE. */
            /* Using cursor P007N5 */
            pr_default.execute(3, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( ( GXutil.strSearch( AV39DebugM, "NODELETE", 1) == 0 ) )
         {
            /* Execute user subroutine: S131 */
            S131 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: S141 */
         S141 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: S151 */
         S151 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV327RetVa = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
   }

   public void S151( )
   {
      /* 'PROCESSAICSI' Routine */
      /* Execute user subroutine: S161 */
      S161 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV169TRNN_ = "000000" ;
      /* Using cursor P007N6 */
      pr_default.execute(4);
      while ( (pr_default.getStatus(4) != 101) )
      {
         A994HntLin = P007N6_A994HntLin[0] ;
         n994HntLin = P007N6_n994HntLin[0] ;
         A993HntUsu = P007N6_A993HntUsu[0] ;
         n993HntUsu = P007N6_n993HntUsu[0] ;
         A997HntSeq = P007N6_A997HntSeq[0] ;
         A998HntSub = P007N6_A998HntSub[0] ;
         AV12Linha = A994HntLin ;
         AV168TRNN_ = GXutil.substring( AV12Linha, 2, 6) ;
         if ( ( GXutil.strcmp(AV169TRNN_, AV168TRNN_) != 0 ) )
         {
            /* Execute user subroutine: S175 */
            S175 ();
            if ( returnInSub )
            {
               pr_default.close(4);
               returnInSub = true;
               if (true) return;
            }
         }
         if ( ( GXutil.len( AV207LccbR) + GXutil.len( AV12Linha) + 2 < 5000 ) && ( AV274flagR == 1 ) )
         {
            AV357Linha = GXutil.substring( AV12Linha, 1, 255) ;
            AV207LccbR = AV207LccbR + GXutil.newLine( ) + GXutil.trim( AV357Linha) ;
         }
         AV127RCID = GXutil.substring( AV12Linha, 1, 1) ;
         if ( ( GXutil.strcmp(AV127RCID, "1") == 0 ) )
         {
            AV35CRS = GXutil.substring( AV12Linha, 8, 4) ;
            AV148SPED = GXutil.substring( AV12Linha, 2, 6) ;
            context.msgStatus( "  Processing File "+AV35CRS+"-"+AV148SPED );
         }
         else if ( ( GXutil.strcmp(AV127RCID, "2") == 0 ) )
         {
            AV272CJCP = GXutil.substring( AV12Linha, 16, 3) ;
            if ( ( GXutil.strcmp(AV272CJCP, "CNJ") == 0 ) )
            {
               if ( ( AV273Count < 10 ) )
               {
                  AV273Count = (byte)(AV273Count+1) ;
                  AV271TDNR_[AV273Count-1] = GXutil.substring( AV12Linha, 35, 10) ;
               }
               else
               {
                  context.msgStatus( "    RET CRITICAL EVENT: More than 10 conjunction tickets in transaction "+AV167TRNN );
               }
            }
            else
            {
               AV8AGTN = GXutil.substring( AV12Linha, 8, 7) ;
               AV158TDNR = GXutil.substring( AV12Linha, 35, 10) ;
               AV152TACN = GXutil.substring( AV12Linha, 68, 3) ;
               AV37DAIS = GXutil.substring( AV12Linha, 23, 6) ;
               AV246PXNM = GXutil.substring( AV12Linha, 75, 49) ;
               AV165TRNC = GXutil.substring( AV12Linha, 48, 4) ;
               if ( ( AV54Flag_I == 0 ) )
               {
                  AV54Flag_I = (byte)(1) ;
                  AV267TRN_C = (int)(AV267TRN_C+1) ;
                  AV221Flag_ = (byte)(0) ;
                  if ( ( GXutil.strSearch( "TKTA,TKTB,TKTT,MCOM,MD50,EMDS,EMDA", AV165TRNC, 1) > 0 ) )
                  {
                     AV78i = 1 ;
                     while ( ( AV78i <= AV204qtdEm ) && ( GXutil.strcmp(GXutil.substring( AV152TACN, 1, 3), AV205ListE[AV78i-1][1-1]) != 0 ) )
                     {
                        AV78i = (int)(AV78i+1) ;
                     }
                     if ( ( AV78i > AV204qtdEm ) )
                     {
                     }
                     else
                     {
                        AV221Flag_ = (byte)(1) ;
                        AV239PLP_D = GXutil.trim( AV205ListE[AV78i-1][2-1]) ;
                        AV240PLP_D = GXutil.trim( AV205ListE[AV78i-1][3-1]) ;
                        AV241PLP_D = GXutil.trim( AV205ListE[AV78i-1][4-1]) ;
                        AV284PLP_D = GXutil.trim( AV205ListE[AV78i-1][5-1]) ;
                        AV289Total = 0 ;
                        AV236PLP_V = 0 ;
                        AV209retfo.clear();
                     }
                  }
               }
               else
               {
                  context.msgStatus( "    RET CRITICAL EVENT: More than one main ticket in transaction "+AV167TRNN );
               }
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "5") == 0 ) && ( AV221Flag_ == 1 ) )
         {
            AV216CUTP = GXutil.substring( AV12Linha, 98, 4) ;
            AV142s = "030,049,068,114,133,152" ;
            AV78i = 1 ;
            while ( ( AV78i <= GXutil.len( AV142s) ) )
            {
               AV88j = (short)(GXutil.val( GXutil.substring( AV142s, AV78i, 3), ".")) ;
               AV219TMFT = GXutil.trim( GXutil.substring( AV12Linha, AV88j, 8)) ;
               if ( ( GXutil.strcmp(GXutil.substring( AV219TMFT, 1, 2), "XT") != 0 ) && ( GXutil.strcmp(GXutil.substring( AV219TMFT, 1, 2), "PD") != 0 ) && ( GXutil.strcmp(AV219TMFT, "") != 0 ) )
               {
                  AV88j = (short)(AV88j+8) ;
                  AV215sAMOU = GXutil.substring( AV12Linha, AV88j, 11) ;
                  /* Execute user subroutine: S185 */
                  S185 ();
                  if ( returnInSub )
                  {
                     pr_default.close(4);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV220Total = (double)(AV220Total+AV214valor) ;
                  AV379Total = (double)(AV379Total+AV214valor) ;
               }
               AV78i = (int)(AV78i+4) ;
            }
         }
         else if ( ( GXutil.strcmp(AV127RCID, "8") == 0 ) && ( AV221Flag_ == 1 ) )
         {
            AV9Count_I = (byte)(AV9Count_I+1) ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( A997HntSeq );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( (byte)(1) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXutil.substring( AV12Linha, 8, 19) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXutil.substring( AV12Linha, 27, 11) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( GXutil.substring( AV12Linha, 38, 6) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( GXutil.substring( AV12Linha, 44, 4) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( GXutil.substring( AV12Linha, 48, 2) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXutil.substring( AV12Linha, 50, 4) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( GXutil.substring( AV12Linha, 60, 4) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( GXutil.substring( AV12Linha, 64, 27) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( GXutil.substring( AV12Linha, 91, 1) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( GXutil.substring( AV12Linha, 92, 2) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( GXutil.substring( AV12Linha, 94, 1) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( GXutil.substring( AV12Linha, 95, 25) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( GXutil.substring( AV12Linha, 120, 11) );
            if ( ( GXutil.strcmp(GXutil.trim( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp()), "") != 0 ) )
            {
               AV290FPTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
               AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
               AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
               /* Execute user subroutine: S195 */
               S195 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
               AV209retfo.add(AV210retfo.Clone(), 0);
            }
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( A997HntSeq );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( (byte)(2) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXutil.substring( AV12Linha, 131, 19) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXutil.substring( AV12Linha, 150, 11) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( GXutil.substring( AV12Linha, 161, 6) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( GXutil.substring( AV12Linha, 167, 4) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( GXutil.substring( AV12Linha, 171, 2) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXutil.substring( AV12Linha, 173, 4) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( GXutil.substring( AV12Linha, 183, 4) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( GXutil.substring( AV12Linha, 187, 27) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( GXutil.substring( AV12Linha, 214, 1) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( GXutil.substring( AV12Linha, 215, 2) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( GXutil.substring( AV12Linha, 217, 1) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( GXutil.substring( AV12Linha, 218, 25) );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( GXutil.substring( AV12Linha, 243, 11) );
            if ( ( GXutil.strcmp(GXutil.trim( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp()), "") != 0 ) )
            {
               AV290FPTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
               AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
               AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
               /* Execute user subroutine: S195 */
               S195 ();
               if ( returnInSub )
               {
                  pr_default.close(4);
                  returnInSub = true;
                  if (true) return;
               }
               AV209retfo.add(AV210retfo.Clone(), 0);
            }
         }
         if ( ( GXutil.strcmp(AV127RCID, "Z") == 0 ) )
         {
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         pr_default.readNext(4);
      }
      pr_default.close(4);
      if ( ( GXutil.strSearch( AV39DebugM, "NOPLP", 1) == 0 ) )
      {
         context.msgStatus( "    Processing PLP Groups of Credit" );
         /* Execute user subroutine: S201 */
         S201 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      context.msgStatus( "  Statistics :" );
      context.msgStatus( "  "+GXutil.trim( GXutil.str( AV267TRN_C, 8, 0))+" Transactions Read" );
      context.msgStatus( "  "+GXutil.trim( GXutil.str( AV182qtdBi, 8, 0))+" BINs  x C. Card Comp.  loaded" );
      context.msgStatus( "  "+GXutil.trim( GXutil.str( AV180qtdFo, 8, 0))+" Forms x GDS x Airlines loaded" );
      context.msgStatus( "  "+GXutil.trim( GXutil.str( AV180qtdFo, 8, 0))+" DOCs  x CC  x Airlines loaded" );
      context.msgStatus( "  "+GXutil.trim( GXutil.str( AV203Count, 8, 0))+" CC Codes Changed based on BIN table" );
      context.msgStatus( "    FOP Type   :    Read / CC ok  / CC ig. / PLP ok / PLP ig./PLP Cash/ PLP CC " );
      context.msgStatus( "      TKT      - "+GXutil.str( AV183TKT_C[1-1], 8, 0)+"/"+GXutil.str( AV183TKT_C[2-1], 8, 0)+"/"+GXutil.str( AV183TKT_C[3-1], 8, 0)+"/"+GXutil.str( AV183TKT_C[4-1], 8, 0)+"/"+GXutil.str( AV183TKT_C[5-1], 8, 0)+"/"+GXutil.str( AV183TKT_C[6-1], 8, 0)+"/"+GXutil.str( AV183TKT_C[7-1], 8, 0) );
      context.msgStatus( "      RFND     - "+GXutil.str( AV184RFN_C[1-1], 8, 0)+"/"+GXutil.str( AV184RFN_C[2-1], 8, 0)+"/"+GXutil.str( AV184RFN_C[3-1], 8, 0)+"/"+GXutil.str( AV184RFN_C[4-1], 8, 0)+"/"+GXutil.str( AV184RFN_C[5-1], 8, 0)+"/"+GXutil.str( AV184RFN_C[6-1], 8, 0)+"/"+GXutil.str( AV184RFN_C[7-1], 8, 0) );
      context.msgStatus( "      MCOM/Etc - "+GXutil.str( AV185MCO_C[1-1], 8, 0)+"/"+GXutil.str( AV185MCO_C[2-1], 8, 0)+"/"+GXutil.str( AV185MCO_C[3-1], 8, 0)+"/"+GXutil.str( AV185MCO_C[4-1], 8, 0)+"/"+GXutil.str( AV185MCO_C[5-1], 8, 0)+"/"+GXutil.str( AV185MCO_C[6-1], 8, 0)+"/"+GXutil.str( AV185MCO_C[7-1], 8, 0) );
   }

   public void S175( )
   {
      /* 'CHECKTRN' Routine */
      if ( ( AV54Flag_I == 1 ) && ( AV221Flag_ == 1 ) )
      {
         /* Execute user subroutine: S211 */
         S211 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV169TRNN_ = AV168TRNN_ ;
      /* Execute user subroutine: S161 */
      S161 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S161( )
   {
      /* 'CLEARTRN' Routine */
      AV54Flag_I = (byte)(0) ;
      AV9Count_I = (byte)(0) ;
      AV209retfo.clear();
      AV207LccbR = "" ;
      AV377lccbF = 0 ;
      AV283ICSII = "" ;
      AV220Total = 0 ;
      AV379Total = 0 ;
      AV289Total = 0 ;
      AV236PLP_V = 0 ;
      AV221Flag_ = (byte)(0) ;
      AV273Count = (byte)(0) ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV271TDNR_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
   }

   public void S141( )
   {
      /* 'CARREGAARRAY' Routine */
      context.msgStatus( "  Loading Auxiliary Tables..." );
      AV204qtdEm = 0 ;
      AV78i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 100 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 5 ) )
         {
            AV205ListE[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      AV391GXLvl = (byte)(0) ;
      /* Using cursor P007N7 */
      pr_default.execute(5);
      while ( (pr_default.getStatus(5) != 101) )
      {
         A1147lccbE = P007N7_A1147lccbE[0] ;
         n1147lccbE = P007N7_n1147lccbE[0] ;
         A1150lccbE = P007N7_A1150lccbE[0] ;
         AV391GXLvl = (byte)(1) ;
         AV238lccbE = A1150lccbE ;
         if ( ( AV204qtdEm < 100 ) )
         {
            AV204qtdEm = (int)(AV204qtdEm+1) ;
            AV205ListE[AV204qtdEm-1][1-1] = AV238lccbE ;
            AV392GXLvl = (byte)(0) ;
            /* Using cursor P007N8 */
            pr_default.execute(6, new Object[] {AV238lccbE});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1166lccbP = P007N8_A1166lccbP[0] ;
               A1150lccbE = P007N8_A1150lccbE[0] ;
               A1165lccbD = P007N8_A1165lccbD[0] ;
               n1165lccbD = P007N8_n1165lccbD[0] ;
               AV392GXLvl = (byte)(1) ;
               AV205ListE[AV204qtdEm-1][2-1] = A1165lccbD ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
            if ( ( AV392GXLvl == 0 ) )
            {
               AV205ListE[AV204qtdEm-1][2-1] = "F" ;
            }
            AV393GXLvl = (byte)(0) ;
            /* Using cursor P007N9 */
            pr_default.execute(7, new Object[] {AV238lccbE});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A1166lccbP = P007N9_A1166lccbP[0] ;
               A1150lccbE = P007N9_A1150lccbE[0] ;
               A1165lccbD = P007N9_A1165lccbD[0] ;
               n1165lccbD = P007N9_n1165lccbD[0] ;
               AV393GXLvl = (byte)(1) ;
               AV205ListE[AV204qtdEm-1][3-1] = A1165lccbD ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
            if ( ( AV393GXLvl == 0 ) )
            {
               AV205ListE[AV204qtdEm-1][3-1] = "F" ;
            }
            AV394GXLvl = (byte)(0) ;
            /* Using cursor P007N10 */
            pr_default.execute(8, new Object[] {AV238lccbE});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A1166lccbP = P007N10_A1166lccbP[0] ;
               A1150lccbE = P007N10_A1150lccbE[0] ;
               A1165lccbD = P007N10_A1165lccbD[0] ;
               n1165lccbD = P007N10_n1165lccbD[0] ;
               AV394GXLvl = (byte)(1) ;
               AV205ListE[AV204qtdEm-1][4-1] = A1165lccbD ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(8);
            if ( ( AV394GXLvl == 0 ) )
            {
               AV205ListE[AV204qtdEm-1][4-1] = "R" ;
            }
            AV395GXLvl = (byte)(0) ;
            /* Using cursor P007N11 */
            pr_default.execute(9, new Object[] {AV238lccbE});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1166lccbP = P007N11_A1166lccbP[0] ;
               A1150lccbE = P007N11_A1150lccbE[0] ;
               A1165lccbD = P007N11_A1165lccbD[0] ;
               n1165lccbD = P007N11_n1165lccbD[0] ;
               AV395GXLvl = (byte)(1) ;
               AV205ListE[AV204qtdEm-1][5-1] = A1165lccbD ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(9);
            if ( ( AV395GXLvl == 0 ) )
            {
               AV205ListE[AV204qtdEm-1][5-1] = "R" ;
            }
         }
         AV78i = (int)(AV78i+1) ;
         pr_default.readNext(5);
      }
      pr_default.close(5);
      if ( ( AV391GXLvl == 0 ) )
      {
         AV145SCETp = "L" ;
         AV342SCEFo = "L" ;
         AV144SCETe = "Alerta|Tabela vazia: lccbEmp" ;
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      if ( ( AV78i != AV204qtdEm ) )
      {
         AV145SCETp = "L" ;
         AV342SCEFo = "L" ;
         AV144SCETe = "Alerta|Estouro da capacidade de Tipos de Documento|Carregados " + GXutil.trim( GXutil.str( AV182qtdBi, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV78i, 10, 0)) ;
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV180qtdFo = 0 ;
      AV78i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 4 ) )
         {
            AV179ListF[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P007N12 */
      pr_default.execute(10);
      while ( (pr_default.getStatus(10) != 101) )
      {
         A1148lccbF = P007N12_A1148lccbF[0] ;
         n1148lccbF = P007N12_n1148lccbF[0] ;
         A1152lccbF = P007N12_A1152lccbF[0] ;
         A1151lccbF = P007N12_A1151lccbF[0] ;
         A1150lccbE = P007N12_A1150lccbE[0] ;
         if ( ( AV180qtdFo < 20000 ) )
         {
            AV180qtdFo = (int)(AV180qtdFo+1) ;
            AV179ListF[AV180qtdFo-1][1-1] = A1150lccbE ;
            AV179ListF[AV180qtdFo-1][2-1] = A1151lccbF ;
            AV179ListF[AV180qtdFo-1][3-1] = GXutil.trim( A1152lccbF) ;
            AV179ListF[AV180qtdFo-1][4-1] = A1148lccbF ;
         }
         AV78i = (int)(AV78i+1) ;
         pr_default.readNext(10);
      }
      pr_default.close(10);
      if ( ( AV78i != AV180qtdFo ) )
      {
         AV145SCETp = "L" ;
         AV342SCEFo = "L" ;
         AV144SCETe = "Alerta|Estouro da capacidade de Forms x GDS|Carregados " + GXutil.trim( GXutil.str( AV180qtdFo, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV78i, 10, 0)) ;
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV225qtdFo = 0 ;
      AV78i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 1000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 3 ) )
         {
            AV224ListF[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P007N13 */
      pr_default.execute(11);
      while ( (pr_default.getStatus(11) != 101) )
      {
         A1294Serie = P007N13_A1294Serie[0] ;
         n1294Serie = P007N13_n1294Serie[0] ;
         A1296Serie = P007N13_A1296Serie[0] ;
         A1295DocCo = P007N13_A1295DocCo[0] ;
         if ( ( AV225qtdFo < 1000 ) )
         {
            AV225qtdFo = (int)(AV225qtdFo+1) ;
            AV224ListF[AV225qtdFo-1][1-1] = GXutil.trim( A1295DocCo) ;
            AV224ListF[AV225qtdFo-1][2-1] = GXutil.trim( GXutil.str( A1296Serie, 10, 0)) ;
            AV224ListF[AV225qtdFo-1][3-1] = GXutil.trim( GXutil.str( A1294Serie, 10, 0)) ;
         }
         AV78i = (int)(AV78i+1) ;
         pr_default.readNext(11);
      }
      pr_default.close(11);
      if ( ( AV78i != AV225qtdFo ) )
      {
         AV145SCETp = "L" ;
         AV342SCEFo = "L" ;
         AV144SCETe = "Alerta|Estouro da capacidade de Tipos de Formul�rios|Carregados " + GXutil.trim( GXutil.str( AV225qtdFo, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV78i, 10, 0)) ;
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV182qtdBi = 0 ;
      AV78i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 1000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 6 ) )
         {
            AV181ListB[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P007N14 */
      pr_default.execute(12);
      while ( (pr_default.getStatus(12) != 101) )
      {
         A1140lccbB = P007N14_A1140lccbB[0] ;
         n1140lccbB = P007N14_n1140lccbB[0] ;
         A1145lccbB = P007N14_A1145lccbB[0] ;
         A1141lccbB = P007N14_A1141lccbB[0] ;
         n1141lccbB = P007N14_n1141lccbB[0] ;
         A1142lccbB = P007N14_A1142lccbB[0] ;
         n1142lccbB = P007N14_n1142lccbB[0] ;
         A1144lccbE = P007N14_A1144lccbE[0] ;
         A1143lccbB = P007N14_A1143lccbB[0] ;
         if ( ( AV182qtdBi < 1000 ) )
         {
            AV182qtdBi = (int)(AV182qtdBi+1) ;
            AV181ListB[AV182qtdBi-1][1-1] = GXutil.trim( A1143lccbB) ;
            AV181ListB[AV182qtdBi-1][2-1] = GXutil.trim( A1144lccbE) ;
            AV181ListB[AV182qtdBi-1][3-1] = GXutil.trim( A1140lccbB) ;
            AV181ListB[AV182qtdBi-1][4-1] = GXutil.trim( GXutil.str( A1145lccbB, 10, 0)) ;
            AV181ListB[AV182qtdBi-1][5-1] = GXutil.trim( A1141lccbB) ;
            AV181ListB[AV182qtdBi-1][6-1] = A1142lccbB ;
         }
         AV78i = (int)(AV78i+1) ;
         pr_default.readNext(12);
      }
      pr_default.close(12);
      if ( ( AV78i != AV182qtdBi ) )
      {
         AV145SCETp = "L" ;
         AV342SCEFo = "L" ;
         AV144SCETe = "Alerta|Estouro da capacidade de Bins|Carregados " + GXutil.trim( GXutil.str( AV182qtdBi, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV78i, 10, 0)) ;
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV188qtdDo = 0 ;
      AV78i = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 5000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 3 ) )
         {
            AV187ListD[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      /* Using cursor P007N15 */
      pr_default.execute(13);
      while ( (pr_default.getStatus(13) != 101) )
      {
         A1149lccbD = P007N15_A1149lccbD[0] ;
         n1149lccbD = P007N15_n1149lccbD[0] ;
         A1154lccbD = P007N15_A1154lccbD[0] ;
         A1153lccbD = P007N15_A1153lccbD[0] ;
         A1150lccbE = P007N15_A1150lccbE[0] ;
         if ( ( AV188qtdDo < 5000 ) )
         {
            AV188qtdDo = (int)(AV188qtdDo+1) ;
            AV187ListD[AV188qtdDo-1][1-1] = A1150lccbE ;
            AV187ListD[AV188qtdDo-1][2-1] = A1153lccbD ;
            AV187ListD[AV188qtdDo-1][3-1] = A1154lccbD ;
         }
         AV78i = (int)(AV78i+1) ;
         pr_default.readNext(13);
      }
      pr_default.close(13);
      if ( ( AV78i != AV188qtdDo ) )
      {
         AV145SCETp = "L" ;
         AV342SCEFo = "L" ;
         AV144SCETe = "Alerta|Estouro da capacidade de Tipos de Documento|Carregados " + GXutil.trim( GXutil.str( AV182qtdBi, 10, 0)) + "|de " + GXutil.trim( GXutil.str( AV78i, 10, 0)) ;
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S221( )
   {
      /* 'ERROARRAY' Routine */
      AV106now = GXutil.now(true, false) ;
      /*
         INSERT RECORD ON TABLE SCEVENTS

      */
      A1298SCETk = "" ;
      n1298SCETk = false ;
      GXt_dtime11 = A1299SCEDa ;
      GXv_date12[0] = GXutil.resetTime(GXt_dtime11) ;
      new pr2string2date(remoteHandle, context).execute( AV37DAIS, GXv_date12) ;
      areticsi_bkp.this.GXt_dtime11 = GXutil.resetTime( GXv_date12[0] );
      A1299SCEDa = GXt_dtime11 ;
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1303SCERE = GXutil.substring( AV178Filen, 1, 20) ;
      n1303SCERE = false ;
      A1305SCETe = GXutil.substring( AV144SCETe, 1, 150) ;
      n1305SCETe = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = GXutil.substring( AV145SCETp, 1, 3) ;
      n1310SCETp = false ;
      A1306SCEAP = "ICSI" ;
      n1306SCEAP = false ;
      A1311SCEVe = GXutil.substring( AV153Versa, 1, 5) ;
      n1311SCEVe = false ;
      A1517SCEFP = AV373SCEFP ;
      n1517SCEFP = false ;
      A1518SCEPa = AV374SCEPa ;
      n1518SCEPa = false ;
      /* Using cursor P007N16 */
      pr_default.execute(14, new Object[] {new Boolean(n1298SCETk), A1298SCETk, new Boolean(n1299SCEDa), A1299SCEDa, new Boolean(n1300SCECR), A1300SCECR, new Boolean(n1301SCEAi), A1301SCEAi, new Boolean(n1303SCERE), A1303SCERE, new Boolean(n1305SCETe), A1305SCETe, new Boolean(n1306SCEAP), A1306SCEAP, new Boolean(n1307SCEIa), A1307SCEIa, new Boolean(n1310SCETp), A1310SCETp, new Boolean(n1311SCEVe), A1311SCEVe, new Boolean(n1517SCEFP), new Double(A1517SCEFP), new Boolean(n1518SCEPa), new Double(A1518SCEPa)});
      /* Retrieving last key number assigned */
      /* Using cursor P007N17 */
      pr_default.execute(15);
      A1312SCEId = P007N17_A1312SCEId[0] ;
      n1312SCEId = P007N17_n1312SCEId[0] ;
      pr_default.close(15);
      if ( (pr_default.getStatus(14) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      AV373SCEFP = 0 ;
      AV374SCEPa = 0 ;
      if ( ( GXutil.strSearch( AV39DebugM, "SAYALL", 1) > 0 ) )
      {
         context.msgStatus( "    "+A1305SCETe );
      }
   }

   public void S23516( )
   {
      /* 'GRAVALOG' Routine */
      AV106now = GXutil.now(true, false) ;
      if ( ( GXutil.strSearch( AV39DebugM, "DEBUG", 1) > 0 ) || ( GXutil.strcmp(GXutil.left( AV145SCETp, 1), "4") == 0 ) || ( GXutil.strcmp(GXutil.left( AV145SCETp, 1), "9") == 0 ) )
      {
         AV354Exist = "N" ;
         GXt_dtime11 = AV355SCEDa ;
         GXv_date12[0] = GXutil.resetTime(GXt_dtime11) ;
         new pr2string2date(remoteHandle, context).execute( AV37DAIS, GXv_date12) ;
         areticsi_bkp.this.GXt_dtime11 = GXutil.resetTime( GXv_date12[0] );
         AV355SCEDa = GXt_dtime11 ;
         /* Using cursor P007N18 */
         pr_default.execute(16, new Object[] {AV158TDNR, AV355SCEDa});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A1309SCEFo = P007N18_A1309SCEFo[0] ;
            n1309SCEFo = P007N18_n1309SCEFo[0] ;
            A1299SCEDa = P007N18_A1299SCEDa[0] ;
            n1299SCEDa = P007N18_n1299SCEDa[0] ;
            A1298SCETk = P007N18_A1298SCETk[0] ;
            n1298SCETk = P007N18_n1298SCETk[0] ;
            A1312SCEId = P007N18_A1312SCEId[0] ;
            n1312SCEId = P007N18_n1312SCEId[0] ;
            AV354Exist = "Y" ;
            pr_default.readNext(16);
         }
         pr_default.close(16);
         if ( ( GXutil.strcmp(AV354Exist, "N") == 0 ) )
         {
            /*
               INSERT RECORD ON TABLE SCEVENTS

            */
            A1298SCETk = GXutil.substring( AV158TDNR, 1, 10) ;
            n1298SCETk = false ;
            A1299SCEDa = AV355SCEDa ;
            n1299SCEDa = false ;
            A1300SCECR = GXutil.substring( AV35CRS, 1, 5) ;
            n1300SCECR = false ;
            A1301SCEAi = GXutil.substring( AV152TACN, 1, 3) ;
            n1301SCEAi = false ;
            A1303SCERE = GXutil.substring( AV178Filen, 1, 50) ;
            n1303SCERE = false ;
            A1305SCETe = GXutil.substring( AV144SCETe, 1, 150) ;
            n1305SCETe = false ;
            A1307SCEIa = GXutil.substring( AV8AGTN, 1, 11) ;
            n1307SCEIa = false ;
            A1310SCETp = GXutil.substring( AV145SCETp, 1, 3) ;
            n1310SCETp = false ;
            A1306SCEAP = "ICSI" ;
            n1306SCEAP = false ;
            A1311SCEVe = GXutil.substring( AV153Versa, 1, 5) ;
            n1311SCEVe = false ;
            A1309SCEFo = AV342SCEFo ;
            n1309SCEFo = false ;
            A1517SCEFP = AV373SCEFP ;
            n1517SCEFP = false ;
            A1518SCEPa = AV374SCEPa ;
            n1518SCEPa = false ;
            /* Using cursor P007N19 */
            pr_default.execute(17, new Object[] {new Boolean(n1298SCETk), A1298SCETk, new Boolean(n1299SCEDa), A1299SCEDa, new Boolean(n1300SCECR), A1300SCECR, new Boolean(n1301SCEAi), A1301SCEAi, new Boolean(n1303SCERE), A1303SCERE, new Boolean(n1305SCETe), A1305SCETe, new Boolean(n1306SCEAP), A1306SCEAP, new Boolean(n1307SCEIa), A1307SCEIa, new Boolean(n1309SCEFo), A1309SCEFo, new Boolean(n1310SCETp), A1310SCETp, new Boolean(n1311SCEVe), A1311SCEVe, new Boolean(n1517SCEFP), new Double(A1517SCEFP), new Boolean(n1518SCEPa), new Double(A1518SCEPa)});
            /* Retrieving last key number assigned */
            /* Using cursor P007N20 */
            pr_default.execute(18);
            A1312SCEId = P007N20_A1312SCEId[0] ;
            n1312SCEId = P007N20_n1312SCEId[0] ;
            pr_default.close(18);
            if ( (pr_default.getStatus(17) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
            AV373SCEFP = 0 ;
            AV374SCEPa = 0 ;
         }
      }
      if ( ( GXutil.strSearch( AV39DebugM, "SAYALL", 1) > 0 ) )
      {
         context.msgStatus( "    "+AV158TDNR+":"+AV144SCETe );
      }
   }

   public void S195( )
   {
      /* 'SUMCASH' Routine */
      if ( ( GXutil.strcmp(GXutil.left( AV290FPTP, 2), "CA") == 0 ) || ( GXutil.strcmp(GXutil.left( AV290FPTP, 4), "MSCA") == 0 ) )
      {
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV289Total = (double)(AV289Total+AV214valor) ;
      }
      if ( ( GXutil.strcmp(GXutil.left( AV290FPTP, 2), "CC") == 0 ) )
      {
         AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
         AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment( AV214valor );
      }
      if ( ( GXutil.strcmp(GXutil.left( AV290FPTP, 2), "CC") == 0 ) || ( GXutil.strcmp(GXutil.left( AV290FPTP, 4), "MSCC") == 0 ) )
      {
         AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
         AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal( AV214valor );
      }
   }

   public void S24590( )
   {
      /* 'GETEMPCC' Routine */
      AV206retva = (byte)(1) ;
      /* Execute user subroutine: S251 */
      S251 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( AV206retva == 1 ) )
      {
         AV78i = 1 ;
         while ( ( AV78i <= AV188qtdDo ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV152TACN, 1, 3), AV187ListD[AV78i-1][1-1]) == 0 ) && ( GXutil.strcmp(AV200EmpCC, AV187ListD[AV78i-1][2-1]) == 0 ) )
            {
               if ( ( GXutil.strcmp(AV165TRNC, AV187ListD[AV78i-1][3-1]) == 0 ) || ( GXutil.strcmp(GXutil.trim( AV187ListD[AV78i-1][3-1]), "*") == 0 ) )
               {
                  if (true) break;
               }
            }
            AV78i = (int)(AV78i+1) ;
         }
         if ( ( AV78i > AV188qtdDo ) )
         {
            AV206retva = (byte)(0) ;
            AV200EmpCC = "" ;
         }
      }
      if ( ( AV206retva == 1 ) )
      {
         AV78i = 1 ;
         while ( ( AV78i <= AV225qtdFo ) && ( GXutil.val( AV158TDNR, ".") >= GXutil.val( AV224ListF[AV78i-1][2-1], ".") ) && ( GXutil.val( AV158TDNR, ".") <= GXutil.val( AV224ListF[AV78i-1][3-1], ".") ) )
         {
            AV78i = (int)(AV78i+1) ;
         }
         if ( ( AV78i <= AV225qtdFo ) )
         {
            AV142s = GXutil.trim( AV224ListF[AV78i-1][1-1]) ;
            AV78i = 1 ;
            while ( ( AV78i <= AV180qtdFo ) )
            {
               if ( ( GXutil.strcmp(AV179ListF[AV78i-1][1-1], GXutil.substring( AV152TACN, 1, 3)) == 0 ) && ( GXutil.strcmp(AV179ListF[AV78i-1][2-1], AV35CRS) == 0 ) && ( ( GXutil.strcmp(AV179ListF[AV78i-1][3-1], AV142s) == 0 ) || ( GXutil.strcmp(AV179ListF[AV78i-1][3-1], "*") == 0 ) ) && ( GXutil.strcmp(AV179ListF[AV78i-1][4-1], "1") == 0 ) )
               {
                  if (true) break;
               }
               AV78i = (int)(AV78i+1) ;
            }
            if ( ( AV78i > AV180qtdFo ) )
            {
               AV206retva = (byte)(0) ;
            }
         }
         else
         {
            AV145SCETp = "L" ;
            AV342SCEFo = "L" ;
            AV144SCETe = "Tipo de Formul�rio para " + AV158TDNR + " n�o encontrado, aceito por default" ;
            /* Execute user subroutine: S23516 */
            S23516 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
      if ( ( AV206retva == 0 ) )
      {
         AV200EmpCC = "" ;
      }
   }

   public void S211( )
   {
      /* 'CHECKFOP' Routine */
      AV372lccbC = 0 ;
      AV372lccbC = AV289Total ;
      if ( ( AV289Total > 0 ) )
      {
         AV358Total = (double)(AV220Total-AV289Total) ;
         if ( ( GXutil.roundDecimal( DecimalUtil.doubleToDec(java.lang.Math.abs( AV358Total)), 2).doubleValue() == 0 ) )
         {
            AV220Total = 0 ;
            AV360FareC[AV361Count-1] = GXutil.trim( AV158TDNR) + "$" + GXutil.trim( GXutil.str( AV289Total, 14, 2)) ;
            AV361Count = (int)(AV361Count+1) ;
         }
         else if ( ( GXutil.truncDecimal( DecimalUtil.doubleToDec(AV289Total*100), 0).compareTo(GXutil.truncDecimal( DecimalUtil.doubleToDec(AV220Total*100), 0)) < 0 ) )
         {
            /* Execute user subroutine: S261 */
            S261 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( ( GXutil.strcmp(AV383Bilhe, "N") == 0 ) )
            {
               AV381Index = (int)(AV381Index+1) ;
               AV382Bilhe[AV381Index-1] = AV158TDNR ;
               AV220Total = (double)(AV220Total-AV289Total) ;
               AV360FareC[AV361Count-1] = GXutil.trim( AV158TDNR) + "$" + GXutil.trim( GXutil.str( AV289Total, 14, 2)) ;
               AV361Count = (int)(AV361Count+1) ;
               AV145SCETp = "908" ;
               AV144SCETe = "Parte CASH n�o comporta as taxas. A venda ser� processada mesmo assim. Cash=" + GXutil.trim( GXutil.str( AV289Total, 14, 2)) + ", Taxas=" + GXutil.trim( GXutil.str( AV220Total, 14, 2)) ;
               AV342SCEFo = "L" ;
               /* Execute user subroutine: S23516 */
               S23516 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
         else if ( ( GXutil.truncDecimal( DecimalUtil.doubleToDec(AV289Total*100), 0).compareTo(GXutil.truncDecimal( DecimalUtil.doubleToDec(AV220Total*100), 0)) > 0 ) )
         {
            AV358Total = 0 ;
            AV358Total = AV289Total ;
            AV360FareC[AV361Count-1] = GXutil.trim( AV158TDNR) + "|" + GXutil.padl( GXutil.trim( GXutil.str( AV220Total, 14, 2)), (short)(17), " ") + "@" + GXutil.trim( GXutil.str( AV289Total, 14, 2)) + "*" ;
            AV361Count = (int)(AV361Count+1) ;
         }
      }
      AV211old_F = "" ;
      AV270ifop = 1 ;
      while ( ( AV270ifop <= AV209retfo.size() ) )
      {
         AV218fop2I = 1 ;
         /* Execute user subroutine: S271 */
         S271 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV210retfo = ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV209retfo.elementAt(-1+AV270ifop)).Clone() ;
         AV212this_ = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
         if ( ( AV270ifop < AV209retfo.size() ) )
         {
            AV213next_ = GXutil.trim( ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV209retfo.elementAt(-1+AV270ifop+1)).getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp()) ;
            AV217retfo = ((Sdtsdt_RETFOP_sdt_RETFOPItem)AV209retfo.elementAt(-1+AV270ifop+1)).Clone() ;
         }
         else
         {
            AV213next_ = "" ;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV212this_, 1, 2), "CC") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV212this_, 3, 2), "GR") != 0 ) && ( GXutil.strcmp(GXutil.substring( AV213next_, 1, 4), "MSCC") != 0 ) )
         {
            AV243FlagI = (byte)(1) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV213next_, 1, 2), "MS") == 0 ) )
            {
               if ( ( GXutil.strcmp(GXutil.substring( AV213next_, 3, 2), "CA") == 0 ) )
               {
                  AV285lccbF = "" ;
                  AV261lccbP = (short)(0) ;
                  AV263lccbS = "Venda Cart�o CC/MSCA submetida normalmente" ;
                  AV295FlagA = (byte)(1) ;
                  /* Execute user subroutine: S281 */
                  S281 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               else
               {
                  AV145SCETp = "903" ;
                  AV144SCETe = "Combina��o [" + GXutil.substring( AV212this_, 1, 2) + "/" + GXutil.substring( AV213next_, 1, 4) + "] inv�lida. Apenas CC/MSCC e CC/MSCA permitidas." ;
                  AV342SCEFo = "L" ;
                  /* Execute user subroutine: S23516 */
                  S23516 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            else
            {
               AV285lccbF = "" ;
               AV261lccbP = (short)(0) ;
               AV263lccbS = "Venda Cart�o CC submetida normalmente" ;
               AV295FlagA = (byte)(1) ;
               /* Execute user subroutine: S281 */
               S281 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
         else if ( ( GXutil.strcmp(GXutil.substring( AV212this_, 1, 2), "CC") == 0 ) && ( GXutil.strcmp(GXutil.substring( AV212this_, 3, 2), "GR") != 0 ) && ( GXutil.strcmp(GXutil.substring( AV213next_, 1, 4), "MSCC") == 0 ) && ( GXutil.len( GXutil.trim( GXutil.substring( AV212this_, 3, 2))) == 2 ) )
         {
            /* Execute user subroutine: S291 */
            S291 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
         }
         AV211old_F = AV212this_ ;
         AV270ifop = (int)(AV270ifop+1) ;
      }
   }

   public void S271( )
   {
      /* 'INCFOPSTATS' Routine */
      if ( ( GXutil.strcmp(GXutil.substring( AV165TRNC, 1, 3), "TKT") == 0 ) )
      {
         AV183TKT_C[AV218fop2I-1] = (int)(AV183TKT_C[AV218fop2I-1]+1) ;
      }
      else if ( ( GXutil.strcmp(GXutil.substring( AV165TRNC, 1, 3), "RFN") == 0 ) )
      {
         AV184RFN_C[AV218fop2I-1] = (int)(AV184RFN_C[AV218fop2I-1]+1) ;
      }
      else
      {
         AV185MCO_C[AV218fop2I-1] = (int)(AV185MCO_C[AV218fop2I-1]+1) ;
      }
   }

   public void S30799( )
   {
      /* 'ADDPLPSTATUS' Routine */
      AV279lccbS = (short)(1) ;
      while ( ( AV279lccbS <= 100 ) )
      {
         AV280flagR = (byte)(0) ;
         /*
            INSERT RECORD ON TABLE LCCBPLP1

         */
         A1229lccbS = GXutil.now(true, false) ;
         A1230lccbS = AV279lccbS ;
         A1186lccbS = GXutil.substring( AV278lccbS, 1, 8) ;
         n1186lccbS = false ;
         A1187lccbS = GXutil.substring( AV263lccbS, 1, 120) ;
         n1187lccbS = false ;
         A1188lccbS = GXutil.substring( AV296lccbS, 1, 4) ;
         n1188lccbS = false ;
         /* Using cursor P007N21 */
         pr_default.execute(19, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS, new Boolean(n1188lccbS), A1188lccbS});
         if ( (pr_default.getStatus(19) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            AV280flagR = (byte)(1) ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         if ( ( AV280flagR == 0 ) )
         {
            if (true) break;
         }
         AV279lccbS = (short)(AV279lccbS+1) ;
      }
   }

   public void S281( )
   {
      /* 'CHECKCC' Routine */
      /* Execute user subroutine: S24590 */
      S24590 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV358Total = (double)(AV220Total-AV289Total) ;
      if ( ( AV358Total == 0 ) )
      {
         AV220Total = 0 ;
      }
      else if ( ( GXutil.truncDecimal( DecimalUtil.doubleToDec(AV289Total*100), 0).compareTo(GXutil.truncDecimal( DecimalUtil.doubleToDec(AV220Total*100), 0)) < 0 ) )
      {
         /* Execute user subroutine: S261 */
         S261 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(AV383Bilhe, "N") == 0 ) )
         {
            AV220Total = (double)(AV220Total-AV289Total) ;
            AV381Index = (int)(AV381Index+1) ;
            AV382Bilhe[AV381Index-1] = AV158TDNR ;
         }
      }
      else if ( ( GXutil.truncDecimal( DecimalUtil.doubleToDec(AV289Total*100), 0).compareTo(GXutil.truncDecimal( DecimalUtil.doubleToDec(AV220Total*100), 0)) > 0 ) )
      {
         AV220Total = 0 ;
      }
      if ( ( GXutil.strcmp(AV200EmpCC, "") == 0 ) )
      {
         if ( ( AV243FlagI == 1 ) )
         {
            AV218fop2I = 3 ;
            /* Execute user subroutine: S271 */
            S271 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
      else
      {
         if ( ( AV243FlagI == 1 ) )
         {
            AV218fop2I = 2 ;
            /* Execute user subroutine: S271 */
            S271 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
         AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV264lccbS = AV214valor ;
         AV376lccbO = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment() ;
         AV377lccbF = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal() ;
         AV338lccbO = AV379Total ;
         AV283ICSII = "CC : FPTP=[" + AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() + "]/FPAM=[" + AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() + "]" ;
         /* Execute user subroutine: S311 */
         S311 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(AV323FileD, "N") == 0 ) )
         {
            if ( ( AV264lccbS > 0.00 ) )
            {
               AV321SaleA = (double)(AV264lccbS-AV220Total) ;
               if ( ( AV321SaleA == 0.00 ) )
               {
                  AV220Total = 0 ;
               }
               else
               {
                  if ( ( ( AV220Total / (double) ( AV321SaleA ) ) >= 0.4 ) )
                  {
                     AV145SCETp = "909" ;
                     AV144SCETe = "Total de Taxas maior que 40% do total (Redecard). Taxas somadas ao valor Tarifa (Taxas=" + GXutil.trim( GXutil.str( AV220Total, 14, 2)) + ", Total=" + GXutil.trim( GXutil.str( AV264lccbS, 14, 2)) + ")" ;
                     AV342SCEFo = "L" ;
                     /* Execute user subroutine: S23516 */
                     S23516 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                     AV220Total = 0 ;
                  }
               }
               AV262lccbS = "TOSUB" ;
            }
            GXt_char10 = AV328LccbC ;
            GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
            GXv_char2[0] = GXt_char10 ;
            new pr2onlynumbers(remoteHandle, context).execute( GXv_char3, GXv_char2) ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXv_char3[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
            AV328LccbC = GXt_char10 ;
            AV329Atrib = GXutil.trim( AV328LccbC) ;
            GXv_char3[0] = AV330Resul ;
            new pcrypto(remoteHandle, context).execute( AV329Atrib, "E", GXv_char3) ;
            areticsi_bkp.this.AV330Resul = GXv_char3[0] ;
            AV328LccbC = AV330Resul ;
            AV370lccbC = 0 ;
            AV370lccbC = (double)(AV264lccbS-AV257lccbT) ;
            /*
               INSERT RECORD ON TABLE LCCBPLP

            */
            A1222lccbI = GXutil.substring( AV8AGTN, 1, 7) ;
            A1150lccbE = GXutil.substring( AV152TACN, 1, 3) ;
            GXt_dtime11 = GXutil.resetTime( A1223lccbD );
            GXv_date12[0] = GXutil.resetTime(GXt_dtime11) ;
            new pr2string2date(remoteHandle, context).execute( AV37DAIS, GXv_date12) ;
            areticsi_bkp.this.GXt_dtime11 = GXutil.resetTime( GXv_date12[0] );
            A1223lccbD = GXt_dtime11 ;
            A1224lccbC = GXutil.substring( AV200EmpCC, 1, 2) ;
            A1225lccbC = AV328LccbC ;
            A1226lccbA = GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc(), 1, 20) ;
            A1227lccbO = "S" ;
            A1228lccbF = GXutil.substring( AV285lccbF, 1, 19) ;
            A1163lccbP = "" ;
            n1163lccbP = false ;
            A1168lccbI = (short)(1) ;
            n1168lccbI = false ;
            A1169lccbD = 0 ;
            n1169lccbD = false ;
            A1170lccbI = 0 ;
            n1170lccbI = false ;
            A1172lccbS = AV264lccbS ;
            n1172lccbS = false ;
            A1179lccbV = GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda(), 3, 2) + GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda(), 1, 2) ;
            n1179lccbV = false ;
            A1180lccbC = GXutil.substring( AV246PXNM, 1, 50) ;
            n1180lccbC = false ;
            A1181lccbC = GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp(), 1, 3) ;
            n1181lccbC = false ;
            A1182lccbS = "" ;
            n1182lccbS = false ;
            A1167lccbG = GXutil.substring( AV35CRS, 1, 4) ;
            n1167lccbG = false ;
            A1171lccbT = AV220Total ;
            n1171lccbT = false ;
            A1183lccbP = AV261lccbP ;
            n1183lccbP = false ;
            A1184lccbS = GXutil.substring( AV262lccbS, 1, 8) ;
            n1184lccbS = false ;
            A1189lccbS = Gx_date ;
            n1189lccbS = false ;
            A1495lccbC = GXutil.resetTime(GXutil.serverNow( context, remoteHandle, "DEFAULT")) ;
            n1495lccbC = false ;
            A1490Distr = "" ;
            n1490Distr = false ;
            A1192lccbS = GXutil.substring( AV178Filen, 1, 20) ;
            n1192lccbS = false ;
            A1178lccbO = ((AV372lccbC>0) ? AV377lccbF : AV377lccbF+AV372lccbC) ;
            n1178lccbO = false ;
            A1176lccbO = AV337lccbO ;
            n1176lccbO = false ;
            A1177lccbO = AV338lccbO ;
            n1177lccbO = false ;
            A1515lccbC = AV372lccbC ;
            n1515lccbC = false ;
            A1514lccbF = (double)((A1178lccbO+A1515lccbC)-A1177lccbO) ;
            n1514lccbF = false ;
            A1516lccbC = AV370lccbC ;
            n1516lccbC = false ;
            A1519lccbO = AV376lccbO ;
            n1519lccbO = false ;
            AV247lccbi = GXutil.substring( A1222lccbI, 1, 7) ;
            AV238lccbE = GXutil.substring( A1150lccbE, 1, 3) ;
            AV248lccbD = A1223lccbD ;
            AV249lccbC = GXutil.substring( A1224lccbC, 1, 2) ;
            AV250lccbC = GXutil.substring( A1225lccbC, 1, 44) ;
            AV251lccbA = GXutil.substring( A1226lccbA, 1, 20) ;
            AV266lccbO = GXutil.substring( A1227lccbO, 1, 1) ;
            AV285lccbF = GXutil.substring( A1228lccbF, 1, 19) ;
            AV278lccbS = GXutil.substring( AV262lccbS, 1, 8) ;
            AV296lccbS = "CC" ;
            /* Execute user subroutine: S30799 */
            S30799 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /*
               INSERT RECORD ON TABLE LCCBPLP2

            */
            A1232lccbT = GXutil.substring( AV165TRNC, 1, 4) ;
            A1231lccbT = GXutil.substring( AV158TDNR, 1, 20) ;
            A1207lccbP = GXutil.substring( AV246PXNM, 1, 50) ;
            n1207lccbP = false ;
            A1208lccbR = GXutil.substring( AV178Filen, 1, 20) ;
            n1208lccbR = false ;
            if ( ( AV274flagR == 1 ) )
            {
               A1209lccbR = GXutil.substring( AV207LccbR, 1, 5000) ;
               n1209lccbR = false ;
            }
            else
            {
               if ( ( AV282flagR == 1 ) )
               {
                  A1209lccbR = GXutil.substring( AV283ICSII, 1, 5000) ;
                  n1209lccbR = false ;
               }
               else
               {
                  A1209lccbR = "" ;
                  n1209lccbR = false ;
               }
            }
            /* Using cursor P007N22 */
            pr_default.execute(20, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR, new Boolean(n1209lccbR), A1209lccbR});
            if ( (pr_default.getStatus(20) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               /* Using cursor P007N23 */
               pr_default.execute(21, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
               while ( (pr_default.getStatus(21) != 101) )
               {
                  A1150lccbE = P007N23_A1150lccbE[0] ;
                  A1222lccbI = P007N23_A1222lccbI[0] ;
                  A1223lccbD = P007N23_A1223lccbD[0] ;
                  A1224lccbC = P007N23_A1224lccbC[0] ;
                  A1225lccbC = P007N23_A1225lccbC[0] ;
                  A1226lccbA = P007N23_A1226lccbA[0] ;
                  A1227lccbO = P007N23_A1227lccbO[0] ;
                  A1228lccbF = P007N23_A1228lccbF[0] ;
                  A1231lccbT = P007N23_A1231lccbT[0] ;
                  A1232lccbT = P007N23_A1232lccbT[0] ;
                  A1207lccbP = P007N23_A1207lccbP[0] ;
                  n1207lccbP = P007N23_n1207lccbP[0] ;
                  A1208lccbR = P007N23_A1208lccbR[0] ;
                  n1208lccbR = P007N23_n1208lccbR[0] ;
                  A1209lccbR = P007N23_A1209lccbR[0] ;
                  n1209lccbR = P007N23_n1209lccbR[0] ;
                  A1207lccbP = GXutil.substring( AV246PXNM, 1, 50) ;
                  n1207lccbP = false ;
                  A1208lccbR = GXutil.substring( AV178Filen, 1, 20) ;
                  n1208lccbR = false ;
                  if ( ( AV274flagR == 1 ) )
                  {
                     A1209lccbR = GXutil.substring( AV207LccbR, 1, 5000) ;
                     n1209lccbR = false ;
                  }
                  else
                  {
                     if ( ( AV282flagR == 1 ) )
                     {
                        A1209lccbR = GXutil.substring( AV283ICSII, 1, 5000) ;
                        n1209lccbR = false ;
                     }
                     else
                     {
                        A1209lccbR = "" ;
                        n1209lccbR = false ;
                     }
                  }
                  /* Using cursor P007N24 */
                  pr_default.execute(22, new Object[] {new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR, new Boolean(n1209lccbR), A1209lccbR, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(21);
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
            while ( ( AV273Count > 0 ) )
            {
               /*
                  INSERT RECORD ON TABLE LCCBPLP2

               */
               A1232lccbT = "CNJ" ;
               A1231lccbT = GXutil.substring( AV271TDNR_[AV273Count-1], 1, 10) ;
               A1207lccbP = GXutil.substring( AV246PXNM, 1, 50) ;
               n1207lccbP = false ;
               A1208lccbR = GXutil.substring( AV178Filen, 1, 20) ;
               n1208lccbR = false ;
               /* Using cursor P007N25 */
               pr_default.execute(23, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR});
               if ( (pr_default.getStatus(23) == 1) )
               {
                  Gx_err = (short)(1) ;
                  Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                  /* Optimized UPDATE. */
                  /* Using cursor P007N26 */
                  pr_default.execute(24, new Object[] {AV246PXNM, AV178Filen, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
                  /* End optimized UPDATE. */
               }
               else
               {
                  Gx_err = (short)(0) ;
                  Gx_emsg = "" ;
               }
               /* End Insert */
               AV273Count = (byte)(AV273Count-1) ;
            }
            /* Using cursor P007N27 */
            pr_default.execute(25, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, new Boolean(n1167lccbG), A1167lccbG, new Boolean(n1163lccbP), A1163lccbP, new Boolean(n1168lccbI), new Short(A1168lccbI), new Boolean(n1169lccbD), new Double(A1169lccbD), new Boolean(n1170lccbI), new Double(A1170lccbI), new Boolean(n1171lccbT), new Double(A1171lccbT), new Boolean(n1172lccbS), new Double(A1172lccbS), new Boolean(n1176lccbO), new Double(A1176lccbO), new Boolean(n1177lccbO), new Double(A1177lccbO), new Boolean(n1178lccbO), new Double(A1178lccbO), new Boolean(n1179lccbV), A1179lccbV, new Boolean(n1180lccbC), A1180lccbC, new Boolean(n1181lccbC), A1181lccbC, new Boolean(n1182lccbS), A1182lccbS, new Boolean(n1183lccbP), new Short(A1183lccbP), new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1189lccbS), A1189lccbS, new Boolean(n1192lccbS), A1192lccbS, new Boolean(n1495lccbC), A1495lccbC, new Boolean(n1490Distr), A1490Distr, new Boolean(n1514lccbF), new Double(A1514lccbF), new Boolean(n1515lccbC), new Double(A1515lccbC), new Boolean(n1516lccbC), new Double(A1516lccbC), new Boolean(n1519lccbO), new Double(A1519lccbO)});
            if ( (pr_default.getStatus(25) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               /* Using cursor P007N28 */
               pr_default.execute(26, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               while ( (pr_default.getStatus(26) != 101) )
               {
                  A1150lccbE = P007N28_A1150lccbE[0] ;
                  A1222lccbI = P007N28_A1222lccbI[0] ;
                  A1223lccbD = P007N28_A1223lccbD[0] ;
                  A1224lccbC = P007N28_A1224lccbC[0] ;
                  A1225lccbC = P007N28_A1225lccbC[0] ;
                  A1226lccbA = P007N28_A1226lccbA[0] ;
                  A1227lccbO = P007N28_A1227lccbO[0] ;
                  A1228lccbF = P007N28_A1228lccbF[0] ;
                  A1183lccbP = P007N28_A1183lccbP[0] ;
                  n1183lccbP = P007N28_n1183lccbP[0] ;
                  A1184lccbS = P007N28_A1184lccbS[0] ;
                  n1184lccbS = P007N28_n1184lccbS[0] ;
                  A1163lccbP = P007N28_A1163lccbP[0] ;
                  n1163lccbP = P007N28_n1163lccbP[0] ;
                  A1168lccbI = P007N28_A1168lccbI[0] ;
                  n1168lccbI = P007N28_n1168lccbI[0] ;
                  A1169lccbD = P007N28_A1169lccbD[0] ;
                  n1169lccbD = P007N28_n1169lccbD[0] ;
                  A1170lccbI = P007N28_A1170lccbI[0] ;
                  n1170lccbI = P007N28_n1170lccbI[0] ;
                  A1171lccbT = P007N28_A1171lccbT[0] ;
                  n1171lccbT = P007N28_n1171lccbT[0] ;
                  A1178lccbO = P007N28_A1178lccbO[0] ;
                  n1178lccbO = P007N28_n1178lccbO[0] ;
                  A1177lccbO = P007N28_A1177lccbO[0] ;
                  n1177lccbO = P007N28_n1177lccbO[0] ;
                  A1172lccbS = P007N28_A1172lccbS[0] ;
                  n1172lccbS = P007N28_n1172lccbS[0] ;
                  A1515lccbC = P007N28_A1515lccbC[0] ;
                  n1515lccbC = P007N28_n1515lccbC[0] ;
                  A1519lccbO = P007N28_A1519lccbO[0] ;
                  n1519lccbO = P007N28_n1519lccbO[0] ;
                  A1514lccbF = P007N28_A1514lccbF[0] ;
                  n1514lccbF = P007N28_n1514lccbF[0] ;
                  A1183lccbP = AV261lccbP ;
                  n1183lccbP = false ;
                  A1184lccbS = GXutil.substring( AV262lccbS, 1, 8) ;
                  n1184lccbS = false ;
                  A1163lccbP = "" ;
                  n1163lccbP = false ;
                  A1168lccbI = (short)(1) ;
                  n1168lccbI = false ;
                  A1169lccbD = 0 ;
                  n1169lccbD = false ;
                  A1170lccbI = 0 ;
                  n1170lccbI = false ;
                  if ( ( AV295FlagA == 1 ) )
                  {
                     A1171lccbT = (double)(A1171lccbT+AV220Total) ;
                     n1171lccbT = false ;
                     A1178lccbO = (double)(A1178lccbO+(((AV372lccbC>0) ? AV377lccbF : AV377lccbF+AV372lccbC))) ;
                     n1178lccbO = false ;
                     A1177lccbO = (double)(A1177lccbO+AV338lccbO) ;
                     n1177lccbO = false ;
                     A1172lccbS = (double)(A1172lccbS+AV264lccbS) ;
                     n1172lccbS = false ;
                     A1515lccbC = (double)(A1515lccbC+AV372lccbC) ;
                     n1515lccbC = false ;
                     A1519lccbO = (double)(A1519lccbO+AV376lccbO) ;
                     n1519lccbO = false ;
                     A1514lccbF = (double)((A1178lccbO+A1515lccbC)-A1177lccbO) ;
                     n1514lccbF = false ;
                  }
                  /* Using cursor P007N29 */
                  pr_default.execute(27, new Object[] {new Boolean(n1183lccbP), new Short(A1183lccbP), new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1163lccbP), A1163lccbP, new Boolean(n1168lccbI), new Short(A1168lccbI), new Boolean(n1169lccbD), new Double(A1169lccbD), new Boolean(n1170lccbI), new Double(A1170lccbI), new Boolean(n1171lccbT), new Double(A1171lccbT), new Boolean(n1178lccbO), new Double(A1178lccbO), new Boolean(n1177lccbO), new Double(A1177lccbO), new Boolean(n1172lccbS), new Double(A1172lccbS), new Boolean(n1515lccbC), new Double(A1515lccbC), new Boolean(n1519lccbO), new Double(A1519lccbO), new Boolean(n1514lccbF), new Double(A1514lccbF), A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(26);
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
         }
      }
      AV377lccbF = 0 ;
   }

   public void S201( )
   {
      /* 'REDOPLP' Routine */
      /* Using cursor P007N30 */
      pr_default.execute(28);
      while ( (pr_default.getStatus(28) != 101) )
      {
         A1228lccbF = P007N30_A1228lccbF[0] ;
         A1227lccbO = P007N30_A1227lccbO[0] ;
         A1226lccbA = P007N30_A1226lccbA[0] ;
         A1225lccbC = P007N30_A1225lccbC[0] ;
         A1224lccbC = P007N30_A1224lccbC[0] ;
         A1223lccbD = P007N30_A1223lccbD[0] ;
         A1222lccbI = P007N30_A1222lccbI[0] ;
         A1150lccbE = P007N30_A1150lccbE[0] ;
         A1184lccbS = P007N30_A1184lccbS[0] ;
         n1184lccbS = P007N30_n1184lccbS[0] ;
         A1167lccbG = P007N30_A1167lccbG[0] ;
         n1167lccbG = P007N30_n1167lccbG[0] ;
         A1172lccbS = P007N30_A1172lccbS[0] ;
         n1172lccbS = P007N30_n1172lccbS[0] ;
         A1169lccbD = P007N30_A1169lccbD[0] ;
         n1169lccbD = P007N30_n1169lccbD[0] ;
         A1171lccbT = P007N30_A1171lccbT[0] ;
         n1171lccbT = P007N30_n1171lccbT[0] ;
         AV247lccbi = A1222lccbI ;
         AV238lccbE = A1150lccbE ;
         AV248lccbD = A1223lccbD ;
         AV249lccbC = A1224lccbC ;
         AV250lccbC = A1225lccbC ;
         AV251lccbA = A1226lccbA ;
         AV266lccbO = A1227lccbO ;
         AV285lccbF = A1228lccbF ;
         AV152TACN = AV238lccbE ;
         AV8AGTN = AV247lccbi ;
         GXt_char10 = AV37DAIS ;
         GXv_date12[0] = AV248lccbD ;
         GXv_int13[0] = (byte)(0) ;
         GXv_char3[0] = GXt_char10 ;
         new pr2date2string(remoteHandle, context).execute( GXv_date12, GXv_int13, GXv_char3) ;
         areticsi_bkp.this.AV248lccbD = GXv_date12[0] ;
         areticsi_bkp.this.GXt_char10 = GXv_char3[0] ;
         AV37DAIS = GXt_char10 ;
         AV200EmpCC = AV249lccbC ;
         AV276PLP_C = A1172lccbS ;
         AV236PLP_V = A1169lccbD ;
         AV220Total = A1171lccbT ;
         /* Using cursor P007N31 */
         pr_default.execute(29, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         while ( (pr_default.getStatus(29) != 101) )
         {
            A1232lccbT = P007N31_A1232lccbT[0] ;
            A1231lccbT = P007N31_A1231lccbT[0] ;
            A1212lccbF = P007N31_A1212lccbF[0] ;
            n1212lccbF = P007N31_n1212lccbF[0] ;
            A1211lccbF = P007N31_A1211lccbF[0] ;
            n1211lccbF = P007N31_n1211lccbF[0] ;
            A1210lccbF = P007N31_A1210lccbF[0] ;
            n1210lccbF = P007N31_n1210lccbF[0] ;
            A1213lccbI = P007N31_A1213lccbI[0] ;
            n1213lccbI = P007N31_n1213lccbI[0] ;
            A1214lccbI = P007N31_A1214lccbI[0] ;
            n1214lccbI = P007N31_n1214lccbI[0] ;
            A1217lccbF = P007N31_A1217lccbF[0] ;
            n1217lccbF = P007N31_n1217lccbF[0] ;
            A1216lccbF = P007N31_A1216lccbF[0] ;
            n1216lccbF = P007N31_n1216lccbF[0] ;
            A1215lccbF = P007N31_A1215lccbF[0] ;
            n1215lccbF = P007N31_n1215lccbF[0] ;
            A1219lccbI = P007N31_A1219lccbI[0] ;
            n1219lccbI = P007N31_n1219lccbI[0] ;
            A1221lccbF = P007N31_A1221lccbF[0] ;
            n1221lccbF = P007N31_n1221lccbF[0] ;
            AV165TRNC = A1232lccbT ;
            AV158TDNR = A1231lccbT ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( AV251lccbA );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( "BRL2" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( "" );
            AV329Atrib = GXutil.trim( A1212lccbF) ;
            GXv_char3[0] = AV330Resul ;
            new pcrypto(remoteHandle, context).execute( AV329Atrib, "D", GXv_char3) ;
            areticsi_bkp.this.AV330Resul = GXv_char3[0] ;
            AV328LccbC = AV330Resul ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( AV328LccbC );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( A1211lccbF );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( A1210lccbF );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( "" );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( A1213lccbI );
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( A1214lccbI );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( AV251lccbA );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( "BRL2" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( A1217lccbF );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( A1216lccbF );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( A1215lccbF );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( "" );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( A1213lccbI );
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( A1219lccbI );
            A1221lccbF = "Y" ;
            n1221lccbF = false ;
            /* Execute user subroutine: S3226 */
            S3226 ();
            if ( returnInSub )
            {
               pr_default.close(29);
               pr_default.close(28);
               returnInSub = true;
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P007N32 */
            pr_default.execute(30, new Object[] {new Boolean(n1221lccbF), A1221lccbF, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
            if (true) break;
            /* Using cursor P007N33 */
            pr_default.execute(31, new Object[] {new Boolean(n1221lccbF), A1221lccbF, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
            pr_default.readNext(29);
         }
         pr_default.close(29);
         pr_default.readNext(28);
      }
      pr_default.close(28);
   }

   public void S3226( )
   {
      /* 'CHECKPLP' Routine */
      AV144SCETe = "" ;
      AV263lccbS = "Venda Cart�o PLP submetida normalmente" ;
      AV235FPAC = GXutil.trim( AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac()) ;
      /* Execute user subroutine: S331 */
      S331 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( AV228PLP_o == 0 ) )
      {
         AV218fop2I = 5 ;
         /* Execute user subroutine: S271 */
         S271 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV262lccbS = "NOSUB" ;
         AV261lccbP = (short)(GXutil.val( AV145SCETp, ".")) ;
         AV263lccbS = AV144SCETe ;
         AV296lccbS = "4123" ;
         /* Execute user subroutine: S341 */
         S341 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( AV228PLP_o == 1 ) )
      {
         AV262lccbS = "TOSUB" ;
         AV296lccbS = "INFO" ;
         /* Execute user subroutine: S341 */
         S341 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV218fop2I = 4 ;
         /* Execute user subroutine: S271 */
         S271 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( AV228PLP_o == 3 ) )
      {
         AV262lccbS = "NOSUB" ;
         AV261lccbP = (short)(GXutil.val( AV145SCETp, ".")) ;
         AV263lccbS = AV144SCETe ;
         AV296lccbS = "4123" ;
         /* Execute user subroutine: S341 */
         S341 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S341( )
   {
      /* 'SUBMITPLP' Routine */
      /* Execute user subroutine: S351 */
      S351 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      GXt_char10 = AV328LccbC ;
      GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
      GXv_char2[0] = GXt_char10 ;
      new pr2onlynumbers(remoteHandle, context).execute( GXv_char3, GXv_char2) ;
      AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXv_char3[0] );
      areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
      AV328LccbC = GXt_char10 ;
      AV329Atrib = GXutil.trim( AV328LccbC) ;
      GXv_char3[0] = AV330Resul ;
      new pcrypto(remoteHandle, context).execute( AV329Atrib, "E", GXv_char3) ;
      areticsi_bkp.this.AV330Resul = GXv_char3[0] ;
      AV328LccbC = AV330Resul ;
      /*
         INSERT RECORD ON TABLE LCCBPLP

      */
      A1222lccbI = GXutil.substring( AV247lccbi, 1, 7) ;
      A1150lccbE = GXutil.substring( AV238lccbE, 1, 20) ;
      A1223lccbD = AV248lccbD ;
      A1224lccbC = GXutil.substring( AV249lccbC, 1, 2) ;
      A1225lccbC = AV328LccbC ;
      A1226lccbA = GXutil.substring( AV251lccbA, 1, 20) ;
      A1227lccbO = GXutil.substring( AV266lccbO, 1, 1) ;
      A1228lccbF = GXutil.substring( AV235FPAC, 1, 20) ;
      A1163lccbP = GXutil.substring( AV233PLP_p, 1, 20) ;
      n1163lccbP = false ;
      A1168lccbI = AV229PLP_q ;
      n1168lccbI = false ;
      A1169lccbD = AV236PLP_V ;
      n1169lccbD = false ;
      A1171lccbT = AV220Total ;
      n1171lccbT = false ;
      A1183lccbP = AV261lccbP ;
      n1183lccbP = false ;
      A1184lccbS = GXutil.substring( AV262lccbS, 1, 8) ;
      n1184lccbS = false ;
      A1189lccbS = AV304lccbS ;
      n1189lccbS = false ;
      A1179lccbV = AV277lccbV ;
      n1179lccbV = false ;
      A1180lccbC = GXutil.substring( AV259lccbC, 1, 50) ;
      n1180lccbC = false ;
      A1181lccbC = GXutil.substring( AV305lccbC, 1, 3) ;
      n1181lccbC = false ;
      A1182lccbS = GXutil.substring( AV306LccbS, 1, 50) ;
      n1182lccbS = false ;
      A1167lccbG = GXutil.substring( AV252lccbG, 1, 4) ;
      n1167lccbG = false ;
      A1495lccbC = GXutil.resetTime(GXutil.serverNow( context, remoteHandle, "DEFAULT")) ;
      n1495lccbC = false ;
      A1176lccbO = AV337lccbO ;
      n1176lccbO = false ;
      A1178lccbO = ((AV372lccbC>0) ? AV377lccbF : AV377lccbF+AV372lccbC) ;
      n1178lccbO = false ;
      A1177lccbO = AV338lccbO ;
      n1177lccbO = false ;
      A1515lccbC = AV372lccbC ;
      n1515lccbC = false ;
      A1516lccbC = AV370lccbC ;
      n1516lccbC = false ;
      A1519lccbO = AV376lccbO ;
      n1519lccbO = false ;
      A1514lccbF = (double)((A1178lccbO+A1515lccbC)-A1177lccbO) ;
      n1514lccbF = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      if ( ( GXutil.strcmp(GXutil.substring( AV262lccbS, 1, 8), "NOSUB") == 0 ) )
      {
         AV348Origi = "Y" ;
         A1172lccbS = AV339lccbO ;
         n1172lccbS = false ;
         A1170lccbI = AV231PLP_V ;
         n1170lccbI = false ;
      }
      else
      {
         A1172lccbS = AV264lccbS ;
         n1172lccbS = false ;
         A1170lccbI = AV345InstA ;
         n1170lccbI = false ;
      }
      A1173lccbO = AV233PLP_p ;
      n1173lccbO = false ;
      AV285lccbF = AV235FPAC ;
      AV278lccbS = AV262lccbS ;
      if ( ( GXutil.strcmp(AV348Origi, "Y") == 0 ) )
      {
         A1174lccbO = AV335lccbO ;
         n1174lccbO = false ;
         A1175lccbO = AV336lccbO ;
         n1175lccbO = false ;
         A1176lccbO = AV337lccbO ;
         n1176lccbO = false ;
      }
      /* Execute user subroutine: S30799 */
      S30799 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      /*
         INSERT RECORD ON TABLE LCCBPLP2

      */
      A1232lccbT = GXutil.substring( AV165TRNC, 1, 4) ;
      A1231lccbT = GXutil.substring( AV158TDNR, 1, 10) ;
      if ( ( AV282flagR == 1 ) )
      {
         A1209lccbR = GXutil.substring( AV283ICSII, 1, 5000) ;
         n1209lccbR = false ;
      }
      A1210lccbF = GXutil.substring( AV307lccbF, 1, 10) ;
      n1210lccbF = false ;
      A1211lccbF = GXutil.substring( AV308lccbF, 1, 11) ;
      n1211lccbF = false ;
      A1212lccbF = GXutil.substring( AV309lccbF, 1, 44) ;
      n1212lccbF = false ;
      A1213lccbI = GXutil.substring( AV310lccbI, 1, 8) ;
      n1213lccbI = false ;
      A1214lccbI = AV311lccbI ;
      n1214lccbI = false ;
      A1215lccbF = GXutil.substring( AV312lccbF, 1, 10) ;
      n1215lccbF = false ;
      A1216lccbF = GXutil.substring( AV313lccbF, 1, 11) ;
      n1216lccbF = false ;
      A1217lccbF = GXutil.substring( AV314lccbF, 1, 19) ;
      n1217lccbF = false ;
      A1218lccbI = GXutil.substring( AV315lccbI, 1, 8) ;
      n1218lccbI = false ;
      A1219lccbI = AV316lccbI ;
      n1219lccbI = false ;
      A1220lccbT = AV317lccbT ;
      n1220lccbT = false ;
      A1221lccbF = GXutil.substring( AV318lccbF, 1, 1) ;
      n1221lccbF = false ;
      A1207lccbP = GXutil.substring( AV319lccbP, 1, 50) ;
      n1207lccbP = false ;
      A1208lccbR = GXutil.substring( AV320lccbR, 1, 20) ;
      n1208lccbR = false ;
      /* Using cursor P007N34 */
      pr_default.execute(32, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR, new Boolean(n1209lccbR), A1209lccbR, new Boolean(n1210lccbF), A1210lccbF, new Boolean(n1211lccbF), A1211lccbF, new Boolean(n1212lccbF), A1212lccbF, new Boolean(n1213lccbI), A1213lccbI, new Boolean(n1214lccbI), new Byte(A1214lccbI), new Boolean(n1215lccbF), A1215lccbF, new Boolean(n1216lccbF), A1216lccbF, new Boolean(n1217lccbF), A1217lccbF, new Boolean(n1218lccbI), A1218lccbI, new Boolean(n1219lccbI), new Byte(A1219lccbI), new Boolean(n1220lccbT), new Double(A1220lccbT), new Boolean(n1221lccbF), A1221lccbF});
      if ( (pr_default.getStatus(32) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P007N35 */
         pr_default.execute(33, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
         while ( (pr_default.getStatus(33) != 101) )
         {
            A1150lccbE = P007N35_A1150lccbE[0] ;
            A1222lccbI = P007N35_A1222lccbI[0] ;
            A1223lccbD = P007N35_A1223lccbD[0] ;
            A1224lccbC = P007N35_A1224lccbC[0] ;
            A1225lccbC = P007N35_A1225lccbC[0] ;
            A1226lccbA = P007N35_A1226lccbA[0] ;
            A1227lccbO = P007N35_A1227lccbO[0] ;
            A1228lccbF = P007N35_A1228lccbF[0] ;
            A1231lccbT = P007N35_A1231lccbT[0] ;
            A1232lccbT = P007N35_A1232lccbT[0] ;
            A1209lccbR = P007N35_A1209lccbR[0] ;
            n1209lccbR = P007N35_n1209lccbR[0] ;
            if ( ( AV282flagR == 1 ) )
            {
               A1209lccbR = GXutil.substring( AV283ICSII, 1, 5000) ;
               n1209lccbR = false ;
            }
            /* Using cursor P007N36 */
            pr_default.execute(34, new Object[] {new Boolean(n1209lccbR), A1209lccbR, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(33);
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      /* Using cursor P007N37 */
      pr_default.execute(35, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, new Boolean(n1167lccbG), A1167lccbG, new Boolean(n1163lccbP), A1163lccbP, new Boolean(n1168lccbI), new Short(A1168lccbI), new Boolean(n1169lccbD), new Double(A1169lccbD), new Boolean(n1170lccbI), new Double(A1170lccbI), new Boolean(n1171lccbT), new Double(A1171lccbT), new Boolean(n1172lccbS), new Double(A1172lccbS), new Boolean(n1173lccbO), A1173lccbO, new Boolean(n1174lccbO), new Short(A1174lccbO), new Boolean(n1175lccbO), new Double(A1175lccbO), new Boolean(n1176lccbO), new Double(A1176lccbO), new Boolean(n1177lccbO), new Double(A1177lccbO), new Boolean(n1178lccbO), new Double(A1178lccbO), new Boolean(n1179lccbV), A1179lccbV, new Boolean(n1180lccbC), A1180lccbC, new Boolean(n1181lccbC), A1181lccbC, new Boolean(n1182lccbS), A1182lccbS, new Boolean(n1183lccbP), new Short(A1183lccbP), new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1189lccbS), A1189lccbS, new Boolean(n1495lccbC), A1495lccbC, new Boolean(n1490Distr), A1490Distr, new Boolean(n1514lccbF), new Double(A1514lccbF), new Boolean(n1515lccbC), new Double(A1515lccbC), new Boolean(n1516lccbC), new Double(A1516lccbC), new Boolean(n1519lccbO), new Double(A1519lccbO)});
      if ( (pr_default.getStatus(35) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P007N38 */
         pr_default.execute(36, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         while ( (pr_default.getStatus(36) != 101) )
         {
            A1150lccbE = P007N38_A1150lccbE[0] ;
            A1222lccbI = P007N38_A1222lccbI[0] ;
            A1223lccbD = P007N38_A1223lccbD[0] ;
            A1224lccbC = P007N38_A1224lccbC[0] ;
            A1225lccbC = P007N38_A1225lccbC[0] ;
            A1226lccbA = P007N38_A1226lccbA[0] ;
            A1227lccbO = P007N38_A1227lccbO[0] ;
            A1228lccbF = P007N38_A1228lccbF[0] ;
            A1183lccbP = P007N38_A1183lccbP[0] ;
            n1183lccbP = P007N38_n1183lccbP[0] ;
            A1184lccbS = P007N38_A1184lccbS[0] ;
            n1184lccbS = P007N38_n1184lccbS[0] ;
            A1163lccbP = P007N38_A1163lccbP[0] ;
            n1163lccbP = P007N38_n1163lccbP[0] ;
            A1168lccbI = P007N38_A1168lccbI[0] ;
            n1168lccbI = P007N38_n1168lccbI[0] ;
            A1169lccbD = P007N38_A1169lccbD[0] ;
            n1169lccbD = P007N38_n1169lccbD[0] ;
            A1170lccbI = P007N38_A1170lccbI[0] ;
            n1170lccbI = P007N38_n1170lccbI[0] ;
            A1173lccbO = P007N38_A1173lccbO[0] ;
            n1173lccbO = P007N38_n1173lccbO[0] ;
            A1172lccbS = P007N38_A1172lccbS[0] ;
            n1172lccbS = P007N38_n1172lccbS[0] ;
            A1171lccbT = P007N38_A1171lccbT[0] ;
            n1171lccbT = P007N38_n1171lccbT[0] ;
            A1178lccbO = P007N38_A1178lccbO[0] ;
            n1178lccbO = P007N38_n1178lccbO[0] ;
            A1177lccbO = P007N38_A1177lccbO[0] ;
            n1177lccbO = P007N38_n1177lccbO[0] ;
            A1515lccbC = P007N38_A1515lccbC[0] ;
            n1515lccbC = P007N38_n1515lccbC[0] ;
            A1516lccbC = P007N38_A1516lccbC[0] ;
            n1516lccbC = P007N38_n1516lccbC[0] ;
            A1519lccbO = P007N38_A1519lccbO[0] ;
            n1519lccbO = P007N38_n1519lccbO[0] ;
            A1514lccbF = P007N38_A1514lccbF[0] ;
            n1514lccbF = P007N38_n1514lccbF[0] ;
            A1174lccbO = P007N38_A1174lccbO[0] ;
            n1174lccbO = P007N38_n1174lccbO[0] ;
            A1175lccbO = P007N38_A1175lccbO[0] ;
            n1175lccbO = P007N38_n1175lccbO[0] ;
            A1176lccbO = P007N38_A1176lccbO[0] ;
            n1176lccbO = P007N38_n1176lccbO[0] ;
            A1183lccbP = AV261lccbP ;
            n1183lccbP = false ;
            A1184lccbS = AV262lccbS ;
            n1184lccbS = false ;
            A1163lccbP = AV233PLP_p ;
            n1163lccbP = false ;
            A1168lccbI = AV229PLP_q ;
            n1168lccbI = false ;
            A1169lccbD = AV236PLP_V ;
            n1169lccbD = false ;
            A1170lccbI = AV345InstA ;
            n1170lccbI = false ;
            A1173lccbO = AV233PLP_p ;
            n1173lccbO = false ;
            if ( ( GXutil.strcmp(GXutil.substring( AV262lccbS, 1, 8), "NOSUB") != 0 ) )
            {
               A1172lccbS = AV346SaleA ;
               n1172lccbS = false ;
            }
            A1171lccbT = AV220Total ;
            n1171lccbT = false ;
            A1178lccbO = (double)(A1178lccbO+(((AV372lccbC>0) ? AV377lccbF : AV377lccbF+AV372lccbC))) ;
            n1178lccbO = false ;
            A1177lccbO = (double)(A1177lccbO+AV338lccbO) ;
            n1177lccbO = false ;
            A1515lccbC = (double)(A1515lccbC+AV372lccbC) ;
            n1515lccbC = false ;
            A1516lccbC = (double)(A1516lccbC+AV370lccbC) ;
            n1516lccbC = false ;
            A1519lccbO = AV376lccbO ;
            n1519lccbO = false ;
            A1514lccbF = (double)((A1178lccbO+A1515lccbC)-A1177lccbO) ;
            n1514lccbF = false ;
            if ( ( GXutil.strcmp(GXutil.substring( AV262lccbS, 1, 8), "NOSUB") == 0 ) )
            {
               AV348Origi = "N" ;
               A1170lccbI = AV231PLP_V ;
               n1170lccbI = false ;
            }
            else
            {
               A1172lccbS = AV346SaleA ;
               n1172lccbS = false ;
               A1170lccbI = AV345InstA ;
               n1170lccbI = false ;
            }
            A1174lccbO = AV335lccbO ;
            n1174lccbO = false ;
            A1175lccbO = AV336lccbO ;
            n1175lccbO = false ;
            A1176lccbO = AV337lccbO ;
            n1176lccbO = false ;
            /* Using cursor P007N39 */
            pr_default.execute(37, new Object[] {new Boolean(n1183lccbP), new Short(A1183lccbP), new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1163lccbP), A1163lccbP, new Boolean(n1168lccbI), new Short(A1168lccbI), new Boolean(n1169lccbD), new Double(A1169lccbD), new Boolean(n1170lccbI), new Double(A1170lccbI), new Boolean(n1173lccbO), A1173lccbO, new Boolean(n1172lccbS), new Double(A1172lccbS), new Boolean(n1171lccbT), new Double(A1171lccbT), new Boolean(n1178lccbO), new Double(A1178lccbO), new Boolean(n1177lccbO), new Double(A1177lccbO), new Boolean(n1515lccbC), new Double(A1515lccbC), new Boolean(n1516lccbC), new Double(A1516lccbC), new Boolean(n1519lccbO), new Double(A1519lccbO), new Boolean(n1514lccbF), new Double(A1514lccbF), new Boolean(n1174lccbO), new Short(A1174lccbO), new Boolean(n1175lccbO), new Double(A1175lccbO), new Boolean(n1176lccbO), new Double(A1176lccbO), A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(36);
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S291( )
   {
      /* 'ADDICSIITEM_STEP1' Routine */
      AV228PLP_o = (byte)(1) ;
      /* Execute user subroutine: S24590 */
      S24590 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strcmp(AV200EmpCC, "") == 0 ) )
      {
         if ( ( AV243FlagI == 1 ) )
         {
            AV218fop2I = 5 ;
            /* Execute user subroutine: S271 */
            S271 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
      else
      {
         AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
         AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV264lccbS = AV214valor ;
         AV376lccbO = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment() ;
         AV377lccbF = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal() ;
         AV338lccbO = AV379Total ;
         AV235FPAC = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
         /* Execute user subroutine: S361 */
         S361 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV232PLP_p, 3, 1), "E") == 0 ) )
         {
            AV236PLP_V = (double)(AV236PLP_V+(AV214valor-AV220Total)) ;
         }
         AV215sAMOU = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
         AV216CUTP = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV264lccbS = (double)(AV264lccbS+AV214valor) ;
         AV377lccbF = (double)(AV377lccbF+(AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal())) ;
         if ( ( AV228PLP_o == 1 ) )
         {
            AV262lccbS = "PENDS" ;
            AV261lccbP = (short)(0) ;
         }
         else
         {
            AV262lccbS = "NOSUB" ;
            AV261lccbP = (short)(GXutil.val( AV145SCETp, ".")) ;
            AV263lccbS = AV144SCETe ;
            AV218fop2I = 5 ;
            /* Execute user subroutine: S271 */
            S271 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: S311 */
         S311 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         GXt_char10 = AV328LccbC ;
         GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
         GXv_char2[0] = GXt_char10 ;
         new pr2onlynumbers(remoteHandle, context).execute( GXv_char3, GXv_char2) ;
         AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXv_char3[0] );
         areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
         AV328LccbC = GXt_char10 ;
         AV329Atrib = GXutil.trim( AV328LccbC) ;
         GXv_char3[0] = AV330Resul ;
         new pcrypto(remoteHandle, context).execute( AV329Atrib, "E", GXv_char3) ;
         areticsi_bkp.this.AV330Resul = GXv_char3[0] ;
         AV328LccbC = AV330Resul ;
         if ( ( GXutil.strcmp(AV323FileD, "N") == 0 ) )
         {
            /*
               INSERT RECORD ON TABLE LCCBPLP

            */
            A1222lccbI = GXutil.substring( AV8AGTN, 1, 7) ;
            A1150lccbE = GXutil.substring( AV152TACN, 1, 3) ;
            GXt_dtime11 = GXutil.resetTime( A1223lccbD );
            GXv_date12[0] = GXutil.resetTime(GXt_dtime11) ;
            new pr2string2date(remoteHandle, context).execute( AV37DAIS, GXv_date12) ;
            areticsi_bkp.this.GXt_dtime11 = GXutil.resetTime( GXv_date12[0] );
            A1223lccbD = GXt_dtime11 ;
            A1224lccbC = AV200EmpCC ;
            A1225lccbC = AV328LccbC ;
            A1226lccbA = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc() ;
            A1227lccbO = "S" ;
            A1228lccbF = GXutil.trim( AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac()) ;
            A1172lccbS = AV264lccbS ;
            n1172lccbS = false ;
            A1171lccbT = AV220Total ;
            n1171lccbT = false ;
            A1519lccbO = AV376lccbO ;
            n1519lccbO = false ;
            A1178lccbO = ((AV372lccbC>0) ? AV377lccbF : AV377lccbF+AV372lccbC) ;
            n1178lccbO = false ;
            A1177lccbO = AV338lccbO ;
            n1177lccbO = false ;
            A1176lccbO = AV337lccbO ;
            n1176lccbO = false ;
            A1163lccbP = "" ;
            n1163lccbP = false ;
            A1168lccbI = (short)(0) ;
            n1168lccbI = false ;
            A1169lccbD = AV236PLP_V ;
            n1169lccbD = false ;
            A1170lccbI = 0 ;
            n1170lccbI = false ;
            A1183lccbP = AV261lccbP ;
            n1183lccbP = false ;
            A1184lccbS = AV262lccbS ;
            n1184lccbS = false ;
            A1189lccbS = Gx_date ;
            n1189lccbS = false ;
            A1495lccbC = GXutil.resetTime(GXutil.serverNow( context, remoteHandle, "DEFAULT")) ;
            n1495lccbC = false ;
            A1490Distr = "" ;
            n1490Distr = false ;
            A1179lccbV = GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda(), 3, 2) + GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda(), 1, 2) ;
            n1179lccbV = false ;
            A1180lccbC = AV246PXNM ;
            n1180lccbC = false ;
            A1181lccbC = GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp(), 1, 3) ;
            n1181lccbC = false ;
            A1182lccbS = "" ;
            n1182lccbS = false ;
            A1167lccbG = AV35CRS ;
            n1167lccbG = false ;
            GXt_char10 = AV328LccbC ;
            GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
            GXv_char2[0] = GXt_char10 ;
            new pr2onlynumbers(remoteHandle, context).execute( GXv_char3, GXv_char2) ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXv_char3[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
            AV328LccbC = GXt_char10 ;
            AV329Atrib = GXutil.trim( AV328LccbC) ;
            A1514lccbF = (double)((A1178lccbO+A1515lccbC)-A1177lccbO) ;
            n1514lccbF = false ;
            GXv_char3[0] = AV330Resul ;
            new pcrypto(remoteHandle, context).execute( AV329Atrib, "E", GXv_char3) ;
            areticsi_bkp.this.AV330Resul = GXv_char3[0] ;
            AV328LccbC = AV330Resul ;
            /*
               INSERT RECORD ON TABLE LCCBPLP2

            */
            A1232lccbT = AV165TRNC ;
            A1231lccbT = AV158TDNR ;
            A1210lccbF = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
            n1210lccbF = false ;
            A1211lccbF = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
            n1211lccbF = false ;
            A1212lccbF = AV328LccbC ;
            n1212lccbF = false ;
            A1213lccbI = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos() ;
            n1213lccbI = false ;
            A1214lccbI = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq() ;
            n1214lccbI = false ;
            A1215lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
            n1215lccbF = false ;
            A1216lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
            n1216lccbF = false ;
            A1217lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
            n1217lccbF = false ;
            A1218lccbI = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos() ;
            n1218lccbI = false ;
            A1219lccbI = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq() ;
            n1219lccbI = false ;
            A1220lccbT = AV220Total ;
            n1220lccbT = false ;
            A1221lccbF = "" ;
            n1221lccbF = false ;
            A1207lccbP = AV246PXNM ;
            n1207lccbP = false ;
            A1208lccbR = GXutil.substring( AV178Filen, 1, 20) ;
            n1208lccbR = false ;
            if ( ( AV274flagR == 1 ) )
            {
               A1209lccbR = AV207LccbR ;
               n1209lccbR = false ;
            }
            else
            {
               A1209lccbR = "" ;
               n1209lccbR = false ;
            }
            /* Using cursor P007N40 */
            pr_default.execute(38, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR, new Boolean(n1209lccbR), A1209lccbR, new Boolean(n1210lccbF), A1210lccbF, new Boolean(n1211lccbF), A1211lccbF, new Boolean(n1212lccbF), A1212lccbF, new Boolean(n1213lccbI), A1213lccbI, new Boolean(n1214lccbI), new Byte(A1214lccbI), new Boolean(n1215lccbF), A1215lccbF, new Boolean(n1216lccbF), A1216lccbF, new Boolean(n1217lccbF), A1217lccbF, new Boolean(n1218lccbI), A1218lccbI, new Boolean(n1219lccbI), new Byte(A1219lccbI), new Boolean(n1220lccbT), new Double(A1220lccbT), new Boolean(n1221lccbF), A1221lccbF});
            if ( (pr_default.getStatus(38) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               GXt_char10 = AV328LccbC ;
               GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
               GXv_char2[0] = GXt_char10 ;
               new pr2onlynumbers(remoteHandle, context).execute( GXv_char3, GXv_char2) ;
               AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( GXv_char3[0] );
               areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
               AV328LccbC = GXt_char10 ;
               AV329Atrib = GXutil.trim( AV328LccbC) ;
               GXv_char3[0] = AV330Resul ;
               new pcrypto(remoteHandle, context).execute( AV329Atrib, "E", GXv_char3) ;
               areticsi_bkp.this.AV330Resul = GXv_char3[0] ;
               AV328LccbC = AV330Resul ;
               /* Using cursor P007N41 */
               pr_default.execute(39, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
               while ( (pr_default.getStatus(39) != 101) )
               {
                  A1150lccbE = P007N41_A1150lccbE[0] ;
                  A1222lccbI = P007N41_A1222lccbI[0] ;
                  A1223lccbD = P007N41_A1223lccbD[0] ;
                  A1224lccbC = P007N41_A1224lccbC[0] ;
                  A1225lccbC = P007N41_A1225lccbC[0] ;
                  A1226lccbA = P007N41_A1226lccbA[0] ;
                  A1227lccbO = P007N41_A1227lccbO[0] ;
                  A1228lccbF = P007N41_A1228lccbF[0] ;
                  A1231lccbT = P007N41_A1231lccbT[0] ;
                  A1232lccbT = P007N41_A1232lccbT[0] ;
                  A1210lccbF = P007N41_A1210lccbF[0] ;
                  n1210lccbF = P007N41_n1210lccbF[0] ;
                  A1211lccbF = P007N41_A1211lccbF[0] ;
                  n1211lccbF = P007N41_n1211lccbF[0] ;
                  A1212lccbF = P007N41_A1212lccbF[0] ;
                  n1212lccbF = P007N41_n1212lccbF[0] ;
                  A1215lccbF = P007N41_A1215lccbF[0] ;
                  n1215lccbF = P007N41_n1215lccbF[0] ;
                  A1216lccbF = P007N41_A1216lccbF[0] ;
                  n1216lccbF = P007N41_n1216lccbF[0] ;
                  A1217lccbF = P007N41_A1217lccbF[0] ;
                  n1217lccbF = P007N41_n1217lccbF[0] ;
                  A1220lccbT = P007N41_A1220lccbT[0] ;
                  n1220lccbT = P007N41_n1220lccbT[0] ;
                  A1221lccbF = P007N41_A1221lccbF[0] ;
                  n1221lccbF = P007N41_n1221lccbF[0] ;
                  A1207lccbP = P007N41_A1207lccbP[0] ;
                  n1207lccbP = P007N41_n1207lccbP[0] ;
                  A1208lccbR = P007N41_A1208lccbR[0] ;
                  n1208lccbR = P007N41_n1208lccbR[0] ;
                  A1209lccbR = P007N41_A1209lccbR[0] ;
                  n1209lccbR = P007N41_n1209lccbR[0] ;
                  A1210lccbF = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
                  n1210lccbF = false ;
                  A1211lccbF = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
                  n1211lccbF = false ;
                  A1212lccbF = AV328LccbC ;
                  n1212lccbF = false ;
                  A1215lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
                  n1215lccbF = false ;
                  A1216lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
                  n1216lccbF = false ;
                  A1217lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
                  n1217lccbF = false ;
                  A1220lccbT = AV220Total ;
                  n1220lccbT = false ;
                  A1221lccbF = "" ;
                  n1221lccbF = false ;
                  A1207lccbP = AV246PXNM ;
                  n1207lccbP = false ;
                  A1208lccbR = GXutil.substring( AV178Filen, 1, 20) ;
                  n1208lccbR = false ;
                  if ( ( AV274flagR == 1 ) )
                  {
                     A1209lccbR = AV207LccbR ;
                     n1209lccbR = false ;
                  }
                  else
                  {
                     A1209lccbR = "" ;
                     n1209lccbR = false ;
                  }
                  /* Using cursor P007N42 */
                  pr_default.execute(40, new Object[] {new Boolean(n1210lccbF), A1210lccbF, new Boolean(n1211lccbF), A1211lccbF, new Boolean(n1212lccbF), A1212lccbF, new Boolean(n1215lccbF), A1215lccbF, new Boolean(n1216lccbF), A1216lccbF, new Boolean(n1217lccbF), A1217lccbF, new Boolean(n1220lccbT), new Double(A1220lccbT), new Boolean(n1221lccbF), A1221lccbF, new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR, new Boolean(n1209lccbR), A1209lccbR, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(39);
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
            while ( ( AV273Count > 0 ) )
            {
               /*
                  INSERT RECORD ON TABLE LCCBPLP2

               */
               A1232lccbT = "CNJ" ;
               A1231lccbT = GXutil.substring( AV271TDNR_[AV273Count-1], 1, 10) ;
               A1221lccbF = "" ;
               n1221lccbF = false ;
               A1207lccbP = GXutil.substring( AV246PXNM, 1, 50) ;
               n1207lccbP = false ;
               A1208lccbR = GXutil.substring( AV178Filen, 1, 20) ;
               n1208lccbR = false ;
               /* Using cursor P007N43 */
               pr_default.execute(41, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT, new Boolean(n1207lccbP), A1207lccbP, new Boolean(n1208lccbR), A1208lccbR, new Boolean(n1221lccbF), A1221lccbF});
               if ( (pr_default.getStatus(41) == 1) )
               {
                  Gx_err = (short)(1) ;
                  Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                  /* Optimized UPDATE. */
                  /* Using cursor P007N44 */
                  pr_default.execute(42, new Object[] {AV246PXNM, AV178Filen, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
                  /* End optimized UPDATE. */
               }
               else
               {
                  Gx_err = (short)(0) ;
                  Gx_emsg = "" ;
               }
               /* End Insert */
               AV273Count = (byte)(AV273Count-1) ;
            }
            /* Using cursor P007N45 */
            pr_default.execute(43, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, new Boolean(n1167lccbG), A1167lccbG, new Boolean(n1163lccbP), A1163lccbP, new Boolean(n1168lccbI), new Short(A1168lccbI), new Boolean(n1169lccbD), new Double(A1169lccbD), new Boolean(n1170lccbI), new Double(A1170lccbI), new Boolean(n1171lccbT), new Double(A1171lccbT), new Boolean(n1172lccbS), new Double(A1172lccbS), new Boolean(n1176lccbO), new Double(A1176lccbO), new Boolean(n1177lccbO), new Double(A1177lccbO), new Boolean(n1178lccbO), new Double(A1178lccbO), new Boolean(n1179lccbV), A1179lccbV, new Boolean(n1180lccbC), A1180lccbC, new Boolean(n1181lccbC), A1181lccbC, new Boolean(n1182lccbS), A1182lccbS, new Boolean(n1183lccbP), new Short(A1183lccbP), new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1189lccbS), A1189lccbS, new Boolean(n1495lccbC), A1495lccbC, new Boolean(n1490Distr), A1490Distr, new Boolean(n1514lccbF), new Double(A1514lccbF), new Boolean(n1519lccbO), new Double(A1519lccbO)});
            if ( (pr_default.getStatus(43) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               /* Using cursor P007N46 */
               pr_default.execute(44, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               while ( (pr_default.getStatus(44) != 101) )
               {
                  A1150lccbE = P007N46_A1150lccbE[0] ;
                  A1222lccbI = P007N46_A1222lccbI[0] ;
                  A1223lccbD = P007N46_A1223lccbD[0] ;
                  A1224lccbC = P007N46_A1224lccbC[0] ;
                  A1225lccbC = P007N46_A1225lccbC[0] ;
                  A1226lccbA = P007N46_A1226lccbA[0] ;
                  A1227lccbO = P007N46_A1227lccbO[0] ;
                  A1228lccbF = P007N46_A1228lccbF[0] ;
                  A1172lccbS = P007N46_A1172lccbS[0] ;
                  n1172lccbS = P007N46_n1172lccbS[0] ;
                  A1171lccbT = P007N46_A1171lccbT[0] ;
                  n1171lccbT = P007N46_n1171lccbT[0] ;
                  A1169lccbD = P007N46_A1169lccbD[0] ;
                  n1169lccbD = P007N46_n1169lccbD[0] ;
                  A1519lccbO = P007N46_A1519lccbO[0] ;
                  n1519lccbO = P007N46_n1519lccbO[0] ;
                  A1178lccbO = P007N46_A1178lccbO[0] ;
                  n1178lccbO = P007N46_n1178lccbO[0] ;
                  A1177lccbO = P007N46_A1177lccbO[0] ;
                  n1177lccbO = P007N46_n1177lccbO[0] ;
                  A1515lccbC = P007N46_A1515lccbC[0] ;
                  n1515lccbC = P007N46_n1515lccbC[0] ;
                  A1514lccbF = P007N46_A1514lccbF[0] ;
                  n1514lccbF = P007N46_n1514lccbF[0] ;
                  A1163lccbP = P007N46_A1163lccbP[0] ;
                  n1163lccbP = P007N46_n1163lccbP[0] ;
                  A1168lccbI = P007N46_A1168lccbI[0] ;
                  n1168lccbI = P007N46_n1168lccbI[0] ;
                  A1170lccbI = P007N46_A1170lccbI[0] ;
                  n1170lccbI = P007N46_n1170lccbI[0] ;
                  A1183lccbP = P007N46_A1183lccbP[0] ;
                  n1183lccbP = P007N46_n1183lccbP[0] ;
                  A1184lccbS = P007N46_A1184lccbS[0] ;
                  n1184lccbS = P007N46_n1184lccbS[0] ;
                  A1172lccbS = (double)(A1172lccbS+AV264lccbS) ;
                  n1172lccbS = false ;
                  A1171lccbT = (double)(A1171lccbT+AV220Total) ;
                  n1171lccbT = false ;
                  A1169lccbD = (double)(A1169lccbD+0) ;
                  n1169lccbD = false ;
                  A1519lccbO = (double)(A1519lccbO+AV376lccbO) ;
                  n1519lccbO = false ;
                  A1178lccbO = (double)(A1178lccbO+(((AV372lccbC>0) ? AV377lccbF : AV377lccbF+AV372lccbC))) ;
                  n1178lccbO = false ;
                  A1177lccbO = (double)(A1177lccbO+AV338lccbO) ;
                  n1177lccbO = false ;
                  A1515lccbC = (double)(A1515lccbC+(AV372lccbC+AV255lccbD)) ;
                  n1515lccbC = false ;
                  A1514lccbF = (double)((A1178lccbO+A1515lccbC)-A1177lccbO) ;
                  n1514lccbF = false ;
                  A1163lccbP = "" ;
                  n1163lccbP = false ;
                  A1168lccbI = (short)(0) ;
                  n1168lccbI = false ;
                  A1170lccbI = 0 ;
                  n1170lccbI = false ;
                  A1183lccbP = AV261lccbP ;
                  n1183lccbP = false ;
                  A1184lccbS = GXutil.substring( AV262lccbS, 1, 8) ;
                  n1184lccbS = false ;
                  /* Using cursor P007N47 */
                  pr_default.execute(45, new Object[] {new Boolean(n1172lccbS), new Double(A1172lccbS), new Boolean(n1171lccbT), new Double(A1171lccbT), new Boolean(n1169lccbD), new Double(A1169lccbD), new Boolean(n1519lccbO), new Double(A1519lccbO), new Boolean(n1178lccbO), new Double(A1178lccbO), new Boolean(n1177lccbO), new Double(A1177lccbO), new Boolean(n1515lccbC), new Double(A1515lccbC), new Boolean(n1514lccbF), new Double(A1514lccbF), new Boolean(n1163lccbP), A1163lccbP, new Boolean(n1168lccbI), new Short(A1168lccbI), new Boolean(n1170lccbI), new Double(A1170lccbI), new Boolean(n1183lccbP), new Short(A1183lccbP), new Boolean(n1184lccbS), A1184lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(44);
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
         }
      }
   }

   public void S361( )
   {
      /* 'SPLITPLPPARTS' Routine */
      AV142s = GXutil.trim( AV235FPAC) ;
      AV232PLP_p = "" ;
      while ( ( GXutil.len( AV142s) > 0 ) )
      {
         AV226c = GXutil.substring( AV142s, 1, 1) ;
         AV142s = GXutil.trim( GXutil.substring( AV142s, 2, 100)) ;
         if ( ( GXutil.strcmp(AV226c, "X") == 0 ) )
         {
            if (true) break;
         }
         AV232PLP_p = AV232PLP_p + AV226c ;
      }
      AV232PLP_p = GXutil.trim( AV232PLP_p) ;
      AV234PLP_p = "" ;
      while ( ( GXutil.len( AV142s) > 0 ) )
      {
         AV88j = (short)(GXutil.len( AV142s)) ;
         AV226c = GXutil.substring( AV142s, AV88j, 1) ;
         AV88j = (short)(AV88j-1) ;
         AV142s = GXutil.trim( GXutil.substring( AV142s, 1, AV88j)) ;
         if ( ( GXutil.strcmp(AV226c, "X") == 0 ) )
         {
            if (true) break;
         }
         AV234PLP_p = AV226c + AV234PLP_p ;
      }
      AV234PLP_p = GXutil.trim( AV234PLP_p) ;
      AV233PLP_p = GXutil.trim( AV142s) ;
   }

   public void S331( )
   {
      /* 'SPLITPLP' Routine */
      AV283ICSII = "PLP: FPAC2=[" + AV235FPAC + "]/FPAM1=[" + AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() + "]/FPAM2=[" + AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() + "]" ;
      AV335lccbO = (short)(0) ;
      AV336lccbO = 0 ;
      AV337lccbO = 0 ;
      AV338lccbO = 0 ;
      AV339lccbO = 0 ;
      AV371lccbF = 0 ;
      AV372lccbC = 0 ;
      AV370lccbC = 0 ;
      AV349fare = 0 ;
      AV349fare = (double)(((AV276PLP_C-AV236PLP_V)-AV220Total)) ;
      AV349fare = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV349fare), 2))) ;
      AV364TemCa = "N" ;
      AV289Total = 0 ;
      AV370lccbC = AV349fare ;
      AV362i2 = 1 ;
      while ( ( AV362i2 <= AV361Count ) )
      {
         if ( ( GXutil.strSearch( GXutil.trim( AV360FareC[AV362i2-1]), GXutil.trim( AV158TDNR), 1) > 0 ) )
         {
            if ( ( GXutil.strSearch( GXutil.trim( AV360FareC[AV362i2-1]), "$", 1) > 0 ) )
            {
               AV365CashS = (int)(GXutil.strSearch( GXutil.trim( AV360FareC[AV362i2-1]), "$", 1)+1) ;
               AV372lccbC = GXutil.val( GXutil.substring( GXutil.trim( AV360FareC[AV362i2-1]), AV365CashS, 17), ".") ;
            }
            else
            {
               AV365CashS = (int)(GXutil.strSearch( GXutil.trim( AV360FareC[AV362i2-1]), "@", 1)+1) ;
               /* Execute user subroutine: S371 */
               S371 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV289Total = GXutil.val( GXutil.substring( GXutil.trim( AV360FareC[AV362i2-1]), AV365CashS, 17), ".") ;
               if ( ( GXutil.strcmp(AV366Grupo, "Y") == 0 ) )
               {
                  AV349fare = (double)(AV349fare+((AV289Total*AV369Count))) ;
               }
               else
               {
                  AV349fare = (double)(AV349fare+AV289Total) ;
               }
               AV375ZeraT = ((GXutil.strSearch( GXutil.trim( AV360FareC[AV362i2-1]), "*", 1)>0) ? "Y" : "N") ;
               AV364TemCa = "Y" ;
               AV372lccbC = AV289Total ;
            }
         }
         AV362i2 = (int)(AV362i2+1) ;
      }
      AV371lccbF = AV349fare ;
      if ( ( GXutil.strcmp(AV366Grupo, "Y") == 0 ) )
      {
         AV289Total = (double)(AV289Total*AV369Count) ;
      }
      AV281Decis = "" ;
      /* Execute user subroutine: S361 */
      S361 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV228PLP_o = (byte)(1) ;
      AV348Origi = "N" ;
      /* Execute user subroutine: S381 */
      S381 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV215sAMOU = AV234PLP_p ;
      AV216CUTP = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV337lccbO = AV214valor ;
      if ( ( GXutil.strcmp(AV281Decis, "") == 0 ) )
      {
         /* Execute user subroutine: S391 */
         S391 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(AV281Decis, "") == 0 ) )
         {
            /* Execute user subroutine: S401 */
            S401 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( ( GXutil.strcmp(AV281Decis, "") == 0 ) )
            {
               /* Execute user subroutine: S411 */
               S411 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( ( GXutil.strcmp(AV281Decis, "") == 0 ) )
               {
                  AV145SCETp = "L" ;
                  AV342SCEFo = "L" ;
                  AV144SCETe = "PLP Submetido sem altera��es (" + GXutil.trim( GXutil.str( AV228PLP_o, 10, 0)) + ")" ;
               }
            }
         }
      }
      AV144SCETe = AV144SCETe + " [" + GXutil.trim( AV235FPAC) + "]" ;
      /* Execute user subroutine: S23516 */
      S23516 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S411( )
   {
      /* 'CHECK_VALUES' Routine */
      AV373SCEFP = AV276PLP_C ;
      AV374SCEPa = AV231PLP_V ;
      if ( ( AV351lccbP <= 0 ) )
      {
         AV345InstA = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV349fare/ (double) (AV229PLP_q)), 2))) ;
         AV346SaleA = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV345InstA*AV229PLP_q), 2))) ;
      }
      else
      {
         AV345InstA = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV349fare*AV351lccbP), 2))) ;
         AV346SaleA = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV345InstA*AV229PLP_q), 2))) ;
      }
      AV346SaleA = (double)((AV346SaleA+AV220Total)-AV289Total) ;
      if ( ( GXutil.strcmp(AV364TemCa, "Y") == 0 ) )
      {
         AV345InstA = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec((AV346SaleA/ (double) (AV229PLP_q))), 2))) ;
         AV346SaleA = (double)(AV345InstA*AV229PLP_q) ;
      }
      AV215sAMOU = GXutil.strReplace( GXutil.str( AV345InstA, 14, 2), ".", "") ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV345InstA = AV214valor ;
      AV215sAMOU = GXutil.strReplace( GXutil.str( AV346SaleA, 14, 2), ".", "") ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV346SaleA = AV214valor ;
      AV214valor = (double)(AV231PLP_V-AV345InstA) ;
      if ( ( AV214valor < 0 ) )
      {
         AV214valor = (double)(AV214valor*-1) ;
      }
      if ( ( AV214valor > GXutil.val( AV332Difer, ".") ) )
      {
         AV142s = "Val.Parc=" + GXutil.trim( GXutil.str( AV286ICSI_, 14, 2)) ;
         AV142s = AV142s + "=(" + GXutil.trim( GXutil.str( AV276PLP_C, 14, 2)) ;
         AV142s = AV142s + "-" + GXutil.trim( GXutil.str( AV236PLP_V, 14, 2)) ;
         AV142s = AV142s + "-" + GXutil.trim( GXutil.str( AV220Total, 14, 2)) ;
         AV142s = AV142s + ")*" + GXutil.trim( GXutil.str( AV288Inter, 9, 5)) ;
         AV142s = AV142s + "/" + GXutil.trim( GXutil.str( AV229PLP_q, 3, 0)) ;
         if ( ( GXutil.strSearch( AV39DebugM, "SAYALL", 1) > 0 ) )
         {
            context.msgStatus( "  ==> "+AV142s );
         }
         AV144SCETe = "Diferen�a " + GXutil.trim( GXutil.str( AV214valor, 14, 2)) + " > " + GXutil.trim( AV332Difer) + " :" ;
         /* Execute user subroutine: S421 */
         S421 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      if ( ( GXutil.strcmp(AV375ZeraT, "Y") == 0 ) )
      {
         AV220Total = 0 ;
         AV375ZeraT = "N" ;
      }
   }

   public void S371( )
   {
      /* 'VERIFICAGRUPOCREDITO' Routine */
      AV368Total = 0 ;
      AV369Count = 0 ;
      AV366Grupo = "N" ;
      /* Using cursor P007N48 */
      pr_default.execute(46, new Object[] {AV238lccbE, AV247lccbi, AV248lccbD, AV249lccbC, AV250lccbC, AV251lccbA, AV266lccbO, AV285lccbF});
      while ( (pr_default.getStatus(46) != 101) )
      {
         A1232lccbT = P007N48_A1232lccbT[0] ;
         A1228lccbF = P007N48_A1228lccbF[0] ;
         A1227lccbO = P007N48_A1227lccbO[0] ;
         A1226lccbA = P007N48_A1226lccbA[0] ;
         A1225lccbC = P007N48_A1225lccbC[0] ;
         A1224lccbC = P007N48_A1224lccbC[0] ;
         A1223lccbD = P007N48_A1223lccbD[0] ;
         A1150lccbE = P007N48_A1150lccbE[0] ;
         A1222lccbI = P007N48_A1222lccbI[0] ;
         A1231lccbT = P007N48_A1231lccbT[0] ;
         AV369Count = (long)(AV369Count+1) ;
         AV367i3 = 1 ;
         while ( ( AV367i3 <= AV361Count ) )
         {
            if ( ( GXutil.strSearch( GXutil.trim( AV360FareC[(int)(AV367i3)-1]), GXutil.trim( A1231lccbT), 1) > 0 ) )
            {
               AV365CashS = (int)(GXutil.strSearch( GXutil.trim( AV360FareC[(int)(AV367i3)-1]), "@", 1)+1) ;
               AV368Total = (double)(AV368Total+(GXutil.val( GXutil.substring( GXutil.trim( AV360FareC[(int)(AV367i3)-1]), AV365CashS, 17), "."))) ;
            }
            AV367i3 = (long)(AV367i3+1) ;
         }
         pr_default.readNext(46);
      }
      pr_default.close(46);
      if ( ( AV369Count > 1 ) )
      {
         AV366Grupo = "Y" ;
      }
   }

   public void S381( )
   {
      /* 'CHECK_NUMBEROFINSTALLMENTS' Routine */
      AV281Decis = "" ;
      if ( ( GXutil.strcmp(GXutil.substring( AV232PLP_p, 1, 2), "MS") == 0 ) )
      {
         AV232PLP_p = GXutil.substring( AV232PLP_p, 3, 17) ;
      }
      if ( ( GXutil.len( AV232PLP_p) == 0 ) )
      {
         AV144SCETe = "Falta qtde parcelas: " ;
         /* Execute user subroutine: S431 */
         S431 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( GXutil.strcmp(GXutil.substring( AV232PLP_p, 1, 2), "PL") != 0 ) || ( GXutil.strSearch( "E1234567890", GXutil.substring( AV232PLP_p, 3, 1), 1) == 0 ) )
      {
         AV144SCETe = "Erro na constru��o do PLP. Deve iniciar com PL ou PLE: " ;
         /* Execute user subroutine: S431 */
         S431 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
         AV216CUTP = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
         /* Execute user subroutine: S185 */
         S185 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV232PLP_p, 3, 1), "E") == 0 ) )
         {
            AV230PLP_F = (byte)(1) ;
         }
         else
         {
            AV230PLP_F = (byte)(0) ;
         }
         AV88j = (short)(3+AV230PLP_F) ;
         AV229PLP_q = (short)(GXutil.val( GXutil.substring( AV232PLP_p, AV88j, 5), ".")) ;
         if ( ( AV229PLP_q < 2 ) || ( AV229PLP_q > 12 ) )
         {
            AV144SCETe = "Erro na constru��o do PLP. Qtd de parcelas do PLP [" + GXutil.trim( GXutil.str( AV229PLP_q, 10, 0)) + "]:" ;
            /* Execute user subroutine: S431 */
            S431 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
   }

   public void S401( )
   {
      /* 'CHECK_PLANCODE' Routine */
      AV281Decis = "" ;
      AV253lccbP = GXutil.trim( AV233PLP_p) ;
      /* Execute user subroutine: S441 */
      S441 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strcmp(AV253lccbP, "") == 0 ) )
      {
         AV253lccbP = GXutil.trim( AV233PLP_p) ;
         /* Execute user subroutine: S451 */
         S451 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: S441 */
         S441 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strcmp(AV253lccbP, "") != 0 ) )
         {
            AV144SCETe = "Plano indicado [" + AV233PLP_p + "] n�o encontrado, usado plano semelhante: [" + AV253lccbP + "]" ;
            AV145SCETp = "416" ;
            /* Execute user subroutine: S23516 */
            S23516 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV144SCETe = "" ;
            AV145SCETp = "" ;
         }
      }
      if ( ( GXutil.strcmp(AV253lccbP, "") != 0 ) )
      {
         if ( ( AV229PLP_q < AV244lccbP ) || ( AV229PLP_q > AV245lccbP ) )
         {
            AV144SCETe = "Quantidade de Parcelas Inv�lida: " ;
            /* Execute user subroutine: S461 */
            S461 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }
      else
      {
         if ( ( AV229PLP_q < 2 ) )
         {
            AV229PLP_q = (short)(2) ;
         }
         AV144SCETe = "C�digo do parcelamento Inv�lido: " ;
         /* Execute user subroutine: S471 */
         S471 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S451( )
   {
      /* 'AJUSTAPLANO' Routine */
      AV142s = GXutil.trim( GXutil.upper( AV253lccbP)) ;
      AV302sNum = "" ;
      AV253lccbP = "" ;
      AV78i = 1 ;
      while ( ( AV78i <= GXutil.len( AV142s) ) )
      {
         AV226c = GXutil.substring( AV142s, AV78i, 1) ;
         if ( ( GXutil.strSearch( "ABCDEFGHIJKLMNOPQRSTUVWXYZ", AV226c, 1) > 0 ) )
         {
            if ( ( GXutil.strcmp(AV302sNum, "") != 0 ) )
            {
               AV253lccbP = AV253lccbP + GXutil.trim( GXutil.str( GXutil.val( AV302sNum, "."), 10, 0)) ;
               AV302sNum = "" ;
            }
            AV253lccbP = AV253lccbP + AV226c ;
         }
         else
         {
            if ( ( GXutil.strSearch( "0123456789", AV226c, 1) > 0 ) )
            {
               AV302sNum = AV302sNum + AV226c ;
            }
         }
         AV78i = (int)(AV78i+1) ;
      }
      if ( ( GXutil.strcmp(AV302sNum, "") != 0 ) )
      {
         AV253lccbP = AV253lccbP + GXutil.trim( GXutil.str( GXutil.val( AV302sNum, "."), 10, 0)) ;
      }
   }

   public void S441( )
   {
      /* 'BUSCAPLANO' Routine */
      GXt_dtime11 = GXutil.resetTime( AV40dt1 );
      GXv_date12[0] = GXutil.resetTime(GXt_dtime11) ;
      new pr2string2date(remoteHandle, context).execute( AV37DAIS, GXv_date12) ;
      areticsi_bkp.this.GXt_dtime11 = GXutil.resetTime( GXv_date12[0] );
      AV40dt1 = GXt_dtime11 ;
      AV275lccbR = 0.00001 ;
      AV287lccbP = 0 ;
      AV351lccbP = 0 ;
      AV244lccbP = (byte)(2) ;
      AV245lccbP = (byte)(12) ;
      AV413GXLvl = (byte)(0) ;
      /* Using cursor P007N49 */
      pr_default.execute(47, new Object[] {AV152TACN, AV253lccbP, AV40dt1, AV253lccbP, AV40dt1});
      while ( (pr_default.getStatus(47) != 101) )
      {
         A1155lccbP = P007N49_A1155lccbP[0] ;
         n1155lccbP = P007N49_n1155lccbP[0] ;
         A1164lccbP = P007N49_A1164lccbP[0] ;
         A1163lccbP = P007N49_A1163lccbP[0] ;
         n1163lccbP = P007N49_n1163lccbP[0] ;
         A1150lccbE = P007N49_A1150lccbE[0] ;
         A1156lccbP = P007N49_A1156lccbP[0] ;
         n1156lccbP = P007N49_n1156lccbP[0] ;
         A1157lccbP = P007N49_A1157lccbP[0] ;
         n1157lccbP = P007N49_n1157lccbP[0] ;
         A1162lccbR = P007N49_A1162lccbR[0] ;
         n1162lccbR = P007N49_n1162lccbR[0] ;
         A1160lccbP = P007N49_A1160lccbP[0] ;
         n1160lccbP = P007N49_n1160lccbP[0] ;
         AV413GXLvl = (byte)(1) ;
         AV244lccbP = A1156lccbP ;
         AV245lccbP = A1157lccbP ;
         AV275lccbR = A1162lccbR ;
         AV287lccbP = A1160lccbP ;
         AV351lccbP = A1160lccbP ;
         pr_default.readNext(47);
      }
      pr_default.close(47);
      if ( ( AV413GXLvl == 0 ) )
      {
         AV351lccbP = 0 ;
         AV253lccbP = "" ;
      }
   }

   public void S391( )
   {
      /* 'CHECK_INSTALLMENTAMOUNT' Routine */
      AV281Decis = "" ;
      if ( ( GXutil.len( AV234PLP_p) == 0 ) )
      {
         AV144SCETe = "Falta valor parcela: " ;
         /* Execute user subroutine: S431 */
         S431 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         AV78i = 1 ;
         while ( ( AV78i <= GXutil.len( AV234PLP_p) ) )
         {
            if ( ( GXutil.strSearch( "1234567890", GXutil.substring( AV234PLP_p, AV78i, 1), 1) == 0 ) )
            {
               AV144SCETe = "Erro na constru��o do PLP. Valor das Parcelas inv�lido: " + GXutil.trim( AV235FPAC) + " " ;
               /* Execute user subroutine: S431 */
               S431 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if (true) break;
            }
            AV78i = (int)(AV78i+1) ;
         }
      }
      AV215sAMOU = AV234PLP_p ;
      AV216CUTP = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp() ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV231PLP_V = AV214valor ;
   }

   public void S431( )
   {
      /* 'PLP_PROBLEMA1' Routine */
      /* Using cursor P007N50 */
      pr_default.execute(48, new Object[] {AV238lccbE});
      while ( (pr_default.getStatus(48) != 101) )
      {
         A1166lccbP = P007N50_A1166lccbP[0] ;
         A1150lccbE = P007N50_A1150lccbE[0] ;
         A1513lccbD = P007N50_A1513lccbD[0] ;
         AV144SCETe = A1513lccbD ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(48);
      if ( ( AV229PLP_q == 1 ) )
      {
         /* Execute user subroutine: S481 */
         S481 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV145SCETp = "408" ;
         AV342SCEFo = "W" ;
         AV228PLP_o = (byte)(2) ;
         AV281Decis = "O" ;
      }
      else if ( ( GXutil.strcmp(AV239PLP_D, "V") == 0 ) )
      {
         /* Execute user subroutine: S481 */
         S481 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV145SCETp = "401" ;
         AV342SCEFo = "W" ;
         AV228PLP_o = (byte)(2) ;
         AV281Decis = "O" ;
      }
      else if ( ( GXutil.strcmp(AV239PLP_D, "C") == 0 ) || ( GXutil.strcmp(AV239PLP_D, "Y") == 0 ) )
      {
         /* Execute user subroutine: S491 */
         S491 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV145SCETp = "402" ;
         AV342SCEFo = "W" ;
         AV228PLP_o = (byte)(3) ;
         AV281Decis = "Y" ;
      }
      else
      {
         AV145SCETp = "403" ;
         AV342SCEFo = "E" ;
         AV228PLP_o = (byte)(0) ;
         AV281Decis = "N" ;
      }
      /* Execute user subroutine: S501 */
      S501 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S471( )
   {
      /* 'PLP_PROBLEMA2' Routine */
      /* Using cursor P007N51 */
      pr_default.execute(49, new Object[] {AV238lccbE});
      while ( (pr_default.getStatus(49) != 101) )
      {
         A1166lccbP = P007N51_A1166lccbP[0] ;
         A1150lccbE = P007N51_A1150lccbE[0] ;
         A1513lccbD = P007N51_A1513lccbD[0] ;
         AV144SCETe = A1513lccbD ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(49);
      if ( ( GXutil.strcmp(AV240PLP_D, "R") == 0 ) || ( GXutil.strcmp(AV240PLP_D, "N") == 0 ) )
      {
         AV145SCETp = "404" ;
         AV228PLP_o = (byte)(1) ;
         AV281Decis = "N" ;
         AV342SCEFo = "W" ;
      }
      else
      {
         AV145SCETp = "405" ;
         AV228PLP_o = (byte)(0) ;
         AV281Decis = "Y" ;
         AV342SCEFo = "E" ;
      }
      /* Execute user subroutine: S501 */
      S501 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S421( )
   {
      /* 'PLP_PROBLEMA3' Routine */
      /* Using cursor P007N52 */
      pr_default.execute(50, new Object[] {AV238lccbE});
      while ( (pr_default.getStatus(50) != 101) )
      {
         A1166lccbP = P007N52_A1166lccbP[0] ;
         A1150lccbE = P007N52_A1150lccbE[0] ;
         A1513lccbD = P007N52_A1513lccbD[0] ;
         AV144SCETe = A1513lccbD ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(50);
      if ( ( GXutil.strcmp(AV241PLP_D, "R") == 0 ) )
      {
         AV145SCETp = "406" ;
         AV228PLP_o = (byte)(1) ;
         AV281Decis = "Y" ;
         AV342SCEFo = "W" ;
      }
      else if ( ( GXutil.strcmp(AV241PLP_D, "C") == 0 ) )
      {
         AV145SCETp = "407" ;
         AV228PLP_o = (byte)(1) ;
         AV281Decis = "Y" ;
         /* Execute user subroutine: S501 */
         S501 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV342SCEFo = "W" ;
      }
      else
      {
         AV145SCETp = "410" ;
         AV228PLP_o = (byte)(0) ;
         AV281Decis = "Y" ;
         AV342SCEFo = "E" ;
      }
      /* Execute user subroutine: S501 */
      S501 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S461( )
   {
      /* 'PLP_PROBLEMA4' Routine */
      /* Using cursor P007N53 */
      pr_default.execute(51, new Object[] {AV238lccbE});
      while ( (pr_default.getStatus(51) != 101) )
      {
         A1166lccbP = P007N53_A1166lccbP[0] ;
         A1150lccbE = P007N53_A1150lccbE[0] ;
         A1513lccbD = P007N53_A1513lccbD[0] ;
         AV144SCETe = A1513lccbD ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(51);
      if ( ( GXutil.strcmp(AV284PLP_D, "R") == 0 ) )
      {
         AV145SCETp = "411" ;
         AV228PLP_o = (byte)(1) ;
         AV342SCEFo = "W" ;
         AV281Decis = "Y" ;
      }
      else
      {
         AV145SCETp = "412" ;
         AV228PLP_o = (byte)(0) ;
         AV342SCEFo = "E" ;
         AV281Decis = "N" ;
      }
      /* Execute user subroutine: S501 */
      S501 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S501( )
   {
      /* 'GRAVAVALORORIGINAL' Routine */
      AV335lccbO = AV229PLP_q ;
      AV336lccbO = AV236PLP_V ;
      AV348Origi = "Y" ;
   }

   public void S481( )
   {
      /* 'PLP_TOCC' Routine */
      AV218fop2I = 6 ;
      /* Execute user subroutine: S271 */
      S271 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV216CUTP = "XXX0" ;
      AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV242valor = AV214valor ;
      AV215sAMOU = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV242valor = (double)(AV242valor+AV214valor) ;
      AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXutil.right( "00000000000"+GXutil.trim( GXutil.str( AV242valor, 10, 0)), 11) );
      /* Using cursor P007N54 */
      pr_default.execute(52, new Object[] {AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos(), AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos()});
      while ( (pr_default.getStatus(52) != 101) )
      {
         A993HntUsu = P007N54_A993HntUsu[0] ;
         n993HntUsu = P007N54_n993HntUsu[0] ;
         A997HntSeq = P007N54_A997HntSeq[0] ;
         A994HntLin = P007N54_A994HntLin[0] ;
         n994HntLin = P007N54_n994HntLin[0] ;
         A998HntSub = P007N54_A998HntSub[0] ;
         if ( GXutil.like( A993HntUsu , GXutil.padr( "RETClone%" , 20 , "%")) )
         {
            AV88j = (short)(27+(AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq()-1)*123) ;
            AV22AuxLin = A994HntLin ;
            GXt_char10 = A994HntLin ;
            GXv_char3[0] = AV22AuxLin ;
            GXv_int14[0] = AV88j ;
            GXv_char2[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
            GXv_char15[0] = GXt_char10 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char3, GXv_int14, GXv_char2, GXv_char15) ;
            areticsi_bkp.this.AV22AuxLin = GXv_char3[0] ;
            areticsi_bkp.this.AV88j = GXv_int14[0] ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXv_char2[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char15[0] ;
            A994HntLin = GXt_char10 ;
            n994HntLin = false ;
            /* Using cursor P007N55 */
            pr_default.execute(53, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(52);
      }
      pr_default.close(52);
      AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXutil.space( (short)(10)) );
      /* Using cursor P007N56 */
      pr_default.execute(54, new Object[] {AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos(), AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos()});
      while ( (pr_default.getStatus(54) != 101) )
      {
         A993HntUsu = P007N56_A993HntUsu[0] ;
         n993HntUsu = P007N56_n993HntUsu[0] ;
         A997HntSeq = P007N56_A997HntSeq[0] ;
         A994HntLin = P007N56_A994HntLin[0] ;
         n994HntLin = P007N56_n994HntLin[0] ;
         A998HntSub = P007N56_A998HntSub[0] ;
         if ( GXutil.like( A993HntUsu , GXutil.padr( "RETClone%" , 20 , "%")) )
         {
            AV88j = (short)(50+(AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq()-1)*123) ;
            AV22AuxLin = A994HntLin ;
            GXt_char10 = A994HntLin ;
            GXv_char15[0] = AV22AuxLin ;
            GXv_int14[0] = AV88j ;
            GXv_char3[0] = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
            GXv_char2[0] = GXt_char10 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char15, GXv_int14, GXv_char3, GXv_char2) ;
            areticsi_bkp.this.AV22AuxLin = GXv_char15[0] ;
            areticsi_bkp.this.AV88j = GXv_int14[0] ;
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXv_char3[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
            A994HntLin = GXt_char10 ;
            n994HntLin = false ;
            /* Using cursor P007N57 */
            pr_default.execute(55, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(54);
      }
      pr_default.close(54);
      AV243FlagI = (byte)(0) ;
      AV285lccbF = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac() ;
      AV261lccbP = (short)(GXutil.val( AV145SCETp, ".")) ;
      AV263lccbS = AV144SCETe ;
      AV295FlagA = (byte)(0) ;
      /* Execute user subroutine: S281 */
      S281 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S491( )
   {
      /* 'PLP_TOCM' Routine */
      AV218fop2I = 7 ;
      /* Execute user subroutine: S271 */
      S271 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV216CUTP = "XXX0" ;
      AV215sAMOU = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV242valor = AV214valor ;
      AV215sAMOU = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
      /* Execute user subroutine: S185 */
      S185 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV242valor = (double)(AV242valor+AV214valor) ;
      AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXutil.right( "00000000000"+GXutil.trim( GXutil.str( AV242valor, 10, 0)), 11) );
      AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( "CM"+GXutil.right( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), 8) );
      /* Using cursor P007N58 */
      pr_default.execute(56, new Object[] {AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos(), AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos()});
      while ( (pr_default.getStatus(56) != 101) )
      {
         A993HntUsu = P007N58_A993HntUsu[0] ;
         n993HntUsu = P007N58_n993HntUsu[0] ;
         A997HntSeq = P007N58_A997HntSeq[0] ;
         A994HntLin = P007N58_A994HntLin[0] ;
         n994HntLin = P007N58_n994HntLin[0] ;
         A998HntSub = P007N58_A998HntSub[0] ;
         if ( GXutil.like( A993HntUsu , GXutil.padr( "RETClone%" , 20 , "%")) )
         {
            AV88j = (short)(27+(AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq()-1)*123) ;
            AV22AuxLin = A994HntLin ;
            GXt_char10 = AV22AuxLin ;
            GXv_char15[0] = AV22AuxLin ;
            GXv_int14[0] = AV88j ;
            GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam() ;
            GXv_char2[0] = GXt_char10 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char15, GXv_int14, GXv_char3, GXv_char2) ;
            areticsi_bkp.this.AV22AuxLin = GXv_char15[0] ;
            areticsi_bkp.this.AV88j = GXv_int14[0] ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( GXv_char3[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
            AV22AuxLin = GXt_char10 ;
            AV88j = (short)(50+(AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq()-1)*123) ;
            GXt_char10 = A994HntLin ;
            GXv_char15[0] = AV22AuxLin ;
            GXv_int14[0] = AV88j ;
            GXv_char3[0] = AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
            GXv_char2[0] = GXt_char10 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char15, GXv_int14, GXv_char3, GXv_char2) ;
            areticsi_bkp.this.AV22AuxLin = GXv_char15[0] ;
            areticsi_bkp.this.AV88j = GXv_int14[0] ;
            AV210retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXv_char3[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
            A994HntLin = GXt_char10 ;
            n994HntLin = false ;
            A994HntLin = AV22AuxLin ;
            n994HntLin = false ;
            /* Using cursor P007N59 */
            pr_default.execute(57, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(56);
      }
      pr_default.close(56);
      AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXutil.space( (short)(10)) );
      /* Using cursor P007N60 */
      pr_default.execute(58, new Object[] {AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos(), AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos()});
      while ( (pr_default.getStatus(58) != 101) )
      {
         A993HntUsu = P007N60_A993HntUsu[0] ;
         n993HntUsu = P007N60_n993HntUsu[0] ;
         A997HntSeq = P007N60_A997HntSeq[0] ;
         A994HntLin = P007N60_A994HntLin[0] ;
         n994HntLin = P007N60_n994HntLin[0] ;
         A998HntSub = P007N60_A998HntSub[0] ;
         if ( GXutil.like( A993HntUsu , GXutil.padr( "RETClone%" , 20 , "%")) )
         {
            AV88j = (short)(50+(AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq()-1)*123) ;
            AV22AuxLin = A994HntLin ;
            GXt_char10 = A994HntLin ;
            GXv_char15[0] = AV22AuxLin ;
            GXv_int14[0] = AV88j ;
            GXv_char3[0] = AV217retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp() ;
            GXv_char2[0] = GXt_char10 ;
            new pr2insertintostring(remoteHandle, context).execute( GXv_char15, GXv_int14, GXv_char3, GXv_char2) ;
            areticsi_bkp.this.AV22AuxLin = GXv_char15[0] ;
            areticsi_bkp.this.AV88j = GXv_int14[0] ;
            AV217retfo.setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( GXv_char3[0] );
            areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
            A994HntLin = GXt_char10 ;
            n994HntLin = false ;
            /* Using cursor P007N61 */
            pr_default.execute(59, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
         }
         pr_default.readNext(58);
      }
      pr_default.close(58);
   }

   public void S185( )
   {
      /* 'GETAMOUNTVALUE' Routine */
      AV214valor = (double)(GXutil.val( AV215sAMOU, ".")/ (double) ((java.lang.Math.pow(10,GXutil.val( GXutil.substring( AV216CUTP, 4, 1), "."))))) ;
   }

   public void S251( )
   {
      /* 'CHECKBIN' Routine */
      AV223orgEm = GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(), 3, 2) ;
      GX_I = 1 ;
      while ( ( GX_I <= 3 ) )
      {
         AV186Found[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV200EmpCC = "" ;
      AV78i = 1 ;
      AV222OKBin = (byte)(1) ;
      while ( ( AV78i <= AV182qtdBi ) && ( GXutil.strcmp(AV200EmpCC, "") == 0 ) )
      {
         AV260lBIN = (byte)(GXutil.len( AV181ListB[AV78i-1][1-1])) ;
         if ( ( GXutil.strcmp(AV181ListB[AV78i-1][1-1], GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac(), 1, AV260lBIN)) == 0 ) && ( GXutil.len( GXutil.trim( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac())) == GXutil.val( AV181ListB[AV78i-1][4-1], ".") ) )
         {
            if ( ( GXutil.strcmp(AV186Found[1-1], "") == 0 ) )
            {
               if ( ( GXutil.len( GXutil.trim( AV181ListB[AV78i-1][2-1])) == 2 ) )
               {
                  AV186Found[1-1] = AV181ListB[AV78i-1][1-1] ;
                  AV186Found[2-1] = GXutil.trim( GXutil.upper( AV181ListB[AV78i-1][2-1])) ;
                  AV186Found[3-1] = AV181ListB[AV78i-1][6-1] ;
               }
            }
            else
            {
               AV145SCETp = "904" ;
               AV342SCEFo = "L" ;
               AV144SCETe = "N�o foi poss�vel definir a Bandeira do Cart�o. Mais de uma possibilidade para o BIN " + GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac(), 1, AV260lBIN) ;
               /* Execute user subroutine: S23516 */
               S23516 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV222OKBin = (byte)(0) ;
               if (true) break;
            }
         }
         AV78i = (int)(AV78i+1) ;
      }
      if ( ( AV222OKBin == 1 ) && ( GXutil.strcmp(AV186Found[1-1], "") != 0 ) )
      {
         if ( ( GXutil.strcmp(AV186Found[3-1], "1") == 0 ) )
         {
            AV200EmpCC = AV186Found[2-1] ;
            if ( ( GXutil.strcmp(AV200EmpCC, AV223orgEm) != 0 ) )
            {
               AV203Count = (int)(AV203Count+1) ;
               AV422GXLvl = (byte)(0) ;
               /* Using cursor P007N62 */
               pr_default.execute(60, new Object[] {AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos(), AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos()});
               while ( (pr_default.getStatus(60) != 101) )
               {
                  A993HntUsu = P007N62_A993HntUsu[0] ;
                  n993HntUsu = P007N62_n993HntUsu[0] ;
                  A997HntSeq = P007N62_A997HntSeq[0] ;
                  A994HntLin = P007N62_A994HntLin[0] ;
                  n994HntLin = P007N62_n994HntLin[0] ;
                  A998HntSub = P007N62_A998HntSub[0] ;
                  if ( GXutil.like( A993HntUsu , GXutil.padr( "RETClone%" , 20 , "%")) )
                  {
                     AV422GXLvl = (byte)(1) ;
                     AV88j = (short)(52+(AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq()-1)*123) ;
                     AV22AuxLin = A994HntLin ;
                     GXt_char10 = A994HntLin ;
                     GXv_char15[0] = AV22AuxLin ;
                     GXv_int14[0] = AV88j ;
                     GXv_char3[0] = AV200EmpCC ;
                     GXv_char2[0] = GXt_char10 ;
                     new pr2insertintostring(remoteHandle, context).execute( GXv_char15, GXv_int14, GXv_char3, GXv_char2) ;
                     areticsi_bkp.this.AV22AuxLin = GXv_char15[0] ;
                     areticsi_bkp.this.AV88j = GXv_int14[0] ;
                     areticsi_bkp.this.AV200EmpCC = GXv_char3[0] ;
                     areticsi_bkp.this.GXt_char10 = GXv_char2[0] ;
                     A994HntLin = GXt_char10 ;
                     n994HntLin = false ;
                     AV145SCETp = "L" ;
                     AV342SCEFo = "L" ;
                     AV144SCETe = "Empresa " + AV223orgEm + " trocada para " + AV200EmpCC + " devido � tabela de BINs" ;
                     /* Execute user subroutine: S23516 */
                     S23516 ();
                     if ( returnInSub )
                     {
                        pr_default.close(60);
                        returnInSub = true;
                        if (true) return;
                     }
                     /* Using cursor P007N63 */
                     pr_default.execute(61, new Object[] {new Boolean(n994HntLin), A994HntLin, A997HntSeq, new Byte(A998HntSub)});
                  }
                  pr_default.readNext(60);
               }
               pr_default.close(60);
               if ( ( AV422GXLvl == 0 ) )
               {
                  context.msgStatus( "    Erro fatal: 0001" );
               }
            }
         }
         else
         {
            AV222OKBin = (byte)(0) ;
         }
      }
      else
      {
         AV78i = 1 ;
         while ( ( AV78i <= AV182qtdBi ) )
         {
            if ( ( GXutil.strcmp(AV223orgEm, AV181ListB[AV78i-1][2-1]) == 0 ) && ( GXutil.len( GXutil.trim( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac())) == GXutil.val( AV181ListB[AV78i-1][4-1], ".") ) && ( GXutil.val( AV181ListB[AV78i-1][6-1], ".") == 1 ) )
            {
               if (true) break;
            }
            AV78i = (int)(AV78i+1) ;
         }
         if ( ( AV78i <= AV182qtdBi ) )
         {
            AV222OKBin = (byte)(1) ;
            AV200EmpCC = AV181ListB[AV78i-1][2-1] ;
            AV144SCETe = "BIN " + GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac(), 1, AV260lBIN) + " n�o cadastrado, usado FPTP (" + AV200EmpCC + ")" ;
            AV342SCEFo = "L" ;
            AV145SCETp = "L" ;
         }
         else
         {
            AV222OKBin = (byte)(0) ;
            AV144SCETe = "BIN " + GXutil.substring( AV210retfo.getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac(), 1, AV260lBIN) + " n�o encontrado para FPTP (" + AV223orgEm + ")" + " ser� faturado fora do ICSI" ;
            AV342SCEFo = "L" ;
            AV145SCETp = "905" ;
            AV200EmpCC = "" ;
         }
         /* Execute user subroutine: S23516 */
         S23516 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV206retva = AV222OKBin ;
   }

   public void S131( )
   {
      /* 'DELRETRECORDS' Routine */
      context.msgStatus( "  Deleting RET records already processed - Started at "+GXutil.time( )+" ..." );
      /* Using cursor P007N64 */
      pr_default.execute(62, new Object[] {AV178Filen, AV178Filen});
      while ( (pr_default.getStatus(62) != 101) )
      {
         A1208lccbR = P007N64_A1208lccbR[0] ;
         n1208lccbR = P007N64_n1208lccbR[0] ;
         A1150lccbE = P007N64_A1150lccbE[0] ;
         A1222lccbI = P007N64_A1222lccbI[0] ;
         A1223lccbD = P007N64_A1223lccbD[0] ;
         A1224lccbC = P007N64_A1224lccbC[0] ;
         A1225lccbC = P007N64_A1225lccbC[0] ;
         A1226lccbA = P007N64_A1226lccbA[0] ;
         A1227lccbO = P007N64_A1227lccbO[0] ;
         A1228lccbF = P007N64_A1228lccbF[0] ;
         A1231lccbT = P007N64_A1231lccbT[0] ;
         A1232lccbT = P007N64_A1232lccbT[0] ;
         AV238lccbE = A1150lccbE ;
         AV247lccbi = A1222lccbI ;
         AV248lccbD = A1223lccbD ;
         AV249lccbC = A1224lccbC ;
         AV250lccbC = A1225lccbC ;
         AV251lccbA = A1226lccbA ;
         AV266lccbO = A1227lccbO ;
         AV285lccbF = A1228lccbF ;
         AV258lccbT = A1231lccbT ;
         /* Execute user subroutine: S5148 */
         S5148 ();
         if ( returnInSub )
         {
            pr_default.close(62);
            returnInSub = true;
            if (true) return;
         }
         if ( ( AV265DelFl == 1 ) )
         {
            AV268Cnt_d = (int)(AV268Cnt_d+1) ;
            /* Using cursor P007N65 */
            pr_default.execute(63, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1231lccbT, A1232lccbT});
         }
         pr_default.readNext(62);
      }
      pr_default.close(62);
      context.msgStatus( "    Deleted "+GXutil.trim( GXutil.str( AV268Cnt_d, 8, 0))+" documents and "+GXutil.trim( GXutil.str( AV269Cnt_d, 8, 0))+" submissions" );
      context.msgStatus( "    Ended at "+GXutil.time( ) );
   }

   public void S5148( )
   {
      /* 'DELLCCBPLP' Routine */
      AV265DelFl = (byte)(1) ;
      /* Using cursor P007N66 */
      pr_default.execute(64, new Object[] {AV238lccbE, AV247lccbi, AV248lccbD, AV249lccbC, AV250lccbC, AV251lccbA, AV266lccbO, AV285lccbF, AV238lccbE, AV247lccbi, AV248lccbD, AV249lccbC, AV250lccbC, AV251lccbA, AV266lccbO, AV285lccbF});
      while ( (pr_default.getStatus(64) != 101) )
      {
         A1228lccbF = P007N66_A1228lccbF[0] ;
         A1227lccbO = P007N66_A1227lccbO[0] ;
         A1226lccbA = P007N66_A1226lccbA[0] ;
         A1225lccbC = P007N66_A1225lccbC[0] ;
         A1224lccbC = P007N66_A1224lccbC[0] ;
         A1223lccbD = P007N66_A1223lccbD[0] ;
         A1222lccbI = P007N66_A1222lccbI[0] ;
         A1150lccbE = P007N66_A1150lccbE[0] ;
         A1172lccbS = P007N66_A1172lccbS[0] ;
         n1172lccbS = P007N66_n1172lccbS[0] ;
         A1184lccbS = P007N66_A1184lccbS[0] ;
         n1184lccbS = P007N66_n1184lccbS[0] ;
         if ( ( GXutil.strcmp(A1184lccbS, "TOSUB") == 0 ) || ( GXutil.strcmp(A1184lccbS, "NOSUB") == 0 ) || ( GXutil.strcmp(A1184lccbS, "PENDS") == 0 ) )
         {
            /* Using cursor P007N67 */
            pr_default.execute(65, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            AV269Cnt_d = (int)(AV269Cnt_d+1) ;
            AV278lccbS = "REPROC" ;
            AV263lccbS = "RET Reprocessada" ;
            AV296lccbS = "INFO" ;
            /* Execute user subroutine: S30799 */
            S30799 ();
            if ( returnInSub )
            {
               pr_default.close(64);
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV145SCETp = "911" ;
            AV144SCETe = "Documento " + AV258lccbT + "/" + AV249lccbC + " n�o pode ser recarregado pois j� foi processado pelo ICSI" ;
            AV342SCEFo = "L" ;
            /* Execute user subroutine: S23516 */
            S23516 ();
            if ( returnInSub )
            {
               pr_default.close(64);
               returnInSub = true;
               if (true) return;
            }
            AV265DelFl = (byte)(0) ;
         }
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(64);
   }

   public void S351( )
   {
      /* 'GETFPACFROMMAIN' Routine */
      AV425GXLvl = (byte)(0) ;
      /* Using cursor P007N68 */
      pr_default.execute(66, new Object[] {AV238lccbE, AV247lccbi, AV248lccbD, AV249lccbC, AV250lccbC, AV251lccbA, AV266lccbO, AV235FPAC});
      while ( (pr_default.getStatus(66) != 101) )
      {
         A1228lccbF = P007N68_A1228lccbF[0] ;
         A1227lccbO = P007N68_A1227lccbO[0] ;
         A1226lccbA = P007N68_A1226lccbA[0] ;
         A1225lccbC = P007N68_A1225lccbC[0] ;
         A1224lccbC = P007N68_A1224lccbC[0] ;
         A1223lccbD = P007N68_A1223lccbD[0] ;
         A1222lccbI = P007N68_A1222lccbI[0] ;
         A1150lccbE = P007N68_A1150lccbE[0] ;
         A1167lccbG = P007N68_A1167lccbG[0] ;
         n1167lccbG = P007N68_n1167lccbG[0] ;
         A1172lccbS = P007N68_A1172lccbS[0] ;
         n1172lccbS = P007N68_n1172lccbS[0] ;
         A1519lccbO = P007N68_A1519lccbO[0] ;
         n1519lccbO = P007N68_n1519lccbO[0] ;
         A1189lccbS = P007N68_A1189lccbS[0] ;
         n1189lccbS = P007N68_n1189lccbS[0] ;
         A1179lccbV = P007N68_A1179lccbV[0] ;
         n1179lccbV = P007N68_n1179lccbV[0] ;
         A1180lccbC = P007N68_A1180lccbC[0] ;
         n1180lccbC = P007N68_n1180lccbC[0] ;
         A1181lccbC = P007N68_A1181lccbC[0] ;
         n1181lccbC = P007N68_n1181lccbC[0] ;
         A1182lccbS = P007N68_A1182lccbS[0] ;
         n1182lccbS = P007N68_n1182lccbS[0] ;
         AV425GXLvl = (byte)(1) ;
         AV264lccbS = A1172lccbS ;
         AV376lccbO = A1519lccbO ;
         AV304lccbS = A1189lccbS ;
         AV277lccbV = A1179lccbV ;
         AV259lccbC = A1180lccbC ;
         AV305lccbC = GXutil.substring( A1181lccbC, 1, 3) ;
         AV306LccbS = A1182lccbS ;
         AV252lccbG = A1167lccbG ;
         /* Using cursor P007N69 */
         pr_default.execute(67, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, AV158TDNR, AV165TRNC});
         while ( (pr_default.getStatus(67) != 101) )
         {
            A1231lccbT = P007N69_A1231lccbT[0] ;
            A1232lccbT = P007N69_A1232lccbT[0] ;
            A1210lccbF = P007N69_A1210lccbF[0] ;
            n1210lccbF = P007N69_n1210lccbF[0] ;
            A1211lccbF = P007N69_A1211lccbF[0] ;
            n1211lccbF = P007N69_n1211lccbF[0] ;
            A1212lccbF = P007N69_A1212lccbF[0] ;
            n1212lccbF = P007N69_n1212lccbF[0] ;
            A1213lccbI = P007N69_A1213lccbI[0] ;
            n1213lccbI = P007N69_n1213lccbI[0] ;
            A1214lccbI = P007N69_A1214lccbI[0] ;
            n1214lccbI = P007N69_n1214lccbI[0] ;
            A1215lccbF = P007N69_A1215lccbF[0] ;
            n1215lccbF = P007N69_n1215lccbF[0] ;
            A1216lccbF = P007N69_A1216lccbF[0] ;
            n1216lccbF = P007N69_n1216lccbF[0] ;
            A1217lccbF = P007N69_A1217lccbF[0] ;
            n1217lccbF = P007N69_n1217lccbF[0] ;
            A1218lccbI = P007N69_A1218lccbI[0] ;
            n1218lccbI = P007N69_n1218lccbI[0] ;
            A1219lccbI = P007N69_A1219lccbI[0] ;
            n1219lccbI = P007N69_n1219lccbI[0] ;
            A1220lccbT = P007N69_A1220lccbT[0] ;
            n1220lccbT = P007N69_n1220lccbT[0] ;
            A1221lccbF = P007N69_A1221lccbF[0] ;
            n1221lccbF = P007N69_n1221lccbF[0] ;
            A1207lccbP = P007N69_A1207lccbP[0] ;
            n1207lccbP = P007N69_n1207lccbP[0] ;
            A1208lccbR = P007N69_A1208lccbR[0] ;
            n1208lccbR = P007N69_n1208lccbR[0] ;
            AV307lccbF = A1210lccbF ;
            AV308lccbF = A1211lccbF ;
            AV309lccbF = A1212lccbF ;
            AV310lccbI = A1213lccbI ;
            AV311lccbI = A1214lccbI ;
            AV312lccbF = A1215lccbF ;
            AV313lccbF = A1216lccbF ;
            AV314lccbF = A1217lccbF ;
            AV315lccbI = A1218lccbI ;
            AV316lccbI = A1219lccbI ;
            AV317lccbT = A1220lccbT ;
            AV318lccbF = A1221lccbF ;
            AV319lccbP = A1207lccbP ;
            AV320lccbR = GXutil.substring( A1208lccbR, 1, 20) ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(67);
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(66);
      if ( ( AV425GXLvl == 0 ) )
      {
         context.msgStatus( "    FATAL ERROR: GetFPACfromMain incorrect" );
      }
      AV427GXLvl = (byte)(0) ;
      /* Using cursor P007N70 */
      pr_default.execute(68, new Object[] {AV238lccbE, AV247lccbi, AV248lccbD, AV249lccbC, AV250lccbC, AV251lccbA, AV266lccbO});
      while ( (pr_default.getStatus(68) != 101) )
      {
         A1227lccbO = P007N70_A1227lccbO[0] ;
         A1226lccbA = P007N70_A1226lccbA[0] ;
         A1225lccbC = P007N70_A1225lccbC[0] ;
         A1224lccbC = P007N70_A1224lccbC[0] ;
         A1223lccbD = P007N70_A1223lccbD[0] ;
         A1150lccbE = P007N70_A1150lccbE[0] ;
         A1222lccbI = P007N70_A1222lccbI[0] ;
         A1167lccbG = P007N70_A1167lccbG[0] ;
         n1167lccbG = P007N70_n1167lccbG[0] ;
         A1228lccbF = P007N70_A1228lccbF[0] ;
         AV427GXLvl = (byte)(1) ;
         AV297FPACo = A1228lccbF ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(68);
      }
      pr_default.close(68);
      if ( ( AV427GXLvl == 0 ) )
      {
         AV297FPACo = AV235FPAC ;
      }
      if ( ( GXutil.strcmp(AV235FPAC, AV297FPACo) != 0 ) )
      {
         AV329Atrib = GXutil.trim( AV250lccbC) ;
         GXv_char15[0] = AV330Resul ;
         new pcrypto(remoteHandle, context).execute( AV329Atrib, "D", GXv_char15) ;
         areticsi_bkp.this.AV330Resul = GXv_char15[0] ;
         AV328LccbC = AV330Resul ;
         AV144SCETe = AV249lccbC + "/" + GXutil.trim( AV328LccbC) + "/" + GXutil.trim( AV251lccbA) + ": Grupo de Cr�dito com FPAC diferentes([" + AV235FPAC + "]<>[" + AV297FPACo + "])" ;
         if ( ( AV298FlagK == 0 ) )
         {
            AV235FPAC = AV297FPACo ;
            AV145SCETp = "906" ;
            AV144SCETe = AV144SCETe + ". Assumido o FPAC principal = [" + AV297FPACo + "]" ;
            AV342SCEFo = "L" ;
         }
         else
         {
            AV145SCETp = "907" ;
         }
         /* Execute user subroutine: S23516 */
         S23516 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S311( )
   {
      /* 'SEARCHSUBFILE' Routine */
      AV428GXLvl = (byte)(0) ;
      /* Using cursor P007N71 */
      pr_default.execute(69, new Object[] {AV158TDNR});
      while ( (pr_default.getStatus(69) != 101) )
      {
         A1231lccbT = P007N71_A1231lccbT[0] ;
         A1150lccbE = P007N71_A1150lccbE[0] ;
         A1222lccbI = P007N71_A1222lccbI[0] ;
         A1223lccbD = P007N71_A1223lccbD[0] ;
         A1224lccbC = P007N71_A1224lccbC[0] ;
         A1225lccbC = P007N71_A1225lccbC[0] ;
         A1226lccbA = P007N71_A1226lccbA[0] ;
         A1227lccbO = P007N71_A1227lccbO[0] ;
         A1228lccbF = P007N71_A1228lccbF[0] ;
         A1232lccbT = P007N71_A1232lccbT[0] ;
         AV428GXLvl = (byte)(1) ;
         AV323FileD = "Y" ;
         AV324LogLi = "Ticket number : " + AV158TDNR + "; File name: " + AV178Filen + ";" + "Date: " + (GXutil.str( GXutil.year( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.month( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.day( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.hour( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.minute( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + GXutil.trim( GXutil.str( GXutil.second( GXutil.serverNow( context, remoteHandle, "DEFAULT")), 10, 0)) + ";" ;
         /* Execute user subroutine: S121 */
         S121 ();
         if ( returnInSub )
         {
            pr_default.close(69);
            returnInSub = true;
            if (true) return;
         }
         pr_default.readNext(69);
      }
      pr_default.close(69);
      if ( ( AV428GXLvl == 0 ) )
      {
         AV323FileD = "N" ;
      }
   }

   public void S121( )
   {
      /* 'WRITELOGLINE' Routine */
      AV327RetVa = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV324LogLi, (short)(63)) ;
      AV327RetVa = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
   }

   public void S261( )
   {
      /* 'VERIFICABILHETECASHMAIORTIP' Routine */
      AV383Bilhe = "N" ;
      AV384p = 1 ;
      while ( ( AV384p <= AV381Index ) )
      {
         if ( ( GXutil.strcmp(AV158TDNR, AV382Bilhe[AV384p-1]) == 0 ) )
         {
            AV383Bilhe = "Y" ;
         }
         AV384p = (int)(AV384p+1) ;
      }
   }

 /*  public static Object refClasses( )
   {
      GXutil.refClasses(preticsi_bkp.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = areticsi_bkp.this.AV176FileS;
      this.aP1[0] = areticsi_bkp.this.AV39DebugM;
      Application.commit(context, remoteHandle, "DEFAULT", "areticsi_bkp");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV178Filen = "" ;
      AV326LogFi = "" ;
      AV332Difer = "" ;
      AV153Versa = "" ;
      AV361Count = 0 ;
      AV373SCEFP = 0 ;
      AV374SCEPa = 0 ;
      AV381Index = 0 ;
      AV267TRN_C = 0 ;
      returnInSub = false ;
      AV274flagR = (byte)(0) ;
      AV282flagR = (byte)(0) ;
      AV327RetVa = 0 ;
      AV324LogLi = "" ;
      AV298FlagK = (byte)(0) ;
      scmdbuf = "" ;
      P007N2_A1228lccbF = new String[] {""} ;
      P007N2_A1227lccbO = new String[] {""} ;
      P007N2_A1226lccbA = new String[] {""} ;
      P007N2_A1225lccbC = new String[] {""} ;
      P007N2_A1224lccbC = new String[] {""} ;
      P007N2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N2_A1222lccbI = new String[] {""} ;
      P007N2_A1150lccbE = new String[] {""} ;
      P007N2_A1184lccbS = new String[] {""} ;
      P007N2_n1184lccbS = new boolean[] {false} ;
      P007N2_A1167lccbG = new String[] {""} ;
      P007N2_n1167lccbG = new boolean[] {false} ;
      A1228lccbF = "" ;
      A1227lccbO = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1150lccbE = "" ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1167lccbG = "" ;
      n1167lccbG = false ;
      AV169TRNN_ = "" ;
      P007N6_A994HntLin = new String[] {""} ;
      P007N6_n994HntLin = new boolean[] {false} ;
      P007N6_A993HntUsu = new String[] {""} ;
      P007N6_n993HntUsu = new boolean[] {false} ;
      P007N6_A997HntSeq = new String[] {""} ;
      P007N6_A998HntSub = new byte[1] ;
      A994HntLin = "" ;
      n994HntLin = false ;
      A993HntUsu = "" ;
      n993HntUsu = false ;
      A997HntSeq = "" ;
      A998HntSub = (byte)(0) ;
      AV12Linha = "" ;
      AV168TRNN_ = "" ;
      AV207LccbR = "" ;
      AV357Linha = "" ;
      AV127RCID = "" ;
      AV35CRS = "" ;
      AV148SPED = "" ;
      AV272CJCP = "" ;
      AV273Count = (byte)(0) ;
      AV271TDNR_ = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV271TDNR_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV167TRNN = "" ;
      AV8AGTN = "" ;
      AV158TDNR = "" ;
      AV152TACN = "" ;
      AV37DAIS = "" ;
      AV246PXNM = "" ;
      AV165TRNC = "" ;
      AV54Flag_I = (byte)(0) ;
      AV221Flag_ = (byte)(0) ;
      AV78i = 0 ;
      AV204qtdEm = 0 ;
      AV205ListE = new String [100][5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 100 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 5 ) )
         {
            AV205ListE[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      AV239PLP_D = "" ;
      AV240PLP_D = "" ;
      AV241PLP_D = "" ;
      AV284PLP_D = "" ;
      AV289Total = 0 ;
      AV236PLP_V = 0 ;
      AV209retfo = new GxObjectCollection(Sdtsdt_RETFOP_sdt_RETFOPItem.class, "sdt_RETFOP.sdt_RETFOPItem", "IataICSI");
      AV216CUTP = "" ;
      AV142s = "" ;
      AV88j = (short)(0) ;
      AV219TMFT = "" ;
      AV215sAMOU = "" ;
      AV220Total = 0 ;
      AV214valor = 0 ;
      AV379Total = 0 ;
      AV9Count_I = (byte)(0) ;
      AV210retfo = new Sdtsdt_RETFOP_sdt_RETFOPItem(context);
      AV290FPTP = "" ;
      GXt_char4 = "" ;
      AV182qtdBi = 0 ;
      GXt_char1 = "" ;
      AV180qtdFo = 0 ;
      GXt_char5 = "" ;
      GXt_char6 = "" ;
      AV203Count = 0 ;
      GXt_char7 = "" ;
      AV183TKT_C = new int [10] ;
      GXt_char8 = "" ;
      AV184RFN_C = new int [10] ;
      AV185MCO_C = new int [10] ;
      AV377lccbF = 0 ;
      AV283ICSII = "" ;
      GX_I = 0 ;
      A1305SCETe = "" ;
      GX_J = 0 ;
      AV391GXLvl = (byte)(0) ;
      P007N7_A1147lccbE = new String[] {""} ;
      P007N7_n1147lccbE = new boolean[] {false} ;
      P007N7_A1150lccbE = new String[] {""} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      AV238lccbE = "" ;
      AV392GXLvl = (byte)(0) ;
      P007N8_A1166lccbP = new String[] {""} ;
      P007N8_A1150lccbE = new String[] {""} ;
      P007N8_A1165lccbD = new String[] {""} ;
      P007N8_n1165lccbD = new boolean[] {false} ;
      A1166lccbP = "" ;
      A1165lccbD = "" ;
      n1165lccbD = false ;
      AV393GXLvl = (byte)(0) ;
      P007N9_A1166lccbP = new String[] {""} ;
      P007N9_A1150lccbE = new String[] {""} ;
      P007N9_A1165lccbD = new String[] {""} ;
      P007N9_n1165lccbD = new boolean[] {false} ;
      AV394GXLvl = (byte)(0) ;
      P007N10_A1166lccbP = new String[] {""} ;
      P007N10_A1150lccbE = new String[] {""} ;
      P007N10_A1165lccbD = new String[] {""} ;
      P007N10_n1165lccbD = new boolean[] {false} ;
      AV395GXLvl = (byte)(0) ;
      P007N11_A1166lccbP = new String[] {""} ;
      P007N11_A1150lccbE = new String[] {""} ;
      P007N11_A1165lccbD = new String[] {""} ;
      P007N11_n1165lccbD = new boolean[] {false} ;
      AV145SCETp = "" ;
      AV342SCEFo = "" ;
      AV144SCETe = "" ;
      AV179ListF = new String [20000][4] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 4 ) )
         {
            AV179ListF[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      P007N12_A1148lccbF = new String[] {""} ;
      P007N12_n1148lccbF = new boolean[] {false} ;
      P007N12_A1152lccbF = new String[] {""} ;
      P007N12_A1151lccbF = new String[] {""} ;
      P007N12_A1150lccbE = new String[] {""} ;
      A1148lccbF = "" ;
      n1148lccbF = false ;
      A1152lccbF = "" ;
      A1151lccbF = "" ;
      AV225qtdFo = 0 ;
      AV224ListF = new String [1000][3] ;
      GX_I = 1 ;
      while ( ( GX_I <= 1000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 3 ) )
         {
            AV224ListF[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      P007N13_A1294Serie = new long[1] ;
      P007N13_n1294Serie = new boolean[] {false} ;
      P007N13_A1296Serie = new long[1] ;
      P007N13_A1295DocCo = new String[] {""} ;
      A1294Serie = 0 ;
      n1294Serie = false ;
      A1296Serie = 0 ;
      A1295DocCo = "" ;
      AV181ListB = new String [1000][6] ;
      GX_I = 1 ;
      while ( ( GX_I <= 1000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 6 ) )
         {
            AV181ListB[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      P007N14_A1140lccbB = new String[] {""} ;
      P007N14_n1140lccbB = new boolean[] {false} ;
      P007N14_A1145lccbB = new byte[1] ;
      P007N14_A1141lccbB = new String[] {""} ;
      P007N14_n1141lccbB = new boolean[] {false} ;
      P007N14_A1142lccbB = new String[] {""} ;
      P007N14_n1142lccbB = new boolean[] {false} ;
      P007N14_A1144lccbE = new String[] {""} ;
      P007N14_A1143lccbB = new String[] {""} ;
      A1140lccbB = "" ;
      n1140lccbB = false ;
      A1145lccbB = (byte)(0) ;
      A1141lccbB = "" ;
      n1141lccbB = false ;
      A1142lccbB = "" ;
      n1142lccbB = false ;
      A1144lccbE = "" ;
      A1143lccbB = "" ;
      AV188qtdDo = 0 ;
      AV187ListD = new String [5000][3] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5000 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 3 ) )
         {
            AV187ListD[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      P007N15_A1149lccbD = new String[] {""} ;
      P007N15_n1149lccbD = new boolean[] {false} ;
      P007N15_A1154lccbD = new String[] {""} ;
      P007N15_A1153lccbD = new String[] {""} ;
      P007N15_A1150lccbE = new String[] {""} ;
      A1149lccbD = "" ;
      n1149lccbD = false ;
      A1154lccbD = "" ;
      A1153lccbD = "" ;
      AV106now = GXutil.resetTime( GXutil.nullDate() );
      GX_INS248 = 0 ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1303SCERE = "" ;
      n1303SCERE = false ;
      n1305SCETe = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "" ;
      n1310SCETp = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      A1311SCEVe = "" ;
      n1311SCEVe = false ;
      A1517SCEFP = 0 ;
      n1517SCEFP = false ;
      A1518SCEPa = 0 ;
      n1518SCEPa = false ;
      P007N17_A1312SCEId = new long[1] ;
      P007N17_n1312SCEId = new boolean[] {false} ;
      A1312SCEId = 0 ;
      n1312SCEId = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV354Exist = "" ;
      AV355SCEDa = GXutil.resetTime( GXutil.nullDate() );
      P007N18_A1309SCEFo = new String[] {""} ;
      P007N18_n1309SCEFo = new boolean[] {false} ;
      P007N18_A1299SCEDa = new java.util.Date[] {GXutil.nullDate()} ;
      P007N18_n1299SCEDa = new boolean[] {false} ;
      P007N18_A1298SCETk = new String[] {""} ;
      P007N18_n1298SCETk = new boolean[] {false} ;
      P007N18_A1312SCEId = new long[1] ;
      P007N18_n1312SCEId = new boolean[] {false} ;
      A1309SCEFo = "" ;
      n1309SCEFo = false ;
      P007N20_A1312SCEId = new long[1] ;
      P007N20_n1312SCEId = new boolean[] {false} ;
      AV206retva = (byte)(0) ;
      AV200EmpCC = "" ;
      AV372lccbC = 0 ;
      AV358Total = 0 ;
      AV360FareC = new String [25000] ;
      GX_I = 1 ;
      while ( ( GX_I <= 25000 ) )
      {
         AV360FareC[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV383Bilhe = "" ;
      AV382Bilhe = new String [20000] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20000 ) )
      {
         AV382Bilhe[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV211old_F = "" ;
      AV270ifop = 0 ;
      AV218fop2I = 0 ;
      AV212this_ = "" ;
      AV213next_ = "" ;
      AV217retfo = new Sdtsdt_RETFOP_sdt_RETFOPItem(context);
      AV243FlagI = (byte)(0) ;
      AV285lccbF = "" ;
      AV261lccbP = (short)(0) ;
      AV263lccbS = "" ;
      AV295FlagA = (byte)(0) ;
      AV279lccbS = (short)(0) ;
      AV280flagR = (byte)(0) ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1230lccbS = (short)(0) ;
      A1186lccbS = "" ;
      AV278lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1188lccbS = "" ;
      AV296lccbS = "" ;
      n1188lccbS = false ;
      AV264lccbS = 0 ;
      AV376lccbO = 0 ;
      AV338lccbO = 0 ;
      AV323FileD = "" ;
      AV321SaleA = 0 ;
      AV262lccbS = "" ;
      AV328LccbC = "" ;
      AV329Atrib = "" ;
      AV330Resul = "" ;
      AV370lccbC = 0 ;
      AV257lccbT = 0 ;
      GX_INS235 = 0 ;
      A1163lccbP = "" ;
      n1163lccbP = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1169lccbD = 0 ;
      n1169lccbD = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1179lccbV = "" ;
      n1179lccbV = false ;
      A1180lccbC = "" ;
      n1180lccbC = false ;
      A1181lccbC = "" ;
      n1181lccbC = false ;
      A1182lccbS = "" ;
      n1182lccbS = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1183lccbP = (short)(0) ;
      n1183lccbP = false ;
      A1189lccbS = GXutil.nullDate() ;
      Gx_date = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1495lccbC = GXutil.nullDate() ;
      n1495lccbC = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1192lccbS = "" ;
      n1192lccbS = false ;
      A1178lccbO = 0 ;
      n1178lccbO = false ;
      A1176lccbO = 0 ;
      AV337lccbO = 0 ;
      n1176lccbO = false ;
      A1177lccbO = 0 ;
      n1177lccbO = false ;
      A1515lccbC = 0 ;
      n1515lccbC = false ;
      A1514lccbF = 0 ;
      n1514lccbF = false ;
      A1516lccbC = 0 ;
      n1516lccbC = false ;
      A1519lccbO = 0 ;
      n1519lccbO = false ;
      AV247lccbi = "" ;
      AV248lccbD = GXutil.nullDate() ;
      AV249lccbC = "" ;
      AV250lccbC = "" ;
      AV251lccbA = "" ;
      AV266lccbO = "" ;
      GX_INS237 = 0 ;
      A1232lccbT = "" ;
      A1231lccbT = "" ;
      A1207lccbP = "" ;
      n1207lccbP = false ;
      A1208lccbR = "" ;
      n1208lccbR = false ;
      A1209lccbR = "" ;
      n1209lccbR = false ;
      P007N23_A1150lccbE = new String[] {""} ;
      P007N23_A1222lccbI = new String[] {""} ;
      P007N23_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N23_A1224lccbC = new String[] {""} ;
      P007N23_A1225lccbC = new String[] {""} ;
      P007N23_A1226lccbA = new String[] {""} ;
      P007N23_A1227lccbO = new String[] {""} ;
      P007N23_A1228lccbF = new String[] {""} ;
      P007N23_A1231lccbT = new String[] {""} ;
      P007N23_A1232lccbT = new String[] {""} ;
      P007N23_A1207lccbP = new String[] {""} ;
      P007N23_n1207lccbP = new boolean[] {false} ;
      P007N23_A1208lccbR = new String[] {""} ;
      P007N23_n1208lccbR = new boolean[] {false} ;
      P007N23_A1209lccbR = new String[] {""} ;
      P007N23_n1209lccbR = new boolean[] {false} ;
      P007N28_A1150lccbE = new String[] {""} ;
      P007N28_A1222lccbI = new String[] {""} ;
      P007N28_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N28_A1224lccbC = new String[] {""} ;
      P007N28_A1225lccbC = new String[] {""} ;
      P007N28_A1226lccbA = new String[] {""} ;
      P007N28_A1227lccbO = new String[] {""} ;
      P007N28_A1228lccbF = new String[] {""} ;
      P007N28_A1183lccbP = new short[1] ;
      P007N28_n1183lccbP = new boolean[] {false} ;
      P007N28_A1184lccbS = new String[] {""} ;
      P007N28_n1184lccbS = new boolean[] {false} ;
      P007N28_A1163lccbP = new String[] {""} ;
      P007N28_n1163lccbP = new boolean[] {false} ;
      P007N28_A1168lccbI = new short[1] ;
      P007N28_n1168lccbI = new boolean[] {false} ;
      P007N28_A1169lccbD = new double[1] ;
      P007N28_n1169lccbD = new boolean[] {false} ;
      P007N28_A1170lccbI = new double[1] ;
      P007N28_n1170lccbI = new boolean[] {false} ;
      P007N28_A1171lccbT = new double[1] ;
      P007N28_n1171lccbT = new boolean[] {false} ;
      P007N28_A1178lccbO = new double[1] ;
      P007N28_n1178lccbO = new boolean[] {false} ;
      P007N28_A1177lccbO = new double[1] ;
      P007N28_n1177lccbO = new boolean[] {false} ;
      P007N28_A1172lccbS = new double[1] ;
      P007N28_n1172lccbS = new boolean[] {false} ;
      P007N28_A1515lccbC = new double[1] ;
      P007N28_n1515lccbC = new boolean[] {false} ;
      P007N28_A1519lccbO = new double[1] ;
      P007N28_n1519lccbO = new boolean[] {false} ;
      P007N28_A1514lccbF = new double[1] ;
      P007N28_n1514lccbF = new boolean[] {false} ;
      P007N30_A1228lccbF = new String[] {""} ;
      P007N30_A1227lccbO = new String[] {""} ;
      P007N30_A1226lccbA = new String[] {""} ;
      P007N30_A1225lccbC = new String[] {""} ;
      P007N30_A1224lccbC = new String[] {""} ;
      P007N30_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N30_A1222lccbI = new String[] {""} ;
      P007N30_A1150lccbE = new String[] {""} ;
      P007N30_A1184lccbS = new String[] {""} ;
      P007N30_n1184lccbS = new boolean[] {false} ;
      P007N30_A1167lccbG = new String[] {""} ;
      P007N30_n1167lccbG = new boolean[] {false} ;
      P007N30_A1172lccbS = new double[1] ;
      P007N30_n1172lccbS = new boolean[] {false} ;
      P007N30_A1169lccbD = new double[1] ;
      P007N30_n1169lccbD = new boolean[] {false} ;
      P007N30_A1171lccbT = new double[1] ;
      P007N30_n1171lccbT = new boolean[] {false} ;
      GXv_int13 = new byte [1] ;
      AV276PLP_C = 0 ;
      P007N31_A1150lccbE = new String[] {""} ;
      P007N31_A1222lccbI = new String[] {""} ;
      P007N31_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N31_A1224lccbC = new String[] {""} ;
      P007N31_A1225lccbC = new String[] {""} ;
      P007N31_A1226lccbA = new String[] {""} ;
      P007N31_A1227lccbO = new String[] {""} ;
      P007N31_A1228lccbF = new String[] {""} ;
      P007N31_A1232lccbT = new String[] {""} ;
      P007N31_A1231lccbT = new String[] {""} ;
      P007N31_A1212lccbF = new String[] {""} ;
      P007N31_n1212lccbF = new boolean[] {false} ;
      P007N31_A1211lccbF = new String[] {""} ;
      P007N31_n1211lccbF = new boolean[] {false} ;
      P007N31_A1210lccbF = new String[] {""} ;
      P007N31_n1210lccbF = new boolean[] {false} ;
      P007N31_A1213lccbI = new String[] {""} ;
      P007N31_n1213lccbI = new boolean[] {false} ;
      P007N31_A1214lccbI = new byte[1] ;
      P007N31_n1214lccbI = new boolean[] {false} ;
      P007N31_A1217lccbF = new String[] {""} ;
      P007N31_n1217lccbF = new boolean[] {false} ;
      P007N31_A1216lccbF = new String[] {""} ;
      P007N31_n1216lccbF = new boolean[] {false} ;
      P007N31_A1215lccbF = new String[] {""} ;
      P007N31_n1215lccbF = new boolean[] {false} ;
      P007N31_A1219lccbI = new byte[1] ;
      P007N31_n1219lccbI = new boolean[] {false} ;
      P007N31_A1221lccbF = new String[] {""} ;
      P007N31_n1221lccbF = new boolean[] {false} ;
      A1212lccbF = "" ;
      n1212lccbF = false ;
      A1211lccbF = "" ;
      n1211lccbF = false ;
      A1210lccbF = "" ;
      n1210lccbF = false ;
      A1213lccbI = "" ;
      n1213lccbI = false ;
      A1214lccbI = (byte)(0) ;
      n1214lccbI = false ;
      A1217lccbF = "" ;
      n1217lccbF = false ;
      A1216lccbF = "" ;
      n1216lccbF = false ;
      A1215lccbF = "" ;
      n1215lccbF = false ;
      A1219lccbI = (byte)(0) ;
      n1219lccbI = false ;
      A1221lccbF = "" ;
      n1221lccbF = false ;
      AV235FPAC = "" ;
      AV228PLP_o = (byte)(0) ;
      AV233PLP_p = "" ;
      AV229PLP_q = (short)(0) ;
      AV304lccbS = GXutil.nullDate() ;
      AV277lccbV = "" ;
      AV259lccbC = "" ;
      AV305lccbC = "" ;
      AV306LccbS = "" ;
      AV252lccbG = "" ;
      AV348Origi = "" ;
      AV339lccbO = 0 ;
      AV231PLP_V = 0 ;
      AV345InstA = 0 ;
      A1173lccbO = "" ;
      n1173lccbO = false ;
      A1174lccbO = (short)(0) ;
      AV335lccbO = (short)(0) ;
      n1174lccbO = false ;
      A1175lccbO = 0 ;
      AV336lccbO = 0 ;
      n1175lccbO = false ;
      AV307lccbF = "" ;
      AV308lccbF = "" ;
      AV309lccbF = "" ;
      AV310lccbI = "" ;
      AV311lccbI = (byte)(0) ;
      AV312lccbF = "" ;
      AV313lccbF = "" ;
      AV314lccbF = "" ;
      A1218lccbI = "" ;
      AV315lccbI = "" ;
      n1218lccbI = false ;
      AV316lccbI = (byte)(0) ;
      A1220lccbT = 0 ;
      AV317lccbT = 0 ;
      n1220lccbT = false ;
      AV318lccbF = "" ;
      AV319lccbP = "" ;
      AV320lccbR = "" ;
      P007N35_A1150lccbE = new String[] {""} ;
      P007N35_A1222lccbI = new String[] {""} ;
      P007N35_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N35_A1224lccbC = new String[] {""} ;
      P007N35_A1225lccbC = new String[] {""} ;
      P007N35_A1226lccbA = new String[] {""} ;
      P007N35_A1227lccbO = new String[] {""} ;
      P007N35_A1228lccbF = new String[] {""} ;
      P007N35_A1231lccbT = new String[] {""} ;
      P007N35_A1232lccbT = new String[] {""} ;
      P007N35_A1209lccbR = new String[] {""} ;
      P007N35_n1209lccbR = new boolean[] {false} ;
      P007N38_A1150lccbE = new String[] {""} ;
      P007N38_A1222lccbI = new String[] {""} ;
      P007N38_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N38_A1224lccbC = new String[] {""} ;
      P007N38_A1225lccbC = new String[] {""} ;
      P007N38_A1226lccbA = new String[] {""} ;
      P007N38_A1227lccbO = new String[] {""} ;
      P007N38_A1228lccbF = new String[] {""} ;
      P007N38_A1183lccbP = new short[1] ;
      P007N38_n1183lccbP = new boolean[] {false} ;
      P007N38_A1184lccbS = new String[] {""} ;
      P007N38_n1184lccbS = new boolean[] {false} ;
      P007N38_A1163lccbP = new String[] {""} ;
      P007N38_n1163lccbP = new boolean[] {false} ;
      P007N38_A1168lccbI = new short[1] ;
      P007N38_n1168lccbI = new boolean[] {false} ;
      P007N38_A1169lccbD = new double[1] ;
      P007N38_n1169lccbD = new boolean[] {false} ;
      P007N38_A1170lccbI = new double[1] ;
      P007N38_n1170lccbI = new boolean[] {false} ;
      P007N38_A1173lccbO = new String[] {""} ;
      P007N38_n1173lccbO = new boolean[] {false} ;
      P007N38_A1172lccbS = new double[1] ;
      P007N38_n1172lccbS = new boolean[] {false} ;
      P007N38_A1171lccbT = new double[1] ;
      P007N38_n1171lccbT = new boolean[] {false} ;
      P007N38_A1178lccbO = new double[1] ;
      P007N38_n1178lccbO = new boolean[] {false} ;
      P007N38_A1177lccbO = new double[1] ;
      P007N38_n1177lccbO = new boolean[] {false} ;
      P007N38_A1515lccbC = new double[1] ;
      P007N38_n1515lccbC = new boolean[] {false} ;
      P007N38_A1516lccbC = new double[1] ;
      P007N38_n1516lccbC = new boolean[] {false} ;
      P007N38_A1519lccbO = new double[1] ;
      P007N38_n1519lccbO = new boolean[] {false} ;
      P007N38_A1514lccbF = new double[1] ;
      P007N38_n1514lccbF = new boolean[] {false} ;
      P007N38_A1174lccbO = new short[1] ;
      P007N38_n1174lccbO = new boolean[] {false} ;
      P007N38_A1175lccbO = new double[1] ;
      P007N38_n1175lccbO = new boolean[] {false} ;
      P007N38_A1176lccbO = new double[1] ;
      P007N38_n1176lccbO = new boolean[] {false} ;
      AV346SaleA = 0 ;
      AV232PLP_p = "" ;
      P007N41_A1150lccbE = new String[] {""} ;
      P007N41_A1222lccbI = new String[] {""} ;
      P007N41_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N41_A1224lccbC = new String[] {""} ;
      P007N41_A1225lccbC = new String[] {""} ;
      P007N41_A1226lccbA = new String[] {""} ;
      P007N41_A1227lccbO = new String[] {""} ;
      P007N41_A1228lccbF = new String[] {""} ;
      P007N41_A1231lccbT = new String[] {""} ;
      P007N41_A1232lccbT = new String[] {""} ;
      P007N41_A1210lccbF = new String[] {""} ;
      P007N41_n1210lccbF = new boolean[] {false} ;
      P007N41_A1211lccbF = new String[] {""} ;
      P007N41_n1211lccbF = new boolean[] {false} ;
      P007N41_A1212lccbF = new String[] {""} ;
      P007N41_n1212lccbF = new boolean[] {false} ;
      P007N41_A1215lccbF = new String[] {""} ;
      P007N41_n1215lccbF = new boolean[] {false} ;
      P007N41_A1216lccbF = new String[] {""} ;
      P007N41_n1216lccbF = new boolean[] {false} ;
      P007N41_A1217lccbF = new String[] {""} ;
      P007N41_n1217lccbF = new boolean[] {false} ;
      P007N41_A1220lccbT = new double[1] ;
      P007N41_n1220lccbT = new boolean[] {false} ;
      P007N41_A1221lccbF = new String[] {""} ;
      P007N41_n1221lccbF = new boolean[] {false} ;
      P007N41_A1207lccbP = new String[] {""} ;
      P007N41_n1207lccbP = new boolean[] {false} ;
      P007N41_A1208lccbR = new String[] {""} ;
      P007N41_n1208lccbR = new boolean[] {false} ;
      P007N41_A1209lccbR = new String[] {""} ;
      P007N41_n1209lccbR = new boolean[] {false} ;
      P007N46_A1150lccbE = new String[] {""} ;
      P007N46_A1222lccbI = new String[] {""} ;
      P007N46_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N46_A1224lccbC = new String[] {""} ;
      P007N46_A1225lccbC = new String[] {""} ;
      P007N46_A1226lccbA = new String[] {""} ;
      P007N46_A1227lccbO = new String[] {""} ;
      P007N46_A1228lccbF = new String[] {""} ;
      P007N46_A1172lccbS = new double[1] ;
      P007N46_n1172lccbS = new boolean[] {false} ;
      P007N46_A1171lccbT = new double[1] ;
      P007N46_n1171lccbT = new boolean[] {false} ;
      P007N46_A1169lccbD = new double[1] ;
      P007N46_n1169lccbD = new boolean[] {false} ;
      P007N46_A1519lccbO = new double[1] ;
      P007N46_n1519lccbO = new boolean[] {false} ;
      P007N46_A1178lccbO = new double[1] ;
      P007N46_n1178lccbO = new boolean[] {false} ;
      P007N46_A1177lccbO = new double[1] ;
      P007N46_n1177lccbO = new boolean[] {false} ;
      P007N46_A1515lccbC = new double[1] ;
      P007N46_n1515lccbC = new boolean[] {false} ;
      P007N46_A1514lccbF = new double[1] ;
      P007N46_n1514lccbF = new boolean[] {false} ;
      P007N46_A1163lccbP = new String[] {""} ;
      P007N46_n1163lccbP = new boolean[] {false} ;
      P007N46_A1168lccbI = new short[1] ;
      P007N46_n1168lccbI = new boolean[] {false} ;
      P007N46_A1170lccbI = new double[1] ;
      P007N46_n1170lccbI = new boolean[] {false} ;
      P007N46_A1183lccbP = new short[1] ;
      P007N46_n1183lccbP = new boolean[] {false} ;
      P007N46_A1184lccbS = new String[] {""} ;
      P007N46_n1184lccbS = new boolean[] {false} ;
      AV255lccbD = 0 ;
      AV226c = "" ;
      AV234PLP_p = "" ;
      AV371lccbF = 0 ;
      AV349fare = 0 ;
      AV364TemCa = "" ;
      AV362i2 = 0 ;
      AV365CashS = 0 ;
      AV366Grupo = "" ;
      AV369Count = 0 ;
      AV375ZeraT = "" ;
      AV281Decis = "" ;
      AV351lccbP = 0 ;
      AV286ICSI_ = 0 ;
      AV288Inter = 0 ;
      AV368Total = 0 ;
      P007N48_A1232lccbT = new String[] {""} ;
      P007N48_A1228lccbF = new String[] {""} ;
      P007N48_A1227lccbO = new String[] {""} ;
      P007N48_A1226lccbA = new String[] {""} ;
      P007N48_A1225lccbC = new String[] {""} ;
      P007N48_A1224lccbC = new String[] {""} ;
      P007N48_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N48_A1150lccbE = new String[] {""} ;
      P007N48_A1222lccbI = new String[] {""} ;
      P007N48_A1231lccbT = new String[] {""} ;
      AV367i3 = 0 ;
      AV230PLP_F = (byte)(0) ;
      AV253lccbP = "" ;
      AV244lccbP = (byte)(0) ;
      AV245lccbP = (byte)(0) ;
      AV302sNum = "" ;
      AV40dt1 = GXutil.nullDate() ;
      GXt_dtime11 = GXutil.resetTime( GXutil.nullDate() );
      GXv_date12 = new java.util.Date [1] ;
      AV275lccbR = 0 ;
      AV287lccbP = 0 ;
      AV413GXLvl = (byte)(0) ;
      P007N49_A1155lccbP = new java.util.Date[] {GXutil.nullDate()} ;
      P007N49_n1155lccbP = new boolean[] {false} ;
      P007N49_A1164lccbP = new java.util.Date[] {GXutil.nullDate()} ;
      P007N49_A1163lccbP = new String[] {""} ;
      P007N49_n1163lccbP = new boolean[] {false} ;
      P007N49_A1150lccbE = new String[] {""} ;
      P007N49_A1156lccbP = new byte[1] ;
      P007N49_n1156lccbP = new boolean[] {false} ;
      P007N49_A1157lccbP = new byte[1] ;
      P007N49_n1157lccbP = new boolean[] {false} ;
      P007N49_A1162lccbR = new double[1] ;
      P007N49_n1162lccbR = new boolean[] {false} ;
      P007N49_A1160lccbP = new double[1] ;
      P007N49_n1160lccbP = new boolean[] {false} ;
      A1155lccbP = GXutil.nullDate() ;
      n1155lccbP = false ;
      A1164lccbP = GXutil.nullDate() ;
      A1156lccbP = (byte)(0) ;
      n1156lccbP = false ;
      A1157lccbP = (byte)(0) ;
      n1157lccbP = false ;
      A1162lccbR = 0 ;
      n1162lccbR = false ;
      A1160lccbP = 0 ;
      n1160lccbP = false ;
      P007N50_A1166lccbP = new String[] {""} ;
      P007N50_A1150lccbE = new String[] {""} ;
      P007N50_A1513lccbD = new String[] {""} ;
      A1513lccbD = "" ;
      P007N51_A1166lccbP = new String[] {""} ;
      P007N51_A1150lccbE = new String[] {""} ;
      P007N51_A1513lccbD = new String[] {""} ;
      P007N52_A1166lccbP = new String[] {""} ;
      P007N52_A1150lccbE = new String[] {""} ;
      P007N52_A1513lccbD = new String[] {""} ;
      P007N53_A1166lccbP = new String[] {""} ;
      P007N53_A1150lccbE = new String[] {""} ;
      P007N53_A1513lccbD = new String[] {""} ;
      AV242valor = 0 ;
      P007N54_A993HntUsu = new String[] {""} ;
      P007N54_n993HntUsu = new boolean[] {false} ;
      P007N54_A997HntSeq = new String[] {""} ;
      P007N54_A994HntLin = new String[] {""} ;
      P007N54_n994HntLin = new boolean[] {false} ;
      P007N54_A998HntSub = new byte[1] ;
      AV22AuxLin = "" ;
      P007N56_A993HntUsu = new String[] {""} ;
      P007N56_n993HntUsu = new boolean[] {false} ;
      P007N56_A997HntSeq = new String[] {""} ;
      P007N56_A994HntLin = new String[] {""} ;
      P007N56_n994HntLin = new boolean[] {false} ;
      P007N56_A998HntSub = new byte[1] ;
      P007N58_A993HntUsu = new String[] {""} ;
      P007N58_n993HntUsu = new boolean[] {false} ;
      P007N58_A997HntSeq = new String[] {""} ;
      P007N58_A994HntLin = new String[] {""} ;
      P007N58_n994HntLin = new boolean[] {false} ;
      P007N58_A998HntSub = new byte[1] ;
      P007N60_A993HntUsu = new String[] {""} ;
      P007N60_n993HntUsu = new boolean[] {false} ;
      P007N60_A997HntSeq = new String[] {""} ;
      P007N60_A994HntLin = new String[] {""} ;
      P007N60_n994HntLin = new boolean[] {false} ;
      P007N60_A998HntSub = new byte[1] ;
      AV223orgEm = "" ;
      AV186Found = new String [3] ;
      GX_I = 1 ;
      while ( ( GX_I <= 3 ) )
      {
         AV186Found[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV222OKBin = (byte)(0) ;
      AV260lBIN = (byte)(0) ;
      AV422GXLvl = (byte)(0) ;
      P007N62_A993HntUsu = new String[] {""} ;
      P007N62_n993HntUsu = new boolean[] {false} ;
      P007N62_A997HntSeq = new String[] {""} ;
      P007N62_A994HntLin = new String[] {""} ;
      P007N62_n994HntLin = new boolean[] {false} ;
      P007N62_A998HntSub = new byte[1] ;
      GXv_int14 = new short [1] ;
      GXv_char3 = new String [1] ;
      GXv_char2 = new String [1] ;
      P007N64_A1208lccbR = new String[] {""} ;
      P007N64_n1208lccbR = new boolean[] {false} ;
      P007N64_A1150lccbE = new String[] {""} ;
      P007N64_A1222lccbI = new String[] {""} ;
      P007N64_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N64_A1224lccbC = new String[] {""} ;
      P007N64_A1225lccbC = new String[] {""} ;
      P007N64_A1226lccbA = new String[] {""} ;
      P007N64_A1227lccbO = new String[] {""} ;
      P007N64_A1228lccbF = new String[] {""} ;
      P007N64_A1231lccbT = new String[] {""} ;
      P007N64_A1232lccbT = new String[] {""} ;
      AV258lccbT = "" ;
      AV265DelFl = (byte)(0) ;
      AV268Cnt_d = 0 ;
      AV269Cnt_d = 0 ;
      GXt_char10 = "" ;
      GXt_char9 = "" ;
      P007N66_A1228lccbF = new String[] {""} ;
      P007N66_A1227lccbO = new String[] {""} ;
      P007N66_A1226lccbA = new String[] {""} ;
      P007N66_A1225lccbC = new String[] {""} ;
      P007N66_A1224lccbC = new String[] {""} ;
      P007N66_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N66_A1222lccbI = new String[] {""} ;
      P007N66_A1150lccbE = new String[] {""} ;
      P007N66_A1172lccbS = new double[1] ;
      P007N66_n1172lccbS = new boolean[] {false} ;
      P007N66_A1184lccbS = new String[] {""} ;
      P007N66_n1184lccbS = new boolean[] {false} ;
      AV425GXLvl = (byte)(0) ;
      P007N68_A1228lccbF = new String[] {""} ;
      P007N68_A1227lccbO = new String[] {""} ;
      P007N68_A1226lccbA = new String[] {""} ;
      P007N68_A1225lccbC = new String[] {""} ;
      P007N68_A1224lccbC = new String[] {""} ;
      P007N68_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N68_A1222lccbI = new String[] {""} ;
      P007N68_A1150lccbE = new String[] {""} ;
      P007N68_A1167lccbG = new String[] {""} ;
      P007N68_n1167lccbG = new boolean[] {false} ;
      P007N68_A1172lccbS = new double[1] ;
      P007N68_n1172lccbS = new boolean[] {false} ;
      P007N68_A1519lccbO = new double[1] ;
      P007N68_n1519lccbO = new boolean[] {false} ;
      P007N68_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P007N68_n1189lccbS = new boolean[] {false} ;
      P007N68_A1179lccbV = new String[] {""} ;
      P007N68_n1179lccbV = new boolean[] {false} ;
      P007N68_A1180lccbC = new String[] {""} ;
      P007N68_n1180lccbC = new boolean[] {false} ;
      P007N68_A1181lccbC = new String[] {""} ;
      P007N68_n1181lccbC = new boolean[] {false} ;
      P007N68_A1182lccbS = new String[] {""} ;
      P007N68_n1182lccbS = new boolean[] {false} ;
      P007N69_A1150lccbE = new String[] {""} ;
      P007N69_A1222lccbI = new String[] {""} ;
      P007N69_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N69_A1224lccbC = new String[] {""} ;
      P007N69_A1225lccbC = new String[] {""} ;
      P007N69_A1226lccbA = new String[] {""} ;
      P007N69_A1227lccbO = new String[] {""} ;
      P007N69_A1228lccbF = new String[] {""} ;
      P007N69_A1231lccbT = new String[] {""} ;
      P007N69_A1232lccbT = new String[] {""} ;
      P007N69_A1210lccbF = new String[] {""} ;
      P007N69_n1210lccbF = new boolean[] {false} ;
      P007N69_A1211lccbF = new String[] {""} ;
      P007N69_n1211lccbF = new boolean[] {false} ;
      P007N69_A1212lccbF = new String[] {""} ;
      P007N69_n1212lccbF = new boolean[] {false} ;
      P007N69_A1213lccbI = new String[] {""} ;
      P007N69_n1213lccbI = new boolean[] {false} ;
      P007N69_A1214lccbI = new byte[1] ;
      P007N69_n1214lccbI = new boolean[] {false} ;
      P007N69_A1215lccbF = new String[] {""} ;
      P007N69_n1215lccbF = new boolean[] {false} ;
      P007N69_A1216lccbF = new String[] {""} ;
      P007N69_n1216lccbF = new boolean[] {false} ;
      P007N69_A1217lccbF = new String[] {""} ;
      P007N69_n1217lccbF = new boolean[] {false} ;
      P007N69_A1218lccbI = new String[] {""} ;
      P007N69_n1218lccbI = new boolean[] {false} ;
      P007N69_A1219lccbI = new byte[1] ;
      P007N69_n1219lccbI = new boolean[] {false} ;
      P007N69_A1220lccbT = new double[1] ;
      P007N69_n1220lccbT = new boolean[] {false} ;
      P007N69_A1221lccbF = new String[] {""} ;
      P007N69_n1221lccbF = new boolean[] {false} ;
      P007N69_A1207lccbP = new String[] {""} ;
      P007N69_n1207lccbP = new boolean[] {false} ;
      P007N69_A1208lccbR = new String[] {""} ;
      P007N69_n1208lccbR = new boolean[] {false} ;
      AV427GXLvl = (byte)(0) ;
      P007N70_A1227lccbO = new String[] {""} ;
      P007N70_A1226lccbA = new String[] {""} ;
      P007N70_A1225lccbC = new String[] {""} ;
      P007N70_A1224lccbC = new String[] {""} ;
      P007N70_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N70_A1150lccbE = new String[] {""} ;
      P007N70_A1222lccbI = new String[] {""} ;
      P007N70_A1167lccbG = new String[] {""} ;
      P007N70_n1167lccbG = new boolean[] {false} ;
      P007N70_A1228lccbF = new String[] {""} ;
      AV297FPACo = "" ;
      GXv_char15 = new String [1] ;
      AV428GXLvl = (byte)(0) ;
      P007N71_A1231lccbT = new String[] {""} ;
      P007N71_A1150lccbE = new String[] {""} ;
      P007N71_A1222lccbI = new String[] {""} ;
      P007N71_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007N71_A1224lccbC = new String[] {""} ;
      P007N71_A1225lccbC = new String[] {""} ;
      P007N71_A1226lccbA = new String[] {""} ;
      P007N71_A1227lccbO = new String[] {""} ;
      P007N71_A1228lccbF = new String[] {""} ;
      P007N71_A1232lccbT = new String[] {""} ;
      AV384p = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new areticsi_bkp__default(),
         new Object[] {
             new Object[] {
            P007N2_A1228lccbF, P007N2_A1227lccbO, P007N2_A1226lccbA, P007N2_A1225lccbC, P007N2_A1224lccbC, P007N2_A1223lccbD, P007N2_A1222lccbI, P007N2_A1150lccbE, P007N2_A1184lccbS, P007N2_n1184lccbS,
            P007N2_A1167lccbG, P007N2_n1167lccbG
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N6_A994HntLin, P007N6_n994HntLin, P007N6_A993HntUsu, P007N6_n993HntUsu, P007N6_A997HntSeq, P007N6_A998HntSub
            }
            , new Object[] {
            P007N7_A1147lccbE, P007N7_n1147lccbE, P007N7_A1150lccbE
            }
            , new Object[] {
            P007N8_A1166lccbP, P007N8_A1150lccbE, P007N8_A1165lccbD, P007N8_n1165lccbD
            }
            , new Object[] {
            P007N9_A1166lccbP, P007N9_A1150lccbE, P007N9_A1165lccbD, P007N9_n1165lccbD
            }
            , new Object[] {
            P007N10_A1166lccbP, P007N10_A1150lccbE, P007N10_A1165lccbD, P007N10_n1165lccbD
            }
            , new Object[] {
            P007N11_A1166lccbP, P007N11_A1150lccbE, P007N11_A1165lccbD, P007N11_n1165lccbD
            }
            , new Object[] {
            P007N12_A1148lccbF, P007N12_n1148lccbF, P007N12_A1152lccbF, P007N12_A1151lccbF, P007N12_A1150lccbE
            }
            , new Object[] {
            P007N13_A1294Serie, P007N13_n1294Serie, P007N13_A1296Serie, P007N13_A1295DocCo
            }
            , new Object[] {
            P007N14_A1140lccbB, P007N14_n1140lccbB, P007N14_A1145lccbB, P007N14_A1141lccbB, P007N14_n1141lccbB, P007N14_A1142lccbB, P007N14_n1142lccbB, P007N14_A1144lccbE, P007N14_A1143lccbB
            }
            , new Object[] {
            P007N15_A1149lccbD, P007N15_n1149lccbD, P007N15_A1154lccbD, P007N15_A1153lccbD, P007N15_A1150lccbE
            }
            , new Object[] {
            }
            , new Object[] {
            P007N17_A1312SCEId
            }
            , new Object[] {
            P007N18_A1309SCEFo, P007N18_n1309SCEFo, P007N18_A1299SCEDa, P007N18_n1299SCEDa, P007N18_A1298SCETk, P007N18_n1298SCETk, P007N18_A1312SCEId
            }
            , new Object[] {
            }
            , new Object[] {
            P007N20_A1312SCEId
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N23_A1150lccbE, P007N23_A1222lccbI, P007N23_A1223lccbD, P007N23_A1224lccbC, P007N23_A1225lccbC, P007N23_A1226lccbA, P007N23_A1227lccbO, P007N23_A1228lccbF, P007N23_A1231lccbT, P007N23_A1232lccbT,
            P007N23_A1207lccbP, P007N23_n1207lccbP, P007N23_A1208lccbR, P007N23_n1208lccbR, P007N23_A1209lccbR, P007N23_n1209lccbR
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N28_A1150lccbE, P007N28_A1222lccbI, P007N28_A1223lccbD, P007N28_A1224lccbC, P007N28_A1225lccbC, P007N28_A1226lccbA, P007N28_A1227lccbO, P007N28_A1228lccbF, P007N28_A1183lccbP, P007N28_n1183lccbP,
            P007N28_A1184lccbS, P007N28_n1184lccbS, P007N28_A1163lccbP, P007N28_n1163lccbP, P007N28_A1168lccbI, P007N28_n1168lccbI, P007N28_A1169lccbD, P007N28_n1169lccbD, P007N28_A1170lccbI, P007N28_n1170lccbI,
            P007N28_A1171lccbT, P007N28_n1171lccbT, P007N28_A1178lccbO, P007N28_n1178lccbO, P007N28_A1177lccbO, P007N28_n1177lccbO, P007N28_A1172lccbS, P007N28_n1172lccbS, P007N28_A1515lccbC, P007N28_n1515lccbC,
            P007N28_A1519lccbO, P007N28_n1519lccbO, P007N28_A1514lccbF, P007N28_n1514lccbF
            }
            , new Object[] {
            }
            , new Object[] {
            P007N30_A1228lccbF, P007N30_A1227lccbO, P007N30_A1226lccbA, P007N30_A1225lccbC, P007N30_A1224lccbC, P007N30_A1223lccbD, P007N30_A1222lccbI, P007N30_A1150lccbE, P007N30_A1184lccbS, P007N30_n1184lccbS,
            P007N30_A1167lccbG, P007N30_n1167lccbG, P007N30_A1172lccbS, P007N30_n1172lccbS, P007N30_A1169lccbD, P007N30_n1169lccbD, P007N30_A1171lccbT, P007N30_n1171lccbT
            }
            , new Object[] {
            P007N31_A1150lccbE, P007N31_A1222lccbI, P007N31_A1223lccbD, P007N31_A1224lccbC, P007N31_A1225lccbC, P007N31_A1226lccbA, P007N31_A1227lccbO, P007N31_A1228lccbF, P007N31_A1232lccbT, P007N31_A1231lccbT,
            P007N31_A1212lccbF, P007N31_n1212lccbF, P007N31_A1211lccbF, P007N31_n1211lccbF, P007N31_A1210lccbF, P007N31_n1210lccbF, P007N31_A1213lccbI, P007N31_n1213lccbI, P007N31_A1214lccbI, P007N31_n1214lccbI,
            P007N31_A1217lccbF, P007N31_n1217lccbF, P007N31_A1216lccbF, P007N31_n1216lccbF, P007N31_A1215lccbF, P007N31_n1215lccbF, P007N31_A1219lccbI, P007N31_n1219lccbI, P007N31_A1221lccbF, P007N31_n1221lccbF
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N35_A1150lccbE, P007N35_A1222lccbI, P007N35_A1223lccbD, P007N35_A1224lccbC, P007N35_A1225lccbC, P007N35_A1226lccbA, P007N35_A1227lccbO, P007N35_A1228lccbF, P007N35_A1231lccbT, P007N35_A1232lccbT,
            P007N35_A1209lccbR, P007N35_n1209lccbR
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N38_A1150lccbE, P007N38_A1222lccbI, P007N38_A1223lccbD, P007N38_A1224lccbC, P007N38_A1225lccbC, P007N38_A1226lccbA, P007N38_A1227lccbO, P007N38_A1228lccbF, P007N38_A1183lccbP, P007N38_n1183lccbP,
            P007N38_A1184lccbS, P007N38_n1184lccbS, P007N38_A1163lccbP, P007N38_n1163lccbP, P007N38_A1168lccbI, P007N38_n1168lccbI, P007N38_A1169lccbD, P007N38_n1169lccbD, P007N38_A1170lccbI, P007N38_n1170lccbI,
            P007N38_A1173lccbO, P007N38_n1173lccbO, P007N38_A1172lccbS, P007N38_n1172lccbS, P007N38_A1171lccbT, P007N38_n1171lccbT, P007N38_A1178lccbO, P007N38_n1178lccbO, P007N38_A1177lccbO, P007N38_n1177lccbO,
            P007N38_A1515lccbC, P007N38_n1515lccbC, P007N38_A1516lccbC, P007N38_n1516lccbC, P007N38_A1519lccbO, P007N38_n1519lccbO, P007N38_A1514lccbF, P007N38_n1514lccbF, P007N38_A1174lccbO, P007N38_n1174lccbO,
            P007N38_A1175lccbO, P007N38_n1175lccbO, P007N38_A1176lccbO, P007N38_n1176lccbO
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N41_A1150lccbE, P007N41_A1222lccbI, P007N41_A1223lccbD, P007N41_A1224lccbC, P007N41_A1225lccbC, P007N41_A1226lccbA, P007N41_A1227lccbO, P007N41_A1228lccbF, P007N41_A1231lccbT, P007N41_A1232lccbT,
            P007N41_A1210lccbF, P007N41_n1210lccbF, P007N41_A1211lccbF, P007N41_n1211lccbF, P007N41_A1212lccbF, P007N41_n1212lccbF, P007N41_A1215lccbF, P007N41_n1215lccbF, P007N41_A1216lccbF, P007N41_n1216lccbF,
            P007N41_A1217lccbF, P007N41_n1217lccbF, P007N41_A1220lccbT, P007N41_n1220lccbT, P007N41_A1221lccbF, P007N41_n1221lccbF, P007N41_A1207lccbP, P007N41_n1207lccbP, P007N41_A1208lccbR, P007N41_n1208lccbR,
            P007N41_A1209lccbR, P007N41_n1209lccbR
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007N46_A1150lccbE, P007N46_A1222lccbI, P007N46_A1223lccbD, P007N46_A1224lccbC, P007N46_A1225lccbC, P007N46_A1226lccbA, P007N46_A1227lccbO, P007N46_A1228lccbF, P007N46_A1172lccbS, P007N46_n1172lccbS,
            P007N46_A1171lccbT, P007N46_n1171lccbT, P007N46_A1169lccbD, P007N46_n1169lccbD, P007N46_A1519lccbO, P007N46_n1519lccbO, P007N46_A1178lccbO, P007N46_n1178lccbO, P007N46_A1177lccbO, P007N46_n1177lccbO,
            P007N46_A1515lccbC, P007N46_n1515lccbC, P007N46_A1514lccbF, P007N46_n1514lccbF, P007N46_A1163lccbP, P007N46_n1163lccbP, P007N46_A1168lccbI, P007N46_n1168lccbI, P007N46_A1170lccbI, P007N46_n1170lccbI,
            P007N46_A1183lccbP, P007N46_n1183lccbP, P007N46_A1184lccbS, P007N46_n1184lccbS
            }
            , new Object[] {
            }
            , new Object[] {
            P007N48_A1232lccbT, P007N48_A1228lccbF, P007N48_A1227lccbO, P007N48_A1226lccbA, P007N48_A1225lccbC, P007N48_A1224lccbC, P007N48_A1223lccbD, P007N48_A1150lccbE, P007N48_A1222lccbI, P007N48_A1231lccbT
            }
            , new Object[] {
            P007N49_A1155lccbP, P007N49_n1155lccbP, P007N49_A1164lccbP, P007N49_A1163lccbP, P007N49_A1150lccbE, P007N49_A1156lccbP, P007N49_n1156lccbP, P007N49_A1157lccbP, P007N49_n1157lccbP, P007N49_A1162lccbR,
            P007N49_n1162lccbR, P007N49_A1160lccbP, P007N49_n1160lccbP
            }
            , new Object[] {
            P007N50_A1166lccbP, P007N50_A1150lccbE, P007N50_A1513lccbD
            }
            , new Object[] {
            P007N51_A1166lccbP, P007N51_A1150lccbE, P007N51_A1513lccbD
            }
            , new Object[] {
            P007N52_A1166lccbP, P007N52_A1150lccbE, P007N52_A1513lccbD
            }
            , new Object[] {
            P007N53_A1166lccbP, P007N53_A1150lccbE, P007N53_A1513lccbD
            }
            , new Object[] {
            P007N54_A993HntUsu, P007N54_n993HntUsu, P007N54_A997HntSeq, P007N54_A994HntLin, P007N54_n994HntLin, P007N54_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P007N56_A993HntUsu, P007N56_n993HntUsu, P007N56_A997HntSeq, P007N56_A994HntLin, P007N56_n994HntLin, P007N56_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P007N58_A993HntUsu, P007N58_n993HntUsu, P007N58_A997HntSeq, P007N58_A994HntLin, P007N58_n994HntLin, P007N58_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P007N60_A993HntUsu, P007N60_n993HntUsu, P007N60_A997HntSeq, P007N60_A994HntLin, P007N60_n994HntLin, P007N60_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P007N62_A993HntUsu, P007N62_n993HntUsu, P007N62_A997HntSeq, P007N62_A994HntLin, P007N62_n994HntLin, P007N62_A998HntSub
            }
            , new Object[] {
            }
            , new Object[] {
            P007N64_A1208lccbR, P007N64_n1208lccbR, P007N64_A1150lccbE, P007N64_A1222lccbI, P007N64_A1223lccbD, P007N64_A1224lccbC, P007N64_A1225lccbC, P007N64_A1226lccbA, P007N64_A1227lccbO, P007N64_A1228lccbF,
            P007N64_A1231lccbT, P007N64_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            P007N66_A1228lccbF, P007N66_A1227lccbO, P007N66_A1226lccbA, P007N66_A1225lccbC, P007N66_A1224lccbC, P007N66_A1223lccbD, P007N66_A1222lccbI, P007N66_A1150lccbE, P007N66_A1172lccbS, P007N66_n1172lccbS,
            P007N66_A1184lccbS, P007N66_n1184lccbS
            }
            , new Object[] {
            }
            , new Object[] {
            P007N68_A1228lccbF, P007N68_A1227lccbO, P007N68_A1226lccbA, P007N68_A1225lccbC, P007N68_A1224lccbC, P007N68_A1223lccbD, P007N68_A1222lccbI, P007N68_A1150lccbE, P007N68_A1167lccbG, P007N68_n1167lccbG,
            P007N68_A1172lccbS, P007N68_n1172lccbS, P007N68_A1519lccbO, P007N68_n1519lccbO, P007N68_A1189lccbS, P007N68_n1189lccbS, P007N68_A1179lccbV, P007N68_n1179lccbV, P007N68_A1180lccbC, P007N68_n1180lccbC,
            P007N68_A1181lccbC, P007N68_n1181lccbC, P007N68_A1182lccbS, P007N68_n1182lccbS
            }
            , new Object[] {
            P007N69_A1150lccbE, P007N69_A1222lccbI, P007N69_A1223lccbD, P007N69_A1224lccbC, P007N69_A1225lccbC, P007N69_A1226lccbA, P007N69_A1227lccbO, P007N69_A1228lccbF, P007N69_A1231lccbT, P007N69_A1232lccbT,
            P007N69_A1210lccbF, P007N69_n1210lccbF, P007N69_A1211lccbF, P007N69_n1211lccbF, P007N69_A1212lccbF, P007N69_n1212lccbF, P007N69_A1213lccbI, P007N69_n1213lccbI, P007N69_A1214lccbI, P007N69_n1214lccbI,
            P007N69_A1215lccbF, P007N69_n1215lccbF, P007N69_A1216lccbF, P007N69_n1216lccbF, P007N69_A1217lccbF, P007N69_n1217lccbF, P007N69_A1218lccbI, P007N69_n1218lccbI, P007N69_A1219lccbI, P007N69_n1219lccbI,
            P007N69_A1220lccbT, P007N69_n1220lccbT, P007N69_A1221lccbF, P007N69_n1221lccbF, P007N69_A1207lccbP, P007N69_n1207lccbP, P007N69_A1208lccbR, P007N69_n1208lccbR
            }
            , new Object[] {
            P007N70_A1227lccbO, P007N70_A1226lccbA, P007N70_A1225lccbC, P007N70_A1224lccbC, P007N70_A1223lccbD, P007N70_A1150lccbE, P007N70_A1222lccbI, P007N70_A1167lccbG, P007N70_n1167lccbG, P007N70_A1228lccbF
            }
            , new Object[] {
            P007N71_A1231lccbT, P007N71_A1150lccbE, P007N71_A1222lccbI, P007N71_A1223lccbD, P007N71_A1224lccbC, P007N71_A1225lccbC, P007N71_A1226lccbA, P007N71_A1227lccbO, P007N71_A1228lccbF, P007N71_A1232lccbT
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV274flagR ;
   private byte AV282flagR ;
   private byte AV298FlagK ;
   private byte A998HntSub ;
   private byte AV273Count ;
   private byte AV54Flag_I ;
   private byte AV221Flag_ ;
   private byte AV9Count_I ;
   private byte AV391GXLvl ;
   private byte AV392GXLvl ;
   private byte AV393GXLvl ;
   private byte AV394GXLvl ;
   private byte AV395GXLvl ;
   private byte A1145lccbB ;
   private byte AV206retva ;
   private byte AV243FlagI ;
   private byte AV295FlagA ;
   private byte AV280flagR ;
   private byte GXv_int13[] ;
   private byte A1214lccbI ;
   private byte A1219lccbI ;
   private byte AV228PLP_o ;
   private byte AV311lccbI ;
   private byte AV316lccbI ;
   private byte AV230PLP_F ;
   private byte AV244lccbP ;
   private byte AV245lccbP ;
   private byte AV413GXLvl ;
   private byte A1156lccbP ;
   private byte A1157lccbP ;
   private byte AV222OKBin ;
   private byte AV260lBIN ;
   private byte AV422GXLvl ;
   private byte AV265DelFl ;
   private byte AV425GXLvl ;
   private byte AV427GXLvl ;
   private byte AV428GXLvl ;
   private short AV88j ;
   private short Gx_err ;
   private short AV261lccbP ;
   private short AV279lccbS ;
   private short A1230lccbS ;
   private short A1168lccbI ;
   private short A1183lccbP ;
   private short AV229PLP_q ;
   private short A1174lccbO ;
   private short AV335lccbO ;
   private short GXv_int14[] ;
   private int AV361Count ;
   private int AV381Index ;
   private int AV267TRN_C ;
   private int AV327RetVa ;
   private int AV78i ;
   private int AV204qtdEm ;
   private int AV182qtdBi ;
   private int AV180qtdFo ;
   private int AV203Count ;
   private int AV183TKT_C[] ;
   private int AV184RFN_C[] ;
   private int AV185MCO_C[] ;
   private int GX_I ;
   private int GX_J ;
   private int AV225qtdFo ;
   private int AV188qtdDo ;
   private int GX_INS248 ;
   private int AV270ifop ;
   private int AV218fop2I ;
   private int GX_INS236 ;
   private int GX_INS235 ;
   private int GX_INS237 ;
   private int AV362i2 ;
   private int AV365CashS ;
   private int AV268Cnt_d ;
   private int AV269Cnt_d ;
   private int AV384p ;
   private long A1294Serie ;
   private long A1296Serie ;
   private long A1312SCEId ;
   private long AV369Count ;
   private long AV367i3 ;
   private double AV373SCEFP ;
   private double AV374SCEPa ;
   private double AV289Total ;
   private double AV236PLP_V ;
   private double AV220Total ;
   private double AV214valor ;
   private double AV379Total ;
   private double AV377lccbF ;
   private double A1517SCEFP ;
   private double A1518SCEPa ;
   private double AV372lccbC ;
   private double AV358Total ;
   private double AV264lccbS ;
   private double AV376lccbO ;
   private double AV338lccbO ;
   private double AV321SaleA ;
   private double AV370lccbC ;
   private double AV257lccbT ;
   private double A1169lccbD ;
   private double A1170lccbI ;
   private double A1172lccbS ;
   private double A1171lccbT ;
   private double A1178lccbO ;
   private double A1176lccbO ;
   private double AV337lccbO ;
   private double A1177lccbO ;
   private double A1515lccbC ;
   private double A1514lccbF ;
   private double A1516lccbC ;
   private double A1519lccbO ;
   private double AV276PLP_C ;
   private double AV339lccbO ;
   private double AV231PLP_V ;
   private double AV345InstA ;
   private double A1175lccbO ;
   private double AV336lccbO ;
   private double A1220lccbT ;
   private double AV317lccbT ;
   private double AV346SaleA ;
   private double AV255lccbD ;
   private double AV371lccbF ;
   private double AV349fare ;
   private double AV351lccbP ;
   private double AV286ICSI_ ;
   private double AV288Inter ;
   private double AV368Total ;
   private double AV275lccbR ;
   private double AV287lccbP ;
   private double A1162lccbR ;
   private double A1160lccbP ;
   private double AV242valor ;
   private String AV176FileS ;
   private String AV39DebugM ;
   private String AV178Filen ;
   private String AV326LogFi ;
   private String AV153Versa ;
   private String AV324LogLi ;
   private String scmdbuf ;
   private String A1228lccbF ;
   private String A1227lccbO ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1150lccbE ;
   private String A1184lccbS ;
   private String A1167lccbG ;
   private String AV169TRNN_ ;
   private String A997HntSeq ;
   private String AV168TRNN_ ;
   private String AV127RCID ;
   private String AV35CRS ;
   private String AV148SPED ;
   private String AV272CJCP ;
   private String AV271TDNR_[] ;
   private String AV167TRNN ;
   private String AV8AGTN ;
   private String AV158TDNR ;
   private String AV152TACN ;
   private String AV37DAIS ;
   private String AV246PXNM ;
   private String AV165TRNC ;
   private String AV205ListE[][] ;
   private String AV239PLP_D ;
   private String AV240PLP_D ;
   private String AV241PLP_D ;
   private String AV284PLP_D ;
   private String AV216CUTP ;
   private String AV142s ;
   private String AV219TMFT ;
   private String AV215sAMOU ;
   private String AV290FPTP ;
   private String GXt_char4 ;
   private String GXt_char1 ;
   private String GXt_char5 ;
   private String GXt_char6 ;
   private String GXt_char7 ;
   private String GXt_char8 ;
   private String A1305SCETe ;
   private String A1147lccbE ;
   private String AV238lccbE ;
   private String A1166lccbP ;
   private String A1165lccbD ;
   private String AV145SCETp ;
   private String AV342SCEFo ;
   private String AV144SCETe ;
   private String AV179ListF[][] ;
   private String A1148lccbF ;
   private String A1152lccbF ;
   private String A1151lccbF ;
   private String AV224ListF[][] ;
   private String A1295DocCo ;
   private String AV181ListB[][] ;
   private String A1140lccbB ;
   private String A1141lccbB ;
   private String A1142lccbB ;
   private String A1144lccbE ;
   private String A1143lccbB ;
   private String AV187ListD[][] ;
   private String A1149lccbD ;
   private String A1154lccbD ;
   private String A1153lccbD ;
   private String A1298SCETk ;
   private String A1300SCECR ;
   private String A1301SCEAi ;
   private String A1303SCERE ;
   private String A1307SCEIa ;
   private String A1310SCETp ;
   private String A1306SCEAP ;
   private String A1311SCEVe ;
   private String Gx_emsg ;
   private String AV354Exist ;
   private String A1309SCEFo ;
   private String AV200EmpCC ;
   private String AV383Bilhe ;
   private String AV211old_F ;
   private String AV212this_ ;
   private String AV213next_ ;
   private String AV285lccbF ;
   private String A1186lccbS ;
   private String AV278lccbS ;
   private String A1188lccbS ;
   private String AV296lccbS ;
   private String AV323FileD ;
   private String AV262lccbS ;
   private String AV328LccbC ;
   private String AV329Atrib ;
   private String AV330Resul ;
   private String A1163lccbP ;
   private String A1179lccbV ;
   private String A1180lccbC ;
   private String A1181lccbC ;
   private String A1182lccbS ;
   private String A1490Distr ;
   private String A1192lccbS ;
   private String AV247lccbi ;
   private String AV249lccbC ;
   private String AV250lccbC ;
   private String AV251lccbA ;
   private String AV266lccbO ;
   private String A1232lccbT ;
   private String A1231lccbT ;
   private String A1207lccbP ;
   private String A1208lccbR ;
   private String A1212lccbF ;
   private String A1211lccbF ;
   private String A1210lccbF ;
   private String A1213lccbI ;
   private String A1217lccbF ;
   private String A1216lccbF ;
   private String A1215lccbF ;
   private String A1221lccbF ;
   private String AV235FPAC ;
   private String AV233PLP_p ;
   private String AV277lccbV ;
   private String AV259lccbC ;
   private String AV305lccbC ;
   private String AV306LccbS ;
   private String AV252lccbG ;
   private String AV348Origi ;
   private String A1173lccbO ;
   private String AV307lccbF ;
   private String AV308lccbF ;
   private String AV309lccbF ;
   private String AV310lccbI ;
   private String AV312lccbF ;
   private String AV313lccbF ;
   private String AV314lccbF ;
   private String A1218lccbI ;
   private String AV315lccbI ;
   private String AV318lccbF ;
   private String AV319lccbP ;
   private String AV320lccbR ;
   private String AV232PLP_p ;
   private String AV226c ;
   private String AV234PLP_p ;
   private String AV364TemCa ;
   private String AV366Grupo ;
   private String AV375ZeraT ;
   private String AV281Decis ;
   private String AV253lccbP ;
   private String AV302sNum ;
   private String AV223orgEm ;
   private String AV186Found[] ;
   private String GXv_char3[] ;
   private String GXv_char2[] ;
   private String AV258lccbT ;
   private String GXt_char10 ;
   private String GXt_char9 ;
   private String AV297FPACo ;
   private String GXv_char15[] ;
   private java.util.Date AV106now ;
   private java.util.Date A1299SCEDa ;
   private java.util.Date AV355SCEDa ;
   private java.util.Date A1229lccbS ;
   private java.util.Date GXt_dtime11 ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1189lccbS ;
   private java.util.Date Gx_date ;
   private java.util.Date A1495lccbC ;
   private java.util.Date AV248lccbD ;
   private java.util.Date AV304lccbS ;
   private java.util.Date AV40dt1 ;
   private java.util.Date GXv_date12[] ;
   private java.util.Date A1155lccbP ;
   private java.util.Date A1164lccbP ;
   private boolean returnInSub ;
   private boolean n1184lccbS ;
   private boolean n1167lccbG ;
   private boolean n994HntLin ;
   private boolean n993HntUsu ;
   private boolean n1147lccbE ;
   private boolean n1165lccbD ;
   private boolean n1148lccbF ;
   private boolean n1294Serie ;
   private boolean n1140lccbB ;
   private boolean n1141lccbB ;
   private boolean n1142lccbB ;
   private boolean n1149lccbD ;
   private boolean n1298SCETk ;
   private boolean n1299SCEDa ;
   private boolean n1300SCECR ;
   private boolean n1301SCEAi ;
   private boolean n1303SCERE ;
   private boolean n1305SCETe ;
   private boolean n1307SCEIa ;
   private boolean n1310SCETp ;
   private boolean n1306SCEAP ;
   private boolean n1311SCEVe ;
   private boolean n1517SCEFP ;
   private boolean n1518SCEPa ;
   private boolean n1312SCEId ;
   private boolean n1309SCEFo ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n1188lccbS ;
   private boolean n1163lccbP ;
   private boolean n1168lccbI ;
   private boolean n1169lccbD ;
   private boolean n1170lccbI ;
   private boolean n1172lccbS ;
   private boolean n1179lccbV ;
   private boolean n1180lccbC ;
   private boolean n1181lccbC ;
   private boolean n1182lccbS ;
   private boolean n1171lccbT ;
   private boolean n1183lccbP ;
   private boolean n1189lccbS ;
   private boolean n1495lccbC ;
   private boolean n1490Distr ;
   private boolean n1192lccbS ;
   private boolean n1178lccbO ;
   private boolean n1176lccbO ;
   private boolean n1177lccbO ;
   private boolean n1515lccbC ;
   private boolean n1514lccbF ;
   private boolean n1516lccbC ;
   private boolean n1519lccbO ;
   private boolean n1207lccbP ;
   private boolean n1208lccbR ;
   private boolean n1209lccbR ;
   private boolean n1212lccbF ;
   private boolean n1211lccbF ;
   private boolean n1210lccbF ;
   private boolean n1213lccbI ;
   private boolean n1214lccbI ;
   private boolean n1217lccbF ;
   private boolean n1216lccbF ;
   private boolean n1215lccbF ;
   private boolean n1219lccbI ;
   private boolean n1221lccbF ;
   private boolean n1173lccbO ;
   private boolean n1174lccbO ;
   private boolean n1175lccbO ;
   private boolean n1218lccbI ;
   private boolean n1220lccbT ;
   private boolean n1155lccbP ;
   private boolean n1156lccbP ;
   private boolean n1157lccbP ;
   private boolean n1162lccbR ;
   private boolean n1160lccbP ;
   private String A994HntLin ;
   private String AV12Linha ;
   private String AV22AuxLin ;
   private String AV332Difer ;
   private String A993HntUsu ;
   private String AV207LccbR ;
   private String AV357Linha ;
   private String AV283ICSII ;
   private String AV360FareC[] ;
   private String AV382Bilhe[] ;
   private String AV263lccbS ;
   private String A1187lccbS ;
   private String A1209lccbR ;
   private String A1513lccbD ;
   private GxObjectCollection AV209retfo ;
   private Sdtsdt_RETFOP_sdt_RETFOPItem AV210retfo ;
   private String[] aP0 ;
   private String[] aP1 ;
   private IDataStoreProvider pr_default ;
   private String[] P007N2_A1228lccbF ;
   private String[] P007N2_A1227lccbO ;
   private String[] P007N2_A1226lccbA ;
   private String[] P007N2_A1225lccbC ;
   private String[] P007N2_A1224lccbC ;
   private java.util.Date[] P007N2_A1223lccbD ;
   private String[] P007N2_A1222lccbI ;
   private String[] P007N2_A1150lccbE ;
   private String[] P007N2_A1184lccbS ;
   private boolean[] P007N2_n1184lccbS ;
   private String[] P007N2_A1167lccbG ;
   private boolean[] P007N2_n1167lccbG ;
   private String[] P007N6_A994HntLin ;
   private boolean[] P007N6_n994HntLin ;
   private String[] P007N6_A993HntUsu ;
   private boolean[] P007N6_n993HntUsu ;
   private String[] P007N6_A997HntSeq ;
   private byte[] P007N6_A998HntSub ;
   private String[] P007N7_A1147lccbE ;
   private boolean[] P007N7_n1147lccbE ;
   private String[] P007N7_A1150lccbE ;
   private String[] P007N8_A1166lccbP ;
   private String[] P007N8_A1150lccbE ;
   private String[] P007N8_A1165lccbD ;
   private boolean[] P007N8_n1165lccbD ;
   private String[] P007N9_A1166lccbP ;
   private String[] P007N9_A1150lccbE ;
   private String[] P007N9_A1165lccbD ;
   private boolean[] P007N9_n1165lccbD ;
   private String[] P007N10_A1166lccbP ;
   private String[] P007N10_A1150lccbE ;
   private String[] P007N10_A1165lccbD ;
   private boolean[] P007N10_n1165lccbD ;
   private String[] P007N11_A1166lccbP ;
   private String[] P007N11_A1150lccbE ;
   private String[] P007N11_A1165lccbD ;
   private boolean[] P007N11_n1165lccbD ;
   private String[] P007N12_A1148lccbF ;
   private boolean[] P007N12_n1148lccbF ;
   private String[] P007N12_A1152lccbF ;
   private String[] P007N12_A1151lccbF ;
   private String[] P007N12_A1150lccbE ;
   private long[] P007N13_A1294Serie ;
   private boolean[] P007N13_n1294Serie ;
   private long[] P007N13_A1296Serie ;
   private String[] P007N13_A1295DocCo ;
   private String[] P007N14_A1140lccbB ;
   private boolean[] P007N14_n1140lccbB ;
   private byte[] P007N14_A1145lccbB ;
   private String[] P007N14_A1141lccbB ;
   private boolean[] P007N14_n1141lccbB ;
   private String[] P007N14_A1142lccbB ;
   private boolean[] P007N14_n1142lccbB ;
   private String[] P007N14_A1144lccbE ;
   private String[] P007N14_A1143lccbB ;
   private String[] P007N15_A1149lccbD ;
   private boolean[] P007N15_n1149lccbD ;
   private String[] P007N15_A1154lccbD ;
   private String[] P007N15_A1153lccbD ;
   private String[] P007N15_A1150lccbE ;
   private long[] P007N17_A1312SCEId ;
   private boolean[] P007N17_n1312SCEId ;
   private String[] P007N18_A1309SCEFo ;
   private boolean[] P007N18_n1309SCEFo ;
   private java.util.Date[] P007N18_A1299SCEDa ;
   private boolean[] P007N18_n1299SCEDa ;
   private String[] P007N18_A1298SCETk ;
   private boolean[] P007N18_n1298SCETk ;
   private long[] P007N18_A1312SCEId ;
   private boolean[] P007N18_n1312SCEId ;
   private long[] P007N20_A1312SCEId ;
   private boolean[] P007N20_n1312SCEId ;
   private String[] P007N23_A1150lccbE ;
   private String[] P007N23_A1222lccbI ;
   private java.util.Date[] P007N23_A1223lccbD ;
   private String[] P007N23_A1224lccbC ;
   private String[] P007N23_A1225lccbC ;
   private String[] P007N23_A1226lccbA ;
   private String[] P007N23_A1227lccbO ;
   private String[] P007N23_A1228lccbF ;
   private String[] P007N23_A1231lccbT ;
   private String[] P007N23_A1232lccbT ;
   private String[] P007N23_A1207lccbP ;
   private boolean[] P007N23_n1207lccbP ;
   private String[] P007N23_A1208lccbR ;
   private boolean[] P007N23_n1208lccbR ;
   private String[] P007N23_A1209lccbR ;
   private boolean[] P007N23_n1209lccbR ;
   private String[] P007N28_A1150lccbE ;
   private String[] P007N28_A1222lccbI ;
   private java.util.Date[] P007N28_A1223lccbD ;
   private String[] P007N28_A1224lccbC ;
   private String[] P007N28_A1225lccbC ;
   private String[] P007N28_A1226lccbA ;
   private String[] P007N28_A1227lccbO ;
   private String[] P007N28_A1228lccbF ;
   private short[] P007N28_A1183lccbP ;
   private boolean[] P007N28_n1183lccbP ;
   private String[] P007N28_A1184lccbS ;
   private boolean[] P007N28_n1184lccbS ;
   private String[] P007N28_A1163lccbP ;
   private boolean[] P007N28_n1163lccbP ;
   private short[] P007N28_A1168lccbI ;
   private boolean[] P007N28_n1168lccbI ;
   private double[] P007N28_A1169lccbD ;
   private boolean[] P007N28_n1169lccbD ;
   private double[] P007N28_A1170lccbI ;
   private boolean[] P007N28_n1170lccbI ;
   private double[] P007N28_A1171lccbT ;
   private boolean[] P007N28_n1171lccbT ;
   private double[] P007N28_A1178lccbO ;
   private boolean[] P007N28_n1178lccbO ;
   private double[] P007N28_A1177lccbO ;
   private boolean[] P007N28_n1177lccbO ;
   private double[] P007N28_A1172lccbS ;
   private boolean[] P007N28_n1172lccbS ;
   private double[] P007N28_A1515lccbC ;
   private boolean[] P007N28_n1515lccbC ;
   private double[] P007N28_A1519lccbO ;
   private boolean[] P007N28_n1519lccbO ;
   private double[] P007N28_A1514lccbF ;
   private boolean[] P007N28_n1514lccbF ;
   private String[] P007N30_A1228lccbF ;
   private String[] P007N30_A1227lccbO ;
   private String[] P007N30_A1226lccbA ;
   private String[] P007N30_A1225lccbC ;
   private String[] P007N30_A1224lccbC ;
   private java.util.Date[] P007N30_A1223lccbD ;
   private String[] P007N30_A1222lccbI ;
   private String[] P007N30_A1150lccbE ;
   private String[] P007N30_A1184lccbS ;
   private boolean[] P007N30_n1184lccbS ;
   private String[] P007N30_A1167lccbG ;
   private boolean[] P007N30_n1167lccbG ;
   private double[] P007N30_A1172lccbS ;
   private boolean[] P007N30_n1172lccbS ;
   private double[] P007N30_A1169lccbD ;
   private boolean[] P007N30_n1169lccbD ;
   private double[] P007N30_A1171lccbT ;
   private boolean[] P007N30_n1171lccbT ;
   private String[] P007N31_A1150lccbE ;
   private String[] P007N31_A1222lccbI ;
   private java.util.Date[] P007N31_A1223lccbD ;
   private String[] P007N31_A1224lccbC ;
   private String[] P007N31_A1225lccbC ;
   private String[] P007N31_A1226lccbA ;
   private String[] P007N31_A1227lccbO ;
   private String[] P007N31_A1228lccbF ;
   private String[] P007N31_A1232lccbT ;
   private String[] P007N31_A1231lccbT ;
   private String[] P007N31_A1212lccbF ;
   private boolean[] P007N31_n1212lccbF ;
   private String[] P007N31_A1211lccbF ;
   private boolean[] P007N31_n1211lccbF ;
   private String[] P007N31_A1210lccbF ;
   private boolean[] P007N31_n1210lccbF ;
   private String[] P007N31_A1213lccbI ;
   private boolean[] P007N31_n1213lccbI ;
   private byte[] P007N31_A1214lccbI ;
   private boolean[] P007N31_n1214lccbI ;
   private String[] P007N31_A1217lccbF ;
   private boolean[] P007N31_n1217lccbF ;
   private String[] P007N31_A1216lccbF ;
   private boolean[] P007N31_n1216lccbF ;
   private String[] P007N31_A1215lccbF ;
   private boolean[] P007N31_n1215lccbF ;
   private byte[] P007N31_A1219lccbI ;
   private boolean[] P007N31_n1219lccbI ;
   private String[] P007N31_A1221lccbF ;
   private boolean[] P007N31_n1221lccbF ;
   private String[] P007N35_A1150lccbE ;
   private String[] P007N35_A1222lccbI ;
   private java.util.Date[] P007N35_A1223lccbD ;
   private String[] P007N35_A1224lccbC ;
   private String[] P007N35_A1225lccbC ;
   private String[] P007N35_A1226lccbA ;
   private String[] P007N35_A1227lccbO ;
   private String[] P007N35_A1228lccbF ;
   private String[] P007N35_A1231lccbT ;
   private String[] P007N35_A1232lccbT ;
   private String[] P007N35_A1209lccbR ;
   private boolean[] P007N35_n1209lccbR ;
   private String[] P007N38_A1150lccbE ;
   private String[] P007N38_A1222lccbI ;
   private java.util.Date[] P007N38_A1223lccbD ;
   private String[] P007N38_A1224lccbC ;
   private String[] P007N38_A1225lccbC ;
   private String[] P007N38_A1226lccbA ;
   private String[] P007N38_A1227lccbO ;
   private String[] P007N38_A1228lccbF ;
   private short[] P007N38_A1183lccbP ;
   private boolean[] P007N38_n1183lccbP ;
   private String[] P007N38_A1184lccbS ;
   private boolean[] P007N38_n1184lccbS ;
   private String[] P007N38_A1163lccbP ;
   private boolean[] P007N38_n1163lccbP ;
   private short[] P007N38_A1168lccbI ;
   private boolean[] P007N38_n1168lccbI ;
   private double[] P007N38_A1169lccbD ;
   private boolean[] P007N38_n1169lccbD ;
   private double[] P007N38_A1170lccbI ;
   private boolean[] P007N38_n1170lccbI ;
   private String[] P007N38_A1173lccbO ;
   private boolean[] P007N38_n1173lccbO ;
   private double[] P007N38_A1172lccbS ;
   private boolean[] P007N38_n1172lccbS ;
   private double[] P007N38_A1171lccbT ;
   private boolean[] P007N38_n1171lccbT ;
   private double[] P007N38_A1178lccbO ;
   private boolean[] P007N38_n1178lccbO ;
   private double[] P007N38_A1177lccbO ;
   private boolean[] P007N38_n1177lccbO ;
   private double[] P007N38_A1515lccbC ;
   private boolean[] P007N38_n1515lccbC ;
   private double[] P007N38_A1516lccbC ;
   private boolean[] P007N38_n1516lccbC ;
   private double[] P007N38_A1519lccbO ;
   private boolean[] P007N38_n1519lccbO ;
   private double[] P007N38_A1514lccbF ;
   private boolean[] P007N38_n1514lccbF ;
   private short[] P007N38_A1174lccbO ;
   private boolean[] P007N38_n1174lccbO ;
   private double[] P007N38_A1175lccbO ;
   private boolean[] P007N38_n1175lccbO ;
   private double[] P007N38_A1176lccbO ;
   private boolean[] P007N38_n1176lccbO ;
   private String[] P007N41_A1150lccbE ;
   private String[] P007N41_A1222lccbI ;
   private java.util.Date[] P007N41_A1223lccbD ;
   private String[] P007N41_A1224lccbC ;
   private String[] P007N41_A1225lccbC ;
   private String[] P007N41_A1226lccbA ;
   private String[] P007N41_A1227lccbO ;
   private String[] P007N41_A1228lccbF ;
   private String[] P007N41_A1231lccbT ;
   private String[] P007N41_A1232lccbT ;
   private String[] P007N41_A1210lccbF ;
   private boolean[] P007N41_n1210lccbF ;
   private String[] P007N41_A1211lccbF ;
   private boolean[] P007N41_n1211lccbF ;
   private String[] P007N41_A1212lccbF ;
   private boolean[] P007N41_n1212lccbF ;
   private String[] P007N41_A1215lccbF ;
   private boolean[] P007N41_n1215lccbF ;
   private String[] P007N41_A1216lccbF ;
   private boolean[] P007N41_n1216lccbF ;
   private String[] P007N41_A1217lccbF ;
   private boolean[] P007N41_n1217lccbF ;
   private double[] P007N41_A1220lccbT ;
   private boolean[] P007N41_n1220lccbT ;
   private String[] P007N41_A1221lccbF ;
   private boolean[] P007N41_n1221lccbF ;
   private String[] P007N41_A1207lccbP ;
   private boolean[] P007N41_n1207lccbP ;
   private String[] P007N41_A1208lccbR ;
   private boolean[] P007N41_n1208lccbR ;
   private String[] P007N41_A1209lccbR ;
   private boolean[] P007N41_n1209lccbR ;
   private String[] P007N46_A1150lccbE ;
   private String[] P007N46_A1222lccbI ;
   private java.util.Date[] P007N46_A1223lccbD ;
   private String[] P007N46_A1224lccbC ;
   private String[] P007N46_A1225lccbC ;
   private String[] P007N46_A1226lccbA ;
   private String[] P007N46_A1227lccbO ;
   private String[] P007N46_A1228lccbF ;
   private double[] P007N46_A1172lccbS ;
   private boolean[] P007N46_n1172lccbS ;
   private double[] P007N46_A1171lccbT ;
   private boolean[] P007N46_n1171lccbT ;
   private double[] P007N46_A1169lccbD ;
   private boolean[] P007N46_n1169lccbD ;
   private double[] P007N46_A1519lccbO ;
   private boolean[] P007N46_n1519lccbO ;
   private double[] P007N46_A1178lccbO ;
   private boolean[] P007N46_n1178lccbO ;
   private double[] P007N46_A1177lccbO ;
   private boolean[] P007N46_n1177lccbO ;
   private double[] P007N46_A1515lccbC ;
   private boolean[] P007N46_n1515lccbC ;
   private double[] P007N46_A1514lccbF ;
   private boolean[] P007N46_n1514lccbF ;
   private String[] P007N46_A1163lccbP ;
   private boolean[] P007N46_n1163lccbP ;
   private short[] P007N46_A1168lccbI ;
   private boolean[] P007N46_n1168lccbI ;
   private double[] P007N46_A1170lccbI ;
   private boolean[] P007N46_n1170lccbI ;
   private short[] P007N46_A1183lccbP ;
   private boolean[] P007N46_n1183lccbP ;
   private String[] P007N46_A1184lccbS ;
   private boolean[] P007N46_n1184lccbS ;
   private String[] P007N48_A1232lccbT ;
   private String[] P007N48_A1228lccbF ;
   private String[] P007N48_A1227lccbO ;
   private String[] P007N48_A1226lccbA ;
   private String[] P007N48_A1225lccbC ;
   private String[] P007N48_A1224lccbC ;
   private java.util.Date[] P007N48_A1223lccbD ;
   private String[] P007N48_A1150lccbE ;
   private String[] P007N48_A1222lccbI ;
   private String[] P007N48_A1231lccbT ;
   private java.util.Date[] P007N49_A1155lccbP ;
   private boolean[] P007N49_n1155lccbP ;
   private java.util.Date[] P007N49_A1164lccbP ;
   private String[] P007N49_A1163lccbP ;
   private boolean[] P007N49_n1163lccbP ;
   private String[] P007N49_A1150lccbE ;
   private byte[] P007N49_A1156lccbP ;
   private boolean[] P007N49_n1156lccbP ;
   private byte[] P007N49_A1157lccbP ;
   private boolean[] P007N49_n1157lccbP ;
   private double[] P007N49_A1162lccbR ;
   private boolean[] P007N49_n1162lccbR ;
   private double[] P007N49_A1160lccbP ;
   private boolean[] P007N49_n1160lccbP ;
   private String[] P007N50_A1166lccbP ;
   private String[] P007N50_A1150lccbE ;
   private String[] P007N50_A1513lccbD ;
   private String[] P007N51_A1166lccbP ;
   private String[] P007N51_A1150lccbE ;
   private String[] P007N51_A1513lccbD ;
   private String[] P007N52_A1166lccbP ;
   private String[] P007N52_A1150lccbE ;
   private String[] P007N52_A1513lccbD ;
   private String[] P007N53_A1166lccbP ;
   private String[] P007N53_A1150lccbE ;
   private String[] P007N53_A1513lccbD ;
   private String[] P007N54_A993HntUsu ;
   private boolean[] P007N54_n993HntUsu ;
   private String[] P007N54_A997HntSeq ;
   private String[] P007N54_A994HntLin ;
   private boolean[] P007N54_n994HntLin ;
   private byte[] P007N54_A998HntSub ;
   private String[] P007N56_A993HntUsu ;
   private boolean[] P007N56_n993HntUsu ;
   private String[] P007N56_A997HntSeq ;
   private String[] P007N56_A994HntLin ;
   private boolean[] P007N56_n994HntLin ;
   private byte[] P007N56_A998HntSub ;
   private String[] P007N58_A993HntUsu ;
   private boolean[] P007N58_n993HntUsu ;
   private String[] P007N58_A997HntSeq ;
   private String[] P007N58_A994HntLin ;
   private boolean[] P007N58_n994HntLin ;
   private byte[] P007N58_A998HntSub ;
   private String[] P007N60_A993HntUsu ;
   private boolean[] P007N60_n993HntUsu ;
   private String[] P007N60_A997HntSeq ;
   private String[] P007N60_A994HntLin ;
   private boolean[] P007N60_n994HntLin ;
   private byte[] P007N60_A998HntSub ;
   private String[] P007N62_A993HntUsu ;
   private boolean[] P007N62_n993HntUsu ;
   private String[] P007N62_A997HntSeq ;
   private String[] P007N62_A994HntLin ;
   private boolean[] P007N62_n994HntLin ;
   private byte[] P007N62_A998HntSub ;
   private String[] P007N64_A1208lccbR ;
   private boolean[] P007N64_n1208lccbR ;
   private String[] P007N64_A1150lccbE ;
   private String[] P007N64_A1222lccbI ;
   private java.util.Date[] P007N64_A1223lccbD ;
   private String[] P007N64_A1224lccbC ;
   private String[] P007N64_A1225lccbC ;
   private String[] P007N64_A1226lccbA ;
   private String[] P007N64_A1227lccbO ;
   private String[] P007N64_A1228lccbF ;
   private String[] P007N64_A1231lccbT ;
   private String[] P007N64_A1232lccbT ;
   private String[] P007N66_A1228lccbF ;
   private String[] P007N66_A1227lccbO ;
   private String[] P007N66_A1226lccbA ;
   private String[] P007N66_A1225lccbC ;
   private String[] P007N66_A1224lccbC ;
   private java.util.Date[] P007N66_A1223lccbD ;
   private String[] P007N66_A1222lccbI ;
   private String[] P007N66_A1150lccbE ;
   private double[] P007N66_A1172lccbS ;
   private boolean[] P007N66_n1172lccbS ;
   private String[] P007N66_A1184lccbS ;
   private boolean[] P007N66_n1184lccbS ;
   private String[] P007N68_A1228lccbF ;
   private String[] P007N68_A1227lccbO ;
   private String[] P007N68_A1226lccbA ;
   private String[] P007N68_A1225lccbC ;
   private String[] P007N68_A1224lccbC ;
   private java.util.Date[] P007N68_A1223lccbD ;
   private String[] P007N68_A1222lccbI ;
   private String[] P007N68_A1150lccbE ;
   private String[] P007N68_A1167lccbG ;
   private boolean[] P007N68_n1167lccbG ;
   private double[] P007N68_A1172lccbS ;
   private boolean[] P007N68_n1172lccbS ;
   private double[] P007N68_A1519lccbO ;
   private boolean[] P007N68_n1519lccbO ;
   private java.util.Date[] P007N68_A1189lccbS ;
   private boolean[] P007N68_n1189lccbS ;
   private String[] P007N68_A1179lccbV ;
   private boolean[] P007N68_n1179lccbV ;
   private String[] P007N68_A1180lccbC ;
   private boolean[] P007N68_n1180lccbC ;
   private String[] P007N68_A1181lccbC ;
   private boolean[] P007N68_n1181lccbC ;
   private String[] P007N68_A1182lccbS ;
   private boolean[] P007N68_n1182lccbS ;
   private String[] P007N69_A1150lccbE ;
   private String[] P007N69_A1222lccbI ;
   private java.util.Date[] P007N69_A1223lccbD ;
   private String[] P007N69_A1224lccbC ;
   private String[] P007N69_A1225lccbC ;
   private String[] P007N69_A1226lccbA ;
   private String[] P007N69_A1227lccbO ;
   private String[] P007N69_A1228lccbF ;
   private String[] P007N69_A1231lccbT ;
   private String[] P007N69_A1232lccbT ;
   private String[] P007N69_A1210lccbF ;
   private boolean[] P007N69_n1210lccbF ;
   private String[] P007N69_A1211lccbF ;
   private boolean[] P007N69_n1211lccbF ;
   private String[] P007N69_A1212lccbF ;
   private boolean[] P007N69_n1212lccbF ;
   private String[] P007N69_A1213lccbI ;
   private boolean[] P007N69_n1213lccbI ;
   private byte[] P007N69_A1214lccbI ;
   private boolean[] P007N69_n1214lccbI ;
   private String[] P007N69_A1215lccbF ;
   private boolean[] P007N69_n1215lccbF ;
   private String[] P007N69_A1216lccbF ;
   private boolean[] P007N69_n1216lccbF ;
   private String[] P007N69_A1217lccbF ;
   private boolean[] P007N69_n1217lccbF ;
   private String[] P007N69_A1218lccbI ;
   private boolean[] P007N69_n1218lccbI ;
   private byte[] P007N69_A1219lccbI ;
   private boolean[] P007N69_n1219lccbI ;
   private double[] P007N69_A1220lccbT ;
   private boolean[] P007N69_n1220lccbT ;
   private String[] P007N69_A1221lccbF ;
   private boolean[] P007N69_n1221lccbF ;
   private String[] P007N69_A1207lccbP ;
   private boolean[] P007N69_n1207lccbP ;
   private String[] P007N69_A1208lccbR ;
   private boolean[] P007N69_n1208lccbR ;
   private String[] P007N70_A1227lccbO ;
   private String[] P007N70_A1226lccbA ;
   private String[] P007N70_A1225lccbC ;
   private String[] P007N70_A1224lccbC ;
   private java.util.Date[] P007N70_A1223lccbD ;
   private String[] P007N70_A1150lccbE ;
   private String[] P007N70_A1222lccbI ;
   private String[] P007N70_A1167lccbG ;
   private boolean[] P007N70_n1167lccbG ;
   private String[] P007N70_A1228lccbF ;
   private String[] P007N71_A1231lccbT ;
   private String[] P007N71_A1150lccbE ;
   private String[] P007N71_A1222lccbI ;
   private java.util.Date[] P007N71_A1223lccbD ;
   private String[] P007N71_A1224lccbC ;
   private String[] P007N71_A1225lccbC ;
   private String[] P007N71_A1226lccbA ;
   private String[] P007N71_A1227lccbO ;
   private String[] P007N71_A1228lccbF ;
   private String[] P007N71_A1232lccbT ;
   private Sdtsdt_RETFOP_sdt_RETFOPItem AV217retfo ;
}

final  class areticsi_bkp__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007N2", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbStatus], [lccbGDS] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbOpCode] = 'S' AND [lccbStatus] = 'PENDS') AND ([lccbOpCode] = 'S' and [lccbStatus] = 'PENDS') ORDER BY [lccbOpCode], [lccbStatus] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N3", "DELETE FROM [LCCBPLP1]  WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N4", "DELETE FROM [LCCBPLP2]  WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N5", "DELETE FROM [LCCBPLP]  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N6", "SELECT [HntLine], [HntUsuCod], [HntSeq], [HntSub] FROM [RETSPECTEMP] WITH (NOLOCK) WHERE [HntUsuCod] = 'RETClone' or [HntUsuCod] = 'RETCloneRFN' or SUBSTRING([HntLine], 1, 1) = '1' or SUBSTRING([HntLine], 1, 1) = 'Z' ORDER BY [HntSeq] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N7", "SELECT [lccbEmpEnab], [lccbEmpCod] FROM [LCCBEMP] WITH (NOLOCK) WHERE [lccbEmpEnab] = '1' ORDER BY [lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N8", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecActID] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '1' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N9", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecActID] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '2' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N10", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecActID] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '3' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N11", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecActID] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '4' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N12", "SELECT [lccbFormEnab], [lccbFormCode], [lccbFormGDS], [lccbEmpCod] FROM [LCCBEMP1] WITH (NOLOCK) ORDER BY [lccbEmpCod], [lccbFormGDS], [lccbFormCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N13", "SELECT [SerieFinal], [SerieInicio], [DocCod] FROM [TIPODOCUMENTOS1] WITH (NOLOCK) ORDER BY [DocCod], [SerieInicio] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N14", "SELECT [lccbBINType], [lccbBINLen], [lccbBinck], [lccbBINEnable], [lccbEmpCC], [lccbBIN] FROM [LCCBBIN] WITH (NOLOCK) ORDER BY [lccbBIN], [lccbEmpCC] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N15", "SELECT [lccbDocEnab], [lccbDocType], [lccbDocCC], [lccbEmpCod] FROM [LCCBEMP2] WITH (NOLOCK) WHERE [lccbDocEnab] = '1' ORDER BY [lccbEmpCod], [lccbDocCC], [lccbDocType] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N16", "INSERT INTO [SCEVENTS] ([SCETkt], [SCEDate], [SCECRS], [SCEAirLine], [SCERETName], [SCEText], [SCEAPP], [SCEIata], [SCETpEvento], [SCEVersao], [SCEFPAM], [SCEParcela], [SCEAirportCode], [SCEFopID]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N17", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
         ,new ForEachCursor("P007N18", "SELECT [SCEFopID], [SCEDate], [SCETkt], [SCEId] FROM [SCEVENTS] WITH (NOLOCK) WHERE ([SCETkt] = SUBSTRING(?, 1, 10)) AND ([SCEFopID] <> 'L') AND ([SCEDate] = ?) ORDER BY [SCEId] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N19", "INSERT INTO [SCEVENTS] ([SCETkt], [SCEDate], [SCECRS], [SCEAirLine], [SCERETName], [SCEText], [SCEAPP], [SCEIata], [SCEFopID], [SCETpEvento], [SCEVersao], [SCEFPAM], [SCEParcela], [SCEAirportCode]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N20", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
         ,new UpdateCursor("P007N21", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N22", "INSERT INTO [LCCBPLP2] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbPaxName], [lccbRETFile], [lccbRETDump], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbIT08Pos2], [lccbIT08Seq2], [lccbTaxes], [lccbFirstTicket], [lccbCCNumEnc2]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '', '', '', convert(int, 0), '', '', '', '', convert(int, 0), convert(int, 0), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N23", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbPaxName], [lccbRETFile], [lccbRETDump] FROM [LCCBPLP2] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? and [lccbTDNR] = ? and [lccbTRNC] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N24", "UPDATE [LCCBPLP2] SET [lccbPaxName]=?, [lccbRETFile]=?, [lccbRETDump]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N25", "INSERT INTO [LCCBPLP2] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbPaxName], [lccbRETFile], [lccbRETDump], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbIT08Pos2], [lccbIT08Seq2], [lccbTaxes], [lccbFirstTicket], [lccbCCNumEnc2]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '', '', '', '', convert(int, 0), '', '', '', '', convert(int, 0), convert(int, 0), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N26", "UPDATE [LCCBPLP2] SET [lccbPaxName]=SUBSTRING(?, 1, 50), [lccbRETFile]=SUBSTRING(?, 1, 20), [lccbRETDump]=''  WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? and [lccbTDNR] = ? and [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N27", "INSERT INTO [LCCBPLP] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbGDS], [lccbPlanCode], [lccbInstallments], [lccbDownPayment], [lccbInstAmount], [lccbTip], [lccbSaleAmount], [lccbOrgInstAmount], [lccbOrgTip], [lccbOrgSaleAmount], [lccbValidDate], [lccbCardHolder], [lccbCurrency], [lccbSecCode], [lccbPLPStatus], [lccbStatus], [lccbSubDate], [lccbSubFile], [lccbCreationDate], [DistribuicaoTransacoesTipo], [lccbFareAmount], [lccbCashAmount], [lccbCCAmount], [lccbOrgFirstInstallment], [lccbOrgPlanCode], [lccbOrgInstallments], [lccbOrgDownPayment], [lccbBatchNum], [lccbSubTime], [lccbSubType], [lccbSubTrn], [lccbSubRO], [lccbSubPOS], [lccbRSubDate], [lccbRSubTime], [lccbRSubType], [lccbRSubFile], [lccbRSubTrn], [lccbRSubError], [lccbRSubCCCF], [lccbRSubRO], [lccbRSubAppCode], [LccbRCredDate], [DistribuicaoTransacoesEmpresa], [lccbCCNumEnc]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0), '', convert( DATETIME, '17530101', 112 ), '', '', '', '', convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', '', '', convert(int, 0), convert(int, 0), '', '', convert( DATETIME, '17530101', 112 ), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N28", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbPLPStatus], [lccbStatus], [lccbPlanCode], [lccbInstallments], [lccbDownPayment], [lccbInstAmount], [lccbTip], [lccbOrgSaleAmount], [lccbOrgTip], [lccbSaleAmount], [lccbCashAmount], [lccbOrgFirstInstallment], [lccbFareAmount] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N29", "UPDATE [LCCBPLP] SET [lccbPLPStatus]=?, [lccbStatus]=?, [lccbPlanCode]=?, [lccbInstallments]=?, [lccbDownPayment]=?, [lccbInstAmount]=?, [lccbTip]=?, [lccbOrgSaleAmount]=?, [lccbOrgTip]=?, [lccbSaleAmount]=?, [lccbCashAmount]=?, [lccbOrgFirstInstallment]=?, [lccbFareAmount]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N30", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbStatus], [lccbGDS], [lccbSaleAmount], [lccbDownPayment], [lccbTip] FROM [LCCBPLP] WITH (NOLOCK) WHERE [lccbOpCode] = 'S' and [lccbStatus] = 'PENDS' ORDER BY [lccbOpCode], [lccbStatus] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N31", "SELECT TOP 1 [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTRNC], [lccbTDNR], [lccbFPAC1], [lccbFPAM1], [lccbFPTP1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPAC2], [lccbFPAM2], [lccbFPTP2], [lccbIT08Seq2], [lccbFirstTicket] FROM [LCCBPLP2] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?) ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N32", "UPDATE [LCCBPLP2] SET [lccbFirstTicket]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N33", "UPDATE [LCCBPLP2] SET [lccbFirstTicket]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N34", "INSERT INTO [LCCBPLP2] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbPaxName], [lccbRETFile], [lccbRETDump], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbIT08Pos2], [lccbIT08Seq2], [lccbTaxes], [lccbFirstTicket], [lccbCCNumEnc2]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N35", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbRETDump] FROM [LCCBPLP2] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? and [lccbTDNR] = ? and [lccbTRNC] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N36", "UPDATE [LCCBPLP2] SET [lccbRETDump]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N37", "INSERT INTO [LCCBPLP] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbGDS], [lccbPlanCode], [lccbInstallments], [lccbDownPayment], [lccbInstAmount], [lccbTip], [lccbSaleAmount], [lccbOrgPlanCode], [lccbOrgInstallments], [lccbOrgDownPayment], [lccbOrgInstAmount], [lccbOrgTip], [lccbOrgSaleAmount], [lccbValidDate], [lccbCardHolder], [lccbCurrency], [lccbSecCode], [lccbPLPStatus], [lccbStatus], [lccbSubDate], [lccbCreationDate], [DistribuicaoTransacoesTipo], [lccbFareAmount], [lccbCashAmount], [lccbCCAmount], [lccbOrgFirstInstallment], [lccbBatchNum], [lccbSubTime], [lccbSubType], [lccbSubFile], [lccbSubTrn], [lccbSubRO], [lccbSubPOS], [lccbRSubDate], [lccbRSubTime], [lccbRSubType], [lccbRSubFile], [lccbRSubTrn], [lccbRSubError], [lccbRSubCCCF], [lccbRSubRO], [lccbRSubAppCode], [LccbRCredDate], [DistribuicaoTransacoesEmpresa], [lccbCCNumEnc]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert( DATETIME, '17530101', 112 ), '', '', '', '', '', convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', '', '', convert(int, 0), convert(int, 0), '', '', convert( DATETIME, '17530101', 112 ), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N38", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbPLPStatus], [lccbStatus], [lccbPlanCode], [lccbInstallments], [lccbDownPayment], [lccbInstAmount], [lccbOrgPlanCode], [lccbSaleAmount], [lccbTip], [lccbOrgSaleAmount], [lccbOrgTip], [lccbCashAmount], [lccbCCAmount], [lccbOrgFirstInstallment], [lccbFareAmount], [lccbOrgInstallments], [lccbOrgDownPayment], [lccbOrgInstAmount] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N39", "UPDATE [LCCBPLP] SET [lccbPLPStatus]=?, [lccbStatus]=?, [lccbPlanCode]=?, [lccbInstallments]=?, [lccbDownPayment]=?, [lccbInstAmount]=?, [lccbOrgPlanCode]=?, [lccbSaleAmount]=?, [lccbTip]=?, [lccbOrgSaleAmount]=?, [lccbOrgTip]=?, [lccbCashAmount]=?, [lccbCCAmount]=?, [lccbOrgFirstInstallment]=?, [lccbFareAmount]=?, [lccbOrgInstallments]=?, [lccbOrgDownPayment]=?, [lccbOrgInstAmount]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N40", "INSERT INTO [LCCBPLP2] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbPaxName], [lccbRETFile], [lccbRETDump], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbIT08Pos2], [lccbIT08Seq2], [lccbTaxes], [lccbFirstTicket], [lccbCCNumEnc2]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N41", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbTaxes], [lccbFirstTicket], [lccbPaxName], [lccbRETFile], [lccbRETDump] FROM [LCCBPLP2] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? and [lccbTDNR] = ? and [lccbTRNC] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N42", "UPDATE [LCCBPLP2] SET [lccbFPTP1]=?, [lccbFPAM1]=?, [lccbFPAC1]=?, [lccbFPTP2]=?, [lccbFPAM2]=?, [lccbFPAC2]=?, [lccbTaxes]=?, [lccbFirstTicket]=?, [lccbPaxName]=?, [lccbRETFile]=?, [lccbRETDump]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N43", "INSERT INTO [LCCBPLP2] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbPaxName], [lccbRETFile], [lccbFirstTicket], [lccbRETDump], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbIT08Pos2], [lccbIT08Seq2], [lccbTaxes], [lccbCCNumEnc2]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '', '', '', '', convert(int, 0), '', '', '', '', convert(int, 0), convert(int, 0), '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N44", "UPDATE [LCCBPLP2] SET [lccbPaxName]=SUBSTRING(?, 1, 50), [lccbRETFile]=SUBSTRING(?, 1, 20), [lccbFirstTicket]='', [lccbRETDump]=''  WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? and [lccbTDNR] = ? and [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007N45", "INSERT INTO [LCCBPLP] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbGDS], [lccbPlanCode], [lccbInstallments], [lccbDownPayment], [lccbInstAmount], [lccbTip], [lccbSaleAmount], [lccbOrgInstAmount], [lccbOrgTip], [lccbOrgSaleAmount], [lccbValidDate], [lccbCardHolder], [lccbCurrency], [lccbSecCode], [lccbPLPStatus], [lccbStatus], [lccbSubDate], [lccbCreationDate], [DistribuicaoTransacoesTipo], [lccbFareAmount], [lccbOrgFirstInstallment], [lccbOrgPlanCode], [lccbOrgInstallments], [lccbOrgDownPayment], [lccbBatchNum], [lccbSubTime], [lccbSubType], [lccbSubFile], [lccbSubTrn], [lccbSubRO], [lccbSubPOS], [lccbRSubDate], [lccbRSubTime], [lccbRSubType], [lccbRSubFile], [lccbRSubTrn], [lccbRSubError], [lccbRSubCCCF], [lccbRSubRO], [lccbRSubAppCode], [LccbRCredDate], [DistribuicaoTransacoesEmpresa], [lccbCCNumEnc], [lccbCashAmount], [lccbCCAmount]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0), '', convert( DATETIME, '17530101', 112 ), '', '', '', '', '', convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', '', '', convert(int, 0), convert(int, 0), '', '', convert( DATETIME, '17530101', 112 ), '', '', convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N46", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSaleAmount], [lccbTip], [lccbDownPayment], [lccbOrgFirstInstallment], [lccbOrgSaleAmount], [lccbOrgTip], [lccbCashAmount], [lccbFareAmount], [lccbPlanCode], [lccbInstallments], [lccbInstAmount], [lccbPLPStatus], [lccbStatus] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N47", "UPDATE [LCCBPLP] SET [lccbSaleAmount]=?, [lccbTip]=?, [lccbDownPayment]=?, [lccbOrgFirstInstallment]=?, [lccbOrgSaleAmount]=?, [lccbOrgTip]=?, [lccbCashAmount]=?, [lccbFareAmount]=?, [lccbPlanCode]=?, [lccbInstallments]=?, [lccbInstAmount]=?, [lccbPLPStatus]=?, [lccbStatus]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N48", "SELECT [lccbTRNC], [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbEmpCod], [lccbIATA], [lccbTDNR] FROM [LCCBPLP2] WITH (NOLOCK) WHERE ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?) AND ([lccbTRNC] <> 'CNJ') ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N49", "SELECT [lccbPlanValidFin], [lccbPlanValidIni], [lccbPlanCode], [lccbEmpCod], [lccbPlanParMin], [lccbPlanParMax], [lccbRounding], [lccbPlanJuros] FROM [LCCBEMPRESAPLANO] WITH (NOLOCK) WHERE ([lccbEmpCod] = ? and [lccbPlanCode] = ?) AND ([lccbPlanValidFin] >= ?) AND (LEN(?) > 0) AND ([lccbPlanValidIni] <= ?) ORDER BY [lccbEmpCod], [lccbPlanCode], [lccbPlanValidIni] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007N50", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecDesc] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '1' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N51", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecDesc] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '2' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N52", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecDesc] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '3' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N53", "SELECT [lccbProblemID], [lccbEmpCod], [lccbDecDesc] FROM [LCCBEMPRESAPROBLEMA] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbProblemID] = '4' ORDER BY [lccbEmpCod], [lccbProblemID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N54", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND ([HntUsuCod] like 'RETClone%')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N55", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N56", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND ([HntUsuCod] like 'RETClone%')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N57", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N58", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND ([HntUsuCod] like 'RETClone%')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N59", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N60", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND ([HntUsuCod] like 'RETClone%')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N61", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N62", "SELECT [HntUsuCod], [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (UPDLOCK) WHERE ([HntSeq] = ?) AND (([HntSeq] = ?) AND ([HntUsuCod] like 'RETClone%')) ORDER BY [HntSeq] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N63", "UPDATE [RETSPECTEMP] SET [HntLine]=?  WHERE [HntSeq] = ? AND [HntSub] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N64", "SELECT [lccbRETFile], [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC] FROM [LCCBPLP2] WITH (UPDLOCK) WHERE ([lccbRETFile] = ?) AND ([lccbRETFile] = ?) ORDER BY [lccbRETFile] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007N65", "DELETE FROM [LCCBPLP2]  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ? AND [lccbTDNR] = ? AND [lccbTRNC] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N66", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbSaleAmount], [lccbStatus] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?) AND ([lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007N67", "DELETE FROM [LCCBPLP]  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007N68", "SELECT TOP 1 [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbGDS], [lccbSaleAmount], [lccbOrgFirstInstallment], [lccbSubDate], [lccbValidDate], [lccbCardHolder], [lccbCurrency], [lccbSecCode] FROM [LCCBPLP] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N69", "SELECT TOP 1 [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC], [lccbFPTP1], [lccbFPAM1], [lccbFPAC1], [lccbIT08Pos1], [lccbIT08Seq1], [lccbFPTP2], [lccbFPAM2], [lccbFPAC2], [lccbIT08Pos2], [lccbIT08Seq2], [lccbTaxes], [lccbFirstTicket], [lccbPaxName], [lccbRETFile] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? and [lccbTDNR] = ? and [lccbTRNC] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbTRNC] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N70", "SELECT TOP 1 [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbEmpCod], [lccbIATA], [lccbGDS], [lccbFPAC_PLP] FROM [LCCBPLP] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P007N71", "SELECT [lccbTDNR], [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbTDNR] = ? ORDER BY [lccbTDNR] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 8) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 8) ;
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 4) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 4) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               break;
            case 11 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((long[]) buf[2])[0] = rslt.getLong(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((byte[]) buf[2])[0] = rslt.getByte(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[8])[0] = rslt.getString(6, 5) ;
               break;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 4) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               break;
            case 15 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDateTime(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((long[]) buf[6])[0] = rslt.getLong(4) ;
               break;
            case 18 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 21 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               ((String[]) buf[10])[0] = rslt.getString(11, 50) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(12, 20) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               break;
            case 26 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((short[]) buf[8])[0] = rslt.getShort(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 8) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 20) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((short[]) buf[14])[0] = rslt.getShort(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((double[]) buf[16])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((double[]) buf[24])[0] = rslt.getDouble(17) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((double[]) buf[26])[0] = rslt.getDouble(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((double[]) buf[28])[0] = rslt.getDouble(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((double[]) buf[30])[0] = rslt.getDouble(20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((double[]) buf[32])[0] = rslt.getDouble(21) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               break;
            case 28 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 8) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((double[]) buf[14])[0] = rslt.getDouble(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((double[]) buf[16])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               break;
            case 29 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 4) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 10) ;
               ((String[]) buf[10])[0] = rslt.getString(11, 44) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(12, 11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 10) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(14, 8) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((byte[]) buf[18])[0] = rslt.getByte(15) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((String[]) buf[20])[0] = rslt.getString(16, 19) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((String[]) buf[22])[0] = rslt.getString(17, 11) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(18, 10) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((byte[]) buf[26])[0] = rslt.getByte(19) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((String[]) buf[28])[0] = rslt.getString(20, 1) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               break;
            case 33 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               ((String[]) buf[10])[0] = rslt.getVarchar(11) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               break;
            case 36 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((short[]) buf[8])[0] = rslt.getShort(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 8) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 20) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((short[]) buf[14])[0] = rslt.getShort(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((double[]) buf[16])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((String[]) buf[20])[0] = rslt.getString(15, 20) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((double[]) buf[24])[0] = rslt.getDouble(17) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((double[]) buf[26])[0] = rslt.getDouble(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((double[]) buf[28])[0] = rslt.getDouble(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((double[]) buf[30])[0] = rslt.getDouble(20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((double[]) buf[32])[0] = rslt.getDouble(21) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((double[]) buf[34])[0] = rslt.getDouble(22) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((double[]) buf[36])[0] = rslt.getDouble(23) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               ((short[]) buf[38])[0] = rslt.getShort(24) ;
               ((boolean[]) buf[39])[0] = rslt.wasNull();
               ((double[]) buf[40])[0] = rslt.getDouble(25) ;
               ((boolean[]) buf[41])[0] = rslt.wasNull();
               ((double[]) buf[42])[0] = rslt.getDouble(26) ;
               ((boolean[]) buf[43])[0] = rslt.wasNull();
               break;
            case 39 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               ((String[]) buf[10])[0] = rslt.getString(11, 10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(12, 11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 44) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(14, 10) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(15, 11) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((String[]) buf[20])[0] = rslt.getString(16, 19) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(17) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(18, 1) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(19, 50) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((String[]) buf[28])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getVarchar(21) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               break;
            case 44 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((double[]) buf[8])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((double[]) buf[10])[0] = rslt.getDouble(10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((double[]) buf[14])[0] = rslt.getDouble(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((double[]) buf[16])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(17, 20) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((short[]) buf[26])[0] = rslt.getShort(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((double[]) buf[28])[0] = rslt.getDouble(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((short[]) buf[30])[0] = rslt.getShort(20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 8) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               break;
            case 46 :
               ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 19) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 2) ;
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 7) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 10) ;
               break;
            case 47 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((byte[]) buf[5])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(6) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((double[]) buf[9])[0] = rslt.getDouble(7) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((double[]) buf[11])[0] = rslt.getDouble(8) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               break;
            case 48 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 49 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 50 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 51 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 52 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 54 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 56 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 58 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 60 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 8) ;
               ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
            case 62 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 7) ;
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 19) ;
               ((String[]) buf[10])[0] = rslt.getString(10, 10) ;
               ((String[]) buf[11])[0] = rslt.getString(11, 4) ;
               break;
            case 64 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((double[]) buf[8])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 8) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               break;
            case 66 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 4) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((double[]) buf[10])[0] = rslt.getDouble(10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[14])[0] = rslt.getGXDate(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 4) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 50) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((String[]) buf[20])[0] = rslt.getString(15, 3) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((String[]) buf[22])[0] = rslt.getString(16, 10) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               break;
            case 67 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               ((String[]) buf[10])[0] = rslt.getString(11, 10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(12, 11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(13, 44) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(14, 8) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((byte[]) buf[18])[0] = rslt.getByte(15) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((String[]) buf[20])[0] = rslt.getString(16, 10) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((String[]) buf[22])[0] = rslt.getString(17, 11) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(18, 19) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(19, 8) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((byte[]) buf[28])[0] = rslt.getByte(20) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((double[]) buf[30])[0] = rslt.getDouble(21) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(22, 1) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((String[]) buf[34])[0] = rslt.getString(23, 50) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((String[]) buf[36])[0] = rslt.getString(24, 20) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               break;
            case 68 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 44) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(5) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 3) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 4) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getString(9, 19) ;
               break;
            case 69 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 7) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 19) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 6 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 7 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 9 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 14 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 5);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 3);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 150);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 4);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 11);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 3);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 5);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[23]).doubleValue());
               }
               break;
            case 16 :
               stmt.setString(1, (String)parms[0], 10);
               stmt.setDateTime(2, (java.util.Date)parms[1], false);
               break;
            case 17 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 5);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 3);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 150);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 4);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 11);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 5);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 3);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[21], 5);
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[25]).doubleValue());
               }
               break;
            case 19 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[15], 4);
               }
               break;
            case 20 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 50);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(12, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(13, (String)parms[15], 5000);
               }
               break;
            case 21 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               stmt.setString(11, (String)parms[10], 3);
               stmt.setString(12, (String)parms[11], 7);
               stmt.setDate(13, (java.util.Date)parms[12]);
               stmt.setString(14, (String)parms[13], 2);
               stmt.setString(15, (String)parms[14], 44);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setString(17, (String)parms[16], 1);
               stmt.setString(18, (String)parms[17], 19);
               stmt.setString(19, (String)parms[18], 10);
               stmt.setString(20, (String)parms[19], 4);
               break;
            case 22 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 50);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 20);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 5000);
               }
               stmt.setString(4, (String)parms[6], 3);
               stmt.setString(5, (String)parms[7], 7);
               stmt.setDate(6, (java.util.Date)parms[8]);
               stmt.setString(7, (String)parms[9], 2);
               stmt.setString(8, (String)parms[10], 44);
               stmt.setString(9, (String)parms[11], 20);
               stmt.setString(10, (String)parms[12], 1);
               stmt.setString(11, (String)parms[13], 19);
               stmt.setString(12, (String)parms[14], 10);
               stmt.setString(13, (String)parms[15], 4);
               break;
            case 23 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 50);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(12, (String)parms[13], 20);
               }
               break;
            case 24 :
               stmt.setString(1, (String)parms[0], 49);
               stmt.setString(2, (String)parms[1], 100);
               stmt.setString(3, (String)parms[2], 3);
               stmt.setString(4, (String)parms[3], 7);
               stmt.setDate(5, (java.util.Date)parms[4]);
               stmt.setString(6, (String)parms[5], 2);
               stmt.setString(7, (String)parms[6], 44);
               stmt.setString(8, (String)parms[7], 20);
               stmt.setString(9, (String)parms[8], 1);
               stmt.setString(10, (String)parms[9], 19);
               stmt.setString(11, (String)parms[10], 10);
               stmt.setString(12, (String)parms[11], 4);
               break;
            case 25 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[9], 4);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(11, ((Number) parms[13]).shortValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(14, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(15, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(16, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(17, ((Number) parms[25]).doubleValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(18, ((Number) parms[27]).doubleValue());
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(19, (String)parms[29], 4);
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(20, (String)parms[31], 50);
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(21, (String)parms[33], 3);
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(22, (String)parms[35], 10);
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(23, ((Number) parms[37]).shortValue());
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(24, (String)parms[39], 8);
               }
               if ( ((Boolean) parms[40]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(25, (java.util.Date)parms[41]);
               }
               if ( ((Boolean) parms[42]).booleanValue() )
               {
                  stmt.setNull( 26 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(26, (String)parms[43], 20);
               }
               if ( ((Boolean) parms[44]).booleanValue() )
               {
                  stmt.setNull( 27 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(27, (java.util.Date)parms[45]);
               }
               if ( ((Boolean) parms[46]).booleanValue() )
               {
                  stmt.setNull( 28 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(28, (String)parms[47], 4);
               }
               if ( ((Boolean) parms[48]).booleanValue() )
               {
                  stmt.setNull( 29 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(29, ((Number) parms[49]).doubleValue());
               }
               if ( ((Boolean) parms[50]).booleanValue() )
               {
                  stmt.setNull( 30 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(30, ((Number) parms[51]).doubleValue());
               }
               if ( ((Boolean) parms[52]).booleanValue() )
               {
                  stmt.setNull( 31 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(31, ((Number) parms[53]).doubleValue());
               }
               if ( ((Boolean) parms[54]).booleanValue() )
               {
                  stmt.setNull( 32 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(32, ((Number) parms[55]).doubleValue());
               }
               break;
            case 26 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 3);
               stmt.setString(10, (String)parms[9], 7);
               stmt.setDate(11, (java.util.Date)parms[10]);
               stmt.setString(12, (String)parms[11], 2);
               stmt.setString(13, (String)parms[12], 44);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 1);
               stmt.setString(16, (String)parms[15], 19);
               break;
            case 27 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(1, ((Number) parms[1]).shortValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 8);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(4, ((Number) parms[7]).shortValue());
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(5, ((Number) parms[9]).doubleValue());
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(6, ((Number) parms[11]).doubleValue());
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(7, ((Number) parms[13]).doubleValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(8, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(9, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(10, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[25]).doubleValue());
               }
               stmt.setString(14, (String)parms[26], 3);
               stmt.setString(15, (String)parms[27], 7);
               stmt.setDate(16, (java.util.Date)parms[28]);
               stmt.setString(17, (String)parms[29], 2);
               stmt.setString(18, (String)parms[30], 44);
               stmt.setString(19, (String)parms[31], 20);
               stmt.setString(20, (String)parms[32], 1);
               stmt.setString(21, (String)parms[33], 19);
               break;
            case 29 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 3);
               stmt.setString(10, (String)parms[9], 7);
               stmt.setDate(11, (java.util.Date)parms[10]);
               stmt.setString(12, (String)parms[11], 2);
               stmt.setString(13, (String)parms[12], 44);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 1);
               stmt.setString(16, (String)parms[15], 19);
               break;
            case 30 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 1);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               stmt.setString(10, (String)parms[10], 10);
               stmt.setString(11, (String)parms[11], 4);
               break;
            case 31 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 1);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               stmt.setString(10, (String)parms[10], 10);
               stmt.setString(11, (String)parms[11], 4);
               break;
            case 32 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 50);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(12, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(13, (String)parms[15], 5000);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(14, (String)parms[17], 10);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(15, (String)parms[19], 11);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(16, (String)parms[21], 44);
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(17, (String)parms[23], 8);
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(18, ((Number) parms[25]).byteValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(19, (String)parms[27], 10);
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(20, (String)parms[29], 11);
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(21, (String)parms[31], 19);
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(22, (String)parms[33], 8);
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(23, ((Number) parms[35]).byteValue());
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(24, ((Number) parms[37]).doubleValue());
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(25, (String)parms[39], 1);
               }
               break;
            case 33 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               stmt.setString(11, (String)parms[10], 3);
               stmt.setString(12, (String)parms[11], 7);
               stmt.setDate(13, (java.util.Date)parms[12]);
               stmt.setString(14, (String)parms[13], 2);
               stmt.setString(15, (String)parms[14], 44);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setString(17, (String)parms[16], 1);
               stmt.setString(18, (String)parms[17], 19);
               stmt.setString(19, (String)parms[18], 10);
               stmt.setString(20, (String)parms[19], 4);
               break;
            case 34 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 5000);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               stmt.setString(10, (String)parms[10], 10);
               stmt.setString(11, (String)parms[11], 4);
               break;
            case 35 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[9], 4);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(11, ((Number) parms[13]).shortValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(14, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(15, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(16, (String)parms[23], 20);
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(17, ((Number) parms[25]).shortValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(18, ((Number) parms[27]).doubleValue());
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(19, ((Number) parms[29]).doubleValue());
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(20, ((Number) parms[31]).doubleValue());
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(21, ((Number) parms[33]).doubleValue());
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(22, (String)parms[35], 4);
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(23, (String)parms[37], 50);
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(24, (String)parms[39], 3);
               }
               if ( ((Boolean) parms[40]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(25, (String)parms[41], 10);
               }
               if ( ((Boolean) parms[42]).booleanValue() )
               {
                  stmt.setNull( 26 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(26, ((Number) parms[43]).shortValue());
               }
               if ( ((Boolean) parms[44]).booleanValue() )
               {
                  stmt.setNull( 27 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(27, (String)parms[45], 8);
               }
               if ( ((Boolean) parms[46]).booleanValue() )
               {
                  stmt.setNull( 28 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(28, (java.util.Date)parms[47]);
               }
               if ( ((Boolean) parms[48]).booleanValue() )
               {
                  stmt.setNull( 29 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(29, (java.util.Date)parms[49]);
               }
               if ( ((Boolean) parms[50]).booleanValue() )
               {
                  stmt.setNull( 30 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(30, (String)parms[51], 4);
               }
               if ( ((Boolean) parms[52]).booleanValue() )
               {
                  stmt.setNull( 31 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(31, ((Number) parms[53]).doubleValue());
               }
               if ( ((Boolean) parms[54]).booleanValue() )
               {
                  stmt.setNull( 32 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(32, ((Number) parms[55]).doubleValue());
               }
               if ( ((Boolean) parms[56]).booleanValue() )
               {
                  stmt.setNull( 33 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(33, ((Number) parms[57]).doubleValue());
               }
               if ( ((Boolean) parms[58]).booleanValue() )
               {
                  stmt.setNull( 34 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(34, ((Number) parms[59]).doubleValue());
               }
               break;
            case 36 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 3);
               stmt.setString(10, (String)parms[9], 7);
               stmt.setDate(11, (java.util.Date)parms[10]);
               stmt.setString(12, (String)parms[11], 2);
               stmt.setString(13, (String)parms[12], 44);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 1);
               stmt.setString(16, (String)parms[15], 19);
               break;
            case 37 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(1, ((Number) parms[1]).shortValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 8);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(4, ((Number) parms[7]).shortValue());
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(5, ((Number) parms[9]).doubleValue());
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(6, ((Number) parms[11]).doubleValue());
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(8, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(9, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(10, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[25]).doubleValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(14, ((Number) parms[27]).doubleValue());
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(15, ((Number) parms[29]).doubleValue());
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(16, ((Number) parms[31]).shortValue());
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(17, ((Number) parms[33]).doubleValue());
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(18, ((Number) parms[35]).doubleValue());
               }
               stmt.setString(19, (String)parms[36], 3);
               stmt.setString(20, (String)parms[37], 7);
               stmt.setDate(21, (java.util.Date)parms[38]);
               stmt.setString(22, (String)parms[39], 2);
               stmt.setString(23, (String)parms[40], 44);
               stmt.setString(24, (String)parms[41], 20);
               stmt.setString(25, (String)parms[42], 1);
               stmt.setString(26, (String)parms[43], 19);
               break;
            case 38 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 50);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(12, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(13, (String)parms[15], 5000);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(14, (String)parms[17], 10);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(15, (String)parms[19], 11);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(16, (String)parms[21], 44);
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(17, (String)parms[23], 8);
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(18, ((Number) parms[25]).byteValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(19, (String)parms[27], 10);
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(20, (String)parms[29], 11);
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(21, (String)parms[31], 19);
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(22, (String)parms[33], 8);
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(23, ((Number) parms[35]).byteValue());
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(24, ((Number) parms[37]).doubleValue());
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(25, (String)parms[39], 1);
               }
               break;
            case 39 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               stmt.setString(11, (String)parms[10], 3);
               stmt.setString(12, (String)parms[11], 7);
               stmt.setDate(13, (java.util.Date)parms[12]);
               stmt.setString(14, (String)parms[13], 2);
               stmt.setString(15, (String)parms[14], 44);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setString(17, (String)parms[16], 1);
               stmt.setString(18, (String)parms[17], 19);
               stmt.setString(19, (String)parms[18], 10);
               stmt.setString(20, (String)parms[19], 4);
               break;
            case 40 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 11);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 44);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 10);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 11);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 19);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(7, ((Number) parms[13]).doubleValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 1);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 50);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 20);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[21], 5000);
               }
               stmt.setString(12, (String)parms[22], 3);
               stmt.setString(13, (String)parms[23], 7);
               stmt.setDate(14, (java.util.Date)parms[24]);
               stmt.setString(15, (String)parms[25], 2);
               stmt.setString(16, (String)parms[26], 44);
               stmt.setString(17, (String)parms[27], 20);
               stmt.setString(18, (String)parms[28], 1);
               stmt.setString(19, (String)parms[29], 19);
               stmt.setString(20, (String)parms[30], 10);
               stmt.setString(21, (String)parms[31], 4);
               break;
            case 41 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 50);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(12, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[15], 1);
               }
               break;
            case 42 :
               stmt.setString(1, (String)parms[0], 49);
               stmt.setString(2, (String)parms[1], 100);
               stmt.setString(3, (String)parms[2], 3);
               stmt.setString(4, (String)parms[3], 7);
               stmt.setDate(5, (java.util.Date)parms[4]);
               stmt.setString(6, (String)parms[5], 2);
               stmt.setString(7, (String)parms[6], 44);
               stmt.setString(8, (String)parms[7], 20);
               stmt.setString(9, (String)parms[8], 1);
               stmt.setString(10, (String)parms[9], 19);
               stmt.setString(11, (String)parms[10], 10);
               stmt.setString(12, (String)parms[11], 4);
               break;
            case 43 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[9], 4);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(11, ((Number) parms[13]).shortValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(14, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(15, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(16, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(17, ((Number) parms[25]).doubleValue());
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(18, ((Number) parms[27]).doubleValue());
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(19, (String)parms[29], 4);
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(20, (String)parms[31], 50);
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(21, (String)parms[33], 3);
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(22, (String)parms[35], 10);
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(23, ((Number) parms[37]).shortValue());
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(24, (String)parms[39], 8);
               }
               if ( ((Boolean) parms[40]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(25, (java.util.Date)parms[41]);
               }
               if ( ((Boolean) parms[42]).booleanValue() )
               {
                  stmt.setNull( 26 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(26, (java.util.Date)parms[43]);
               }
               if ( ((Boolean) parms[44]).booleanValue() )
               {
                  stmt.setNull( 27 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(27, (String)parms[45], 4);
               }
               if ( ((Boolean) parms[46]).booleanValue() )
               {
                  stmt.setNull( 28 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(28, ((Number) parms[47]).doubleValue());
               }
               if ( ((Boolean) parms[48]).booleanValue() )
               {
                  stmt.setNull( 29 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(29, ((Number) parms[49]).doubleValue());
               }
               break;
            case 44 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 3);
               stmt.setString(10, (String)parms[9], 7);
               stmt.setDate(11, (java.util.Date)parms[10]);
               stmt.setString(12, (String)parms[11], 2);
               stmt.setString(13, (String)parms[12], 44);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 1);
               stmt.setString(16, (String)parms[15], 19);
               break;
            case 45 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(1, ((Number) parms[1]).doubleValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(2, ((Number) parms[3]).doubleValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(3, ((Number) parms[5]).doubleValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(4, ((Number) parms[7]).doubleValue());
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(5, ((Number) parms[9]).doubleValue());
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(6, ((Number) parms[11]).doubleValue());
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(7, ((Number) parms[13]).doubleValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(8, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 20);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(10, ((Number) parms[19]).shortValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(12, ((Number) parms[23]).shortValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[25], 8);
               }
               stmt.setString(14, (String)parms[26], 3);
               stmt.setString(15, (String)parms[27], 7);
               stmt.setDate(16, (java.util.Date)parms[28]);
               stmt.setString(17, (String)parms[29], 2);
               stmt.setString(18, (String)parms[30], 44);
               stmt.setString(19, (String)parms[31], 20);
               stmt.setString(20, (String)parms[32], 1);
               stmt.setString(21, (String)parms[33], 19);
               break;
            case 46 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 47 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setDate(5, (java.util.Date)parms[4]);
               break;
            case 48 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 49 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 50 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 51 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 52 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 53 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 54 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 55 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 56 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 57 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 58 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 59 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 60 :
               stmt.setString(1, (String)parms[0], 8);
               stmt.setString(2, (String)parms[1], 8);
               break;
            case 61 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(1, (String)parms[1]);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setByte(3, ((Number) parms[3]).byteValue());
               break;
            case 62 :
               stmt.setString(1, (String)parms[0], 100);
               stmt.setString(2, (String)parms[1], 100);
               break;
            case 63 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               break;
            case 64 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 3);
               stmt.setString(10, (String)parms[9], 7);
               stmt.setDate(11, (java.util.Date)parms[10]);
               stmt.setString(12, (String)parms[11], 2);
               stmt.setString(13, (String)parms[12], 44);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 1);
               stmt.setString(16, (String)parms[15], 19);
               break;
            case 65 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 66 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 44);
               break;
            case 67 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setString(10, (String)parms[9], 4);
               break;
            case 68 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               break;
            case 69 :
               stmt.setString(1, (String)parms[0], 10);
               break;
      }
   }

}


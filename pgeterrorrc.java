/*
               File: GetErrorRC
        Description: Obt�m a descri��o do erro de submiss�o Redecard
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pgeterrorrc extends GXProcedure
{
   public pgeterrorrc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pgeterrorrc.class ), "" );
   }

   public pgeterrorrc( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      pgeterrorrc.this.AV8ErrType = aP0[0];
      this.aP0 = aP0;
      pgeterrorrc.this.AV9ErrCod = aP1[0];
      this.aP1 = aP1;
      pgeterrorrc.this.AV10ErrDes = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( GXutil.strcmp(AV8ErrType, "A") == 0 ) )
      {
         /* Execute user subroutine: S116 */
         S116 ();
         if ( returnInSub )
         {
            returnInSub = true;
            cleanup();
            if (true) return;
         }
      }
      else
      {
         /* Execute user subroutine: S12142 */
         S12142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            cleanup();
            if (true) return;
         }
      }
      cleanup();
   }

   public void S116( )
   {
      /* 'GETMOVERRORA' Routine */
      if ( ( GXutil.strcmp(AV9ErrCod, "00") == 0 ) )
      {
         AV10ErrDes = "C.V. OK - Transa��o Aceita" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "01") == 0 ) )
      {
         AV10ErrDes = "C.V. para Associado em Boletim de Prote��o" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "02") == 0 ) )
      {
         AV10ErrDes = "C.V. com n�mero do cart�o inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "03") == 0 ) )
      {
         AV10ErrDes = "C.V. com n�mero de maquineta inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "04") == 0 ) )
      {
         AV10ErrDes = "C.V. sem n�mero da autoriza��o" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "05") == 0 ) )
      {
         AV10ErrDes = "C.V. com data da compra inv�lida" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "06") == 0 ) )
      {
         AV10ErrDes = "C.V. com valor da venda incorreto" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "07") == 0 ) )
      {
         AV10ErrDes = "C.V. com valor total incorreto" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "08") == 0 ) )
      {
         AV10ErrDes = "Estabelecimento n�o opera Off-line" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "09") == 0 ) )
      {
         AV10ErrDes = "C.V. para cart�o Jur�dico" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "10") == 0 ) )
      {
         AV10ErrDes = "C.V. para cart�o Internacional" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "11") == 0 ) )
      {
         AV10ErrDes = "Compra Duplicada SideCard (P�o de A��car)" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "12") == 0 ) )
      {
         AV10ErrDes = "C.V. com n�mero de autoriza��o inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "13") == 0 ) )
      {
         AV10ErrDes = "C.V. com n�mero de autoriza��o j� utilizado" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "14") == 0 ) )
      {
         AV10ErrDes = "C.V. com n�mero de autoriza��o n�o encontrado" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "15") == 0 ) )
      {
         AV10ErrDes = "C.V. com valor a maior do autorizado" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "16") == 0 ) )
      {
         AV10ErrDes = "C.V. com autoriza��o para cart�o diferente" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "17") == 0 ) )
      {
         AV10ErrDes = "C.V. com autoriza��o para transa��o diferente" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "18") == 0 ) )
      {
         AV10ErrDes = "Compra acima do Limite do Estab. (RedeShop Cr�dito-Sendas)" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "19") == 0 ) )
      {
         AV10ErrDes = "Validade Expirada/Cart�o Vencido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "20") == 0 ) )
      {
         AV10ErrDes = "Transa��o inv�lida para o tipo de Cart�o" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "22") == 0 ) )
      {
         AV10ErrDes = "Conta Cancelada" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "23") == 0 ) )
      {
         AV10ErrDes = "Transa��o n�o autorizada pelo emissor" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "25") == 0 ) )
      {
         AV10ErrDes = "Taxa de Embarque - Valor Excedido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "26") == 0 ) )
      {
         AV10ErrDes = "Transa��o j� processada on-line" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "27") == 0 ) )
      {
         AV10ErrDes = "Transa��o com Moeda Incorreta" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "31") == 0 ) )
      {
         AV10ErrDes = "Autoriza��o Negada" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "32") == 0 ) )
      {
         AV10ErrDes = "N�mero de Parcelas Inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "33") == 0 ) )
      {
         AV10ErrDes = "Valor da Autoriza��o Inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "34") == 0 ) )
      {
         AV10ErrDes = "Tipo de Autoriza��o Inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "35") == 0 ) )
      {
         AV10ErrDes = "Cart�o Inv�lido, Vencido ou Data de Validade Inv�lida" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "36") == 0 ) )
      {
         AV10ErrDes = "Estabelecimento Inv�lido" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "37") == 0 ) )
      {
         AV10ErrDes = "Estabelecimento Cancelado" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "38") == 0 ) )
      {
         AV10ErrDes = "Estabelecimento n�o Opera Rotativo" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "39") == 0 ) )
      {
         AV10ErrDes = "Estabelecimento n�o Opera Parcelado com Juros" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "40") == 0 ) )
      {
         AV10ErrDes = "Estabelecimento n�o Opera Parcelado sem Juros" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "41") == 0 ) )
      {
         AV10ErrDes = "Cart�o s� opera com leitura de tarja" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "42") == 0 ) )
      {
         AV10ErrDes = "Ligar para o Emissor do Cart�o" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "43") == 0 ) )
      {
         AV10ErrDes = "N�o foi poss�vel realizar a conex�o" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "49") == 0 ) )
      {
         AV10ErrDes = "Transa��o n�o p�de ser Autorizada" ;
      }
      else
      {
         AV10ErrDes = "Erro desconhecido:[" + AV9ErrCod + "]" ;
      }
   }

   public void S12142( )
   {
      /* 'GETMOVERRORR' Routine */
      if ( ( GXutil.strcmp(AV9ErrCod, "00") == 0 ) )
      {
         AV10ErrDes = "MOVIMENTO ACEITO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "01") == 0 ) )
      {
         AV10ErrDes = "NAO TEM HEADER" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "02") == 0 ) )
      {
         AV10ErrDes = "REG. DIF 00 APOS 99" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "03") == 0 ) )
      {
         AV10ErrDes = "TIPO REG INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "04") == 0 ) )
      {
         AV10ErrDes = "COD TRANS INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "05") == 0 ) )
      {
         AV10ErrDes = "TRANS. DIF. NO MOVTO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "06") == 0 ) )
      {
         AV10ErrDes = "REQUIS. NT INFORMADA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "07") == 0 ) )
      {
         AV10ErrDes = "QTDE PARC. INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "08") == 0 ) )
      {
         AV10ErrDes = "QTDE PARC. INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "09") == 0 ) )
      {
         AV10ErrDes = "QTDE PARC. INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "10") == 0 ) )
      {
         AV10ErrDes = "QTDE PARC. INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "11") == 0 ) )
      {
         AV10ErrDes = "QTDE PARC. INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "12") == 0 ) )
      {
         AV10ErrDes = "QTDE PARC. INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "13") == 0 ) )
      {
         AV10ErrDes = "SEQ LOTE INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "14") == 0 ) )
      {
         AV10ErrDes = "NUM LOTE INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "15") == 0 ) )
      {
         AV10ErrDes = "REG DET SEM REG LOTE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "16") == 0 ) )
      {
         AV10ErrDes = "SEQ REG INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "17") == 0 ) )
      {
         AV10ErrDes = "VALOR COMPRA INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "18") == 0 ) )
      {
         AV10ErrDes = "VALOR PARCELA INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "19") == 0 ) )
      {
         AV10ErrDes = "VL TRN ORIG LOTE INVAL" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "20") == 0 ) )
      {
         AV10ErrDes = "VL VENDA LOTE INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "21") == 0 ) )
      {
         AV10ErrDes = "VL VENDA DIF LOTE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "22") == 0 ) )
      {
         AV10ErrDes = "QTD DET DIF LOTE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "23") == 0 ) )
      {
         AV10ErrDes = "NUM SEQ LOTE INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "24") == 0 ) )
      {
         AV10ErrDes = "NUMERO DO LOTE INVALID" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "25") == 0 ) )
      {
         AV10ErrDes = "NUM LOTE DUPLIC" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "26") == 0 ) )
      {
         AV10ErrDes = "SEQ REG INVAL - LOTE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "27") == 0 ) )
      {
         AV10ErrDes = "TOT GORJETA DIF LOTE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "28") == 0 ) )
      {
         AV10ErrDes = "VAL CANCEL INVAL - LOTE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "29") == 0 ) )
      {
         AV10ErrDes = "VAL COMPRA DIF TOT 1" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "30") == 0 ) )
      {
         AV10ErrDes = "VAL COMPRA INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "31") == 0 ) )
      {
         AV10ErrDes = "ENTRADA DIF TOT ENT" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "32") == 0 ) )
      {
         AV10ErrDes = "EMBARQUE DIF TOT EMB" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "33") == 0 ) )
      {
         AV10ErrDes = "PARCELA DIF TOT PARC" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "34") == 0 ) )
      {
         AV10ErrDes = "QTD PARC NT CONFERE" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "35") == 0 ) )
      {
         AV10ErrDes = "TIPO 40 DUPLICADO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "36") == 0 ) )
      {
         AV10ErrDes = "FALTA REG TIPO 30" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "37") == 0 ) )
      {
         AV10ErrDes = "VAL COMPRA NT TOT 30" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "39") == 0 ) )
      {
         AV10ErrDes = "SEQ REG INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "40") == 0 ) )
      {
         AV10ErrDes = "VAL PARC. NT TOT 30" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "42") == 0 ) )
      {
         AV10ErrDes = "TIPO 99 DUPLICADO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "43") == 0 ) )
      {
         AV10ErrDes = "SEQ REG INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "44") == 0 ) )
      {
         AV10ErrDes = "REG HEADER NT ZEROS" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "45") == 0 ) )
      {
         AV10ErrDes = "IDENT EMPR INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "46") == 0 ) )
      {
         AV10ErrDes = "NOME EMPR INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "47") == 0 ) )
      {
         AV10ErrDes = "SEQ FITA INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "48") == 0 ) )
      {
         AV10ErrDes = "DT GRAVACAO INVALIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "49") == 0 ) )
      {
         AV10ErrDes = "HR GRAVACAO NAO NUM." ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "50") == 0 ) )
      {
         AV10ErrDes = "PRODUTO INVALIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "52") == 0 ) )
      {
         AV10ErrDes = "EMPR NAO CADASTRADA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "53") == 0 ) )
      {
         AV10ErrDes = "EMPRESA EXCLUIDA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "54") == 0 ) )
      {
         AV10ErrDes = "SEQ. NAO CADASTRADA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "55") == 0 ) )
      {
         AV10ErrDes = "MOVTO EXCLUIDO" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "56") == 0 ) )
      {
         AV10ErrDes = "SEQ. JA PROCESSADA" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "59") == 0 ) )
      {
         AV10ErrDes = "MOVTO FORA DE SEQ." ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "60") == 0 ) )
      {
         AV10ErrDes = "FALTA REG. 40 OU 99" ;
      }
      else if ( ( GXutil.strcmp(AV9ErrCod, "61") == 0 ) )
      {
         AV10ErrDes = "VALIDADE DO CART�O INVALIDO / C�DIGO DE ESTABELECIMENTO INV�LIDO" ;
      }
      else
      {
         AV10ErrDes = "ERRO DESCONHECIDO:[" + AV9ErrCod + "]" ;
      }
   }

   protected void cleanup( )
   {
      this.aP0[0] = pgeterrorrc.this.AV8ErrType;
      this.aP1[0] = pgeterrorrc.this.AV9ErrCod;
      this.aP2[0] = pgeterrorrc.this.AV10ErrDes;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      returnInSub = false ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV8ErrType ;
   private String AV9ErrCod ;
   private String AV10ErrDes ;
   private boolean returnInSub ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
}


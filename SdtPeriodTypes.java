import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtPeriodTypes extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtPeriodTypes( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtPeriodTypes.class));
   }

   public SdtPeriodTypes( int remoteHandle ,
                          ModelContext context )
   {
      super( context, "SdtPeriodTypes");
      initialize( remoteHandle) ;
   }

   public SdtPeriodTypes( int remoteHandle ,
                          StructSdtPeriodTypes struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV387PeriodTypesCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV387PeriodTypesCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Periodtypescode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Periodtypesdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesStatus") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Periodtypesstatus = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Periodtypescode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Periodtypesdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesStatus_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriodTypes_Periodtypesstatus_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "PeriodTypes" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("PeriodTypesCode", GXutil.rtrim( gxTv_SdtPeriodTypes_Periodtypescode));
      oWriter.writeElement("PeriodTypesDescription", GXutil.rtrim( gxTv_SdtPeriodTypes_Periodtypesdescription));
      oWriter.writeElement("PeriodTypesStatus", GXutil.rtrim( gxTv_SdtPeriodTypes_Periodtypesstatus));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtPeriodTypes_Mode));
      oWriter.writeElement("PeriodTypesCode_Z", GXutil.rtrim( gxTv_SdtPeriodTypes_Periodtypescode_Z));
      oWriter.writeElement("PeriodTypesDescription_Z", GXutil.rtrim( gxTv_SdtPeriodTypes_Periodtypesdescription_Z));
      oWriter.writeElement("PeriodTypesStatus_Z", GXutil.rtrim( gxTv_SdtPeriodTypes_Periodtypesstatus_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Periodtypescode( )
   {
      return gxTv_SdtPeriodTypes_Periodtypescode ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypescode( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypescode = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypescode_SetNull( )
   {
      gxTv_SdtPeriodTypes_Periodtypescode = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Periodtypesdescription( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesdescription ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesdescription( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesdescription = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesdescription_SetNull( )
   {
      gxTv_SdtPeriodTypes_Periodtypesdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Periodtypesstatus( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesstatus ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesstatus( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesstatus = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesstatus_SetNull( )
   {
      gxTv_SdtPeriodTypes_Periodtypesstatus = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Mode( )
   {
      return gxTv_SdtPeriodTypes_Mode ;
   }

   public void setgxTv_SdtPeriodTypes_Mode( String value )
   {
      gxTv_SdtPeriodTypes_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Mode_SetNull( )
   {
      gxTv_SdtPeriodTypes_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Periodtypescode_Z( )
   {
      return gxTv_SdtPeriodTypes_Periodtypescode_Z ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypescode_Z( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypescode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypescode_Z_SetNull( )
   {
      gxTv_SdtPeriodTypes_Periodtypescode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Periodtypesdescription_Z( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesdescription_Z ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesdescription_Z( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesdescription_Z_SetNull( )
   {
      gxTv_SdtPeriodTypes_Periodtypesdescription_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriodTypes_Periodtypesstatus_Z( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesstatus_Z ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesstatus_Z( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesstatus_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriodTypes_Periodtypesstatus_Z_SetNull( )
   {
      gxTv_SdtPeriodTypes_Periodtypesstatus_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tperiodtypes_bc obj ;
      obj = new tperiodtypes_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtPeriodTypes_Periodtypescode = "" ;
      gxTv_SdtPeriodTypes_Periodtypesdescription = "" ;
      gxTv_SdtPeriodTypes_Periodtypesstatus = "" ;
      gxTv_SdtPeriodTypes_Mode = "" ;
      gxTv_SdtPeriodTypes_Periodtypescode_Z = "" ;
      gxTv_SdtPeriodTypes_Periodtypesdescription_Z = "" ;
      gxTv_SdtPeriodTypes_Periodtypesstatus_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtPeriodTypes Clone( )
   {
      SdtPeriodTypes sdt ;
      tperiodtypes_bc obj ;
      sdt = (SdtPeriodTypes)(clone()) ;
      obj = (tperiodtypes_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtPeriodTypes struct )
   {
      setgxTv_SdtPeriodTypes_Periodtypescode(struct.getPeriodtypescode());
      setgxTv_SdtPeriodTypes_Periodtypesdescription(struct.getPeriodtypesdescription());
      setgxTv_SdtPeriodTypes_Periodtypesstatus(struct.getPeriodtypesstatus());
      setgxTv_SdtPeriodTypes_Mode(struct.getMode());
      setgxTv_SdtPeriodTypes_Periodtypescode_Z(struct.getPeriodtypescode_Z());
      setgxTv_SdtPeriodTypes_Periodtypesdescription_Z(struct.getPeriodtypesdescription_Z());
      setgxTv_SdtPeriodTypes_Periodtypesstatus_Z(struct.getPeriodtypesstatus_Z());
   }

   public StructSdtPeriodTypes getStruct( )
   {
      StructSdtPeriodTypes struct = new StructSdtPeriodTypes ();
      struct.setPeriodtypescode(getgxTv_SdtPeriodTypes_Periodtypescode());
      struct.setPeriodtypesdescription(getgxTv_SdtPeriodTypes_Periodtypesdescription());
      struct.setPeriodtypesstatus(getgxTv_SdtPeriodTypes_Periodtypesstatus());
      struct.setMode(getgxTv_SdtPeriodTypes_Mode());
      struct.setPeriodtypescode_Z(getgxTv_SdtPeriodTypes_Periodtypescode_Z());
      struct.setPeriodtypesdescription_Z(getgxTv_SdtPeriodTypes_Periodtypesdescription_Z());
      struct.setPeriodtypesstatus_Z(getgxTv_SdtPeriodTypes_Periodtypesstatus_Z());
      return struct ;
   }

   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtPeriodTypes_Periodtypesstatus ;
   protected String gxTv_SdtPeriodTypes_Mode ;
   protected String gxTv_SdtPeriodTypes_Periodtypesstatus_Z ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtPeriodTypes_Periodtypescode ;
   protected String gxTv_SdtPeriodTypes_Periodtypesdescription ;
   protected String gxTv_SdtPeriodTypes_Periodtypescode_Z ;
   protected String gxTv_SdtPeriodTypes_Periodtypesdescription_Z ;
}


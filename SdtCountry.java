import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtCountry extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCountry( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCountry.class));
   }

   public SdtCountry( int remoteHandle ,
                      ModelContext context )
   {
      super( context, "SdtCountry");
      initialize( remoteHandle) ;
   }

   public SdtCountry( int remoteHandle ,
                      StructSdtCountry struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV23ISOCod )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV23ISOCod});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Isocod = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Isodes = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Level1") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtCountry_Level1.readxml(oReader, "Level1") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtCountry_Contacttypes.readxml(oReader, "ContactTypes") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Currencies") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtCountry_Currencies.readxml(oReader, "Currencies") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Isocod_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Isodes_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Isocod_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Isodes_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Country" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ISOCod", GXutil.rtrim( gxTv_SdtCountry_Isocod));
      oWriter.writeElement("ISODes", GXutil.rtrim( gxTv_SdtCountry_Isodes));
      gxTv_SdtCountry_Level1.writexml(oWriter, "Level1", "IataICSI");
      gxTv_SdtCountry_Contacttypes.writexml(oWriter, "ContactTypes", "IataICSI");
      gxTv_SdtCountry_Currencies.writexml(oWriter, "Currencies", "IataICSI");
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCountry_Mode));
      oWriter.writeElement("ISOCod_Z", GXutil.rtrim( gxTv_SdtCountry_Isocod_Z));
      oWriter.writeElement("ISODes_Z", GXutil.rtrim( gxTv_SdtCountry_Isodes_Z));
      oWriter.writeElement("ISOCod_N", GXutil.trim( GXutil.str( gxTv_SdtCountry_Isocod_N, 1, 0)));
      oWriter.writeElement("ISODes_N", GXutil.trim( GXutil.str( gxTv_SdtCountry_Isodes_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtCountry_Isocod( )
   {
      return gxTv_SdtCountry_Isocod ;
   }

   public void setgxTv_SdtCountry_Isocod( String value )
   {
      gxTv_SdtCountry_Isocod_N = (byte)(0) ;
      gxTv_SdtCountry_Isocod = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Isocod_SetNull( )
   {
      gxTv_SdtCountry_Isocod_N = (byte)(1) ;
      gxTv_SdtCountry_Isocod = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_Isodes( )
   {
      return gxTv_SdtCountry_Isodes ;
   }

   public void setgxTv_SdtCountry_Isodes( String value )
   {
      gxTv_SdtCountry_Isodes_N = (byte)(0) ;
      gxTv_SdtCountry_Isodes = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Isodes_SetNull( )
   {
      gxTv_SdtCountry_Isodes_N = (byte)(1) ;
      gxTv_SdtCountry_Isodes = "" ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtCountry_Level1( )
   {
      return gxTv_SdtCountry_Level1 ;
   }

   public void setgxTv_SdtCountry_Level1( GxSilentTrnGridCollection value )
   {
      gxTv_SdtCountry_Level1 = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Level1_SetNull( )
   {
      gxTv_SdtCountry_Level1 = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtCountry_Contacttypes( )
   {
      return gxTv_SdtCountry_Contacttypes ;
   }

   public void setgxTv_SdtCountry_Contacttypes( GxSilentTrnGridCollection value )
   {
      gxTv_SdtCountry_Contacttypes = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Contacttypes_SetNull( )
   {
      gxTv_SdtCountry_Contacttypes = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtCountry_Currencies( )
   {
      return gxTv_SdtCountry_Currencies ;
   }

   public void setgxTv_SdtCountry_Currencies( GxSilentTrnGridCollection value )
   {
      gxTv_SdtCountry_Currencies = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_SetNull( )
   {
      gxTv_SdtCountry_Currencies = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public String getgxTv_SdtCountry_Mode( )
   {
      return gxTv_SdtCountry_Mode ;
   }

   public void setgxTv_SdtCountry_Mode( String value )
   {
      gxTv_SdtCountry_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Mode_SetNull( )
   {
      gxTv_SdtCountry_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_Isocod_Z( )
   {
      return gxTv_SdtCountry_Isocod_Z ;
   }

   public void setgxTv_SdtCountry_Isocod_Z( String value )
   {
      gxTv_SdtCountry_Isocod_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Isocod_Z_SetNull( )
   {
      gxTv_SdtCountry_Isocod_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_Isodes_Z( )
   {
      return gxTv_SdtCountry_Isodes_Z ;
   }

   public void setgxTv_SdtCountry_Isodes_Z( String value )
   {
      gxTv_SdtCountry_Isodes_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Isodes_Z_SetNull( )
   {
      gxTv_SdtCountry_Isodes_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtCountry_Isocod_N( )
   {
      return gxTv_SdtCountry_Isocod_N ;
   }

   public void setgxTv_SdtCountry_Isocod_N( byte value )
   {
      gxTv_SdtCountry_Isocod_N = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Isocod_N_SetNull( )
   {
      gxTv_SdtCountry_Isocod_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtCountry_Isodes_N( )
   {
      return gxTv_SdtCountry_Isodes_N ;
   }

   public void setgxTv_SdtCountry_Isodes_N( byte value )
   {
      gxTv_SdtCountry_Isodes_N = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Isodes_N_SetNull( )
   {
      gxTv_SdtCountry_Isodes_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tcountry_bc obj ;
      obj = new tcountry_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtCountry_Isocod = "" ;
      gxTv_SdtCountry_Isodes = "" ;
      gxTv_SdtCountry_Level1 = new GxSilentTrnGridCollection(SdtCountry_CountryParameters.class, "Country.CountryParameters", "IataICSI");
      gxTv_SdtCountry_Contacttypes = new GxSilentTrnGridCollection(SdtCountry_ContactTypes.class, "Country.ContactTypes", "IataICSI");
      gxTv_SdtCountry_Currencies = new GxSilentTrnGridCollection(SdtCountry_Currencies.class, "Country.Currencies", "IataICSI");
      gxTv_SdtCountry_Mode = "" ;
      gxTv_SdtCountry_Isocod_Z = "" ;
      gxTv_SdtCountry_Isodes_Z = "" ;
      gxTv_SdtCountry_Isocod_N = (byte)(0) ;
      gxTv_SdtCountry_Isodes_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtCountry Clone( )
   {
      SdtCountry sdt ;
      tcountry_bc obj ;
      sdt = (SdtCountry)(clone()) ;
      obj = (tcountry_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtCountry struct )
   {
      setgxTv_SdtCountry_Isocod(struct.getIsocod());
      setgxTv_SdtCountry_Isodes(struct.getIsodes());
      setgxTv_SdtCountry_Level1(new GxSilentTrnGridCollection(SdtCountry_CountryParameters.class, "Country.CountryParameters", "IataICSI", struct.getLevel1()));
      setgxTv_SdtCountry_Contacttypes(new GxSilentTrnGridCollection(SdtCountry_ContactTypes.class, "Country.ContactTypes", "IataICSI", struct.getContacttypes()));
      setgxTv_SdtCountry_Currencies(new GxSilentTrnGridCollection(SdtCountry_Currencies.class, "Country.Currencies", "IataICSI", struct.getCurrencies()));
      setgxTv_SdtCountry_Mode(struct.getMode());
      setgxTv_SdtCountry_Isocod_Z(struct.getIsocod_Z());
      setgxTv_SdtCountry_Isodes_Z(struct.getIsodes_Z());
      setgxTv_SdtCountry_Isocod_N(struct.getIsocod_N());
      setgxTv_SdtCountry_Isodes_N(struct.getIsodes_N());
   }

   public StructSdtCountry getStruct( )
   {
      StructSdtCountry struct = new StructSdtCountry ();
      struct.setIsocod(getgxTv_SdtCountry_Isocod());
      struct.setIsodes(getgxTv_SdtCountry_Isodes());
      struct.setLevel1(getgxTv_SdtCountry_Level1().getStruct());
      struct.setContacttypes(getgxTv_SdtCountry_Contacttypes().getStruct());
      struct.setCurrencies(getgxTv_SdtCountry_Currencies().getStruct());
      struct.setMode(getgxTv_SdtCountry_Mode());
      struct.setIsocod_Z(getgxTv_SdtCountry_Isocod_Z());
      struct.setIsodes_Z(getgxTv_SdtCountry_Isodes_Z());
      struct.setIsocod_N(getgxTv_SdtCountry_Isocod_N());
      struct.setIsodes_N(getgxTv_SdtCountry_Isodes_N());
      return struct ;
   }

   protected byte gxTv_SdtCountry_Isocod_N ;
   protected byte gxTv_SdtCountry_Isodes_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtCountry_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtCountry_Isocod ;
   protected String gxTv_SdtCountry_Isodes ;
   protected String gxTv_SdtCountry_Isocod_Z ;
   protected String gxTv_SdtCountry_Isodes_Z ;
   protected GxSilentTrnGridCollection gxTv_SdtCountry_Level1 ;
   protected GxSilentTrnGridCollection gxTv_SdtCountry_Contacttypes ;
   protected GxSilentTrnGridCollection gxTv_SdtCountry_Currencies ;
}


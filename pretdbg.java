/*
               File: RETdbg
        Description: Debug de RET Handling
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pretdbg extends GXProcedure
{
   public pretdbg( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretdbg.class ), "" );
   }

   public pretdbg( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 )
   {
      pretdbg.this.AV9sdb = aP0[0];
      this.aP0 = aP0;
      pretdbg.this.AV10FileNa = aP1[0];
      this.aP1 = aP1;
      pretdbg.this.AV11Versao = aP2[0];
      this.aP2 = aP2;
      pretdbg.this.AV8DebugMo = aP3[0];
      this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( GXutil.strSearch( AV8DebugMo, "DEBUG", 1) > 0 ) )
      {
         AV13Now = GXutil.now(true, false) ;
         /*
            INSERT RECORD ON TABLE SCEVENTS

         */
         A1298SCETk = "0000000000" ;
         n1298SCETk = false ;
         A1299SCEDa = AV13Now ;
         n1299SCEDa = false ;
         A1300SCECR = "DEBG" ;
         n1300SCECR = false ;
         A1301SCEAi = "000" ;
         n1301SCEAi = false ;
         A1303SCERE = AV10FileNa ;
         n1303SCERE = false ;
         A1304SCEAi = "" ;
         n1304SCEAi = false ;
         A1305SCETe = AV9sdb ;
         n1305SCETe = false ;
         A1307SCEIa = "" ;
         n1307SCEIa = false ;
         A1310SCETp = "D" ;
         n1310SCETp = false ;
         A1306SCEAP = "RETH" ;
         n1306SCEAP = false ;
         A1311SCEVe = AV11Versao ;
         n1311SCEVe = false ;
         /* Using cursor P005T2 */
         pr_default.execute(0, new Object[] {new Boolean(n1298SCETk), A1298SCETk, new Boolean(n1299SCEDa), A1299SCEDa, new Boolean(n1300SCECR), A1300SCECR, new Boolean(n1301SCEAi), A1301SCEAi, new Boolean(n1303SCERE), A1303SCERE, new Boolean(n1304SCEAi), A1304SCEAi, new Boolean(n1305SCETe), A1305SCETe, new Boolean(n1306SCEAP), A1306SCEAP, new Boolean(n1307SCEIa), A1307SCEIa, new Boolean(n1310SCETp), A1310SCETp, new Boolean(n1311SCEVe), A1311SCEVe});
         /* Retrieving last key number assigned */
         /* Using cursor P005T3 */
         pr_default.execute(1);
         A1312SCEId = P005T3_A1312SCEId[0] ;
         n1312SCEId = P005T3_n1312SCEId[0] ;
         pr_default.close(1);
         if ( (pr_default.getStatus(0) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         AV12ret = (byte)(1) ;
         Application.commit(context, remoteHandle, "DEFAULT", "pretdbg");
      }
      else
      {
         AV12ret = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV8DebugMo, "VERBOSE", 1) > 0 ) )
      {
         context.msgStatus( "    -> "+AV9sdb );
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pretdbg.this.AV9sdb;
      this.aP1[0] = pretdbg.this.AV10FileNa;
      this.aP2[0] = pretdbg.this.AV11Versao;
      this.aP3[0] = pretdbg.this.AV8DebugMo;
      Application.commit(context, remoteHandle, "DEFAULT", "pretdbg");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV13Now = GXutil.resetTime( GXutil.nullDate() );
      GX_INS248 = 0 ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1303SCERE = "" ;
      n1303SCERE = false ;
      A1304SCEAi = "" ;
      n1304SCEAi = false ;
      A1305SCETe = "" ;
      n1305SCETe = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "" ;
      n1310SCETp = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      A1311SCEVe = "" ;
      n1311SCEVe = false ;
      scmdbuf = "" ;
      P005T3_A1312SCEId = new long[1] ;
      P005T3_n1312SCEId = new boolean[] {false} ;
      A1312SCEId = 0 ;
      n1312SCEId = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV12ret = (byte)(0) ;
      GXt_char1 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretdbg__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            P005T3_A1312SCEId
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV12ret ;
   private short Gx_err ;
   private int GX_INS248 ;
   private long A1312SCEId ;
   private String AV9sdb ;
   private String AV10FileNa ;
   private String AV11Versao ;
   private String AV8DebugMo ;
   private String A1298SCETk ;
   private String A1300SCECR ;
   private String A1301SCEAi ;
   private String A1303SCERE ;
   private String A1304SCEAi ;
   private String A1305SCETe ;
   private String A1307SCEIa ;
   private String A1310SCETp ;
   private String A1306SCEAP ;
   private String A1311SCEVe ;
   private String scmdbuf ;
   private String Gx_emsg ;
   private String GXt_char1 ;
   private java.util.Date AV13Now ;
   private java.util.Date A1299SCEDa ;
   private boolean n1298SCETk ;
   private boolean n1299SCEDa ;
   private boolean n1300SCECR ;
   private boolean n1301SCEAi ;
   private boolean n1303SCERE ;
   private boolean n1304SCEAi ;
   private boolean n1305SCETe ;
   private boolean n1307SCEIa ;
   private boolean n1310SCETp ;
   private boolean n1306SCEAP ;
   private boolean n1311SCEVe ;
   private boolean n1312SCEId ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private IDataStoreProvider pr_default ;
   private long[] P005T3_A1312SCEId ;
   private boolean[] P005T3_n1312SCEId ;
}

final  class pretdbg__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P005T2", "INSERT INTO [SCEVENTS] ([SCETkt], [SCEDate], [SCECRS], [SCEAirLine], [SCERETName], [SCEAirportCode], [SCEText], [SCEAPP], [SCEIata], [SCETpEvento], [SCEVersao], [SCEFopID], [SCEFPAM], [SCEParcela]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005T3", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 5);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 3);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 3);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 150);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 4);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 11);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 3);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[21], 5);
               }
               break;
      }
   }

}


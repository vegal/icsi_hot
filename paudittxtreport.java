/*
               File: AuditTxtReport
        Description: Stub for AuditTxtReport
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:5.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class paudittxtreport extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      paudittxtreport pgm = new paudittxtreport (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String aP0 = "";

      try
      {
         aP0 = (String) args[0];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0);
   }

   public paudittxtreport( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( paudittxtreport.class ), "" );
   }

   public paudittxtreport( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String aP0 )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new aaudittxtreport(remoteHandle, context).execute( aP0 );
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
}


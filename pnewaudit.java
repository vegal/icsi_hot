/*
               File: NewAudit
        Description: New Audit
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pnewaudit extends GXProcedure
{
   public pnewaudit( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pnewaudit.class ), "" );
   }

   public pnewaudit( int remoteHandle ,
                     ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 )
   {
      pnewaudit.this.AV10lgnLog = aP0[0];
      this.aP0 = aP0;
      pnewaudit.this.AV11AuditT = aP1[0];
      this.aP1 = aP1;
      pnewaudit.this.AV12AuditT = aP2[0];
      this.aP2 = aP2;
      pnewaudit.this.AV13AuditT = aP3[0];
      this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      /*
         INSERT RECORD ON TABLE AUDITTRAIL

      */
      A218Auditl = AV10lgnLog ;
      n218Auditl = false ;
      A215AuditT = GXutil.now(true, false) ;
      n215AuditT = false ;
      A212AuditT = AV11AuditT ;
      n212AuditT = false ;
      A213AuditT = AV12AuditT ;
      n213AuditT = false ;
      A217AuditT = AV13AuditT ;
      n217AuditT = false ;
      A864AuditT = GXutil.trim( context.getHttpContext().getRemoteAddr( )) ;
      n864AuditT = false ;
      /* Using cursor P001T2 */
      pr_default.execute(0, new Object[] {new Boolean(n215AuditT), A215AuditT, new Boolean(n212AuditT), A212AuditT, new Boolean(n213AuditT), A213AuditT, new Boolean(n217AuditT), A217AuditT, new Boolean(n218Auditl), A218Auditl, new Boolean(n864AuditT), A864AuditT});
      /* Retrieving last key number assigned */
      /* Using cursor P001T3 */
      pr_default.execute(1);
      A214AuditT = P001T3_A214AuditT[0] ;
      n214AuditT = P001T3_n214AuditT[0] ;
      pr_default.close(1);
      if ( (pr_default.getStatus(0) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pnewaudit.this.AV10lgnLog;
      this.aP1[0] = pnewaudit.this.AV11AuditT;
      this.aP2[0] = pnewaudit.this.AV12AuditT;
      this.aP3[0] = pnewaudit.this.AV13AuditT;
      Application.commit(context, remoteHandle, "DEFAULT", "pnewaudit");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      GX_INS41 = 0 ;
      A218Auditl = "" ;
      n218Auditl = false ;
      A215AuditT = GXutil.resetTime( GXutil.nullDate() );
      n215AuditT = false ;
      A212AuditT = "" ;
      n212AuditT = false ;
      A213AuditT = "" ;
      n213AuditT = false ;
      A217AuditT = "" ;
      n217AuditT = false ;
      A864AuditT = "" ;
      n864AuditT = false ;
      scmdbuf = "" ;
      P001T3_A214AuditT = new long[1] ;
      P001T3_n214AuditT = new boolean[] {false} ;
      A214AuditT = 0 ;
      n214AuditT = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pnewaudit__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            P001T3_A214AuditT
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private int GX_INS41 ;
   private long A214AuditT ;
   private String scmdbuf ;
   private String Gx_emsg ;
   private java.util.Date A215AuditT ;
   private boolean n218Auditl ;
   private boolean n215AuditT ;
   private boolean n212AuditT ;
   private boolean n213AuditT ;
   private boolean n217AuditT ;
   private boolean n864AuditT ;
   private boolean n214AuditT ;
   private String AV10lgnLog ;
   private String AV11AuditT ;
   private String AV12AuditT ;
   private String AV13AuditT ;
   private String A218Auditl ;
   private String A212AuditT ;
   private String A213AuditT ;
   private String A217AuditT ;
   private String A864AuditT ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private IDataStoreProvider pr_default ;
   private long[] P001T3_A214AuditT ;
   private boolean[] P001T3_n214AuditT ;
}

final  class pnewaudit__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P001T2", "INSERT INTO [AUDITTRAIL] ([AuditTrailDate], [AuditTrailType], [AuditTrailSubType], [AuditTrailDesc], [AuditlgnLogin], [AuditTrailIP]) VALUES (?, ?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P001T3", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(1, (java.util.Date)parms[1], false);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 20);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[7], 100);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[9], 20);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(6, (String)parms[11], 30);
               }
               break;
      }
   }

}


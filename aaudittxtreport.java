/*
               File: AuditTxtReport
        Description: Audit Txt Report
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:55:32.25
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class aaudittxtreport extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      aaudittxtreport pgm = new aaudittxtreport (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String aP0 = "";

      try
      {
         aP0 = (String) args[0];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0);
   }

   public aaudittxtreport( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( aaudittxtreport.class ), "" );
   }

   public aaudittxtreport( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String aP0 )
   {
      aaudittxtreport.this.AV10Date = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV11AuditD = GXutil.resetTime( GXutil.nullDate() );
      AV11AuditD = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( AV10Date, 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( AV10Date, 5, 2), ".")), (byte)(GXutil.val( GXutil.substring( AV10Date, 7, 2), ".")), (byte)(0), (byte)(0), (byte)(1)) ;
      GXt_svchar1 = AV13Path ;
      GXv_svchar2[0] = GXt_svchar1 ;
      new pr2getparm(remoteHandle, context).execute( "PATHAUDITTXTRPT", "Caminho grava��o arquivo texto AUDIT", "F", "", GXv_svchar2) ;
      aaudittxtreport.this.GXt_svchar1 = GXv_svchar2[0] ;
      AV13Path = GXt_svchar1 ;
      AV9PathNam = GXutil.trim( AV13Path) + "Audit.txt" ;
      AV8AuditTx.openURL(AV9PathNam);
      /* Using cursor P007F2 */
      pr_default.execute(0, new Object[] {AV11AuditD});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A215AuditT = P007F2_A215AuditT[0] ;
         n215AuditT = P007F2_n215AuditT[0] ;
         A864AuditT = P007F2_A864AuditT[0] ;
         n864AuditT = P007F2_n864AuditT[0] ;
         A218Auditl = P007F2_A218Auditl[0] ;
         n218Auditl = P007F2_n218Auditl[0] ;
         A213AuditT = P007F2_A213AuditT[0] ;
         n213AuditT = P007F2_n213AuditT[0] ;
         A212AuditT = P007F2_A212AuditT[0] ;
         n212AuditT = P007F2_n212AuditT[0] ;
         A217AuditT = P007F2_A217AuditT[0] ;
         n217AuditT = P007F2_n217AuditT[0] ;
         A214AuditT = P007F2_A214AuditT[0] ;
         AV8AuditTx.writeRawText(GXutil.padr( GXutil.str( A214AuditT, 10, 0), (short)(10), " ")+" - "+GXutil.padr( GXutil.trim( A217AuditT), (short)(100), " ")+" - "+GXutil.padr( localUtil.ttoc( A215AuditT, 8, 5, 0, 2, "/", ":", " "), (short)(20), " ")+" - "+GXutil.padr( GXutil.trim( A212AuditT), (short)(20), " ")+" - "+GXutil.trim( A213AuditT)+" - "+GXutil.padr( GXutil.trim( A218Auditl), (short)(20), " ")+" - "+GXutil.padr( GXutil.trim( A864AuditT), (short)(30), " ")+GXutil.newLine( ));
         pr_default.readNext(0);
      }
      pr_default.close(0);
      AV8AuditTx.close();
      cleanup();
   }



   protected void cleanup( )
   {
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV11AuditD = GXutil.resetTime( GXutil.nullDate() );
      AV13Path = "" ;
      GXv_svchar2 = new String [1] ;
      AV9PathNam = "" ;
      AV8AuditTx = new com.genexus.xml.XMLWriter();
      scmdbuf = "" ;
      P007F2_A215AuditT = new java.util.Date[] {GXutil.nullDate()} ;
      P007F2_n215AuditT = new boolean[] {false} ;
      P007F2_A864AuditT = new String[] {""} ;
      P007F2_n864AuditT = new boolean[] {false} ;
      P007F2_A218Auditl = new String[] {""} ;
      P007F2_n218Auditl = new boolean[] {false} ;
      P007F2_A213AuditT = new String[] {""} ;
      P007F2_n213AuditT = new boolean[] {false} ;
      P007F2_A212AuditT = new String[] {""} ;
      P007F2_n212AuditT = new boolean[] {false} ;
      P007F2_A217AuditT = new String[] {""} ;
      P007F2_n217AuditT = new boolean[] {false} ;
      P007F2_A214AuditT = new long[1] ;
      A215AuditT = GXutil.resetTime( GXutil.nullDate() );
      n215AuditT = false ;
      A864AuditT = "" ;
      n864AuditT = false ;
      A218Auditl = "" ;
      n218Auditl = false ;
      A213AuditT = "" ;
      n213AuditT = false ;
      A212AuditT = "" ;
      n212AuditT = false ;
      A217AuditT = "" ;
      n217AuditT = false ;
      A214AuditT = 0 ;
      GXt_svchar1 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new aaudittxtreport__default(),
         new Object[] {
             new Object[] {
            P007F2_A215AuditT, P007F2_n215AuditT, P007F2_A864AuditT, P007F2_n864AuditT, P007F2_A218Auditl, P007F2_n218Auditl, P007F2_A213AuditT, P007F2_n213AuditT, P007F2_A212AuditT, P007F2_n212AuditT,
            P007F2_A217AuditT, P007F2_n217AuditT, P007F2_A214AuditT
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private long A214AuditT ;
   private String AV10Date ;
   private String scmdbuf ;
   private java.util.Date AV11AuditD ;
   private java.util.Date A215AuditT ;
   private boolean n215AuditT ;
   private boolean n864AuditT ;
   private boolean n218Auditl ;
   private boolean n213AuditT ;
   private boolean n212AuditT ;
   private boolean n217AuditT ;
   private String AV13Path ;
   private String GXv_svchar2[] ;
   private String AV9PathNam ;
   private String A864AuditT ;
   private String A218Auditl ;
   private String A213AuditT ;
   private String A212AuditT ;
   private String A217AuditT ;
   private String GXt_svchar1 ;
   private com.genexus.xml.XMLWriter AV8AuditTx ;
   private IDataStoreProvider pr_default ;
   private java.util.Date[] P007F2_A215AuditT ;
   private boolean[] P007F2_n215AuditT ;
   private String[] P007F2_A864AuditT ;
   private boolean[] P007F2_n864AuditT ;
   private String[] P007F2_A218Auditl ;
   private boolean[] P007F2_n218Auditl ;
   private String[] P007F2_A213AuditT ;
   private boolean[] P007F2_n213AuditT ;
   private String[] P007F2_A212AuditT ;
   private boolean[] P007F2_n212AuditT ;
   private String[] P007F2_A217AuditT ;
   private boolean[] P007F2_n217AuditT ;
   private long[] P007F2_A214AuditT ;
}

final  class aaudittxtreport__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007F2", "SELECT [AuditTrailDate], [AuditTrailIP], [AuditlgnLogin], [AuditTrailSubType], [AuditTrailType], [AuditTrailDesc], [AuditTrailID] FROM [AUDITTRAIL] WITH (NOLOCK) WHERE [AuditTrailDate] > ? ORDER BY [AuditTrailID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDateTime(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((long[]) buf[12])[0] = rslt.getLong(7) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setDateTime(1, (java.util.Date)parms[0], false);
               break;
      }
   }

}


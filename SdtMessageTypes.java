import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtMessageTypes extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtMessageTypes( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtMessageTypes.class));
   }

   public SdtMessageTypes( int remoteHandle ,
                           ModelContext context )
   {
      super( context, "SdtMessageTypes");
      initialize( remoteHandle) ;
   }

   public SdtMessageTypes( int remoteHandle ,
                           StructSdtMessageTypes struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV100MsgCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV100MsgCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MSGSubject") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgsubject = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgBody") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgbody = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgStatus") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgstatus = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MSGSubject_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgsubject_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgBody_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgbody_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgStatus_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgstatus_Z = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgDescription_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgdescription_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MSGSubject_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgsubject_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgBody_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgbody_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MsgStatus_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessageTypes_Msgstatus_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "MessageTypes" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("MsgCode", GXutil.rtrim( gxTv_SdtMessageTypes_Msgcode));
      oWriter.writeElement("MsgDescription", GXutil.rtrim( gxTv_SdtMessageTypes_Msgdescription));
      oWriter.writeElement("MSGSubject", GXutil.rtrim( gxTv_SdtMessageTypes_Msgsubject));
      oWriter.writeElement("MsgBody", GXutil.rtrim( gxTv_SdtMessageTypes_Msgbody));
      oWriter.writeElement("MsgStatus", GXutil.trim( GXutil.str( gxTv_SdtMessageTypes_Msgstatus, 1, 0)));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtMessageTypes_Mode));
      oWriter.writeElement("MsgCode_Z", GXutil.rtrim( gxTv_SdtMessageTypes_Msgcode_Z));
      oWriter.writeElement("MsgDescription_Z", GXutil.rtrim( gxTv_SdtMessageTypes_Msgdescription_Z));
      oWriter.writeElement("MSGSubject_Z", GXutil.rtrim( gxTv_SdtMessageTypes_Msgsubject_Z));
      oWriter.writeElement("MsgBody_Z", GXutil.rtrim( gxTv_SdtMessageTypes_Msgbody_Z));
      oWriter.writeElement("MsgStatus_Z", GXutil.trim( GXutil.str( gxTv_SdtMessageTypes_Msgstatus_Z, 1, 0)));
      oWriter.writeElement("MsgDescription_N", GXutil.trim( GXutil.str( gxTv_SdtMessageTypes_Msgdescription_N, 1, 0)));
      oWriter.writeElement("MSGSubject_N", GXutil.trim( GXutil.str( gxTv_SdtMessageTypes_Msgsubject_N, 1, 0)));
      oWriter.writeElement("MsgBody_N", GXutil.trim( GXutil.str( gxTv_SdtMessageTypes_Msgbody_N, 1, 0)));
      oWriter.writeElement("MsgStatus_N", GXutil.trim( GXutil.str( gxTv_SdtMessageTypes_Msgstatus_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgcode( )
   {
      return gxTv_SdtMessageTypes_Msgcode ;
   }

   public void setgxTv_SdtMessageTypes_Msgcode( String value )
   {
      gxTv_SdtMessageTypes_Msgcode = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgcode_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgcode = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgdescription( )
   {
      return gxTv_SdtMessageTypes_Msgdescription ;
   }

   public void setgxTv_SdtMessageTypes_Msgdescription( String value )
   {
      gxTv_SdtMessageTypes_Msgdescription_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgdescription = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgdescription_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgdescription_N = (byte)(1) ;
      gxTv_SdtMessageTypes_Msgdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgsubject( )
   {
      return gxTv_SdtMessageTypes_Msgsubject ;
   }

   public void setgxTv_SdtMessageTypes_Msgsubject( String value )
   {
      gxTv_SdtMessageTypes_Msgsubject_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgsubject = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgsubject_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgsubject_N = (byte)(1) ;
      gxTv_SdtMessageTypes_Msgsubject = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgbody( )
   {
      return gxTv_SdtMessageTypes_Msgbody ;
   }

   public void setgxTv_SdtMessageTypes_Msgbody( String value )
   {
      gxTv_SdtMessageTypes_Msgbody_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgbody = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgbody_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgbody_N = (byte)(1) ;
      gxTv_SdtMessageTypes_Msgbody = "" ;
      return  ;
   }

   public byte getgxTv_SdtMessageTypes_Msgstatus( )
   {
      return gxTv_SdtMessageTypes_Msgstatus ;
   }

   public void setgxTv_SdtMessageTypes_Msgstatus( byte value )
   {
      gxTv_SdtMessageTypes_Msgstatus_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgstatus = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgstatus_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgstatus_N = (byte)(1) ;
      gxTv_SdtMessageTypes_Msgstatus = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Mode( )
   {
      return gxTv_SdtMessageTypes_Mode ;
   }

   public void setgxTv_SdtMessageTypes_Mode( String value )
   {
      gxTv_SdtMessageTypes_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Mode_SetNull( )
   {
      gxTv_SdtMessageTypes_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgcode_Z( )
   {
      return gxTv_SdtMessageTypes_Msgcode_Z ;
   }

   public void setgxTv_SdtMessageTypes_Msgcode_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgcode_Z_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgdescription_Z( )
   {
      return gxTv_SdtMessageTypes_Msgdescription_Z ;
   }

   public void setgxTv_SdtMessageTypes_Msgdescription_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgdescription_Z_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgdescription_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgsubject_Z( )
   {
      return gxTv_SdtMessageTypes_Msgsubject_Z ;
   }

   public void setgxTv_SdtMessageTypes_Msgsubject_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgsubject_Z = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgsubject_Z_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgsubject_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtMessageTypes_Msgbody_Z( )
   {
      return gxTv_SdtMessageTypes_Msgbody_Z ;
   }

   public void setgxTv_SdtMessageTypes_Msgbody_Z( String value )
   {
      gxTv_SdtMessageTypes_Msgbody_Z = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgbody_Z_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgbody_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtMessageTypes_Msgstatus_Z( )
   {
      return gxTv_SdtMessageTypes_Msgstatus_Z ;
   }

   public void setgxTv_SdtMessageTypes_Msgstatus_Z( byte value )
   {
      gxTv_SdtMessageTypes_Msgstatus_Z = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgstatus_Z_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgstatus_Z = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtMessageTypes_Msgdescription_N( )
   {
      return gxTv_SdtMessageTypes_Msgdescription_N ;
   }

   public void setgxTv_SdtMessageTypes_Msgdescription_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgdescription_N = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgdescription_N_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgdescription_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtMessageTypes_Msgsubject_N( )
   {
      return gxTv_SdtMessageTypes_Msgsubject_N ;
   }

   public void setgxTv_SdtMessageTypes_Msgsubject_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgsubject_N = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgsubject_N_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgsubject_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtMessageTypes_Msgbody_N( )
   {
      return gxTv_SdtMessageTypes_Msgbody_N ;
   }

   public void setgxTv_SdtMessageTypes_Msgbody_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgbody_N = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgbody_N_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgbody_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtMessageTypes_Msgstatus_N( )
   {
      return gxTv_SdtMessageTypes_Msgstatus_N ;
   }

   public void setgxTv_SdtMessageTypes_Msgstatus_N( byte value )
   {
      gxTv_SdtMessageTypes_Msgstatus_N = value ;
      return  ;
   }

   public void setgxTv_SdtMessageTypes_Msgstatus_N_SetNull( )
   {
      gxTv_SdtMessageTypes_Msgstatus_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tmessagetypes_bc obj ;
      obj = new tmessagetypes_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtMessageTypes_Msgcode = "" ;
      gxTv_SdtMessageTypes_Msgdescription = "" ;
      gxTv_SdtMessageTypes_Msgsubject = "" ;
      gxTv_SdtMessageTypes_Msgbody = "" ;
      gxTv_SdtMessageTypes_Msgstatus = (byte)(0) ;
      gxTv_SdtMessageTypes_Mode = "" ;
      gxTv_SdtMessageTypes_Msgcode_Z = "" ;
      gxTv_SdtMessageTypes_Msgdescription_Z = "" ;
      gxTv_SdtMessageTypes_Msgsubject_Z = "" ;
      gxTv_SdtMessageTypes_Msgbody_Z = "" ;
      gxTv_SdtMessageTypes_Msgstatus_Z = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgdescription_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgsubject_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgbody_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgstatus_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtMessageTypes Clone( )
   {
      SdtMessageTypes sdt ;
      tmessagetypes_bc obj ;
      sdt = (SdtMessageTypes)(clone()) ;
      obj = (tmessagetypes_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtMessageTypes struct )
   {
      setgxTv_SdtMessageTypes_Msgcode(struct.getMsgcode());
      setgxTv_SdtMessageTypes_Msgdescription(struct.getMsgdescription());
      setgxTv_SdtMessageTypes_Msgsubject(struct.getMsgsubject());
      setgxTv_SdtMessageTypes_Msgbody(struct.getMsgbody());
      setgxTv_SdtMessageTypes_Msgstatus(struct.getMsgstatus());
      setgxTv_SdtMessageTypes_Mode(struct.getMode());
      setgxTv_SdtMessageTypes_Msgcode_Z(struct.getMsgcode_Z());
      setgxTv_SdtMessageTypes_Msgdescription_Z(struct.getMsgdescription_Z());
      setgxTv_SdtMessageTypes_Msgsubject_Z(struct.getMsgsubject_Z());
      setgxTv_SdtMessageTypes_Msgbody_Z(struct.getMsgbody_Z());
      setgxTv_SdtMessageTypes_Msgstatus_Z(struct.getMsgstatus_Z());
      setgxTv_SdtMessageTypes_Msgdescription_N(struct.getMsgdescription_N());
      setgxTv_SdtMessageTypes_Msgsubject_N(struct.getMsgsubject_N());
      setgxTv_SdtMessageTypes_Msgbody_N(struct.getMsgbody_N());
      setgxTv_SdtMessageTypes_Msgstatus_N(struct.getMsgstatus_N());
   }

   public StructSdtMessageTypes getStruct( )
   {
      StructSdtMessageTypes struct = new StructSdtMessageTypes ();
      struct.setMsgcode(getgxTv_SdtMessageTypes_Msgcode());
      struct.setMsgdescription(getgxTv_SdtMessageTypes_Msgdescription());
      struct.setMsgsubject(getgxTv_SdtMessageTypes_Msgsubject());
      struct.setMsgbody(getgxTv_SdtMessageTypes_Msgbody());
      struct.setMsgstatus(getgxTv_SdtMessageTypes_Msgstatus());
      struct.setMode(getgxTv_SdtMessageTypes_Mode());
      struct.setMsgcode_Z(getgxTv_SdtMessageTypes_Msgcode_Z());
      struct.setMsgdescription_Z(getgxTv_SdtMessageTypes_Msgdescription_Z());
      struct.setMsgsubject_Z(getgxTv_SdtMessageTypes_Msgsubject_Z());
      struct.setMsgbody_Z(getgxTv_SdtMessageTypes_Msgbody_Z());
      struct.setMsgstatus_Z(getgxTv_SdtMessageTypes_Msgstatus_Z());
      struct.setMsgdescription_N(getgxTv_SdtMessageTypes_Msgdescription_N());
      struct.setMsgsubject_N(getgxTv_SdtMessageTypes_Msgsubject_N());
      struct.setMsgbody_N(getgxTv_SdtMessageTypes_Msgbody_N());
      struct.setMsgstatus_N(getgxTv_SdtMessageTypes_Msgstatus_N());
      return struct ;
   }

   protected byte gxTv_SdtMessageTypes_Msgstatus ;
   protected byte gxTv_SdtMessageTypes_Msgstatus_Z ;
   protected byte gxTv_SdtMessageTypes_Msgdescription_N ;
   protected byte gxTv_SdtMessageTypes_Msgsubject_N ;
   protected byte gxTv_SdtMessageTypes_Msgbody_N ;
   protected byte gxTv_SdtMessageTypes_Msgstatus_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtMessageTypes_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtMessageTypes_Msgbody ;
   protected String gxTv_SdtMessageTypes_Msgbody_Z ;
   protected String gxTv_SdtMessageTypes_Msgcode ;
   protected String gxTv_SdtMessageTypes_Msgdescription ;
   protected String gxTv_SdtMessageTypes_Msgsubject ;
   protected String gxTv_SdtMessageTypes_Msgcode_Z ;
   protected String gxTv_SdtMessageTypes_Msgdescription_Z ;
   protected String gxTv_SdtMessageTypes_Msgsubject_Z ;
}


/*
               File: AirlineCttSeq
        Description: Airline Ctt Seq
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:52.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pairlinecttseq extends GXProcedure
{
   public pairlinecttseq( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pairlinecttseq.class ), "" );
   }

   public pairlinecttseq( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String aP2 ,
                        short[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String aP2 ,
                             short[] aP3 )
   {
      pairlinecttseq.this.AV8ISOCod = aP0;
      pairlinecttseq.this.AV13AirLin = aP1;
      pairlinecttseq.this.AV11Contac = aP2;
      pairlinecttseq.this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV16GXLvl1 = (byte)(0) ;
      /* Using cursor P005O2 */
      pr_default.execute(0, new Object[] {AV8ISOCod, AV13AirLin, AV11Contac});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A23ISOCod = P005O2_A23ISOCod[0] ;
         A1136AirLi = P005O2_A1136AirLi[0] ;
         A365Contac = P005O2_A365Contac[0] ;
         A1138AirLi = P005O2_A1138AirLi[0] ;
         AV16GXLvl1 = (byte)(1) ;
         AV12AirLin = (short)(A1138AirLi+1) ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( ( AV16GXLvl1 == 0 ) )
      {
         AV12AirLin = (short)(1) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP3[0] = pairlinecttseq.this.AV12AirLin;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV12AirLin = (short)(0) ;
      AV16GXLvl1 = (byte)(0) ;
      scmdbuf = "" ;
      P005O2_A23ISOCod = new String[] {""} ;
      P005O2_A1136AirLi = new String[] {""} ;
      P005O2_A365Contac = new String[] {""} ;
      P005O2_A1138AirLi = new short[1] ;
      A23ISOCod = "" ;
      A1136AirLi = "" ;
      A365Contac = "" ;
      A1138AirLi = (short)(0) ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pairlinecttseq__default(),
         new Object[] {
             new Object[] {
            P005O2_A23ISOCod, P005O2_A1136AirLi, P005O2_A365Contac, P005O2_A1138AirLi
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV16GXLvl1 ;
   private short AV12AirLin ;
   private short A1138AirLi ;
   private short Gx_err ;
   private String scmdbuf ;
   private String AV8ISOCod ;
   private String AV13AirLin ;
   private String AV11Contac ;
   private String A23ISOCod ;
   private String A1136AirLi ;
   private String A365Contac ;
   private short[] aP3 ;
   private IDataStoreProvider pr_default ;
   private String[] P005O2_A23ISOCod ;
   private String[] P005O2_A1136AirLi ;
   private String[] P005O2_A365Contac ;
   private short[] P005O2_A1138AirLi ;
}

final  class pairlinecttseq__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P005O2", "SELECT TOP 1 [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE ([ISOCod] = ?) AND ([AirLineCode] = ?) AND ([ContactTypesCode] = ?) ORDER BY [AirLineCttSeq] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3);
               stmt.setVarchar(2, (String)parms[1], 20);
               stmt.setVarchar(3, (String)parms[2], 5);
               break;
      }
   }

}


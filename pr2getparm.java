/*
               File: R2GetParm
        Description: R2 Get Parm
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:1.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2getparm extends GXProcedure
{
   public pr2getparm( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2getparm.class ), "" );
   }

   public pr2getparm( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String aP2 ,
                        String aP3 ,
                        String[] aP4 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String aP2 ,
                             String aP3 ,
                             String[] aP4 )
   {
      pr2getparm.this.AV12Config = aP0;
      pr2getparm.this.AV13Config = aP1;
      pr2getparm.this.AV9ConfigT = aP2;
      pr2getparm.this.AV10Config = aP3;
      pr2getparm.this.aP4 = aP4;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV8ConfigI = GXutil.left( AV12Config, 20) ;
      AV17GXLvl2 = (byte)(0) ;
      /* Using cursor P000O2 */
      pr_default.execute(0, new Object[] {AV8ConfigI});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A19ConfigI = P000O2_A19ConfigI[0] ;
         A17ConfigV = P000O2_A17ConfigV[0] ;
         AV17GXLvl2 = (byte)(1) ;
         AV11Config = A17ConfigV ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(0);
      if ( ( AV17GXLvl2 == 0 ) )
      {
         /*
            INSERT RECORD ON TABLE CONFIG

         */
         A19ConfigI = AV8ConfigI ;
         A17ConfigV = AV10Config ;
         A18ConfigD = AV13Config ;
         /* Using cursor P000O3 */
         pr_default.execute(1, new Object[] {A19ConfigI, A17ConfigV, A18ConfigD});
         if ( (pr_default.getStatus(1) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         AV11Config = AV10Config ;
      }
      if ( ( GXutil.strcmp(AV9ConfigT, "B") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.left( GXutil.upper( AV11Config), 1), "Y") == 0 ) || ( GXutil.strcmp(GXutil.left( GXutil.upper( AV11Config), 1), "S") == 0 ) )
         {
            AV11Config = "Y" ;
         }
         else
         {
            AV11Config = "N" ;
         }
      }
      else if ( ( GXutil.strcmp(AV9ConfigT, "F") == 0 ) )
      {
         AV11Config = GXutil.trim( AV11Config) ;
         if ( ( GXutil.strcmp(GXutil.right( AV11Config, 1), "\\") != 0 ) )
         {
            AV11Config = AV11Config + "\\" ;
         }
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP4[0] = pr2getparm.this.AV11Config;
      Application.commit(context, remoteHandle, "DEFAULT", "pr2getparm");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV11Config = "" ;
      AV8ConfigI = "" ;
      AV17GXLvl2 = (byte)(0) ;
      scmdbuf = "" ;
      P000O2_A19ConfigI = new String[] {""} ;
      P000O2_A17ConfigV = new String[] {""} ;
      A19ConfigI = "" ;
      A17ConfigV = "" ;
      GX_INS3 = 0 ;
      A18ConfigD = "" ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pr2getparm__default(),
         new Object[] {
             new Object[] {
            P000O2_A19ConfigI, P000O2_A17ConfigV
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV17GXLvl2 ;
   private short Gx_err ;
   private int GX_INS3 ;
   private String AV9ConfigT ;
   private String AV10Config ;
   private String scmdbuf ;
   private String Gx_emsg ;
   private String AV12Config ;
   private String AV13Config ;
   private String AV11Config ;
   private String AV8ConfigI ;
   private String A19ConfigI ;
   private String A17ConfigV ;
   private String A18ConfigD ;
   private String[] aP4 ;
   private IDataStoreProvider pr_default ;
   private String[] P000O2_A19ConfigI ;
   private String[] P000O2_A17ConfigV ;
}

final  class pr2getparm__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P000O2", "SELECT [ConfigID], [ConfigValue] FROM [CONFIG] WITH (NOLOCK) WHERE [ConfigID] = ? ORDER BY [ConfigID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P000O3", "INSERT INTO [CONFIG] ([ConfigID], [ConfigValue], [ConfigDes]) VALUES (?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 255, false);
               stmt.setVarchar(3, (String)parms[2], 100, false);
               break;
      }
   }

}


/*
               File: sanitycheckCielo
        Description: sanitycheck Cielo
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:23.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class psanitycheckcielo extends GXProcedure
{
   public psanitycheckcielo( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psanitycheckcielo.class ), "" );
   }

   public psanitycheckcielo( int remoteHandle ,
                             ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String[] aP1 )
   {
      psanitycheckcielo.this.AV24FileNa = aP0;
      psanitycheckcielo.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV26Caminh = AV24FileNa ;
      AV18File.setSource( AV26Caminh );
      if ( AV18File.exists() )
      {
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfropen( AV26Caminh, 255, "%", "\"", "") ;
         AV28HotCou = 0 ;
         AV10ErroHe = "Header Not Found." ;
         AV11ErroTr = "Trailer Not Found." ;
         while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
         {
            GXv_char1[0] = AV9linha ;
            GXt_int2 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char1, (short)(255)) ;
            AV9linha = GXv_char1[0] ;
            AV27RetVal = GXt_int2 ;
            AV28HotCou = (long)(AV28HotCou+1) ;
            if ( ( AV28HotCou == 1 ) )
            {
               /* Execute user subroutine: S1178 */
               S1178 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               AV10ErroHe = AV15Erro ;
            }
         }
         if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV9linha))==0)) )
         {
            /* Execute user subroutine: S12119 */
            S12119 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            AV11ErroTr = AV15Erro ;
         }
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         AV25msgErr = "" ;
         if ( ! ( ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV11ErroTr))==0)) ) )
         {
            AV25msgErr = AV10ErroHe + "=ErrHeader / ErrTrailer=" + AV11ErroTr ;
            GXt_svchar3 = AV29ShortN ;
            GXv_char1[0] = AV26Caminh ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2shortname(remoteHandle, context).execute( GXv_char1, GXv_char4) ;
            psanitycheckcielo.this.AV26Caminh = GXv_char1[0] ;
            psanitycheckcielo.this.GXt_svchar3 = GXv_char4[0] ;
            AV29ShortN = GXt_svchar3 ;
            GXt_svchar3 = AV12BasePa ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getpath(remoteHandle, context).execute( AV26Caminh, GXv_char4) ;
            psanitycheckcielo.this.GXt_svchar3 = GXv_char4[0] ;
            AV12BasePa = GXt_svchar3 ;
            AV30SubFol = "Error\\" ;
            AV12BasePa = AV12BasePa + AV30SubFol ;
            AV19NewFil = GXutil.trim( AV12BasePa) + GXutil.trim( AV29ShortN) ;
            AV18File.setSource( AV26Caminh );
            AV18File.rename(GXutil.trim( AV19NewFil));
            AV14ShortN = GXutil.trim( AV29ShortN) + ".err" ;
            AV31Arquiv = GXutil.trim( AV12BasePa) + GXutil.trim( AV14ShortN) ;
            AV13LOGFil.openURL(AV31Arquiv);
            AV10ErroHe = AV10ErroHe + GXutil.newLine( ) ;
            AV11ErroTr = AV11ErroTr + GXutil.newLine( ) ;
            AV13LOGFil.writeRawText(AV10ErroHe);
            AV13LOGFil.writeRawText(AV11ErroTr);
            AV13LOGFil.close();
            GXt_svchar3 = AV23Subjec ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_SUBJ_VI", "Mensagem no subject do e-mail do tipo Sanity VI", "S", "Problem submission file VI", GXv_char4) ;
            psanitycheckcielo.this.GXt_svchar3 = GXv_char4[0] ;
            AV23Subjec = GXt_svchar3 ;
            GXt_svchar3 = AV21Body ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_BODY_VI", "Mensagem no corpo do e-mail do tipo Sanity VI", "S", "<br>The submission file [FILE] related to VI was rejected in the Sanity Check.<br><br>Please check the file.<br><br>Thanks", GXv_char4) ;
            psanitycheckcielo.this.GXt_svchar3 = GXv_char4[0] ;
            AV21Body = GXt_svchar3 ;
            AV21Body = GXutil.strReplace( AV21Body, "[FILE]", AV29ShortN) ;
            GXt_svchar3 = AV40To ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "", "S", "", GXv_char4) ;
            psanitycheckcielo.this.GXt_svchar3 = GXv_char4[0] ;
            AV40To = GXutil.trim( GXt_svchar3) ;
            GXt_svchar3 = AV22CC ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "", "S", "", GXv_char4) ;
            psanitycheckcielo.this.GXt_svchar3 = GXv_char4[0] ;
            AV22CC = GXutil.trim( GXt_svchar3) ;
            AV20BCC = "" ;
            GX_I = 1 ;
            while ( ( GX_I <= 5 ) )
            {
               AV8Anexos[GX_I-1] = "" ;
               GX_I = (int)(GX_I+1) ;
            }
            new penviaemail(remoteHandle, context).execute( AV23Subjec, AV21Body, AV40To, AV22CC, AV20BCC, AV8Anexos) ;
         }
      }
      cleanup();
   }

   public void S1178( )
   {
      /* 'CHECKHEADER' Routine */
      AV15Erro = "" ;
      AV17LinhaA = GXutil.strReplace( AV9linha, " ", "x") ;
      if ( ( GXutil.len( GXutil.trim( AV17LinhaA)) != 250 ) )
      {
         AV15Erro = "Header size record Not equal 250 / " ;
      }
      AV32yyyy = GXutil.substring( AV9linha, 7, 4) ;
      AV33mm = GXutil.substring( AV9linha, 5, 2) ;
      AV34dd = GXutil.substring( AV9linha, 3, 2) ;
      AV35Nyyyy = (int)(GXutil.val( AV32yyyy, ".")) ;
      AV36Nmm = (int)(GXutil.val( AV33mm, ".")) ;
      AV37Ndd = (int)(GXutil.val( AV34dd, ".")) ;
      AV16DataGe = localUtil.ymdtod( AV35Nyyyy, AV36Nmm, AV37Ndd) ;
      if ( (GXutil.nullDate().equals(AV16DataGe)) )
      {
         AV15Erro = AV15Erro + "Header Date Not Valid / " ;
      }
      AV38Sequen = GXutil.substring( AV9linha, 11, 7) ;
      if ( ( GXutil.val( AV38Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Sequential file Not Valid / " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 31, 10) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Merchant Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "00") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Identifier Not equal 00 " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 28, 3), "000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Information Not equal 000 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 41, 5), "986PV") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Information Not equal 986PV / " ;
      }
   }

   public void S12119( )
   {
      /* 'CHECKTRAILER' Routine */
      AV15Erro = "" ;
      AV17LinhaA = GXutil.strReplace( AV9linha, " ", "x") ;
      if ( ( GXutil.len( GXutil.trim( AV17LinhaA)) != 250 ) )
      {
         AV15Erro = "Trailer size record Not equal 250 / " ;
      }
      AV38Sequen = GXutil.substring( AV9linha, 3, 7) ;
      if ( ( GXutil.val( AV38Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer total of lines Not Valid / " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 10, 15) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer total of sales Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "99") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Identifier Not equal 99 " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 25, 44), "00000000000000000000000000000000000000000000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Information Not equal 00000000000000000000000000000000000000000000 / " ;
      }
   }

   protected void cleanup( )
   {
      this.aP1[0] = psanitycheckcielo.this.AV25msgErr;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25msgErr = "" ;
      AV26Caminh = "" ;
      AV18File = new com.genexus.util.GXFile();
      AV27RetVal = 0 ;
      AV28HotCou = 0 ;
      AV10ErroHe = "" ;
      AV11ErroTr = "" ;
      AV9linha = "" ;
      GXt_int2 = (short)(0) ;
      returnInSub = false ;
      AV15Erro = "" ;
      AV29ShortN = "" ;
      GXv_char1 = new String [1] ;
      AV12BasePa = "" ;
      AV30SubFol = "" ;
      AV19NewFil = "" ;
      AV14ShortN = "" ;
      AV31Arquiv = "" ;
      AV13LOGFil = new com.genexus.xml.XMLWriter();
      AV23Subjec = "" ;
      AV21Body = "" ;
      AV40To = "" ;
      AV22CC = "" ;
      GXt_svchar3 = "" ;
      GXv_char4 = new String [1] ;
      AV20BCC = "" ;
      GX_I = 0 ;
      AV8Anexos = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV8Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV17LinhaA = "" ;
      AV32yyyy = "" ;
      AV33mm = "" ;
      AV34dd = "" ;
      AV35Nyyyy = 0 ;
      AV36Nmm = 0 ;
      AV37Ndd = 0 ;
      AV16DataGe = GXutil.nullDate() ;
      AV38Sequen = "" ;
      AV39Sequen = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short GXt_int2 ;
   private short Gx_err ;
   private int GX_I ;
   private int AV35Nyyyy ;
   private int AV36Nmm ;
   private int AV37Ndd ;
   private long AV27RetVal ;
   private long AV28HotCou ;
   private String GXv_char1[] ;
   private String AV12BasePa ;
   private String GXv_char4[] ;
   private String AV32yyyy ;
   private String AV33mm ;
   private String AV34dd ;
   private java.util.Date AV16DataGe ;
   private boolean returnInSub ;
   private String AV24FileNa ;
   private String AV25msgErr ;
   private String AV26Caminh ;
   private String AV10ErroHe ;
   private String AV11ErroTr ;
   private String AV9linha ;
   private String AV15Erro ;
   private String AV29ShortN ;
   private String AV30SubFol ;
   private String AV19NewFil ;
   private String AV14ShortN ;
   private String AV31Arquiv ;
   private String AV23Subjec ;
   private String AV21Body ;
   private String AV40To ;
   private String AV22CC ;
   private String GXt_svchar3 ;
   private String AV20BCC ;
   private String AV8Anexos[] ;
   private String AV17LinhaA ;
   private String AV38Sequen ;
   private String AV39Sequen ;
   private com.genexus.xml.XMLWriter AV13LOGFil ;
   private com.genexus.util.GXFile AV18File ;
   private String[] aP1 ;
}


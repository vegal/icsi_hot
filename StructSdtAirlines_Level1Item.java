
public final  class StructSdtAirlines_Level1Item implements Cloneable, java.io.Serializable
{
   public StructSdtAirlines_Level1Item( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinedatabank = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans = "" ;
      gxTv_SdtAirlines_Level1Item_Mode = "" ;
      gxTv_SdtAirlines_Level1Item_Modified = (short)(0) ;
      gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getAirlinecurcode( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurcode ;
   }

   public void setAirlinecurcode( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode = value ;
      return  ;
   }

   public String getAirlinedatabank( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinedatabank ;
   }

   public void setAirlinedatabank( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinedatabank = value ;
      return  ;
   }

   public String getAirlinecurtrans( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurtrans ;
   }

   public void setAirlinecurtrans( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtAirlines_Level1Item_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtAirlines_Level1Item_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtAirlines_Level1Item_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtAirlines_Level1Item_Modified = value ;
      return  ;
   }

   public String getAirlinecurcode_Z( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z ;
   }

   public void setAirlinecurcode_Z( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = value ;
      return  ;
   }

   public String getAirlinedatabank_Z( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z ;
   }

   public void setAirlinedatabank_Z( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = value ;
      return  ;
   }

   public String getAirlinecurtrans_Z( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z ;
   }

   public void setAirlinecurtrans_Z( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = value ;
      return  ;
   }

   protected short gxTv_SdtAirlines_Level1Item_Modified ;
   protected String gxTv_SdtAirlines_Level1Item_Mode ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinedatabank ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurcode ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurtrans ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z ;
}


/*
               File: R1085
        Description: Gera��o de Arquivo 1085
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:0.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pr1085 extends GXProcedure
{
   public pr1085( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr1085.class ), "" );
   }

   public pr1085( int remoteHandle ,
                  ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV8lccbSub = GXutil.resetTime(GXutil.now(true, false)) ;
      AV19Sequen = 0 ;
      AV45Now = GXutil.resetTime(GXutil.now(true, false)) ;
      GXt_char1 = AV46Path ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_R1085", "Caminho dos arquivos R1085", "F", "C:\\TEMP\\", GXv_svchar2) ;
      pr1085.this.GXt_char1 = GXv_svchar2[0] ;
      AV46Path = GXt_char1 ;
      /* Using cursor P006W2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1147lccbE = P006W2_A1147lccbE[0] ;
         n1147lccbE = P006W2_n1147lccbE[0] ;
         A1150lccbE = P006W2_A1150lccbE[0] ;
         AV53Lccbem = A1150lccbE ;
         AV44FileNa = GXutil.trim( AV46Path) + "bspR1085.AXBRL." + GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV45Now), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV45Now), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV45Now), 10, 0)), (short)(2), "0") + GXutil.trim( A1150lccbE) + ".txt" ;
         AV39xmlWri.openURL(AV44FileNa);
         /* Execute user subroutine: S1117 */
         S1117 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            cleanup();
            if (true) return;
         }
         AV39xmlWri.close();
         pr_default.readNext(0);
      }
      pr_default.close(0);
      cleanup();
   }

   public void S1117( )
   {
      /* 'GERAARQUIVO' Routine */
      /* Using cursor P006W3 */
      pr_default.execute(1, new Object[] {AV53Lccbem, AV8lccbSub});
      while ( (pr_default.getStatus(1) != 101) )
      {
         brk6W3 = false ;
         A1227lccbO = P006W3_A1227lccbO[0] ;
         A1189lccbS = P006W3_A1189lccbS[0] ;
         n1189lccbS = P006W3_n1189lccbS[0] ;
         A1224lccbC = P006W3_A1224lccbC[0] ;
         A1150lccbE = P006W3_A1150lccbE[0] ;
         A1184lccbS = P006W3_A1184lccbS[0] ;
         n1184lccbS = P006W3_n1184lccbS[0] ;
         A1228lccbF = P006W3_A1228lccbF[0] ;
         A1226lccbA = P006W3_A1226lccbA[0] ;
         A1225lccbC = P006W3_A1225lccbC[0] ;
         A1223lccbD = P006W3_A1223lccbD[0] ;
         A1222lccbI = P006W3_A1222lccbI[0] ;
         A1231lccbT = P006W3_A1231lccbT[0] ;
         A1232lccbT = P006W3_A1232lccbT[0] ;
         A1172lccbS = P006W3_A1172lccbS[0] ;
         n1172lccbS = P006W3_n1172lccbS[0] ;
         A1169lccbD = P006W3_A1169lccbD[0] ;
         n1169lccbD = P006W3_n1169lccbD[0] ;
         A1170lccbI = P006W3_A1170lccbI[0] ;
         n1170lccbI = P006W3_n1170lccbI[0] ;
         A1168lccbI = P006W3_A1168lccbI[0] ;
         n1168lccbI = P006W3_n1168lccbI[0] ;
         A1171lccbT = P006W3_A1171lccbT[0] ;
         n1171lccbT = P006W3_n1171lccbT[0] ;
         A1179lccbV = P006W3_A1179lccbV[0] ;
         n1179lccbV = P006W3_n1179lccbV[0] ;
         A1163lccbP = P006W3_A1163lccbP[0] ;
         n1163lccbP = P006W3_n1163lccbP[0] ;
         A1189lccbS = P006W3_A1189lccbS[0] ;
         n1189lccbS = P006W3_n1189lccbS[0] ;
         A1184lccbS = P006W3_A1184lccbS[0] ;
         n1184lccbS = P006W3_n1184lccbS[0] ;
         A1172lccbS = P006W3_A1172lccbS[0] ;
         n1172lccbS = P006W3_n1172lccbS[0] ;
         A1169lccbD = P006W3_A1169lccbD[0] ;
         n1169lccbD = P006W3_n1169lccbD[0] ;
         A1170lccbI = P006W3_A1170lccbI[0] ;
         n1170lccbI = P006W3_n1170lccbI[0] ;
         A1168lccbI = P006W3_A1168lccbI[0] ;
         n1168lccbI = P006W3_n1168lccbI[0] ;
         A1171lccbT = P006W3_A1171lccbT[0] ;
         n1171lccbT = P006W3_n1171lccbT[0] ;
         A1179lccbV = P006W3_A1179lccbV[0] ;
         n1179lccbV = P006W3_n1179lccbV[0] ;
         A1163lccbP = P006W3_A1163lccbP[0] ;
         n1163lccbP = P006W3_n1163lccbP[0] ;
         AV19Sequen = (int)(AV19Sequen+1) ;
         AV9lccbTDN = A1231lccbT ;
         AV11lccbTR = A1232lccbT ;
         AV24SalesA = GXutil.strReplace( GXutil.trim( GXutil.str( A1172lccbS, 14, 2)), ".", "") ;
         AV29DownPa = GXutil.strReplace( GXutil.trim( GXutil.str( A1169lccbD, 14, 2)), ".", "") ;
         AV30lccbIn = GXutil.strReplace( GXutil.trim( GXutil.str( A1170lccbI, 14, 2)), ".", "") ;
         AV37NumPar = ((A1168lccbI==1) ? "0" : GXutil.trim( GXutil.str( A1168lccbI, 10, 0))) ;
         AV12lccbDa = A1223lccbD ;
         AV13IssueD = GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV12lccbDa), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV12lccbDa), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV12lccbDa), 10, 0)), (short)(2), "0") ;
         GXt_int3 = AV48Week ;
         GXv_int4[0] = GXt_int3 ;
         new pgetweekofmonth(remoteHandle, context).execute( AV12lccbDa, GXv_int4) ;
         pr1085.this.GXt_int3 = GXv_int4[0] ;
         AV48Week = GXt_int3 ;
         AV14Billin = GXutil.trim( GXutil.str( AV48Week, 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV12lccbDa), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.year( AV12lccbDa), 10, 0)), (short)(4), "0") ;
         GXt_int5 = AV47Julian ;
         GXv_int6[0] = GXt_int5 ;
         new pgetjuliandate(remoteHandle, context).execute( AV12lccbDa, GXv_int6) ;
         pr1085.this.GXt_int5 = GXv_int6[0] ;
         AV47Julian = GXt_int5 ;
         AV16InvRef = GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV12lccbDa), 10, 0)), 3, 2) + GXutil.padl( GXutil.str( AV47Julian, 10, 0), (short)(3), "0") + "1" + GXutil.trim( A1224lccbC) ;
         AV35InvNum = GXutil.trim( A1224lccbC) + GXutil.trim( A1150lccbE) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV45Now), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV45Now), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV45Now), 10, 0)), (short)(2), "0") + GXutil.trim( A1150lccbE) ;
         AV21InvDat = GXutil.resetTime(GXutil.now(true, false)) ;
         AV22InvDat = GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV21InvDat), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV21InvDat), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV21InvDat), 10, 0)), (short)(2), "0") ;
         AV49lccbIn = A1168lccbI ;
         /* Using cursor P006W4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1488ICSI_ = P006W4_A1488ICSI_[0] ;
            A1487ICSI_ = P006W4_A1487ICSI_[0] ;
            A1480ICSI_ = P006W4_A1480ICSI_[0] ;
            n1480ICSI_ = P006W4_n1480ICSI_[0] ;
            A1481ICSI_ = P006W4_A1481ICSI_[0] ;
            n1481ICSI_ = P006W4_n1481ICSI_[0] ;
            if ( ( AV49lccbIn <= 1 ) )
            {
               AV36Maquin = A1480ICSI_ ;
            }
            else
            {
               AV36Maquin = A1481ICSI_ ;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Execute user subroutine: S123 */
         S123 ();
         if ( returnInSub )
         {
            pr_default.close(1);
            pr_default.close(1);
            returnInSub = true;
            if (true) return;
         }
         AV20FPAMAu = A1172lccbS ;
         AV17FPAM = GXutil.strReplace( GXutil.trim( GXutil.str( AV20FPAMAu, 10, 2)), ".", "") ;
         AV32DownTa = (double)(A1169lccbD+A1171lccbT) ;
         AV33DownTa = GXutil.strReplace( GXutil.trim( GXutil.str( AV32DownTa, 12, 2)), ".", "") ;
         AV42Valida = GXutil.trim( A1179lccbV) ;
         AV10Linha = "11" + GXutil.padl( GXutil.trim( A1150lccbE), (short)(3), "0") + GXutil.padl( GXutil.trim( AV9lccbTDN), (short)(10), "0") + GXutil.padl( GXutil.trim( A1222lccbI), (short)(7), "0") + GXutil.padl( GXutil.trim( AV13IssueD), (short)(6), "0") + GXutil.padl( GXutil.trim( AV14Billin), (short)(7), "0") + GXutil.padl( GXutil.trim( AV16InvRef), (short)(8), "0") ;
         AV10Linha = AV10Linha + GXutil.padr( GXutil.trim( AV15PAX_NA), (short)(25), " ") + GXutil.padr( GXutil.trim( A1224lccbC), (short)(2), " ") + GXutil.padr( GXutil.trim( A1225lccbC), (short)(16), " ") + "A" + GXutil.space( (short)(11)) + GXutil.padl( GXutil.trim( AV17FPAM), (short)(11), "0") + GXutil.padr( GXutil.trim( A1226lccbA), (short)(10), " ") + GXutil.padl( GXutil.trim( GXutil.str( AV19Sequen, 10, 0)), (short)(8), "0") ;
         AV10Linha = AV10Linha + GXutil.padl( GXutil.trim( AV22InvDat), (short)(6), "0") + GXutil.padr( GXutil.trim( A1163lccbP), (short)(6), " ") + GXutil.padr( GXutil.trim( AV23FromCi[1-1]), (short)(3), " ") + GXutil.padr( GXutil.trim( AV25FareBa[1-1]), (short)(12), " ") + GXutil.padr( GXutil.trim( AV27Carrie[1-1]), (short)(4), " ") + GXutil.padr( GXutil.trim( AV28Class[1-1]), (short)(2), " ") ;
         AV10Linha = AV10Linha + GXutil.padr( GXutil.trim( AV23FromCi[2-1]), (short)(3), " ") + GXutil.padl( GXutil.trim( AV29DownPa), (short)(12), "0") + GXutil.padr( GXutil.trim( AV27Carrie[2-1]), (short)(4), " ") + GXutil.padr( GXutil.trim( AV28Class[2-1]), (short)(2), " ") + GXutil.padr( GXutil.trim( AV23FromCi[3-1]), (short)(3), " ") + GXutil.padl( GXutil.trim( AV30lccbIn), (short)(12), "0") ;
         AV10Linha = AV10Linha + GXutil.padr( GXutil.trim( AV27Carrie[3-1]), (short)(4), " ") + GXutil.padr( GXutil.trim( AV28Class[3-1]), (short)(2), " ") + GXutil.padr( GXutil.trim( AV23FromCi[4-1]), (short)(3), " ") + GXutil.padl( GXutil.trim( AV33DownTa), (short)(12), "0") + GXutil.padr( GXutil.trim( AV27Carrie[4-1]), (short)(4), " ") + GXutil.padr( GXutil.trim( AV28Class[4-1]), (short)(2), " ") ;
         AV10Linha = AV10Linha + GXutil.padr( GXutil.trim( AV23FromCi[5-1]), (short)(3), " ") + GXutil.padr( GXutil.trim( AV34MOEDA), (short)(4), " ") + GXutil.space( (short)(8)) + GXutil.padr( GXutil.trim( AV40I_To), (short)(3), " ") + GXutil.space( (short)(12)) + GXutil.space( (short)(2)) + GXutil.padl( GXutil.trim( AV35InvNum), (short)(14), "0") + GXutil.padl( GXutil.trim( AV36Maquin), (short)(16), "0") ;
         AV10Linha = AV10Linha + GXutil.padl( GXutil.trim( AV37NumPar), (short)(2), "0") + GXutil.padr( GXutil.trim( AV42Valida), (short)(4), " ") + GXutil.padl( GXutil.trim( AV41CommRa), (short)(4), "0") + GXutil.padr( GXutil.trim( AV43Fli_Da), (short)(5), " ") + GXutil.newLine( ) ;
         AV39xmlWri.writeRawText(AV10Linha);
         while ( (pr_default.getStatus(1) != 101) && ( GXutil.strcmp(P006W3_A1150lccbE[0], A1150lccbE) == 0 ) && ( GXutil.strcmp(P006W3_A1222lccbI[0], A1222lccbI) == 0 ) && P006W3_A1223lccbD[0].equals( A1223lccbD ) && ( GXutil.strcmp(P006W3_A1224lccbC[0], A1224lccbC) == 0 ) )
         {
            if ( ! ( ( GXutil.strcmp(P006W3_A1225lccbC[0], A1225lccbC) == 0 ) && ( GXutil.strcmp(P006W3_A1226lccbA[0], A1226lccbA) == 0 ) && ( GXutil.strcmp(P006W3_A1228lccbF[0], A1228lccbF) == 0 ) ) )
            {
               if (true) break;
            }
            brk6W3 = false ;
            A1227lccbO = P006W3_A1227lccbO[0] ;
            A1231lccbT = P006W3_A1231lccbT[0] ;
            A1232lccbT = P006W3_A1232lccbT[0] ;
            brk6W3 = true ;
            pr_default.readNext(1);
         }
         if ( ! brk6W3 )
         {
            brk6W3 = true ;
            pr_default.readNext(1);
         }
      }
      pr_default.close(1);
   }

   public void S123( )
   {
      /* 'HOT' Routine */
      AV26i = 1 ;
      while ( ( AV26i <= 20 ) )
      {
         AV23FromCi[AV26i-1] = "" ;
         AV25FareBa[AV26i-1] = "" ;
         AV27Carrie[AV26i-1] = "" ;
         AV28Class[AV26i-1] = "" ;
         AV40I_To = "" ;
         AV43Fli_Da = "" ;
         AV26i = (int)(AV26i+1) ;
      }
      /* Using cursor P006W5 */
      pr_default.execute(3, new Object[] {AV9lccbTDN, AV11lccbTR});
      while ( (pr_default.getStatus(3) != 101) )
      {
         A970DATA = P006W5_A970DATA[0] ;
         A969TIPO_V = P006W5_A969TIPO_V[0] ;
         A968NUM_BI = P006W5_A968NUM_BI[0] ;
         A967IATA = P006W5_A967IATA[0] ;
         A966CODE = P006W5_A966CODE[0] ;
         A965PER_NA = P006W5_A965PER_NA[0] ;
         A964CiaCod = P006W5_A964CiaCod[0] ;
         A963ISOC = P006W5_A963ISOC[0] ;
         A878PAX_NA = P006W5_A878PAX_NA[0] ;
         n878PAX_NA = P006W5_n878PAX_NA[0] ;
         A900MOEDA = P006W5_A900MOEDA[0] ;
         n900MOEDA = P006W5_n900MOEDA[0] ;
         A871COMMIS = P006W5_A871COMMIS[0] ;
         n871COMMIS = P006W5_n871COMMIS[0] ;
         AV15PAX_NA = A878PAX_NA ;
         AV34MOEDA = A900MOEDA ;
         AV41CommRa = GXutil.strReplace( GXutil.trim( GXutil.str( A871COMMIS, 12, 2)), ".", "") ;
         AV26i = 0 ;
         /* Using cursor P006W6 */
         pr_default.execute(4, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A923I_From = P006W6_A923I_From[0] ;
            n923I_From = P006W6_n923I_From[0] ;
            A971ISeq = P006W6_A971ISeq[0] ;
            A926Tariff = P006W6_A926Tariff[0] ;
            n926Tariff = P006W6_n926Tariff[0] ;
            A925Carrie = P006W6_A925Carrie[0] ;
            n925Carrie = P006W6_n925Carrie[0] ;
            A928Claxss = P006W6_A928Claxss[0] ;
            n928Claxss = P006W6_n928Claxss[0] ;
            A924I_To = P006W6_A924I_To[0] ;
            n924I_To = P006W6_n924I_To[0] ;
            A929Fli_Da = P006W6_A929Fli_Da[0] ;
            n929Fli_Da = P006W6_n929Fli_Da[0] ;
            AV26i = (int)(AV26i+1) ;
            AV23FromCi[A971ISeq-1] = GXutil.substring( GXutil.trim( A923I_From), 1, 3) ;
            AV25FareBa[A971ISeq-1] = GXutil.substring( GXutil.trim( A926Tariff), 1, 12) ;
            AV27Carrie[A971ISeq-1] = GXutil.substring( GXutil.trim( A925Carrie), 1, 4) ;
            AV28Class[A971ISeq-1] = GXutil.trim( A928Claxss) ;
            AV40I_To = GXutil.trim( A924I_To) ;
            if ( ( GXutil.strcmp(AV43Fli_Da, "") == 0 ) )
            {
               AV43Fli_Da = A929Fli_Da ;
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(3);
      }
      pr_default.close(3);
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV8lccbSub = GXutil.nullDate() ;
      AV19Sequen = 0 ;
      AV45Now = GXutil.nullDate() ;
      AV46Path = "" ;
      GXt_char1 = "" ;
      GXv_svchar2 = new String [1] ;
      scmdbuf = "" ;
      P006W2_A1147lccbE = new String[] {""} ;
      P006W2_n1147lccbE = new boolean[] {false} ;
      P006W2_A1150lccbE = new String[] {""} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      AV53Lccbem = "" ;
      AV44FileNa = "" ;
      AV39xmlWri = new com.genexus.xml.XMLWriter();
      returnInSub = false ;
      P006W3_A1227lccbO = new String[] {""} ;
      P006W3_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P006W3_n1189lccbS = new boolean[] {false} ;
      P006W3_A1224lccbC = new String[] {""} ;
      P006W3_A1150lccbE = new String[] {""} ;
      P006W3_A1184lccbS = new String[] {""} ;
      P006W3_n1184lccbS = new boolean[] {false} ;
      P006W3_A1228lccbF = new String[] {""} ;
      P006W3_A1226lccbA = new String[] {""} ;
      P006W3_A1225lccbC = new String[] {""} ;
      P006W3_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006W3_A1222lccbI = new String[] {""} ;
      P006W3_A1231lccbT = new String[] {""} ;
      P006W3_A1232lccbT = new String[] {""} ;
      P006W3_A1172lccbS = new double[1] ;
      P006W3_n1172lccbS = new boolean[] {false} ;
      P006W3_A1169lccbD = new double[1] ;
      P006W3_n1169lccbD = new boolean[] {false} ;
      P006W3_A1170lccbI = new double[1] ;
      P006W3_n1170lccbI = new boolean[] {false} ;
      P006W3_A1168lccbI = new short[1] ;
      P006W3_n1168lccbI = new boolean[] {false} ;
      P006W3_A1171lccbT = new double[1] ;
      P006W3_n1171lccbT = new boolean[] {false} ;
      P006W3_A1179lccbV = new String[] {""} ;
      P006W3_n1179lccbV = new boolean[] {false} ;
      P006W3_A1163lccbP = new String[] {""} ;
      P006W3_n1163lccbP = new boolean[] {false} ;
      brk6W3 = false ;
      A1227lccbO = "" ;
      A1189lccbS = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1224lccbC = "" ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1228lccbF = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1231lccbT = "" ;
      A1232lccbT = "" ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1169lccbD = 0 ;
      n1169lccbD = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1179lccbV = "" ;
      n1179lccbV = false ;
      A1163lccbP = "" ;
      n1163lccbP = false ;
      AV9lccbTDN = "" ;
      AV11lccbTR = "" ;
      AV24SalesA = "" ;
      AV29DownPa = "" ;
      AV30lccbIn = "" ;
      AV37NumPar = "" ;
      AV12lccbDa = GXutil.nullDate() ;
      AV13IssueD = "" ;
      AV48Week = (byte)(0) ;
      GXt_int3 = (byte)(0) ;
      GXv_int4 = new byte [1] ;
      AV14Billin = "" ;
      AV47Julian = (short)(0) ;
      GXt_int5 = (short)(0) ;
      GXv_int6 = new short [1] ;
      AV16InvRef = "" ;
      AV35InvNum = "" ;
      AV21InvDat = GXutil.nullDate() ;
      AV22InvDat = "" ;
      AV49lccbIn = (short)(0) ;
      P006W4_A1488ICSI_ = new String[] {""} ;
      P006W4_A1487ICSI_ = new String[] {""} ;
      P006W4_A1480ICSI_ = new String[] {""} ;
      P006W4_n1480ICSI_ = new boolean[] {false} ;
      P006W4_A1481ICSI_ = new String[] {""} ;
      P006W4_n1481ICSI_ = new boolean[] {false} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      A1480ICSI_ = "" ;
      n1480ICSI_ = false ;
      A1481ICSI_ = "" ;
      n1481ICSI_ = false ;
      AV36Maquin = "" ;
      AV20FPAMAu = 0 ;
      AV17FPAM = "" ;
      AV32DownTa = 0 ;
      AV33DownTa = "" ;
      AV42Valida = "" ;
      AV10Linha = "" ;
      AV15PAX_NA = "" ;
      AV23FromCi = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV23FromCi[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV25FareBa = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV25FareBa[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV27Carrie = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV27Carrie[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV28Class = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV28Class[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV34MOEDA = "" ;
      AV40I_To = "" ;
      AV41CommRa = "" ;
      AV43Fli_Da = "" ;
      AV26i = 0 ;
      P006W5_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P006W5_A969TIPO_V = new String[] {""} ;
      P006W5_A968NUM_BI = new String[] {""} ;
      P006W5_A967IATA = new String[] {""} ;
      P006W5_A966CODE = new String[] {""} ;
      P006W5_A965PER_NA = new String[] {""} ;
      P006W5_A964CiaCod = new String[] {""} ;
      P006W5_A963ISOC = new String[] {""} ;
      P006W5_A878PAX_NA = new String[] {""} ;
      P006W5_n878PAX_NA = new boolean[] {false} ;
      P006W5_A900MOEDA = new String[] {""} ;
      P006W5_n900MOEDA = new boolean[] {false} ;
      P006W5_A871COMMIS = new double[1] ;
      P006W5_n871COMMIS = new boolean[] {false} ;
      A970DATA = GXutil.nullDate() ;
      A969TIPO_V = "" ;
      A968NUM_BI = "" ;
      A967IATA = "" ;
      A966CODE = "" ;
      A965PER_NA = "" ;
      A964CiaCod = "" ;
      A963ISOC = "" ;
      A878PAX_NA = "" ;
      n878PAX_NA = false ;
      A900MOEDA = "" ;
      n900MOEDA = false ;
      A871COMMIS = 0 ;
      n871COMMIS = false ;
      P006W6_A963ISOC = new String[] {""} ;
      P006W6_A964CiaCod = new String[] {""} ;
      P006W6_A965PER_NA = new String[] {""} ;
      P006W6_A966CODE = new String[] {""} ;
      P006W6_A967IATA = new String[] {""} ;
      P006W6_A968NUM_BI = new String[] {""} ;
      P006W6_A969TIPO_V = new String[] {""} ;
      P006W6_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P006W6_A923I_From = new String[] {""} ;
      P006W6_n923I_From = new boolean[] {false} ;
      P006W6_A971ISeq = new int[1] ;
      P006W6_A926Tariff = new String[] {""} ;
      P006W6_n926Tariff = new boolean[] {false} ;
      P006W6_A925Carrie = new String[] {""} ;
      P006W6_n925Carrie = new boolean[] {false} ;
      P006W6_A928Claxss = new String[] {""} ;
      P006W6_n928Claxss = new boolean[] {false} ;
      P006W6_A924I_To = new String[] {""} ;
      P006W6_n924I_To = new boolean[] {false} ;
      P006W6_A929Fli_Da = new String[] {""} ;
      P006W6_n929Fli_Da = new boolean[] {false} ;
      A923I_From = "" ;
      n923I_From = false ;
      A971ISeq = 0 ;
      A926Tariff = "" ;
      n926Tariff = false ;
      A925Carrie = "" ;
      n925Carrie = false ;
      A928Claxss = "" ;
      n928Claxss = false ;
      A924I_To = "" ;
      n924I_To = false ;
      A929Fli_Da = "" ;
      n929Fli_Da = false ;
      GX_I = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pr1085__default(),
         new Object[] {
             new Object[] {
            P006W2_A1147lccbE, P006W2_n1147lccbE, P006W2_A1150lccbE
            }
            , new Object[] {
            P006W3_A1227lccbO, P006W3_A1189lccbS, P006W3_n1189lccbS, P006W3_A1224lccbC, P006W3_A1150lccbE, P006W3_A1184lccbS, P006W3_n1184lccbS, P006W3_A1228lccbF, P006W3_A1226lccbA, P006W3_A1225lccbC,
            P006W3_A1223lccbD, P006W3_A1222lccbI, P006W3_A1231lccbT, P006W3_A1232lccbT, P006W3_A1172lccbS, P006W3_n1172lccbS, P006W3_A1169lccbD, P006W3_n1169lccbD, P006W3_A1170lccbI, P006W3_n1170lccbI,
            P006W3_A1168lccbI, P006W3_n1168lccbI, P006W3_A1171lccbT, P006W3_n1171lccbT, P006W3_A1179lccbV, P006W3_n1179lccbV, P006W3_A1163lccbP, P006W3_n1163lccbP
            }
            , new Object[] {
            P006W4_A1488ICSI_, P006W4_A1487ICSI_, P006W4_A1480ICSI_, P006W4_n1480ICSI_, P006W4_A1481ICSI_, P006W4_n1481ICSI_
            }
            , new Object[] {
            P006W5_A970DATA, P006W5_A969TIPO_V, P006W5_A968NUM_BI, P006W5_A967IATA, P006W5_A966CODE, P006W5_A965PER_NA, P006W5_A964CiaCod, P006W5_A963ISOC, P006W5_A878PAX_NA, P006W5_n878PAX_NA,
            P006W5_A900MOEDA, P006W5_n900MOEDA, P006W5_A871COMMIS, P006W5_n871COMMIS
            }
            , new Object[] {
            P006W6_A963ISOC, P006W6_A964CiaCod, P006W6_A965PER_NA, P006W6_A966CODE, P006W6_A967IATA, P006W6_A968NUM_BI, P006W6_A969TIPO_V, P006W6_A970DATA, P006W6_A923I_From, P006W6_n923I_From,
            P006W6_A971ISeq, P006W6_A926Tariff, P006W6_n926Tariff, P006W6_A925Carrie, P006W6_n925Carrie, P006W6_A928Claxss, P006W6_n928Claxss, P006W6_A924I_To, P006W6_n924I_To, P006W6_A929Fli_Da,
            P006W6_n929Fli_Da
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV48Week ;
   private byte GXt_int3 ;
   private byte GXv_int4[] ;
   private short A1168lccbI ;
   private short AV47Julian ;
   private short GXt_int5 ;
   private short GXv_int6[] ;
   private short AV49lccbIn ;
   private short Gx_err ;
   private int AV19Sequen ;
   private int AV26i ;
   private int A971ISeq ;
   private int GX_I ;
   private double A1172lccbS ;
   private double A1169lccbD ;
   private double A1170lccbI ;
   private double A1171lccbT ;
   private double AV20FPAMAu ;
   private double AV32DownTa ;
   private double A871COMMIS ;
   private String AV46Path ;
   private String GXt_char1 ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String AV53Lccbem ;
   private String AV44FileNa ;
   private String A1227lccbO ;
   private String A1224lccbC ;
   private String A1184lccbS ;
   private String A1228lccbF ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1222lccbI ;
   private String A1231lccbT ;
   private String A1232lccbT ;
   private String A1179lccbV ;
   private String A1163lccbP ;
   private String AV9lccbTDN ;
   private String AV11lccbTR ;
   private String AV24SalesA ;
   private String AV29DownPa ;
   private String AV30lccbIn ;
   private String AV37NumPar ;
   private String AV13IssueD ;
   private String AV14Billin ;
   private String AV16InvRef ;
   private String AV35InvNum ;
   private String AV22InvDat ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String A1480ICSI_ ;
   private String A1481ICSI_ ;
   private String AV36Maquin ;
   private String AV17FPAM ;
   private String AV33DownTa ;
   private String AV42Valida ;
   private String AV23FromCi[] ;
   private String AV25FareBa[] ;
   private String AV27Carrie[] ;
   private String AV28Class[] ;
   private String AV34MOEDA ;
   private String AV41CommRa ;
   private String AV43Fli_Da ;
   private String A969TIPO_V ;
   private String A968NUM_BI ;
   private String A967IATA ;
   private String A966CODE ;
   private String A965PER_NA ;
   private String A964CiaCod ;
   private String A963ISOC ;
   private String A900MOEDA ;
   private String A928Claxss ;
   private String A929Fli_Da ;
   private java.util.Date AV8lccbSub ;
   private java.util.Date AV45Now ;
   private java.util.Date A1189lccbS ;
   private java.util.Date A1223lccbD ;
   private java.util.Date AV12lccbDa ;
   private java.util.Date AV21InvDat ;
   private java.util.Date A970DATA ;
   private boolean n1147lccbE ;
   private boolean returnInSub ;
   private boolean brk6W3 ;
   private boolean n1189lccbS ;
   private boolean n1184lccbS ;
   private boolean n1172lccbS ;
   private boolean n1169lccbD ;
   private boolean n1170lccbI ;
   private boolean n1168lccbI ;
   private boolean n1171lccbT ;
   private boolean n1179lccbV ;
   private boolean n1163lccbP ;
   private boolean n1480ICSI_ ;
   private boolean n1481ICSI_ ;
   private boolean n878PAX_NA ;
   private boolean n900MOEDA ;
   private boolean n871COMMIS ;
   private boolean n923I_From ;
   private boolean n926Tariff ;
   private boolean n925Carrie ;
   private boolean n928Claxss ;
   private boolean n924I_To ;
   private boolean n929Fli_Da ;
   private String GXv_svchar2[] ;
   private String AV10Linha ;
   private String AV15PAX_NA ;
   private String AV40I_To ;
   private String A878PAX_NA ;
   private String A923I_From ;
   private String A926Tariff ;
   private String A925Carrie ;
   private String A924I_To ;
   private com.genexus.xml.XMLWriter AV39xmlWri ;
   private IDataStoreProvider pr_default ;
   private String[] P006W2_A1147lccbE ;
   private boolean[] P006W2_n1147lccbE ;
   private String[] P006W2_A1150lccbE ;
   private String[] P006W3_A1227lccbO ;
   private java.util.Date[] P006W3_A1189lccbS ;
   private boolean[] P006W3_n1189lccbS ;
   private String[] P006W3_A1224lccbC ;
   private String[] P006W3_A1150lccbE ;
   private String[] P006W3_A1184lccbS ;
   private boolean[] P006W3_n1184lccbS ;
   private String[] P006W3_A1228lccbF ;
   private String[] P006W3_A1226lccbA ;
   private String[] P006W3_A1225lccbC ;
   private java.util.Date[] P006W3_A1223lccbD ;
   private String[] P006W3_A1222lccbI ;
   private String[] P006W3_A1231lccbT ;
   private String[] P006W3_A1232lccbT ;
   private double[] P006W3_A1172lccbS ;
   private boolean[] P006W3_n1172lccbS ;
   private double[] P006W3_A1169lccbD ;
   private boolean[] P006W3_n1169lccbD ;
   private double[] P006W3_A1170lccbI ;
   private boolean[] P006W3_n1170lccbI ;
   private short[] P006W3_A1168lccbI ;
   private boolean[] P006W3_n1168lccbI ;
   private double[] P006W3_A1171lccbT ;
   private boolean[] P006W3_n1171lccbT ;
   private String[] P006W3_A1179lccbV ;
   private boolean[] P006W3_n1179lccbV ;
   private String[] P006W3_A1163lccbP ;
   private boolean[] P006W3_n1163lccbP ;
   private String[] P006W4_A1488ICSI_ ;
   private String[] P006W4_A1487ICSI_ ;
   private String[] P006W4_A1480ICSI_ ;
   private boolean[] P006W4_n1480ICSI_ ;
   private String[] P006W4_A1481ICSI_ ;
   private boolean[] P006W4_n1481ICSI_ ;
   private java.util.Date[] P006W5_A970DATA ;
   private String[] P006W5_A969TIPO_V ;
   private String[] P006W5_A968NUM_BI ;
   private String[] P006W5_A967IATA ;
   private String[] P006W5_A966CODE ;
   private String[] P006W5_A965PER_NA ;
   private String[] P006W5_A964CiaCod ;
   private String[] P006W5_A963ISOC ;
   private String[] P006W5_A878PAX_NA ;
   private boolean[] P006W5_n878PAX_NA ;
   private String[] P006W5_A900MOEDA ;
   private boolean[] P006W5_n900MOEDA ;
   private double[] P006W5_A871COMMIS ;
   private boolean[] P006W5_n871COMMIS ;
   private String[] P006W6_A963ISOC ;
   private String[] P006W6_A964CiaCod ;
   private String[] P006W6_A965PER_NA ;
   private String[] P006W6_A966CODE ;
   private String[] P006W6_A967IATA ;
   private String[] P006W6_A968NUM_BI ;
   private String[] P006W6_A969TIPO_V ;
   private java.util.Date[] P006W6_A970DATA ;
   private String[] P006W6_A923I_From ;
   private boolean[] P006W6_n923I_From ;
   private int[] P006W6_A971ISeq ;
   private String[] P006W6_A926Tariff ;
   private boolean[] P006W6_n926Tariff ;
   private String[] P006W6_A925Carrie ;
   private boolean[] P006W6_n925Carrie ;
   private String[] P006W6_A928Claxss ;
   private boolean[] P006W6_n928Claxss ;
   private String[] P006W6_A924I_To ;
   private boolean[] P006W6_n924I_To ;
   private String[] P006W6_A929Fli_Da ;
   private boolean[] P006W6_n929Fli_Da ;
}

final  class pr1085__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006W2", "SELECT [lccbEmpEnab], [lccbEmpCod] FROM [LCCBEMP] WITH (NOLOCK) WHERE [lccbEmpEnab] = '1' ORDER BY [lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006W3", "SELECT T1.[lccbOpCode], T2.[lccbSubDate], T1.[lccbCCard], T1.[lccbEmpCod], T2.[lccbStatus], T1.[lccbFPAC_PLP], T1.[lccbAppCode], T1.[lccbCCNum], T1.[lccbDate], T1.[lccbIATA], T1.[lccbTDNR], T1.[lccbTRNC], T2.[lccbSaleAmount], T2.[lccbDownPayment], T2.[lccbInstAmount], T2.[lccbInstallments], T2.[lccbTip], T2.[lccbValidDate], T2.[lccbPlanCode] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate] = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum] AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP]) WHERE (T1.[lccbEmpCod] = ?) AND (T2.[lccbSubDate] = ?) AND (T1.[lccbCCard] = 'AX') AND (T2.[lccbStatus] = 'PROCPLP') ORDER BY T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbDate], T1.[lccbCCard], T1.[lccbCCNum], T1.[lccbAppCode], T1.[lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006W4", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCPOS1], [ICSI_CCPOS2] FROM [ICSI_CCINFO] WITH (NOLOCK) WHERE [ICSI_EmpCod] = '000' and [ICSI_CCCod] = 'AX' ORDER BY [ICSI_EmpCod], [ICSI_CCCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P006W5", "SELECT TOP 1 [DATA], [TIPO_VEND], [NUM_BIL], [IATA], [CODE], [PER_NAME], [CiaCod], [ISOC], [PAX_NAME], [MOEDA], [COMMISSION] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL] = ?) AND ([CODE] = ?) ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P006W6", "SELECT [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [I_From], [ISeq], [Tariff], [Carrier], [Claxss], [I_To], [Fli_DateC] FROM [HOT1] WITH (NOLOCK) WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 8) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getString(6, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 44) ;
               ((java.util.Date[]) buf[10])[0] = rslt.getGXDate(9) ;
               ((String[]) buf[11])[0] = rslt.getString(10, 7) ;
               ((String[]) buf[12])[0] = rslt.getString(11, 10) ;
               ((String[]) buf[13])[0] = rslt.getString(12, 4) ;
               ((double[]) buf[14])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((double[]) buf[16])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((short[]) buf[20])[0] = rslt.getShort(16) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(17) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(18, 4) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(19, 20) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 3 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 2) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 5) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((int[]) buf[10])[0] = rslt.getInt(10) ;
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((String[]) buf[15])[0] = rslt.getString(13, 2) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((String[]) buf[17])[0] = rslt.getVarchar(14) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((String[]) buf[19])[0] = rslt.getString(15, 5) ;
               ((boolean[]) buf[20])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setDate(2, (java.util.Date)parms[1]);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 10);
               stmt.setString(2, (String)parms[1], 4);
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               break;
      }
   }

}


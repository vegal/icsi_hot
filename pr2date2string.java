/*
               File: R2Date2String
        Description: Converte data em string YYYYMMDD
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:1.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2date2string extends GXProcedure
{
   public pr2date2string( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2date2string.class ), "" );
   }

   public pr2date2string( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( java.util.Date[] aP0 ,
                        byte[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( java.util.Date[] aP0 ,
                             byte[] aP1 ,
                             String[] aP2 )
   {
      pr2date2string.this.AV8dt = aP0[0];
      this.aP0 = aP0;
      pr2date2string.this.AV9fCentur = aP1[0];
      this.aP1 = aP1;
      pr2date2string.this.AV10sOutpu = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV11s = "00" + GXutil.trim( GXutil.str( GXutil.day( AV8dt), 2, 0)) ;
      AV10sOutpu = GXutil.right( AV11s, 2) ;
      AV11s = "00" + GXutil.trim( GXutil.str( GXutil.month( AV8dt), 2, 0)) ;
      AV10sOutpu = GXutil.right( AV11s, 2) + AV10sOutpu ;
      AV11s = GXutil.trim( GXutil.str( GXutil.year( AV8dt), 4, 0)) ;
      if ( ( AV9fCentur == 0 ) )
      {
         AV11s = GXutil.substring( AV11s, 3, 2) ;
      }
      AV10sOutpu = AV11s + AV10sOutpu ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2date2string.this.AV8dt;
      this.aP1[0] = pr2date2string.this.AV9fCentur;
      this.aP2[0] = pr2date2string.this.AV10sOutpu;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV11s = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV9fCentur ;
   private short Gx_err ;
   private String AV10sOutpu ;
   private String AV11s ;
   private java.util.Date AV8dt ;
   private java.util.Date[] aP0 ;
   private byte[] aP1 ;
   private String[] aP2 ;
}


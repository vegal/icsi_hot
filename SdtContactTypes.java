import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtContactTypes extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtContactTypes( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtContactTypes.class));
   }

   public SdtContactTypes( int remoteHandle ,
                           ModelContext context )
   {
      super( context, "SdtContactTypes");
      initialize( remoteHandle) ;
   }

   public SdtContactTypes( int remoteHandle ,
                           StructSdtContactTypes struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV365ContactTypesCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV365ContactTypesCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Contacttypescode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Contacttypesdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesStatus") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Contacttypesstatus = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Contacttypescode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Contacttypesdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ContactTypesStatus_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtContactTypes_Contacttypesstatus_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "ContactTypes" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ContactTypesCode", GXutil.rtrim( gxTv_SdtContactTypes_Contacttypescode));
      oWriter.writeElement("ContactTypesDescription", GXutil.rtrim( gxTv_SdtContactTypes_Contacttypesdescription));
      oWriter.writeElement("ContactTypesStatus", GXutil.rtrim( gxTv_SdtContactTypes_Contacttypesstatus));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtContactTypes_Mode));
      oWriter.writeElement("ContactTypesCode_Z", GXutil.rtrim( gxTv_SdtContactTypes_Contacttypescode_Z));
      oWriter.writeElement("ContactTypesDescription_Z", GXutil.rtrim( gxTv_SdtContactTypes_Contacttypesdescription_Z));
      oWriter.writeElement("ContactTypesStatus_Z", GXutil.rtrim( gxTv_SdtContactTypes_Contacttypesstatus_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtContactTypes_Contacttypescode( )
   {
      return gxTv_SdtContactTypes_Contacttypescode ;
   }

   public void setgxTv_SdtContactTypes_Contacttypescode( String value )
   {
      gxTv_SdtContactTypes_Contacttypescode = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Contacttypescode_SetNull( )
   {
      gxTv_SdtContactTypes_Contacttypescode = "" ;
      return  ;
   }

   public String getgxTv_SdtContactTypes_Contacttypesdescription( )
   {
      return gxTv_SdtContactTypes_Contacttypesdescription ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesdescription( String value )
   {
      gxTv_SdtContactTypes_Contacttypesdescription = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesdescription_SetNull( )
   {
      gxTv_SdtContactTypes_Contacttypesdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtContactTypes_Contacttypesstatus( )
   {
      return gxTv_SdtContactTypes_Contacttypesstatus ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesstatus( String value )
   {
      gxTv_SdtContactTypes_Contacttypesstatus = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesstatus_SetNull( )
   {
      gxTv_SdtContactTypes_Contacttypesstatus = "" ;
      return  ;
   }

   public String getgxTv_SdtContactTypes_Mode( )
   {
      return gxTv_SdtContactTypes_Mode ;
   }

   public void setgxTv_SdtContactTypes_Mode( String value )
   {
      gxTv_SdtContactTypes_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Mode_SetNull( )
   {
      gxTv_SdtContactTypes_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtContactTypes_Contacttypescode_Z( )
   {
      return gxTv_SdtContactTypes_Contacttypescode_Z ;
   }

   public void setgxTv_SdtContactTypes_Contacttypescode_Z( String value )
   {
      gxTv_SdtContactTypes_Contacttypescode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Contacttypescode_Z_SetNull( )
   {
      gxTv_SdtContactTypes_Contacttypescode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtContactTypes_Contacttypesdescription_Z( )
   {
      return gxTv_SdtContactTypes_Contacttypesdescription_Z ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesdescription_Z( String value )
   {
      gxTv_SdtContactTypes_Contacttypesdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesdescription_Z_SetNull( )
   {
      gxTv_SdtContactTypes_Contacttypesdescription_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtContactTypes_Contacttypesstatus_Z( )
   {
      return gxTv_SdtContactTypes_Contacttypesstatus_Z ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesstatus_Z( String value )
   {
      gxTv_SdtContactTypes_Contacttypesstatus_Z = value ;
      return  ;
   }

   public void setgxTv_SdtContactTypes_Contacttypesstatus_Z_SetNull( )
   {
      gxTv_SdtContactTypes_Contacttypesstatus_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tcontacttypes_bc obj ;
      obj = new tcontacttypes_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtContactTypes_Contacttypescode = "" ;
      gxTv_SdtContactTypes_Contacttypesdescription = "" ;
      gxTv_SdtContactTypes_Contacttypesstatus = "" ;
      gxTv_SdtContactTypes_Mode = "" ;
      gxTv_SdtContactTypes_Contacttypescode_Z = "" ;
      gxTv_SdtContactTypes_Contacttypesdescription_Z = "" ;
      gxTv_SdtContactTypes_Contacttypesstatus_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtContactTypes Clone( )
   {
      SdtContactTypes sdt ;
      tcontacttypes_bc obj ;
      sdt = (SdtContactTypes)(clone()) ;
      obj = (tcontacttypes_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtContactTypes struct )
   {
      setgxTv_SdtContactTypes_Contacttypescode(struct.getContacttypescode());
      setgxTv_SdtContactTypes_Contacttypesdescription(struct.getContacttypesdescription());
      setgxTv_SdtContactTypes_Contacttypesstatus(struct.getContacttypesstatus());
      setgxTv_SdtContactTypes_Mode(struct.getMode());
      setgxTv_SdtContactTypes_Contacttypescode_Z(struct.getContacttypescode_Z());
      setgxTv_SdtContactTypes_Contacttypesdescription_Z(struct.getContacttypesdescription_Z());
      setgxTv_SdtContactTypes_Contacttypesstatus_Z(struct.getContacttypesstatus_Z());
   }

   public StructSdtContactTypes getStruct( )
   {
      StructSdtContactTypes struct = new StructSdtContactTypes ();
      struct.setContacttypescode(getgxTv_SdtContactTypes_Contacttypescode());
      struct.setContacttypesdescription(getgxTv_SdtContactTypes_Contacttypesdescription());
      struct.setContacttypesstatus(getgxTv_SdtContactTypes_Contacttypesstatus());
      struct.setMode(getgxTv_SdtContactTypes_Mode());
      struct.setContacttypescode_Z(getgxTv_SdtContactTypes_Contacttypescode_Z());
      struct.setContacttypesdescription_Z(getgxTv_SdtContactTypes_Contacttypesdescription_Z());
      struct.setContacttypesstatus_Z(getgxTv_SdtContactTypes_Contacttypesstatus_Z());
      return struct ;
   }

   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtContactTypes_Contacttypesstatus ;
   protected String gxTv_SdtContactTypes_Mode ;
   protected String gxTv_SdtContactTypes_Contacttypesstatus_Z ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtContactTypes_Contacttypescode ;
   protected String gxTv_SdtContactTypes_Contacttypesdescription ;
   protected String gxTv_SdtContactTypes_Contacttypescode_Z ;
   protected String gxTv_SdtContactTypes_Contacttypesdescription_Z ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtIT0A_IT0AItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtIT0A_IT0AItem( )
   {
      this(  new ModelContext(SdtIT0A_IT0AItem.class));
   }

   public SdtIT0A_IT0AItem( ModelContext context )
   {
      super( context, "SdtIT0A_IT0AItem");
   }

   public SdtIT0A_IT0AItem( StructSdtIT0A_IT0AItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "RFID_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Rfid_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "BERA_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Bera_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICDN_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Icdn_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CDGT_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Cdgt_it0a = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AMIL_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Amil_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "RFIC_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Rfic_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MPOC_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Mpoc_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MPEQ_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Mpeq_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MPEV_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Mpev_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MPSC_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Mpsc_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "MPTX_IT0A") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0A_IT0AItem_Mptx_it0a = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "IT0A.IT0AItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("RFID_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Rfid_it0a));
      oWriter.writeElement("BERA_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Bera_it0a));
      oWriter.writeElement("ICDN_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Icdn_it0a));
      oWriter.writeElement("CDGT_IT0A", GXutil.trim( GXutil.str( gxTv_SdtIT0A_IT0AItem_Cdgt_it0a, 1, 0)));
      oWriter.writeElement("AMIL_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Amil_it0a));
      oWriter.writeElement("RFIC_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Rfic_it0a));
      oWriter.writeElement("MPOC_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Mpoc_it0a));
      oWriter.writeElement("MPEQ_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Mpeq_it0a));
      oWriter.writeElement("MPEV_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Mpev_it0a));
      oWriter.writeElement("MPSC_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Mpsc_it0a));
      oWriter.writeElement("MPTX_IT0A", GXutil.rtrim( gxTv_SdtIT0A_IT0AItem_Mptx_it0a));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Rfid_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Rfid_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Rfid_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Rfid_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Rfid_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Rfid_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Bera_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Bera_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Bera_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Bera_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Bera_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Bera_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Icdn_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Icdn_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Icdn_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Icdn_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Icdn_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Icdn_it0a = "" ;
      return  ;
   }

   public byte getgxTv_SdtIT0A_IT0AItem_Cdgt_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Cdgt_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Cdgt_it0a( byte value )
   {
      gxTv_SdtIT0A_IT0AItem_Cdgt_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Cdgt_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Cdgt_it0a = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Amil_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Amil_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Amil_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Amil_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Amil_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Amil_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Rfic_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Rfic_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Rfic_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Rfic_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Rfic_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Rfic_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Mpoc_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpoc_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpoc_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpoc_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpoc_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Mpoc_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Mpeq_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpeq_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpeq_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpeq_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpeq_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Mpeq_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Mpev_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpev_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpev_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpev_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpev_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Mpev_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Mpsc_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpsc_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpsc_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpsc_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mpsc_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Mpsc_it0a = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0A_IT0AItem_Mptx_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mptx_it0a ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mptx_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mptx_it0a = value ;
      return  ;
   }

   public void setgxTv_SdtIT0A_IT0AItem_Mptx_it0a_SetNull( )
   {
      gxTv_SdtIT0A_IT0AItem_Mptx_it0a = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtIT0A_IT0AItem_Rfid_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Bera_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Icdn_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Cdgt_it0a = (byte)(0) ;
      gxTv_SdtIT0A_IT0AItem_Amil_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Rfic_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpoc_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpeq_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpev_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpsc_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mptx_it0a = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtIT0A_IT0AItem Clone( )
   {
      return (SdtIT0A_IT0AItem)(clone()) ;
   }

   public void setStruct( StructSdtIT0A_IT0AItem struct )
   {
      setgxTv_SdtIT0A_IT0AItem_Rfid_it0a(struct.getRfid_it0a());
      setgxTv_SdtIT0A_IT0AItem_Bera_it0a(struct.getBera_it0a());
      setgxTv_SdtIT0A_IT0AItem_Icdn_it0a(struct.getIcdn_it0a());
      setgxTv_SdtIT0A_IT0AItem_Cdgt_it0a(struct.getCdgt_it0a());
      setgxTv_SdtIT0A_IT0AItem_Amil_it0a(struct.getAmil_it0a());
      setgxTv_SdtIT0A_IT0AItem_Rfic_it0a(struct.getRfic_it0a());
      setgxTv_SdtIT0A_IT0AItem_Mpoc_it0a(struct.getMpoc_it0a());
      setgxTv_SdtIT0A_IT0AItem_Mpeq_it0a(struct.getMpeq_it0a());
      setgxTv_SdtIT0A_IT0AItem_Mpev_it0a(struct.getMpev_it0a());
      setgxTv_SdtIT0A_IT0AItem_Mpsc_it0a(struct.getMpsc_it0a());
      setgxTv_SdtIT0A_IT0AItem_Mptx_it0a(struct.getMptx_it0a());
   }

   public StructSdtIT0A_IT0AItem getStruct( )
   {
      StructSdtIT0A_IT0AItem struct = new StructSdtIT0A_IT0AItem ();
      struct.setRfid_it0a(getgxTv_SdtIT0A_IT0AItem_Rfid_it0a());
      struct.setBera_it0a(getgxTv_SdtIT0A_IT0AItem_Bera_it0a());
      struct.setIcdn_it0a(getgxTv_SdtIT0A_IT0AItem_Icdn_it0a());
      struct.setCdgt_it0a(getgxTv_SdtIT0A_IT0AItem_Cdgt_it0a());
      struct.setAmil_it0a(getgxTv_SdtIT0A_IT0AItem_Amil_it0a());
      struct.setRfic_it0a(getgxTv_SdtIT0A_IT0AItem_Rfic_it0a());
      struct.setMpoc_it0a(getgxTv_SdtIT0A_IT0AItem_Mpoc_it0a());
      struct.setMpeq_it0a(getgxTv_SdtIT0A_IT0AItem_Mpeq_it0a());
      struct.setMpev_it0a(getgxTv_SdtIT0A_IT0AItem_Mpev_it0a());
      struct.setMpsc_it0a(getgxTv_SdtIT0A_IT0AItem_Mpsc_it0a());
      struct.setMptx_it0a(getgxTv_SdtIT0A_IT0AItem_Mptx_it0a());
      return struct ;
   }

   private byte gxTv_SdtIT0A_IT0AItem_Cdgt_it0a ;
   private short nOutParmCount ;
   private short readOk ;
   private String sTagName ;
   private String GXt_char1 ;
   private String gxTv_SdtIT0A_IT0AItem_Rfid_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Bera_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Icdn_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Amil_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Rfic_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Mpoc_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Mpeq_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Mpev_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Mpsc_it0a ;
   private String gxTv_SdtIT0A_IT0AItem_Mptx_it0a ;
}



public final  class StructSdtConfig implements Cloneable, java.io.Serializable
{
   public StructSdtConfig( )
   {
      gxTv_SdtConfig_Configid = "" ;
      gxTv_SdtConfig_Configvalue = "" ;
      gxTv_SdtConfig_Configdes = "" ;
      gxTv_SdtConfig_Mode = "" ;
      gxTv_SdtConfig_Configid_Z = "" ;
      gxTv_SdtConfig_Configvalue_Z = "" ;
      gxTv_SdtConfig_Configdes_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getConfigid( )
   {
      return gxTv_SdtConfig_Configid ;
   }

   public void setConfigid( String value )
   {
      gxTv_SdtConfig_Configid = value ;
      return  ;
   }

   public String getConfigvalue( )
   {
      return gxTv_SdtConfig_Configvalue ;
   }

   public void setConfigvalue( String value )
   {
      gxTv_SdtConfig_Configvalue = value ;
      return  ;
   }

   public String getConfigdes( )
   {
      return gxTv_SdtConfig_Configdes ;
   }

   public void setConfigdes( String value )
   {
      gxTv_SdtConfig_Configdes = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtConfig_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtConfig_Mode = value ;
      return  ;
   }

   public String getConfigid_Z( )
   {
      return gxTv_SdtConfig_Configid_Z ;
   }

   public void setConfigid_Z( String value )
   {
      gxTv_SdtConfig_Configid_Z = value ;
      return  ;
   }

   public String getConfigvalue_Z( )
   {
      return gxTv_SdtConfig_Configvalue_Z ;
   }

   public void setConfigvalue_Z( String value )
   {
      gxTv_SdtConfig_Configvalue_Z = value ;
      return  ;
   }

   public String getConfigdes_Z( )
   {
      return gxTv_SdtConfig_Configdes_Z ;
   }

   public void setConfigdes_Z( String value )
   {
      gxTv_SdtConfig_Configdes_Z = value ;
      return  ;
   }

   protected String gxTv_SdtConfig_Mode ;
   protected String gxTv_SdtConfig_Configid ;
   protected String gxTv_SdtConfig_Configvalue ;
   protected String gxTv_SdtConfig_Configdes ;
   protected String gxTv_SdtConfig_Configid_Z ;
   protected String gxTv_SdtConfig_Configvalue_Z ;
   protected String gxTv_SdtConfig_Configdes_Z ;
}


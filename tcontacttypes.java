/*
               File: ContactTypes
        Description: Contact Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:1.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

/* Client and Server side code */
public final  class tcontacttypes extends GXTransaction
{
   public tcontacttypes( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tcontacttypes.class ), "" );
   }

   public tcontacttypes( int remoteHandle ,
                         ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void initializeNonKey28223( )
   {
      A366Contac = "" ;
      A1104Conta = "N" ;
   }

   public void initAll28223( )
   {
      A365Contac = "" ;
      K365Contac = A365Contac ;
      geteqAfterKey = (byte)(1) ;
      clear( ) ;
      initializeNonKey28223( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void resetCaption280( )
   {
   }

   /* Client side code */
   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
   }

   protected String getObjectName( )
   {
      return "ContactTypes" ;
   }

   protected String getFrmTitle( )
   {
      return "Contact Types" ;
   }

   protected GXMenuBar getMenuBar( )
   {
      return ApplicationUI.getDefaultMenuBar(this) ;
   }

   protected int getFrmTop( )
   {
      return 0 ;
   }

   protected int getFrmLeft( )
   {
      return 0 ;
   }

   protected int getFrmWidth( )
   {
      return 727 ;
   }

   protected int getFrmHeight( )
   {
      return 438 ;
   }

   protected String getHelpId( )
   {
      return "HLP_TContactTypes.htm";
   }

   protected int getFrmBackground( )
   {
      return UIFactory.getColor(255, 255, 255) ;
   }

   protected int getFrmForeground( )
   {
      return UIFactory.getColor(6) ;
   }

   protected boolean isMainProgram( )
   {
      return false;
   }

   protected boolean isModal( )
   {
      return true ;
   }

   protected boolean hasDBAccess( )
   {
      return true ;
   }

   protected int getRefreshTimeout( )
   {
      return 0 ;
   }

   public boolean getRefreshTimeoutAlways( )
   {
      return true ;
   }

   protected boolean getPaintAfterStart( )
   {
      return true ;
   }

   protected int getBorderStyle( )
   {
      return 2 ;
   }

   protected boolean getMaxButton( )
   {
      return true ;
   }

   protected boolean getMinButton( )
   {
      return true ;
   }

   protected boolean getCtrlBox( )
   {
      return true ;
   }

   protected boolean getShowInTaskbar( )
   {
      return true ;
   }

   protected String getFormIcon( )
   {
      return "" ;
   }

   protected boolean getAutocenter( )
   {
      return false ;
   }

   public void execute( String aP0 ,
                        String aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String aP1 )
   {
      tcontacttypes.this.AV12Contac = aP0;
      tcontacttypes.this.Gx_mode = aP1;
      start();
   }

   protected void screen( )
   {
      GXPanel1 = new GXPanel(this, 3 , 28 , 727 , 438 );
      this.setIBackground(UIFactory.getColor(255, 255, 255));
      this.setIForeground(UIFactory.getColor(6));
      GXPanel1 .setPixelsPerInch( 96 , 96 );
      GXPanel1.refreshFrame();
      edtContactTypesCode = new GUIObjectString ( new GXEdit(5, "XXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 89, 45, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 89 , 45 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A365Contac" );
      ((GXEdit) edtContactTypesCode.getGXComponent()).setAlignment(ILabel.LEFT);
      edtContactTypesCode.addFocusListener(this);
      edtContactTypesCode.getGXComponent().setHelpId("HLP_TContactTypes.htm");
      edtContactTypesDescription = new GUIObjectString ( new GXEdit(30, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", UIFactory.getFont( "Courier New", 0, 9),189, 113, 220, 21, GXPanel1, false, ILabel.BORDER_3D, GXTypeConstants.VARCHAR, false, true, UIFactory.getColor(5), false) , GXPanel1 , 189 , 113 , 220 , 21 , UIFactory.getColor(5) , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A366Contac" );
      ((GXEdit) edtContactTypesDescription.getGXComponent()).setAlignment(ILabel.LEFT);
      edtContactTypesDescription.addFocusListener(this);
      edtContactTypesDescription.getGXComponent().setHelpId("HLP_TContactTypes.htm");
      chkContactTypesStatus = new GUIObjectString ( new GXCheckBox(GXPanel1, "Enabled ?" , "Y", "N") , GXPanel1 , 189 , 137 , 84 , 16 , Integer.MAX_VALUE , UIFactory.getColor(8) , UIFactory.getFont( "Courier New", 0, 9) , true , "A1104Conta" );
      chkContactTypesStatus.addFocusListener(this);
      chkContactTypesStatus.addItemListener(this);
      chkContactTypesStatus.getGXComponent().setHelpId("HLP_TContactTypes.htm");
      bttBtn_first = UIFactory.getGXButton( GXPanel1 , "first1.bmp" ,  103 ,  8 ,  40 ,  40  );
      bttBtn_first.setTooltip("|<");
      bttBtn_first.addActionListener(this);
      bttBtn_first.setFiresEvents(false);
      bttBtn_prev = UIFactory.getGXButton( GXPanel1 , "prev1.bmp" ,  146 ,  8 ,  40 ,  40  );
      bttBtn_prev.setTooltip("<");
      bttBtn_prev.addActionListener(this);
      bttBtn_prev.setFiresEvents(false);
      bttBtn_next = UIFactory.getGXButton( GXPanel1 , "next1.bmp" ,  210 ,  8 ,  40 ,  40  );
      bttBtn_next.setTooltip(">");
      bttBtn_next.addActionListener(this);
      bttBtn_next.setFiresEvents(false);
      bttBtn_last = UIFactory.getGXButton( GXPanel1 , "last1.bmp" ,  252 ,  8 ,  40 ,  40  );
      bttBtn_last.setTooltip(">|");
      bttBtn_last.addActionListener(this);
      bttBtn_last.setFiresEvents(false);
      bttBtn_exit2 = UIFactory.getGXButton( GXPanel1 , "lanterna.bmp" ,  347 ,  8 ,  40 ,  40  );
      bttBtn_exit2.setTooltip("Select");
      bttBtn_exit2.addActionListener(this);
      bttBtn_exit2.setFiresEvents(false);
      bttBtn_exit3 = UIFactory.getGXButton( GXPanel1 , "limpar.bmp" ,  430 ,  8 ,  40 ,  40  );
      bttBtn_exit3.setTooltip("Delete");
      bttBtn_exit3.addActionListener(this);
      bttBtn_exit1 = UIFactory.getGXButton( GXPanel1 , "confirm.bmp" ,  513 ,  8 ,  40 ,  40  );
      bttBtn_exit1.setTooltip("Confirm");
      bttBtn_exit1.addActionListener(this);
      bttBtn_exit = UIFactory.getGXButton( GXPanel1 , "sair.bmp" ,  596 ,  8 ,  40 ,  40  );
      bttBtn_exit.setTooltip("Close");
      bttBtn_exit.addActionListener(this);
      bttBtn_exit.setFiresEvents(false);
      lbllbl12 = UIFactory.getLabel(GXPanel1, "Contact Type Code", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 89 , 110 , 13 );
      lbllbl14 = UIFactory.getLabel(GXPanel1, "Contact Type Description", ILabel.LEFT, Integer.MAX_VALUE, UIFactory.getColor(8), UIFactory.getFont( "MS Sans Serif", IFontConstants.BOLD, 8), false, ILabel.BORDER_NONE , 21 , 113 , 145 , 13 );
      imgimg7  =  UIFactory.getGXImage( GXPanel1 , "tx_logo.jpg" , 0 , 0 , 89 , 89 );
      focusManager.setControlList(new IFocusableControl[] {
                edtContactTypesCode ,
                edtContactTypesDescription ,
                chkContactTypesStatus ,
                bttBtn_exit1 ,
                bttBtn_exit ,
                bttBtn_first ,
                bttBtn_prev ,
                bttBtn_next ,
                bttBtn_last ,
                bttBtn_exit2 ,
                bttBtn_exit3
      });
   }

   protected void setFocusFirst( )
   {
      setFocus(edtContactTypesCode, true);
   }

   public void reloadDynamicLists( int id )
   {
   }

   public void clear( )
   {
      initializeNonKey28223( ) ;
   }

   public void disable_std_buttons( )
   {
      bttBtn_first.setGXEnabled( 0 );
      bttBtn_prev.setGXEnabled( 0 );
      bttBtn_next.setGXEnabled( 0 );
      bttBtn_last.setGXEnabled( 0 );
      bttBtn_exit2.setGXEnabled( 0 );
      if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
      {
         bttBtn_exit3.setGXEnabled( 0 );
         bttBtn_exit1.setGXEnabled( 0 );
         edtContactTypesCode.setEnabled( 0 );
         edtContactTypesDescription.setEnabled( 0 );
         chkContactTypesStatus.setEnabled( 0 );
         setFocus(bttBtn_exit1, true);
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            context.msgStatus( localUtil.getMessages().getMessage("confdelete") );
         }
         else
         {
            context.msgStatus( localUtil.getMessages().getMessage("mustconfirm") );
         }
         bttBtn_exit1.setBitmap( "gxconfirm_cnf.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionconfirm") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_add.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionadd") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_upd.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captionupdate") );
      }
      else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         bttBtn_exit1.setBitmap( "gxconfirm_dlt.gif" );
         bttBtn_exit1.setTooltip( localUtil.getMessages().getMessage("captiondelete") );
      }
      else
      {
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         /* Execute user event: e11282 */
         e11282 ();
         trnEnded = 0 ;
         if (!cleanedUp) {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               initAll28223( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }
   }

   protected boolean hasStatusBar( )
   {
      return true;
   }

   public void refreshArray( String array )
   {
   }

   public void refreshSDT( String name )
   {
   }

   protected void GXEnter( )
   {
      btn_enter( ) ;
      if (!cleanedUp) {
         VariablesToControls();
         set_caption( );
      }
   }

   protected void GXStart( )
   {
      standaloneStartup( ) ;
      VariablesToControls();
      /* Execute Start event if defined. */
      /* Execute user event: e12282 */
      e12282 ();
   }

   protected void actionEventDispatch( Object eventSource )
   {
      lastAnyError = AnyError ;
      AnyError = 0;
      if ( bttBtn_exit.isEventSource(eventSource) ) {
         if (canCleanup())

         return;
      }
      if ( bttBtn_exit1.isEventSource(eventSource) ) {
         if ( ( lastAnyError != 0 ) )
         {
            return;
         }
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return;
      }
   }

   protected void setCurrentGXCursor( Object eventSource )
   {
      if ( edtContactTypesCode.isEventSource(eventSource) ) {
         setGXCursor( edtContactTypesCode.getGXCursor() );
         return;
      }
      if ( edtContactTypesDescription.isEventSource(eventSource) ) {
         setGXCursor( edtContactTypesDescription.getGXCursor() );
         return;
      }
      if ( chkContactTypesStatus.isEventSource(eventSource) ) {
         setGXCursor( chkContactTypesStatus.getGXCursor() );
         return;
      }
   }

   protected void gotFocusEventDispatch( Object eventSource )
   {
   }

   protected void focusEventDispatch( Object eventSource )
   {
      AnyError = (short)(0) ;
      if ( edtContactTypesCode.isEventSource(eventSource) ) {
         valid_Contacttypescode ();
         return;
      }
      if ( edtContactTypesDescription.isEventSource(eventSource) ) {
         valid_Contacttypesdescription ();
         return;
      }
   }

   protected void updateAttributes( Object eventSource )
   {
      if ( edtContactTypesCode.isEventSource(eventSource) ) {
         A365Contac = edtContactTypesCode.getValue() ;
         return;
      }
      if ( edtContactTypesDescription.isEventSource(eventSource) ) {
         A366Contac = edtContactTypesDescription.getValue() ;
         return;
      }
      if ( chkContactTypesStatus.isEventSource(eventSource) ) {
         A1104Conta = chkContactTypesStatus.getValue() ;
         return;
      }
   }

   protected void itemEventDispatch( Object eventSource )
   {
   }

   public void mouseEventDispatch( Object eventSource ,
                                   int modifier )
   {
   }

   public boolean keyEventDispatch( Object eventSource ,
                                    int keyCode )
   {
      if (triggerEventEnter(eventSource, keyCode)) {
         AnyError = 0;
         btn_enter( ) ;
         if (!cleanedUp) {
            VariablesToControls();
            set_caption( );
         }
         return true ;
      }
      if (keyCode == getContext().getClientPreferences().getKEY_PROMPT()) {
         return promptHandler(eventSource);
      }
      return false;
   }

   public boolean contextItemDispatch( Object eventSource ,
                                       String action )
   {
      return false;
   }

   public boolean isKeyControl( Object eventSource )
   {
      if ( ( edtContactTypesCode.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A365Contac, edtContactTypesCode.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean isBodyControl( Object eventSource )
   {
      if ( ( edtContactTypesDescription.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A366Contac, edtContactTypesDescription.getValue()) != 0 ) ) )
      {
         return true;
      }
      if ( ( chkContactTypesStatus.isEventSource(eventSource) ) && ( ( GXutil.strcmp(A1104Conta, chkContactTypesStatus.getValue()) != 0 ) ) )
      {
         return true;
      }
      return false;
   }

   public boolean menuActionPerformed( String action )
   {
      return false;
   }

   protected void resetCaption( )
   {
      resetCaption280( ) ;
   }

   protected void setAddCaption( )
   {
   }

   protected boolean getModeByParameter( )
   {
      return true ;
   }

   public IGXButton getCancelButton( )
   {
      return bttBtn_exit ;
   }

   public IGXButton getConfirmButton( )
   {
      return bttBtn_exit1 ;
   }

   public IGXButton getDeleteButton( )
   {
      return bttBtn_exit3 ;
   }

   public boolean promptHandler( Object eventSource )
   {
      return false;
   }

   public void deleteLineHandler( Object eventSource ,
                                  int row )
   {
   }

   public void setNoAccept( Object eventSource )
   {
      if ( edtContactTypesCode.isEventSource(eventSource) )
      {
         edtContactTypesCode.setEnabled(!( ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ) ));
      }
      if ( edtContactTypesDescription.isEventSource(eventSource) )
      {
         edtContactTypesDescription.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
      if ( chkContactTypesStatus.isEventSource(eventSource) )
      {
         chkContactTypesStatus.setEnabled(!( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  || ( GXutil.strcmp(Gx_mode, "DSP") == 0 )  ));
      }
   }

   protected void VariablesToControls( )
   {
      edtContactTypesCode.setValue( A365Contac );
      edtContactTypesDescription.setValue( A366Contac );
      chkContactTypesStatus.setValue( A1104Conta );
   }

   protected void ControlsToVariables( )
   {
      A365Contac = edtContactTypesCode.getValue() ;
      A366Contac = edtContactTypesDescription.getValue() ;
      A1104Conta = chkContactTypesStatus.getValue() ;
   }

   protected void eventNoLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelContext( )
   {
      ControlsToVariables();
   }

   protected void eventLevelResetContext( )
   {
   }

   protected void reloadGridRow( )
   {
   }

   public void valid_Contacttypescode( )
   {
      if ( ( GXutil.strcmp(A365Contac, K365Contac) != 0 ) || ( geteqAfterKey == 1 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            K365Contac = A365Contac ;
            getEqualNoModal( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               standaloneModalInsert( ) ;
            }
            VariablesToControls();
         }
         if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A365Contac), "") == 0 ) )
         {
            GXutil.msg( me(), "Code can not be null" );
            AnyError = (short)(1) ;
            setNextFocus( edtContactTypesCode );
            setFocusNext();
         }
         if ( ( AnyError == 0 ) )
         {
            geteqAfterKey = (byte)(0) ;
         }
         else
         {
            geteqAfterKey = (byte)(1) ;
         }
      }
      IsConfirmed = (short)(0) ;
      set_caption( ) ;
   }

   public void valid_Contacttypesdescription( )
   {
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A366Contac), "") == 0 ) )
      {
         GXutil.msg( me(), "Description can not be null" );
         AnyError = (short)(1) ;
         setNextFocus( edtContactTypesDescription );
         setFocusNext();
      }
   }

   public void e12282( )
   {
      eventNoLevelContext();
      /* Start Routine */
      AV13LgnLog = AV11Sessio.getValue("LOGGED") ;
      if ( ( GXutil.strcmp(AV13LgnLog, "") == 0 ) )
      {
      }
   }

   public void e13282( )
   {
      eventLevelContext();
      /* 'Back' Routine */
   }

   public void e11282( )
   {
      /* After Trn Routine */
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
      }
   }

   /* Server side code */
   /* Aggregate/select formulas */
   /* Vertical formulas */
   public void standaloneStartupServer( )
   {
      if ( ( AnyError == 0 ) )
      {
         standaloneNotModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "DSP") == 0 ) )
         {
            sMode223 = Gx_mode ;
            Gx_mode = "UPD" ;
            Gx_mode = sMode223 ;
         }
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            getByPrimaryKey( ) ;
            if ( ( RcdFound223 != 1 ) )
            {
               pushError( localUtil.getMessages().getMessage("noinsert") );
               AnyError = (short)(1) ;
               keepFocus();
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
         }
      }
      set_caption( ) ;
      PreviousBitmap = bttBtn_exit1.getBitmap() ;
   }

   public void zm28223( int GX_JID )
   {
      if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
         {
            Z366Contac = T00283_A366Contac[0] ;
            Z1104Conta = T00283_A1104Conta[0] ;
         }
         else
         {
            Z366Contac = A366Contac ;
            Z1104Conta = A1104Conta ;
         }
      }
      if ( ( GX_JID == -6 ) )
      {
         Z365Contac = A365Contac ;
         Z366Contac = A366Contac ;
         Z1104Conta = A1104Conta ;
      }
   }

   public void standaloneNotModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
      {
         A365Contac = AV12Contac ;
         edtContactTypesCode.setValue(A365Contac);
      }
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
      {
         edtContactTypesCode.setEnabled( 0 );
      }
      else
      {
         edtContactTypesCode.setEnabled( 1 );
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
      {
      }
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
      }
   }

   public void load28223( )
   {
      /* Using cursor T00284 */
      pr_default.execute(2, new Object[] {A365Contac});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound223 = (short)(1) ;
         A366Contac = T00284_A366Contac[0] ;
         A1104Conta = T00284_A1104Conta[0] ;
         zm28223( -6) ;
      }
      pr_default.close(2);
      onLoadActions28223( ) ;
   }

   public void onLoadActions28223( )
   {
   }

   public void checkExtendedTable28223( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      Gx_BScreen = (byte)(0) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A365Contac), "") == 0 ) )
      {
         pushError( "Code can not be null" );
         AnyError = (short)(1) ;
         keepFocus();
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A366Contac), "") == 0 ) )
      {
         pushError( "Description can not be null" );
         AnyError = (short)(1) ;
         keepFocus();
      }
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors28223( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey28223( )
   {
      /* Using cursor T00285 */
      pr_default.execute(3, new Object[] {A365Contac});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound223 = (short)(1) ;
      }
      else
      {
         RcdFound223 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00283 */
      pr_default.execute(1, new Object[] {A365Contac});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm28223( 6) ;
         RcdFound223 = (short)(1) ;
         A365Contac = T00283_A365Contac[0] ;
         A366Contac = T00283_A366Contac[0] ;
         A1104Conta = T00283_A1104Conta[0] ;
         Z365Contac = A365Contac ;
         sMode223 = Gx_mode ;
         Gx_mode = "DSP" ;
         load28223( ) ;
         Gx_mode = sMode223 ;
      }
      else
      {
         RcdFound223 = (short)(0) ;
         initializeNonKey28223( ) ;
         sMode223 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode223 ;
      }
      K365Contac = A365Contac ;
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey28223( ) ;
      if ( ( RcdFound223 == 0 ) )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound223 = (short)(0) ;
      /* Using cursor T00286 */
      pr_default.execute(4, new Object[] {A365Contac});
      if ( (pr_default.getStatus(4) != 101) )
      {
         while ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T00286_A365Contac[0], A365Contac) < 0 ) ) )
         {
            pr_default.readNext(4);
         }
         if ( (pr_default.getStatus(4) != 101) && ( ( GXutil.strcmp(T00286_A365Contac[0], A365Contac) > 0 ) ) )
         {
            A365Contac = T00286_A365Contac[0] ;
            RcdFound223 = (short)(1) ;
         }
      }
      pr_default.close(4);
   }

   public void move_previous( )
   {
      RcdFound223 = (short)(0) ;
      /* Using cursor T00287 */
      pr_default.execute(5, new Object[] {A365Contac});
      if ( (pr_default.getStatus(5) != 101) )
      {
         while ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T00287_A365Contac[0], A365Contac) > 0 ) ) )
         {
            pr_default.readNext(5);
         }
         if ( (pr_default.getStatus(5) != 101) && ( ( GXutil.strcmp(T00287_A365Contac[0], A365Contac) < 0 ) ) )
         {
            A365Contac = T00287_A365Contac[0] ;
            RcdFound223 = (short)(1) ;
         }
      }
      pr_default.close(5);
   }

   public void btn_enter( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
      {
         btn_delete( ) ;
         if	(loopOnce) cleanup();
         return  ;
      }
      nKeyPressed = (byte)(1) ;
      getKey28223( ) ;
      if ( ( RcdFound223 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("noupdate") );
            AnyError = (short)(1) ;
            setNextFocus( edtContactTypesCode );
         }
         else if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
         {
            A365Contac = Z365Contac ;
            edtContactTypesCode.setValue(A365Contac);
            pushError( localUtil.getMessages().getMessage("getbeforeupd") );
            AnyError = (short)(1) ;
            setNextFocus( edtContactTypesCode );
         }
         else
         {
            /* Update record */
            update28223( ) ;
            setNextFocus( edtContactTypesCode );
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
         {
            /* Insert record */
            insert28223( ) ;
            setNextFocus( edtContactTypesCode );
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               pushError( localUtil.getMessages().getMessage("recdeleted") );
               AnyError = (short)(1) ;
               setNextFocus( edtContactTypesCode );
            }
            else
            {
               /* Insert record */
               insert28223( ) ;
               setNextFocus( edtContactTypesCode );
            }
         }
      }
      handleErrors();
      afterTrn( ) ;
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
      AnyError = (short)(0) ;
      if	(loopOnce) cleanup();
   }

   public void btn_delete( )
   {
      if ( ( GXutil.strcmp(A365Contac, Z365Contac) != 0 ) )
      {
         A365Contac = Z365Contac ;
         edtContactTypesCode.setValue(A365Contac);
         pushError( localUtil.getMessages().getMessage("getbeforedlt") );
         AnyError = (short)(1) ;
         setNextFocus( edtContactTypesCode );
      }
      else
      {
         delete( ) ;
         handleErrors();
         afterTrn( ) ;
         setNextFocus( edtContactTypesCode );
      }
      if ( ( AnyError != 0 ) )
      {
      }
      if ( ( sanomodError == 0 ) )
      {
         handleErrors();
      }
      setFocusNext();
   }

   public void checkOptimisticConcurrency28223( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor T00282 */
         pr_default.execute(0, new Object[] {A365Contac});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            pushError( localUtil.getMessages().getMessage("lock", new Object[] {"CONTACTTYPES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z366Contac, T00282_A366Contac[0]) != 0 ) || ( GXutil.strcmp(Z1104Conta, T00282_A1104Conta[0]) != 0 ) )
         {
            pushError( localUtil.getMessages().getMessage("waschg", new Object[] {"CONTACTTYPES"}) );
            AnyError = (short)(1) ;
            keepFocus();
            return  ;
         }
      }
   }

   public void insert28223( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm28223( 0) ;
         checkOptimisticConcurrency28223( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm28223( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert28223( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T00288 */
                  pr_default.execute(6, new Object[] {A365Contac, A366Contac, A1104Conta});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     pushError( localUtil.getMessages().getMessage("noupdate") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        context.msgStatus( localUtil.getMessages().getMessage("sucadded") );
                        resetCaption280( ) ;
                     }
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
         else
         {
            load28223( ) ;
         }
         endLevel28223( ) ;
      }
      closeExtendedTableCursors28223( ) ;
   }

   public void update28223( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency28223( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm28223( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate28223( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor T00289 */
                  pr_default.execute(7, new Object[] {A366Contac, A1104Conta, A365Contac});
                  deferredUpdate28223( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        loopOnce = true;
                     }
                  }
                  else
                  {
                     pushError( localUtil.getMessages().getMessage("unexp") );
                     AnyError = (short)(1) ;
                     keepFocus();
                  }
               }
            }
         }
         endLevel28223( ) ;
      }
      closeExtendedTableCursors28223( ) ;
   }

   public void deferredUpdate28223( )
   {
   }

   public void delete( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency28223( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls28223( ) ;
         /* No cascading delete specified. */
         afterConfirm28223( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete28223( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor T002810 */
               pr_default.execute(8, new Object[] {A365Contac});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     loopOnce = true;
                  }
               }
               else
               {
                  pushError( localUtil.getMessages().getMessage("unexp") );
                  AnyError = (short)(1) ;
                  keepFocus();
               }
            }
         }
      }
      sMode223 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel28223( ) ;
      Gx_mode = sMode223 ;
   }

   public void onDeleteControls28223( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor T002811 */
         pr_default.execute(9, new Object[] {A365Contac});
         if ( (pr_default.getStatus(9) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"Contacts"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(9);
         /* Using cursor T002812 */
         pr_default.execute(10, new Object[] {A365Contac});
         if ( (pr_default.getStatus(10) != 101) )
         {
            pushError( localUtil.getMessages().getMessage("del", new Object[] {"Contact Types"}) );
            AnyError = (short)(1) ;
            keepFocus();
         }
         pr_default.close(10);
      }
   }

   public void endLevel28223( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete28223( ) ;
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "tcontacttypes");
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "tcontacttypes");
      }
      IsModified = (short)(0) ;
   }

   public void scanStart28223( )
   {
      /* Using cursor T002813 */
      pr_default.execute(11);
      RcdFound223 = (short)(0) ;
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound223 = (short)(1) ;
         A365Contac = T002813_A365Contac[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext28223( )
   {
      pr_default.readNext(11);
      RcdFound223 = (short)(0) ;
      if ( (pr_default.getStatus(11) != 101) )
      {
         RcdFound223 = (short)(1) ;
         A365Contac = T002813_A365Contac[0] ;
      }
   }

   public void scanEnd28223( )
   {
      pr_default.close(11);
   }

   public void afterConfirm28223( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert28223( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate28223( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete28223( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete28223( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate28223( )
   {
      /* Before Validate Rules */
   }

   public void confirm_280( )
   {
      beforeValidate28223( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls28223( ) ;
         }
         else
         {
            checkExtendedTable28223( ) ;
            closeExtendedTableCursors28223( ) ;
         }
      }
      if ( ( AnyError == 0 ) && ( sanomodError == 0 ) )
      {
         PreviousBitmap = bttBtn_exit1.getBitmap() ;
         PreviousTooltip = bttBtn_exit1.getTooltip() ;
         IsConfirmed = (short)(1) ;
      }
   }

   protected void cleanup( )
   {
      if (cleanedUp) {
         return  ;
      }
      if (!exitExecuted) {
         exitExecuted = true;
         GXExit();
      }
      CloseOpenCursors();
      super.cleanup();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   public void initialize( )
   {
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousBitmap = "" ;
      A366Contac = "" ;
      A1104Conta = "" ;
      A365Contac = "" ;
      K365Contac = "" ;
      lastAnyError = 0 ;
      AV13LgnLog = "" ;
      AV11Sessio = null; //httpContext.getWebSession();
      sMode223 = "" ;
      RcdFound223 = (short)(0) ;
      Z366Contac = "" ;
      Z1104Conta = "" ;
      scmdbuf = "" ;
      GX_JID = 0 ;
      Z365Contac = "" ;
      Gx_BScreen = (byte)(0) ;
      T00284_A365Contac = new String[] {""} ;
      T00284_A366Contac = new String[] {""} ;
      T00284_A1104Conta = new String[] {""} ;
      T00285_A365Contac = new String[] {""} ;
      T00283_A365Contac = new String[] {""} ;
      T00283_A366Contac = new String[] {""} ;
      T00283_A1104Conta = new String[] {""} ;
      T00286_A365Contac = new String[] {""} ;
      T00287_A365Contac = new String[] {""} ;
      T00282_A365Contac = new String[] {""} ;
      T00282_A366Contac = new String[] {""} ;
      T00282_A1104Conta = new String[] {""} ;
      T002811_A23ISOCod = new String[] {""} ;
      T002811_A1136AirLi = new String[] {""} ;
      T002811_A365Contac = new String[] {""} ;
      T002811_A1138AirLi = new short[1] ;
      T002812_A23ISOCod = new String[] {""} ;
      T002812_A365Contac = new String[] {""} ;
      T002812_A406ISOCtt = new short[1] ;
      T002813_A365Contac = new String[] {""} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new tcontacttypes__default(),
         new Object[] {
             new Object[] {
            T00282_A365Contac, T00282_A366Contac, T00282_A1104Conta
            }
            , new Object[] {
            T00283_A365Contac, T00283_A366Contac, T00283_A1104Conta
            }
            , new Object[] {
            T00284_A365Contac, T00284_A366Contac, T00284_A1104Conta
            }
            , new Object[] {
            T00285_A365Contac
            }
            , new Object[] {
            T00286_A365Contac
            }
            , new Object[] {
            T00287_A365Contac
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T002811_A23ISOCod, T002811_A1136AirLi, T002811_A365Contac, T002811_A1138AirLi
            }
            , new Object[] {
            T002812_A23ISOCod, T002812_A365Contac, T002812_A406ISOCtt
            }
            , new Object[] {
            T002813_A365Contac
            }
         }
      );
      reloadDynamicLists(0);
      A1104Conta = "N" ;
      chkContactTypesStatus.setValue(A1104Conta);
   }

   protected byte nKeyPressed ;
   protected byte geteqAfterKey= 1 ;
   protected byte Gx_BScreen ;
   protected short IsConfirmed ;
   protected short IsModified ;
   protected short RcdFound223 ;
   protected int trnEnded ;
   protected int lastAnyError ;
   protected int GX_JID ;
   protected String PreviousTooltip ;
   protected String PreviousBitmap ;
   protected String A1104Conta ;
   protected String sMode223 ;
   protected String Z1104Conta ;
   protected String scmdbuf ;
   protected String A366Contac ;
   protected String A365Contac ;
   protected String K365Contac ;
   protected String AV12Contac ;
   protected String AV13LgnLog ;
   protected String Z366Contac ;
   protected String Z365Contac ;
   protected com.genexus.webpanels.WebSession AV11Sessio ;
   protected GXPanel GXPanel1 ;
   protected GUIObjectString edtContactTypesCode ;
   protected GUIObjectString edtContactTypesDescription ;
   protected GUIObjectString chkContactTypesStatus ;
   protected IGXButton bttBtn_first ;
   protected IGXButton bttBtn_prev ;
   protected IGXButton bttBtn_next ;
   protected IGXButton bttBtn_last ;
   protected IGXButton bttBtn_exit2 ;
   protected IGXButton bttBtn_exit3 ;
   protected IGXButton bttBtn_exit1 ;
   protected IGXButton bttBtn_exit ;
   protected ILabel lbllbl12 ;
   protected ILabel lbllbl14 ;
   protected IGXImage imgimg7 ;
   protected IDataStoreProvider pr_default ;
   protected String[] T00284_A365Contac ;
   protected String[] T00284_A366Contac ;
   protected String[] T00284_A1104Conta ;
   protected String[] T00285_A365Contac ;
   protected String[] T00283_A365Contac ;
   protected String[] T00283_A366Contac ;
   protected String[] T00283_A1104Conta ;
   protected String[] T00286_A365Contac ;
   protected String[] T00287_A365Contac ;
   protected String[] T00282_A365Contac ;
   protected String[] T00282_A366Contac ;
   protected String[] T00282_A1104Conta ;
   protected String[] T002811_A23ISOCod ;
   protected String[] T002811_A1136AirLi ;
   protected String[] T002811_A365Contac ;
   protected short[] T002811_A1138AirLi ;
   protected String[] T002812_A23ISOCod ;
   protected String[] T002812_A365Contac ;
   protected short[] T002812_A406ISOCtt ;
   protected String[] T002813_A365Contac ;
}

final  class tcontacttypes__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00282", "SELECT [ContactTypesCode], [ContactTypesDescription], [ContactTypesStatus] FROM [CONTACTTYPES] WITH (UPDLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00283", "SELECT [ContactTypesCode], [ContactTypesDescription], [ContactTypesStatus] FROM [CONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00284", "SELECT TM1.[ContactTypesCode], TM1.[ContactTypesDescription], TM1.[ContactTypesStatus] FROM [CONTACTTYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ContactTypesCode] = ? ORDER BY TM1.[ContactTypesCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00285", "SELECT [ContactTypesCode] FROM [CONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("T00286", "SELECT TOP 1 [ContactTypesCode] FROM [CONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ContactTypesCode] > ?) ORDER BY [ContactTypesCode] ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T00287", "SELECT TOP 1 [ContactTypesCode] FROM [CONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE ( [ContactTypesCode] < ?) ORDER BY [ContactTypesCode] DESC ",true, GX_NOMASK, false, this,0,true )
         ,new UpdateCursor("T00288", "INSERT INTO [CONTACTTYPES] ([ContactTypesCode], [ContactTypesDescription], [ContactTypesStatus]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T00289", "UPDATE [CONTACTTYPES] SET [ContactTypesDescription]=?, [ContactTypesStatus]=?  WHERE [ContactTypesCode] = ?", GX_NOMASK)
         ,new UpdateCursor("T002810", "DELETE FROM [CONTACTTYPES]  WHERE [ContactTypesCode] = ?", GX_NOMASK)
         ,new ForEachCursor("T002811", "SELECT TOP 1 [ISOCod], [AirLineCode], [ContactTypesCode], [AirLineCttSeq] FROM [AIRLINESCONTACTS] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002812", "SELECT TOP 1 [ISOCod], [ContactTypesCode], [ISOCttSeq] FROM [COUNTRYCONTACTTYPES] WITH (NOLOCK) WHERE [ContactTypesCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("T002813", "SELECT [ContactTypesCode] FROM [CONTACTTYPES] WITH (FASTFIRSTROW NOLOCK) ORDER BY [ContactTypesCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 9 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               stmt.setVarchar(2, (String)parms[1], 30, false);
               stmt.setString(3, (String)parms[2], 1);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setString(2, (String)parms[1], 1);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 9 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 10 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
      }
   }

}


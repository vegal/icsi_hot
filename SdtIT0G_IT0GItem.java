import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtIT0G_IT0GItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtIT0G_IT0GItem( )
   {
      this(  new ModelContext(SdtIT0G_IT0GItem.class));
   }

   public SdtIT0G_IT0GItem( ModelContext context )
   {
      super( context, "SdtIT0G_IT0GItem");
   }

   public SdtIT0G_IT0GItem( StructSdtIT0G_IT0GItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMCP_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emcp_it0g = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMCV_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emcv_it0g = GXutil.lval( oReader.getValue()) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CUTP_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Cutp_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMRT_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emrt_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMRC_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emrc_it0g = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMSC_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emsc_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMOC_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emoc_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "XBOA_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Xboa_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "XBRU_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Xbru_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "XBNE_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Xbne_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMRM_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emrm_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EMCI_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Emci_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "XBCT_IT0G") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0G_IT0GItem_Xbct_it0g = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "IT0G.IT0GItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("EMCP_IT0G", GXutil.trim( GXutil.str( gxTv_SdtIT0G_IT0GItem_Emcp_it0g, 1, 0)));
      oWriter.writeElement("EMCV_IT0G", GXutil.trim( GXutil.str( gxTv_SdtIT0G_IT0GItem_Emcv_it0g, 11, 0)));
      oWriter.writeElement("CUTP_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Cutp_it0g));
      oWriter.writeElement("EMRT_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Emrt_it0g));
      oWriter.writeElement("EMRC_IT0G", GXutil.trim( GXutil.str( gxTv_SdtIT0G_IT0GItem_Emrc_it0g, 1, 0)));
      oWriter.writeElement("EMSC_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Emsc_it0g));
      oWriter.writeElement("EMOC_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Emoc_it0g));
      oWriter.writeElement("XBOA_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Xboa_it0g));
      oWriter.writeElement("XBRU_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Xbru_it0g));
      oWriter.writeElement("XBNE_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Xbne_it0g));
      oWriter.writeElement("EMRM_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Emrm_it0g));
      oWriter.writeElement("EMCI_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Emci_it0g));
      oWriter.writeElement("XBCT_IT0G", GXutil.rtrim( gxTv_SdtIT0G_IT0GItem_Xbct_it0g));
      oWriter.writeEndElement();
      return  ;
   }

   public byte getgxTv_SdtIT0G_IT0GItem_Emcp_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emcp_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emcp_it0g( byte value )
   {
      gxTv_SdtIT0G_IT0GItem_Emcp_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emcp_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emcp_it0g = (byte)(0) ;
      return  ;
   }

   public long getgxTv_SdtIT0G_IT0GItem_Emcv_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emcv_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emcv_it0g( long value )
   {
      gxTv_SdtIT0G_IT0GItem_Emcv_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emcv_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emcv_it0g = 0 ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Cutp_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Cutp_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Cutp_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Cutp_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Cutp_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Cutp_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Emrt_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emrt_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emrt_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emrt_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emrt_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emrt_it0g = "" ;
      return  ;
   }

   public byte getgxTv_SdtIT0G_IT0GItem_Emrc_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emrc_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emrc_it0g( byte value )
   {
      gxTv_SdtIT0G_IT0GItem_Emrc_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emrc_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emrc_it0g = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Emsc_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emsc_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emsc_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emsc_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emsc_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emsc_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Emoc_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emoc_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emoc_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emoc_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emoc_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emoc_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Xboa_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xboa_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xboa_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xboa_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xboa_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Xboa_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Xbru_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xbru_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xbru_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xbru_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xbru_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Xbru_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Xbne_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xbne_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xbne_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xbne_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xbne_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Xbne_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Emrm_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emrm_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emrm_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emrm_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emrm_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emrm_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Emci_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Emci_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emci_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Emci_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Emci_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Emci_it0g = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0G_IT0GItem_Xbct_it0g( )
   {
      return gxTv_SdtIT0G_IT0GItem_Xbct_it0g ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xbct_it0g( String value )
   {
      gxTv_SdtIT0G_IT0GItem_Xbct_it0g = value ;
      return  ;
   }

   public void setgxTv_SdtIT0G_IT0GItem_Xbct_it0g_SetNull( )
   {
      gxTv_SdtIT0G_IT0GItem_Xbct_it0g = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtIT0G_IT0GItem_Emcp_it0g = (byte)(0) ;
      gxTv_SdtIT0G_IT0GItem_Emcv_it0g = 0 ;
      gxTv_SdtIT0G_IT0GItem_Cutp_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emrt_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emrc_it0g = (byte)(0) ;
      gxTv_SdtIT0G_IT0GItem_Emsc_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emoc_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xboa_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xbru_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xbne_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emrm_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Emci_it0g = "" ;
      gxTv_SdtIT0G_IT0GItem_Xbct_it0g = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char5 = "" ;
      return  ;
   }

   public SdtIT0G_IT0GItem Clone( )
   {
      return (SdtIT0G_IT0GItem)(clone()) ;
   }

   public void setStruct( StructSdtIT0G_IT0GItem struct )
   {
      setgxTv_SdtIT0G_IT0GItem_Emcp_it0g(struct.getEmcp_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emcv_it0g(struct.getEmcv_it0g());
      setgxTv_SdtIT0G_IT0GItem_Cutp_it0g(struct.getCutp_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emrt_it0g(struct.getEmrt_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emrc_it0g(struct.getEmrc_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emsc_it0g(struct.getEmsc_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emoc_it0g(struct.getEmoc_it0g());
      setgxTv_SdtIT0G_IT0GItem_Xboa_it0g(struct.getXboa_it0g());
      setgxTv_SdtIT0G_IT0GItem_Xbru_it0g(struct.getXbru_it0g());
      setgxTv_SdtIT0G_IT0GItem_Xbne_it0g(struct.getXbne_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emrm_it0g(struct.getEmrm_it0g());
      setgxTv_SdtIT0G_IT0GItem_Emci_it0g(struct.getEmci_it0g());
      setgxTv_SdtIT0G_IT0GItem_Xbct_it0g(struct.getXbct_it0g());
   }

   public StructSdtIT0G_IT0GItem getStruct( )
   {
      StructSdtIT0G_IT0GItem struct = new StructSdtIT0G_IT0GItem ();
      struct.setEmcp_it0g(getgxTv_SdtIT0G_IT0GItem_Emcp_it0g());
      struct.setEmcv_it0g(getgxTv_SdtIT0G_IT0GItem_Emcv_it0g());
      struct.setCutp_it0g(getgxTv_SdtIT0G_IT0GItem_Cutp_it0g());
      struct.setEmrt_it0g(getgxTv_SdtIT0G_IT0GItem_Emrt_it0g());
      struct.setEmrc_it0g(getgxTv_SdtIT0G_IT0GItem_Emrc_it0g());
      struct.setEmsc_it0g(getgxTv_SdtIT0G_IT0GItem_Emsc_it0g());
      struct.setEmoc_it0g(getgxTv_SdtIT0G_IT0GItem_Emoc_it0g());
      struct.setXboa_it0g(getgxTv_SdtIT0G_IT0GItem_Xboa_it0g());
      struct.setXbru_it0g(getgxTv_SdtIT0G_IT0GItem_Xbru_it0g());
      struct.setXbne_it0g(getgxTv_SdtIT0G_IT0GItem_Xbne_it0g());
      struct.setEmrm_it0g(getgxTv_SdtIT0G_IT0GItem_Emrm_it0g());
      struct.setEmci_it0g(getgxTv_SdtIT0G_IT0GItem_Emci_it0g());
      struct.setXbct_it0g(getgxTv_SdtIT0G_IT0GItem_Xbct_it0g());
      return struct ;
   }

   private byte gxTv_SdtIT0G_IT0GItem_Emcp_it0g ;
   private byte gxTv_SdtIT0G_IT0GItem_Emrc_it0g ;
   private short nOutParmCount ;
   private short readOk ;
   private long gxTv_SdtIT0G_IT0GItem_Emcv_it0g ;
   private String sTagName ;
   private String GXt_char5 ;
   private String gxTv_SdtIT0G_IT0GItem_Cutp_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Emrt_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Emsc_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Emoc_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Xboa_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Xbru_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Xbne_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Emrm_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Emci_it0g ;
   private String gxTv_SdtIT0G_IT0GItem_Xbct_it0g ;
}


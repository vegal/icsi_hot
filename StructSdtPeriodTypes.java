
public final  class StructSdtPeriodTypes implements Cloneable, java.io.Serializable
{
   public StructSdtPeriodTypes( )
   {
      gxTv_SdtPeriodTypes_Periodtypescode = "" ;
      gxTv_SdtPeriodTypes_Periodtypesdescription = "" ;
      gxTv_SdtPeriodTypes_Periodtypesstatus = "" ;
      gxTv_SdtPeriodTypes_Mode = "" ;
      gxTv_SdtPeriodTypes_Periodtypescode_Z = "" ;
      gxTv_SdtPeriodTypes_Periodtypesdescription_Z = "" ;
      gxTv_SdtPeriodTypes_Periodtypesstatus_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getPeriodtypescode( )
   {
      return gxTv_SdtPeriodTypes_Periodtypescode ;
   }

   public void setPeriodtypescode( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypescode = value ;
      return  ;
   }

   public String getPeriodtypesdescription( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesdescription ;
   }

   public void setPeriodtypesdescription( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesdescription = value ;
      return  ;
   }

   public String getPeriodtypesstatus( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesstatus ;
   }

   public void setPeriodtypesstatus( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesstatus = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtPeriodTypes_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtPeriodTypes_Mode = value ;
      return  ;
   }

   public String getPeriodtypescode_Z( )
   {
      return gxTv_SdtPeriodTypes_Periodtypescode_Z ;
   }

   public void setPeriodtypescode_Z( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypescode_Z = value ;
      return  ;
   }

   public String getPeriodtypesdescription_Z( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesdescription_Z ;
   }

   public void setPeriodtypesdescription_Z( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesdescription_Z = value ;
      return  ;
   }

   public String getPeriodtypesstatus_Z( )
   {
      return gxTv_SdtPeriodTypes_Periodtypesstatus_Z ;
   }

   public void setPeriodtypesstatus_Z( String value )
   {
      gxTv_SdtPeriodTypes_Periodtypesstatus_Z = value ;
      return  ;
   }

   protected String gxTv_SdtPeriodTypes_Periodtypesstatus ;
   protected String gxTv_SdtPeriodTypes_Mode ;
   protected String gxTv_SdtPeriodTypes_Periodtypesstatus_Z ;
   protected String gxTv_SdtPeriodTypes_Periodtypescode ;
   protected String gxTv_SdtPeriodTypes_Periodtypesdescription ;
   protected String gxTv_SdtPeriodTypes_Periodtypescode_Z ;
   protected String gxTv_SdtPeriodTypes_Periodtypesdescription_Z ;
}


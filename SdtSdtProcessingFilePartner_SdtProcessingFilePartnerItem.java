import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem( )
   {
      this(  new ModelContext(SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem.class));
   }

   public SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem( ModelContext context )
   {
      super( context, "SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem");
   }

   public SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem( StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Code") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "TradeName") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Warning") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "SdtProcessingFilePartner.SdtProcessingFilePartnerItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("Code", GXutil.rtrim( gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code));
      oWriter.writeElement("TradeName", GXutil.rtrim( gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename));
      oWriter.writeElement("Warning", GXutil.rtrim( gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code( )
   {
      return gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code ;
   }

   public void setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code( String value )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code = value ;
      return  ;
   }

   public void setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code_SetNull( )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code = "" ;
      return  ;
   }

   public String getgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename( )
   {
      return gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename ;
   }

   public void setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename( String value )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename = value ;
      return  ;
   }

   public void setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename_SetNull( )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename = "" ;
      return  ;
   }

   public String getgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning( )
   {
      return gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning ;
   }

   public void setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning( String value )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning = value ;
      return  ;
   }

   public void setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning_SetNull( )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code = "" ;
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename = "" ;
      gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      return  ;
   }

   public SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem Clone( )
   {
      return (SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem)(clone()) ;
   }

   public void setStruct( StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem struct )
   {
      setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code(struct.getCode());
      setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename(struct.getTradename());
      setgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning(struct.getWarning());
   }

   public StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem getStruct( )
   {
      StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem struct = new StructSdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem ();
      struct.setCode(getgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code());
      struct.setTradename(getgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename());
      struct.setWarning(getgxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning());
      return struct ;
   }

   private short nOutParmCount ;
   private short readOk ;
   private String gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Code ;
   private String gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Tradename ;
   private String gxTv_SdtSdtProcessingFilePartner_SdtProcessingFilePartnerItem_Warning ;
   private String sTagName ;
   private String GXt_char2 ;
}



public final  class StructSdtPeriods implements Cloneable, java.io.Serializable
{
   public StructSdtPeriods( )
   {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      cal.set(1, 0, 1, 0, 0, 0);
      cal.set(java.util.Calendar.MILLISECOND, 0);
      gxTv_SdtPeriods_Isocod = "" ;
      gxTv_SdtPeriods_Curcode = "" ;
      gxTv_SdtPeriods_Periodtypescode = "" ;
      gxTv_SdtPeriods_Perid = "" ;
      gxTv_SdtPeriods_Isodes = "" ;
      gxTv_SdtPeriods_Curdescription = "" ;
      gxTv_SdtPeriods_Perdescr = "" ;
      gxTv_SdtPeriods_Perdatini = cal.getTime() ;
      gxTv_SdtPeriods_Perdatend = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdisini = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdisfin = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdueini = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdueend = cal.getTime() ;
      gxTv_SdtPeriods_Perclose = "" ;
      gxTv_SdtPeriods_Perdatins = cal.getTime() ;
      gxTv_SdtPeriods_Perdatupd = cal.getTime() ;
      gxTv_SdtPeriods_Peridadm = "" ;
      gxTv_SdtPeriods_Mode = "" ;
      gxTv_SdtPeriods_Isocod_Z = "" ;
      gxTv_SdtPeriods_Curcode_Z = "" ;
      gxTv_SdtPeriods_Periodtypescode_Z = "" ;
      gxTv_SdtPeriods_Perid_Z = "" ;
      gxTv_SdtPeriods_Isodes_Z = "" ;
      gxTv_SdtPeriods_Curdescription_Z = "" ;
      gxTv_SdtPeriods_Perdescr_Z = "" ;
      gxTv_SdtPeriods_Perdatini_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perdatend_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdisini_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdisfin_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdueini_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perdatdueend_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perclose_Z = "" ;
      gxTv_SdtPeriods_Perdatins_Z = cal.getTime() ;
      gxTv_SdtPeriods_Perdatupd_Z = cal.getTime() ;
      gxTv_SdtPeriods_Peridadm_Z = "" ;
      gxTv_SdtPeriods_Isodes_N = (byte)(0) ;
      gxTv_SdtPeriods_Curdescription_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisini_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisfin_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getIsocod( )
   {
      return gxTv_SdtPeriods_Isocod ;
   }

   public void setIsocod( String value )
   {
      gxTv_SdtPeriods_Isocod = value ;
      return  ;
   }

   public String getCurcode( )
   {
      return gxTv_SdtPeriods_Curcode ;
   }

   public void setCurcode( String value )
   {
      gxTv_SdtPeriods_Curcode = value ;
      return  ;
   }

   public String getPeriodtypescode( )
   {
      return gxTv_SdtPeriods_Periodtypescode ;
   }

   public void setPeriodtypescode( String value )
   {
      gxTv_SdtPeriods_Periodtypescode = value ;
      return  ;
   }

   public String getPerid( )
   {
      return gxTv_SdtPeriods_Perid ;
   }

   public void setPerid( String value )
   {
      gxTv_SdtPeriods_Perid = value ;
      return  ;
   }

   public String getIsodes( )
   {
      return gxTv_SdtPeriods_Isodes ;
   }

   public void setIsodes( String value )
   {
      gxTv_SdtPeriods_Isodes = value ;
      return  ;
   }

   public String getCurdescription( )
   {
      return gxTv_SdtPeriods_Curdescription ;
   }

   public void setCurdescription( String value )
   {
      gxTv_SdtPeriods_Curdescription = value ;
      return  ;
   }

   public String getPerdescr( )
   {
      return gxTv_SdtPeriods_Perdescr ;
   }

   public void setPerdescr( String value )
   {
      gxTv_SdtPeriods_Perdescr = value ;
      return  ;
   }

   public java.util.Date getPerdatini( )
   {
      return gxTv_SdtPeriods_Perdatini ;
   }

   public void setPerdatini( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatini = value ;
      return  ;
   }

   public java.util.Date getPerdatend( )
   {
      return gxTv_SdtPeriods_Perdatend ;
   }

   public void setPerdatend( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatend = value ;
      return  ;
   }

   public java.util.Date getPerdatdisini( )
   {
      return gxTv_SdtPeriods_Perdatdisini ;
   }

   public void setPerdatdisini( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisini = value ;
      return  ;
   }

   public java.util.Date getPerdatdisfin( )
   {
      return gxTv_SdtPeriods_Perdatdisfin ;
   }

   public void setPerdatdisfin( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisfin = value ;
      return  ;
   }

   public java.util.Date getPerdatdueini( )
   {
      return gxTv_SdtPeriods_Perdatdueini ;
   }

   public void setPerdatdueini( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueini = value ;
      return  ;
   }

   public java.util.Date getPerdatdueend( )
   {
      return gxTv_SdtPeriods_Perdatdueend ;
   }

   public void setPerdatdueend( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueend = value ;
      return  ;
   }

   public String getPerclose( )
   {
      return gxTv_SdtPeriods_Perclose ;
   }

   public void setPerclose( String value )
   {
      gxTv_SdtPeriods_Perclose = value ;
      return  ;
   }

   public java.util.Date getPerdatins( )
   {
      return gxTv_SdtPeriods_Perdatins ;
   }

   public void setPerdatins( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatins = value ;
      return  ;
   }

   public java.util.Date getPerdatupd( )
   {
      return gxTv_SdtPeriods_Perdatupd ;
   }

   public void setPerdatupd( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatupd = value ;
      return  ;
   }

   public String getPeridadm( )
   {
      return gxTv_SdtPeriods_Peridadm ;
   }

   public void setPeridadm( String value )
   {
      gxTv_SdtPeriods_Peridadm = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtPeriods_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtPeriods_Mode = value ;
      return  ;
   }

   public String getIsocod_Z( )
   {
      return gxTv_SdtPeriods_Isocod_Z ;
   }

   public void setIsocod_Z( String value )
   {
      gxTv_SdtPeriods_Isocod_Z = value ;
      return  ;
   }

   public String getCurcode_Z( )
   {
      return gxTv_SdtPeriods_Curcode_Z ;
   }

   public void setCurcode_Z( String value )
   {
      gxTv_SdtPeriods_Curcode_Z = value ;
      return  ;
   }

   public String getPeriodtypescode_Z( )
   {
      return gxTv_SdtPeriods_Periodtypescode_Z ;
   }

   public void setPeriodtypescode_Z( String value )
   {
      gxTv_SdtPeriods_Periodtypescode_Z = value ;
      return  ;
   }

   public String getPerid_Z( )
   {
      return gxTv_SdtPeriods_Perid_Z ;
   }

   public void setPerid_Z( String value )
   {
      gxTv_SdtPeriods_Perid_Z = value ;
      return  ;
   }

   public String getIsodes_Z( )
   {
      return gxTv_SdtPeriods_Isodes_Z ;
   }

   public void setIsodes_Z( String value )
   {
      gxTv_SdtPeriods_Isodes_Z = value ;
      return  ;
   }

   public String getCurdescription_Z( )
   {
      return gxTv_SdtPeriods_Curdescription_Z ;
   }

   public void setCurdescription_Z( String value )
   {
      gxTv_SdtPeriods_Curdescription_Z = value ;
      return  ;
   }

   public String getPerdescr_Z( )
   {
      return gxTv_SdtPeriods_Perdescr_Z ;
   }

   public void setPerdescr_Z( String value )
   {
      gxTv_SdtPeriods_Perdescr_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatini_Z( )
   {
      return gxTv_SdtPeriods_Perdatini_Z ;
   }

   public void setPerdatini_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatini_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatend_Z( )
   {
      return gxTv_SdtPeriods_Perdatend_Z ;
   }

   public void setPerdatend_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatend_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatdisini_Z( )
   {
      return gxTv_SdtPeriods_Perdatdisini_Z ;
   }

   public void setPerdatdisini_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisini_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatdisfin_Z( )
   {
      return gxTv_SdtPeriods_Perdatdisfin_Z ;
   }

   public void setPerdatdisfin_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisfin_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatdueini_Z( )
   {
      return gxTv_SdtPeriods_Perdatdueini_Z ;
   }

   public void setPerdatdueini_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueini_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatdueend_Z( )
   {
      return gxTv_SdtPeriods_Perdatdueend_Z ;
   }

   public void setPerdatdueend_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueend_Z = value ;
      return  ;
   }

   public String getPerclose_Z( )
   {
      return gxTv_SdtPeriods_Perclose_Z ;
   }

   public void setPerclose_Z( String value )
   {
      gxTv_SdtPeriods_Perclose_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatins_Z( )
   {
      return gxTv_SdtPeriods_Perdatins_Z ;
   }

   public void setPerdatins_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatins_Z = value ;
      return  ;
   }

   public java.util.Date getPerdatupd_Z( )
   {
      return gxTv_SdtPeriods_Perdatupd_Z ;
   }

   public void setPerdatupd_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatupd_Z = value ;
      return  ;
   }

   public String getPeridadm_Z( )
   {
      return gxTv_SdtPeriods_Peridadm_Z ;
   }

   public void setPeridadm_Z( String value )
   {
      gxTv_SdtPeriods_Peridadm_Z = value ;
      return  ;
   }

   public byte getIsodes_N( )
   {
      return gxTv_SdtPeriods_Isodes_N ;
   }

   public void setIsodes_N( byte value )
   {
      gxTv_SdtPeriods_Isodes_N = value ;
      return  ;
   }

   public byte getCurdescription_N( )
   {
      return gxTv_SdtPeriods_Curdescription_N ;
   }

   public void setCurdescription_N( byte value )
   {
      gxTv_SdtPeriods_Curdescription_N = value ;
      return  ;
   }

   public byte getPerdatdisini_N( )
   {
      return gxTv_SdtPeriods_Perdatdisini_N ;
   }

   public void setPerdatdisini_N( byte value )
   {
      gxTv_SdtPeriods_Perdatdisini_N = value ;
      return  ;
   }

   public byte getPerdatdisfin_N( )
   {
      return gxTv_SdtPeriods_Perdatdisfin_N ;
   }

   public void setPerdatdisfin_N( byte value )
   {
      gxTv_SdtPeriods_Perdatdisfin_N = value ;
      return  ;
   }

   protected byte gxTv_SdtPeriods_Isodes_N ;
   protected byte gxTv_SdtPeriods_Curdescription_N ;
   protected byte gxTv_SdtPeriods_Perdatdisini_N ;
   protected byte gxTv_SdtPeriods_Perdatdisfin_N ;
   protected String gxTv_SdtPeriods_Perclose ;
   protected String gxTv_SdtPeriods_Mode ;
   protected String gxTv_SdtPeriods_Perclose_Z ;
   protected String gxTv_SdtPeriods_Isocod ;
   protected String gxTv_SdtPeriods_Curcode ;
   protected String gxTv_SdtPeriods_Periodtypescode ;
   protected String gxTv_SdtPeriods_Perid ;
   protected String gxTv_SdtPeriods_Isodes ;
   protected String gxTv_SdtPeriods_Curdescription ;
   protected String gxTv_SdtPeriods_Perdescr ;
   protected String gxTv_SdtPeriods_Peridadm ;
   protected String gxTv_SdtPeriods_Isocod_Z ;
   protected String gxTv_SdtPeriods_Curcode_Z ;
   protected String gxTv_SdtPeriods_Periodtypescode_Z ;
   protected String gxTv_SdtPeriods_Perid_Z ;
   protected String gxTv_SdtPeriods_Isodes_Z ;
   protected String gxTv_SdtPeriods_Curdescription_Z ;
   protected String gxTv_SdtPeriods_Perdescr_Z ;
   protected String gxTv_SdtPeriods_Peridadm_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatini ;
   protected java.util.Date gxTv_SdtPeriods_Perdatend ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisini ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisfin ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueini ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueend ;
   protected java.util.Date gxTv_SdtPeriods_Perdatins ;
   protected java.util.Date gxTv_SdtPeriods_Perdatupd ;
   protected java.util.Date gxTv_SdtPeriods_Perdatini_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatend_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisini_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisfin_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueini_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueend_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatins_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatupd_Z ;
}


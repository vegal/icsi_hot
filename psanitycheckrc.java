/*
               File: sanitycheckRC
        Description: sanitycheck RC
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:24.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class psanitycheckrc extends GXProcedure
{
   public psanitycheckrc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psanitycheckrc.class ), "" );
   }

   public psanitycheckrc( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String[] aP1 )
   {
      psanitycheckrc.this.AV24FileNa = aP0;
      psanitycheckrc.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV26Caminh = AV24FileNa ;
      AV18File.setSource( AV26Caminh );
      if ( AV18File.exists() )
      {
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfropen( AV26Caminh, 255, "%", "\"", "") ;
         AV28HotCou = 0 ;
         AV10ErroHe = "Header Not Found." ;
         AV11ErroTr = "Trailer Not Found." ;
         while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
         {
            GXv_char1[0] = AV9linha ;
            GXt_int2 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char1, (short)(255)) ;
            AV9linha = GXv_char1[0] ;
            AV27RetVal = GXt_int2 ;
            AV28HotCou = (long)(AV28HotCou+1) ;
            if ( ( AV28HotCou == 1 ) )
            {
               /* Execute user subroutine: S1177 */
               S1177 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               AV10ErroHe = AV15Erro ;
            }
         }
         if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV9linha))==0)) )
         {
            /* Execute user subroutine: S12133 */
            S12133 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            AV11ErroTr = AV15Erro ;
         }
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         AV25msgErr = "" ;
         if ( ! ( ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV11ErroTr))==0)) ) )
         {
            AV25msgErr = AV10ErroHe + "=ErrHeader / ErrTrailer=" + AV11ErroTr ;
            GXt_svchar3 = AV29ShortN ;
            GXv_char1[0] = AV26Caminh ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2shortname(remoteHandle, context).execute( GXv_char1, GXv_char4) ;
            psanitycheckrc.this.AV26Caminh = GXv_char1[0] ;
            psanitycheckrc.this.GXt_svchar3 = GXv_char4[0] ;
            AV29ShortN = GXt_svchar3 ;
            GXt_svchar3 = AV12BasePa ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getpath(remoteHandle, context).execute( AV26Caminh, GXv_char4) ;
            psanitycheckrc.this.GXt_svchar3 = GXv_char4[0] ;
            AV12BasePa = GXt_svchar3 ;
            AV30SubFol = "Error\\" ;
            AV12BasePa = AV12BasePa + AV30SubFol ;
            AV19NewFil = GXutil.trim( AV12BasePa) + GXutil.trim( AV29ShortN) ;
            AV18File.setSource( AV26Caminh );
            AV18File.rename(GXutil.trim( AV19NewFil));
            AV14ShortN = GXutil.trim( AV29ShortN) + ".err" ;
            AV31Arquiv = GXutil.trim( AV12BasePa) + GXutil.trim( AV14ShortN) ;
            AV13LOGFil.openURL(AV31Arquiv);
            AV10ErroHe = AV10ErroHe + GXutil.newLine( ) ;
            AV11ErroTr = AV11ErroTr + GXutil.newLine( ) ;
            AV13LOGFil.writeRawText(AV10ErroHe);
            AV13LOGFil.writeRawText(AV11ErroTr);
            AV13LOGFil.close();
            GXt_svchar3 = AV23Subjec ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_SUBJ_RC", "Mensagem no subject do e-mail do tipo Sanity RC", "S", "Problem submission file RC", GXv_char4) ;
            psanitycheckrc.this.GXt_svchar3 = GXv_char4[0] ;
            AV23Subjec = GXt_svchar3 ;
            GXt_svchar3 = AV21Body ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_BODY_RC", "Mensagem no corpo do e-mail do tipo Sanity RC", "S", "<br>The submission file [FILE] related to RC was rejected in the Sanity Check.<br><br>Please check the file.<br><br>Thanks", GXv_char4) ;
            psanitycheckrc.this.GXt_svchar3 = GXv_char4[0] ;
            AV21Body = GXt_svchar3 ;
            AV21Body = GXutil.strReplace( AV21Body, "[FILE]", AV29ShortN) ;
            GXt_svchar3 = AV40To ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "", "S", "", GXv_char4) ;
            psanitycheckrc.this.GXt_svchar3 = GXv_char4[0] ;
            AV40To = GXutil.trim( GXt_svchar3) ;
            GXt_svchar3 = AV22CC ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "", "S", "", GXv_char4) ;
            psanitycheckrc.this.GXt_svchar3 = GXv_char4[0] ;
            AV22CC = GXutil.trim( GXt_svchar3) ;
            AV20BCC = "" ;
            GX_I = 1 ;
            while ( ( GX_I <= 5 ) )
            {
               AV8Anexos[GX_I-1] = "" ;
               GX_I = (int)(GX_I+1) ;
            }
            new penviaemail(remoteHandle, context).execute( AV23Subjec, AV21Body, AV40To, AV22CC, AV20BCC, AV8Anexos) ;
         }
      }
      cleanup();
   }

   public void S1177( )
   {
      /* 'CHECKHEADER' Routine */
      AV15Erro = "" ;
      AV17LinhaA = GXutil.strReplace( AV9linha, " ", "x") ;
      if ( ( GXutil.len( GXutil.trim( AV17LinhaA)) != 250 ) )
      {
         AV15Erro = "Header size record Not equal 250 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "00") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Identifier Not equal 00 /" ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 3, 13), "0000000000000") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Reference number Not equal 0000000000000 / " ;
      }
      AV38Sequen = GXutil.substring( AV9linha, 16, 10) ;
      if ( ( GXutil.val( AV38Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header POS Not Valid / " ;
      }
      AV32yy = GXutil.substring( AV9linha, 65, 2) ;
      AV33mm = GXutil.substring( AV9linha, 67, 2) ;
      AV34dd = GXutil.substring( AV9linha, 69, 2) ;
      AV42hh = GXutil.substring( AV9linha, 71, 2) ;
      AV43mi = GXutil.substring( AV9linha, 73, 2) ;
      AV44ss = GXutil.substring( AV9linha, 75, 2) ;
      AV41val = (int)(GXutil.val( AV32yy, ".")) ;
      if ( ( AV41val > 40 ) )
      {
         AV35Nyy = (int)(GXutil.val( "19"+GXutil.trim( AV32yy), ".")) ;
      }
      else
      {
         AV35Nyy = (int)(GXutil.val( "20"+GXutil.trim( AV32yy), ".")) ;
      }
      AV36Nmm = (int)(GXutil.val( AV33mm, ".")) ;
      AV37Ndd = (int)(GXutil.val( AV34dd, ".")) ;
      AV45Nhh = (int)(GXutil.val( AV42hh, ".")) ;
      AV46Nmi = (int)(GXutil.val( AV43mi, ".")) ;
      AV47Nss = (int)(GXutil.val( AV44ss, ".")) ;
      AV16DataGe = localUtil.ymdhmsToT( (short)(AV35Nyy), (byte)(AV36Nmm), (byte)(AV37Ndd), (byte)(AV45Nhh), (byte)(AV46Nmi), (byte)(AV47Nss)) ;
      if ( (GXutil.nullDate().equals(AV16DataGe)) )
      {
         AV15Erro = AV15Erro + "Header Date Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 25, 10), "          ") == 0 ) )
      {
         AV15Erro = AV15Erro + "Header Name Company Not Valid / " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 61, 4) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Sequencial Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 77, 2), " 3") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Information 77 Not Valid / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 246, 5), "00001") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Information 246 Not Valid / " ;
      }
   }

   public void S12133( )
   {
      /* 'CHECKTRAILER' Routine */
      AV15Erro = "" ;
      AV17LinhaA = GXutil.strReplace( AV9linha, " ", "x") ;
      if ( ( GXutil.len( GXutil.trim( AV17LinhaA)) != 250 ) )
      {
         AV15Erro = "Trailer size record Not equal 250 / " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 2), "99") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Identifier Not equal 99 " ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 3, 13), "9999999999999") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Reference number Not equal 9999999999999 / " ;
      }
   }

   protected void cleanup( )
   {
      this.aP1[0] = psanitycheckrc.this.AV25msgErr;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25msgErr = "" ;
      AV26Caminh = "" ;
      AV18File = new com.genexus.util.GXFile();
      AV27RetVal = 0 ;
      AV28HotCou = 0 ;
      AV10ErroHe = "" ;
      AV11ErroTr = "" ;
      AV9linha = "" ;
      GXt_int2 = (short)(0) ;
      returnInSub = false ;
      AV15Erro = "" ;
      AV29ShortN = "" ;
      GXv_char1 = new String [1] ;
      AV12BasePa = "" ;
      AV30SubFol = "" ;
      AV19NewFil = "" ;
      AV14ShortN = "" ;
      AV31Arquiv = "" ;
      AV13LOGFil = new com.genexus.xml.XMLWriter();
      AV23Subjec = "" ;
      AV21Body = "" ;
      AV40To = "" ;
      AV22CC = "" ;
      GXv_char4 = new String [1] ;
      AV20BCC = "" ;
      GX_I = 0 ;
      AV8Anexos = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV8Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV17LinhaA = "" ;
      AV38Sequen = "" ;
      AV32yy = "" ;
      AV33mm = "" ;
      AV34dd = "" ;
      AV42hh = "" ;
      AV43mi = "" ;
      AV44ss = "" ;
      AV41val = 0 ;
      AV35Nyy = 0 ;
      GXt_svchar3 = "" ;
      AV36Nmm = 0 ;
      AV37Ndd = 0 ;
      AV45Nhh = 0 ;
      AV46Nmi = 0 ;
      AV47Nss = 0 ;
      AV16DataGe = GXutil.resetTime( GXutil.nullDate() );
      AV39Sequen = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short GXt_int2 ;
   private short Gx_err ;
   private int GX_I ;
   private int AV41val ;
   private int AV35Nyy ;
   private int AV36Nmm ;
   private int AV37Ndd ;
   private int AV45Nhh ;
   private int AV46Nmi ;
   private int AV47Nss ;
   private long AV27RetVal ;
   private long AV28HotCou ;
   private String GXv_char1[] ;
   private String AV12BasePa ;
   private String GXv_char4[] ;
   private String AV32yy ;
   private String AV33mm ;
   private String AV34dd ;
   private String AV42hh ;
   private String AV43mi ;
   private String AV44ss ;
   private java.util.Date AV16DataGe ;
   private boolean returnInSub ;
   private String AV24FileNa ;
   private String AV25msgErr ;
   private String AV26Caminh ;
   private String AV10ErroHe ;
   private String AV11ErroTr ;
   private String AV9linha ;
   private String AV15Erro ;
   private String AV29ShortN ;
   private String AV30SubFol ;
   private String AV19NewFil ;
   private String AV14ShortN ;
   private String AV31Arquiv ;
   private String AV23Subjec ;
   private String AV21Body ;
   private String AV40To ;
   private String AV22CC ;
   private String AV20BCC ;
   private String AV8Anexos[] ;
   private String AV17LinhaA ;
   private String AV38Sequen ;
   private String GXt_svchar3 ;
   private String AV39Sequen ;
   private com.genexus.xml.XMLWriter AV13LOGFil ;
   private com.genexus.util.GXFile AV18File ;
   private String[] aP1 ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtUserFunctions extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtUserFunctions( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtUserFunctions.class));
   }

   public SdtUserFunctions( int remoteHandle ,
                            ModelContext context )
   {
      super( context, "SdtUserFunctions");
      initialize( remoteHandle) ;
   }

   public SdtUserFunctions( int remoteHandle ,
                            StructSdtUserFunctions struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV47uspCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV47uspCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserFunctions_Uspcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserFunctions_Uspdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserFunctions_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserFunctions_Uspcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "uspDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserFunctions_Uspdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "UserFunctions" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("uspCode", GXutil.rtrim( gxTv_SdtUserFunctions_Uspcode));
      oWriter.writeElement("uspDescription", GXutil.rtrim( gxTv_SdtUserFunctions_Uspdescription));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtUserFunctions_Mode));
      oWriter.writeElement("uspCode_Z", GXutil.rtrim( gxTv_SdtUserFunctions_Uspcode_Z));
      oWriter.writeElement("uspDescription_Z", GXutil.rtrim( gxTv_SdtUserFunctions_Uspdescription_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtUserFunctions_Uspcode( )
   {
      return gxTv_SdtUserFunctions_Uspcode ;
   }

   public void setgxTv_SdtUserFunctions_Uspcode( String value )
   {
      gxTv_SdtUserFunctions_Uspcode = value ;
      return  ;
   }

   public void setgxTv_SdtUserFunctions_Uspcode_SetNull( )
   {
      gxTv_SdtUserFunctions_Uspcode = "" ;
      return  ;
   }

   public String getgxTv_SdtUserFunctions_Uspdescription( )
   {
      return gxTv_SdtUserFunctions_Uspdescription ;
   }

   public void setgxTv_SdtUserFunctions_Uspdescription( String value )
   {
      gxTv_SdtUserFunctions_Uspdescription = value ;
      return  ;
   }

   public void setgxTv_SdtUserFunctions_Uspdescription_SetNull( )
   {
      gxTv_SdtUserFunctions_Uspdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtUserFunctions_Mode( )
   {
      return gxTv_SdtUserFunctions_Mode ;
   }

   public void setgxTv_SdtUserFunctions_Mode( String value )
   {
      gxTv_SdtUserFunctions_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtUserFunctions_Mode_SetNull( )
   {
      gxTv_SdtUserFunctions_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtUserFunctions_Uspcode_Z( )
   {
      return gxTv_SdtUserFunctions_Uspcode_Z ;
   }

   public void setgxTv_SdtUserFunctions_Uspcode_Z( String value )
   {
      gxTv_SdtUserFunctions_Uspcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtUserFunctions_Uspcode_Z_SetNull( )
   {
      gxTv_SdtUserFunctions_Uspcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtUserFunctions_Uspdescription_Z( )
   {
      return gxTv_SdtUserFunctions_Uspdescription_Z ;
   }

   public void setgxTv_SdtUserFunctions_Uspdescription_Z( String value )
   {
      gxTv_SdtUserFunctions_Uspdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtUserFunctions_Uspdescription_Z_SetNull( )
   {
      gxTv_SdtUserFunctions_Uspdescription_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tuserfunctions_bc obj ;
      obj = new tuserfunctions_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtUserFunctions_Uspcode = "" ;
      gxTv_SdtUserFunctions_Uspdescription = "" ;
      gxTv_SdtUserFunctions_Mode = "" ;
      gxTv_SdtUserFunctions_Uspcode_Z = "" ;
      gxTv_SdtUserFunctions_Uspdescription_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtUserFunctions Clone( )
   {
      SdtUserFunctions sdt ;
      tuserfunctions_bc obj ;
      sdt = (SdtUserFunctions)(clone()) ;
      obj = (tuserfunctions_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtUserFunctions struct )
   {
      setgxTv_SdtUserFunctions_Uspcode(struct.getUspcode());
      setgxTv_SdtUserFunctions_Uspdescription(struct.getUspdescription());
      setgxTv_SdtUserFunctions_Mode(struct.getMode());
      setgxTv_SdtUserFunctions_Uspcode_Z(struct.getUspcode_Z());
      setgxTv_SdtUserFunctions_Uspdescription_Z(struct.getUspdescription_Z());
   }

   public StructSdtUserFunctions getStruct( )
   {
      StructSdtUserFunctions struct = new StructSdtUserFunctions ();
      struct.setUspcode(getgxTv_SdtUserFunctions_Uspcode());
      struct.setUspdescription(getgxTv_SdtUserFunctions_Uspdescription());
      struct.setMode(getgxTv_SdtUserFunctions_Mode());
      struct.setUspcode_Z(getgxTv_SdtUserFunctions_Uspcode_Z());
      struct.setUspdescription_Z(getgxTv_SdtUserFunctions_Uspdescription_Z());
      return struct ;
   }

   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtUserFunctions_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtUserFunctions_Uspcode ;
   protected String gxTv_SdtUserFunctions_Uspdescription ;
   protected String gxTv_SdtUserFunctions_Uspcode_Z ;
   protected String gxTv_SdtUserFunctions_Uspdescription_Z ;
}


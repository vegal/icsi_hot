/*
               File: tmessagetypes_bc
        Description: Message Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:10.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tmessagetypes_bc extends GXWebPanel implements IGxSilentTrn
{
   public tmessagetypes_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tmessagetypes_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tmessagetypes_bc.class ));
   }

   public tmessagetypes_bc( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z100MsgCod = A100MsgCod ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_0C0( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls0C19( ) ;
         }
         else
         {
            checkExtendedTable0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors0C19( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues0C0( ) ;
      }
   }

   public void e110C2( )
   {
      /* 'Back' Routine */
   }

   public void zm0C19( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         Z96MsgDesc = A96MsgDesc ;
         Z868MSGSub = A868MSGSub ;
         Z98MsgStat = A98MsgStat ;
      }
      if ( ( GX_JID == -1 ) )
      {
         Z100MsgCod = A100MsgCod ;
         Z96MsgDesc = A96MsgDesc ;
         Z868MSGSub = A868MSGSub ;
         Z97MsgBody = A97MsgBody ;
         Z98MsgStat = A98MsgStat ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load0C19( )
   {
      /* Using cursor BC000C4 */
      pr_default.execute(2, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound19 = (short)(1) ;
         A96MsgDesc = BC000C4_A96MsgDesc[0] ;
         n96MsgDesc = BC000C4_n96MsgDesc[0] ;
         A868MSGSub = BC000C4_A868MSGSub[0] ;
         n868MSGSub = BC000C4_n868MSGSub[0] ;
         A97MsgBody = BC000C4_A97MsgBody[0] ;
         n97MsgBody = BC000C4_n97MsgBody[0] ;
         A98MsgStat = BC000C4_A98MsgStat[0] ;
         n98MsgStat = BC000C4_n98MsgStat[0] ;
         zm0C19( -1) ;
      }
      pr_default.close(2);
      onLoadActions0C19( ) ;
   }

   public void onLoadActions0C19( )
   {
   }

   public void checkExtendedTable0C19( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors0C19( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey0C19( )
   {
      /* Using cursor BC000C5 */
      pr_default.execute(3, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound19 = (short)(1) ;
      }
      else
      {
         RcdFound19 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC000C3 */
      pr_default.execute(1, new Object[] {A100MsgCod});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0C19( 1) ;
         RcdFound19 = (short)(1) ;
         A100MsgCod = BC000C3_A100MsgCod[0] ;
         A96MsgDesc = BC000C3_A96MsgDesc[0] ;
         n96MsgDesc = BC000C3_n96MsgDesc[0] ;
         A868MSGSub = BC000C3_A868MSGSub[0] ;
         n868MSGSub = BC000C3_n868MSGSub[0] ;
         A97MsgBody = BC000C3_A97MsgBody[0] ;
         n97MsgBody = BC000C3_n97MsgBody[0] ;
         A98MsgStat = BC000C3_A98MsgStat[0] ;
         n98MsgStat = BC000C3_n98MsgStat[0] ;
         Z100MsgCod = A100MsgCod ;
         sMode19 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load0C19( ) ;
         Gx_mode = sMode19 ;
      }
      else
      {
         RcdFound19 = (short)(0) ;
         initializeNonKey0C19( ) ;
         sMode19 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode19 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey0C19( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_0C0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency0C19( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC000C2 */
         pr_default.execute(0, new Object[] {A100MsgCod});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"MESSAGETYPES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z96MsgDesc, BC000C2_A96MsgDesc[0]) != 0 ) || ( GXutil.strcmp(Z868MSGSub, BC000C2_A868MSGSub[0]) != 0 ) || ( Z98MsgStat != BC000C2_A98MsgStat[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"MESSAGETYPES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0C19( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm0C19( 0) ;
         checkOptimisticConcurrency0C19( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert0C19( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000C6 */
                  pr_default.execute(4, new Object[] {A100MsgCod, new Boolean(n96MsgDesc), A96MsgDesc, new Boolean(n868MSGSub), A868MSGSub, new Boolean(n97MsgBody), A97MsgBody, new Boolean(n98MsgStat), new Byte(A98MsgStat)});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0C19( ) ;
         }
         endLevel0C19( ) ;
      }
      closeExtendedTableCursors0C19( ) ;
   }

   public void update0C19( )
   {
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0C19( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate0C19( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000C7 */
                  pr_default.execute(5, new Object[] {new Boolean(n96MsgDesc), A96MsgDesc, new Boolean(n868MSGSub), A868MSGSub, new Boolean(n97MsgBody), A97MsgBody, new Boolean(n98MsgStat), new Byte(A98MsgStat), A100MsgCod});
                  deferredUpdate0C19( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0C19( ) ;
      }
      closeExtendedTableCursors0C19( ) ;
   }

   public void deferredUpdate0C19( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate0C19( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls0C19( ) ;
         /* No cascading delete specified. */
         afterConfirm0C19( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete0C19( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000C8 */
               pr_default.execute(6, new Object[] {A100MsgCod});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode19 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel0C19( ) ;
      Gx_mode = sMode19 ;
   }

   public void onDeleteControls0C19( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel0C19( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete0C19( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues0C0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart0C19( )
   {
      /* Using cursor BC000C9 */
      pr_default.execute(7, new Object[] {A100MsgCod});
      RcdFound19 = (short)(0) ;
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound19 = (short)(1) ;
         A100MsgCod = BC000C9_A100MsgCod[0] ;
         A96MsgDesc = BC000C9_A96MsgDesc[0] ;
         n96MsgDesc = BC000C9_n96MsgDesc[0] ;
         A868MSGSub = BC000C9_A868MSGSub[0] ;
         n868MSGSub = BC000C9_n868MSGSub[0] ;
         A97MsgBody = BC000C9_A97MsgBody[0] ;
         n97MsgBody = BC000C9_n97MsgBody[0] ;
         A98MsgStat = BC000C9_A98MsgStat[0] ;
         n98MsgStat = BC000C9_n98MsgStat[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0C19( )
   {
      pr_default.readNext(7);
      RcdFound19 = (short)(0) ;
      scanLoad0C19( ) ;
   }

   public void scanLoad0C19( )
   {
      sMode19 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound19 = (short)(1) ;
         A100MsgCod = BC000C9_A100MsgCod[0] ;
         A96MsgDesc = BC000C9_A96MsgDesc[0] ;
         n96MsgDesc = BC000C9_n96MsgDesc[0] ;
         A868MSGSub = BC000C9_A868MSGSub[0] ;
         n868MSGSub = BC000C9_n868MSGSub[0] ;
         A97MsgBody = BC000C9_A97MsgBody[0] ;
         n97MsgBody = BC000C9_n97MsgBody[0] ;
         A98MsgStat = BC000C9_A98MsgStat[0] ;
         n98MsgStat = BC000C9_n98MsgStat[0] ;
      }
      Gx_mode = sMode19 ;
   }

   public void scanEnd0C19( )
   {
      pr_default.close(7);
   }

   public void afterConfirm0C19( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0C19( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0C19( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0C19( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0C19( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0C19( )
   {
      /* Before Validate Rules */
   }

   public void addRow0C19( )
   {
      VarsToRow19( bcMessageTypes) ;
   }

   public void sendRow0C19( )
   {
   }

   public void readRow0C19( )
   {
      RowToVars19( bcMessageTypes, 0) ;
   }

   public void confirmValues0C0( )
   {
   }

   public void initializeNonKey0C19( )
   {
      A96MsgDesc = "" ;
      n96MsgDesc = false ;
      A868MSGSub = "" ;
      n868MSGSub = false ;
      A97MsgBody = "" ;
      n97MsgBody = false ;
      A98MsgStat = (byte)(0) ;
      n98MsgStat = false ;
   }

   public void initAll0C19( )
   {
      A100MsgCod = "" ;
      initializeNonKey0C19( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow19( SdtMessageTypes obj19 )
   {
      obj19.setgxTv_SdtMessageTypes_Mode( Gx_mode );
      obj19.setgxTv_SdtMessageTypes_Msgdescription( A96MsgDesc );
      obj19.setgxTv_SdtMessageTypes_Msgsubject( A868MSGSub );
      obj19.setgxTv_SdtMessageTypes_Msgbody( A97MsgBody );
      obj19.setgxTv_SdtMessageTypes_Msgstatus( A98MsgStat );
      obj19.setgxTv_SdtMessageTypes_Msgcode( A100MsgCod );
      obj19.setgxTv_SdtMessageTypes_Msgcode_Z( Z100MsgCod );
      obj19.setgxTv_SdtMessageTypes_Msgdescription_Z( Z96MsgDesc );
      obj19.setgxTv_SdtMessageTypes_Msgsubject_Z( Z868MSGSub );
      obj19.setgxTv_SdtMessageTypes_Msgbody_Z( Z97MsgBody );
      obj19.setgxTv_SdtMessageTypes_Msgstatus_Z( Z98MsgStat );
      obj19.setgxTv_SdtMessageTypes_Msgdescription_N( (byte)((byte)((n96MsgDesc)?1:0)) );
      obj19.setgxTv_SdtMessageTypes_Msgsubject_N( (byte)((byte)((n868MSGSub)?1:0)) );
      obj19.setgxTv_SdtMessageTypes_Msgbody_N( (byte)((byte)((n97MsgBody)?1:0)) );
      obj19.setgxTv_SdtMessageTypes_Msgstatus_N( (byte)((byte)((n98MsgStat)?1:0)) );
      obj19.setgxTv_SdtMessageTypes_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars19( SdtMessageTypes obj19 ,
                            int forceLoad )
   {
      Gx_mode = obj19.getgxTv_SdtMessageTypes_Mode() ;
      A96MsgDesc = obj19.getgxTv_SdtMessageTypes_Msgdescription() ;
      A868MSGSub = obj19.getgxTv_SdtMessageTypes_Msgsubject() ;
      A97MsgBody = obj19.getgxTv_SdtMessageTypes_Msgbody() ;
      A98MsgStat = obj19.getgxTv_SdtMessageTypes_Msgstatus() ;
      A100MsgCod = obj19.getgxTv_SdtMessageTypes_Msgcode() ;
      Z100MsgCod = obj19.getgxTv_SdtMessageTypes_Msgcode_Z() ;
      Z96MsgDesc = obj19.getgxTv_SdtMessageTypes_Msgdescription_Z() ;
      Z868MSGSub = obj19.getgxTv_SdtMessageTypes_Msgsubject_Z() ;
      Z97MsgBody = obj19.getgxTv_SdtMessageTypes_Msgbody_Z() ;
      Z98MsgStat = obj19.getgxTv_SdtMessageTypes_Msgstatus_Z() ;
      n96MsgDesc = (boolean)((obj19.getgxTv_SdtMessageTypes_Msgdescription_N()==0)?false:true) ;
      n868MSGSub = (boolean)((obj19.getgxTv_SdtMessageTypes_Msgsubject_N()==0)?false:true) ;
      n97MsgBody = (boolean)((obj19.getgxTv_SdtMessageTypes_Msgbody_N()==0)?false:true) ;
      n98MsgStat = (boolean)((obj19.getgxTv_SdtMessageTypes_Msgstatus_N()==0)?false:true) ;
      Gx_mode = obj19.getgxTv_SdtMessageTypes_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A100MsgCod = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey0C19( ) ;
      scanStart0C19( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z100MsgCod = A100MsgCod ;
      }
      onLoadActions0C19( ) ;
      zm0C19( 0) ;
      addRow0C19( ) ;
      scanEnd0C19( ) ;
      if ( ( RcdFound19 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars19( bcMessageTypes, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey0C19( ) ;
      if ( ( RcdFound19 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
         {
            A100MsgCod = Z100MsgCod ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update0C19( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert0C19( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert0C19( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow19( bcMessageTypes) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars19( bcMessageTypes, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey0C19( ) ;
      if ( ( RcdFound19 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
         {
            A100MsgCod = Z100MsgCod ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A100MsgCod, Z100MsgCod) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tmessagetypes_bc");
      VarsToRow19( bcMessageTypes) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcMessageTypes.getgxTv_SdtMessageTypes_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcMessageTypes.setgxTv_SdtMessageTypes_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtMessageTypes sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcMessageTypes ) )
      {
         bcMessageTypes = sdt ;
         if ( ( GXutil.strcmp(bcMessageTypes.getgxTv_SdtMessageTypes_Mode(), "") == 0 ) )
         {
            bcMessageTypes.setgxTv_SdtMessageTypes_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow19( bcMessageTypes) ;
         }
         else
         {
            RowToVars19( bcMessageTypes, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcMessageTypes.getgxTv_SdtMessageTypes_Mode(), "") == 0 ) )
         {
            bcMessageTypes.setgxTv_SdtMessageTypes_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars19( bcMessageTypes, 1) ;
      return  ;
   }

   public SdtMessageTypes getMessageTypes_BC( )
   {
      return bcMessageTypes ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z100MsgCod = "" ;
      A100MsgCod = "" ;
      gxTv_SdtMessageTypes_Msgcode_Z = "" ;
      gxTv_SdtMessageTypes_Msgdescription_Z = "" ;
      gxTv_SdtMessageTypes_Msgsubject_Z = "" ;
      gxTv_SdtMessageTypes_Msgbody_Z = "" ;
      gxTv_SdtMessageTypes_Msgstatus_Z = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgdescription_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgsubject_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgbody_N = (byte)(0) ;
      gxTv_SdtMessageTypes_Msgstatus_N = (byte)(0) ;
      GX_JID = 0 ;
      Z96MsgDesc = "" ;
      A96MsgDesc = "" ;
      Z868MSGSub = "" ;
      A868MSGSub = "" ;
      Z98MsgStat = (byte)(0) ;
      A98MsgStat = (byte)(0) ;
      Z97MsgBody = "" ;
      A97MsgBody = "" ;
      BC000C4_A100MsgCod = new String[] {""} ;
      BC000C4_A96MsgDesc = new String[] {""} ;
      BC000C4_n96MsgDesc = new boolean[] {false} ;
      BC000C4_A868MSGSub = new String[] {""} ;
      BC000C4_n868MSGSub = new boolean[] {false} ;
      BC000C4_A97MsgBody = new String[] {""} ;
      BC000C4_n97MsgBody = new boolean[] {false} ;
      BC000C4_A98MsgStat = new byte[1] ;
      BC000C4_n98MsgStat = new boolean[] {false} ;
      RcdFound19 = (short)(0) ;
      n96MsgDesc = false ;
      n868MSGSub = false ;
      n97MsgBody = false ;
      n98MsgStat = false ;
      BC000C5_A100MsgCod = new String[] {""} ;
      BC000C3_A100MsgCod = new String[] {""} ;
      BC000C3_A96MsgDesc = new String[] {""} ;
      BC000C3_n96MsgDesc = new boolean[] {false} ;
      BC000C3_A868MSGSub = new String[] {""} ;
      BC000C3_n868MSGSub = new boolean[] {false} ;
      BC000C3_A97MsgBody = new String[] {""} ;
      BC000C3_n97MsgBody = new boolean[] {false} ;
      BC000C3_A98MsgStat = new byte[1] ;
      BC000C3_n98MsgStat = new boolean[] {false} ;
      sMode19 = "" ;
      BC000C2_A100MsgCod = new String[] {""} ;
      BC000C2_A96MsgDesc = new String[] {""} ;
      BC000C2_n96MsgDesc = new boolean[] {false} ;
      BC000C2_A868MSGSub = new String[] {""} ;
      BC000C2_n868MSGSub = new boolean[] {false} ;
      BC000C2_A97MsgBody = new String[] {""} ;
      BC000C2_n97MsgBody = new boolean[] {false} ;
      BC000C2_A98MsgStat = new byte[1] ;
      BC000C2_n98MsgStat = new boolean[] {false} ;
      BC000C9_A100MsgCod = new String[] {""} ;
      BC000C9_A96MsgDesc = new String[] {""} ;
      BC000C9_n96MsgDesc = new boolean[] {false} ;
      BC000C9_A868MSGSub = new String[] {""} ;
      BC000C9_n868MSGSub = new boolean[] {false} ;
      BC000C9_A97MsgBody = new String[] {""} ;
      BC000C9_n97MsgBody = new boolean[] {false} ;
      BC000C9_A98MsgStat = new byte[1] ;
      BC000C9_n98MsgStat = new boolean[] {false} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tmessagetypes_bc__default(),
         new Object[] {
             new Object[] {
            BC000C2_A100MsgCod, BC000C2_A96MsgDesc, BC000C2_n96MsgDesc, BC000C2_A868MSGSub, BC000C2_n868MSGSub, BC000C2_A97MsgBody, BC000C2_n97MsgBody, BC000C2_A98MsgStat, BC000C2_n98MsgStat
            }
            , new Object[] {
            BC000C3_A100MsgCod, BC000C3_A96MsgDesc, BC000C3_n96MsgDesc, BC000C3_A868MSGSub, BC000C3_n868MSGSub, BC000C3_A97MsgBody, BC000C3_n97MsgBody, BC000C3_A98MsgStat, BC000C3_n98MsgStat
            }
            , new Object[] {
            BC000C4_A100MsgCod, BC000C4_A96MsgDesc, BC000C4_n96MsgDesc, BC000C4_A868MSGSub, BC000C4_n868MSGSub, BC000C4_A97MsgBody, BC000C4_n97MsgBody, BC000C4_A98MsgStat, BC000C4_n98MsgStat
            }
            , new Object[] {
            BC000C5_A100MsgCod
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000C9_A100MsgCod, BC000C9_A96MsgDesc, BC000C9_n96MsgDesc, BC000C9_A868MSGSub, BC000C9_n868MSGSub, BC000C9_A97MsgBody, BC000C9_n97MsgBody, BC000C9_A98MsgStat, BC000C9_n98MsgStat
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private byte gxTv_SdtMessageTypes_Msgstatus_Z ;
   private byte gxTv_SdtMessageTypes_Msgdescription_N ;
   private byte gxTv_SdtMessageTypes_Msgsubject_N ;
   private byte gxTv_SdtMessageTypes_Msgbody_N ;
   private byte gxTv_SdtMessageTypes_Msgstatus_N ;
   private byte Z98MsgStat ;
   private byte A98MsgStat ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound19 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode19 ;
   private boolean n96MsgDesc ;
   private boolean n868MSGSub ;
   private boolean n97MsgBody ;
   private boolean n98MsgStat ;
   private String gxTv_SdtMessageTypes_Msgbody_Z ;
   private String Z97MsgBody ;
   private String A97MsgBody ;
   private String Z100MsgCod ;
   private String A100MsgCod ;
   private String gxTv_SdtMessageTypes_Msgcode_Z ;
   private String gxTv_SdtMessageTypes_Msgdescription_Z ;
   private String gxTv_SdtMessageTypes_Msgsubject_Z ;
   private String Z96MsgDesc ;
   private String A96MsgDesc ;
   private String Z868MSGSub ;
   private String A868MSGSub ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtMessageTypes bcMessageTypes ;
   private IDataStoreProvider pr_default ;
   private String[] BC000C4_A100MsgCod ;
   private String[] BC000C4_A96MsgDesc ;
   private boolean[] BC000C4_n96MsgDesc ;
   private String[] BC000C4_A868MSGSub ;
   private boolean[] BC000C4_n868MSGSub ;
   private String[] BC000C4_A97MsgBody ;
   private boolean[] BC000C4_n97MsgBody ;
   private byte[] BC000C4_A98MsgStat ;
   private boolean[] BC000C4_n98MsgStat ;
   private String[] BC000C5_A100MsgCod ;
   private String[] BC000C3_A100MsgCod ;
   private String[] BC000C3_A96MsgDesc ;
   private boolean[] BC000C3_n96MsgDesc ;
   private String[] BC000C3_A868MSGSub ;
   private boolean[] BC000C3_n868MSGSub ;
   private String[] BC000C3_A97MsgBody ;
   private boolean[] BC000C3_n97MsgBody ;
   private byte[] BC000C3_A98MsgStat ;
   private boolean[] BC000C3_n98MsgStat ;
   private String[] BC000C2_A100MsgCod ;
   private String[] BC000C2_A96MsgDesc ;
   private boolean[] BC000C2_n96MsgDesc ;
   private String[] BC000C2_A868MSGSub ;
   private boolean[] BC000C2_n868MSGSub ;
   private String[] BC000C2_A97MsgBody ;
   private boolean[] BC000C2_n97MsgBody ;
   private byte[] BC000C2_A98MsgStat ;
   private boolean[] BC000C2_n98MsgStat ;
   private String[] BC000C9_A100MsgCod ;
   private String[] BC000C9_A96MsgDesc ;
   private boolean[] BC000C9_n96MsgDesc ;
   private String[] BC000C9_A868MSGSub ;
   private boolean[] BC000C9_n868MSGSub ;
   private String[] BC000C9_A97MsgBody ;
   private boolean[] BC000C9_n97MsgBody ;
   private byte[] BC000C9_A98MsgStat ;
   private boolean[] BC000C9_n98MsgStat ;
}

final  class tmessagetypes_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC000C2", "SELECT [MsgCode], [MsgDescription], [MSGSubject], [MsgBody], [MsgStatus] FROM [MESSAGETYPES] WITH (UPDLOCK) WHERE [MsgCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000C3", "SELECT [MsgCode], [MsgDescription], [MSGSubject], [MsgBody], [MsgStatus] FROM [MESSAGETYPES] WITH (NOLOCK) WHERE [MsgCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000C4", "SELECT TM1.[MsgCode], TM1.[MsgDescription], TM1.[MSGSubject], TM1.[MsgBody], TM1.[MsgStatus] FROM [MESSAGETYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[MsgCode] = ? ORDER BY TM1.[MsgCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000C5", "SELECT [MsgCode] FROM [MESSAGETYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [MsgCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000C6", "INSERT INTO [MESSAGETYPES] ([MsgCode], [MsgDescription], [MSGSubject], [MsgBody], [MsgStatus]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000C7", "UPDATE [MESSAGETYPES] SET [MsgDescription]=?, [MSGSubject]=?, [MsgBody]=?, [MsgStatus]=?  WHERE [MsgCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000C8", "DELETE FROM [MESSAGETYPES]  WHERE [MsgCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000C9", "SELECT TM1.[MsgCode], TM1.[MsgDescription], TM1.[MSGSubject], TM1.[MsgBody], TM1.[MsgStatus] FROM [MESSAGETYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[MsgCode] = ? ORDER BY TM1.[MsgCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((byte[]) buf[7])[0] = rslt.getByte(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 50);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[4], 50);
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(4, (String)parms[6]);
               }
               if ( ((Boolean) parms[7]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(5, ((Number) parms[8]).byteValue());
               }
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 50);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 50);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(3, (String)parms[5]);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(4, ((Number) parms[7]).byteValue());
               }
               stmt.setVarchar(5, (String)parms[8], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


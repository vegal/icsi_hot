import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtCountry_Currencies extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCountry_Currencies( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCountry_Currencies.class));
   }

   public SdtCountry_Currencies( int remoteHandle ,
                                 ModelContext context )
   {
      super( context, "SdtCountry_Currencies");
      initialize( remoteHandle) ;
   }

   public SdtCountry_Currencies( int remoteHandle ,
                                 StructSdtCountry_Currencies struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtCountry_Currencies( )
   {
      super( new ModelContext(SdtCountry_Currencies.class), "SdtCountry_Currencies");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurPlaces") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curplaces = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurPlaces_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curplaces_Z = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurPlaces_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurDescription_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Country.Currencies" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("CurCode", GXutil.rtrim( gxTv_SdtCountry_Currencies_Curcode));
      oWriter.writeElement("CurPlaces", GXutil.trim( GXutil.str( gxTv_SdtCountry_Currencies_Curplaces, 1, 0)));
      oWriter.writeElement("CurDescription", GXutil.rtrim( gxTv_SdtCountry_Currencies_Curdescription));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCountry_Currencies_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtCountry_Currencies_Modified, 4, 0)));
      oWriter.writeElement("CurCode_Z", GXutil.rtrim( gxTv_SdtCountry_Currencies_Curcode_Z));
      oWriter.writeElement("CurPlaces_Z", GXutil.trim( GXutil.str( gxTv_SdtCountry_Currencies_Curplaces_Z, 1, 0)));
      oWriter.writeElement("CurDescription_Z", GXutil.rtrim( gxTv_SdtCountry_Currencies_Curdescription_Z));
      oWriter.writeElement("CurPlaces_N", GXutil.trim( GXutil.str( gxTv_SdtCountry_Currencies_Curplaces_N, 1, 0)));
      oWriter.writeElement("CurDescription_N", GXutil.trim( GXutil.str( gxTv_SdtCountry_Currencies_Curdescription_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtCountry_Currencies_Curcode( )
   {
      return gxTv_SdtCountry_Currencies_Curcode ;
   }

   public void setgxTv_SdtCountry_Currencies_Curcode( String value )
   {
      gxTv_SdtCountry_Currencies_Curcode = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curcode_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curcode = "" ;
      return  ;
   }

   public byte getgxTv_SdtCountry_Currencies_Curplaces( )
   {
      return gxTv_SdtCountry_Currencies_Curplaces ;
   }

   public void setgxTv_SdtCountry_Currencies_Curplaces( byte value )
   {
      gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curplaces = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curplaces_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(1) ;
      gxTv_SdtCountry_Currencies_Curplaces = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_Currencies_Curdescription( )
   {
      return gxTv_SdtCountry_Currencies_Curdescription ;
   }

   public void setgxTv_SdtCountry_Currencies_Curdescription( String value )
   {
      gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curdescription_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(1) ;
      gxTv_SdtCountry_Currencies_Curdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_Currencies_Mode( )
   {
      return gxTv_SdtCountry_Currencies_Mode ;
   }

   public void setgxTv_SdtCountry_Currencies_Mode( String value )
   {
      gxTv_SdtCountry_Currencies_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Mode_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtCountry_Currencies_Modified( )
   {
      return gxTv_SdtCountry_Currencies_Modified ;
   }

   public void setgxTv_SdtCountry_Currencies_Modified( short value )
   {
      gxTv_SdtCountry_Currencies_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Modified_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_Currencies_Curcode_Z( )
   {
      return gxTv_SdtCountry_Currencies_Curcode_Z ;
   }

   public void setgxTv_SdtCountry_Currencies_Curcode_Z( String value )
   {
      gxTv_SdtCountry_Currencies_Curcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curcode_Z_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curcode_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtCountry_Currencies_Curplaces_Z( )
   {
      return gxTv_SdtCountry_Currencies_Curplaces_Z ;
   }

   public void setgxTv_SdtCountry_Currencies_Curplaces_Z( byte value )
   {
      gxTv_SdtCountry_Currencies_Curplaces_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curplaces_Z_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curplaces_Z = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_Currencies_Curdescription_Z( )
   {
      return gxTv_SdtCountry_Currencies_Curdescription_Z ;
   }

   public void setgxTv_SdtCountry_Currencies_Curdescription_Z( String value )
   {
      gxTv_SdtCountry_Currencies_Curdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curdescription_Z_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curdescription_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtCountry_Currencies_Curplaces_N( )
   {
      return gxTv_SdtCountry_Currencies_Curplaces_N ;
   }

   public void setgxTv_SdtCountry_Currencies_Curplaces_N( byte value )
   {
      gxTv_SdtCountry_Currencies_Curplaces_N = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curplaces_N_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtCountry_Currencies_Curdescription_N( )
   {
      return gxTv_SdtCountry_Currencies_Curdescription_N ;
   }

   public void setgxTv_SdtCountry_Currencies_Curdescription_N( byte value )
   {
      gxTv_SdtCountry_Currencies_Curdescription_N = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_Currencies_Curdescription_N_SetNull( )
   {
      gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtCountry_Currencies_Curcode = "" ;
      gxTv_SdtCountry_Currencies_Curplaces = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription = "" ;
      gxTv_SdtCountry_Currencies_Mode = "" ;
      gxTv_SdtCountry_Currencies_Modified = (short)(0) ;
      gxTv_SdtCountry_Currencies_Curcode_Z = "" ;
      gxTv_SdtCountry_Currencies_Curplaces_Z = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription_Z = "" ;
      gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char4 = "" ;
      return  ;
   }

   public SdtCountry_Currencies Clone( )
   {
      return (SdtCountry_Currencies)(clone()) ;
   }

   public void setStruct( StructSdtCountry_Currencies struct )
   {
      setgxTv_SdtCountry_Currencies_Curcode(struct.getCurcode());
      setgxTv_SdtCountry_Currencies_Curplaces(struct.getCurplaces());
      setgxTv_SdtCountry_Currencies_Curdescription(struct.getCurdescription());
      setgxTv_SdtCountry_Currencies_Mode(struct.getMode());
      setgxTv_SdtCountry_Currencies_Modified(struct.getModified());
      setgxTv_SdtCountry_Currencies_Curcode_Z(struct.getCurcode_Z());
      setgxTv_SdtCountry_Currencies_Curplaces_Z(struct.getCurplaces_Z());
      setgxTv_SdtCountry_Currencies_Curdescription_Z(struct.getCurdescription_Z());
      setgxTv_SdtCountry_Currencies_Curplaces_N(struct.getCurplaces_N());
      setgxTv_SdtCountry_Currencies_Curdescription_N(struct.getCurdescription_N());
   }

   public StructSdtCountry_Currencies getStruct( )
   {
      StructSdtCountry_Currencies struct = new StructSdtCountry_Currencies ();
      struct.setCurcode(getgxTv_SdtCountry_Currencies_Curcode());
      struct.setCurplaces(getgxTv_SdtCountry_Currencies_Curplaces());
      struct.setCurdescription(getgxTv_SdtCountry_Currencies_Curdescription());
      struct.setMode(getgxTv_SdtCountry_Currencies_Mode());
      struct.setModified(getgxTv_SdtCountry_Currencies_Modified());
      struct.setCurcode_Z(getgxTv_SdtCountry_Currencies_Curcode_Z());
      struct.setCurplaces_Z(getgxTv_SdtCountry_Currencies_Curplaces_Z());
      struct.setCurdescription_Z(getgxTv_SdtCountry_Currencies_Curdescription_Z());
      struct.setCurplaces_N(getgxTv_SdtCountry_Currencies_Curplaces_N());
      struct.setCurdescription_N(getgxTv_SdtCountry_Currencies_Curdescription_N());
      return struct ;
   }

   protected byte gxTv_SdtCountry_Currencies_Curplaces ;
   protected byte gxTv_SdtCountry_Currencies_Curplaces_Z ;
   protected byte gxTv_SdtCountry_Currencies_Curplaces_N ;
   protected byte gxTv_SdtCountry_Currencies_Curdescription_N ;
   protected short gxTv_SdtCountry_Currencies_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtCountry_Currencies_Mode ;
   protected String sTagName ;
   protected String GXt_char4 ;
   protected String gxTv_SdtCountry_Currencies_Curcode ;
   protected String gxTv_SdtCountry_Currencies_Curdescription ;
   protected String gxTv_SdtCountry_Currencies_Curcode_Z ;
   protected String gxTv_SdtCountry_Currencies_Curdescription_Z ;
}


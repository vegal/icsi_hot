import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx06a020 extends GXSubfileElement
{
   private String ISOCod ;
   private String ISOCodDescription ;
   private String AirLineCode ;
   private String AirLineDescription ;
   public String getISOCod( )
   {
      return ISOCod ;
   }

   public void setISOCod( String value )
   {
      ISOCod = value;
   }

   public String getISOCodDescription( )
   {
      return ISOCodDescription ;
   }

   public void setISOCodDescription( String value )
   {
      ISOCodDescription = value;
   }

   public String getAirLineCode( )
   {
      return AirLineCode ;
   }

   public void setAirLineCode( String value )
   {
      AirLineCode = value;
   }

   public String getAirLineDescription( )
   {
      return AirLineDescription ;
   }

   public void setAirLineDescription( String value )
   {
      AirLineDescription = value;
   }

   public void clear( )
   {
      ISOCod = "" ;
      AirLineCode = "" ;
      AirLineDescription = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getISOCod().compareTo(((subwgx06a020) element).getISOCod()) ;
            case 1 :
               return  getAirLineCode().compareTo(((subwgx06a020) element).getAirLineCode()) ;
            case 2 :
               return  getAirLineDescription().compareTo(((subwgx06a020) element).getAirLineDescription()) ;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getISOCod(), "") == 0 ) && ( GXutil.strcmp(getAirLineCode(), "") == 0 ) && ( GXutil.strcmp(getAirLineDescription(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getISOCod() );
            break;
         case 1 :
            cell.setValue( getAirLineCode() );
            break;
         case 2 :
            cell.setValue( getAirLineDescription() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
         case 0 :
            return getISOCodDescription() ;
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getISOCod()) == 0) );
         case 1 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCode()) == 0) );
         case 2 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineDescription()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setISOCod(value.getStringValue());
               setISOCodDescription( value.getText());
               break;
            case 1 :
               setAirLineCode(value.getStringValue());
               break;
            case 2 :
               setAirLineDescription(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setISOCod(((subwgx06a020) element).getISOCod());
               setISOCodDescription(((subwgx06a020) element).getISOCodDescription());
               return;
            case 1 :
               setAirLineCode(((subwgx06a020) element).getAirLineCode());
               return;
            case 2 :
               setAirLineDescription(((subwgx06a020) element).getAirLineDescription());
               return;
      }
   }

}


/*
               File: Utilitarios
        Description: Stub for Utilitarios
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:25.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class putilitarios extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      putilitarios pgm = new putilitarios (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};
      String[] aP1 = new String[] {""};
      String[] aP2 = new String[] {""};

      try
      {
         aP0[0] = (String) args[0];
         aP1[0] = (String) args[1];
         aP2[0] = (String) args[2];
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0, aP1, aP2);
   }

   public putilitarios( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( putilitarios.class ), "" );
   }

   public putilitarios( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      putilitarios.this.AV2Codigo = aP0[0];
      this.aP0 = aP0;
      putilitarios.this.AV3Parm1 = aP1[0];
      this.aP1 = aP1;
      putilitarios.this.AV4Parm2 = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new autilitarios(remoteHandle, context).execute( aP0, aP1, aP2 );
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = putilitarios.this.AV2Codigo;
      this.aP1[0] = putilitarios.this.AV3Parm1;
      this.aP2[0] = putilitarios.this.AV4Parm2;
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV2Codigo ;
   private String AV3Parm1 ;
   private String AV4Parm2 ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
}


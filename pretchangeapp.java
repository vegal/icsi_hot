/*
               File: RETChangeApp
        Description: RETChange App
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pretchangeapp extends GXProcedure
{
   public pretchangeapp( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretchangeapp.class ), "" );
   }

   public pretchangeapp( int remoteHandle ,
                         ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pretchangeapp.this.AV8HntSeq = aP0[0];
      this.aP0 = aP0;
      pretchangeapp.this.AV11AppCod = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV9HntSeq_ = AV8HntSeq ;
      AV10HntSeq = AV8HntSeq ;
      /* Execute user subroutine: S119 */
      S119 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S119( )
   {
      /* 'CHANGEAPPRANGE' Routine */
      /* Optimized UPDATE. */
      /* Using cursor P005R2 */
      pr_default.execute(0, new Object[] {new Boolean(n993HntUsu), AV11AppCod, AV9HntSeq_, AV10HntSeq});
      /* End optimized UPDATE. */
   }

   protected void cleanup( )
   {
      this.aP0[0] = pretchangeapp.this.AV8HntSeq;
      this.aP1[0] = pretchangeapp.this.AV11AppCod;
      Application.commit(context, remoteHandle, "DEFAULT", "pretchangeapp");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9HntSeq_ = "" ;
      AV10HntSeq = "" ;
      returnInSub = false ;
      n993HntUsu = false ;
      A993HntUsu = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretchangeapp__default(),
         new Object[] {
             new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV8HntSeq ;
   private String AV9HntSeq_ ;
   private String AV10HntSeq ;
   private boolean returnInSub ;
   private boolean n993HntUsu ;
   private String AV11AppCod ;
   private String A993HntUsu ;
   private String[] aP0 ;
   private String[] aP1 ;
   private IDataStoreProvider pr_default ;
}

final  class pretchangeapp__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P005R2", "UPDATE [RETSPECTEMP] SET [HntUsuCod]=?  WHERE ([HntSeq] >= ?) AND (SUBSTRING([HntUsuCod], 1, 8) = 'RETClone') AND ([HntSeq] <= ?)", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20);
               }
               stmt.setString(2, (String)parms[2], 8);
               stmt.setString(3, (String)parms[3], 8);
               break;
      }
   }

}


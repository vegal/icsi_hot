
public final  class StructSdtContactTypes implements Cloneable, java.io.Serializable
{
   public StructSdtContactTypes( )
   {
      gxTv_SdtContactTypes_Contacttypescode = "" ;
      gxTv_SdtContactTypes_Contacttypesdescription = "" ;
      gxTv_SdtContactTypes_Contacttypesstatus = "" ;
      gxTv_SdtContactTypes_Mode = "" ;
      gxTv_SdtContactTypes_Contacttypescode_Z = "" ;
      gxTv_SdtContactTypes_Contacttypesdescription_Z = "" ;
      gxTv_SdtContactTypes_Contacttypesstatus_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getContacttypescode( )
   {
      return gxTv_SdtContactTypes_Contacttypescode ;
   }

   public void setContacttypescode( String value )
   {
      gxTv_SdtContactTypes_Contacttypescode = value ;
      return  ;
   }

   public String getContacttypesdescription( )
   {
      return gxTv_SdtContactTypes_Contacttypesdescription ;
   }

   public void setContacttypesdescription( String value )
   {
      gxTv_SdtContactTypes_Contacttypesdescription = value ;
      return  ;
   }

   public String getContacttypesstatus( )
   {
      return gxTv_SdtContactTypes_Contacttypesstatus ;
   }

   public void setContacttypesstatus( String value )
   {
      gxTv_SdtContactTypes_Contacttypesstatus = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtContactTypes_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtContactTypes_Mode = value ;
      return  ;
   }

   public String getContacttypescode_Z( )
   {
      return gxTv_SdtContactTypes_Contacttypescode_Z ;
   }

   public void setContacttypescode_Z( String value )
   {
      gxTv_SdtContactTypes_Contacttypescode_Z = value ;
      return  ;
   }

   public String getContacttypesdescription_Z( )
   {
      return gxTv_SdtContactTypes_Contacttypesdescription_Z ;
   }

   public void setContacttypesdescription_Z( String value )
   {
      gxTv_SdtContactTypes_Contacttypesdescription_Z = value ;
      return  ;
   }

   public String getContacttypesstatus_Z( )
   {
      return gxTv_SdtContactTypes_Contacttypesstatus_Z ;
   }

   public void setContacttypesstatus_Z( String value )
   {
      gxTv_SdtContactTypes_Contacttypesstatus_Z = value ;
      return  ;
   }

   protected String gxTv_SdtContactTypes_Contacttypesstatus ;
   protected String gxTv_SdtContactTypes_Mode ;
   protected String gxTv_SdtContactTypes_Contacttypesstatus_Z ;
   protected String gxTv_SdtContactTypes_Contacttypescode ;
   protected String gxTv_SdtContactTypes_Contacttypesdescription ;
   protected String gxTv_SdtContactTypes_Contacttypescode_Z ;
   protected String gxTv_SdtContactTypes_Contacttypesdescription_Z ;
}


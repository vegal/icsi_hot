/*
               File: tlogins_bc
        Description: Login Control
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:7.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tlogins_bc extends GXWebPanel implements IGxSilentTrn
{
   public tlogins_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tlogins_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tlogins_bc.class ));
   }

   public tlogins_bc( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z30lgnLogi = A30lgnLogi ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_090( )
   {
      beforeValidate09187( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls09187( ) ;
         }
         else
         {
            checkExtendedTable09187( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors09187( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         /* Save parent mode. */
         sMode187 = Gx_mode ;
         confirm_09188( ) ;
         if ( ( AnyError == 0 ) )
         {
            confirm_09189( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Restore parent mode. */
               Gx_mode = sMode187 ;
               IsConfirmed = (short)(1) ;
            }
         }
         /* Restore parent mode. */
         Gx_mode = sMode187 ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues090( ) ;
      }
   }

   public void confirm_09189( )
   {
      nGXsfl_189_idx = (short)(0) ;
      while ( ( nGXsfl_189_idx < bcLogins.getgxTv_SdtLogins_Profiles().size() ) )
      {
         readRow09189( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound189 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_189 != 0 ) )
         {
            getKey09189( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound189 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate09189( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable09189( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        zm09189( 12) ;
                     }
                     closeExtendedTableCursors09189( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound189 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey09189( ) ;
                     load09189( ) ;
                     beforeValidate09189( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls09189( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_189 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate09189( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable09189( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              zm09189( 12) ;
                           }
                           closeExtendedTableCursors09189( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void confirm_09188( )
   {
      nGXsfl_188_idx = (short)(0) ;
      while ( ( nGXsfl_188_idx < bcLogins.getgxTv_SdtLogins_Lastlogininfo().size() ) )
      {
         readRow09188( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound188 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_188 != 0 ) )
         {
            getKey09188( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) && ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
            {
               if ( ( RcdFound188 == 0 ) )
               {
                  Gx_mode = "INS" ;
                  beforeValidate09188( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     checkExtendedTable09188( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                     }
                     closeExtendedTableCursors09188( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( ( RcdFound188 != 0 ) )
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     getByPrimaryKey09188( ) ;
                     load09188( ) ;
                     beforeValidate09188( ) ;
                     if ( ( AnyError == 0 ) )
                     {
                        onDeleteControls09188( ) ;
                     }
                  }
                  else
                  {
                     if ( ( nIsMod_188 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        beforeValidate09188( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           checkExtendedTable09188( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                           }
                           closeExtendedTableCursors09188( ) ;
                           if ( ( AnyError == 0 ) )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                        }
                     }
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(Gx_mode, "DLT") != 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void e11092( )
   {
      /* Start Routine */
      A57lgnPwd_Visible = 0 ;
      A59lgnPwdL_Enabled = 0 ;
      A58lgnLast_Enabled = 0 ;
   }

   public void e12092( )
   {
      /* 'Create Password' Routine */
      if ( ( GXutil.strcmp(AV9lgnLogi, A30lgnLogi) != 0 ) )
      {
         AV10Random = (int)(GXutil.Int( GXutil.random( )*10000000)) ;
         AV8Passwor = GXutil.trim( GXutil.str( AV10Random, 10, 0)) ;
         GXt_char1 = AV11Encryp ;
         GXv_svchar2[0] = A30lgnLogi ;
         GXv_char3[0] = AV8Passwor ;
         GXv_char4[0] = GXt_char1 ;
         new pencryptpassword(remoteHandle, context).execute( GXv_svchar2, GXv_char3, GXv_char4) ;
         tlogins_bc.this.A30lgnLogi = GXv_svchar2[0] ;
         tlogins_bc.this.AV8Passwor = GXv_char3[0] ;
         tlogins_bc.this.GXt_char1 = GXv_char4[0] ;
         AV11Encryp = GXt_char1 ;
         GXv_char4[0] = A30lgnLogi ;
         GXv_char3[0] = AV11Encryp ;
         GXv_svchar2[0] = "CHG" ;
         new psavelogininfo(remoteHandle, context).execute( GXv_char4, GXv_char3, GXv_svchar2) ;
         tlogins_bc.this.A30lgnLogi = GXv_char4[0] ;
         tlogins_bc.this.AV11Encryp = GXv_char3[0] ;
         httpContext.GX_msglist.addItem("Actual Password for  "+A30lgnLogi+": "+AV8Passwor);
      }
      else
      {
         httpContext.GX_msglist.addItem("You cannot change your own password from here.");
      }
   }

   public void e13092( )
   {
      /* 'voltar' Routine */
      httpContext.setLinkToRedirect(formatLink("hsoplogin") );
   }

   public void zm09187( int GX_JID )
   {
      if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
      {
         Z43lgnName = A43lgnName ;
         Z55lgnEnab = A55lgnEnab ;
         Z56lgnEmai = A56lgnEmai ;
         Z57lgnPwd = A57lgnPwd ;
         Z227lgnPwd = A227lgnPwd ;
         Z58lgnLast = A58lgnLast ;
         Z59lgnPwdL = A59lgnPwdL ;
         Z60lgnPWDF = A60lgnPWDF ;
         Z61lgnSupe = A61lgnSupe ;
         Z63lgnLast = A63lgnLast ;
         Z62lgnLast = A62lgnLast ;
         Z64ustCode = A64ustCode ;
         Z65ustDesc = A65ustDesc ;
      }
      if ( ( GX_JID == -9 ) )
      {
         Z30lgnLogi = A30lgnLogi ;
         Z43lgnName = A43lgnName ;
         Z55lgnEnab = A55lgnEnab ;
         Z56lgnEmai = A56lgnEmai ;
         Z57lgnPwd = A57lgnPwd ;
         Z227lgnPwd = A227lgnPwd ;
         Z58lgnLast = A58lgnLast ;
         Z59lgnPwdL = A59lgnPwdL ;
         Z60lgnPWDF = A60lgnPWDF ;
         Z61lgnSupe = A61lgnSupe ;
      }
   }

   public void standaloneNotModal( )
   {
      Gx_BScreen = (byte)(0) ;
   }

   public void standaloneModal( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  && ((0==A55lgnEnab)) && ( Gx_BScreen == 0 ) )
      {
         A55lgnEnab = (byte)(1) ;
         n55lgnEnab = false ;
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  && ((0==A60lgnPWDF)) && ( Gx_BScreen == 0 ) )
      {
         A60lgnPWDF = (byte)(1) ;
         n60lgnPWDF = false ;
      }
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  && ((0==A61lgnSupe)) && ( Gx_BScreen == 0 ) )
      {
         A61lgnSupe = (byte)(0) ;
         n61lgnSupe = false ;
      }
   }

   public void load09187( )
   {
      /* Using cursor BC00099 */
      pr_default.execute(7, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound187 = (short)(1) ;
         A43lgnName = BC00099_A43lgnName[0] ;
         n43lgnName = BC00099_n43lgnName[0] ;
         A55lgnEnab = BC00099_A55lgnEnab[0] ;
         n55lgnEnab = BC00099_n55lgnEnab[0] ;
         A56lgnEmai = BC00099_A56lgnEmai[0] ;
         n56lgnEmai = BC00099_n56lgnEmai[0] ;
         A57lgnPwd = BC00099_A57lgnPwd[0] ;
         n57lgnPwd = BC00099_n57lgnPwd[0] ;
         A227lgnPwd = BC00099_A227lgnPwd[0] ;
         n227lgnPwd = BC00099_n227lgnPwd[0] ;
         A58lgnLast = BC00099_A58lgnLast[0] ;
         n58lgnLast = BC00099_n58lgnLast[0] ;
         A59lgnPwdL = BC00099_A59lgnPwdL[0] ;
         n59lgnPwdL = BC00099_n59lgnPwdL[0] ;
         A60lgnPWDF = BC00099_A60lgnPWDF[0] ;
         n60lgnPWDF = BC00099_n60lgnPWDF[0] ;
         A61lgnSupe = BC00099_A61lgnSupe[0] ;
         n61lgnSupe = BC00099_n61lgnSupe[0] ;
         zm09187( -9) ;
      }
      pr_default.close(7);
      onLoadActions09187( ) ;
   }

   public void onLoadActions09187( )
   {
   }

   public void checkExtendedTable09187( )
   {
      standaloneModal( ) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A30lgnLogi), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Login cannot be null", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A58lgnLast)) || (( A58lgnLast.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A58lgnLast.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Last Login is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      if ( ! ( (GXutil.nullDate().equals(A59lgnPwdL)) || (( A59lgnPwdL.after( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) ) || ( A59lgnPwdL.equals( localUtil.ymdhmsToT( (short)(1753), (byte)(1), (byte)(1), (byte)(0), (byte)(0), (byte)(0)) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Date and Time Password was Last Changed is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
   }

   public void closeExtendedTableCursors09187( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey09187( )
   {
      /* Using cursor BC000910 */
      pr_default.execute(8, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound187 = (short)(1) ;
      }
      else
      {
         RcdFound187 = (short)(0) ;
      }
      pr_default.close(8);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC00098 */
      pr_default.execute(6, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      if ( (pr_default.getStatus(6) != 101) )
      {
         zm09187( 9) ;
         RcdFound187 = (short)(1) ;
         A30lgnLogi = BC00098_A30lgnLogi[0] ;
         n30lgnLogi = BC00098_n30lgnLogi[0] ;
         A43lgnName = BC00098_A43lgnName[0] ;
         n43lgnName = BC00098_n43lgnName[0] ;
         A55lgnEnab = BC00098_A55lgnEnab[0] ;
         n55lgnEnab = BC00098_n55lgnEnab[0] ;
         A56lgnEmai = BC00098_A56lgnEmai[0] ;
         n56lgnEmai = BC00098_n56lgnEmai[0] ;
         A57lgnPwd = BC00098_A57lgnPwd[0] ;
         n57lgnPwd = BC00098_n57lgnPwd[0] ;
         A227lgnPwd = BC00098_A227lgnPwd[0] ;
         n227lgnPwd = BC00098_n227lgnPwd[0] ;
         A58lgnLast = BC00098_A58lgnLast[0] ;
         n58lgnLast = BC00098_n58lgnLast[0] ;
         A59lgnPwdL = BC00098_A59lgnPwdL[0] ;
         n59lgnPwdL = BC00098_n59lgnPwdL[0] ;
         A60lgnPWDF = BC00098_A60lgnPWDF[0] ;
         n60lgnPWDF = BC00098_n60lgnPWDF[0] ;
         A61lgnSupe = BC00098_A61lgnSupe[0] ;
         n61lgnSupe = BC00098_n61lgnSupe[0] ;
         Z30lgnLogi = A30lgnLogi ;
         sMode187 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load09187( ) ;
         Gx_mode = sMode187 ;
      }
      else
      {
         RcdFound187 = (short)(0) ;
         initializeNonKey09187( ) ;
         sMode187 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode187 ;
      }
      pr_default.close(6);
   }

   public void getEqualNoModal( )
   {
      getKey09187( ) ;
      if ( ( RcdFound187 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_090( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency09187( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00097 */
         pr_default.execute(5, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
         if ( ! (pr_default.getStatus(5) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"LOGINS"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         Gx_longc = false ;
         if ( (pr_default.getStatus(5) == 101) || ( GXutil.strcmp(Z43lgnName, BC00097_A43lgnName[0]) != 0 ) || ( Z55lgnEnab != BC00097_A55lgnEnab[0] ) || ( GXutil.strcmp(Z56lgnEmai, BC00097_A56lgnEmai[0]) != 0 ) || ( GXutil.strcmp(Z57lgnPwd, BC00097_A57lgnPwd[0]) != 0 ) || ( GXutil.strcmp(Z227lgnPwd, BC00097_A227lgnPwd[0]) != 0 ) )
         {
            Gx_longc = true ;
         }
         if ( Gx_longc || !( Z58lgnLast.equals( BC00097_A58lgnLast[0] ) ) || !( Z59lgnPwdL.equals( BC00097_A59lgnPwdL[0] ) ) || ( Z60lgnPWDF != BC00097_A60lgnPWDF[0] ) || ( Z61lgnSupe != BC00097_A61lgnSupe[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"LOGINS"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert09187( )
   {
      beforeValidate09187( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable09187( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm09187( 0) ;
         checkOptimisticConcurrency09187( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09187( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert09187( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000911 */
                  pr_default.execute(9, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, new Boolean(n43lgnName), A43lgnName, new Boolean(n55lgnEnab), new Byte(A55lgnEnab), new Boolean(n56lgnEmai), A56lgnEmai, new Boolean(n57lgnPwd), A57lgnPwd, new Boolean(n227lgnPwd), A227lgnPwd, new Boolean(n58lgnLast), A58lgnLast, new Boolean(n59lgnPwdL), A59lgnPwdL, new Boolean(n60lgnPWDF), new Byte(A60lgnPWDF), new Boolean(n61lgnSupe), new Byte(A61lgnSupe)});
                  if ( (pr_default.getStatus(9) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel09187( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           /* Save values for previous() function. */
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load09187( ) ;
         }
         endLevel09187( ) ;
      }
      closeExtendedTableCursors09187( ) ;
   }

   public void update09187( )
   {
      beforeValidate09187( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable09187( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency09187( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09187( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate09187( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000912 */
                  pr_default.execute(10, new Object[] {new Boolean(n43lgnName), A43lgnName, new Boolean(n55lgnEnab), new Byte(A55lgnEnab), new Boolean(n56lgnEmai), A56lgnEmai, new Boolean(n57lgnPwd), A57lgnPwd, new Boolean(n227lgnPwd), A227lgnPwd, new Boolean(n58lgnLast), A58lgnLast, new Boolean(n59lgnPwdL), A59lgnPwdL, new Boolean(n60lgnPWDF), new Byte(A60lgnPWDF), new Boolean(n61lgnSupe), new Byte(A61lgnSupe), new Boolean(n30lgnLogi), A30lgnLogi});
                  deferredUpdate09187( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        processLevel09187( ) ;
                        if ( ( AnyError == 0 ) )
                        {
                           getByPrimaryKey( ) ;
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel09187( ) ;
      }
      closeExtendedTableCursors09187( ) ;
   }

   public void deferredUpdate09187( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate09187( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency09187( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls09187( ) ;
         scanStart09189( ) ;
         while ( ( RcdFound189 != 0 ) )
         {
            getByPrimaryKey09189( ) ;
            delete09189( ) ;
            scanNext09189( ) ;
         }
         scanEnd09189( ) ;
         scanStart09188( ) ;
         while ( ( RcdFound188 != 0 ) )
         {
            getByPrimaryKey09188( ) ;
            delete09188( ) ;
            scanNext09188( ) ;
         }
         scanEnd09188( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09187( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeDelete09187( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000913 */
                  pr_default.execute(11, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      sMode187 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel09187( ) ;
      Gx_mode = sMode187 ;
   }

   public void onDeleteControls09187( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC000914 */
         pr_default.execute(12, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
         if ( (pr_default.getStatus(12) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Country Parameter"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(12);
         /* Using cursor BC000915 */
         pr_default.execute(13, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
         if ( (pr_default.getStatus(13) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"File Downloads"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(13);
      }
   }

   public void processNestedLevel09188( )
   {
      nGXsfl_188_idx = (short)(0) ;
      while ( ( nGXsfl_188_idx < bcLogins.getgxTv_SdtLogins_Lastlogininfo().size() ) )
      {
         readRow09188( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound188 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_188 != 0 ) )
         {
            standaloneNotModal09188( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert09188( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete09188( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update09188( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_188 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_188 = (short)(1) ;
               Gxremove188 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_188 = (short)(0) ;
               Gxremove188 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcLogins.getgxTv_SdtLogins_Lastlogininfo().removeElement(nGXsfl_188_idx);
            nGXsfl_188_idx = (short)(nGXsfl_188_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow188( ((SdtLogins_LastLoginInfo)bcLogins.getgxTv_SdtLogins_Lastlogininfo().elementAt(-1+nGXsfl_188_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll09188( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processNestedLevel09189( )
   {
      nGXsfl_189_idx = (short)(0) ;
      while ( ( nGXsfl_189_idx < bcLogins.getgxTv_SdtLogins_Profiles().size() ) )
      {
         readRow09189( ) ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( Gx_mode))==0)) )
         {
            if ( ( RcdFound189 == 0 ) )
            {
               Gx_mode = "INS" ;
            }
            else
            {
               Gx_mode = "UPD" ;
            }
         }
         if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) || ( nIsMod_189 != 0 ) )
         {
            standaloneNotModal09189( ) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
            {
               Gx_mode = "INS" ;
               insert09189( ) ;
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
               {
                  Gx_mode = "DLT" ;
                  delete09189( ) ;
               }
               else
               {
                  Gx_mode = "UPD" ;
                  update09189( ) ;
               }
            }
         }
         if ( ( AnyError == 0 ) )
         {
            nIsMod_189 = (short)(0) ;
            if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) || ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               nRcdExists_189 = (short)(1) ;
               Gxremove189 = (byte)(0) ;
            }
            else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
            {
               nRcdExists_189 = (short)(0) ;
               Gxremove189 = (byte)(1) ;
            }
         }
         /* Update SDT rows */
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            bcLogins.getgxTv_SdtLogins_Profiles().removeElement(nGXsfl_189_idx);
            nGXsfl_189_idx = (short)(nGXsfl_189_idx-1) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            VarsToRow189( ((SdtLogins_Profiles)bcLogins.getgxTv_SdtLogins_Profiles().elementAt(-1+nGXsfl_189_idx))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll09189( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
   }

   public void processLevel09187( )
   {
      /* Save parent mode. */
      sMode187 = Gx_mode ;
      processNestedLevel09188( ) ;
      processNestedLevel09189( ) ;
      if ( ( AnyError != 0 ) )
      {
      }
      /* Restore parent mode. */
      Gx_mode = sMode187 ;
      /* ' Update level parameters */
   }

   public void endLevel09187( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(5);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete09187( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues090( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart09187( )
   {
      /* Using cursor BC000916 */
      pr_default.execute(14, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      RcdFound187 = (short)(0) ;
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound187 = (short)(1) ;
         A30lgnLogi = BC000916_A30lgnLogi[0] ;
         n30lgnLogi = BC000916_n30lgnLogi[0] ;
         A43lgnName = BC000916_A43lgnName[0] ;
         n43lgnName = BC000916_n43lgnName[0] ;
         A55lgnEnab = BC000916_A55lgnEnab[0] ;
         n55lgnEnab = BC000916_n55lgnEnab[0] ;
         A56lgnEmai = BC000916_A56lgnEmai[0] ;
         n56lgnEmai = BC000916_n56lgnEmai[0] ;
         A57lgnPwd = BC000916_A57lgnPwd[0] ;
         n57lgnPwd = BC000916_n57lgnPwd[0] ;
         A227lgnPwd = BC000916_A227lgnPwd[0] ;
         n227lgnPwd = BC000916_n227lgnPwd[0] ;
         A58lgnLast = BC000916_A58lgnLast[0] ;
         n58lgnLast = BC000916_n58lgnLast[0] ;
         A59lgnPwdL = BC000916_A59lgnPwdL[0] ;
         n59lgnPwdL = BC000916_n59lgnPwdL[0] ;
         A60lgnPWDF = BC000916_A60lgnPWDF[0] ;
         n60lgnPWDF = BC000916_n60lgnPWDF[0] ;
         A61lgnSupe = BC000916_A61lgnSupe[0] ;
         n61lgnSupe = BC000916_n61lgnSupe[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext09187( )
   {
      pr_default.readNext(14);
      RcdFound187 = (short)(0) ;
      scanLoad09187( ) ;
   }

   public void scanLoad09187( )
   {
      sMode187 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(14) != 101) )
      {
         RcdFound187 = (short)(1) ;
         A30lgnLogi = BC000916_A30lgnLogi[0] ;
         n30lgnLogi = BC000916_n30lgnLogi[0] ;
         A43lgnName = BC000916_A43lgnName[0] ;
         n43lgnName = BC000916_n43lgnName[0] ;
         A55lgnEnab = BC000916_A55lgnEnab[0] ;
         n55lgnEnab = BC000916_n55lgnEnab[0] ;
         A56lgnEmai = BC000916_A56lgnEmai[0] ;
         n56lgnEmai = BC000916_n56lgnEmai[0] ;
         A57lgnPwd = BC000916_A57lgnPwd[0] ;
         n57lgnPwd = BC000916_n57lgnPwd[0] ;
         A227lgnPwd = BC000916_A227lgnPwd[0] ;
         n227lgnPwd = BC000916_n227lgnPwd[0] ;
         A58lgnLast = BC000916_A58lgnLast[0] ;
         n58lgnLast = BC000916_n58lgnLast[0] ;
         A59lgnPwdL = BC000916_A59lgnPwdL[0] ;
         n59lgnPwdL = BC000916_n59lgnPwdL[0] ;
         A60lgnPWDF = BC000916_A60lgnPWDF[0] ;
         n60lgnPWDF = BC000916_n60lgnPWDF[0] ;
         A61lgnSupe = BC000916_A61lgnSupe[0] ;
         n61lgnSupe = BC000916_n61lgnSupe[0] ;
      }
      Gx_mode = sMode187 ;
   }

   public void scanEnd09187( )
   {
      pr_default.close(14);
   }

   public void afterConfirm09187( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert09187( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate09187( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete09187( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete09187( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate09187( )
   {
      /* Before Validate Rules */
   }

   public void addRow09187( )
   {
      VarsToRow187( bcLogins) ;
   }

   public void sendRow09187( )
   {
   }

   public void readRow09187( )
   {
      RowToVars187( bcLogins, 0) ;
   }

   public void zm09188( int GX_JID )
   {
      if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
      {
         Z62lgnLast = A62lgnLast ;
         Z43lgnName = A43lgnName ;
         Z55lgnEnab = A55lgnEnab ;
         Z56lgnEmai = A56lgnEmai ;
         Z57lgnPwd = A57lgnPwd ;
         Z227lgnPwd = A227lgnPwd ;
         Z58lgnLast = A58lgnLast ;
         Z59lgnPwdL = A59lgnPwdL ;
         Z60lgnPWDF = A60lgnPWDF ;
         Z61lgnSupe = A61lgnSupe ;
         Z64ustCode = A64ustCode ;
         Z65ustDesc = A65ustDesc ;
      }
      if ( ( GX_JID == -10 ) )
      {
         Z30lgnLogi = A30lgnLogi ;
         Z63lgnLast = A63lgnLast ;
         Z62lgnLast = A62lgnLast ;
      }
   }

   public void standaloneNotModal09188( )
   {
   }

   public void standaloneModal09188( )
   {
   }

   public void load09188( )
   {
      /* Using cursor BC000917 */
      pr_default.execute(15, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast});
      if ( (pr_default.getStatus(15) != 101) )
      {
         RcdFound188 = (short)(1) ;
         A62lgnLast = BC000917_A62lgnLast[0] ;
         n62lgnLast = BC000917_n62lgnLast[0] ;
         zm09188( -10) ;
      }
      pr_default.close(15);
      onLoadActions09188( ) ;
   }

   public void onLoadActions09188( )
   {
   }

   public void checkExtendedTable09188( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal09188( ) ;
      Gx_BScreen = (byte)(0) ;
      if ( ! ( (GXutil.nullDate().equals(A62lgnLast)) || (( A62lgnLast.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A62lgnLast.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Field Date when password was used is out of range", "OutOfRange", 1);
         AnyError = (short)(1) ;
      }
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors09188( )
   {
   }

   public void enableDisable09188( )
   {
   }

   public void getKey09188( )
   {
      /* Using cursor BC000918 */
      pr_default.execute(16, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast});
      if ( (pr_default.getStatus(16) != 101) )
      {
         RcdFound188 = (short)(1) ;
      }
      else
      {
         RcdFound188 = (short)(0) ;
      }
      pr_default.close(16);
   }

   public void getByPrimaryKey09188( )
   {
      /* Using cursor BC00096 */
      pr_default.execute(4, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast});
      if ( (pr_default.getStatus(4) != 101) )
      {
         zm09188( 10) ;
         RcdFound188 = (short)(1) ;
         initializeNonKey09188( ) ;
         A63lgnLast = BC00096_A63lgnLast[0] ;
         A62lgnLast = BC00096_A62lgnLast[0] ;
         n62lgnLast = BC00096_n62lgnLast[0] ;
         Z30lgnLogi = A30lgnLogi ;
         Z63lgnLast = A63lgnLast ;
         sMode188 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal09188( ) ;
         load09188( ) ;
         Gx_mode = sMode188 ;
      }
      else
      {
         RcdFound188 = (short)(0) ;
         initializeNonKey09188( ) ;
         sMode188 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal09188( ) ;
         Gx_mode = sMode188 ;
      }
      pr_default.close(4);
   }

   public void checkOptimisticConcurrency09188( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00095 */
         pr_default.execute(3, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast});
         if ( ! (pr_default.getStatus(3) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"LOGINSLASTLOGININFO"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(3) == 101) || !( Z62lgnLast.equals( BC00095_A62lgnLast[0] ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"LOGINSLASTLOGININFO"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert09188( )
   {
      beforeValidate09188( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable09188( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm09188( 0) ;
         checkOptimisticConcurrency09188( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09188( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert09188( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000919 */
                  pr_default.execute(17, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast, new Boolean(n62lgnLast), A62lgnLast});
                  if ( (pr_default.getStatus(17) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load09188( ) ;
         }
         endLevel09188( ) ;
      }
      closeExtendedTableCursors09188( ) ;
   }

   public void update09188( )
   {
      beforeValidate09188( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable09188( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency09188( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09188( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate09188( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000920 */
                  pr_default.execute(18, new Object[] {new Boolean(n62lgnLast), A62lgnLast, new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast});
                  deferredUpdate09188( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey09188( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel09188( ) ;
      }
      closeExtendedTableCursors09188( ) ;
   }

   public void deferredUpdate09188( )
   {
   }

   public void delete09188( )
   {
      Gx_mode = "DLT" ;
      beforeValidate09188( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency09188( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls09188( ) ;
         /* No cascading delete specified. */
         afterConfirm09188( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete09188( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000921 */
               pr_default.execute(19, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A63lgnLast});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode188 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel09188( ) ;
      Gx_mode = sMode188 ;
   }

   public void onDeleteControls09188( )
   {
      standaloneModal09188( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel09188( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(3);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart09188( )
   {
      /* Using cursor BC000922 */
      pr_default.execute(20, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      RcdFound188 = (short)(0) ;
      if ( (pr_default.getStatus(20) != 101) )
      {
         RcdFound188 = (short)(1) ;
         A63lgnLast = BC000922_A63lgnLast[0] ;
         A62lgnLast = BC000922_A62lgnLast[0] ;
         n62lgnLast = BC000922_n62lgnLast[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext09188( )
   {
      pr_default.readNext(20);
      RcdFound188 = (short)(0) ;
      scanLoad09188( ) ;
   }

   public void scanLoad09188( )
   {
      sMode188 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(20) != 101) )
      {
         RcdFound188 = (short)(1) ;
         A63lgnLast = BC000922_A63lgnLast[0] ;
         A62lgnLast = BC000922_A62lgnLast[0] ;
         n62lgnLast = BC000922_n62lgnLast[0] ;
      }
      Gx_mode = sMode188 ;
   }

   public void scanEnd09188( )
   {
      pr_default.close(20);
   }

   public void afterConfirm09188( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert09188( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate09188( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete09188( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete09188( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate09188( )
   {
      /* Before Validate Rules */
   }

   public void addRow09188( )
   {
      SdtLogins_LastLoginInfo obj188 ;
      obj188 = new SdtLogins_LastLoginInfo(remoteHandle);
      VarsToRow188( obj188) ;
      bcLogins.getgxTv_SdtLogins_Lastlogininfo().add(obj188, 0);
      obj188.setgxTv_SdtLogins_LastLoginInfo_Mode( "UPD" );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Modified( (short)(0) );
   }

   public void sendRow09188( )
   {
   }

   public void readRow09188( )
   {
      nGXsfl_188_idx = (short)(nGXsfl_188_idx+1) ;
      RowToVars188( ((SdtLogins_LastLoginInfo)bcLogins.getgxTv_SdtLogins_Lastlogininfo().elementAt(-1+nGXsfl_188_idx)), 0) ;
   }

   public void zm09189( int GX_JID )
   {
      if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
      {
         Z43lgnName = A43lgnName ;
         Z55lgnEnab = A55lgnEnab ;
         Z56lgnEmai = A56lgnEmai ;
         Z57lgnPwd = A57lgnPwd ;
         Z227lgnPwd = A227lgnPwd ;
         Z58lgnLast = A58lgnLast ;
         Z59lgnPwdL = A59lgnPwdL ;
         Z60lgnPWDF = A60lgnPWDF ;
         Z61lgnSupe = A61lgnSupe ;
         Z63lgnLast = A63lgnLast ;
         Z62lgnLast = A62lgnLast ;
      }
      if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
      {
         Z65ustDesc = A65ustDesc ;
         Z43lgnName = A43lgnName ;
         Z55lgnEnab = A55lgnEnab ;
         Z56lgnEmai = A56lgnEmai ;
         Z57lgnPwd = A57lgnPwd ;
         Z227lgnPwd = A227lgnPwd ;
         Z58lgnLast = A58lgnLast ;
         Z59lgnPwdL = A59lgnPwdL ;
         Z60lgnPWDF = A60lgnPWDF ;
         Z61lgnSupe = A61lgnSupe ;
         Z63lgnLast = A63lgnLast ;
         Z62lgnLast = A62lgnLast ;
      }
      if ( ( GX_JID == -11 ) )
      {
         Z30lgnLogi = A30lgnLogi ;
         Z64ustCode = A64ustCode ;
      }
   }

   public void standaloneNotModal09189( )
   {
   }

   public void standaloneModal09189( )
   {
   }

   public void load09189( )
   {
      /* Using cursor BC000923 */
      pr_default.execute(21, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A64ustCode});
      if ( (pr_default.getStatus(21) != 101) )
      {
         RcdFound189 = (short)(1) ;
         A65ustDesc = BC000923_A65ustDesc[0] ;
         n65ustDesc = BC000923_n65ustDesc[0] ;
         zm09189( -11) ;
      }
      pr_default.close(21);
      onLoadActions09189( ) ;
   }

   public void onLoadActions09189( )
   {
   }

   public void checkExtendedTable09189( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal09189( ) ;
      Gx_BScreen = (byte)(0) ;
      /* Using cursor BC00094 */
      pr_default.execute(2, new Object[] {A64ustCode});
      if ( (pr_default.getStatus(2) == 101) )
      {
         httpContext.GX_msglist.addItem("No matching 'User Profiles'.", "ForeignKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      A65ustDesc = BC00094_A65ustDesc[0] ;
      n65ustDesc = BC00094_n65ustDesc[0] ;
      pr_default.close(2);
      Gx_BScreen = (byte)(1) ;
   }

   public void closeExtendedTableCursors09189( )
   {
      pr_default.close(2);
   }

   public void enableDisable09189( )
   {
   }

   public void getKey09189( )
   {
      /* Using cursor BC000924 */
      pr_default.execute(22, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A64ustCode});
      if ( (pr_default.getStatus(22) != 101) )
      {
         RcdFound189 = (short)(1) ;
      }
      else
      {
         RcdFound189 = (short)(0) ;
      }
      pr_default.close(22);
   }

   public void getByPrimaryKey09189( )
   {
      /* Using cursor BC00093 */
      pr_default.execute(1, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A64ustCode});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm09189( 11) ;
         RcdFound189 = (short)(1) ;
         initializeNonKey09189( ) ;
         A64ustCode = BC00093_A64ustCode[0] ;
         Z30lgnLogi = A30lgnLogi ;
         Z64ustCode = A64ustCode ;
         sMode189 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal09189( ) ;
         load09189( ) ;
         Gx_mode = sMode189 ;
      }
      else
      {
         RcdFound189 = (short)(0) ;
         initializeNonKey09189( ) ;
         sMode189 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal09189( ) ;
         Gx_mode = sMode189 ;
      }
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency09189( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00092 */
         pr_default.execute(0, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A64ustCode});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"LOGINSPROFILES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"LOGINSPROFILES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert09189( )
   {
      beforeValidate09189( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable09189( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm09189( 0) ;
         checkOptimisticConcurrency09189( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09189( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert09189( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC000925 */
                  pr_default.execute(23, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A64ustCode});
                  if ( (pr_default.getStatus(23) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load09189( ) ;
         }
         endLevel09189( ) ;
      }
      closeExtendedTableCursors09189( ) ;
   }

   public void update09189( )
   {
      beforeValidate09189( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable09189( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency09189( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm09189( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate09189( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* No attributes to update on table [LOGINSPROFILES] */
                  deferredUpdate09189( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey09189( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel09189( ) ;
      }
      closeExtendedTableCursors09189( ) ;
   }

   public void deferredUpdate09189( )
   {
   }

   public void delete09189( )
   {
      Gx_mode = "DLT" ;
      beforeValidate09189( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency09189( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls09189( ) ;
         /* No cascading delete specified. */
         afterConfirm09189( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete09189( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC000926 */
               pr_default.execute(24, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi, A64ustCode});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode189 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel09189( ) ;
      Gx_mode = sMode189 ;
   }

   public void onDeleteControls09189( )
   {
      standaloneModal09189( ) ;
      if ( ( AnyError == 0 ) )
      {
         /* Delete mode formulas */
         /* Using cursor BC000927 */
         pr_default.execute(25, new Object[] {A64ustCode});
         A65ustDesc = BC000927_A65ustDesc[0] ;
         n65ustDesc = BC000927_n65ustDesc[0] ;
         pr_default.close(25);
      }
   }

   public void endLevel09189( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart09189( )
   {
      /* Using cursor BC000928 */
      pr_default.execute(26, new Object[] {new Boolean(n30lgnLogi), A30lgnLogi});
      RcdFound189 = (short)(0) ;
      if ( (pr_default.getStatus(26) != 101) )
      {
         RcdFound189 = (short)(1) ;
         A65ustDesc = BC000928_A65ustDesc[0] ;
         n65ustDesc = BC000928_n65ustDesc[0] ;
         A64ustCode = BC000928_A64ustCode[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext09189( )
   {
      pr_default.readNext(26);
      RcdFound189 = (short)(0) ;
      scanLoad09189( ) ;
   }

   public void scanLoad09189( )
   {
      sMode189 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(26) != 101) )
      {
         RcdFound189 = (short)(1) ;
         A65ustDesc = BC000928_A65ustDesc[0] ;
         n65ustDesc = BC000928_n65ustDesc[0] ;
         A64ustCode = BC000928_A64ustCode[0] ;
      }
      Gx_mode = sMode189 ;
   }

   public void scanEnd09189( )
   {
      pr_default.close(26);
   }

   public void afterConfirm09189( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert09189( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate09189( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete09189( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete09189( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate09189( )
   {
      /* Before Validate Rules */
   }

   public void addRow09189( )
   {
      SdtLogins_Profiles obj189 ;
      obj189 = new SdtLogins_Profiles(remoteHandle);
      VarsToRow189( obj189) ;
      bcLogins.getgxTv_SdtLogins_Profiles().add(obj189, 0);
      obj189.setgxTv_SdtLogins_Profiles_Mode( "UPD" );
      obj189.setgxTv_SdtLogins_Profiles_Modified( (short)(0) );
   }

   public void sendRow09189( )
   {
   }

   public void readRow09189( )
   {
      nGXsfl_189_idx = (short)(nGXsfl_189_idx+1) ;
      RowToVars189( ((SdtLogins_Profiles)bcLogins.getgxTv_SdtLogins_Profiles().elementAt(-1+nGXsfl_189_idx)), 0) ;
   }

   public void confirmValues090( )
   {
   }

   public void initializeNonKey09187( )
   {
      A43lgnName = "" ;
      n43lgnName = false ;
      A55lgnEnab = (byte)(1) ;
      n55lgnEnab = false ;
      A56lgnEmai = "" ;
      n56lgnEmai = false ;
      A57lgnPwd = "" ;
      n57lgnPwd = false ;
      A227lgnPwd = "" ;
      n227lgnPwd = false ;
      A58lgnLast = GXutil.resetTime( GXutil.nullDate() );
      n58lgnLast = false ;
      A59lgnPwdL = GXutil.resetTime( GXutil.nullDate() );
      n59lgnPwdL = false ;
      A60lgnPWDF = (byte)(1) ;
      n60lgnPWDF = false ;
      A61lgnSupe = (byte)(0) ;
      n61lgnSupe = false ;
   }

   public void initAll09187( )
   {
      A30lgnLogi = "" ;
      n30lgnLogi = false ;
      initializeNonKey09187( ) ;
   }

   public void standaloneModalInsert( )
   {
      A55lgnEnab = i55lgnEnab ;
      n55lgnEnab = false ;
      A60lgnPWDF = i60lgnPWDF ;
      n60lgnPWDF = false ;
      A61lgnSupe = i61lgnSupe ;
      n61lgnSupe = false ;
   }

   public void initializeNonKey09188( )
   {
      A62lgnLast = GXutil.nullDate() ;
      n62lgnLast = false ;
   }

   public void initAll09188( )
   {
      A63lgnLast = "" ;
      initializeNonKey09188( ) ;
   }

   public void standaloneModalInsert09188( )
   {
   }

   public void initializeNonKey09189( )
   {
      A65ustDesc = "" ;
      n65ustDesc = false ;
   }

   public void initAll09189( )
   {
      A64ustCode = "" ;
      initializeNonKey09189( ) ;
   }

   public void standaloneModalInsert09189( )
   {
   }

   public void VarsToRow187( SdtLogins obj187 )
   {
      obj187.setgxTv_SdtLogins_Mode( Gx_mode );
      obj187.setgxTv_SdtLogins_Lgnname( A43lgnName );
      obj187.setgxTv_SdtLogins_Lgnenable( A55lgnEnab );
      obj187.setgxTv_SdtLogins_Lgnemail( A56lgnEmai );
      obj187.setgxTv_SdtLogins_Lgnpwd( A57lgnPwd );
      obj187.setgxTv_SdtLogins_Lgnpwdtmp( A227lgnPwd );
      obj187.setgxTv_SdtLogins_Lgnlastlogin( A58lgnLast );
      obj187.setgxTv_SdtLogins_Lgnpwdlastchanged( A59lgnPwdL );
      obj187.setgxTv_SdtLogins_Lgnpwdforcechange( A60lgnPWDF );
      obj187.setgxTv_SdtLogins_Lgnsuperuser( A61lgnSupe );
      obj187.setgxTv_SdtLogins_Lgnlogin( A30lgnLogi );
      obj187.setgxTv_SdtLogins_Lgnlogin_Z( Z30lgnLogi );
      obj187.setgxTv_SdtLogins_Lgnname_Z( Z43lgnName );
      obj187.setgxTv_SdtLogins_Lgnenable_Z( Z55lgnEnab );
      obj187.setgxTv_SdtLogins_Lgnemail_Z( Z56lgnEmai );
      obj187.setgxTv_SdtLogins_Lgnpwd_Z( Z57lgnPwd );
      obj187.setgxTv_SdtLogins_Lgnpwdtmp_Z( Z227lgnPwd );
      obj187.setgxTv_SdtLogins_Lgnlastlogin_Z( Z58lgnLast );
      obj187.setgxTv_SdtLogins_Lgnpwdlastchanged_Z( Z59lgnPwdL );
      obj187.setgxTv_SdtLogins_Lgnpwdforcechange_Z( Z60lgnPWDF );
      obj187.setgxTv_SdtLogins_Lgnsuperuser_Z( Z61lgnSupe );
      obj187.setgxTv_SdtLogins_Lgnlogin_N( (byte)((byte)((n30lgnLogi)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnname_N( (byte)((byte)((n43lgnName)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnenable_N( (byte)((byte)((n55lgnEnab)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnemail_N( (byte)((byte)((n56lgnEmai)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnpwd_N( (byte)((byte)((n57lgnPwd)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnpwdtmp_N( (byte)((byte)((n227lgnPwd)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnlastlogin_N( (byte)((byte)((n58lgnLast)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnpwdlastchanged_N( (byte)((byte)((n59lgnPwdL)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnpwdforcechange_N( (byte)((byte)((n60lgnPWDF)?1:0)) );
      obj187.setgxTv_SdtLogins_Lgnsuperuser_N( (byte)((byte)((n61lgnSupe)?1:0)) );
      obj187.setgxTv_SdtLogins_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars187( SdtLogins obj187 ,
                             int forceLoad )
   {
      Gx_mode = obj187.getgxTv_SdtLogins_Mode() ;
      A43lgnName = obj187.getgxTv_SdtLogins_Lgnname() ;
      A55lgnEnab = obj187.getgxTv_SdtLogins_Lgnenable() ;
      A56lgnEmai = obj187.getgxTv_SdtLogins_Lgnemail() ;
      A57lgnPwd = obj187.getgxTv_SdtLogins_Lgnpwd() ;
      A227lgnPwd = obj187.getgxTv_SdtLogins_Lgnpwdtmp() ;
      A58lgnLast = obj187.getgxTv_SdtLogins_Lgnlastlogin() ;
      A59lgnPwdL = obj187.getgxTv_SdtLogins_Lgnpwdlastchanged() ;
      A60lgnPWDF = obj187.getgxTv_SdtLogins_Lgnpwdforcechange() ;
      A61lgnSupe = obj187.getgxTv_SdtLogins_Lgnsuperuser() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A30lgnLogi = obj187.getgxTv_SdtLogins_Lgnlogin() ;
      }
      Z30lgnLogi = obj187.getgxTv_SdtLogins_Lgnlogin_Z() ;
      Z43lgnName = obj187.getgxTv_SdtLogins_Lgnname_Z() ;
      Z55lgnEnab = obj187.getgxTv_SdtLogins_Lgnenable_Z() ;
      Z56lgnEmai = obj187.getgxTv_SdtLogins_Lgnemail_Z() ;
      Z57lgnPwd = obj187.getgxTv_SdtLogins_Lgnpwd_Z() ;
      Z227lgnPwd = obj187.getgxTv_SdtLogins_Lgnpwdtmp_Z() ;
      Z58lgnLast = obj187.getgxTv_SdtLogins_Lgnlastlogin_Z() ;
      Z59lgnPwdL = obj187.getgxTv_SdtLogins_Lgnpwdlastchanged_Z() ;
      Z60lgnPWDF = obj187.getgxTv_SdtLogins_Lgnpwdforcechange_Z() ;
      Z61lgnSupe = obj187.getgxTv_SdtLogins_Lgnsuperuser_Z() ;
      n30lgnLogi = (boolean)((obj187.getgxTv_SdtLogins_Lgnlogin_N()==0)?false:true) ;
      n43lgnName = (boolean)((obj187.getgxTv_SdtLogins_Lgnname_N()==0)?false:true) ;
      n55lgnEnab = (boolean)((obj187.getgxTv_SdtLogins_Lgnenable_N()==0)?false:true) ;
      n56lgnEmai = (boolean)((obj187.getgxTv_SdtLogins_Lgnemail_N()==0)?false:true) ;
      n57lgnPwd = (boolean)((obj187.getgxTv_SdtLogins_Lgnpwd_N()==0)?false:true) ;
      n227lgnPwd = (boolean)((obj187.getgxTv_SdtLogins_Lgnpwdtmp_N()==0)?false:true) ;
      n58lgnLast = (boolean)((obj187.getgxTv_SdtLogins_Lgnlastlogin_N()==0)?false:true) ;
      n59lgnPwdL = (boolean)((obj187.getgxTv_SdtLogins_Lgnpwdlastchanged_N()==0)?false:true) ;
      n60lgnPWDF = (boolean)((obj187.getgxTv_SdtLogins_Lgnpwdforcechange_N()==0)?false:true) ;
      n61lgnSupe = (boolean)((obj187.getgxTv_SdtLogins_Lgnsuperuser_N()==0)?false:true) ;
      Gx_mode = obj187.getgxTv_SdtLogins_Mode() ;
      return  ;
   }

   public void VarsToRow188( SdtLogins_LastLoginInfo obj188 )
   {
      obj188.setgxTv_SdtLogins_LastLoginInfo_Mode( Gx_mode );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate( A62lgnLast );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd( A63lgnLast );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z( Z63lgnLast );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z( Z62lgnLast );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N( (byte)((byte)((n62lgnLast)?1:0)) );
      obj188.setgxTv_SdtLogins_LastLoginInfo_Modified( nIsMod_188 );
      return  ;
   }

   public void RowToVars188( SdtLogins_LastLoginInfo obj188 ,
                             int forceLoad )
   {
      Gx_mode = obj188.getgxTv_SdtLogins_LastLoginInfo_Mode() ;
      A62lgnLast = obj188.getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate() ;
      A63lgnLast = obj188.getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd() ;
      Z63lgnLast = obj188.getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z() ;
      Z62lgnLast = obj188.getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z() ;
      n62lgnLast = (boolean)((obj188.getgxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N()==0)?false:true) ;
      nIsMod_188 = obj188.getgxTv_SdtLogins_LastLoginInfo_Modified() ;
      return  ;
   }

   public void VarsToRow189( SdtLogins_Profiles obj189 )
   {
      obj189.setgxTv_SdtLogins_Profiles_Mode( Gx_mode );
      obj189.setgxTv_SdtLogins_Profiles_Ustdescription( A65ustDesc );
      obj189.setgxTv_SdtLogins_Profiles_Ustcode( A64ustCode );
      obj189.setgxTv_SdtLogins_Profiles_Ustcode_Z( Z64ustCode );
      obj189.setgxTv_SdtLogins_Profiles_Ustdescription_Z( Z65ustDesc );
      obj189.setgxTv_SdtLogins_Profiles_Ustdescription_N( (byte)((byte)((n65ustDesc)?1:0)) );
      obj189.setgxTv_SdtLogins_Profiles_Modified( nIsMod_189 );
      return  ;
   }

   public void RowToVars189( SdtLogins_Profiles obj189 ,
                             int forceLoad )
   {
      Gx_mode = obj189.getgxTv_SdtLogins_Profiles_Mode() ;
      A65ustDesc = obj189.getgxTv_SdtLogins_Profiles_Ustdescription() ;
      A64ustCode = obj189.getgxTv_SdtLogins_Profiles_Ustcode() ;
      Z64ustCode = obj189.getgxTv_SdtLogins_Profiles_Ustcode_Z() ;
      Z65ustDesc = obj189.getgxTv_SdtLogins_Profiles_Ustdescription_Z() ;
      n65ustDesc = (boolean)((obj189.getgxTv_SdtLogins_Profiles_Ustdescription_N()==0)?false:true) ;
      nIsMod_189 = obj189.getgxTv_SdtLogins_Profiles_Modified() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A30lgnLogi = (String)obj[0] ;
      n30lgnLogi = false ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey09187( ) ;
      scanStart09187( ) ;
      if ( ( RcdFound187 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z30lgnLogi = A30lgnLogi ;
      }
      onLoadActions09187( ) ;
      zm09187( 0) ;
      addRow09187( ) ;
      bcLogins.getgxTv_SdtLogins_Lastlogininfo().clearCollection();
      if ( ( RcdFound187 == 1 ) )
      {
         scanStart09188( ) ;
         nGXsfl_188_idx = (short)(1) ;
         while ( ( RcdFound188 != 0 ) )
         {
            onLoadActions09188( ) ;
            Z30lgnLogi = A30lgnLogi ;
            Z63lgnLast = A63lgnLast ;
            zm09188( 0) ;
            nRcdExists_188 = (short)(1) ;
            nIsMod_188 = (short)(0) ;
            addRow09188( ) ;
            nGXsfl_188_idx = (short)(nGXsfl_188_idx+1) ;
            scanNext09188( ) ;
         }
         scanEnd09188( ) ;
      }
      bcLogins.getgxTv_SdtLogins_Profiles().clearCollection();
      if ( ( RcdFound187 == 1 ) )
      {
         scanStart09189( ) ;
         nGXsfl_189_idx = (short)(1) ;
         while ( ( RcdFound189 != 0 ) )
         {
            onLoadActions09189( ) ;
            Z30lgnLogi = A30lgnLogi ;
            Z64ustCode = A64ustCode ;
            zm09189( 0) ;
            nRcdExists_189 = (short)(1) ;
            nIsMod_189 = (short)(0) ;
            addRow09189( ) ;
            nGXsfl_189_idx = (short)(nGXsfl_189_idx+1) ;
            scanNext09189( ) ;
         }
         scanEnd09189( ) ;
      }
      scanEnd09187( ) ;
      if ( ( RcdFound187 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars187( bcLogins, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey09187( ) ;
      if ( ( RcdFound187 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A30lgnLogi, Z30lgnLogi) != 0 ) )
         {
            A30lgnLogi = Z30lgnLogi ;
            n30lgnLogi = false ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update09187( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A30lgnLogi, Z30lgnLogi) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert09187( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert09187( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow187( bcLogins) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars187( bcLogins, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey09187( ) ;
      if ( ( RcdFound187 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A30lgnLogi, Z30lgnLogi) != 0 ) )
         {
            A30lgnLogi = Z30lgnLogi ;
            n30lgnLogi = false ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A30lgnLogi, Z30lgnLogi) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      pr_default.close(25);
      Application.rollback(context, remoteHandle, "DEFAULT", "tlogins_bc");
      VarsToRow187( bcLogins) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcLogins.getgxTv_SdtLogins_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcLogins.setgxTv_SdtLogins_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtLogins sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcLogins ) )
      {
         bcLogins = sdt ;
         if ( ( GXutil.strcmp(bcLogins.getgxTv_SdtLogins_Mode(), "") == 0 ) )
         {
            bcLogins.setgxTv_SdtLogins_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow187( bcLogins) ;
         }
         else
         {
            RowToVars187( bcLogins, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcLogins.getgxTv_SdtLogins_Mode(), "") == 0 ) )
         {
            bcLogins.setgxTv_SdtLogins_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars187( bcLogins, 1) ;
      return  ;
   }

   public SdtLogins getLogins_BC( )
   {
      return bcLogins ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(25);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z30lgnLogi = "" ;
      A30lgnLogi = "" ;
      sMode187 = "" ;
      nIsMod_189 = (short)(0) ;
      RcdFound189 = (short)(0) ;
      nIsMod_188 = (short)(0) ;
      RcdFound188 = (short)(0) ;
      A57lgnPwd_Visible = 0 ;
      A59lgnPwdL_Enabled = 0 ;
      A58lgnLast_Enabled = 0 ;
      AV9lgnLogi = "" ;
      AV10Random = 0 ;
      AV8Passwor = "" ;
      AV11Encryp = "" ;
      GXv_char4 = new String [1] ;
      GXv_char3 = new String [1] ;
      GXv_svchar2 = new String [1] ;
      GXt_char1 = "" ;
      gxTv_SdtLogins_Lgnlogin_Z = "" ;
      gxTv_SdtLogins_Lgnname_Z = "" ;
      gxTv_SdtLogins_Lgnenable_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail_Z = "" ;
      gxTv_SdtLogins_Lgnpwd_Z = "" ;
      gxTv_SdtLogins_Lgnpwdtmp_Z = "" ;
      gxTv_SdtLogins_Lgnlastlogin_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtLogins_Lgnpwdlastchanged_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtLogins_Lgnpwdforcechange_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser_Z = (byte)(0) ;
      gxTv_SdtLogins_Lgnlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnname_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnenable_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnemail_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwd_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdtmp_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnlastlogin_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdlastchanged_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnpwdforcechange_N = (byte)(0) ;
      gxTv_SdtLogins_Lgnsuperuser_N = (byte)(0) ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = "" ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = GXutil.nullDate() ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(0) ;
      gxTv_SdtLogins_Profiles_Ustcode_Z = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription_Z = "" ;
      gxTv_SdtLogins_Profiles_Ustdescription_N = (byte)(0) ;
      GX_JID = 0 ;
      Z43lgnName = "" ;
      A43lgnName = "" ;
      Z55lgnEnab = (byte)(0) ;
      A55lgnEnab = (byte)(0) ;
      Z56lgnEmai = "" ;
      A56lgnEmai = "" ;
      Z57lgnPwd = "" ;
      A57lgnPwd = "" ;
      Z227lgnPwd = "" ;
      A227lgnPwd = "" ;
      Z58lgnLast = GXutil.resetTime( GXutil.nullDate() );
      A58lgnLast = GXutil.resetTime( GXutil.nullDate() );
      Z59lgnPwdL = GXutil.resetTime( GXutil.nullDate() );
      A59lgnPwdL = GXutil.resetTime( GXutil.nullDate() );
      Z60lgnPWDF = (byte)(0) ;
      A60lgnPWDF = (byte)(0) ;
      Z61lgnSupe = (byte)(0) ;
      A61lgnSupe = (byte)(0) ;
      Z63lgnLast = "" ;
      A63lgnLast = "" ;
      Z62lgnLast = GXutil.nullDate() ;
      A62lgnLast = GXutil.nullDate() ;
      Z64ustCode = "" ;
      A64ustCode = "" ;
      Z65ustDesc = "" ;
      A65ustDesc = "" ;
      Gx_BScreen = (byte)(0) ;
      n55lgnEnab = false ;
      n60lgnPWDF = false ;
      n61lgnSupe = false ;
      n30lgnLogi = false ;
      BC00099_A30lgnLogi = new String[] {""} ;
      BC00099_n30lgnLogi = new boolean[] {false} ;
      BC00099_A43lgnName = new String[] {""} ;
      BC00099_n43lgnName = new boolean[] {false} ;
      BC00099_A55lgnEnab = new byte[1] ;
      BC00099_n55lgnEnab = new boolean[] {false} ;
      BC00099_A56lgnEmai = new String[] {""} ;
      BC00099_n56lgnEmai = new boolean[] {false} ;
      BC00099_A57lgnPwd = new String[] {""} ;
      BC00099_n57lgnPwd = new boolean[] {false} ;
      BC00099_A227lgnPwd = new String[] {""} ;
      BC00099_n227lgnPwd = new boolean[] {false} ;
      BC00099_A58lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC00099_n58lgnLast = new boolean[] {false} ;
      BC00099_A59lgnPwdL = new java.util.Date[] {GXutil.nullDate()} ;
      BC00099_n59lgnPwdL = new boolean[] {false} ;
      BC00099_A60lgnPWDF = new byte[1] ;
      BC00099_n60lgnPWDF = new boolean[] {false} ;
      BC00099_A61lgnSupe = new byte[1] ;
      BC00099_n61lgnSupe = new boolean[] {false} ;
      RcdFound187 = (short)(0) ;
      n43lgnName = false ;
      n56lgnEmai = false ;
      n57lgnPwd = false ;
      n227lgnPwd = false ;
      n58lgnLast = false ;
      n59lgnPwdL = false ;
      BC000910_A30lgnLogi = new String[] {""} ;
      BC000910_n30lgnLogi = new boolean[] {false} ;
      BC00098_A30lgnLogi = new String[] {""} ;
      BC00098_n30lgnLogi = new boolean[] {false} ;
      BC00098_A43lgnName = new String[] {""} ;
      BC00098_n43lgnName = new boolean[] {false} ;
      BC00098_A55lgnEnab = new byte[1] ;
      BC00098_n55lgnEnab = new boolean[] {false} ;
      BC00098_A56lgnEmai = new String[] {""} ;
      BC00098_n56lgnEmai = new boolean[] {false} ;
      BC00098_A57lgnPwd = new String[] {""} ;
      BC00098_n57lgnPwd = new boolean[] {false} ;
      BC00098_A227lgnPwd = new String[] {""} ;
      BC00098_n227lgnPwd = new boolean[] {false} ;
      BC00098_A58lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC00098_n58lgnLast = new boolean[] {false} ;
      BC00098_A59lgnPwdL = new java.util.Date[] {GXutil.nullDate()} ;
      BC00098_n59lgnPwdL = new boolean[] {false} ;
      BC00098_A60lgnPWDF = new byte[1] ;
      BC00098_n60lgnPWDF = new boolean[] {false} ;
      BC00098_A61lgnSupe = new byte[1] ;
      BC00098_n61lgnSupe = new boolean[] {false} ;
      BC00097_A30lgnLogi = new String[] {""} ;
      BC00097_n30lgnLogi = new boolean[] {false} ;
      BC00097_A43lgnName = new String[] {""} ;
      BC00097_n43lgnName = new boolean[] {false} ;
      BC00097_A55lgnEnab = new byte[1] ;
      BC00097_n55lgnEnab = new boolean[] {false} ;
      BC00097_A56lgnEmai = new String[] {""} ;
      BC00097_n56lgnEmai = new boolean[] {false} ;
      BC00097_A57lgnPwd = new String[] {""} ;
      BC00097_n57lgnPwd = new boolean[] {false} ;
      BC00097_A227lgnPwd = new String[] {""} ;
      BC00097_n227lgnPwd = new boolean[] {false} ;
      BC00097_A58lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC00097_n58lgnLast = new boolean[] {false} ;
      BC00097_A59lgnPwdL = new java.util.Date[] {GXutil.nullDate()} ;
      BC00097_n59lgnPwdL = new boolean[] {false} ;
      BC00097_A60lgnPWDF = new byte[1] ;
      BC00097_n60lgnPWDF = new boolean[] {false} ;
      BC00097_A61lgnSupe = new byte[1] ;
      BC00097_n61lgnSupe = new boolean[] {false} ;
      Gx_longc = false ;
      BC000914_A23ISOCod = new String[] {""} ;
      BC000914_A448ISOCon = new String[] {""} ;
      BC000915_A42Downloa = new long[1] ;
      nRcdExists_188 = (short)(0) ;
      Gxremove188 = (byte)(0) ;
      nRcdExists_189 = (short)(0) ;
      Gxremove189 = (byte)(0) ;
      BC000916_A30lgnLogi = new String[] {""} ;
      BC000916_n30lgnLogi = new boolean[] {false} ;
      BC000916_A43lgnName = new String[] {""} ;
      BC000916_n43lgnName = new boolean[] {false} ;
      BC000916_A55lgnEnab = new byte[1] ;
      BC000916_n55lgnEnab = new boolean[] {false} ;
      BC000916_A56lgnEmai = new String[] {""} ;
      BC000916_n56lgnEmai = new boolean[] {false} ;
      BC000916_A57lgnPwd = new String[] {""} ;
      BC000916_n57lgnPwd = new boolean[] {false} ;
      BC000916_A227lgnPwd = new String[] {""} ;
      BC000916_n227lgnPwd = new boolean[] {false} ;
      BC000916_A58lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC000916_n58lgnLast = new boolean[] {false} ;
      BC000916_A59lgnPwdL = new java.util.Date[] {GXutil.nullDate()} ;
      BC000916_n59lgnPwdL = new boolean[] {false} ;
      BC000916_A60lgnPWDF = new byte[1] ;
      BC000916_n60lgnPWDF = new boolean[] {false} ;
      BC000916_A61lgnSupe = new byte[1] ;
      BC000916_n61lgnSupe = new boolean[] {false} ;
      BC000917_A30lgnLogi = new String[] {""} ;
      BC000917_n30lgnLogi = new boolean[] {false} ;
      BC000917_A63lgnLast = new String[] {""} ;
      BC000917_A62lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC000917_n62lgnLast = new boolean[] {false} ;
      n62lgnLast = false ;
      BC000918_A30lgnLogi = new String[] {""} ;
      BC000918_n30lgnLogi = new boolean[] {false} ;
      BC000918_A63lgnLast = new String[] {""} ;
      BC00096_A30lgnLogi = new String[] {""} ;
      BC00096_n30lgnLogi = new boolean[] {false} ;
      BC00096_A63lgnLast = new String[] {""} ;
      BC00096_A62lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC00096_n62lgnLast = new boolean[] {false} ;
      sMode188 = "" ;
      BC00095_A30lgnLogi = new String[] {""} ;
      BC00095_n30lgnLogi = new boolean[] {false} ;
      BC00095_A63lgnLast = new String[] {""} ;
      BC00095_A62lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC00095_n62lgnLast = new boolean[] {false} ;
      BC000922_A30lgnLogi = new String[] {""} ;
      BC000922_n30lgnLogi = new boolean[] {false} ;
      BC000922_A63lgnLast = new String[] {""} ;
      BC000922_A62lgnLast = new java.util.Date[] {GXutil.nullDate()} ;
      BC000922_n62lgnLast = new boolean[] {false} ;
      BC000923_A30lgnLogi = new String[] {""} ;
      BC000923_n30lgnLogi = new boolean[] {false} ;
      BC000923_A65ustDesc = new String[] {""} ;
      BC000923_n65ustDesc = new boolean[] {false} ;
      BC000923_A64ustCode = new String[] {""} ;
      n65ustDesc = false ;
      BC00094_A65ustDesc = new String[] {""} ;
      BC00094_n65ustDesc = new boolean[] {false} ;
      BC000924_A30lgnLogi = new String[] {""} ;
      BC000924_n30lgnLogi = new boolean[] {false} ;
      BC000924_A64ustCode = new String[] {""} ;
      BC00093_A30lgnLogi = new String[] {""} ;
      BC00093_n30lgnLogi = new boolean[] {false} ;
      BC00093_A64ustCode = new String[] {""} ;
      sMode189 = "" ;
      BC00092_A30lgnLogi = new String[] {""} ;
      BC00092_n30lgnLogi = new boolean[] {false} ;
      BC00092_A64ustCode = new String[] {""} ;
      BC000927_A65ustDesc = new String[] {""} ;
      BC000927_n65ustDesc = new boolean[] {false} ;
      BC000928_A30lgnLogi = new String[] {""} ;
      BC000928_n30lgnLogi = new boolean[] {false} ;
      BC000928_A65ustDesc = new String[] {""} ;
      BC000928_n65ustDesc = new boolean[] {false} ;
      BC000928_A64ustCode = new String[] {""} ;
      i55lgnEnab = (byte)(0) ;
      i60lgnPWDF = (byte)(0) ;
      i61lgnSupe = (byte)(0) ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tlogins_bc__default(),
         new Object[] {
             new Object[] {
            BC00092_A30lgnLogi, BC00092_A64ustCode
            }
            , new Object[] {
            BC00093_A30lgnLogi, BC00093_A64ustCode
            }
            , new Object[] {
            BC00094_A65ustDesc, BC00094_n65ustDesc
            }
            , new Object[] {
            BC00095_A30lgnLogi, BC00095_A63lgnLast, BC00095_A62lgnLast, BC00095_n62lgnLast
            }
            , new Object[] {
            BC00096_A30lgnLogi, BC00096_A63lgnLast, BC00096_A62lgnLast, BC00096_n62lgnLast
            }
            , new Object[] {
            BC00097_A30lgnLogi, BC00097_A43lgnName, BC00097_n43lgnName, BC00097_A55lgnEnab, BC00097_n55lgnEnab, BC00097_A56lgnEmai, BC00097_n56lgnEmai, BC00097_A57lgnPwd, BC00097_n57lgnPwd, BC00097_A227lgnPwd,
            BC00097_n227lgnPwd, BC00097_A58lgnLast, BC00097_n58lgnLast, BC00097_A59lgnPwdL, BC00097_n59lgnPwdL, BC00097_A60lgnPWDF, BC00097_n60lgnPWDF, BC00097_A61lgnSupe, BC00097_n61lgnSupe
            }
            , new Object[] {
            BC00098_A30lgnLogi, BC00098_A43lgnName, BC00098_n43lgnName, BC00098_A55lgnEnab, BC00098_n55lgnEnab, BC00098_A56lgnEmai, BC00098_n56lgnEmai, BC00098_A57lgnPwd, BC00098_n57lgnPwd, BC00098_A227lgnPwd,
            BC00098_n227lgnPwd, BC00098_A58lgnLast, BC00098_n58lgnLast, BC00098_A59lgnPwdL, BC00098_n59lgnPwdL, BC00098_A60lgnPWDF, BC00098_n60lgnPWDF, BC00098_A61lgnSupe, BC00098_n61lgnSupe
            }
            , new Object[] {
            BC00099_A30lgnLogi, BC00099_A43lgnName, BC00099_n43lgnName, BC00099_A55lgnEnab, BC00099_n55lgnEnab, BC00099_A56lgnEmai, BC00099_n56lgnEmai, BC00099_A57lgnPwd, BC00099_n57lgnPwd, BC00099_A227lgnPwd,
            BC00099_n227lgnPwd, BC00099_A58lgnLast, BC00099_n58lgnLast, BC00099_A59lgnPwdL, BC00099_n59lgnPwdL, BC00099_A60lgnPWDF, BC00099_n60lgnPWDF, BC00099_A61lgnSupe, BC00099_n61lgnSupe
            }
            , new Object[] {
            BC000910_A30lgnLogi
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000914_A23ISOCod, BC000914_A448ISOCon
            }
            , new Object[] {
            BC000915_A42Downloa
            }
            , new Object[] {
            BC000916_A30lgnLogi, BC000916_A43lgnName, BC000916_n43lgnName, BC000916_A55lgnEnab, BC000916_n55lgnEnab, BC000916_A56lgnEmai, BC000916_n56lgnEmai, BC000916_A57lgnPwd, BC000916_n57lgnPwd, BC000916_A227lgnPwd,
            BC000916_n227lgnPwd, BC000916_A58lgnLast, BC000916_n58lgnLast, BC000916_A59lgnPwdL, BC000916_n59lgnPwdL, BC000916_A60lgnPWDF, BC000916_n60lgnPWDF, BC000916_A61lgnSupe, BC000916_n61lgnSupe
            }
            , new Object[] {
            BC000917_A30lgnLogi, BC000917_A63lgnLast, BC000917_A62lgnLast, BC000917_n62lgnLast
            }
            , new Object[] {
            BC000918_A30lgnLogi, BC000918_A63lgnLast
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000922_A30lgnLogi, BC000922_A63lgnLast, BC000922_A62lgnLast, BC000922_n62lgnLast
            }
            , new Object[] {
            BC000923_A30lgnLogi, BC000923_A65ustDesc, BC000923_n65ustDesc, BC000923_A64ustCode
            }
            , new Object[] {
            BC000924_A30lgnLogi, BC000924_A64ustCode
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC000927_A65ustDesc, BC000927_n65ustDesc
            }
            , new Object[] {
            BC000928_A30lgnLogi, BC000928_A65ustDesc, BC000928_n65ustDesc, BC000928_A64ustCode
            }
         }
      );
      Z61lgnSupe = (byte)(0) ;
      n61lgnSupe = false ;
      Z60lgnPWDF = (byte)(1) ;
      n60lgnPWDF = false ;
      Z55lgnEnab = (byte)(1) ;
      n55lgnEnab = false ;
      /* Execute Start event if defined. */
      /* Execute user event: e11092 */
      e11092 ();
   }

   private byte nKeyPressed ;
   private byte gxTv_SdtLogins_Lgnenable_Z ;
   private byte gxTv_SdtLogins_Lgnpwdforcechange_Z ;
   private byte gxTv_SdtLogins_Lgnsuperuser_Z ;
   private byte gxTv_SdtLogins_Lgnlogin_N ;
   private byte gxTv_SdtLogins_Lgnname_N ;
   private byte gxTv_SdtLogins_Lgnenable_N ;
   private byte gxTv_SdtLogins_Lgnemail_N ;
   private byte gxTv_SdtLogins_Lgnpwd_N ;
   private byte gxTv_SdtLogins_Lgnpwdtmp_N ;
   private byte gxTv_SdtLogins_Lgnlastlogin_N ;
   private byte gxTv_SdtLogins_Lgnpwdlastchanged_N ;
   private byte gxTv_SdtLogins_Lgnpwdforcechange_N ;
   private byte gxTv_SdtLogins_Lgnsuperuser_N ;
   private byte gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N ;
   private byte gxTv_SdtLogins_Profiles_Ustdescription_N ;
   private byte Z55lgnEnab ;
   private byte A55lgnEnab ;
   private byte Z60lgnPWDF ;
   private byte A60lgnPWDF ;
   private byte Z61lgnSupe ;
   private byte A61lgnSupe ;
   private byte Gx_BScreen ;
   private byte Gxremove188 ;
   private byte Gxremove189 ;
   private byte i55lgnEnab ;
   private byte i60lgnPWDF ;
   private byte i61lgnSupe ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short nGXsfl_189_idx=1 ;
   private short nIsMod_189 ;
   private short RcdFound189 ;
   private short nGXsfl_188_idx=1 ;
   private short nIsMod_188 ;
   private short RcdFound188 ;
   private short RcdFound187 ;
   private short nRcdExists_188 ;
   private short nRcdExists_189 ;
   private int trnEnded ;
   private int A57lgnPwd_Visible ;
   private int A59lgnPwdL_Enabled ;
   private int A58lgnLast_Enabled ;
   private int AV10Random ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode187 ;
   private String AV8Passwor ;
   private String AV11Encryp ;
   private String GXv_char4[] ;
   private String GXv_char3[] ;
   private String GXt_char1 ;
   private String sMode188 ;
   private String sMode189 ;
   private java.util.Date gxTv_SdtLogins_Lgnlastlogin_Z ;
   private java.util.Date gxTv_SdtLogins_Lgnpwdlastchanged_Z ;
   private java.util.Date Z58lgnLast ;
   private java.util.Date A58lgnLast ;
   private java.util.Date Z59lgnPwdL ;
   private java.util.Date A59lgnPwdL ;
   private java.util.Date gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z ;
   private java.util.Date Z62lgnLast ;
   private java.util.Date A62lgnLast ;
   private boolean n55lgnEnab ;
   private boolean n60lgnPWDF ;
   private boolean n61lgnSupe ;
   private boolean n30lgnLogi ;
   private boolean n43lgnName ;
   private boolean n56lgnEmai ;
   private boolean n57lgnPwd ;
   private boolean n227lgnPwd ;
   private boolean n58lgnLast ;
   private boolean n59lgnPwdL ;
   private boolean Gx_longc ;
   private boolean n62lgnLast ;
   private boolean n65ustDesc ;
   private String Z30lgnLogi ;
   private String A30lgnLogi ;
   private String AV9lgnLogi ;
   private String GXv_svchar2[] ;
   private String gxTv_SdtLogins_Lgnlogin_Z ;
   private String gxTv_SdtLogins_Lgnname_Z ;
   private String gxTv_SdtLogins_Lgnemail_Z ;
   private String gxTv_SdtLogins_Lgnpwd_Z ;
   private String gxTv_SdtLogins_Lgnpwdtmp_Z ;
   private String gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z ;
   private String gxTv_SdtLogins_Profiles_Ustcode_Z ;
   private String gxTv_SdtLogins_Profiles_Ustdescription_Z ;
   private String Z43lgnName ;
   private String A43lgnName ;
   private String Z56lgnEmai ;
   private String A56lgnEmai ;
   private String Z57lgnPwd ;
   private String A57lgnPwd ;
   private String Z227lgnPwd ;
   private String A227lgnPwd ;
   private String Z63lgnLast ;
   private String A63lgnLast ;
   private String Z64ustCode ;
   private String A64ustCode ;
   private String Z65ustDesc ;
   private String A65ustDesc ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtLogins bcLogins ;
   private IDataStoreProvider pr_default ;
   private String[] BC00099_A30lgnLogi ;
   private boolean[] BC00099_n30lgnLogi ;
   private String[] BC00099_A43lgnName ;
   private boolean[] BC00099_n43lgnName ;
   private byte[] BC00099_A55lgnEnab ;
   private boolean[] BC00099_n55lgnEnab ;
   private String[] BC00099_A56lgnEmai ;
   private boolean[] BC00099_n56lgnEmai ;
   private String[] BC00099_A57lgnPwd ;
   private boolean[] BC00099_n57lgnPwd ;
   private String[] BC00099_A227lgnPwd ;
   private boolean[] BC00099_n227lgnPwd ;
   private java.util.Date[] BC00099_A58lgnLast ;
   private boolean[] BC00099_n58lgnLast ;
   private java.util.Date[] BC00099_A59lgnPwdL ;
   private boolean[] BC00099_n59lgnPwdL ;
   private byte[] BC00099_A60lgnPWDF ;
   private boolean[] BC00099_n60lgnPWDF ;
   private byte[] BC00099_A61lgnSupe ;
   private boolean[] BC00099_n61lgnSupe ;
   private String[] BC000910_A30lgnLogi ;
   private boolean[] BC000910_n30lgnLogi ;
   private String[] BC00098_A30lgnLogi ;
   private boolean[] BC00098_n30lgnLogi ;
   private String[] BC00098_A43lgnName ;
   private boolean[] BC00098_n43lgnName ;
   private byte[] BC00098_A55lgnEnab ;
   private boolean[] BC00098_n55lgnEnab ;
   private String[] BC00098_A56lgnEmai ;
   private boolean[] BC00098_n56lgnEmai ;
   private String[] BC00098_A57lgnPwd ;
   private boolean[] BC00098_n57lgnPwd ;
   private String[] BC00098_A227lgnPwd ;
   private boolean[] BC00098_n227lgnPwd ;
   private java.util.Date[] BC00098_A58lgnLast ;
   private boolean[] BC00098_n58lgnLast ;
   private java.util.Date[] BC00098_A59lgnPwdL ;
   private boolean[] BC00098_n59lgnPwdL ;
   private byte[] BC00098_A60lgnPWDF ;
   private boolean[] BC00098_n60lgnPWDF ;
   private byte[] BC00098_A61lgnSupe ;
   private boolean[] BC00098_n61lgnSupe ;
   private String[] BC00097_A30lgnLogi ;
   private boolean[] BC00097_n30lgnLogi ;
   private String[] BC00097_A43lgnName ;
   private boolean[] BC00097_n43lgnName ;
   private byte[] BC00097_A55lgnEnab ;
   private boolean[] BC00097_n55lgnEnab ;
   private String[] BC00097_A56lgnEmai ;
   private boolean[] BC00097_n56lgnEmai ;
   private String[] BC00097_A57lgnPwd ;
   private boolean[] BC00097_n57lgnPwd ;
   private String[] BC00097_A227lgnPwd ;
   private boolean[] BC00097_n227lgnPwd ;
   private java.util.Date[] BC00097_A58lgnLast ;
   private boolean[] BC00097_n58lgnLast ;
   private java.util.Date[] BC00097_A59lgnPwdL ;
   private boolean[] BC00097_n59lgnPwdL ;
   private byte[] BC00097_A60lgnPWDF ;
   private boolean[] BC00097_n60lgnPWDF ;
   private byte[] BC00097_A61lgnSupe ;
   private boolean[] BC00097_n61lgnSupe ;
   private String[] BC000914_A23ISOCod ;
   private String[] BC000914_A448ISOCon ;
   private long[] BC000915_A42Downloa ;
   private String[] BC000916_A30lgnLogi ;
   private boolean[] BC000916_n30lgnLogi ;
   private String[] BC000916_A43lgnName ;
   private boolean[] BC000916_n43lgnName ;
   private byte[] BC000916_A55lgnEnab ;
   private boolean[] BC000916_n55lgnEnab ;
   private String[] BC000916_A56lgnEmai ;
   private boolean[] BC000916_n56lgnEmai ;
   private String[] BC000916_A57lgnPwd ;
   private boolean[] BC000916_n57lgnPwd ;
   private String[] BC000916_A227lgnPwd ;
   private boolean[] BC000916_n227lgnPwd ;
   private java.util.Date[] BC000916_A58lgnLast ;
   private boolean[] BC000916_n58lgnLast ;
   private java.util.Date[] BC000916_A59lgnPwdL ;
   private boolean[] BC000916_n59lgnPwdL ;
   private byte[] BC000916_A60lgnPWDF ;
   private boolean[] BC000916_n60lgnPWDF ;
   private byte[] BC000916_A61lgnSupe ;
   private boolean[] BC000916_n61lgnSupe ;
   private String[] BC000917_A30lgnLogi ;
   private boolean[] BC000917_n30lgnLogi ;
   private String[] BC000917_A63lgnLast ;
   private java.util.Date[] BC000917_A62lgnLast ;
   private boolean[] BC000917_n62lgnLast ;
   private String[] BC000918_A30lgnLogi ;
   private boolean[] BC000918_n30lgnLogi ;
   private String[] BC000918_A63lgnLast ;
   private String[] BC00096_A30lgnLogi ;
   private boolean[] BC00096_n30lgnLogi ;
   private String[] BC00096_A63lgnLast ;
   private java.util.Date[] BC00096_A62lgnLast ;
   private boolean[] BC00096_n62lgnLast ;
   private String[] BC00095_A30lgnLogi ;
   private boolean[] BC00095_n30lgnLogi ;
   private String[] BC00095_A63lgnLast ;
   private java.util.Date[] BC00095_A62lgnLast ;
   private boolean[] BC00095_n62lgnLast ;
   private String[] BC000922_A30lgnLogi ;
   private boolean[] BC000922_n30lgnLogi ;
   private String[] BC000922_A63lgnLast ;
   private java.util.Date[] BC000922_A62lgnLast ;
   private boolean[] BC000922_n62lgnLast ;
   private String[] BC000923_A30lgnLogi ;
   private boolean[] BC000923_n30lgnLogi ;
   private String[] BC000923_A65ustDesc ;
   private boolean[] BC000923_n65ustDesc ;
   private String[] BC000923_A64ustCode ;
   private String[] BC00094_A65ustDesc ;
   private boolean[] BC00094_n65ustDesc ;
   private String[] BC000924_A30lgnLogi ;
   private boolean[] BC000924_n30lgnLogi ;
   private String[] BC000924_A64ustCode ;
   private String[] BC00093_A30lgnLogi ;
   private boolean[] BC00093_n30lgnLogi ;
   private String[] BC00093_A64ustCode ;
   private String[] BC00092_A30lgnLogi ;
   private boolean[] BC00092_n30lgnLogi ;
   private String[] BC00092_A64ustCode ;
   private String[] BC000927_A65ustDesc ;
   private boolean[] BC000927_n65ustDesc ;
   private String[] BC000928_A30lgnLogi ;
   private boolean[] BC000928_n30lgnLogi ;
   private String[] BC000928_A65ustDesc ;
   private boolean[] BC000928_n65ustDesc ;
   private String[] BC000928_A64ustCode ;
}

final  class tlogins_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00092", "SELECT [lgnLogin], [ustCode] FROM [LOGINSPROFILES] WITH (UPDLOCK) WHERE [lgnLogin] = ? AND [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00093", "SELECT [lgnLogin], [ustCode] FROM [LOGINSPROFILES] WITH (NOLOCK) WHERE [lgnLogin] = ? AND [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00094", "SELECT [ustDescription] FROM [USERPROFILES] WITH (NOLOCK) WHERE [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00095", "SELECT [lgnLogin], [lgnLastPwd], [lgnLastPwdDate] FROM [LOGINSLASTLOGININFO] WITH (UPDLOCK) WHERE [lgnLogin] = ? AND [lgnLastPwd] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00096", "SELECT [lgnLogin], [lgnLastPwd], [lgnLastPwdDate] FROM [LOGINSLASTLOGININFO] WITH (NOLOCK) WHERE [lgnLogin] = ? AND [lgnLastPwd] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00097", "SELECT [lgnLogin], [lgnName], [lgnEnable], [lgnEmail], [lgnPwd], [lgnPwdTmp], [lgnLastLogin], [lgnPwdLastChanged], [lgnPWDForceChange], [lgnSuperUser] FROM [LOGINS] WITH (UPDLOCK) WHERE [lgnLogin] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00098", "SELECT [lgnLogin], [lgnName], [lgnEnable], [lgnEmail], [lgnPwd], [lgnPwdTmp], [lgnLastLogin], [lgnPwdLastChanged], [lgnPWDForceChange], [lgnSuperUser] FROM [LOGINS] WITH (NOLOCK) WHERE [lgnLogin] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00099", "SELECT TM1.[lgnLogin], TM1.[lgnName], TM1.[lgnEnable], TM1.[lgnEmail], TM1.[lgnPwd], TM1.[lgnPwdTmp], TM1.[lgnLastLogin], TM1.[lgnPwdLastChanged], TM1.[lgnPWDForceChange], TM1.[lgnSuperUser] FROM [LOGINS] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[lgnLogin] = ? ORDER BY TM1.[lgnLogin] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000910", "SELECT [lgnLogin] FROM [LOGINS] WITH (FASTFIRSTROW NOLOCK) WHERE [lgnLogin] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000911", "INSERT INTO [LOGINS] ([lgnLogin], [lgnName], [lgnEnable], [lgnEmail], [lgnPwd], [lgnPwdTmp], [lgnLastLogin], [lgnPwdLastChanged], [lgnPWDForceChange], [lgnSuperUser]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000912", "UPDATE [LOGINS] SET [lgnName]=?, [lgnEnable]=?, [lgnEmail]=?, [lgnPwd]=?, [lgnPwdTmp]=?, [lgnLastLogin]=?, [lgnPwdLastChanged]=?, [lgnPWDForceChange]=?, [lgnSuperUser]=?  WHERE [lgnLogin] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000913", "DELETE FROM [LOGINS]  WHERE [lgnLogin] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000914", "SELECT TOP 1 [ISOCod], [ISOConfigID] FROM [COUNTRYPARAMETER] WITH (NOLOCK) WHERE [lgnLogin] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000915", "SELECT TOP 1 [DownloadID] FROM [DOWNLOAD] WITH (NOLOCK) WHERE [lgnLogin] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC000916", "SELECT TM1.[lgnLogin], TM1.[lgnName], TM1.[lgnEnable], TM1.[lgnEmail], TM1.[lgnPwd], TM1.[lgnPwdTmp], TM1.[lgnLastLogin], TM1.[lgnPwdLastChanged], TM1.[lgnPWDForceChange], TM1.[lgnSuperUser] FROM [LOGINS] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[lgnLogin] = ? ORDER BY TM1.[lgnLogin] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000917", "SELECT [lgnLogin], [lgnLastPwd], [lgnLastPwdDate] FROM [LOGINSLASTLOGININFO] WITH (FASTFIRSTROW NOLOCK) WHERE [lgnLogin] = ? and [lgnLastPwd] = ? ORDER BY [lgnLogin], [lgnLastPwd] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000918", "SELECT [lgnLogin], [lgnLastPwd] FROM [LOGINSLASTLOGININFO] WITH (FASTFIRSTROW NOLOCK) WHERE [lgnLogin] = ? AND [lgnLastPwd] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000919", "INSERT INTO [LOGINSLASTLOGININFO] ([lgnLogin], [lgnLastPwd], [lgnLastPwdDate]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000920", "UPDATE [LOGINSLASTLOGININFO] SET [lgnLastPwdDate]=?  WHERE [lgnLogin] = ? AND [lgnLastPwd] = ?", GX_NOMASK)
         ,new UpdateCursor("BC000921", "DELETE FROM [LOGINSLASTLOGININFO]  WHERE [lgnLogin] = ? AND [lgnLastPwd] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000922", "SELECT [lgnLogin], [lgnLastPwd], [lgnLastPwdDate] FROM [LOGINSLASTLOGININFO] WITH (FASTFIRSTROW NOLOCK) WHERE [lgnLogin] = ? ORDER BY [lgnLogin], [lgnLastPwd] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000923", "SELECT T1.[lgnLogin], T2.[ustDescription], T1.[ustCode] FROM ([LOGINSPROFILES] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [USERPROFILES] T2 WITH (NOLOCK) ON T2.[ustCode] = T1.[ustCode]) WHERE T1.[lgnLogin] = ? and T1.[ustCode] = ? ORDER BY T1.[lgnLogin], T1.[ustCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000924", "SELECT [lgnLogin], [ustCode] FROM [LOGINSPROFILES] WITH (FASTFIRSTROW NOLOCK) WHERE [lgnLogin] = ? AND [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC000925", "INSERT INTO [LOGINSPROFILES] ([lgnLogin], [ustCode]) VALUES (?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC000926", "DELETE FROM [LOGINSPROFILES]  WHERE [lgnLogin] = ? AND [ustCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC000927", "SELECT [ustDescription] FROM [USERPROFILES] WITH (NOLOCK) WHERE [ustCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC000928", "SELECT T1.[lgnLogin], T2.[ustDescription], T1.[ustCode] FROM ([LOGINSPROFILES] T1 WITH (FASTFIRSTROW NOLOCK) INNER JOIN [USERPROFILES] T2 WITH (NOLOCK) ON T2.[ustCode] = T1.[ustCode]) WHERE T1.[lgnLogin] = ? ORDER BY T1.[lgnLogin], T1.[ustCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDateTime(7) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[13])[0] = rslt.getGXDateTime(8) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((byte[]) buf[15])[0] = rslt.getByte(9) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((byte[]) buf[17])[0] = rslt.getByte(10) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDateTime(7) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[13])[0] = rslt.getGXDateTime(8) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((byte[]) buf[15])[0] = rslt.getByte(9) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((byte[]) buf[17])[0] = rslt.getByte(10) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDateTime(7) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[13])[0] = rslt.getGXDateTime(8) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((byte[]) buf[15])[0] = rslt.getByte(9) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((byte[]) buf[17])[0] = rslt.getByte(10) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 13 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDateTime(7) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[13])[0] = rslt.getGXDateTime(8) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((byte[]) buf[15])[0] = rslt.getByte(9) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((byte[]) buf[17])[0] = rslt.getByte(10) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               break;
            case 15 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 20 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 21 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               break;
            case 22 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 25 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 26 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 1 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 8 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 50);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(3, ((Number) parms[5]).byteValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[7], 80);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[9], 255);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(6, (String)parms[11], 255);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(7, (java.util.Date)parms[13], false);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(8, (java.util.Date)parms[15], false);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(9, ((Number) parms[17]).byteValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(10, ((Number) parms[19]).byteValue());
               }
               break;
            case 10 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 50);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(2, ((Number) parms[3]).byteValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 80);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(4, (String)parms[7], 255);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(5, (String)parms[9], 255);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(6, (java.util.Date)parms[11], false);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(7, (java.util.Date)parms[13], false);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(8, ((Number) parms[15]).byteValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.NUMERIC );
               }
               else
               {
                  stmt.setByte(9, ((Number) parms[17]).byteValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(10, (String)parms[19], 20, false);
               }
               break;
            case 11 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 12 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20);
               }
               break;
            case 13 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20);
               }
               break;
            case 14 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 15 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 16 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 17 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(3, (java.util.Date)parms[4]);
               }
               break;
            case 18 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(1, (java.util.Date)parms[1]);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 20, false);
               }
               stmt.setVarchar(3, (String)parms[4], 255, false);
               break;
            case 19 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 255, false);
               break;
            case 20 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
            case 21 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 22 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 23 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 24 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               stmt.setVarchar(2, (String)parms[2], 20, false);
               break;
            case 25 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 26 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 20, false);
               }
               break;
      }
   }

}


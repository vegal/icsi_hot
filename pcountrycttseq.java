/*
               File: CountryCttSeq
        Description: Gerador de ID Country Contact Sequence
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pcountrycttseq extends GXProcedure
{
   public pcountrycttseq( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pcountrycttseq.class ), "" );
   }

   public pcountrycttseq( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        short[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             short[] aP2 )
   {
      pcountrycttseq.this.AV8ISOCod = aP0;
      pcountrycttseq.this.AV11Contac = aP1;
      pcountrycttseq.this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV15GXLvl1 = (byte)(0) ;
      /* Using cursor P005D2 */
      pr_default.execute(0, new Object[] {AV8ISOCod, AV11Contac});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A23ISOCod = P005D2_A23ISOCod[0] ;
         A365Contac = P005D2_A365Contac[0] ;
         A406ISOCtt = P005D2_A406ISOCtt[0] ;
         AV15GXLvl1 = (byte)(1) ;
         AV12ISOCtt = (short)(A406ISOCtt+1) ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( ( AV15GXLvl1 == 0 ) )
      {
         AV12ISOCtt = (short)(1) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP2[0] = pcountrycttseq.this.AV12ISOCtt;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV12ISOCtt = (short)(0) ;
      AV15GXLvl1 = (byte)(0) ;
      scmdbuf = "" ;
      P005D2_A23ISOCod = new String[] {""} ;
      P005D2_A365Contac = new String[] {""} ;
      P005D2_A406ISOCtt = new short[1] ;
      A23ISOCod = "" ;
      A365Contac = "" ;
      A406ISOCtt = (short)(0) ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pcountrycttseq__default(),
         new Object[] {
             new Object[] {
            P005D2_A23ISOCod, P005D2_A365Contac, P005D2_A406ISOCtt
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV15GXLvl1 ;
   private short AV12ISOCtt ;
   private short A406ISOCtt ;
   private short Gx_err ;
   private String scmdbuf ;
   private String AV8ISOCod ;
   private String AV11Contac ;
   private String A23ISOCod ;
   private String A365Contac ;
   private short[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P005D2_A23ISOCod ;
   private String[] P005D2_A365Contac ;
   private short[] P005D2_A406ISOCtt ;
}

final  class pcountrycttseq__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P005D2", "SELECT TOP 1 [ISOCod], [ContactTypesCode], [ISOCttSeq] FROM [COUNTRYCONTACTTYPES] WITH (NOLOCK) WHERE ([ISOCod] = ?) AND ([ContactTypesCode] = ?) ORDER BY [ISOCttSeq] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 3);
               stmt.setVarchar(2, (String)parms[1], 5);
               break;
      }
   }

}



public final  class StructSdtCountry_Currencies implements Cloneable, java.io.Serializable
{
   public StructSdtCountry_Currencies( )
   {
      gxTv_SdtCountry_Currencies_Curcode = "" ;
      gxTv_SdtCountry_Currencies_Curplaces = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription = "" ;
      gxTv_SdtCountry_Currencies_Mode = "" ;
      gxTv_SdtCountry_Currencies_Modified = (short)(0) ;
      gxTv_SdtCountry_Currencies_Curcode_Z = "" ;
      gxTv_SdtCountry_Currencies_Curplaces_Z = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription_Z = "" ;
      gxTv_SdtCountry_Currencies_Curplaces_N = (byte)(0) ;
      gxTv_SdtCountry_Currencies_Curdescription_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getCurcode( )
   {
      return gxTv_SdtCountry_Currencies_Curcode ;
   }

   public void setCurcode( String value )
   {
      gxTv_SdtCountry_Currencies_Curcode = value ;
      return  ;
   }

   public byte getCurplaces( )
   {
      return gxTv_SdtCountry_Currencies_Curplaces ;
   }

   public void setCurplaces( byte value )
   {
      gxTv_SdtCountry_Currencies_Curplaces = value ;
      return  ;
   }

   public String getCurdescription( )
   {
      return gxTv_SdtCountry_Currencies_Curdescription ;
   }

   public void setCurdescription( String value )
   {
      gxTv_SdtCountry_Currencies_Curdescription = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtCountry_Currencies_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCountry_Currencies_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtCountry_Currencies_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtCountry_Currencies_Modified = value ;
      return  ;
   }

   public String getCurcode_Z( )
   {
      return gxTv_SdtCountry_Currencies_Curcode_Z ;
   }

   public void setCurcode_Z( String value )
   {
      gxTv_SdtCountry_Currencies_Curcode_Z = value ;
      return  ;
   }

   public byte getCurplaces_Z( )
   {
      return gxTv_SdtCountry_Currencies_Curplaces_Z ;
   }

   public void setCurplaces_Z( byte value )
   {
      gxTv_SdtCountry_Currencies_Curplaces_Z = value ;
      return  ;
   }

   public String getCurdescription_Z( )
   {
      return gxTv_SdtCountry_Currencies_Curdescription_Z ;
   }

   public void setCurdescription_Z( String value )
   {
      gxTv_SdtCountry_Currencies_Curdescription_Z = value ;
      return  ;
   }

   public byte getCurplaces_N( )
   {
      return gxTv_SdtCountry_Currencies_Curplaces_N ;
   }

   public void setCurplaces_N( byte value )
   {
      gxTv_SdtCountry_Currencies_Curplaces_N = value ;
      return  ;
   }

   public byte getCurdescription_N( )
   {
      return gxTv_SdtCountry_Currencies_Curdescription_N ;
   }

   public void setCurdescription_N( byte value )
   {
      gxTv_SdtCountry_Currencies_Curdescription_N = value ;
      return  ;
   }

   protected byte gxTv_SdtCountry_Currencies_Curplaces ;
   protected byte gxTv_SdtCountry_Currencies_Curplaces_Z ;
   protected byte gxTv_SdtCountry_Currencies_Curplaces_N ;
   protected byte gxTv_SdtCountry_Currencies_Curdescription_N ;
   protected short gxTv_SdtCountry_Currencies_Modified ;
   protected String gxTv_SdtCountry_Currencies_Mode ;
   protected String gxTv_SdtCountry_Currencies_Curcode ;
   protected String gxTv_SdtCountry_Currencies_Curdescription ;
   protected String gxTv_SdtCountry_Currencies_Curcode_Z ;
   protected String gxTv_SdtCountry_Currencies_Curdescription_Z ;
}


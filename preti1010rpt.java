/*
               File: RETi1010rpt
        Description: Relat�rio submiss�es aceitas - i1009/i1010 - 1089
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: June 19, 2020 16:8:52.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.reports.*;

public final  class preti1010rpt extends GXReport
{
   public preti1010rpt( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( preti1010rpt.class ), "" );
   }

   public preti1010rpt( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 ,
                        String[] aP5 ,
                        String[] aP6 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5, aP6);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 ,
                             String[] aP5 ,
                             String[] aP6 )
   {
      preti1010rpt.this.AV18lccbEm = aP0[0];
      this.aP0 = aP0;
      preti1010rpt.this.AV19EmpNom = aP1[0];
      this.aP1 = aP1;
      preti1010rpt.this.AV22Pathpd = aP2[0];
      this.aP2 = aP2;
      preti1010rpt.this.AV23PathTx = aP3[0];
      this.aP3 = aP3;
      preti1010rpt.this.AV10DataB = aP4[0];
      this.aP4 = aP4;
      preti1010rpt.this.AV12DebugM = aP5[0];
      this.aP5 = aP5;
      preti1010rpt.this.AV139Modo = aP6[0];
      this.aP6 = aP6;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      M_top = 0 ;
      M_bot = 1 ;
      P_lines = (int)(66-M_bot) ;
      getPrinter().GxClearAttris() ;
      add_metrics( ) ;
      lineHeight = 11 ;
      PrtOffset = 0 ;
      gxXPage = 96 ;
      gxYPage = 96 ;
      getPrinter().GxSetDocName(AV22Pathpd) ;
      getPrinter().GxSetDocFormat("PDF") ;
      try
      {
         Gx_out = "FIL" ;
         if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 9, 16838, 11906, 0, 1, 1, 0, 1, 1) )
         {
            cleanup();
            return;
         }
         getPrinter().setModal(true) ;
         P_lines = (int)(gxYPage-(lineHeight*1)) ;
         Gx_line = (int)(P_lines+1) ;
         getPrinter().setPageLines(P_lines);
         getPrinter().setLineHeight(lineHeight);
         getPrinter().setM_top(M_top);
         getPrinter().setM_bot(M_bot);
         GXt_char1 = AV128CarFi ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "UTILIZA_LINHA_FIM", "S= utiliza Char(20) / N= NewLine() ", "S", "N", GXv_svchar2) ;
         preti1010rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV128CarFi = GXt_char1 ;
         GXt_char1 = AV148Bande ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER", "C�digo do cart�o Hipercard", "S", "HC", GXv_svchar2) ;
         preti1010rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV148Bande = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV149NovaB ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER_NEW", "C�digo do cart�o Hipercard para arquivos output", "S", "HP", GXv_svchar2) ;
         preti1010rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV149NovaB = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV150Bande ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO", "C�digo do cart�o ELO", "S", "EL", GXv_svchar2) ;
         preti1010rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV150Bande = GXutil.trim( GXt_char1) ;
         GXt_char1 = AV151NovaB ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO_NEW", "C�digo do cart�o ELO para arquivos output", "S", "EL", GXv_svchar2) ;
         preti1010rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV151NovaB = GXutil.trim( GXt_char1) ;
         AV12DebugM = GXutil.trim( GXutil.upper( AV12DebugM)) ;
         AV30NumReg = 0 ;
         if ( ( GXutil.strSearch( AV12DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( "  Company Code: "+AV18lccbEm+" ("+GXutil.trim( AV19EmpNom)+")" );
            context.msgStatus( "  PDF File    : "+AV22Pathpd );
            context.msgStatus( "  Txt File    : "+AV23PathTx );
            context.msgStatus( "  Base Date   : "+AV10DataB );
         }
         if ( ( GXutil.strSearch( AV12DebugM, "TESTMODE", 1) > 0 ) )
         {
            AV84TestMo = (byte)(1) ;
         }
         else
         {
            AV84TestMo = (byte)(0) ;
         }
         /* Using cursor P006I2 */
         pr_default.execute(0, new Object[] {AV18lccbEm});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1488ICSI_ = P006I2_A1488ICSI_[0] ;
            A1487ICSI_ = P006I2_A1487ICSI_[0] ;
            AV87EmpOk = "S" ;
            AV29CriaAr = "S" ;
            AV81ICSI_E = A1487ICSI_ ;
            /* Execute user subroutine: S1149 */
            S1149 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ( GXutil.strSearch( AV12DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( "  Ended at "+GXutil.time( ) );
         }
         /* Print footer for last page */
         ToSkip = (int)(P_lines+1) ;
         h6I0( true, 0) ;
         /* Close printer file */
         getPrinter().GxEndDocument() ;
         endPrinter();
      }
      catch ( ProcessInterruptedException e )
      {
      }
      cleanup();
   }

   public void S1149( ) throws ProcessInterruptedException
   {
      /* 'MAIN' Routine */
      AV95W = (byte)(1) ;
      while ( ( AV95W <= 2 ) )
      {
         AV106TotSa = 0.00 ;
         AV102Total = 0.00 ;
         AV109TotEn = 0.00 ;
         AV107TotTi = 0.00 ;
         if ( ( AV95W == 1 ) )
         {
            AV77ValPar = 0 ;
            AV112Total = 0.00 ;
         }
         else
         {
            AV77ValPar = 1 ;
            AV108TotPa = 0 ;
         }
         AV118DataC = "" ;
         AV38NovLot = "S" ;
         AV127Old_L = "" ;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              new Double(AV77ValPar) ,
                                              new Double(A1170lccbI) ,
                                              AV81ICSI_E ,
                                              A1227lccbO ,
                                              A1184lccbS ,
                                              A1150lccbE },
                                              new int[] {
                                              TypeConstants.DOUBLE, TypeConstants.DOUBLE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         /* Using cursor P006I3 */
         pr_default.execute(1, new Object[] {AV81ICSI_E, AV81ICSI_E});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1227lccbO = P006I3_A1227lccbO[0] ;
            A1184lccbS = P006I3_A1184lccbS[0] ;
            n1184lccbS = P006I3_n1184lccbS[0] ;
            A1150lccbE = P006I3_A1150lccbE[0] ;
            A1228lccbF = P006I3_A1228lccbF[0] ;
            A1226lccbA = P006I3_A1226lccbA[0] ;
            A1225lccbC = P006I3_A1225lccbC[0] ;
            A1224lccbC = P006I3_A1224lccbC[0] ;
            A1223lccbD = P006I3_A1223lccbD[0] ;
            A1222lccbI = P006I3_A1222lccbI[0] ;
            A1490Distr = P006I3_A1490Distr[0] ;
            n1490Distr = P006I3_n1490Distr[0] ;
            A1170lccbI = P006I3_A1170lccbI[0] ;
            n1170lccbI = P006I3_n1170lccbI[0] ;
            A1189lccbS = P006I3_A1189lccbS[0] ;
            n1189lccbS = P006I3_n1189lccbS[0] ;
            A1206LccbR = P006I3_A1206LccbR[0] ;
            n1206LccbR = P006I3_n1206LccbR[0] ;
            A1193lccbS = P006I3_A1193lccbS[0] ;
            n1193lccbS = P006I3_n1193lccbS[0] ;
            A1172lccbS = P006I3_A1172lccbS[0] ;
            n1172lccbS = P006I3_n1172lccbS[0] ;
            A1168lccbI = P006I3_A1168lccbI[0] ;
            n1168lccbI = P006I3_n1168lccbI[0] ;
            A1515lccbC = P006I3_A1515lccbC[0] ;
            n1515lccbC = P006I3_n1515lccbC[0] ;
            A1171lccbT = P006I3_A1171lccbT[0] ;
            n1171lccbT = P006I3_n1171lccbT[0] ;
            A1203lccbR = P006I3_A1203lccbR[0] ;
            n1203lccbR = P006I3_n1203lccbR[0] ;
            A1163lccbP = P006I3_A1163lccbP[0] ;
            n1163lccbP = P006I3_n1163lccbP[0] ;
            A1204lccbR = P006I3_A1204lccbR[0] ;
            n1204lccbR = P006I3_n1204lccbR[0] ;
            A1495lccbC = P006I3_A1495lccbC[0] ;
            n1495lccbC = P006I3_n1495lccbC[0] ;
            A1190lccbS = P006I3_A1190lccbS[0] ;
            n1190lccbS = P006I3_n1190lccbS[0] ;
            A1191lccbS = P006I3_A1191lccbS[0] ;
            n1191lccbS = P006I3_n1191lccbS[0] ;
            A1194lccbS = P006I3_A1194lccbS[0] ;
            n1194lccbS = P006I3_n1194lccbS[0] ;
            AV154SemDa = "N" ;
            AV32Dia = (byte)(GXutil.day( A1189lccbS)) ;
            AV33Mes = (byte)(GXutil.month( A1189lccbS)) ;
            AV34Ano = (short)(GXutil.year( A1189lccbS)) ;
            AV96LccbDa = localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
            if ( ( GXutil.strcmp(AV29CriaAr, "S") == 0 ) )
            {
               AV30NumReg = 0 ;
               AV49LccbTi = (short)(0) ;
               AV50LccbTi = (short)(0) ;
               AV30NumReg = (int)(AV30NumReg+1) ;
               AV73ValTot = 0 ;
               AV74ValTot = 0 ;
               AV70ValTot = 0 ;
               AV72ValTot = 0 ;
               AV71ValTot = 0 ;
               AV69ValTot = 0 ;
               AV97ArqNum = 0 ;
               /* Execute user subroutine: S124 */
               S124 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  getPrinter().GxEndPage() ;
                  /* Close printer file */
                  getPrinter().GxEndDocument() ;
                  endPrinter();
                  returnInSub = true;
                  if (true) return;
               }
               AV40Txt = "HC" ;
               AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
               AV40Txt = AV40Txt + GXutil.space( (short)(8)) ;
               AV40Txt = AV40Txt + AV10DataB ;
               AV40Txt = AV40Txt + AV39HoraB ;
               AV40Txt = AV40Txt + "000" ;
               AV41Num03P = (short)(GXutil.val( A1150lccbE, ".")) ;
               AV40Txt = AV40Txt + localUtil.format( AV41Num03P, "999") ;
               AV40Txt = AV40Txt + "00" ;
               AV40Txt = AV40Txt + GXutil.space( (short)(11)) ;
               AV40Txt = AV40Txt + localUtil.format( AV97ArqNum, "99999") ;
               AV40Txt = AV40Txt + GXutil.space( (short)(181)) ;
               if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
               {
                  AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(235)) ;
                  AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
               }
               else
               {
                  AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
                  AV40Txt = AV40Txt + GXutil.newLine( ) ;
                  AV129xmlWr.writeRawText(AV40Txt);
               }
               AV29CriaAr = "N" ;
               AV38NovLot = "S" ;
            }
            AV32Dia = (byte)(GXutil.day( A1206LccbR)) ;
            AV33Mes = (byte)(GXutil.month( A1206LccbR)) ;
            AV34Ano = (short)(GXutil.year( A1206LccbR)) ;
            AV53LccbCr = localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
            if ( ( GXutil.strcmp(A1224lccbC, "DC") == 0 ) || ( GXutil.strcmp(A1224lccbC, "CA") == 0 ) )
            {
               AV122LccbC = "CC" ;
            }
            else
            {
               if ( ( GXutil.strcmp(A1224lccbC, AV148Bande) == 0 ) )
               {
                  AV122LccbC = AV149NovaB ;
               }
               else if ( ( GXutil.strcmp(A1224lccbC, AV150Bande) == 0 ) )
               {
                  AV122LccbC = AV151NovaB ;
               }
               else
               {
                  AV122LccbC = A1224lccbC ;
               }
            }
            if ( ( GXutil.strcmp(A1224lccbC, AV148Bande) == 0 ) )
            {
               AV122LccbC = AV149NovaB ;
            }
            else if ( ( GXutil.strcmp(A1224lccbC, AV150Bande) == 0 ) )
            {
               AV122LccbC = AV151NovaB ;
            }
            else
            {
               AV122LccbC = A1224lccbC ;
            }
            AV83LccbRC = A1206LccbR ;
            if ( ( GXutil.strcmp(AV127Old_L, A1194lccbS) != 0 ) )
            {
               AV38NovLot = "S" ;
               if ( ( GXutil.strcmp(AV127Old_L, "") != 0 ) )
               {
                  if ( ( AV30NumReg > 0 ) )
                  {
                     /* Execute user subroutine: S134 */
                     S134 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        getPrinter().GxEndPage() ;
                        /* Close printer file */
                        getPrinter().GxEndDocument() ;
                        endPrinter();
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  h6I0( false, 26) ;
                  getPrinter().GxAttris("MS Serif", 7, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Totais", 0, Gx_line+5, 30, Gx_line+16, 0+256) ;
                  getPrinter().GxAttris("MS Serif", 7, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV106TotSa, "ZZ,ZZZ,ZZZ,ZZ9.99")), 476, Gx_line+5, 560, Gx_line+16, 2) ;
                  getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV107TotTi, "ZZ,ZZZ,ZZZ,ZZ9.99")), 565, Gx_line+5, 639, Gx_line+16, 2) ;
                  getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV109TotEn, "ZZ,ZZZ,ZZZ,ZZ9.99")), 706, Gx_line+5, 785, Gx_line+16, 2) ;
                  Gx_OldLine = Gx_line ;
                  Gx_line = (int)(Gx_line+26) ;
                  AV106TotSa = 0.00 ;
                  AV107TotTi = 0.00 ;
                  AV108TotPa = 0.00 ;
                  AV109TotEn = 0.00 ;
                  AV144Total = 0 ;
               }
            }
            AV127Old_L = A1194lccbS ;
            if ( ( GXutil.strcmp(AV38NovLot, "S") == 0 ) )
            {
               AV57NumReg = 0 ;
               AV67ValTot = 0 ;
               AV68ValTot = 0 ;
               AV63ValTot = 0 ;
               AV65ValTot = 0 ;
               AV64ValTot = 0 ;
               AV62ValTot = 0 ;
               if ( ( GXutil.strcmp(GXutil.trim( A1194lccbS), "") != 0 ) )
               {
                  AV43NumLot = (int)(GXutil.val( A1194lccbS, ".")) ;
               }
               else
               {
                  if ( ( GXutil.strcmp(AV82LccbCC, "AX") == 0 ) )
                  {
                  }
                  else if ( ( GXutil.strcmp(AV82LccbCC, "RC") == 0 ) )
                  {
                     AV43NumLot = (int)(GXutil.val( GXutil.substring( A1193lccbS, 6, 5), ".")) ;
                  }
                  else if ( ( GXutil.strcmp(AV82LccbCC, "DC") == 0 ) )
                  {
                     AV43NumLot = (int)(GXutil.val( GXutil.substring( A1193lccbS, 6, 5), ".")) ;
                  }
                  else if ( ( GXutil.strcmp(AV82LccbCC, "VI") == 0 ) )
                  {
                  }
                  else if ( ( GXutil.strcmp(AV82LccbCC, "HC") == 0 ) )
                  {
                  }
               }
               AV50LccbTi = AV95W ;
               AV40Txt = "HL" ;
               AV30NumReg = (int)(AV30NumReg+1) ;
               AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
               AV40Txt = AV40Txt + localUtil.format( AV43NumLot, "99999999") ;
               AV40Txt = AV40Txt + AV10DataB ;
               AV40Txt = AV40Txt + AV39HoraB ;
               AV40Txt = AV40Txt + localUtil.format( AV50LccbTi, "999") ;
               AV41Num03P = (short)(GXutil.val( AV81ICSI_E, ".")) ;
               AV40Txt = AV40Txt + localUtil.format( AV41Num03P, "999") ;
               AV40Txt = AV40Txt + AV122LccbC ;
               AV40Txt = AV40Txt + GXutil.space( (short)(11)) ;
               AV40Txt = AV40Txt + localUtil.format( AV97ArqNum, "99999") ;
               AV40Txt = AV40Txt + GXutil.space( (short)(165)) ;
               AV40Txt = AV40Txt + AV96LccbDa ;
               AV40Txt = AV40Txt + AV53LccbCr ;
               if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
               {
                  AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(235)) ;
                  AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
               }
               else
               {
                  AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
                  AV40Txt = AV40Txt + GXutil.newLine( ) ;
                  AV129xmlWr.writeRawText(AV40Txt);
               }
               AV38NovLot = "N" ;
               AV100Gravo = "N" ;
               AV99Gravou = "S" ;
            }
            AV66QtdTot = 0 ;
            AV88LccbDa = A1223lccbD ;
            if ( ( GXutil.strcmp(A1224lccbC, "DC") == 0 ) || ( GXutil.strcmp(A1224lccbC, "CA") == 0 ) )
            {
               AV122LccbC = "CC" ;
            }
            else
            {
               if ( ( GXutil.strcmp(A1224lccbC, AV148Bande) == 0 ) )
               {
                  AV122LccbC = AV149NovaB ;
               }
               else if ( ( GXutil.strcmp(A1224lccbC, AV150Bande) == 0 ) )
               {
                  AV122LccbC = AV151NovaB ;
               }
               else
               {
                  AV122LccbC = A1224lccbC ;
               }
            }
            if ( ( GXutil.strcmp(A1224lccbC, AV148Bande) == 0 ) )
            {
               AV82LccbCC = AV149NovaB ;
            }
            else if ( ( GXutil.strcmp(A1224lccbC, AV150Bande) == 0 ) )
            {
               AV82LccbCC = AV151NovaB ;
            }
            else
            {
               AV82LccbCC = A1224lccbC ;
            }
            AV140Atrib = GXutil.trim( A1225lccbC) ;
            GXv_svchar2[0] = AV142Resul ;
            new pcrypto(remoteHandle, context).execute( AV140Atrib, "D", GXv_svchar2) ;
            preti1010rpt.this.AV142Resul = GXv_svchar2[0] ;
            GXt_char5 = AV89LccbCC ;
            GXv_svchar2[0] = AV142Resul ;
            GXv_char6[0] = GXt_char5 ;
            new pmask(remoteHandle, context).execute( GXv_svchar2, GXv_char6) ;
            preti1010rpt.this.AV142Resul = GXv_svchar2[0] ;
            preti1010rpt.this.GXt_char5 = GXv_char6[0] ;
            AV89LccbCC = GXt_char5 ;
            AV119Num17 = (long)(GXutil.val( AV89LccbCC, ".")) ;
            AV90LccbAp = A1226lccbA ;
            AV91LccbOp = A1227lccbO ;
            AV92LccbFP = A1228lccbF ;
            AV52Lccbia = A1222lccbI ;
            AV42Num08p = (int)(GXutil.val( A1222lccbI, ".")) ;
            AV93LccbIa = localUtil.format( AV42Num08p, "99999999") ;
            AV120LccbS = (double)((A1172lccbS)) ;
            AV106TotSa = (double)(AV106TotSa+((A1172lccbS))) ;
            AV116LccbI = (double)((A1170lccbI*A1168lccbI)) ;
            AV108TotPa = (double)(AV108TotPa+AV116LccbI) ;
            AV102Total = (double)(AV102Total+((A1170lccbI*A1168lccbI))) ;
            AV109TotEn = (double)(AV109TotEn+A1515lccbC) ;
            AV107TotTi = (double)(AV107TotTi+A1171lccbT) ;
            AV131CV = GXutil.substring( A1193lccbS, 10, 7) ;
            AV111Total = (double)(AV111Total+((A1172lccbS))) ;
            AV110TotRe = (int)(AV110TotRe+1) ;
            AV113Total = (double)(AV113Total+A1171lccbT) ;
            if ( ( A1170lccbI <= 0.00 ) )
            {
               AV112Total = (double)(AV112Total+((A1172lccbS))) ;
            }
            else
            {
               AV114Total = (double)(AV114Total+A1515lccbC) ;
               AV115Total = (double)(AV115Total+((A1170lccbI*A1168lccbI))) ;
            }
            AV30NumReg = (int)(AV30NumReg+1) ;
            AV57NumReg = (int)(AV57NumReg+1) ;
            AV144Total = (int)(AV144Total+1) ;
            AV145Total = (int)(AV145Total+1) ;
            AV40Txt = "DE" ;
            AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
            AV40Txt = AV40Txt + localUtil.format( AV43NumLot, "99999999") ;
            AV40Txt = AV40Txt + AV93LccbIa ;
            AV56ContaT = (short)(0) ;
            AV51Flgtkt = "S" ;
            AV60NumCCC = A1203lccbR ;
            GX_I = 1 ;
            while ( ( GX_I <= 99 ) )
            {
               AV47lccbTD[GX_I-1] = GXutil.space( (short)(1)) ;
               GX_I = (int)(GX_I+1) ;
            }
            GX_I = 1 ;
            while ( ( GX_I <= 99 ) )
            {
               AV44DbIata[GX_I-1] = 0 ;
               GX_I = (int)(GX_I+1) ;
            }
            GX_I = 1 ;
            while ( ( GX_I <= 99 ) )
            {
               AV45DbCCcf[GX_I-1] = 0 ;
               GX_I = (int)(GX_I+1) ;
            }
            GX_I = 1 ;
            while ( ( GX_I <= 99 ) )
            {
               AV132Ticke[GX_I-1] = 0 ;
               GX_I = (int)(GX_I+1) ;
            }
            GX_I = 1 ;
            while ( ( GX_I <= 99 ) )
            {
               AV46DbPass[GX_I-1] = GXutil.space( (short)(1)) ;
               GX_I = (int)(GX_I+1) ;
            }
            AV56ContaT = (short)(0) ;
            AV51Flgtkt = "S" ;
            /* Using cursor P006I4 */
            pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1231lccbT = P006I4_A1231lccbT[0] ;
               A1207lccbP = P006I4_A1207lccbP[0] ;
               n1207lccbP = P006I4_n1207lccbP[0] ;
               A1232lccbT = P006I4_A1232lccbT[0] ;
               AV56ContaT = (short)(AV56ContaT+1) ;
               AV47lccbTD[AV56ContaT-1] = A1231lccbT ;
               AV44DbIata[AV56ContaT-1] = (int)(GXutil.val( AV93LccbIa, ".")) ;
               if ( ( GXutil.strcmp(A1490Distr, "CMC") == 0 ) || ( GXutil.strcmp(A1490Distr, "CVI") == 0 ) )
               {
                  AV130lccbT = "0000" + AV131CV ;
                  AV117LccbC = GXutil.trim( AV130lccbT) ;
                  AV45DbCCcf[AV56ContaT-1] = (long)(GXutil.val( AV117LccbC, ".")) ;
                  GXt_char5 = AV134TKT_V ;
                  GXv_char6[0] = GXt_char5 ;
                  new pdigitoverificador(remoteHandle, context).execute( A1231lccbT, GXv_char6) ;
                  preti1010rpt.this.GXt_char5 = GXv_char6[0] ;
                  AV134TKT_V = GXutil.trim( A1231lccbT) + GXt_char5 ;
                  AV132Ticke[AV56ContaT-1] = (long)(GXutil.val( AV134TKT_V, ".")) ;
               }
               else
               {
                  GXt_char5 = AV117LccbC ;
                  GXv_char6[0] = GXt_char5 ;
                  new pdigitoverificador(remoteHandle, context).execute( A1231lccbT, GXv_char6) ;
                  preti1010rpt.this.GXt_char5 = GXv_char6[0] ;
                  AV117LccbC = GXutil.trim( A1231lccbT) + GXt_char5 ;
                  AV45DbCCcf[AV56ContaT-1] = (long)(GXutil.val( AV117LccbC, ".")) ;
               }
               AV46DbPass[AV56ContaT-1] = GXutil.substring( A1207lccbP, 1, 40) ;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            AV48Num11P = AV45DbCCcf[1-1] ;
            AV133TKT_V = AV132Ticke[1-1] ;
            AV85TxtTmp = GXutil.trim( GXutil.substring( A1226lccbA, 1, 6)) ;
            /* Execute user subroutine: S144 */
            S144 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               if (true) return;
            }
            if ( ( GXutil.strcmp(AV122LccbC, "AX") == 0 ) )
            {
               AV40Txt = AV40Txt + GXutil.padl( GXutil.trim( AV85TxtTmp), (short)(11), "0") ;
               AV146CCCFD = GXutil.padl( GXutil.trim( AV85TxtTmp), (short)(11), "0") ;
            }
            else if ( ( GXutil.strcmp(AV82LccbCC, "DC") == 0 ) || ( GXutil.strcmp(AV82LccbCC, "CA") == 0 ) )
            {
               AV40Txt = AV40Txt + "00000000000" ;
               AV146CCCFD = "00000000000" ;
            }
            else if ( ( GXutil.strcmp(AV122LccbC, "VI") == 0 ) )
            {
               AV40Txt = AV40Txt + localUtil.format( AV48Num11P, "99999999999") ;
               AV146CCCFD = localUtil.format( AV48Num11P, "99999999999") ;
            }
            else
            {
               AV40Txt = AV40Txt + "00000000000" ;
               AV146CCCFD = "00000000000" ;
            }
            AV40Txt = AV40Txt + localUtil.format( AV50LccbTi, "999") ;
            AV40Txt = AV40Txt + GXutil.padl( GXutil.trim( AV89LccbCC), (short)(17), "0") ;
            AV40Txt = AV40Txt + GXutil.padr( GXutil.trim( A1226lccbA), (short)(6), " ") ;
            AV90LccbAp = GXutil.padr( GXutil.trim( A1226lccbA), (short)(6), " ") ;
            AV55Num15P = A1172lccbS ;
            AV147Num15 = GXutil.strReplace( GXutil.strReplace( GXutil.str( A1172lccbS, 14, 2), ".", ""), ",", "") ;
            AV40Txt = AV40Txt + GXutil.padl( AV147Num15, (short)(15), "0") ;
            AV75ValVen = AV55Num15P ;
            AV78ValAce = AV55Num15P ;
            AV55Num15P = A1515lccbC ;
            AV147Num15 = GXutil.strReplace( GXutil.strReplace( GXutil.str( A1515lccbC, 14, 2), ".", ""), ",", "") ;
            AV40Txt = AV40Txt + GXutil.padl( AV147Num15, (short)(15), "0") ;
            AV76ValEnt = AV55Num15P ;
            AV55Num15P = A1171lccbT ;
            AV147Num15 = GXutil.strReplace( GXutil.strReplace( GXutil.str( A1171lccbT, 14, 2), ".", ""), ",", "") ;
            AV40Txt = AV40Txt + GXutil.padl( AV147Num15, (short)(15), "0") ;
            AV80ValTax = AV55Num15P ;
            AV55Num15P = A1170lccbI ;
            AV147Num15 = GXutil.strReplace( GXutil.strReplace( GXutil.str( A1170lccbI, 14, 2), ".", ""), ",", "") ;
            AV40Txt = AV40Txt + GXutil.padl( AV147Num15, (short)(15), "0") ;
            AV77ValPar = AV55Num15P ;
            AV79ValDes = 0 ;
            AV31Num02P = (byte)(A1168lccbI) ;
            AV40Txt = AV40Txt + localUtil.format( AV31Num02P, "99") ;
            AV32Dia = (byte)(GXutil.day( A1223lccbD)) ;
            AV33Mes = (byte)(GXutil.month( A1223lccbD)) ;
            AV34Ano = (short)(GXutil.year( A1223lccbD)) ;
            AV40Txt = AV40Txt + localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
            AV40Txt = AV40Txt + localUtil.format( AV57NumReg, "999999") ;
            AV40Txt = AV40Txt + "D" ;
            AV40Txt = AV40Txt + GXutil.padl( GXutil.trim( AV47lccbTD[1-1]), (short)(11), "0") ;
            if ( ( GXutil.len( GXutil.trim( A1163lccbP)) > 5 ) )
            {
               AV40Txt = AV40Txt + GXutil.right( GXutil.trim( A1163lccbP), 5) ;
            }
            else
            {
               AV40Txt = AV40Txt + GXutil.padr( GXutil.trim( A1163lccbP), (short)(5), " ") ;
            }
            AV40Txt = AV40Txt + "BRL2" ;
            AV40Txt = AV40Txt + GXutil.substring( AV46DbPass[1-1], 1, 40) ;
            if ( ( GXutil.strcmp(GXutil.trim( A1204lccbR), "") != 0 ) )
            {
               AV105LccbS = A1204lccbR ;
            }
            else
            {
               AV105LccbS = A1194lccbS ;
            }
            AV42Num08p = (int)(GXutil.val( AV105LccbS, ".")) ;
            AV40Txt = AV40Txt + localUtil.format( AV42Num08p, "99999999") ;
            AV40Txt = AV40Txt + GXutil.space( (short)(3)) ;
            AV32Dia = (byte)(GXutil.day( A1495lccbC)) ;
            AV33Mes = (byte)(GXutil.month( A1495lccbC)) ;
            AV34Ano = (short)(GXutil.year( A1495lccbC)) ;
            AV40Txt = AV40Txt + localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
            AV32Dia = (byte)(GXutil.day( A1189lccbS)) ;
            AV33Mes = (byte)(GXutil.month( A1189lccbS)) ;
            AV34Ano = (short)(GXutil.year( A1189lccbS)) ;
            AV40Txt = AV40Txt + localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
            AV32Dia = (byte)(GXutil.day( A1206LccbR)) ;
            AV33Mes = (byte)(GXutil.month( A1206LccbR)) ;
            AV34Ano = (short)(GXutil.year( A1206LccbR)) ;
            AV61CreDat = localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
            AV40Txt = AV40Txt + AV61CreDat ;
            if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "C") == 0 ) )
            {
               AV40Txt = AV40Txt + "01" ;
               AV143Proce = "Cielo" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "R") == 0 ) )
            {
               AV40Txt = AV40Txt + "02" ;
               AV143Proce = "Rede" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "A") == 0 ) )
            {
               AV40Txt = AV40Txt + "03" ;
               AV143Proce = "Amex" ;
            }
            else
            {
               AV40Txt = AV40Txt + "00" ;
               AV143Proce = "" ;
            }
            if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
            {
               AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(234)) ;
               AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
            }
            else
            {
               AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
               AV40Txt = AV40Txt + GXutil.newLine( ) ;
               AV129xmlWr.writeRawText(AV40Txt);
            }
            /*
               INSERT RECORD ON TABLE LCCBPLP1

            */
            A1229lccbS = GXutil.now(true, false) ;
            A1186lccbS = "OKI1010" ;
            n1186lccbS = false ;
            A1187lccbS = "VENDA ACEITA" ;
            n1187lccbS = false ;
            /* Using cursor P006I5 */
            pr_default.execute(3, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
            if ( (pr_default.getStatus(3) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
            /* Execute user subroutine: S154 */
            S154 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               if (true) return;
            }
            h6I0( false, 14) ;
            getPrinter().GxAttris("MS Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( A1171lccbT, "ZZ,ZZZ,ZZZ,ZZ9.99")), 566, Gx_line+0, 639, Gx_line+11, 2) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1224lccbC, "XX")), 0, Gx_line+0, 13, Gx_line+11, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV105LccbS, "XXXXXXXXXX")), 91, Gx_line+0, 140, Gx_line+11, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV89LccbCC, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 195, Gx_line+0, 414, Gx_line+11, 0+256) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV120LccbS, "ZZ,ZZZ,ZZZ,ZZ9.99")), 487, Gx_line+0, 560, Gx_line+11, 2) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( A1168lccbI, "ZZZ9")), 568, Gx_line+0, 587, Gx_line+11, 0+256) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( A1170lccbI, "ZZ,ZZZ,ZZZ,ZZ9.99")), 630, Gx_line+0, 703, Gx_line+11, 2) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( A1515lccbC, "ZZ,ZZZ,ZZZ,ZZ9.99")), 712, Gx_line+0, 785, Gx_line+11, 2) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV45DbCCcf[1-1], "99999999999")), 304, Gx_line+0, 358, Gx_line+11, 2+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV93LccbIa, "XXXXXXXX")), 139, Gx_line+0, 193, Gx_line+11, 0) ;
            getPrinter().GxDrawText(localUtil.format( AV88LccbDa, "99/99/99"), 362, Gx_line+0, 408, Gx_line+11, 0) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV143Proce, "XXXXXXXXXXXXXXX")), 20, Gx_line+0, 94, Gx_line+11, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV90LccbAp, "XXXXXXXXXXXXXXXXXXXX")), 419, Gx_line+0, 479, Gx_line+11, 1) ;
            Gx_OldLine = Gx_line ;
            Gx_line = (int)(Gx_line+14) ;
            AV121y = (short)(2) ;
            AV121y = (short)(2) ;
            while ( ( AV121y <= AV56ContaT ) )
            {
               if ( ( AV45DbCCcf[AV121y-1] != 0 ) )
               {
                  h6I0( false, 15) ;
                  getPrinter().GxAttris("MS Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV105LccbS, "XXXXXXXXXX")), 91, Gx_line+0, 140, Gx_line+11, 0+256) ;
                  getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1224lccbC, "XX")), 0, Gx_line+0, 13, Gx_line+11, 0+256) ;
                  getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV89LccbCC, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 195, Gx_line+0, 414, Gx_line+11, 0+256) ;
                  getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV45DbCCcf[AV121y-1], "99999999999")), 304, Gx_line+0, 358, Gx_line+11, 2+256) ;
                  getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV93LccbIa, "XXXXXXXX")), 139, Gx_line+0, 193, Gx_line+11, 0) ;
                  getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV143Proce, "XXXXXXXXXXXXXXX")), 20, Gx_line+0, 94, Gx_line+11, 0+256) ;
                  Gx_OldLine = Gx_line ;
                  Gx_line = (int)(Gx_line+15) ;
               }
               AV121y = (short)(AV121y+1) ;
            }
            AV67ValTot = (double)(AV67ValTot+AV78ValAce) ;
            AV68ValTot = (double)(AV68ValTot+AV79ValDes) ;
            AV63ValTot = (double)(AV63ValTot+AV76ValEnt) ;
            AV65ValTot = (double)(AV65ValTot+AV77ValPar) ;
            AV64ValTot = (double)(AV64ValTot+AV80ValTax) ;
            AV62ValTot = (double)(AV62ValTot+AV75ValVen) ;
            AV66QtdTot = (int)(AV66QtdTot+1) ;
            AV73ValTot = (double)(AV73ValTot+AV78ValAce) ;
            AV74ValTot = (double)(AV74ValTot+AV79ValDes) ;
            AV70ValTot = (double)(AV70ValTot+AV76ValEnt) ;
            AV72ValTot = (double)(AV72ValTot+AV77ValPar) ;
            AV71ValTot = (double)(AV71ValTot+AV80ValTax) ;
            AV69ValTot = (double)(AV69ValTot+AV75ValVen) ;
            A1184lccbS = "OKI1010" ;
            n1184lccbS = false ;
            A1190lccbS = GXutil.now(true, false) ;
            n1190lccbS = false ;
            A1191lccbS = "F" ;
            n1191lccbS = false ;
            AV100Gravo = "N" ;
            AV99Gravou = "N" ;
            /* Using cursor P006I6 */
            pr_default.execute(4, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1190lccbS), A1190lccbS, new Boolean(n1191lccbS), A1191lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( ( AV30NumReg > 0 ) )
         {
            /* Execute user subroutine: S134 */
            S134 ();
            if ( returnInSub )
            {
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               if (true) return;
            }
         }
         h6I0( false, 26) ;
         getPrinter().GxAttris("MS Serif", 7, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Totais", 0, Gx_line+5, 30, Gx_line+16, 0+256) ;
         getPrinter().GxAttris("MS Serif", 7, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV106TotSa, "ZZ,ZZZ,ZZZ,ZZ9.99")), 476, Gx_line+5, 560, Gx_line+16, 2) ;
         getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV107TotTi, "ZZ,ZZZ,ZZZ,ZZ9.99")), 565, Gx_line+5, 639, Gx_line+16, 2) ;
         getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV109TotEn, "ZZ,ZZZ,ZZZ,ZZ9.99")), 706, Gx_line+5, 785, Gx_line+16, 2) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+26) ;
         AV106TotSa = 0.00 ;
         AV107TotTi = 0.00 ;
         AV108TotPa = 0.00 ;
         AV109TotEn = 0.00 ;
         AV95W = (byte)(AV95W+1) ;
      }
      if ( ( AV30NumReg > 0 ) )
      {
         /* Execute user subroutine: S161 */
         S161 ();
         if ( returnInSub )
         {
            getPrinter().GxEndPage() ;
            /* Close printer file */
            getPrinter().GxEndDocument() ;
            endPrinter();
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         AV32Dia = (byte)(GXutil.day( Gx_date)) ;
         AV33Mes = (byte)(GXutil.month( Gx_date)) ;
         AV34Ano = (short)(GXutil.year( Gx_date)) ;
         AV96LccbDa = localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
         if ( ( GXutil.strcmp(AV29CriaAr, "S") == 0 ) )
         {
            AV30NumReg = 1 ;
            AV97ArqNum = 0 ;
            /* Execute user subroutine: S124 */
            S124 ();
            if ( returnInSub )
            {
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               if (true) return;
            }
            AV40Txt = "HC" ;
            AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
            AV40Txt = AV40Txt + GXutil.space( (short)(8)) ;
            AV40Txt = AV40Txt + AV10DataB ;
            AV40Txt = AV40Txt + AV39HoraB ;
            AV40Txt = AV40Txt + "000" ;
            AV41Num03P = (short)(957) ;
            AV40Txt = AV40Txt + localUtil.format( AV41Num03P, "999") ;
            AV40Txt = AV40Txt + "00" ;
            AV40Txt = AV40Txt + GXutil.space( (short)(11)) ;
            AV40Txt = AV40Txt + localUtil.format( AV97ArqNum, "99999") ;
            AV40Txt = AV40Txt + GXutil.space( (short)(181)) ;
            AV154SemDa = "Y" ;
            if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
            {
               AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(235)) ;
               AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
            }
            else
            {
               AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
               AV40Txt = AV40Txt + GXutil.newLine( ) ;
               AV129xmlWr.writeRawText(AV40Txt);
            }
            AV29CriaAr = "N" ;
            AV38NovLot = "S" ;
         }
         /* Execute user subroutine: S161 */
         S161 ();
         if ( returnInSub )
         {
            getPrinter().GxEndPage() ;
            /* Close printer file */
            getPrinter().GxEndDocument() ;
            endPrinter();
            returnInSub = true;
            if (true) return;
         }
      }
      h6I0( false, 40) ;
      getPrinter().GxAttris("MS Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
      getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV110TotRe, "ZZZZZ9")), 142, Gx_line+6, 171, Gx_line+17, 2+256) ;
      getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV111Total, "ZZ,ZZZ,ZZZ,ZZ9.99")), 317, Gx_line+6, 401, Gx_line+17, 2+256) ;
      getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV112Total, "ZZ,ZZZ,ZZZ,ZZ9.99")), 317, Gx_line+23, 401, Gx_line+34, 2+256) ;
      getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV113Total, "ZZ,ZZZ,ZZZ,ZZ9.99")), 518, Gx_line+23, 602, Gx_line+34, 2+256) ;
      getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( AV114Total, "ZZ,ZZZ,ZZZ,ZZ9.99")), 518, Gx_line+6, 602, Gx_line+17, 2+256) ;
      getPrinter().GxAttris("MS Serif", 7, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
      getPrinter().GxDrawText("Total Vendas Processadas:", 0, Gx_line+6, 133, Gx_line+17, 0+256) ;
      getPrinter().GxDrawText("Total vendas Processado:", 181, Gx_line+6, 307, Gx_line+17, 0+256) ;
      getPrinter().GxDrawText("Valor total a vista:", 215, Gx_line+23, 307, Gx_line+34, 0+256) ;
      getPrinter().GxDrawText("Valor Total Taxas:", 418, Gx_line+23, 508, Gx_line+34, 0+256) ;
      getPrinter().GxDrawText("Valor total entrada:", 411, Gx_line+6, 508, Gx_line+17, 0+256) ;
      Gx_OldLine = Gx_line ;
      Gx_line = (int)(Gx_line+40) ;
      if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
      {
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      }
      else
      {
         AV129xmlWr.close();
      }
      GXt_char5 = AV152Proce ;
      GXv_char6[0] = GXt_char5 ;
      new pr2getparm(remoteHandle, context).execute( "PROCESSA_SCHECK", "Verifica se processa o sanity check", "S", "N", GXv_char6) ;
      preti1010rpt.this.GXt_char5 = GXv_char6[0] ;
      AV152Proce = GXt_char5 ;
      if ( ( GXutil.strcmp(AV152Proce, "Y") == 0 ) )
      {
         context.msgStatus( "Sanity Check " );
         GXv_char6[0] = AV153msgEr ;
         new psanitychecki1010(remoteHandle, context).execute( AV23PathTx, AV18lccbEm, GXv_char6) ;
         preti1010rpt.this.AV153msgEr = GXv_char6[0] ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( AV153msgEr))==0)) )
         {
            if ( ( GXutil.strSearch( AV12DebugM, "NOMOVE", 1) == 0 ) )
            {
               AV24Comman = "cmd /c move " + AV23PathTx + " " + AV21Path1 ;
            }
         }
         else
         {
            context.msgStatus( AV153msgEr );
         }
      }
   }

   public void S124( ) throws ProcessInterruptedException
   {
      /* 'ARQNOVO' Routine */
      if ( ( GXutil.strSearch( AV12DebugM, "SEPPIPE", 1) > 0 ) )
      {
         AV36Sep = "|" ;
      }
      else
      {
         AV36Sep = "" ;
      }
      GXt_char5 = AV20Path ;
      GXv_char6[0] = GXt_char5 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_1089", "Caminho arquivo R1089 Aceitos", "F", "C:\\Temp\\ICSI\\R1089", GXv_char6) ;
      preti1010rpt.this.GXt_char5 = GXv_char6[0] ;
      AV20Path = GXt_char5 ;
      GXt_char5 = AV21Path1 ;
      GXv_char6[0] = GXt_char5 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_1089_BSPLink", "Caminho arquivo R1089 Aceitos - BSPLink", "F", "C:\\Temp\\ICSI\\R1089\\BSPLink", GXv_char6) ;
      preti1010rpt.this.GXt_char5 = GXv_char6[0] ;
      AV21Path1 = GXt_char5 ;
      AV32Dia = (byte)(GXutil.day( GXutil.today( ))) ;
      AV33Mes = (byte)(GXutil.month( GXutil.today( ))) ;
      AV34Ano = (short)(GXutil.year( GXutil.today( ))) ;
      AV10DataB = localUtil.format( AV34Ano, "9999") + localUtil.format( AV33Mes, "99") + localUtil.format( AV32Dia, "99") ;
      AV39HoraB = GXutil.substring( Gx_time, 1, 2) + GXutil.substring( Gx_time, 4, 2) + GXutil.substring( Gx_time, 7, 2) ;
      AV163GXLvl = (byte)(0) ;
      /* Using cursor P006I7 */
      pr_default.execute(5, new Object[] {AV18lccbEm, AV18lccbEm});
      while ( (pr_default.getStatus(5) != 101) )
      {
         A1488ICSI_ = P006I7_A1488ICSI_[0] ;
         A1487ICSI_ = P006I7_A1487ICSI_[0] ;
         A1479ICSI_ = P006I7_A1479ICSI_[0] ;
         n1479ICSI_ = P006I7_n1479ICSI_[0] ;
         AV163GXLvl = (byte)(1) ;
         A1479ICSI_ = (int)(A1479ICSI_+1) ;
         n1479ICSI_ = false ;
         if ( ( A1479ICSI_ > 99999 ) )
         {
            A1479ICSI_ = 1 ;
            n1479ICSI_ = false ;
         }
         AV97ArqNum = A1479ICSI_ ;
         /* Using cursor P006I8 */
         pr_default.execute(6, new Object[] {new Boolean(n1479ICSI_), new Integer(A1479ICSI_), A1487ICSI_, A1488ICSI_});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(5);
      if ( ( AV163GXLvl == 0 ) )
      {
      }
      if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
      {
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwopen( AV23PathTx, AV36Sep, "", (byte)(0), "") ;
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      }
      else
      {
         AV129xmlWr.openURL(AV23PathTx);
         AV129xmlWr.close();
      }
      if ( ( GXutil.fileExists( AV23PathTx) == 0 ) )
      {
      }
      else
      {
         if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
         {
            AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwopen( AV23PathTx, AV36Sep, "", (byte)(0), "") ;
         }
         else
         {
            AV129xmlWr.openURL(AV23PathTx);
         }
      }
   }

   public void S154( ) throws ProcessInterruptedException
   {
      /* 'BILHETES' Routine */
      AV59i = (short)(2) ;
      while ( ( AV59i <= AV56ContaT ) )
      {
         AV40Txt = "DB" ;
         AV30NumReg = (int)(AV30NumReg+1) ;
         AV57NumReg = (int)(AV57NumReg+1) ;
         AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
         AV40Txt = AV40Txt + localUtil.format( AV43NumLot, "99999999") ;
         AV42Num08p = AV44DbIata[AV59i-1] ;
         AV40Txt = AV40Txt + localUtil.format( AV42Num08p, "99999999") ;
         AV40Txt = AV40Txt + GXutil.padl( GXutil.trim( AV146CCCFD), (short)(11), "0") ;
         AV40Txt = AV40Txt + GXutil.space( (short)(96)) ;
         AV40Txt = AV40Txt + localUtil.format( AV57NumReg, "999999") ;
         AV40Txt = AV40Txt + GXutil.space( (short)(1)) ;
         AV117LccbC = GXutil.padl( GXutil.trim( AV47lccbTD[AV59i-1]), (short)(11), "0") ;
         AV40Txt = AV40Txt + AV117LccbC ;
         AV40Txt = AV40Txt + GXutil.space( (short)(76)) ;
         AV40Txt = AV40Txt + AV61CreDat ;
         if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "C") == 0 ) )
         {
            AV40Txt = AV40Txt + "01" ;
            AV143Proce = "Cielo" ;
         }
         else if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "R") == 0 ) )
         {
            AV40Txt = AV40Txt + "02" ;
            AV143Proce = "Rede" ;
         }
         else if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "A") == 0 ) )
         {
            AV40Txt = AV40Txt + "03" ;
            AV143Proce = "Amex" ;
         }
         else
         {
            AV40Txt = AV40Txt + "00" ;
            AV143Proce = "" ;
         }
         if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
         {
            AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(234)) ;
            AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         }
         else
         {
            AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
            AV40Txt = AV40Txt + GXutil.newLine( ) ;
            AV129xmlWr.writeRawText(AV40Txt);
         }
         AV59i = (short)(AV59i+1) ;
      }
   }

   public void S134( ) throws ProcessInterruptedException
   {
      /* 'GRAVATL' Routine */
      AV30NumReg = (int)(AV30NumReg+1) ;
      AV126Num5P = AV144Total ;
      AV40Txt = "TL" ;
      AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
      AV40Txt = AV40Txt + localUtil.format( AV43NumLot, "99999999") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV62ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV63ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV64ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV65ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + localUtil.format( AV50LccbTi, "999") ;
      AV40Txt = AV40Txt + localUtil.format( AV126Num5P, "99999") ;
      AV40Txt = AV40Txt + localUtil.format( AV126Num5P, "99999") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV67ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV68ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.space( (short)(131)) ;
      AV40Txt = GXutil.substring( AV40Txt, 1, 235) ;
      if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
      {
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(235)) ;
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      else
      {
         AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
         AV40Txt = AV40Txt + GXutil.newLine( ) ;
         AV129xmlWr.writeRawText(AV40Txt);
      }
   }

   public void S161( ) throws ProcessInterruptedException
   {
      /* 'GRAVATC' Routine */
      AV30NumReg = (int)(AV30NumReg+1) ;
      AV40Txt = "TC" ;
      AV40Txt = AV40Txt + localUtil.format( AV30NumReg, "999999") ;
      AV40Txt = AV40Txt + "00000000" ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV69ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV70ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV71ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV72ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + "000" ;
      AV58Num05P = AV145Total ;
      AV40Txt = AV40Txt + localUtil.format( AV58Num05P, "99999") ;
      AV40Txt = AV40Txt + "00000" ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV73ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.padl( GXutil.strReplace( GXutil.strReplace( localUtil.format( AV74ValTot, "999999999999.99"), ".", ""), ",", ""), (short)(15), "0") ;
      AV40Txt = AV40Txt + GXutil.space( (short)(131)) ;
      AV40Txt = GXutil.substring( AV40Txt, 1, 235) ;
      if ( ( GXutil.strcmp(AV128CarFi, "S") == 0 ) )
      {
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV40Txt, (short)(235)) ;
         AV35FileOu = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      else
      {
         AV40Txt = GXutil.padr( AV40Txt, (short)(235), " ") ;
         AV40Txt = AV40Txt + GXutil.newLine( ) ;
         AV129xmlWr.writeRawText(AV40Txt);
      }
   }

   public void S144( ) throws ProcessInterruptedException
   {
      /* 'CONVERTE_AUTORIZA��O' Routine */
      AV135sInpu = AV85TxtTmp ;
      AV136sOutp = "" ;
      AV137j = 1 ;
      while ( ( AV137j <= GXutil.len( AV135sInpu) ) )
      {
         AV138c = GXutil.substring( AV135sInpu, AV137j, 1) ;
         if ( ( GXutil.strSearch( "0123456789", AV138c, 1) > 0 ) )
         {
            AV136sOutp = AV136sOutp + AV138c ;
         }
         else
         {
            AV136sOutp = AV136sOutp + "0" ;
         }
         AV137j = (int)(AV137j+1) ;
      }
      AV85TxtTmp = AV136sOutp ;
   }

   public void h6I0( boolean bFoot ,
                     int Inc )
   {
      /* Skip the required number of lines */
      while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
      {
         if ( ( Gx_line + Inc >= P_lines ) )
         {
            if ( ( Gx_page > 0 ) )
            {
               /* Print footers */
               Gx_line = P_lines ;
               if ( ! bFoot )
               {
                  getPrinter().GxEndPage() ;
               }
               if ( bFoot )
               {
                  return  ;
               }
            }
            ToSkip = 0 ;
            Gx_line = 0 ;
            Gx_page = (int)(Gx_page+1) ;
            /* Skip Margin Top Lines */
            Gx_line = (int)(Gx_line+(M_top*lineHeight)) ;
            /* Print headers */
            getPrinter().GxStartPage() ;
            getPrinter().setPage(Gx_page);
            getPrinter().GxDrawLine(0, Gx_line+100, 783, Gx_line+100, 1, 0, 0, 0, 0) ;
            getPrinter().GxDrawLine(0, Gx_line+80, 783, Gx_line+80, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(localUtil.format( Gx_date, "99/99/99"), 737, Gx_line+25, 778, Gx_line+39, 0+256) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( Gx_page, "ZZZZZ9")), 737, Gx_line+5, 772, Gx_line+19, 2+256) ;
            getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("PAGINA:", 678, Gx_line+5, 720, Gx_line+19, 0+256) ;
            getPrinter().GxDrawText("DATA:", 678, Gx_line+23, 708, Gx_line+37, 0+256) ;
            getPrinter().GxAttris("Arial", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("EMPRESA AEREA:", 0, Gx_line+53, 99, Gx_line+68, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("TIESS TAM", 0, Gx_line+6, 68, Gx_line+22, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV18lccbEm, "@!")), 109, Gx_line+52, 147, Gx_line+68, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV19EmpNom, "@!")), 155, Gx_line+52, 334, Gx_line+68, 0+256) ;
            getPrinter().GxAttris("MS Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("RELATORIO DE VENDAS A CREDITO ACEITAS", 212, Gx_line+20, 604, Gx_line+40, 0+256) ;
            getPrinter().GxAttris("MS Serif", 7, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("i1009", 0, Gx_line+27, 25, Gx_line+38, 0+256) ;
            getPrinter().GxDrawText("N� Cart�o", 195, Gx_line+84, 243, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Tp", 0, Gx_line+84, 12, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Valor CC", 516, Gx_line+84, 560, Gx_line+95, 2+256) ;
            getPrinter().GxDrawText("Taxas ", 607, Gx_line+84, 639, Gx_line+95, 2+256) ;
            getPrinter().GxDrawText("Par(s)", 568, Gx_line+84, 598, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Vlr Parcela", 647, Gx_line+84, 703, Gx_line+95, 2+256) ;
            getPrinter().GxDrawText("Valor Entrada", 716, Gx_line+84, 785, Gx_line+95, 2+256) ;
            getPrinter().GxDrawText("N� Ro", 90, Gx_line+84, 118, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Bilhete", 313, Gx_line+84, 349, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Iata", 139, Gx_line+84, 160, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Emissao", 362, Gx_line+84, 403, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Processadora", 20, Gx_line+84, 87, Gx_line+95, 0+256) ;
            getPrinter().GxDrawText("Autoriza��o", 419, Gx_line+84, 480, Gx_line+95, 0+256) ;
            Gx_OldLine = Gx_line ;
            Gx_line = (int)(Gx_line+106) ;
            if (true) break;
         }
         else
         {
            PrtOffset = 0 ;
            Gx_line = (int)(Gx_line+1) ;
         }
         ToSkip = (int)(ToSkip-1) ;
      }
      getPrinter().setPage(Gx_page);
   }

   public void add_metrics( )
   {
      add_metrics0( ) ;
      add_metrics1( ) ;
      add_metrics2( ) ;
      add_metrics3( ) ;
      add_metrics4( ) ;
   }

   public void add_metrics0( )
   {
      getPrinter().setMetrics("MS Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics1( )
   {
      getPrinter().setMetrics("MS Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 36, 48, 14, 36, 21, 64, 36, 36, 21, 64, 43, 21, 64, 48, 39, 48, 48, 14, 14, 21, 21, 22, 36, 64, 20, 64, 32, 21, 60, 48, 31, 43, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
   }

   public void add_metrics2( )
   {
      getPrinter().setMetrics("Arial", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 36, 48, 14, 36, 21, 64, 36, 36, 21, 64, 43, 21, 64, 48, 39, 48, 48, 14, 14, 21, 21, 22, 36, 64, 20, 64, 32, 21, 60, 48, 31, 43, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
   }

   public void add_metrics3( )
   {
      getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics4( )
   {
      getPrinter().setMetrics("MS Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   protected int getOutputType( )
   {
      return OUTPUT_PDF;
   }

   protected void cleanup( )
   {
      this.aP0[0] = preti1010rpt.this.AV18lccbEm;
      this.aP1[0] = preti1010rpt.this.AV19EmpNom;
      this.aP2[0] = preti1010rpt.this.AV22Pathpd;
      this.aP3[0] = preti1010rpt.this.AV23PathTx;
      this.aP4[0] = preti1010rpt.this.AV10DataB;
      this.aP5[0] = preti1010rpt.this.AV12DebugM;
      this.aP6[0] = preti1010rpt.this.AV139Modo;
      Application.commit(context, remoteHandle, "DEFAULT", "preti1010rpt");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      M_top = 0 ;
      M_bot = 0 ;
      Gx_line = 0 ;
      ToSkip = 0 ;
      PrtOffset = 0 ;
      AV128CarFi = "" ;
      AV148Bande = "" ;
      AV149NovaB = "" ;
      AV150Bande = "" ;
      AV151NovaB = "" ;
      AV30NumReg = 0 ;
      GXt_char1 = "" ;
      GXt_char3 = "" ;
      GXt_char4 = "" ;
      AV84TestMo = (byte)(0) ;
      scmdbuf = "" ;
      P006I2_A1488ICSI_ = new String[] {""} ;
      P006I2_A1487ICSI_ = new String[] {""} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      AV87EmpOk = "" ;
      AV29CriaAr = "" ;
      AV81ICSI_E = "" ;
      returnInSub = false ;
      AV95W = (byte)(0) ;
      AV106TotSa = 0 ;
      AV102Total = 0 ;
      AV109TotEn = 0 ;
      AV107TotTi = 0 ;
      AV77ValPar = 0 ;
      AV112Total = 0 ;
      AV108TotPa = 0 ;
      AV118DataC = "" ;
      AV38NovLot = "" ;
      AV127Old_L = "" ;
      A1170lccbI = 0 ;
      A1227lccbO = "" ;
      A1184lccbS = "" ;
      A1150lccbE = "" ;
      P006I3_A1227lccbO = new String[] {""} ;
      P006I3_A1184lccbS = new String[] {""} ;
      P006I3_n1184lccbS = new boolean[] {false} ;
      P006I3_A1150lccbE = new String[] {""} ;
      P006I3_A1228lccbF = new String[] {""} ;
      P006I3_A1226lccbA = new String[] {""} ;
      P006I3_A1225lccbC = new String[] {""} ;
      P006I3_A1224lccbC = new String[] {""} ;
      P006I3_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006I3_A1222lccbI = new String[] {""} ;
      P006I3_A1490Distr = new String[] {""} ;
      P006I3_n1490Distr = new boolean[] {false} ;
      P006I3_A1170lccbI = new double[1] ;
      P006I3_n1170lccbI = new boolean[] {false} ;
      P006I3_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P006I3_n1189lccbS = new boolean[] {false} ;
      P006I3_A1206LccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006I3_n1206LccbR = new boolean[] {false} ;
      P006I3_A1193lccbS = new String[] {""} ;
      P006I3_n1193lccbS = new boolean[] {false} ;
      P006I3_A1172lccbS = new double[1] ;
      P006I3_n1172lccbS = new boolean[] {false} ;
      P006I3_A1168lccbI = new short[1] ;
      P006I3_n1168lccbI = new boolean[] {false} ;
      P006I3_A1515lccbC = new double[1] ;
      P006I3_n1515lccbC = new boolean[] {false} ;
      P006I3_A1171lccbT = new double[1] ;
      P006I3_n1171lccbT = new boolean[] {false} ;
      P006I3_A1203lccbR = new long[1] ;
      P006I3_n1203lccbR = new boolean[] {false} ;
      P006I3_A1163lccbP = new String[] {""} ;
      P006I3_n1163lccbP = new boolean[] {false} ;
      P006I3_A1204lccbR = new String[] {""} ;
      P006I3_n1204lccbR = new boolean[] {false} ;
      P006I3_A1495lccbC = new java.util.Date[] {GXutil.nullDate()} ;
      P006I3_n1495lccbC = new boolean[] {false} ;
      P006I3_A1190lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P006I3_n1190lccbS = new boolean[] {false} ;
      P006I3_A1191lccbS = new String[] {""} ;
      P006I3_n1191lccbS = new boolean[] {false} ;
      P006I3_A1194lccbS = new String[] {""} ;
      P006I3_n1194lccbS = new boolean[] {false} ;
      n1184lccbS = false ;
      A1228lccbF = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1490Distr = "" ;
      n1490Distr = false ;
      n1170lccbI = false ;
      A1189lccbS = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1206LccbR = GXutil.nullDate() ;
      n1206LccbR = false ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1515lccbC = 0 ;
      n1515lccbC = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1203lccbR = 0 ;
      n1203lccbR = false ;
      A1163lccbP = "" ;
      n1163lccbP = false ;
      A1204lccbR = "" ;
      n1204lccbR = false ;
      A1495lccbC = GXutil.nullDate() ;
      n1495lccbC = false ;
      A1190lccbS = GXutil.resetTime( GXutil.nullDate() );
      n1190lccbS = false ;
      A1191lccbS = "" ;
      n1191lccbS = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      AV154SemDa = "" ;
      AV32Dia = (byte)(0) ;
      AV33Mes = (byte)(0) ;
      AV34Ano = (short)(0) ;
      AV96LccbDa = "" ;
      AV49LccbTi = (short)(0) ;
      AV50LccbTi = (short)(0) ;
      AV73ValTot = 0 ;
      AV74ValTot = 0 ;
      AV70ValTot = 0 ;
      AV72ValTot = 0 ;
      AV71ValTot = 0 ;
      AV69ValTot = 0 ;
      AV97ArqNum = 0 ;
      AV40Txt = "" ;
      AV39HoraB = "" ;
      AV41Num03P = (short)(0) ;
      AV35FileOu = 0 ;
      AV129xmlWr = new com.genexus.xml.XMLWriter();
      AV53LccbCr = "" ;
      AV122LccbC = "" ;
      AV83LccbRC = GXutil.nullDate() ;
      Gx_OldLine = 0 ;
      AV144Total = 0 ;
      AV57NumReg = 0 ;
      AV67ValTot = 0 ;
      AV68ValTot = 0 ;
      AV63ValTot = 0 ;
      AV65ValTot = 0 ;
      AV64ValTot = 0 ;
      AV62ValTot = 0 ;
      AV43NumLot = 0 ;
      AV82LccbCC = "" ;
      AV100Gravo = "" ;
      AV99Gravou = "" ;
      AV66QtdTot = 0 ;
      AV88LccbDa = GXutil.nullDate() ;
      AV140Atrib = "" ;
      AV142Resul = "" ;
      AV89LccbCC = "" ;
      GXv_svchar2 = new String [1] ;
      AV119Num17 = 0 ;
      AV90LccbAp = "" ;
      AV91LccbOp = "" ;
      AV92LccbFP = "" ;
      AV52Lccbia = "" ;
      AV42Num08p = 0 ;
      AV93LccbIa = "" ;
      AV120LccbS = 0 ;
      AV116LccbI = 0 ;
      AV131CV = "" ;
      AV111Total = 0 ;
      AV110TotRe = 0 ;
      AV113Total = 0 ;
      AV114Total = 0 ;
      AV115Total = 0 ;
      AV145Total = 0 ;
      AV56ContaT = (short)(0) ;
      AV51Flgtkt = "" ;
      AV60NumCCC = 0 ;
      GX_I = 0 ;
      AV47lccbTD = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV47lccbTD[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV44DbIata = new int [99] ;
      AV45DbCCcf = new long [99] ;
      AV132Ticke = new long [99] ;
      AV46DbPass = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV46DbPass[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      P006I4_A1150lccbE = new String[] {""} ;
      P006I4_A1222lccbI = new String[] {""} ;
      P006I4_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006I4_A1224lccbC = new String[] {""} ;
      P006I4_A1225lccbC = new String[] {""} ;
      P006I4_A1226lccbA = new String[] {""} ;
      P006I4_A1227lccbO = new String[] {""} ;
      P006I4_A1228lccbF = new String[] {""} ;
      P006I4_A1231lccbT = new String[] {""} ;
      P006I4_A1207lccbP = new String[] {""} ;
      P006I4_n1207lccbP = new boolean[] {false} ;
      P006I4_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1207lccbP = "" ;
      n1207lccbP = false ;
      A1232lccbT = "" ;
      AV130lccbT = "" ;
      AV117LccbC = "" ;
      AV134TKT_V = "" ;
      AV48Num11P = 0 ;
      AV133TKT_V = 0 ;
      AV85TxtTmp = "" ;
      AV146CCCFD = "" ;
      AV55Num15P = 0 ;
      AV147Num15 = "" ;
      AV75ValVen = 0 ;
      AV78ValAce = 0 ;
      AV76ValEnt = 0 ;
      AV80ValTax = 0 ;
      AV79ValDes = 0 ;
      AV31Num02P = (byte)(0) ;
      AV105LccbS = "" ;
      AV61CreDat = "" ;
      AV143Proce = "" ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      A1230lccbS = (short)(0) ;
      AV121y = (short)(0) ;
      Gx_date = GXutil.nullDate() ;
      AV152Proce = "" ;
      AV153msgEr = "" ;
      AV24Comman = "" ;
      AV21Path1 = "" ;
      AV36Sep = "" ;
      AV20Path = "" ;
      GXt_char5 = "" ;
      GXv_char6 = new String [1] ;
      Gx_time = "" ;
      AV163GXLvl = (byte)(0) ;
      P006I7_A1488ICSI_ = new String[] {""} ;
      P006I7_A1487ICSI_ = new String[] {""} ;
      P006I7_A1479ICSI_ = new int[1] ;
      P006I7_n1479ICSI_ = new boolean[] {false} ;
      A1479ICSI_ = 0 ;
      n1479ICSI_ = false ;
      AV59i = (short)(0) ;
      AV126Num5P = 0 ;
      AV58Num05P = 0 ;
      AV135sInpu = "" ;
      AV136sOutp = "" ;
      AV137j = 0 ;
      AV138c = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new preti1010rpt__default(),
         new Object[] {
             new Object[] {
            P006I2_A1488ICSI_, P006I2_A1487ICSI_
            }
            , new Object[] {
            P006I3_A1227lccbO, P006I3_A1184lccbS, P006I3_n1184lccbS, P006I3_A1150lccbE, P006I3_A1228lccbF, P006I3_A1226lccbA, P006I3_A1225lccbC, P006I3_A1224lccbC, P006I3_A1223lccbD, P006I3_A1222lccbI,
            P006I3_A1490Distr, P006I3_n1490Distr, P006I3_A1170lccbI, P006I3_n1170lccbI, P006I3_A1189lccbS, P006I3_n1189lccbS, P006I3_A1206LccbR, P006I3_n1206LccbR, P006I3_A1193lccbS, P006I3_n1193lccbS,
            P006I3_A1172lccbS, P006I3_n1172lccbS, P006I3_A1168lccbI, P006I3_n1168lccbI, P006I3_A1515lccbC, P006I3_n1515lccbC, P006I3_A1171lccbT, P006I3_n1171lccbT, P006I3_A1203lccbR, P006I3_n1203lccbR,
            P006I3_A1163lccbP, P006I3_n1163lccbP, P006I3_A1204lccbR, P006I3_n1204lccbR, P006I3_A1495lccbC, P006I3_n1495lccbC, P006I3_A1190lccbS, P006I3_n1190lccbS, P006I3_A1191lccbS, P006I3_n1191lccbS,
            P006I3_A1194lccbS, P006I3_n1194lccbS
            }
            , new Object[] {
            P006I4_A1150lccbE, P006I4_A1222lccbI, P006I4_A1223lccbD, P006I4_A1224lccbC, P006I4_A1225lccbC, P006I4_A1226lccbA, P006I4_A1227lccbO, P006I4_A1228lccbF, P006I4_A1231lccbT, P006I4_A1207lccbP,
            P006I4_n1207lccbP, P006I4_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006I7_A1488ICSI_, P006I7_A1487ICSI_, P006I7_A1479ICSI_, P006I7_n1479ICSI_
            }
            , new Object[] {
            }
         }
      );
      Gx_time = GXutil.time( ) ;
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_line = 0 ;
      Gx_time = GXutil.time( ) ;
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV84TestMo ;
   private byte AV95W ;
   private byte AV32Dia ;
   private byte AV33Mes ;
   private byte AV31Num02P ;
   private byte AV163GXLvl ;
   private short A1168lccbI ;
   private short AV34Ano ;
   private short AV49LccbTi ;
   private short AV50LccbTi ;
   private short AV41Num03P ;
   private short AV56ContaT ;
   private short Gx_err ;
   private short A1230lccbS ;
   private short AV121y ;
   private short AV59i ;
   private int M_top ;
   private int M_bot ;
   private int Line ;
   private int ToSkip ;
   private int PrtOffset ;
   private int AV30NumReg ;
   private int AV97ArqNum ;
   private int Gx_OldLine ;
   private int AV144Total ;
   private int AV57NumReg ;
   private int AV43NumLot ;
   private int AV66QtdTot ;
   private int AV42Num08p ;
   private int AV110TotRe ;
   private int AV145Total ;
   private int GX_I ;
   private int AV44DbIata[] ;
   private int GX_INS236 ;
   private int A1479ICSI_ ;
   private int AV126Num5P ;
   private int AV58Num05P ;
   private int AV137j ;
   private long A1203lccbR ;
   private long AV35FileOu ;
   private long AV119Num17 ;
   private long AV60NumCCC ;
   private long AV45DbCCcf[] ;
   private long AV132Ticke[] ;
   private long AV48Num11P ;
   private long AV133TKT_V ;
   private double AV106TotSa ;
   private double AV102Total ;
   private double AV109TotEn ;
   private double AV107TotTi ;
   private double AV77ValPar ;
   private double AV112Total ;
   private double AV108TotPa ;
   private double A1170lccbI ;
   private double A1172lccbS ;
   private double A1515lccbC ;
   private double A1171lccbT ;
   private double AV73ValTot ;
   private double AV74ValTot ;
   private double AV70ValTot ;
   private double AV72ValTot ;
   private double AV71ValTot ;
   private double AV69ValTot ;
   private double AV67ValTot ;
   private double AV68ValTot ;
   private double AV63ValTot ;
   private double AV65ValTot ;
   private double AV64ValTot ;
   private double AV62ValTot ;
   private double AV120LccbS ;
   private double AV116LccbI ;
   private double AV111Total ;
   private double AV113Total ;
   private double AV114Total ;
   private double AV115Total ;
   private double AV55Num15P ;
   private double AV75ValVen ;
   private double AV78ValAce ;
   private double AV76ValEnt ;
   private double AV80ValTax ;
   private double AV79ValDes ;
   private String AV18lccbEm ;
   private String AV19EmpNom ;
   private String AV22Pathpd ;
   private String AV23PathTx ;
   private String AV10DataB ;
   private String AV12DebugM ;
   private String AV139Modo ;
   private String AV128CarFi ;
   private String GXt_char1 ;
   private String GXt_char3 ;
   private String GXt_char4 ;
   private String scmdbuf ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String AV87EmpOk ;
   private String AV29CriaAr ;
   private String AV81ICSI_E ;
   private String AV118DataC ;
   private String AV38NovLot ;
   private String AV127Old_L ;
   private String A1227lccbO ;
   private String A1184lccbS ;
   private String A1150lccbE ;
   private String A1228lccbF ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1490Distr ;
   private String A1193lccbS ;
   private String A1163lccbP ;
   private String A1204lccbR ;
   private String A1191lccbS ;
   private String A1194lccbS ;
   private String AV154SemDa ;
   private String AV96LccbDa ;
   private String AV40Txt ;
   private String AV39HoraB ;
   private String AV53LccbCr ;
   private String AV122LccbC ;
   private String AV82LccbCC ;
   private String AV100Gravo ;
   private String AV99Gravou ;
   private String AV140Atrib ;
   private String AV142Resul ;
   private String AV89LccbCC ;
   private String AV90LccbAp ;
   private String AV91LccbOp ;
   private String AV92LccbFP ;
   private String AV52Lccbia ;
   private String AV93LccbIa ;
   private String AV131CV ;
   private String AV51Flgtkt ;
   private String AV47lccbTD[] ;
   private String AV46DbPass[] ;
   private String A1231lccbT ;
   private String A1207lccbP ;
   private String A1232lccbT ;
   private String AV130lccbT ;
   private String AV117LccbC ;
   private String AV134TKT_V ;
   private String AV85TxtTmp ;
   private String AV146CCCFD ;
   private String AV105LccbS ;
   private String AV61CreDat ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String AV24Comman ;
   private String AV21Path1 ;
   private String AV36Sep ;
   private String AV20Path ;
   private String GXt_char5 ;
   private String GXv_char6[] ;
   private String Gx_time ;
   private String AV135sInpu ;
   private String AV136sOutp ;
   private String AV138c ;
   private java.util.Date A1190lccbS ;
   private java.util.Date A1229lccbS ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1189lccbS ;
   private java.util.Date A1206LccbR ;
   private java.util.Date A1495lccbC ;
   private java.util.Date AV83LccbRC ;
   private java.util.Date AV88LccbDa ;
   private java.util.Date Gx_date ;
   private boolean returnInSub ;
   private boolean n1184lccbS ;
   private boolean n1490Distr ;
   private boolean n1170lccbI ;
   private boolean n1189lccbS ;
   private boolean n1206LccbR ;
   private boolean n1193lccbS ;
   private boolean n1172lccbS ;
   private boolean n1168lccbI ;
   private boolean n1515lccbC ;
   private boolean n1171lccbT ;
   private boolean n1203lccbR ;
   private boolean n1163lccbP ;
   private boolean n1204lccbR ;
   private boolean n1495lccbC ;
   private boolean n1190lccbS ;
   private boolean n1191lccbS ;
   private boolean n1194lccbS ;
   private boolean n1207lccbP ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n1479ICSI_ ;
   private String AV148Bande ;
   private String AV149NovaB ;
   private String AV150Bande ;
   private String AV151NovaB ;
   private String GXv_svchar2[] ;
   private String AV147Num15 ;
   private String AV143Proce ;
   private String A1187lccbS ;
   private String AV152Proce ;
   private String AV153msgEr ;
   private com.genexus.xml.XMLWriter AV129xmlWr ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
   private String[] aP5 ;
   private String[] aP6 ;
   private IDataStoreProvider pr_default ;
   private String[] P006I2_A1488ICSI_ ;
   private String[] P006I2_A1487ICSI_ ;
   private String[] P006I3_A1227lccbO ;
   private String[] P006I3_A1184lccbS ;
   private boolean[] P006I3_n1184lccbS ;
   private String[] P006I3_A1150lccbE ;
   private String[] P006I3_A1228lccbF ;
   private String[] P006I3_A1226lccbA ;
   private String[] P006I3_A1225lccbC ;
   private String[] P006I3_A1224lccbC ;
   private java.util.Date[] P006I3_A1223lccbD ;
   private String[] P006I3_A1222lccbI ;
   private String[] P006I3_A1490Distr ;
   private boolean[] P006I3_n1490Distr ;
   private double[] P006I3_A1170lccbI ;
   private boolean[] P006I3_n1170lccbI ;
   private java.util.Date[] P006I3_A1189lccbS ;
   private boolean[] P006I3_n1189lccbS ;
   private java.util.Date[] P006I3_A1206LccbR ;
   private boolean[] P006I3_n1206LccbR ;
   private String[] P006I3_A1193lccbS ;
   private boolean[] P006I3_n1193lccbS ;
   private double[] P006I3_A1172lccbS ;
   private boolean[] P006I3_n1172lccbS ;
   private short[] P006I3_A1168lccbI ;
   private boolean[] P006I3_n1168lccbI ;
   private double[] P006I3_A1515lccbC ;
   private boolean[] P006I3_n1515lccbC ;
   private double[] P006I3_A1171lccbT ;
   private boolean[] P006I3_n1171lccbT ;
   private long[] P006I3_A1203lccbR ;
   private boolean[] P006I3_n1203lccbR ;
   private String[] P006I3_A1163lccbP ;
   private boolean[] P006I3_n1163lccbP ;
   private String[] P006I3_A1204lccbR ;
   private boolean[] P006I3_n1204lccbR ;
   private java.util.Date[] P006I3_A1495lccbC ;
   private boolean[] P006I3_n1495lccbC ;
   private java.util.Date[] P006I3_A1190lccbS ;
   private boolean[] P006I3_n1190lccbS ;
   private String[] P006I3_A1191lccbS ;
   private boolean[] P006I3_n1191lccbS ;
   private String[] P006I3_A1194lccbS ;
   private boolean[] P006I3_n1194lccbS ;
   private String[] P006I4_A1150lccbE ;
   private String[] P006I4_A1222lccbI ;
   private java.util.Date[] P006I4_A1223lccbD ;
   private String[] P006I4_A1224lccbC ;
   private String[] P006I4_A1225lccbC ;
   private String[] P006I4_A1226lccbA ;
   private String[] P006I4_A1227lccbO ;
   private String[] P006I4_A1228lccbF ;
   private String[] P006I4_A1231lccbT ;
   private String[] P006I4_A1207lccbP ;
   private boolean[] P006I4_n1207lccbP ;
   private String[] P006I4_A1232lccbT ;
   private String[] P006I7_A1488ICSI_ ;
   private String[] P006I7_A1487ICSI_ ;
   private int[] P006I7_A1479ICSI_ ;
   private boolean[] P006I7_n1479ICSI_ ;
}

final  class preti1010rpt__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P006I3( double AV77ValPar ,
                                        double A1170lccbI ,
                                        String AV81ICSI_E ,
                                        String A1227lccbO ,
                                        String A1184lccbS ,
                                        String A1150lccbE )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT [lccbOpCode], [lccbStatus], [lccbEmpCod], [lccbFPAC_PLP], [lccbAppCode], [lccbCCNum]," ;
      scmdbuf = scmdbuf + " [lccbCCard], [lccbDate], [lccbIATA], [DistribuicaoTransacoesTipo], [lccbInstAmount]," ;
      scmdbuf = scmdbuf + " [lccbSubDate], [LccbRCredDate], [lccbSubTrn], [lccbSaleAmount], [lccbInstallments]," ;
      scmdbuf = scmdbuf + " [lccbCashAmount], [lccbTip], [lccbRSubCCCF], [lccbPlanCode], [lccbRSubRO], [lccbCreationDate]," ;
      scmdbuf = scmdbuf + " [lccbSubTime], [lccbSubType], [lccbSubRO] FROM [LCCBPLP] WITH (NOLOCK)" ;
      scmdbuf = scmdbuf + " WHERE ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and ([lccbStatus] = 'RETOK')" ;
      scmdbuf = scmdbuf + " and ([lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV81ICSI_E, "'", "''")) + "')" ;
      scmdbuf = scmdbuf + " and ([lccbOpCode] = 'S' and [lccbStatus] = 'RETOK' and [lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV81ICSI_E, "'", "''")) + "')" ;
      sWhereString = "" ;
      if ( ( AV77ValPar == 0 ) )
      {
         sWhereString = sWhereString + " and ([lccbInstAmount] <= 0.00)" ;
      }
      if ( ( AV77ValPar == 1 ) )
      {
         sWhereString = sWhereString + " and ([lccbInstAmount] > 0.00)" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY [lccbOpCode], [lccbStatus], [lccbEmpCod], [lccbCCard], [lccbSubRO]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 1 :
                  return conditional_P006I3( ((Number) dynConstraints[0]).doubleValue() , ((Number) dynConstraints[1]).doubleValue() , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006I2", "SELECT [ICSI_CCCod], [ICSI_EmpCod] FROM [ICSI_CCINFO] WITH (NOLOCK) WHERE [ICSI_EmpCod] = ? and [ICSI_CCCod] = 'I1010' ORDER BY [ICSI_EmpCod], [ICSI_CCCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P006I3", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006I4", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbPaxName], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006I5", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006I6", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbSubTime]=?, [lccbSubType]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006I7", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCSeqFile] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = ? AND [ICSI_CCCod] = 'I1010') AND ([ICSI_EmpCod] = ? and [ICSI_CCCod] = 'I1010') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P006I8", "UPDATE [ICSI_CCINFO] SET [ICSI_CCSeqFile]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 8) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 19) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[7])[0] = rslt.getString(7, 2) ;
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 7) ;
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[14])[0] = rslt.getGXDate(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[16])[0] = rslt.getGXDate(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 20) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((short[]) buf[22])[0] = rslt.getShort(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((double[]) buf[24])[0] = rslt.getDouble(17) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((double[]) buf[26])[0] = rslt.getDouble(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((long[]) buf[28])[0] = rslt.getLong(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 10) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[34])[0] = rslt.getGXDate(22) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[36])[0] = rslt.getGXDateTime(23) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               ((String[]) buf[38])[0] = rslt.getString(24, 1) ;
               ((boolean[]) buf[39])[0] = rslt.wasNull();
               ((String[]) buf[40])[0] = rslt.getString(25, 10) ;
               ((boolean[]) buf[41])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 50) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(11, 4) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((int[]) buf[2])[0] = rslt.getInt(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 1);
               }
               stmt.setString(4, (String)parms[6], 3);
               stmt.setString(5, (String)parms[7], 7);
               stmt.setDate(6, (java.util.Date)parms[8]);
               stmt.setString(7, (String)parms[9], 2);
               stmt.setString(8, (String)parms[10], 44);
               stmt.setString(9, (String)parms[11], 20);
               stmt.setString(10, (String)parms[12], 1);
               stmt.setString(11, (String)parms[13], 19);
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 3);
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
      }
   }

}



public final  class StructSdtLogins_LastLoginInfo implements Cloneable, java.io.Serializable
{
   public StructSdtLogins_LastLoginInfo( )
   {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      cal.set(1, 0, 1, 0, 0, 0);
      cal.set(java.util.Calendar.MILLISECOND, 0);
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd = "" ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = cal.getTime() ;
      gxTv_SdtLogins_LastLoginInfo_Mode = "" ;
      gxTv_SdtLogins_LastLoginInfo_Modified = (short)(0) ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = "" ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = cal.getTime() ;
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getLgnlastpwd( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd ;
   }

   public void setLgnlastpwd( String value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd = value ;
      return  ;
   }

   public java.util.Date getLgnlastpwddate( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate ;
   }

   public void setLgnlastpwddate( java.util.Date value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtLogins_LastLoginInfo_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtLogins_LastLoginInfo_Modified = value ;
      return  ;
   }

   public String getLgnlastpwd_Z( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z ;
   }

   public void setLgnlastpwd_Z( String value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z = value ;
      return  ;
   }

   public java.util.Date getLgnlastpwddate_Z( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z ;
   }

   public void setLgnlastpwddate_Z( java.util.Date value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z = value ;
      return  ;
   }

   public byte getLgnlastpwddate_N( )
   {
      return gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N ;
   }

   public void setLgnlastpwddate_N( byte value )
   {
      gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N = value ;
      return  ;
   }

   protected byte gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_N ;
   protected short gxTv_SdtLogins_LastLoginInfo_Modified ;
   protected String gxTv_SdtLogins_LastLoginInfo_Mode ;
   protected String gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd ;
   protected String gxTv_SdtLogins_LastLoginInfo_Lgnlastpwd_Z ;
   protected java.util.Date gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate ;
   protected java.util.Date gxTv_SdtLogins_LastLoginInfo_Lgnlastpwddate_Z ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtAirlines_Level1Item extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtAirlines_Level1Item( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtAirlines_Level1Item.class));
   }

   public SdtAirlines_Level1Item( int remoteHandle ,
                                  ModelContext context )
   {
      super( context, "SdtAirlines_Level1Item");
      initialize( remoteHandle) ;
   }

   public SdtAirlines_Level1Item( int remoteHandle ,
                                  StructSdtAirlines_Level1Item struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtAirlines_Level1Item( )
   {
      super( new ModelContext(SdtAirlines_Level1Item.class), "SdtAirlines_Level1Item");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCurCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Airlinecurcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLIneDataBank") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Airlinedatabank = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCurTrans") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Airlinecurtrans = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCurCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLIneDataBank_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AirLineCurTrans_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Airlines.Level1Item" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("AirLineCurCode", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Airlinecurcode));
      oWriter.writeElement("AirLIneDataBank", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Airlinedatabank));
      oWriter.writeElement("AirLineCurTrans", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Airlinecurtrans));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtAirlines_Level1Item_Modified, 4, 0)));
      oWriter.writeElement("AirLineCurCode_Z", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z));
      oWriter.writeElement("AirLIneDataBank_Z", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z));
      oWriter.writeElement("AirLineCurTrans_Z", GXutil.rtrim( gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Airlinecurcode( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurcode ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurcode( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurcode_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Airlinedatabank( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinedatabank ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinedatabank( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinedatabank = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinedatabank_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinedatabank = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Airlinecurtrans( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurtrans ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurtrans( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurtrans_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Mode( )
   {
      return gxTv_SdtAirlines_Level1Item_Mode ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Mode( String value )
   {
      gxTv_SdtAirlines_Level1Item_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Mode_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtAirlines_Level1Item_Modified( )
   {
      return gxTv_SdtAirlines_Level1Item_Modified ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Modified( short value )
   {
      gxTv_SdtAirlines_Level1Item_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Modified_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z( )
   {
      return gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z( String value )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = value ;
      return  ;
   }

   public void setgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z_SetNull( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtAirlines_Level1Item_Airlinecurcode = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinedatabank = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans = "" ;
      gxTv_SdtAirlines_Level1Item_Mode = "" ;
      gxTv_SdtAirlines_Level1Item_Modified = (short)(0) ;
      gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z = "" ;
      gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char3 = "" ;
      return  ;
   }

   public SdtAirlines_Level1Item Clone( )
   {
      return (SdtAirlines_Level1Item)(clone()) ;
   }

   public void setStruct( StructSdtAirlines_Level1Item struct )
   {
      setgxTv_SdtAirlines_Level1Item_Airlinecurcode(struct.getAirlinecurcode());
      setgxTv_SdtAirlines_Level1Item_Airlinedatabank(struct.getAirlinedatabank());
      setgxTv_SdtAirlines_Level1Item_Airlinecurtrans(struct.getAirlinecurtrans());
      setgxTv_SdtAirlines_Level1Item_Mode(struct.getMode());
      setgxTv_SdtAirlines_Level1Item_Modified(struct.getModified());
      setgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z(struct.getAirlinecurcode_Z());
      setgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z(struct.getAirlinedatabank_Z());
      setgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z(struct.getAirlinecurtrans_Z());
   }

   public StructSdtAirlines_Level1Item getStruct( )
   {
      StructSdtAirlines_Level1Item struct = new StructSdtAirlines_Level1Item ();
      struct.setAirlinecurcode(getgxTv_SdtAirlines_Level1Item_Airlinecurcode());
      struct.setAirlinedatabank(getgxTv_SdtAirlines_Level1Item_Airlinedatabank());
      struct.setAirlinecurtrans(getgxTv_SdtAirlines_Level1Item_Airlinecurtrans());
      struct.setMode(getgxTv_SdtAirlines_Level1Item_Mode());
      struct.setModified(getgxTv_SdtAirlines_Level1Item_Modified());
      struct.setAirlinecurcode_Z(getgxTv_SdtAirlines_Level1Item_Airlinecurcode_Z());
      struct.setAirlinedatabank_Z(getgxTv_SdtAirlines_Level1Item_Airlinedatabank_Z());
      struct.setAirlinecurtrans_Z(getgxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z());
      return struct ;
   }

   protected short gxTv_SdtAirlines_Level1Item_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtAirlines_Level1Item_Mode ;
   protected String sTagName ;
   protected String GXt_char3 ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinedatabank ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinedatabank_Z ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurcode ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurtrans ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurcode_Z ;
   protected String gxTv_SdtAirlines_Level1Item_Airlinecurtrans_Z ;
}


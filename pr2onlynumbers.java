/*
               File: R2OnlyNumbers
        Description: Devolve apenas os n�meros contidos na string
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2onlynumbers extends GXProcedure
{
   public pr2onlynumbers( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2onlynumbers.class ), "" );
   }

   public pr2onlynumbers( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pr2onlynumbers.this.AV9sInput = aP0[0];
      this.aP0 = aP0;
      pr2onlynumbers.this.AV10sOutpu = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV10sOutpu = "" ;
      AV8i = (short)(1) ;
      while ( ( AV8i <= GXutil.len( AV9sInput) ) )
      {
         AV11c = GXutil.substring( AV9sInput, AV8i, 1) ;
         if ( ( GXutil.strSearch( "0123456789", AV11c, 1) > 0 ) )
         {
            AV10sOutpu = AV10sOutpu + AV11c ;
         }
         AV8i = (short)(AV8i+1) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2onlynumbers.this.AV9sInput;
      this.aP1[0] = pr2onlynumbers.this.AV10sOutpu;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV8i = (short)(0) ;
      AV11c = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short AV8i ;
   private short Gx_err ;
   private String AV9sInput ;
   private String AV10sOutpu ;
   private String AV11c ;
   private String[] aP0 ;
   private String[] aP1 ;
}


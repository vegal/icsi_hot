/*
               File: AtualizaConfig
        Description: Atualiza Config
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:5.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class patualizaconfig extends GXProcedure
{
   public patualizaconfig( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( patualizaconfig.class ), "" );
   }

   public patualizaconfig( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String aP0 ,
                             String aP1 )
   {
      patualizaconfig.this.AV9ConfigI = aP0;
      patualizaconfig.this.AV8ConfigV = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      /* Using cursor P007E2 */
      pr_default.execute(0, new Object[] {AV9ConfigI, AV9ConfigI});
      while ( (pr_default.getStatus(0) != 101) )
      {
         gxt7E2 = (byte)(0) ;
         A19ConfigI = P007E2_A19ConfigI[0] ;
         A17ConfigV = P007E2_A17ConfigV[0] ;
         A17ConfigV = AV8ConfigV ;
         gxt7E2 = (byte)(1) ;
         /* Using cursor P007E3 */
         pr_default.execute(1, new Object[] {A17ConfigV, A19ConfigI});
         if ( ( gxt7E2 == 1 ) )
         {
            Application.commit(context, remoteHandle, "DEFAULT", "patualizaconfig");
         }
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(0);
      cleanup();
   }

   protected void cleanup( )
   {
      Application.commit(context, remoteHandle, "DEFAULT", "patualizaconfig");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      P007E2_A19ConfigI = new String[] {""} ;
      P007E2_A17ConfigV = new String[] {""} ;
      gxt7E2 = (byte)(0) ;
      A19ConfigI = "" ;
      A17ConfigV = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new patualizaconfig__default(),
         new Object[] {
             new Object[] {
            P007E2_A19ConfigI, P007E2_A17ConfigV
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte gxt7E2 ;
   private short Gx_err ;
   private String scmdbuf ;
   private String AV9ConfigI ;
   private String AV8ConfigV ;
   private String A19ConfigI ;
   private String A17ConfigV ;
   private IDataStoreProvider pr_default ;
   private String[] P007E2_A19ConfigI ;
   private String[] P007E2_A17ConfigV ;
}

final  class patualizaconfig__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007E2", "SELECT [ConfigID], [ConfigValue] FROM [CONFIG] WITH (UPDLOCK) WHERE ([ConfigID] = ?) AND ([ConfigID] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007E3", "UPDATE [CONFIG] SET [ConfigValue]=?  WHERE [ConfigID] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20);
               stmt.setVarchar(2, (String)parms[1], 20);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 255, false);
               stmt.setVarchar(2, (String)parms[1], 20, false);
               break;
      }
   }

}


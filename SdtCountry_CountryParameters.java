import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtCountry_CountryParameters extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtCountry_CountryParameters( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtCountry_CountryParameters.class));
   }

   public SdtCountry_CountryParameters( int remoteHandle ,
                                        ModelContext context )
   {
      super( context, "SdtCountry_CountryParameters");
      initialize( remoteHandle) ;
   }

   public SdtCountry_CountryParameters( int remoteHandle ,
                                        StructSdtCountry_CountryParameters struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtCountry_CountryParameters( )
   {
      super( new ModelContext(SdtCountry_CountryParameters.class), "SdtCountry_CountryParameters");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOConfigID") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Isoconfigid = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOConfigDes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Isoconfigdes = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOConfigValue") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Isoconfigvalue = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLogin") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Lgnlogin = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "DateUpd") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtCountry_CountryParameters_Dateupd = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtCountry_CountryParameters_Dateupd = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOConfigID_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOConfigDes_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOConfigValue_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLogin_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "DateUpd_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtCountry_CountryParameters_Dateupd_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtCountry_CountryParameters_Dateupd_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "lgnLogin_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Country.CountryParameters" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ISOConfigID", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Isoconfigid));
      oWriter.writeElement("ISOConfigDes", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Isoconfigdes));
      oWriter.writeElement("ISOConfigValue", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Isoconfigvalue));
      oWriter.writeElement("lgnLogin", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Lgnlogin));
      if ( (GXutil.nullDate().equals(gxTv_SdtCountry_CountryParameters_Dateupd)) )
      {
         oWriter.writeElement("DateUpd", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtCountry_CountryParameters_Dateupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtCountry_CountryParameters_Dateupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtCountry_CountryParameters_Dateupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtCountry_CountryParameters_Dateupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtCountry_CountryParameters_Dateupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtCountry_CountryParameters_Dateupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("DateUpd", sDateCnv);
      }
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtCountry_CountryParameters_Modified, 4, 0)));
      oWriter.writeElement("ISOConfigID_Z", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Isoconfigid_Z));
      oWriter.writeElement("ISOConfigDes_Z", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z));
      oWriter.writeElement("ISOConfigValue_Z", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z));
      oWriter.writeElement("lgnLogin_Z", GXutil.rtrim( gxTv_SdtCountry_CountryParameters_Lgnlogin_Z));
      if ( (GXutil.nullDate().equals(gxTv_SdtCountry_CountryParameters_Dateupd_Z)) )
      {
         oWriter.writeElement("DateUpd_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtCountry_CountryParameters_Dateupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtCountry_CountryParameters_Dateupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtCountry_CountryParameters_Dateupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtCountry_CountryParameters_Dateupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtCountry_CountryParameters_Dateupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtCountry_CountryParameters_Dateupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("DateUpd_Z", sDateCnv);
      }
      oWriter.writeElement("lgnLogin_N", GXutil.trim( GXutil.str( gxTv_SdtCountry_CountryParameters_Lgnlogin_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Isoconfigid( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigid ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigid( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigid_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Isoconfigdes( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigdes ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigdes( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigdes = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigdes_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigdes = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Isoconfigvalue( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigvalue ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigvalue( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigvalue_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Lgnlogin( )
   {
      return gxTv_SdtCountry_CountryParameters_Lgnlogin ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Lgnlogin( String value )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(0) ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Lgnlogin_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(1) ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtCountry_CountryParameters_Dateupd( )
   {
      return gxTv_SdtCountry_CountryParameters_Dateupd ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Dateupd( java.util.Date value )
   {
      gxTv_SdtCountry_CountryParameters_Dateupd = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Dateupd_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Dateupd = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Mode( )
   {
      return gxTv_SdtCountry_CountryParameters_Mode ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Mode( String value )
   {
      gxTv_SdtCountry_CountryParameters_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Mode_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtCountry_CountryParameters_Modified( )
   {
      return gxTv_SdtCountry_CountryParameters_Modified ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Modified( short value )
   {
      gxTv_SdtCountry_CountryParameters_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Modified_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Modified = (short)(0) ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Isoconfigid_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigid_Z ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigid_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigid_Z_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtCountry_CountryParameters_Lgnlogin_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Lgnlogin_Z ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Lgnlogin_Z( String value )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Lgnlogin_Z_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtCountry_CountryParameters_Dateupd_Z( )
   {
      return gxTv_SdtCountry_CountryParameters_Dateupd_Z ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Dateupd_Z( java.util.Date value )
   {
      gxTv_SdtCountry_CountryParameters_Dateupd_Z = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Dateupd_Z_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Dateupd_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public byte getgxTv_SdtCountry_CountryParameters_Lgnlogin_N( )
   {
      return gxTv_SdtCountry_CountryParameters_Lgnlogin_N ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Lgnlogin_N( byte value )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = value ;
      return  ;
   }

   public void setgxTv_SdtCountry_CountryParameters_Lgnlogin_N_SetNull( )
   {
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtCountry_CountryParameters_Isoconfigid = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigdes = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue = "" ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin = "" ;
      gxTv_SdtCountry_CountryParameters_Dateupd = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtCountry_CountryParameters_Mode = "" ;
      gxTv_SdtCountry_CountryParameters_Modified = (short)(0) ;
      gxTv_SdtCountry_CountryParameters_Isoconfigid_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Lgnlogin_Z = "" ;
      gxTv_SdtCountry_CountryParameters_Dateupd_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtCountry_CountryParameters_Lgnlogin_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      sDateCnv = "" ;
      sNumToPad = "" ;
      return  ;
   }

   public SdtCountry_CountryParameters Clone( )
   {
      return (SdtCountry_CountryParameters)(clone()) ;
   }

   public void setStruct( StructSdtCountry_CountryParameters struct )
   {
      setgxTv_SdtCountry_CountryParameters_Isoconfigid(struct.getIsoconfigid());
      setgxTv_SdtCountry_CountryParameters_Isoconfigdes(struct.getIsoconfigdes());
      setgxTv_SdtCountry_CountryParameters_Isoconfigvalue(struct.getIsoconfigvalue());
      setgxTv_SdtCountry_CountryParameters_Lgnlogin(struct.getLgnlogin());
      setgxTv_SdtCountry_CountryParameters_Dateupd(struct.getDateupd());
      setgxTv_SdtCountry_CountryParameters_Mode(struct.getMode());
      setgxTv_SdtCountry_CountryParameters_Modified(struct.getModified());
      setgxTv_SdtCountry_CountryParameters_Isoconfigid_Z(struct.getIsoconfigid_Z());
      setgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z(struct.getIsoconfigdes_Z());
      setgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z(struct.getIsoconfigvalue_Z());
      setgxTv_SdtCountry_CountryParameters_Lgnlogin_Z(struct.getLgnlogin_Z());
      setgxTv_SdtCountry_CountryParameters_Dateupd_Z(struct.getDateupd_Z());
      setgxTv_SdtCountry_CountryParameters_Lgnlogin_N(struct.getLgnlogin_N());
   }

   public StructSdtCountry_CountryParameters getStruct( )
   {
      StructSdtCountry_CountryParameters struct = new StructSdtCountry_CountryParameters ();
      struct.setIsoconfigid(getgxTv_SdtCountry_CountryParameters_Isoconfigid());
      struct.setIsoconfigdes(getgxTv_SdtCountry_CountryParameters_Isoconfigdes());
      struct.setIsoconfigvalue(getgxTv_SdtCountry_CountryParameters_Isoconfigvalue());
      struct.setLgnlogin(getgxTv_SdtCountry_CountryParameters_Lgnlogin());
      struct.setDateupd(getgxTv_SdtCountry_CountryParameters_Dateupd());
      struct.setMode(getgxTv_SdtCountry_CountryParameters_Mode());
      struct.setModified(getgxTv_SdtCountry_CountryParameters_Modified());
      struct.setIsoconfigid_Z(getgxTv_SdtCountry_CountryParameters_Isoconfigid_Z());
      struct.setIsoconfigdes_Z(getgxTv_SdtCountry_CountryParameters_Isoconfigdes_Z());
      struct.setIsoconfigvalue_Z(getgxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z());
      struct.setLgnlogin_Z(getgxTv_SdtCountry_CountryParameters_Lgnlogin_Z());
      struct.setDateupd_Z(getgxTv_SdtCountry_CountryParameters_Dateupd_Z());
      struct.setLgnlogin_N(getgxTv_SdtCountry_CountryParameters_Lgnlogin_N());
      return struct ;
   }

   protected byte gxTv_SdtCountry_CountryParameters_Lgnlogin_N ;
   protected short gxTv_SdtCountry_CountryParameters_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtCountry_CountryParameters_Mode ;
   protected String sTagName ;
   protected String GXt_char2 ;
   protected String sDateCnv ;
   protected String sNumToPad ;
   protected java.util.Date gxTv_SdtCountry_CountryParameters_Dateupd ;
   protected java.util.Date gxTv_SdtCountry_CountryParameters_Dateupd_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigid ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigdes ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigvalue ;
   protected String gxTv_SdtCountry_CountryParameters_Lgnlogin ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigid_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigdes_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Isoconfigvalue_Z ;
   protected String gxTv_SdtCountry_CountryParameters_Lgnlogin_Z ;
}


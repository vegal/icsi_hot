/*
               File: R2StrZero
        Description: Fun��o que devolve uma string com zeros a esquerda
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2strzero extends GXProcedure
{
   public pr2strzero( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2strzero.class ), "" );
   }

   public pr2strzero( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( double[] aP0 ,
                        byte[] aP1 ,
                        byte[] aP2 ,
                        String[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( double[] aP0 ,
                             byte[] aP1 ,
                             byte[] aP2 ,
                             String[] aP3 )
   {
      pr2strzero.this.AV8nValor = aP0[0];
      this.aP0 = aP0;
      pr2strzero.this.AV9nLen = aP1[0];
      this.aP1 = aP1;
      pr2strzero.this.AV10nDec = aP2[0];
      this.aP2 = aP2;
      pr2strzero.this.AV11sOutpu = aP3[0];
      this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV11sOutpu = GXutil.right( "000000000000000000000000000000000000000000000000000000000000"+GXutil.trim( GXutil.str( AV8nValor, AV9nLen, AV10nDec)), AV9nLen) ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2strzero.this.AV8nValor;
      this.aP1[0] = pr2strzero.this.AV9nLen;
      this.aP2[0] = pr2strzero.this.AV10nDec;
      this.aP3[0] = pr2strzero.this.AV11sOutpu;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      GXt_char1 = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV9nLen ;
   private byte AV10nDec ;
   private short Gx_err ;
   private double AV8nValor ;
   private String AV11sOutpu ;
   private String GXt_char1 ;
   private double[] aP0 ;
   private byte[] aP1 ;
   private byte[] aP2 ;
   private String[] aP3 ;
}


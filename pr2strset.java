/*
               File: R2StrSet
        Description: Repete um caracter na string passada
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:2.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2strset extends GXProcedure
{
   public pr2strset( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2strset.class ), "" );
   }

   public pr2strset( int remoteHandle ,
                     ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        short[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             short[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 )
   {
      pr2strset.this.AV8sInput = aP0[0];
      this.aP0 = aP0;
      pr2strset.this.AV10c = aP1[0];
      this.aP1 = aP1;
      pr2strset.this.AV12n = aP2[0];
      this.aP2 = aP2;
      pr2strset.this.AV11cPos = aP3[0];
      this.aP3 = aP3;
      pr2strset.this.AV9sOutput = aP4[0];
      this.aP4 = aP4;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV13s = "" ;
      AV12n = (short)(AV12n-(GXutil.len( AV8sInput))) ;
      while ( ( AV12n > 0 ) )
      {
         AV13s = AV13s + AV10c ;
         AV12n = (short)(AV12n-1) ;
      }
      if ( ( GXutil.strcmp(GXutil.upper( AV11cPos), "L") == 0 ) )
      {
         AV9sOutput = AV13s + AV8sInput ;
      }
      else
      {
         AV9sOutput = AV8sInput + AV13s ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2strset.this.AV8sInput;
      this.aP1[0] = pr2strset.this.AV10c;
      this.aP2[0] = pr2strset.this.AV12n;
      this.aP3[0] = pr2strset.this.AV11cPos;
      this.aP4[0] = pr2strset.this.AV9sOutput;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV13s = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short AV12n ;
   private short Gx_err ;
   private String AV8sInput ;
   private String AV10c ;
   private String AV11cPos ;
   private String AV9sOutput ;
   private String AV13s ;
   private String[] aP0 ;
   private String[] aP1 ;
   private short[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
}


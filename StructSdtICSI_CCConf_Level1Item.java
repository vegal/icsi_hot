
public final  class StructSdtICSI_CCConf_Level1Item implements Cloneable, java.io.Serializable
{
   public StructSdtICSI_CCConf_Level1Item( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror = 0 ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Mode = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Modified = (short)(0) ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = 0 ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public int getIcsi_ccerror( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror ;
   }

   public void setIcsi_ccerror( int value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror = value ;
      return  ;
   }

   public String getIcsi_ccerrordsc( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc ;
   }

   public void setIcsi_ccerrordsc( String value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Modified = value ;
      return  ;
   }

   public int getIcsi_ccerror_Z( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z ;
   }

   public void setIcsi_ccerror_Z( int value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = value ;
      return  ;
   }

   public String getIcsi_ccerrordsc_Z( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z ;
   }

   public void setIcsi_ccerrordsc_Z( String value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = value ;
      return  ;
   }

   public byte getIcsi_ccerrordsc_N( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N ;
   }

   public void setIcsi_ccerrordsc_N( byte value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = value ;
      return  ;
   }

   protected byte gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N ;
   protected short gxTv_SdtICSI_CCConf_Level1Item_Modified ;
   protected int gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror ;
   protected int gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z ;
   protected String gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc ;
   protected String gxTv_SdtICSI_CCConf_Level1Item_Mode ;
   protected String gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z ;
}


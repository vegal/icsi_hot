/*
               File: MaintananceTamFiles
        Description: Maintanance Tam Files
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: August 1, 2013 9:51:41.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pmaintanancetamfiles extends GXProcedure
{
   public pmaintanancetamfiles( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pmaintanancetamfiles.class ), "" );
   }

   public pmaintanancetamfiles( int remoteHandle ,
                                ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV9Pathsft = "" ;
      AV10Portsf = "" ;
      AV11Keysft = "" ;
      AV12Usersf = "" ;
      AV13Passsf = "" ;
      AV14Namesf = "" ;
      AV15Hotsft = "" ;
      AV17Missin = "" ;
      AV23Comand = "" ;
      /* Using cursor P007G2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A19ConfigI = P007G2_A19ConfigI[0] ;
         A17ConfigV = P007G2_A17ConfigV[0] ;
         if ( ( GXutil.strcmp(A19ConfigI, "PATHSFTPTAM") == 0 ) )
         {
            AV9Pathsft = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "PORTSFTPTAM") == 0 ) )
         {
            AV10Portsf = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "KEYSFTPTAM") == 0 ) )
         {
            AV11Keysft = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "USERSFTPTAM") == 0 ) )
         {
            AV12Usersf = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "PASSSFTPTAM") == 0 ) )
         {
            AV13Passsf = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "NAMESFTPTAM") == 0 ) )
         {
            AV14Namesf = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "HOSTSFTPTAM") == 0 ) )
         {
            AV15Hotsft = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "PATHBATHSENDEMAIL") == 0 ) )
         {
            AV15Hotsft = A17ConfigV ;
         }
         if ( ( GXutil.strcmp(A19ConfigI, "PATHBATHSENDEMAIL") == 0 ) )
         {
            AV30PathBa = A17ConfigV ;
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( ( GXutil.strcmp(AV15Hotsft, "") == 0 ) || ( GXutil.strcmp(AV14Namesf, "") == 0 ) || ( GXutil.strcmp(AV13Passsf, "") == 0 ) || ( GXutil.strcmp(AV12Usersf, "") == 0 ) || ( GXutil.strcmp(AV11Keysft, "") == 0 ) || ( GXutil.strcmp(AV10Portsf, "") == 0 ) || ( GXutil.strcmp(AV9Pathsft, "") == 0 ) || ( GXutil.strcmp(AV30PathBa, "") == 0 ) )
      {
         GXutil.msg( this, "ATENCAO...Envio abortado! Existem configurações de envio dos arquivos para a TAM em branco !!!" );
         /* Execute user subroutine: S1171 */
         S1171 ();
         if ( returnInSub )
         {
            returnInSub = true;
            cleanup();
            if (true) return;
         }
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      AV23Comand = "\"" + GXutil.trim( AV30PathBa) + "\" \"" + GXutil.trim( AV9Pathsft) + "\" \"" + GXutil.trim( AV10Portsf) + "\" \"" + GXutil.trim( AV11Keysft) + "\" \"" + GXutil.trim( AV12Usersf) + "\" \"" + GXutil.trim( AV13Passsf) + "\" \"" + GXutil.trim( AV14Namesf) + "\" \"" + GXutil.trim( AV15Hotsft) + "\"" ;
      AV24Retorn = (byte)(GXutil.shell( AV23Comand, 1)) ;
      context.msgStatus( "Parameters provided to the batch of TAM files." );
      AV25AuditT = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " Parâmetros passados para a batch de arquivos da TAM." ;
      /* Execute user subroutine: S1286 */
      S1286 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S1171( )
   {
      /* 'SENDEMAIL' Routine */
   }

   public void S1286( )
   {
      /* 'AUDITTRAIL' Routine */
      AV27Auditl = "Localhost" ;
      AV28AuditT = "TIES_ICSI" ;
      AV29AuditT = "PCI" ;
      GXv_svchar1[0] = AV27Auditl ;
      GXv_svchar2[0] = AV28AuditT ;
      GXv_svchar3[0] = AV29AuditT ;
      GXv_svchar4[0] = AV25AuditT ;
      new pnewaudit(remoteHandle, context).execute( GXv_svchar1, GXv_svchar2, GXv_svchar3, GXv_svchar4) ;
      pmaintanancetamfiles.this.AV27Auditl = GXv_svchar1[0] ;
      pmaintanancetamfiles.this.AV28AuditT = GXv_svchar2[0] ;
      pmaintanancetamfiles.this.AV29AuditT = GXv_svchar3[0] ;
      pmaintanancetamfiles.this.AV25AuditT = GXv_svchar4[0] ;
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9Pathsft = "" ;
      AV10Portsf = "" ;
      AV11Keysft = "" ;
      AV12Usersf = "" ;
      AV13Passsf = "" ;
      AV14Namesf = "" ;
      AV15Hotsft = "" ;
      AV17Missin = "" ;
      AV23Comand = "" ;
      scmdbuf = "" ;
      P007G2_A19ConfigI = new String[] {""} ;
      P007G2_A17ConfigV = new String[] {""} ;
      A19ConfigI = "" ;
      A17ConfigV = "" ;
      AV30PathBa = "" ;
      returnInSub = false ;
      AV24Retorn = (byte)(0) ;
      AV25AuditT = "" ;
      AV27Auditl = "" ;
      AV28AuditT = "" ;
      AV29AuditT = "" ;
      GXv_svchar1 = new String [1] ;
      GXv_svchar2 = new String [1] ;
      GXv_svchar3 = new String [1] ;
      GXv_svchar4 = new String [1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pmaintanancetamfiles__default(),
         new Object[] {
             new Object[] {
            P007G2_A19ConfigI, P007G2_A17ConfigV
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV24Retorn ;
   private short Gx_err ;
   private String scmdbuf ;
   private String AV30PathBa ;
   private boolean returnInSub ;
   private String AV9Pathsft ;
   private String AV10Portsf ;
   private String AV11Keysft ;
   private String AV12Usersf ;
   private String AV13Passsf ;
   private String AV14Namesf ;
   private String AV15Hotsft ;
   private String AV17Missin ;
   private String AV23Comand ;
   private String A19ConfigI ;
   private String A17ConfigV ;
   private String AV25AuditT ;
   private String AV27Auditl ;
   private String AV28AuditT ;
   private String AV29AuditT ;
   private String GXv_svchar1[] ;
   private String GXv_svchar2[] ;
   private String GXv_svchar3[] ;
   private String GXv_svchar4[] ;
   private IDataStoreProvider pr_default ;
   private String[] P007G2_A19ConfigI ;
   private String[] P007G2_A17ConfigV ;
}

final  class pmaintanancetamfiles__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007G2", "SELECT [ConfigID], [ConfigValue] FROM [CONFIG] WITH (NOLOCK) WHERE [ConfigID] = 'PATHSFTPTAM' or [ConfigID] = 'PORTSFTPTAM' or [ConfigID] = 'KEYSFTPTAM' or [ConfigID] = 'USERSFTPTAM' or [ConfigID] = 'PASSSFTPTAM' or [ConfigID] = 'NAMESFTPTAM' or [ConfigID] = 'HOSTSFTPTAM' or [ConfigID] = 'PATHBATHSENDEMAIL' ORDER BY [ConfigID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

}


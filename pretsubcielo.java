/*
               File: RETSubCielo
        Description: RETSub Cielo
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:20.59
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pretsubcielo extends GXProcedure
{
   public pretsubcielo( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretsubcielo.class ), "" );
   }

   public pretsubcielo( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      pretsubcielo.this.AV10DebugM = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV84Versao = "0.00.36" ;
      AV10DebugM = GXutil.trim( GXutil.upper( AV10DebugM)) ;
      context.msgStatus( "Visa - Submission - Version "+AV84Versao );
      context.msgStatus( "  Running mode: ["+AV10DebugM+"] - Started at "+GXutil.time( ) );
      context.msgStatus( "  Creating Visa Submission File" );
      if ( ( GXutil.strSearch( AV10DebugM, "SEPPIPE", 1) > 0 ) )
      {
         AV64Sep = "|" ;
      }
      else
      {
         AV64Sep = "" ;
      }
      if ( ( GXutil.strSearch( AV10DebugM, "TESTMODE", 1) > 0 ) )
      {
         AV108TestM = (byte)(1) ;
      }
      else
      {
         AV108TestM = (byte)(0) ;
      }
      AV193DataA = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      AV9DataDep = GXutil.substring( AV193DataA, 7, 2) + GXutil.substring( AV193DataA, 5, 2) + GXutil.substring( AV193DataA, 1, 4) ;
      AV27HoraA = GXutil.time( ) ;
      AV28HoraB = GXutil.substring( AV27HoraA, 1, 2) + GXutil.substring( AV27HoraA, 4, 2) + GXutil.substring( AV27HoraA, 7, 2) ;
      /* Execute user subroutine: S1132 */
      S1132 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S1132( )
   {
      /* 'MAIN' Routine */
      GXt_char2 = AV13FileNa ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_SUB_VI", "Caminho dos arquivos de Submiss�o Visa", "F", "C:\\TEMP\\ICSI\\VI\\", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV13FileNa = GXt_char2 ;
      GXt_char2 = AV207UsaNo ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "USA_NOVA_BAND_HC", "Par�metro que indica se no arquivo de submiss�o Redecard a nova bandeira HC deve ser utilizada", "S", "Y", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV207UsaNo = GXutil.trim( GXt_char2) ;
      GXt_char2 = AV208Bande ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER", "C�digo do cart�o Hipercard", "S", "HC", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV208Bande = GXutil.trim( GXt_char2) ;
      GXt_char2 = AV209NovaB ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER_NEW", "C�digo do cart�o Hipercard para arquivos output", "S", "HP", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV209NovaB = GXutil.trim( GXt_char2) ;
      GXt_char2 = AV211Bande ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO", "C�digo do cart�o ELO", "S", "EL", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV211Bande = GXutil.trim( GXt_char2) ;
      GXt_char2 = AV210NovaB ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO_NEW", "C�digo do cart�o ELO para arquivos output", "S", "EL", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV210NovaB = GXutil.trim( GXt_char2) ;
      GXt_char2 = AV215ICSI_ ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_AIRLINE_TAM", "C�digo Airline TAM", "S", "957", GXv_svchar3) ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV215ICSI_ = GXutil.trim( GXt_char2) ;
      if ( ( AV108TestM == 0 ) )
      {
         AV140Short = "REM" + GXutil.trim( AV193DataA) + AV28HoraB + "_i41VI" ;
         AV152RcptN = AV13FileNa + "REM" + GXutil.trim( AV193DataA) + AV28HoraB + "_i42VI.txt" ;
         AV200Audit = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio gera��o arquivo " + GXutil.trim( AV140Short) ;
         /* Execute user subroutine: S121 */
         S121 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV13FileNa = AV13FileNa + AV140Short + ".txt" ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV13FileNa, AV64Sep, "", (byte)(0), "") ;
         AV163ArqN[1-1] = AV140Short ;
         AV162ArqD1[1-1] = GXutil.now(true, false) ;
         AV166NumRe = 0 ;
      }
      else
      {
         AV140Short = "Prv" + GXutil.trim( AV193DataA) + AV28HoraB + "_i41VI" ;
         AV152RcptN = "Prv" + AV13FileNa + GXutil.trim( AV193DataA) + AV28HoraB + "_i42VI.txt" ;
         AV13FileNa = AV13FileNa + AV140Short + ".txt" ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV13FileNa, AV64Sep, "", (byte)(0), "") ;
         AV163ArqN[1-1] = AV140Short ;
         AV162ArqD1[1-1] = GXutil.now(true, false) ;
         AV166NumRe = 0 ;
      }
      context.msgStatus( "  Creating file "+AV13FileNa );
      AV149OldEm = "" ;
      AV110OldTr = (byte)(0) ;
      AV109trnTy = (byte)(0) ;
      AV216OldLc = "" ;
      /* Execute user subroutine: S131 */
      S131 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      /* Using cursor P006Q2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1228lccbF = P006Q2_A1228lccbF[0] ;
         A1227lccbO = P006Q2_A1227lccbO[0] ;
         A1226lccbA = P006Q2_A1226lccbA[0] ;
         A1225lccbC = P006Q2_A1225lccbC[0] ;
         A1223lccbD = P006Q2_A1223lccbD[0] ;
         A1222lccbI = P006Q2_A1222lccbI[0] ;
         A1150lccbE = P006Q2_A1150lccbE[0] ;
         A1224lccbC = P006Q2_A1224lccbC[0] ;
         A1147lccbE = P006Q2_A1147lccbE[0] ;
         n1147lccbE = P006Q2_n1147lccbE[0] ;
         A1172lccbS = P006Q2_A1172lccbS[0] ;
         n1172lccbS = P006Q2_n1172lccbS[0] ;
         A1490Distr = P006Q2_A1490Distr[0] ;
         n1490Distr = P006Q2_n1490Distr[0] ;
         A1184lccbS = P006Q2_A1184lccbS[0] ;
         n1184lccbS = P006Q2_n1184lccbS[0] ;
         A1167lccbG = P006Q2_A1167lccbG[0] ;
         n1167lccbG = P006Q2_n1167lccbG[0] ;
         A1179lccbV = P006Q2_A1179lccbV[0] ;
         n1179lccbV = P006Q2_n1179lccbV[0] ;
         A1170lccbI = P006Q2_A1170lccbI[0] ;
         n1170lccbI = P006Q2_n1170lccbI[0] ;
         A1171lccbT = P006Q2_A1171lccbT[0] ;
         n1171lccbT = P006Q2_n1171lccbT[0] ;
         A1192lccbS = P006Q2_A1192lccbS[0] ;
         n1192lccbS = P006Q2_n1192lccbS[0] ;
         A1190lccbS = P006Q2_A1190lccbS[0] ;
         n1190lccbS = P006Q2_n1190lccbS[0] ;
         A1191lccbS = P006Q2_A1191lccbS[0] ;
         n1191lccbS = P006Q2_n1191lccbS[0] ;
         A1185lccbB = P006Q2_A1185lccbB[0] ;
         n1185lccbB = P006Q2_n1185lccbB[0] ;
         A1193lccbS = P006Q2_A1193lccbS[0] ;
         n1193lccbS = P006Q2_n1193lccbS[0] ;
         A1194lccbS = P006Q2_A1194lccbS[0] ;
         n1194lccbS = P006Q2_n1194lccbS[0] ;
         A1195lccbS = P006Q2_A1195lccbS[0] ;
         n1195lccbS = P006Q2_n1195lccbS[0] ;
         A1189lccbS = P006Q2_A1189lccbS[0] ;
         n1189lccbS = P006Q2_n1189lccbS[0] ;
         A1168lccbI = P006Q2_A1168lccbI[0] ;
         n1168lccbI = P006Q2_n1168lccbI[0] ;
         A1147lccbE = P006Q2_A1147lccbE[0] ;
         n1147lccbE = P006Q2_n1147lccbE[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "C") == 0 ) )
         {
            if ( ( A1172lccbS > 0.00 ) )
            {
               if ( ( GXutil.strcmp(A1147lccbE, "1") == 0 ) )
               {
                  if ( ( A1168lccbI > 1 ) )
                  {
                     AV109trnTy = (byte)(2) ;
                  }
                  else
                  {
                     AV109trnTy = (byte)(1) ;
                  }
                  if ( ( GXutil.strcmp(AV149OldEm, A1150lccbE) != 0 ) || ( AV109trnTy != AV110OldTr ) || ( GXutil.strcmp(AV216OldLc, A1224lccbC) != 0 ) )
                  {
                     if ( ( GXutil.strcmp(AV149OldEm, "") != 0 ) || ( AV110OldTr != 0 ) || ( GXutil.strcmp(AV216OldLc, "") != 0 ) )
                     {
                        /* Execute user subroutine: S142 */
                        S142 ();
                        if ( returnInSub )
                        {
                           pr_default.close(0);
                           pr_default.close(0);
                           returnInSub = true;
                           if (true) return;
                        }
                     }
                     AV149OldEm = A1150lccbE ;
                     AV110OldTr = AV109trnTy ;
                     AV216OldLc = A1224lccbC ;
                     AV221GXLvl = (byte)(0) ;
                     /* Using cursor P006Q3 */
                     pr_default.execute(1, new Object[] {AV149OldEm});
                     while ( (pr_default.getStatus(1) != 101) )
                     {
                        A1488ICSI_ = P006Q3_A1488ICSI_[0] ;
                        A1487ICSI_ = P006Q3_A1487ICSI_[0] ;
                        A1480ICSI_ = P006Q3_A1480ICSI_[0] ;
                        n1480ICSI_ = P006Q3_n1480ICSI_[0] ;
                        A1481ICSI_ = P006Q3_A1481ICSI_[0] ;
                        n1481ICSI_ = P006Q3_n1481ICSI_[0] ;
                        AV221GXLvl = (byte)(1) ;
                        AV150ICSI_ = GXutil.trim( A1480ICSI_) ;
                        AV151ICSI_ = GXutil.trim( A1481ICSI_) ;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(1);
                     if ( ( AV221GXLvl == 0 ) )
                     {
                        AV150ICSI_ = "" ;
                        AV151ICSI_ = "" ;
                        context.msgStatus( "    ERROR: POS number undefined" );
                     }
                     if ( ( GXutil.strcmp(AV149OldEm, A1150lccbE) != 0 ) )
                     {
                        context.msgStatus( "  Creating Submission records for "+A1150lccbE );
                     }
                     AV190Total = 0 ;
                     /* Execute user subroutine: S152 */
                     S152 ();
                     if ( returnInSub )
                     {
                        pr_default.close(0);
                        pr_default.close(0);
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  AV144lccbE = A1150lccbE ;
                  AV130lccbI = A1222lccbI ;
                  AV145lccbD = A1223lccbD ;
                  if ( ( GXutil.strcmp(AV207UsaNo, "Y") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(A1224lccbC, AV208Bande) == 0 ) )
                     {
                        AV146lccbC = AV209NovaB ;
                     }
                     else if ( ( GXutil.strcmp(A1224lccbC, AV211Bande) == 0 ) )
                     {
                        AV146lccbC = AV210NovaB ;
                     }
                     else
                     {
                        AV146lccbC = A1224lccbC ;
                     }
                  }
                  else
                  {
                     AV146lccbC = A1224lccbC ;
                  }
                  AV194Atrib = A1225lccbC ;
                  GXv_svchar3[0] = AV195Resul ;
                  new pcrypto(remoteHandle, context).execute( AV194Atrib, "D", GXv_svchar3) ;
                  pretsubcielo.this.AV195Resul = GXv_svchar3[0] ;
                  AV137lccbC = AV195Resul ;
                  AV136lccbA = GXutil.left( A1226lccbA, 6) ;
                  AV147lccbO = A1227lccbO ;
                  AV148lccbF = A1228lccbF ;
                  AV114lccbI = A1168lccbI ;
                  AV188lccbV = A1179lccbV ;
                  AV160VlrEn = 0 ;
                  AV161Txt = GXutil.substring( GXutil.str( A1170lccbI, 14, 2), 1, 11) + GXutil.substring( GXutil.str( A1170lccbI, 14, 2), 13, 2) ;
                  AV174ValIn = (long)(GXutil.val( AV161Txt, ".")) ;
                  AV133lccbI = GXutil.val( AV161Txt, ".") ;
                  AV143lccbD = 0 ;
                  AV161Txt = GXutil.substring( GXutil.str( A1171lccbT, 14, 2), 1, 11) + GXutil.substring( GXutil.str( A1171lccbT, 14, 2), 13, 2) ;
                  AV176ValLc = (int)(GXutil.val( AV161Txt, ".")) ;
                  AV134lccbT = GXutil.val( AV161Txt, ".") ;
                  AV135lccbS = 0.00 ;
                  AV179ValSa = (long)((AV176ValLc+AV175ValDo+AV174ValIn)) ;
                  AV181ValRe = (long)(AV181ValRe+(AV174ValIn*AV114lccbI+AV175ValDo+AV176ValLc)) ;
                  AV135lccbS = A1172lccbS ;
                  AV179ValSa = (long)(A1172lccbS) ;
                  GX_I = 1 ;
                  while ( ( GX_I <= 5 ) )
                  {
                     AV118lccbT[GX_I-1] = "" ;
                     GX_I = (int)(GX_I+1) ;
                  }
                  GX_I = 1 ;
                  while ( ( GX_I <= 4 ) )
                  {
                     AV117lccbP[GX_I-1] = "" ;
                     GX_I = (int)(GX_I+1) ;
                  }
                  AV142i = 1 ;
                  AV214bilhe = "" ;
                  /* Using cursor P006Q4 */
                  pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A1231lccbT = P006Q4_A1231lccbT[0] ;
                     A1207lccbP = P006Q4_A1207lccbP[0] ;
                     n1207lccbP = P006Q4_n1207lccbP[0] ;
                     A1232lccbT = P006Q4_A1232lccbT[0] ;
                     AV118lccbT[AV142i-1] = A1231lccbT ;
                     GXt_char2 = AV111s ;
                     GXv_svchar3[0] = A1207lccbP ;
                     GXv_char4[0] = " " ;
                     GXv_int5[0] = (short)(40) ;
                     GXv_char6[0] = "R" ;
                     GXv_char7[0] = GXt_char2 ;
                     new pr2strset(remoteHandle, context).execute( GXv_svchar3, GXv_char4, GXv_int5, GXv_char6, GXv_char7) ;
                     pretsubcielo.this.A1207lccbP = GXv_svchar3[0] ;
                     pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
                     AV111s = GXt_char2 ;
                     AV117lccbP[AV142i-1] = AV111s ;
                     if ( ( AV142i == 1 ) )
                     {
                        AV132lccbT = A1232lccbT ;
                        AV214bilhe = GXutil.trim( AV215ICSI_) + GXutil.trim( A1231lccbT) ;
                     }
                     AV142i = (int)(AV142i+1) ;
                     if ( ( AV142i > 4 ) )
                     {
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
                  AV214bilhe = GXutil.padl( GXutil.trim( AV214bilhe), (short)(15), "0") ;
                  /* Execute user subroutine: S162 */
                  S162 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
                  if ( ( AV108TestM == 0 ) )
                  {
                     A1184lccbS = "PROCPLP" ;
                     n1184lccbS = false ;
                     /*
                        INSERT RECORD ON TABLE LCCBPLP1

                     */
                     A1229lccbS = GXutil.now(true, false) ;
                     A1186lccbS = "PROCPLP" ;
                     n1186lccbS = false ;
                     A1187lccbS = "Visa - Submiss�o realizada" ;
                     n1187lccbS = false ;
                     /* Using cursor P006Q5 */
                     pr_default.execute(3, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                     if ( (pr_default.getStatus(3) == 1) )
                     {
                        Gx_err = (short)(1) ;
                        Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                     }
                     else
                     {
                        Gx_err = (short)(0) ;
                        Gx_emsg = "" ;
                     }
                     /* End Insert */
                  }
                  A1189lccbS = Gx_date ;
                  n1189lccbS = false ;
                  A1192lccbS = GXutil.substring( AV140Short, 1, 20) ;
                  n1192lccbS = false ;
                  A1190lccbS = GXutil.now(true, false) ;
                  n1190lccbS = false ;
                  A1191lccbS = "F" ;
                  n1191lccbS = false ;
                  AV177LccbB = GXutil.trim( AV112Resum) ;
                  A1185lccbB = GXutil.substring( AV177LccbB, 1, 20) ;
                  n1185lccbB = false ;
                  A1193lccbS = GXutil.substring( AV141Refer, 1, 20) ;
                  n1193lccbS = false ;
                  A1194lccbS = GXutil.substring( AV112Resum, 1, 10) ;
                  n1194lccbS = false ;
                  if ( ( AV109trnTy == 1 ) )
                  {
                     A1195lccbS = GXutil.trim( AV150ICSI_) ;
                     n1195lccbS = false ;
                  }
                  else
                  {
                     A1195lccbS = GXutil.trim( AV151ICSI_) ;
                     n1195lccbS = false ;
                  }
                  /* Using cursor P006Q6 */
                  pr_default.execute(4, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1192lccbS), A1192lccbS, new Boolean(n1190lccbS), A1190lccbS, new Boolean(n1191lccbS), A1191lccbS, new Boolean(n1185lccbB), A1185lccbB, new Boolean(n1193lccbS), A1193lccbS, new Boolean(n1194lccbS), A1194lccbS, new Boolean(n1195lccbS), A1195lccbS, new Boolean(n1189lccbS), A1189lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               }
            }
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( ( GXutil.strcmp(AV149OldEm, "") != 0 ) || ( AV110OldTr != 0 ) || ( GXutil.strcmp(AV216OldLc, "") != 0 ) )
      {
         /* Execute user subroutine: S142 */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      GXt_char2 = AV212Proce ;
      GXv_char7[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "PROCESSA_SCHECK", "Verifica se processa o sanity check", "S", "N", GXv_char7) ;
      pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
      AV212Proce = GXt_char2 ;
      if ( ( GXutil.strcmp(AV212Proce, "Y") == 0 ) )
      {
         context.msgStatus( "Sanity Check " );
         GXv_char7[0] = AV213msgEr ;
         new psanitycheckcielo(remoteHandle, context).execute( AV13FileNa, GXv_char7) ;
         pretsubcielo.this.AV213msgEr = GXv_char7[0] ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( AV213msgEr))==0)) )
         {
            /* Execute user subroutine: S171 */
            S171 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV223Lccbs = "NOSUB" ;
         }
         else
         {
            context.msgStatus( AV213msgEr );
            AV223Lccbs = "NOSUB" ;
         }
      }
      AV164ArqTR[1-1] = AV166NumRe ;
      AV168SCEAi = AV149OldEm ;
      AV13FileNa = AV140Short ;
      if ( ( AV157tSale > 0 ) )
      {
         AV169SCETe = "Total de Vendas A Vista|" + GXutil.trim( GXutil.str( AV157tSale, 10, 0)) ;
         AV169SCETe = AV169SCETe + "|Total de Taxas A Vista|" + GXutil.trim( GXutil.str( AV170tTipC, 10, 0)) ;
         /* Execute user subroutine: S181 */
         S181 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      if ( ( AV158tSale > 0 ) )
      {
         AV169SCETe = "Total de Vendas A Prazo|" + GXutil.trim( GXutil.str( AV158tSale, 10, 0)) ;
         AV169SCETe = AV169SCETe + "|Total de Taxas A Prazo|" + GXutil.trim( GXutil.str( AV171tTipP, 10, 0)) ;
         AV169SCETe = AV169SCETe + "|Total de Entradas A Prazo|" + GXutil.trim( GXutil.str( AV172tDown, 10, 0)) ;
         /* Execute user subroutine: S181 */
         S181 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      context.msgStatus( "Records        Amount" );
      context.msgStatus( "Total CC  = "+GXutil.str( AV154cntCC, 8, 0)+"  "+GXutil.str( AV157tSale, 12, 2) );
      context.msgStatus( "Total PLP = "+GXutil.str( AV155cntPL, 8, 0)+"  "+GXutil.str( AV158tSale, 12, 2) );
      /* Using cursor P006Q7 */
      pr_default.execute(5);
      while ( (pr_default.getStatus(5) != 101) )
      {
         A1228lccbF = P006Q7_A1228lccbF[0] ;
         A1227lccbO = P006Q7_A1227lccbO[0] ;
         A1226lccbA = P006Q7_A1226lccbA[0] ;
         A1225lccbC = P006Q7_A1225lccbC[0] ;
         A1224lccbC = P006Q7_A1224lccbC[0] ;
         A1223lccbD = P006Q7_A1223lccbD[0] ;
         A1222lccbI = P006Q7_A1222lccbI[0] ;
         A1150lccbE = P006Q7_A1150lccbE[0] ;
         A1172lccbS = P006Q7_A1172lccbS[0] ;
         n1172lccbS = P006Q7_n1172lccbS[0] ;
         A1490Distr = P006Q7_A1490Distr[0] ;
         n1490Distr = P006Q7_n1490Distr[0] ;
         A1184lccbS = P006Q7_A1184lccbS[0] ;
         n1184lccbS = P006Q7_n1184lccbS[0] ;
         A1167lccbG = P006Q7_A1167lccbG[0] ;
         n1167lccbG = P006Q7_n1167lccbG[0] ;
         A1189lccbS = P006Q7_A1189lccbS[0] ;
         n1189lccbS = P006Q7_n1189lccbS[0] ;
         A1168lccbI = P006Q7_A1168lccbI[0] ;
         n1168lccbI = P006Q7_n1168lccbI[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "C") == 0 ) )
         {
            if ( ( A1172lccbS <= 0.00 ) )
            {
               if ( ( AV108TestM == 0 ) )
               {
                  A1184lccbS = AV223Lccbs ;
                  n1184lccbS = false ;
                  /*
                     INSERT RECORD ON TABLE LCCBPLP1

                  */
                  A1229lccbS = GXutil.now(true, false) ;
                  A1186lccbS = AV223Lccbs ;
                  n1186lccbS = false ;
                  A1187lccbS = "Visa - n�o submetido" ;
                  n1187lccbS = false ;
                  /* Using cursor P006Q8 */
                  pr_default.execute(6, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                  if ( (pr_default.getStatus(6) == 1) )
                  {
                     Gx_err = (short)(1) ;
                     Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                  }
                  else
                  {
                     Gx_err = (short)(0) ;
                     Gx_emsg = "" ;
                  }
                  /* End Insert */
               }
               /* Using cursor P006Q9 */
               pr_default.execute(7, new Object[] {new Boolean(n1184lccbS), A1184lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
            }
         }
         pr_default.readNext(5);
      }
      pr_default.close(5);
   }

   public void S131( )
   {
      /* 'BUSCA_CONFIGURACOES' Routine */
      AV115Total = 0 ;
      AV116Total = 0 ;
      AV153SeqFi = 0 ;
      AV154cntCC = 0 ;
      AV155cntPL = 0 ;
      AV157tSale = 0 ;
      AV158tSale = 0 ;
      AV225GXLvl = (byte)(0) ;
      /* Using cursor P006Q10 */
      pr_default.execute(8);
      while ( (pr_default.getStatus(8) != 101) )
      {
         A1488ICSI_ = P006Q10_A1488ICSI_[0] ;
         A1487ICSI_ = P006Q10_A1487ICSI_[0] ;
         A1484ICSI_ = P006Q10_A1484ICSI_[0] ;
         n1484ICSI_ = P006Q10_n1484ICSI_[0] ;
         A1485ICSI_ = P006Q10_A1485ICSI_[0] ;
         n1485ICSI_ = P006Q10_n1485ICSI_[0] ;
         A1479ICSI_ = P006Q10_A1479ICSI_[0] ;
         n1479ICSI_ = P006Q10_n1479ICSI_[0] ;
         AV225GXLvl = (byte)(1) ;
         AV138ICSI_ = GXutil.trim( A1484ICSI_) ;
         AV139ICSI_ = GXutil.trim( A1485ICSI_) ;
         if ( ( AV108TestM == 0 ) )
         {
            A1479ICSI_ = (int)(A1479ICSI_+1) ;
            n1479ICSI_ = false ;
         }
         AV32ICSI_C = A1479ICSI_ ;
         context.msgStatus( "  Info: File sequence="+GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0))+", Batch="+GXutil.trim( GXutil.str( AV30ICSI_C, 10, 0)) );
         /* Using cursor P006Q11 */
         pr_default.execute(9, new Object[] {new Boolean(n1479ICSI_), new Integer(A1479ICSI_), A1487ICSI_, A1488ICSI_});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(8);
      if ( ( AV225GXLvl == 0 ) )
      {
         AV138ICSI_ = "0" ;
         AV139ICSI_ = "R2" ;
         AV32ICSI_C = 0 ;
         AV30ICSI_C = 0 ;
         /*
            INSERT RECORD ON TABLE ICSI_CCINFO

         */
         A1487ICSI_ = "000" ;
         A1488ICSI_ = "VI" ;
         A1484ICSI_ = AV138ICSI_ ;
         n1484ICSI_ = false ;
         A1485ICSI_ = AV139ICSI_ ;
         n1485ICSI_ = false ;
         A1479ICSI_ = AV32ICSI_C ;
         n1479ICSI_ = false ;
         A1478ICSI_ = AV30ICSI_C ;
         n1478ICSI_ = false ;
         /* Using cursor P006Q12 */
         pr_default.execute(10, new Object[] {A1487ICSI_, A1488ICSI_, new Boolean(n1478ICSI_), new Integer(A1478ICSI_), new Boolean(n1479ICSI_), new Integer(A1479ICSI_), new Boolean(n1484ICSI_), A1484ICSI_, new Boolean(n1485ICSI_), A1485ICSI_});
         if ( (pr_default.getStatus(10) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         context.msgStatus( "  ERROR: VISA not configured" );
      }
   }

   public void S152( )
   {
      /* 'GERA_REGISTRO_00' Routine */
      AV113SeqRe = 0 ;
      AV191QtdLi = 0 ;
      AV190Total = 0 ;
      AV189Total = 0 ;
      AV226GXLvl = (byte)(0) ;
      /* Using cursor P006Q13 */
      pr_default.execute(11);
      while ( (pr_default.getStatus(11) != 101) )
      {
         A1488ICSI_ = P006Q13_A1488ICSI_[0] ;
         A1487ICSI_ = P006Q13_A1487ICSI_[0] ;
         A1478ICSI_ = P006Q13_A1478ICSI_[0] ;
         n1478ICSI_ = P006Q13_n1478ICSI_[0] ;
         AV226GXLvl = (byte)(1) ;
         if ( ( AV108TestM == 0 ) )
         {
            A1478ICSI_ = (int)(A1478ICSI_+1) ;
            n1478ICSI_ = false ;
         }
         AV30ICSI_C = A1478ICSI_ ;
         context.msgStatus( "  Info: New batch="+GXutil.trim( GXutil.str( AV30ICSI_C, 10, 0)) );
         /* Using cursor P006Q14 */
         pr_default.execute(12, new Object[] {new Boolean(n1478ICSI_), new Integer(A1478ICSI_), A1487ICSI_, A1488ICSI_});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(11);
      if ( ( AV226GXLvl == 0 ) )
      {
         context.msgStatus( "  ERROR: VISA configuration missing!" );
      }
      if ( ( AV109trnTy == 1 ) )
      {
         AV156ICSI_ = GXutil.trim( AV150ICSI_) ;
      }
      else
      {
         AV156ICSI_ = GXutil.trim( AV151ICSI_) ;
      }
      AV191QtdLi = (int)(AV191QtdLi+1) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00", (short)(2)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV9DataDep, (short)(8)) ;
      GXt_char2 = AV112Resum ;
      GXv_int8[0] = AV30ICSI_C ;
      GXv_int9[0] = (byte)(7) ;
      GXv_int10[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char2 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int9, GXv_int10, GXv_char7) ;
      pretsubcielo.this.AV30ICSI_C = (int)((int)(GXv_int8[0])) ;
      pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
      AV112Resum = GXt_char2 ;
      AV111s = GXutil.right( AV112Resum, 7) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV111s, (short)(7)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( " ", (short)(10)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000", (short)(3)) ;
      GXt_char2 = AV111s ;
      GXv_char7[0] = AV156ICSI_ ;
      GXv_char6[0] = "0" ;
      GXv_int5[0] = (short)(10) ;
      GXv_char4[0] = "L" ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar3) ;
      pretsubcielo.this.AV156ICSI_ = GXv_char7[0] ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV111s = GXt_char2 ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV111s, (short)(10)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "986", (short)(3)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "P", (short)(1)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "V", (short)(1)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "", (short)(205)) ;
      AV166NumRe = (long)(AV166NumRe+1) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
   }

   public void S162( )
   {
      /* 'GERA_REGISTRO_01' Routine */
      AV113SeqRe = (int)(AV113SeqRe+1) ;
      AV153SeqFi = (int)(AV153SeqFi+1) ;
      AV191QtdLi = (int)(AV191QtdLi+1) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "01", (short)(2)) ;
      GXt_char2 = AV111s ;
      GXv_int8[0] = AV113SeqRe ;
      GXv_int10[0] = (byte)(7) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char2 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      pretsubcielo.this.AV113SeqRe = (int)((int)(GXv_int8[0])) ;
      pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
      AV111s = GXt_char2 ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV111s, (short)(7)) ;
      GXt_char2 = AV111s ;
      GXv_char7[0] = AV137lccbC ;
      GXv_char6[0] = GXt_char2 ;
      new pr2onlynumbers(remoteHandle, context).execute( GXv_char7, GXv_char6) ;
      pretsubcielo.this.AV137lccbC = GXv_char7[0] ;
      pretsubcielo.this.GXt_char2 = GXv_char6[0] ;
      AV111s = GXt_char2 ;
      GXt_char2 = AV111s ;
      GXv_char7[0] = AV111s ;
      GXv_char6[0] = "0" ;
      GXv_int5[0] = (short)(19) ;
      GXv_char4[0] = "L" ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar3) ;
      pretsubcielo.this.AV111s = GXv_char7[0] ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV111s = GXt_char2 ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV111s, (short)(19)) ;
      AV159Num06 = AV136lccbA ;
      AV161Txt = GXutil.rtrim( localUtil.format( AV159Num06, "XXXXXX")) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV161Txt, (short)(6)) ;
      AV183DataC = GXutil.trim( GXutil.str( GXutil.year( AV145lccbD), 10, 0)) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( AV145lccbD)+100, 10, 0)), 2, 3) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( AV145lccbD)+100, 10, 0)), 2, 3) ;
      AV182DataV = GXutil.substring( AV183DataC, 7, 2) + GXutil.substring( AV183DataC, 5, 2) + GXutil.substring( AV183DataC, 1, 4) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV182DataV, (short)(2)) ;
      if ( ( AV109trnTy == 2 ) )
      {
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "4", (short)(1)) ;
         AV155cntPL = (int)(AV155cntPL+1) ;
      }
      else
      {
         AV154cntCC = (int)(AV154cntCC+1) ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "0", (short)(1)) ;
      }
      if ( ( AV109trnTy == 2 ) )
      {
         AV184Total = (long)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(A1172lccbS), 2).multiply(DecimalUtil.doubleToDec(100)))) ;
         AV190Total = A1172lccbS ;
         AV158tSale = (double)(AV158tSale+AV190Total) ;
         AV185Valor = localUtil.format( AV184Total, "999999999999999") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      }
      else
      {
         AV157tSale = (double)(AV157tSale+A1172lccbS) ;
         AV184Total = (long)(A1172lccbS) ;
         AV190Total = A1172lccbS ;
         AV185Valor = GXutil.strReplace( GXutil.strReplace( localUtil.format( A1172lccbS, "ZZ,ZZZ,ZZZ,ZZ9.99"), ".", ""), ",", "") ;
         AV185Valor = GXutil.padl( AV185Valor, (short)(15), "0") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      }
      if ( ( AV109trnTy == 2 ) )
      {
         AV186QtdPa = AV114lccbI ;
         AV187QtdCa = localUtil.format( AV186QtdPa, "999") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV187QtdCa, (short)(3)) ;
      }
      else
      {
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000", (short)(3)) ;
      }
      if ( ( AV109trnTy == 2 ) )
      {
         AV184Total = (long)((AV114lccbI*AV133lccbI)) ;
         AV185Valor = localUtil.format( AV184Total, "999999999999999") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      }
      else
      {
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000000000000", (short)(15)) ;
      }
      if ( ( AV109trnTy == 2 ) )
      {
         AV184Total = 0 ;
         AV185Valor = localUtil.format( AV184Total, "999999999999999") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      }
      else
      {
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000000000000", (short)(15)) ;
      }
      if ( ( AV109trnTy == 2 ) )
      {
         AV184Total = (long)(AV134lccbT) ;
         AV185Valor = localUtil.format( AV184Total, "999999999999999") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      }
      else
      {
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000000000000", (short)(15)) ;
      }
      if ( ( AV109trnTy == 2 ) )
      {
         AV184Total = (long)(AV133lccbI) ;
         AV185Valor = localUtil.format( AV184Total, "999999999999999") ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      }
      else
      {
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000000000000", (short)(15)) ;
      }
      GXt_char2 = AV112Resum ;
      GXv_int8[0] = AV30ICSI_C ;
      GXv_int10[0] = (byte)(7) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char2 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      pretsubcielo.this.AV30ICSI_C = (int)((int)(GXv_int8[0])) ;
      pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
      AV112Resum = GXt_char2 ;
      AV111s = GXutil.right( AV112Resum, 7) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV111s, (short)(7)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000", (short)(3)) ;
      GXt_char2 = AV111s ;
      GXv_char7[0] = AV156ICSI_ ;
      GXv_char6[0] = "0" ;
      GXv_int5[0] = (short)(10) ;
      GXv_char4[0] = "L" ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2strset(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_int5, GXv_char4, GXv_svchar3) ;
      pretsubcielo.this.AV156ICSI_ = GXv_char7[0] ;
      pretsubcielo.this.GXt_char2 = GXv_svchar3[0] ;
      AV111s = GXt_char2 ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV111s, (short)(10)) ;
      GXt_char2 = AV111s ;
      GXv_int8[0] = AV113SeqRe ;
      GXv_int10[0] = (byte)(7) ;
      GXv_int9[0] = (byte)(0) ;
      GXv_char7[0] = GXt_char2 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int10, GXv_int9, GXv_char7) ;
      pretsubcielo.this.AV113SeqRe = (int)((int)(GXv_int8[0])) ;
      pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
      AV111s = GXt_char2 ;
      AV141Refer = GXutil.trim( AV146lccbC) + AV112Resum + GXutil.right( AV111s, 7) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV141Refer, (short)(16)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( " ", (short)(14)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00", (short)(2)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00000000", (short)(8)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV188lccbV, (short)(1)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "0000000", (short)(7)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV214bilhe, (short)(15)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "   ", (short)(3)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "    ", (short)(4)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(36)), (short)(36)) ;
      AV166NumRe = (long)(AV166NumRe+1) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      AV189Total = (double)(AV189Total+AV190Total) ;
   }

   public void S142( )
   {
      /* 'GERA_REGISTRO_99' Routine */
      AV191QtdLi = (int)(AV191QtdLi+1) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "99", (short)(2)) ;
      AV192QtdCa = localUtil.format( AV191QtdLi, "9999999") ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV192QtdCa, (short)(7)) ;
      AV185Valor = GXutil.strReplace( GXutil.strReplace( localUtil.format( AV189Total, "999999999999.99"), ".", ""), ",", "") ;
      AV185Valor = GXutil.padl( AV185Valor, (short)(15), "0") ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV185Valor, (short)(15)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000000000000", (short)(15)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000000000000", (short)(15)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "00000000", (short)(8)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( "000000", (short)(6)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.space( (short)(182)), (short)(182)) ;
      AV166NumRe = (long)(AV166NumRe+1) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
   }

   public void S181( )
   {
      /* 'LOG' Routine */
      /*
         INSERT RECORD ON TABLE SCEVENTS

      */
      A1301SCEAi = GXutil.substring( AV168SCEAi, 1, 3) ;
      n1301SCEAi = false ;
      A1303SCERE = GXutil.substring( AV13FileNa, 1, 50) ;
      n1303SCERE = false ;
      A1305SCETe = GXutil.substring( AV169SCETe, 1, 150) ;
      n1305SCETe = false ;
      A1311SCEVe = GXutil.substring( AV84Versao, 1, 5) ;
      n1311SCEVe = false ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.now(true, false) ;
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "VI" ;
      n1310SCETp = false ;
      A1306SCEAP = "SUB" ;
      n1306SCEAP = false ;
      /* Using cursor P006Q15 */
      pr_default.execute(13, new Object[] {new Boolean(n1298SCETk), A1298SCETk, new Boolean(n1299SCEDa), A1299SCEDa, new Boolean(n1300SCECR), A1300SCECR, new Boolean(n1301SCEAi), A1301SCEAi, new Boolean(n1303SCERE), A1303SCERE, new Boolean(n1305SCETe), A1305SCETe, new Boolean(n1306SCEAP), A1306SCEAP, new Boolean(n1307SCEIa), A1307SCEIa, new Boolean(n1310SCETp), A1310SCETp, new Boolean(n1311SCEVe), A1311SCEVe});
      /* Retrieving last key number assigned */
      /* Using cursor P006Q16 */
      pr_default.execute(14);
      A1312SCEId = P006Q16_A1312SCEId[0] ;
      n1312SCEId = P006Q16_n1312SCEId[0] ;
      pr_default.close(14);
      if ( (pr_default.getStatus(13) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      if ( ( GXutil.strSearch( AV10DebugM, "VERBOSE", 1) > 0 ) )
      {
         context.msgStatus( "    "+A1305SCETe );
      }
   }

   public void S121( )
   {
      /* 'AUDITTRAIL' Routine */
      AV197Audit = "Localhost" ;
      AV198Audit = "TIES_ICSI" ;
      AV199Audit = "PCI" ;
      GXv_char7[0] = AV197Audit ;
      GXv_char6[0] = AV198Audit ;
      GXv_char4[0] = AV199Audit ;
      GXv_svchar3[0] = AV200Audit ;
      new pnewaudit(remoteHandle, context).execute( GXv_char7, GXv_char6, GXv_char4, GXv_svchar3) ;
      pretsubcielo.this.AV197Audit = GXv_char7[0] ;
      pretsubcielo.this.AV198Audit = GXv_char6[0] ;
      pretsubcielo.this.AV199Audit = GXv_char4[0] ;
      pretsubcielo.this.AV200Audit = GXv_svchar3[0] ;
   }

   public void S171( )
   {
      /* 'COPIA_ARQUIVO_NA_PASTA_DA_TIVIT' Routine */
      GXt_char2 = AV201PathC ;
      GXv_char7[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "PATHCOPSUBVI", "Caminho C�pia Submiss�o Visa", "F", "C:\\R2Tech\\Sistemas\\ICSI\\TIVIT\\VI", GXv_char7) ;
      pretsubcielo.this.GXt_char2 = GXv_char7[0] ;
      AV201PathC = GXt_char2 ;
      AV204Diret.setSource( GXutil.trim( AV201PathC) );
      if ( AV204Diret.exists() )
      {
         AV203File.setSource( GXutil.trim( AV13FileNa) );
         GXt_char2 = AV140Short ;
         GXv_char7[0] = AV13FileNa ;
         GXv_char6[0] = GXt_char2 ;
         new pr2shortname(remoteHandle, context).execute( GXv_char7, GXv_char6) ;
         pretsubcielo.this.AV13FileNa = GXv_char7[0] ;
         pretsubcielo.this.GXt_char2 = GXv_char6[0] ;
         AV140Short = GXt_char2 ;
         AV202Filen = GXutil.trim( AV201PathC) + "\\" + GXutil.trim( AV140Short) ;
         AV203File.copy(AV202Filen);
      }
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(pretsubcielo.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = pretsubcielo.this.AV10DebugM;
      Application.commit(context, remoteHandle, "DEFAULT", "pretsubcielo");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV84Versao = "" ;
      AV64Sep = "" ;
      AV108TestM = (byte)(0) ;
      AV193DataA = "" ;
      Gx_date = GXutil.nullDate() ;
      AV9DataDep = "" ;
      AV27HoraA = "" ;
      AV28HoraB = "" ;
      returnInSub = false ;
      A1305SCETe = "" ;
      AV13FileNa = "" ;
      AV207UsaNo = "" ;
      AV208Bande = "" ;
      AV209NovaB = "" ;
      AV211Bande = "" ;
      AV210NovaB = "" ;
      AV215ICSI_ = "" ;
      AV140Short = "" ;
      AV152RcptN = "" ;
      AV200Audit = "" ;
      AV14FileNo = 0 ;
      AV163ArqN = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV163ArqN[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV162ArqD1 = new java.util.Date [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV162ArqD1[GX_I-1] = GXutil.resetTime( GXutil.nullDate() );
         GX_I = (int)(GX_I+1) ;
      }
      AV166NumRe = 0 ;
      AV149OldEm = "" ;
      AV110OldTr = (byte)(0) ;
      AV109trnTy = (byte)(0) ;
      AV216OldLc = "" ;
      scmdbuf = "" ;
      P006Q2_A1228lccbF = new String[] {""} ;
      P006Q2_A1227lccbO = new String[] {""} ;
      P006Q2_A1226lccbA = new String[] {""} ;
      P006Q2_A1225lccbC = new String[] {""} ;
      P006Q2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006Q2_A1222lccbI = new String[] {""} ;
      P006Q2_A1150lccbE = new String[] {""} ;
      P006Q2_A1224lccbC = new String[] {""} ;
      P006Q2_A1147lccbE = new String[] {""} ;
      P006Q2_n1147lccbE = new boolean[] {false} ;
      P006Q2_A1172lccbS = new double[1] ;
      P006Q2_n1172lccbS = new boolean[] {false} ;
      P006Q2_A1490Distr = new String[] {""} ;
      P006Q2_n1490Distr = new boolean[] {false} ;
      P006Q2_A1184lccbS = new String[] {""} ;
      P006Q2_n1184lccbS = new boolean[] {false} ;
      P006Q2_A1167lccbG = new String[] {""} ;
      P006Q2_n1167lccbG = new boolean[] {false} ;
      P006Q2_A1179lccbV = new String[] {""} ;
      P006Q2_n1179lccbV = new boolean[] {false} ;
      P006Q2_A1170lccbI = new double[1] ;
      P006Q2_n1170lccbI = new boolean[] {false} ;
      P006Q2_A1171lccbT = new double[1] ;
      P006Q2_n1171lccbT = new boolean[] {false} ;
      P006Q2_A1192lccbS = new String[] {""} ;
      P006Q2_n1192lccbS = new boolean[] {false} ;
      P006Q2_A1190lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P006Q2_n1190lccbS = new boolean[] {false} ;
      P006Q2_A1191lccbS = new String[] {""} ;
      P006Q2_n1191lccbS = new boolean[] {false} ;
      P006Q2_A1185lccbB = new String[] {""} ;
      P006Q2_n1185lccbB = new boolean[] {false} ;
      P006Q2_A1193lccbS = new String[] {""} ;
      P006Q2_n1193lccbS = new boolean[] {false} ;
      P006Q2_A1194lccbS = new String[] {""} ;
      P006Q2_n1194lccbS = new boolean[] {false} ;
      P006Q2_A1195lccbS = new String[] {""} ;
      P006Q2_n1195lccbS = new boolean[] {false} ;
      P006Q2_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P006Q2_n1189lccbS = new boolean[] {false} ;
      P006Q2_A1168lccbI = new short[1] ;
      P006Q2_n1168lccbI = new boolean[] {false} ;
      A1228lccbF = "" ;
      A1227lccbO = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1150lccbE = "" ;
      A1224lccbC = "" ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1167lccbG = "" ;
      n1167lccbG = false ;
      A1179lccbV = "" ;
      n1179lccbV = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1192lccbS = "" ;
      n1192lccbS = false ;
      A1190lccbS = GXutil.resetTime( GXutil.nullDate() );
      n1190lccbS = false ;
      A1191lccbS = "" ;
      n1191lccbS = false ;
      A1185lccbB = "" ;
      n1185lccbB = false ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1195lccbS = "" ;
      n1195lccbS = false ;
      A1189lccbS = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      AV221GXLvl = (byte)(0) ;
      P006Q3_A1488ICSI_ = new String[] {""} ;
      P006Q3_A1487ICSI_ = new String[] {""} ;
      P006Q3_A1480ICSI_ = new String[] {""} ;
      P006Q3_n1480ICSI_ = new boolean[] {false} ;
      P006Q3_A1481ICSI_ = new String[] {""} ;
      P006Q3_n1481ICSI_ = new boolean[] {false} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      A1480ICSI_ = "" ;
      n1480ICSI_ = false ;
      A1481ICSI_ = "" ;
      n1481ICSI_ = false ;
      AV150ICSI_ = "" ;
      AV151ICSI_ = "" ;
      AV190Total = 0 ;
      AV144lccbE = "" ;
      AV130lccbI = "" ;
      AV145lccbD = GXutil.nullDate() ;
      AV146lccbC = "" ;
      AV194Atrib = "" ;
      AV195Resul = "" ;
      AV137lccbC = "" ;
      AV136lccbA = "" ;
      AV147lccbO = "" ;
      AV148lccbF = "" ;
      AV114lccbI = (short)(0) ;
      AV188lccbV = "" ;
      AV160VlrEn = 0 ;
      AV161Txt = "" ;
      AV174ValIn = 0 ;
      AV133lccbI = 0 ;
      AV143lccbD = 0 ;
      AV176ValLc = 0 ;
      AV134lccbT = 0 ;
      AV135lccbS = 0 ;
      AV179ValSa = 0 ;
      AV175ValDo = 0 ;
      AV181ValRe = 0 ;
      GX_I = 0 ;
      AV118lccbT = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV118lccbT[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV117lccbP = new String [4] ;
      GX_I = 1 ;
      while ( ( GX_I <= 4 ) )
      {
         AV117lccbP[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV142i = 0 ;
      AV214bilhe = "" ;
      P006Q4_A1150lccbE = new String[] {""} ;
      P006Q4_A1222lccbI = new String[] {""} ;
      P006Q4_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006Q4_A1224lccbC = new String[] {""} ;
      P006Q4_A1225lccbC = new String[] {""} ;
      P006Q4_A1226lccbA = new String[] {""} ;
      P006Q4_A1227lccbO = new String[] {""} ;
      P006Q4_A1228lccbF = new String[] {""} ;
      P006Q4_A1231lccbT = new String[] {""} ;
      P006Q4_A1207lccbP = new String[] {""} ;
      P006Q4_n1207lccbP = new boolean[] {false} ;
      P006Q4_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1207lccbP = "" ;
      n1207lccbP = false ;
      A1232lccbT = "" ;
      AV111s = "" ;
      AV132lccbT = "" ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV177LccbB = "" ;
      AV112Resum = "" ;
      AV141Refer = "" ;
      AV212Proce = "" ;
      AV213msgEr = "" ;
      AV223Lccbs = "" ;
      AV164ArqTR = new long [99] ;
      AV168SCEAi = "" ;
      AV157tSale = 0 ;
      AV169SCETe = "" ;
      AV170tTipC = 0 ;
      AV158tSale = 0 ;
      AV171tTipP = 0 ;
      AV172tDown = 0 ;
      AV154cntCC = 0 ;
      AV155cntPL = 0 ;
      GXt_char1 = "" ;
      P006Q7_A1228lccbF = new String[] {""} ;
      P006Q7_A1227lccbO = new String[] {""} ;
      P006Q7_A1226lccbA = new String[] {""} ;
      P006Q7_A1225lccbC = new String[] {""} ;
      P006Q7_A1224lccbC = new String[] {""} ;
      P006Q7_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006Q7_A1222lccbI = new String[] {""} ;
      P006Q7_A1150lccbE = new String[] {""} ;
      P006Q7_A1172lccbS = new double[1] ;
      P006Q7_n1172lccbS = new boolean[] {false} ;
      P006Q7_A1490Distr = new String[] {""} ;
      P006Q7_n1490Distr = new boolean[] {false} ;
      P006Q7_A1184lccbS = new String[] {""} ;
      P006Q7_n1184lccbS = new boolean[] {false} ;
      P006Q7_A1167lccbG = new String[] {""} ;
      P006Q7_n1167lccbG = new boolean[] {false} ;
      P006Q7_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P006Q7_n1189lccbS = new boolean[] {false} ;
      P006Q7_A1168lccbI = new short[1] ;
      P006Q7_n1168lccbI = new boolean[] {false} ;
      AV115Total = 0 ;
      AV116Total = 0 ;
      AV153SeqFi = 0 ;
      AV225GXLvl = (byte)(0) ;
      P006Q10_A1488ICSI_ = new String[] {""} ;
      P006Q10_A1487ICSI_ = new String[] {""} ;
      P006Q10_A1484ICSI_ = new String[] {""} ;
      P006Q10_n1484ICSI_ = new boolean[] {false} ;
      P006Q10_A1485ICSI_ = new String[] {""} ;
      P006Q10_n1485ICSI_ = new boolean[] {false} ;
      P006Q10_A1479ICSI_ = new int[1] ;
      P006Q10_n1479ICSI_ = new boolean[] {false} ;
      A1484ICSI_ = "" ;
      n1484ICSI_ = false ;
      A1485ICSI_ = "" ;
      n1485ICSI_ = false ;
      A1479ICSI_ = 0 ;
      n1479ICSI_ = false ;
      AV138ICSI_ = "" ;
      AV139ICSI_ = "" ;
      AV32ICSI_C = 0 ;
      AV30ICSI_C = 0 ;
      GX_INS273 = 0 ;
      A1478ICSI_ = 0 ;
      n1478ICSI_ = false ;
      AV113SeqRe = 0 ;
      AV191QtdLi = 0 ;
      AV189Total = 0 ;
      AV226GXLvl = (byte)(0) ;
      P006Q13_A1488ICSI_ = new String[] {""} ;
      P006Q13_A1487ICSI_ = new String[] {""} ;
      P006Q13_A1478ICSI_ = new int[1] ;
      P006Q13_n1478ICSI_ = new boolean[] {false} ;
      AV156ICSI_ = "" ;
      AV159Num06 = "" ;
      AV183DataC = "" ;
      AV182DataV = "" ;
      AV184Total = 0 ;
      AV185Valor = "" ;
      AV186QtdPa = (short)(0) ;
      AV187QtdCa = "" ;
      GXv_int5 = new short [1] ;
      GXv_int8 = new double [1] ;
      GXv_int10 = new byte [1] ;
      GXv_int9 = new byte [1] ;
      AV192QtdCa = "" ;
      GX_INS248 = 0 ;
      A1301SCEAi = "" ;
      n1301SCEAi = false ;
      A1303SCERE = "" ;
      n1303SCERE = false ;
      n1305SCETe = false ;
      A1311SCEVe = "" ;
      n1311SCEVe = false ;
      A1298SCETk = "" ;
      n1298SCETk = false ;
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      n1299SCEDa = false ;
      A1300SCECR = "" ;
      n1300SCECR = false ;
      A1307SCEIa = "" ;
      n1307SCEIa = false ;
      A1310SCETp = "" ;
      n1310SCETp = false ;
      A1306SCEAP = "" ;
      n1306SCEAP = false ;
      P006Q16_A1312SCEId = new long[1] ;
      P006Q16_n1312SCEId = new boolean[] {false} ;
      A1312SCEId = 0 ;
      n1312SCEId = false ;
      AV197Audit = "" ;
      AV198Audit = "" ;
      AV199Audit = "" ;
      GXv_char4 = new String [1] ;
      GXv_svchar3 = new String [1] ;
      AV201PathC = "" ;
      AV204Diret = new com.genexus.util.GXDirectory();
      AV203File = new com.genexus.util.GXFile();
      GXt_char2 = "" ;
      GXv_char7 = new String [1] ;
      GXv_char6 = new String [1] ;
      AV202Filen = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretsubcielo__default(),
         new Object[] {
             new Object[] {
            P006Q2_A1228lccbF, P006Q2_A1227lccbO, P006Q2_A1226lccbA, P006Q2_A1225lccbC, P006Q2_A1223lccbD, P006Q2_A1222lccbI, P006Q2_A1150lccbE, P006Q2_A1224lccbC, P006Q2_A1147lccbE, P006Q2_n1147lccbE,
            P006Q2_A1172lccbS, P006Q2_n1172lccbS, P006Q2_A1490Distr, P006Q2_n1490Distr, P006Q2_A1184lccbS, P006Q2_n1184lccbS, P006Q2_A1167lccbG, P006Q2_n1167lccbG, P006Q2_A1179lccbV, P006Q2_n1179lccbV,
            P006Q2_A1170lccbI, P006Q2_n1170lccbI, P006Q2_A1171lccbT, P006Q2_n1171lccbT, P006Q2_A1192lccbS, P006Q2_n1192lccbS, P006Q2_A1190lccbS, P006Q2_n1190lccbS, P006Q2_A1191lccbS, P006Q2_n1191lccbS,
            P006Q2_A1185lccbB, P006Q2_n1185lccbB, P006Q2_A1193lccbS, P006Q2_n1193lccbS, P006Q2_A1194lccbS, P006Q2_n1194lccbS, P006Q2_A1195lccbS, P006Q2_n1195lccbS, P006Q2_A1189lccbS, P006Q2_n1189lccbS,
            P006Q2_A1168lccbI, P006Q2_n1168lccbI
            }
            , new Object[] {
            P006Q3_A1488ICSI_, P006Q3_A1487ICSI_, P006Q3_A1480ICSI_, P006Q3_n1480ICSI_, P006Q3_A1481ICSI_, P006Q3_n1481ICSI_
            }
            , new Object[] {
            P006Q4_A1150lccbE, P006Q4_A1222lccbI, P006Q4_A1223lccbD, P006Q4_A1224lccbC, P006Q4_A1225lccbC, P006Q4_A1226lccbA, P006Q4_A1227lccbO, P006Q4_A1228lccbF, P006Q4_A1231lccbT, P006Q4_A1207lccbP,
            P006Q4_n1207lccbP, P006Q4_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006Q7_A1228lccbF, P006Q7_A1227lccbO, P006Q7_A1226lccbA, P006Q7_A1225lccbC, P006Q7_A1224lccbC, P006Q7_A1223lccbD, P006Q7_A1222lccbI, P006Q7_A1150lccbE, P006Q7_A1172lccbS, P006Q7_n1172lccbS,
            P006Q7_A1490Distr, P006Q7_n1490Distr, P006Q7_A1184lccbS, P006Q7_n1184lccbS, P006Q7_A1167lccbG, P006Q7_n1167lccbG, P006Q7_A1189lccbS, P006Q7_n1189lccbS, P006Q7_A1168lccbI, P006Q7_n1168lccbI
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006Q10_A1488ICSI_, P006Q10_A1487ICSI_, P006Q10_A1484ICSI_, P006Q10_n1484ICSI_, P006Q10_A1485ICSI_, P006Q10_n1485ICSI_, P006Q10_A1479ICSI_, P006Q10_n1479ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006Q13_A1488ICSI_, P006Q13_A1487ICSI_, P006Q13_A1478ICSI_, P006Q13_n1478ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006Q16_A1312SCEId
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV108TestM ;
   private byte AV110OldTr ;
   private byte AV109trnTy ;
   private byte AV221GXLvl ;
   private byte AV225GXLvl ;
   private byte AV226GXLvl ;
   private byte GXv_int10[] ;
   private byte GXv_int9[] ;
   private short A1168lccbI ;
   private short AV114lccbI ;
   private short A1230lccbS ;
   private short Gx_err ;
   private short AV186QtdPa ;
   private short GXv_int5[] ;
   private int AV160VlrEn ;
   private int AV176ValLc ;
   private int GX_I ;
   private int AV142i ;
   private int GX_INS236 ;
   private int AV154cntCC ;
   private int AV155cntPL ;
   private int AV153SeqFi ;
   private int A1479ICSI_ ;
   private int AV32ICSI_C ;
   private int AV30ICSI_C ;
   private int GX_INS273 ;
   private int A1478ICSI_ ;
   private int AV113SeqRe ;
   private int AV191QtdLi ;
   private int GX_INS248 ;
   private long AV14FileNo ;
   private long AV166NumRe ;
   private long AV174ValIn ;
   private long AV179ValSa ;
   private long AV175ValDo ;
   private long AV181ValRe ;
   private long AV164ArqTR[] ;
   private long AV115Total ;
   private long AV116Total ;
   private long AV184Total ;
   private long A1312SCEId ;
   private double A1172lccbS ;
   private double A1170lccbI ;
   private double A1171lccbT ;
   private double AV190Total ;
   private double AV133lccbI ;
   private double AV143lccbD ;
   private double AV134lccbT ;
   private double AV135lccbS ;
   private double AV157tSale ;
   private double AV170tTipC ;
   private double AV158tSale ;
   private double AV171tTipP ;
   private double AV172tDown ;
   private double AV189Total ;
   private double GXv_int8[] ;
   private String AV10DebugM ;
   private String AV84Versao ;
   private String AV64Sep ;
   private String AV193DataA ;
   private String AV9DataDep ;
   private String AV27HoraA ;
   private String AV28HoraB ;
   private String A1305SCETe ;
   private String AV13FileNa ;
   private String AV207UsaNo ;
   private String AV140Short ;
   private String AV152RcptN ;
   private String AV163ArqN[] ;
   private String AV149OldEm ;
   private String AV216OldLc ;
   private String scmdbuf ;
   private String A1228lccbF ;
   private String A1227lccbO ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1222lccbI ;
   private String A1150lccbE ;
   private String A1224lccbC ;
   private String A1147lccbE ;
   private String A1490Distr ;
   private String A1184lccbS ;
   private String A1167lccbG ;
   private String A1179lccbV ;
   private String A1192lccbS ;
   private String A1191lccbS ;
   private String A1185lccbB ;
   private String A1193lccbS ;
   private String A1194lccbS ;
   private String A1195lccbS ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String A1480ICSI_ ;
   private String A1481ICSI_ ;
   private String AV150ICSI_ ;
   private String AV151ICSI_ ;
   private String AV144lccbE ;
   private String AV130lccbI ;
   private String AV146lccbC ;
   private String AV194Atrib ;
   private String AV195Resul ;
   private String AV137lccbC ;
   private String AV136lccbA ;
   private String AV147lccbO ;
   private String AV148lccbF ;
   private String AV188lccbV ;
   private String AV161Txt ;
   private String AV118lccbT[] ;
   private String AV117lccbP[] ;
   private String AV214bilhe ;
   private String A1231lccbT ;
   private String A1207lccbP ;
   private String A1232lccbT ;
   private String AV111s ;
   private String AV132lccbT ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String AV177LccbB ;
   private String AV112Resum ;
   private String AV141Refer ;
   private String AV223Lccbs ;
   private String AV168SCEAi ;
   private String AV169SCETe ;
   private String GXt_char1 ;
   private String A1484ICSI_ ;
   private String A1485ICSI_ ;
   private String AV138ICSI_ ;
   private String AV139ICSI_ ;
   private String AV156ICSI_ ;
   private String AV183DataC ;
   private String AV182DataV ;
   private String AV185Valor ;
   private String AV187QtdCa ;
   private String AV192QtdCa ;
   private String A1301SCEAi ;
   private String A1303SCERE ;
   private String A1311SCEVe ;
   private String A1298SCETk ;
   private String A1300SCECR ;
   private String A1307SCEIa ;
   private String A1310SCETp ;
   private String A1306SCEAP ;
   private String GXv_char4[] ;
   private String AV201PathC ;
   private String GXt_char2 ;
   private String GXv_char7[] ;
   private String GXv_char6[] ;
   private String AV202Filen ;
   private java.util.Date AV162ArqD1[] ;
   private java.util.Date A1190lccbS ;
   private java.util.Date A1229lccbS ;
   private java.util.Date A1299SCEDa ;
   private java.util.Date Gx_date ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1189lccbS ;
   private java.util.Date AV145lccbD ;
   private boolean returnInSub ;
   private boolean n1147lccbE ;
   private boolean n1172lccbS ;
   private boolean n1490Distr ;
   private boolean n1184lccbS ;
   private boolean n1167lccbG ;
   private boolean n1179lccbV ;
   private boolean n1170lccbI ;
   private boolean n1171lccbT ;
   private boolean n1192lccbS ;
   private boolean n1190lccbS ;
   private boolean n1191lccbS ;
   private boolean n1185lccbB ;
   private boolean n1193lccbS ;
   private boolean n1194lccbS ;
   private boolean n1195lccbS ;
   private boolean n1189lccbS ;
   private boolean n1168lccbI ;
   private boolean n1480ICSI_ ;
   private boolean n1481ICSI_ ;
   private boolean n1207lccbP ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n1484ICSI_ ;
   private boolean n1485ICSI_ ;
   private boolean n1479ICSI_ ;
   private boolean n1478ICSI_ ;
   private boolean n1301SCEAi ;
   private boolean n1303SCERE ;
   private boolean n1305SCETe ;
   private boolean n1311SCEVe ;
   private boolean n1298SCETk ;
   private boolean n1299SCEDa ;
   private boolean n1300SCECR ;
   private boolean n1307SCEIa ;
   private boolean n1310SCETp ;
   private boolean n1306SCEAP ;
   private boolean n1312SCEId ;
   private String AV208Bande ;
   private String AV209NovaB ;
   private String AV211Bande ;
   private String AV210NovaB ;
   private String AV215ICSI_ ;
   private String AV200Audit ;
   private String A1187lccbS ;
   private String AV212Proce ;
   private String AV213msgEr ;
   private String AV159Num06 ;
   private String AV197Audit ;
   private String AV198Audit ;
   private String AV199Audit ;
   private String GXv_svchar3[] ;
   private com.genexus.util.GXFile AV203File ;
   private com.genexus.util.GXDirectory AV204Diret ;
   private String[] aP0 ;
   private IDataStoreProvider pr_default ;
   private String[] P006Q2_A1228lccbF ;
   private String[] P006Q2_A1227lccbO ;
   private String[] P006Q2_A1226lccbA ;
   private String[] P006Q2_A1225lccbC ;
   private java.util.Date[] P006Q2_A1223lccbD ;
   private String[] P006Q2_A1222lccbI ;
   private String[] P006Q2_A1150lccbE ;
   private String[] P006Q2_A1224lccbC ;
   private String[] P006Q2_A1147lccbE ;
   private boolean[] P006Q2_n1147lccbE ;
   private double[] P006Q2_A1172lccbS ;
   private boolean[] P006Q2_n1172lccbS ;
   private String[] P006Q2_A1490Distr ;
   private boolean[] P006Q2_n1490Distr ;
   private String[] P006Q2_A1184lccbS ;
   private boolean[] P006Q2_n1184lccbS ;
   private String[] P006Q2_A1167lccbG ;
   private boolean[] P006Q2_n1167lccbG ;
   private String[] P006Q2_A1179lccbV ;
   private boolean[] P006Q2_n1179lccbV ;
   private double[] P006Q2_A1170lccbI ;
   private boolean[] P006Q2_n1170lccbI ;
   private double[] P006Q2_A1171lccbT ;
   private boolean[] P006Q2_n1171lccbT ;
   private String[] P006Q2_A1192lccbS ;
   private boolean[] P006Q2_n1192lccbS ;
   private java.util.Date[] P006Q2_A1190lccbS ;
   private boolean[] P006Q2_n1190lccbS ;
   private String[] P006Q2_A1191lccbS ;
   private boolean[] P006Q2_n1191lccbS ;
   private String[] P006Q2_A1185lccbB ;
   private boolean[] P006Q2_n1185lccbB ;
   private String[] P006Q2_A1193lccbS ;
   private boolean[] P006Q2_n1193lccbS ;
   private String[] P006Q2_A1194lccbS ;
   private boolean[] P006Q2_n1194lccbS ;
   private String[] P006Q2_A1195lccbS ;
   private boolean[] P006Q2_n1195lccbS ;
   private java.util.Date[] P006Q2_A1189lccbS ;
   private boolean[] P006Q2_n1189lccbS ;
   private short[] P006Q2_A1168lccbI ;
   private boolean[] P006Q2_n1168lccbI ;
   private String[] P006Q3_A1488ICSI_ ;
   private String[] P006Q3_A1487ICSI_ ;
   private String[] P006Q3_A1480ICSI_ ;
   private boolean[] P006Q3_n1480ICSI_ ;
   private String[] P006Q3_A1481ICSI_ ;
   private boolean[] P006Q3_n1481ICSI_ ;
   private String[] P006Q4_A1150lccbE ;
   private String[] P006Q4_A1222lccbI ;
   private java.util.Date[] P006Q4_A1223lccbD ;
   private String[] P006Q4_A1224lccbC ;
   private String[] P006Q4_A1225lccbC ;
   private String[] P006Q4_A1226lccbA ;
   private String[] P006Q4_A1227lccbO ;
   private String[] P006Q4_A1228lccbF ;
   private String[] P006Q4_A1231lccbT ;
   private String[] P006Q4_A1207lccbP ;
   private boolean[] P006Q4_n1207lccbP ;
   private String[] P006Q4_A1232lccbT ;
   private String[] P006Q7_A1228lccbF ;
   private String[] P006Q7_A1227lccbO ;
   private String[] P006Q7_A1226lccbA ;
   private String[] P006Q7_A1225lccbC ;
   private String[] P006Q7_A1224lccbC ;
   private java.util.Date[] P006Q7_A1223lccbD ;
   private String[] P006Q7_A1222lccbI ;
   private String[] P006Q7_A1150lccbE ;
   private double[] P006Q7_A1172lccbS ;
   private boolean[] P006Q7_n1172lccbS ;
   private String[] P006Q7_A1490Distr ;
   private boolean[] P006Q7_n1490Distr ;
   private String[] P006Q7_A1184lccbS ;
   private boolean[] P006Q7_n1184lccbS ;
   private String[] P006Q7_A1167lccbG ;
   private boolean[] P006Q7_n1167lccbG ;
   private java.util.Date[] P006Q7_A1189lccbS ;
   private boolean[] P006Q7_n1189lccbS ;
   private short[] P006Q7_A1168lccbI ;
   private boolean[] P006Q7_n1168lccbI ;
   private String[] P006Q10_A1488ICSI_ ;
   private String[] P006Q10_A1487ICSI_ ;
   private String[] P006Q10_A1484ICSI_ ;
   private boolean[] P006Q10_n1484ICSI_ ;
   private String[] P006Q10_A1485ICSI_ ;
   private boolean[] P006Q10_n1485ICSI_ ;
   private int[] P006Q10_A1479ICSI_ ;
   private boolean[] P006Q10_n1479ICSI_ ;
   private String[] P006Q13_A1488ICSI_ ;
   private String[] P006Q13_A1487ICSI_ ;
   private int[] P006Q13_A1478ICSI_ ;
   private boolean[] P006Q13_n1478ICSI_ ;
   private long[] P006Q16_A1312SCEId ;
   private boolean[] P006Q16_n1312SCEId ;
}

final  class pretsubcielo__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006Q2", "SELECT T1.[lccbFPAC_PLP], T1.[lccbOpCode], T1.[lccbAppCode], T1.[lccbCCNum], T1.[lccbDate], T1.[lccbIATA], T1.[lccbEmpCod], T1.[lccbCCard], T2.[lccbEmpEnab], T1.[lccbSaleAmount], T1.[DistribuicaoTransacoesTipo], T1.[lccbStatus], T1.[lccbGDS], T1.[lccbValidDate], T1.[lccbInstAmount], T1.[lccbTip], T1.[lccbSubFile], T1.[lccbSubTime], T1.[lccbSubType], T1.[lccbBatchNum], T1.[lccbSubTrn], T1.[lccbSubRO], T1.[lccbSubPOS], T1.[lccbSubDate], T1.[lccbInstallments] FROM ([LCCBPLP] T1 WITH (UPDLOCK) INNER JOIN [LCCBEMP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod]) WHERE (T1.[lccbOpCode] = 'S' and T1.[lccbStatus] = 'TOSUB') AND (SUBSTRING(T1.[DistribuicaoTransacoesTipo], 1, 1) = 'C') AND (T1.[lccbSaleAmount] > 0.00) AND (T2.[lccbEmpEnab] = '1') ORDER BY T1.[lccbOpCode], T1.[lccbStatus], T1.[DistribuicaoTransacoesTipo], T1.[lccbEmpCod], T1.[lccbInstallments], T1.[lccbSubDate] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P006Q3", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCPOS1], [ICSI_CCPOS2] FROM [ICSI_CCINFO] WITH (NOLOCK) WHERE [ICSI_EmpCod] = ? and [ICSI_CCCod] = 'VI' ORDER BY [ICSI_EmpCod], [ICSI_CCCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P006Q4", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbPaxName], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006Q5", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006Q6", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbSubFile]=?, [lccbSubTime]=?, [lccbSubType]=?, [lccbBatchNum]=?, [lccbSubTrn]=?, [lccbSubRO]=?, [lccbSubPOS]=?, [lccbSubDate]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006Q7", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbSaleAmount], [DistribuicaoTransacoesTipo], [lccbStatus], [lccbGDS], [lccbSubDate], [lccbInstallments] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbOpCode] = 'S' AND [lccbStatus] = 'TOSUB') AND (([lccbOpCode] = 'S' and [lccbStatus] = 'TOSUB') AND (SUBSTRING([DistribuicaoTransacoesTipo], 1, 1) = 'C') AND ([lccbSaleAmount] <= 0.00)) ORDER BY [lccbOpCode], [lccbStatus], [lccbCCard], [lccbEmpCod], [lccbInstallments], [lccbSubDate] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006Q8", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006Q9", "UPDATE [LCCBPLP] SET [lccbStatus]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006Q10", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCEstab], [ICSI_CCNome], [ICSI_CCSeqFile] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = '000' AND [ICSI_CCCod] = 'VI') AND ([ICSI_EmpCod] = '000' and [ICSI_CCCod] = 'VI') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P006Q11", "UPDATE [ICSI_CCINFO] SET [ICSI_CCSeqFile]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006Q12", "INSERT INTO [ICSI_CCINFO] ([ICSI_EmpCod], [ICSI_CCCod], [ICSI_CCLote], [ICSI_CCSeqFile], [ICSI_CCEstab], [ICSI_CCNome], [ICSI_CCPOS1], [ICSI_CCPOS2], [ICSI_CCPOS3], [ICSI_SplitPLP]) VALUES (?, ?, ?, ?, ?, ?, '', '', '', convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006Q13", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCLote] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = '000' AND [ICSI_CCCod] = 'VI') AND ([ICSI_EmpCod] = '000' and [ICSI_CCCod] = 'VI') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P006Q14", "UPDATE [ICSI_CCINFO] SET [ICSI_CCLote]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006Q15", "INSERT INTO [SCEVENTS] ([SCETkt], [SCEDate], [SCECRS], [SCEAirLine], [SCERETName], [SCEText], [SCEAPP], [SCEIata], [SCETpEvento], [SCEVersao], [SCEAirportCode], [SCEFopID], [SCEFPAM], [SCEParcela]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '', convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006Q16", "SELECT @@IDENTITY ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,3,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(5) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 7) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 3) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 2) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 1) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((double[]) buf[10])[0] = rslt.getDouble(10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 4) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 8) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 4) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 4) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(17, 20) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[26])[0] = rslt.getGXDateTime(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((String[]) buf[28])[0] = rslt.getString(19, 1) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 20) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((String[]) buf[34])[0] = rslt.getString(22, 10) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((String[]) buf[36])[0] = rslt.getString(23, 20) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[38])[0] = rslt.getGXDate(24) ;
               ((boolean[]) buf[39])[0] = rslt.wasNull();
               ((short[]) buf[40])[0] = rslt.getShort(25) ;
               ((boolean[]) buf[41])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 50) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(11, 4) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((double[]) buf[8])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 8) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 4) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[16])[0] = rslt.getGXDate(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((short[]) buf[18])[0] = rslt.getShort(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((int[]) buf[6])[0] = rslt.getInt(5) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((int[]) buf[2])[0] = rslt.getInt(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 14 :
               ((long[]) buf[0])[0] = rslt.getLong(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 20);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(3, (java.util.Date)parms[5], false);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 1);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 20);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 10);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 20);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(9, (java.util.Date)parms[17]);
               }
               stmt.setString(10, (String)parms[18], 3);
               stmt.setString(11, (String)parms[19], 7);
               stmt.setDate(12, (java.util.Date)parms[20]);
               stmt.setString(13, (String)parms[21], 2);
               stmt.setString(14, (String)parms[22], 44);
               stmt.setString(15, (String)parms[23], 20);
               stmt.setString(16, (String)parms[24], 1);
               stmt.setString(17, (String)parms[25], 19);
               break;
            case 6 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
            case 10 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 10);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(3, ((Number) parms[3]).intValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(4, ((Number) parms[5]).intValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[7], 20);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[9], 30);
               }
               break;
            case 12 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
            case 13 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(2, (java.util.Date)parms[3], false);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 5);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 3);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 50);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 150);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 4);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 11);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 3);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 5);
               }
               break;
      }
   }

}


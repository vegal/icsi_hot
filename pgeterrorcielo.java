/*
               File: GetErrorCielo
        Description: Get Error Cielo
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pgeterrorcielo extends GXProcedure
{
   public pgeterrorcielo( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pgeterrorcielo.class ), "" );
   }

   public pgeterrorcielo( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      pgeterrorcielo.this.AV8ErrType = aP0[0];
      this.aP0 = aP0;
      pgeterrorcielo.this.AV9ErrCod = aP1[0];
      this.aP1 = aP1;
      pgeterrorcielo.this.AV10ErrDes = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( GXutil.strcmp(AV8ErrType, "99") == 0 ) )
      {
         /* Execute user subroutine: S11166 */
         S11166 ();
         if ( returnInSub )
         {
            returnInSub = true;
            cleanup();
            if (true) return;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(AV9ErrCod, "00") == 0 ) )
         {
            AV10ErrDes = "Transa��o Aceita" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "01") == 0 ) )
         {
            AV10ErrDes = "Erro no arquivo" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "02") == 0 ) )
         {
            AV10ErrDes = "C�digo de autoriza��o inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "03") == 0 ) )
         {
            AV10ErrDes = "Estabelecimento inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "04") == 0 ) )
         {
            AV10ErrDes = "Lote misturado" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "05") == 0 ) )
         {
            AV10ErrDes = "N�mero de parcelas inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "06") == 0 ) )
         {
            AV10ErrDes = "Diferen�a de valor no RO" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "07") == 0 ) )
         {
            AV10ErrDes = "N�mero do RO inv�lido (registro BH)" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "08") == 0 ) )
         {
            AV10ErrDes = "Valor de entrada inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "09") == 0 ) )
         {
            AV10ErrDes = "Valor da taxa de embarque inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "10") == 0 ) )
         {
            AV10ErrDes = "Valor da parcela inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "11") == 0 ) )
         {
            AV10ErrDes = "C�digo de transa��o inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "12") == 0 ) )
         {
            AV10ErrDes = "Transa��o inv�lida" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "13") == 0 ) )
         {
            AV10ErrDes = "Valor inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "14") == 0 ) )
         {
            AV10ErrDes = "N�mero do cart�o inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "15") == 0 ) )
         {
            AV10ErrDes = "Valor do cancelamento inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "16") == 0 ) )
         {
            AV10ErrDes = "Transa��o original  n�o localizada (para cancelamento)" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "17") == 0 ) )
         {
            AV10ErrDes = "N� de itens informados no RO n�o compat�vel com os CV�s" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "18") == 0 ) )
         {
            AV10ErrDes = "N�mero de refer�ncia inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "54") == 0 ) )
         {
            AV10ErrDes = "N�o � permitido cancelamento parcial de um plano parcelado que est� sendo contestado pelo portador." ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "20") == 0 ) )
         {
            AV10ErrDes = "Cancelamento para parcelado de transa��o j� cancelada" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "21") == 0 ) )
         {
            AV10ErrDes = "Valor do cancelamento maior que o valor da venda" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "22") == 0 ) )
         {
            AV10ErrDes = "Valor do cancelamento maior que o permitido (al�ada)" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "23") == 0 ) )
         {
            AV10ErrDes = "N�mero do RO original inv�lido (registro I2)" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "42") == 0 ) )
         {
            AV10ErrDes = "Cart�o em Boletim " ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "56") == 0 ) )
         {
            AV10ErrDes = "Tipo de plano de cart�o inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "59") == 0 ) )
         {
            AV10ErrDes = "Tipo cart�o inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "60") == 0 ) )
         {
            AV10ErrDes = "Data inv�lida" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "71") == 0 ) )
         {
            AV10ErrDes = "Transa��o rejeitada pelo banco emissor" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "72") == 0 ) )
         {
            AV10ErrDes = "Transa��o rejeitada pelo banco emissor" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "73") == 0 ) )
         {
            AV10ErrDes = "Cart�o com problema - reter o cart�o" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "74") == 0 ) )
         {
            AV10ErrDes = "Autoriza��o negada" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "75") == 0 ) )
         {
            AV10ErrDes = "Erro (POS)" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "76") == 0 ) )
         {
            AV10ErrDes = "Reter o cart�o - condi��o especial" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "77") == 0 ) )
         {
            AV10ErrDes = "Erro de sintaxe - refa�a a transa��o" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "78") == 0 ) )
         {
            AV10ErrDes = "N�o foi encontrada autoriza��o no emissor" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "79") == 0 ) )
         {
            AV10ErrDes = "Cart�o perdido - reter o cart�o" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "80") == 0 ) )
         {
            AV10ErrDes = "Cart�o roubado - reter o cart�o" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "81") == 0 ) )
         {
            AV10ErrDes = "Fundos insuficientes" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "82") == 0 ) )
         {
            AV10ErrDes = "Cart�o vencido ou data do vencimento errada" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "83") == 0 ) )
         {
            AV10ErrDes = "Senha incorreta" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "86") == 0 ) )
         {
            AV10ErrDes = "Excedeu o n�mero de saques no per�odo" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "87") == 0 ) )
         {
            AV10ErrDes = "Cart�o restrito" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "88") == 0 ) )
         {
            AV10ErrDes = "Excedeu o n�mero de transa��es no per�odo" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "89") == 0 ) )
         {
            AV10ErrDes = "Mensagem difere da mensagem original" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "90") == 0 ) )
         {
            AV10ErrDes = "CVV inv�lido" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "91") == 0 ) )
         {
            AV10ErrDes = "Imposs�vel verificar senha" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "92") == 0 ) )
         {
            AV10ErrDes = "Banco emissor sem comunica��o" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "93") == 0 ) )
         {
            AV10ErrDes = "Cancelamento com mais de 300 dias" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "94") == 0 ) )
         {
            AV10ErrDes = "Duplicidade de linhas a�reas" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "95") == 0 ) )
         {
            AV10ErrDes = "Sem saldo em aberto" ;
         }
         else if ( ( GXutil.strcmp(AV9ErrCod, "55") == 0 ) )
         {
            AV10ErrDes = "D�bito efetuado, conforme contesta��o do portador" ;
         }
         else
         {
            AV10ErrDes = "Erro desconhecido" ;
         }
      }
      cleanup();
   }

   public void S11166( )
   {
      /* 'GETMOVERROR' Routine */
      AV11ErrTab = "|900|Tipo de Registro Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|901|Data do Dep�sito do Header Inv�lida" ;
      AV11ErrTab = AV11ErrTab + "|902|N�mero do Resumo de Opera��es (RO) do Header Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|903|N�mero do Estabelecimento do Header Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|904|Moeda do Header Inv�lida" ;
      AV11ErrTab = AV11ErrTab + "|905|Indicador de teste do Header Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|906|Indicador de venda do Header Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|911|N�mero do Comprovante de Venda (CV) Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|912|Data da Venda Inv�lida" ;
      AV11ErrTab = AV11ErrTab + "|913|Op��o da Venda Inv�lida" ;
      AV11ErrTab = AV11ErrTab + "|914|N�mero do Cart�o Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|915|Valor da Venda Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|916|Quantidade de Parcelas Inv�lida" ;
      AV11ErrTab = AV11ErrTab + "|917|Valor Financiado Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|918|Valor da Entrada Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|919|Valor da Taxa Embarque Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|920|Valor da Parcela Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|921|N�mero do Resumo de Opera��es (RO) Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|922|N�mero do Estabelecimento Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|923|Validade do Cart�o Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|924|N�mero do Resumo de Opera��es (RO) Original Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|925|Valor do Reembolso Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|926|N�mero de Referencia Inv�lido" ;
      AV11ErrTab = AV11ErrTab + "|930|Quantidade de Registros do Trailer n�o confere" ;
      AV11ErrTab = AV11ErrTab + "|931|Valor total bruto n�o confere" ;
      AV11ErrTab = AV11ErrTab + "|" ;
      AV14s = "|" + AV9ErrCod + "|" ;
      AV13n = GXutil.strSearch( AV11ErrTab, AV14s, 1) ;
      if ( ( AV13n > 0 ) )
      {
         AV12i = (int)(AV13n+5) ;
         AV13n = GXutil.strSearch( AV11ErrTab, "|", AV12i) ;
         AV13n = (int)(AV13n-AV12i) ;
         AV10ErrDes = GXutil.substring( AV11ErrTab, AV13n, AV12i) ;
      }
      else
      {
         AV10ErrDes = "ERRO DESCONHECIDO: " + AV9ErrCod ;
      }
   }

   protected void cleanup( )
   {
      this.aP0[0] = pgeterrorcielo.this.AV8ErrType;
      this.aP1[0] = pgeterrorcielo.this.AV9ErrCod;
      this.aP2[0] = pgeterrorcielo.this.AV10ErrDes;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      returnInSub = false ;
      AV11ErrTab = "" ;
      AV14s = "" ;
      AV13n = 0 ;
      AV12i = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private int AV13n ;
   private int AV12i ;
   private String AV8ErrType ;
   private String AV9ErrCod ;
   private String AV10ErrDes ;
   private String AV14s ;
   private boolean returnInSub ;
   private String AV11ErrTab ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
}


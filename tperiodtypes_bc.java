/*
               File: tperiodtypes_bc
        Description: Period Types
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:13.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tperiodtypes_bc extends GXWebPanel implements IGxSilentTrn
{
   public tperiodtypes_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tperiodtypes_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tperiodtypes_bc.class ));
   }

   public tperiodtypes_bc( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         /* Execute user event: e112A2 */
         e112A2 ();
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z387Period = A387Period ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_2A0( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls2A225( ) ;
         }
         else
         {
            checkExtendedTable2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors2A225( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues2A0( ) ;
      }
   }

   public void e122A2( )
   {
      /* Start Routine */
      AV14LgnLog = AV11Sessio.getValue("LOGGED") ;
      if ( ( GXutil.strcmp(AV14LgnLog, "") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hlogin") );
      }
   }

   public void e132A2( )
   {
      /* 'Back' Routine */
      httpContext.setLinkToRedirect(formatLink("hperiodtypes") );
   }

   public void e112A2( )
   {
      /* After Trn Routine */
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
      {
         httpContext.setLinkToRedirect(formatLink("hperiodtypes") );
      }
   }

   public void zm2A225( int GX_JID )
   {
      if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
      {
         Z1116Perio = A1116Perio ;
         Z1117Perio = A1117Perio ;
      }
      if ( ( GX_JID == -4 ) )
      {
         Z387Period = A387Period ;
         Z1116Perio = A1116Perio ;
         Z1117Perio = A1117Perio ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load2A225( )
   {
      /* Using cursor BC002A4 */
      pr_default.execute(2, new Object[] {A387Period});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound225 = (short)(1) ;
         A1116Perio = BC002A4_A1116Perio[0] ;
         A1117Perio = BC002A4_A1117Perio[0] ;
         zm2A225( -4) ;
      }
      pr_default.close(2);
      onLoadActions2A225( ) ;
   }

   public void onLoadActions2A225( )
   {
   }

   public void checkExtendedTable2A225( )
   {
      standaloneModal( ) ;
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A387Period), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Code can not be null", 1);
         AnyError = (short)(1) ;
      }
      if ( true /* After */ && ( GXutil.strcmp(GXutil.trim( A1116Perio), "") == 0 ) )
      {
         httpContext.GX_msglist.addItem("Description can not be null", 1);
         AnyError = (short)(1) ;
      }
   }

   public void closeExtendedTableCursors2A225( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey2A225( )
   {
      /* Using cursor BC002A5 */
      pr_default.execute(3, new Object[] {A387Period});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound225 = (short)(1) ;
      }
      else
      {
         RcdFound225 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC002A3 */
      pr_default.execute(1, new Object[] {A387Period});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm2A225( 4) ;
         RcdFound225 = (short)(1) ;
         A387Period = BC002A3_A387Period[0] ;
         A1116Perio = BC002A3_A1116Perio[0] ;
         A1117Perio = BC002A3_A1117Perio[0] ;
         Z387Period = A387Period ;
         sMode225 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load2A225( ) ;
         Gx_mode = sMode225 ;
      }
      else
      {
         RcdFound225 = (short)(0) ;
         initializeNonKey2A225( ) ;
         sMode225 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode225 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey2A225( ) ;
      if ( ( RcdFound225 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_2A0( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency2A225( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC002A2 */
         pr_default.execute(0, new Object[] {A387Period});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"PERIODTYPES"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z1116Perio, BC002A2_A1116Perio[0]) != 0 ) || ( GXutil.strcmp(Z1117Perio, BC002A2_A1117Perio[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"PERIODTYPES"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert2A225( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm2A225( 0) ;
         checkOptimisticConcurrency2A225( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert2A225( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002A6 */
                  pr_default.execute(4, new Object[] {A387Period, A1116Perio, A1117Perio});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load2A225( ) ;
         }
         endLevel2A225( ) ;
      }
      closeExtendedTableCursors2A225( ) ;
   }

   public void update2A225( )
   {
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2A225( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate2A225( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC002A7 */
                  pr_default.execute(5, new Object[] {A1116Perio, A1117Perio, A387Period});
                  deferredUpdate2A225( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel2A225( ) ;
      }
      closeExtendedTableCursors2A225( ) ;
   }

   public void deferredUpdate2A225( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate2A225( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls2A225( ) ;
         /* No cascading delete specified. */
         afterConfirm2A225( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete2A225( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC002A8 */
               pr_default.execute(6, new Object[] {A387Period});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode225 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel2A225( ) ;
      Gx_mode = sMode225 ;
   }

   public void onDeleteControls2A225( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
      if ( ( AnyError == 0 ) )
      {
         /* Using cursor BC002A9 */
         pr_default.execute(7, new Object[] {A387Period});
         if ( (pr_default.getStatus(7) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("del", new Object[] {"Period Configuration"}), "CannotDeleteReferencedRecord", 1);
            AnyError = (short)(1) ;
         }
         pr_default.close(7);
      }
   }

   public void endLevel2A225( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete2A225( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues2A0( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart2A225( )
   {
      /* Using cursor BC002A10 */
      pr_default.execute(8, new Object[] {A387Period});
      RcdFound225 = (short)(0) ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound225 = (short)(1) ;
         A387Period = BC002A10_A387Period[0] ;
         A1116Perio = BC002A10_A1116Perio[0] ;
         A1117Perio = BC002A10_A1117Perio[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext2A225( )
   {
      pr_default.readNext(8);
      RcdFound225 = (short)(0) ;
      scanLoad2A225( ) ;
   }

   public void scanLoad2A225( )
   {
      sMode225 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(8) != 101) )
      {
         RcdFound225 = (short)(1) ;
         A387Period = BC002A10_A387Period[0] ;
         A1116Perio = BC002A10_A1116Perio[0] ;
         A1117Perio = BC002A10_A1117Perio[0] ;
      }
      Gx_mode = sMode225 ;
   }

   public void scanEnd2A225( )
   {
      pr_default.close(8);
   }

   public void afterConfirm2A225( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert2A225( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate2A225( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete2A225( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete2A225( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate2A225( )
   {
      /* Before Validate Rules */
   }

   public void addRow2A225( )
   {
      VarsToRow225( bcPeriodTypes) ;
   }

   public void sendRow2A225( )
   {
   }

   public void readRow2A225( )
   {
      RowToVars225( bcPeriodTypes, 0) ;
   }

   public void confirmValues2A0( )
   {
   }

   public void initializeNonKey2A225( )
   {
      A1116Perio = "" ;
      A1117Perio = "" ;
   }

   public void initAll2A225( )
   {
      A387Period = "" ;
      initializeNonKey2A225( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow225( SdtPeriodTypes obj225 )
   {
      obj225.setgxTv_SdtPeriodTypes_Mode( Gx_mode );
      obj225.setgxTv_SdtPeriodTypes_Periodtypesdescription( A1116Perio );
      obj225.setgxTv_SdtPeriodTypes_Periodtypesstatus( A1117Perio );
      obj225.setgxTv_SdtPeriodTypes_Periodtypescode( A387Period );
      obj225.setgxTv_SdtPeriodTypes_Periodtypescode_Z( Z387Period );
      obj225.setgxTv_SdtPeriodTypes_Periodtypesdescription_Z( Z1116Perio );
      obj225.setgxTv_SdtPeriodTypes_Periodtypesstatus_Z( Z1117Perio );
      obj225.setgxTv_SdtPeriodTypes_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars225( SdtPeriodTypes obj225 ,
                             int forceLoad )
   {
      Gx_mode = obj225.getgxTv_SdtPeriodTypes_Mode() ;
      A1116Perio = obj225.getgxTv_SdtPeriodTypes_Periodtypesdescription() ;
      A1117Perio = obj225.getgxTv_SdtPeriodTypes_Periodtypesstatus() ;
      if ( ! ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  || ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  ) || ( forceLoad == 1 ) )
      {
         A387Period = obj225.getgxTv_SdtPeriodTypes_Periodtypescode() ;
      }
      Z387Period = obj225.getgxTv_SdtPeriodTypes_Periodtypescode_Z() ;
      Z1116Perio = obj225.getgxTv_SdtPeriodTypes_Periodtypesdescription_Z() ;
      Z1117Perio = obj225.getgxTv_SdtPeriodTypes_Periodtypesstatus_Z() ;
      Gx_mode = obj225.getgxTv_SdtPeriodTypes_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A387Period = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey2A225( ) ;
      scanStart2A225( ) ;
      if ( ( RcdFound225 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z387Period = A387Period ;
      }
      onLoadActions2A225( ) ;
      zm2A225( 0) ;
      addRow2A225( ) ;
      scanEnd2A225( ) ;
      if ( ( RcdFound225 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars225( bcPeriodTypes, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey2A225( ) ;
      if ( ( RcdFound225 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
         {
            A387Period = Z387Period ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update2A225( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2A225( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert2A225( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow225( bcPeriodTypes) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars225( bcPeriodTypes, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey2A225( ) ;
      if ( ( RcdFound225 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
         {
            A387Period = Z387Period ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A387Period, Z387Period) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tperiodtypes_bc");
      VarsToRow225( bcPeriodTypes) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcPeriodTypes.getgxTv_SdtPeriodTypes_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcPeriodTypes.setgxTv_SdtPeriodTypes_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtPeriodTypes sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcPeriodTypes ) )
      {
         bcPeriodTypes = sdt ;
         if ( ( GXutil.strcmp(bcPeriodTypes.getgxTv_SdtPeriodTypes_Mode(), "") == 0 ) )
         {
            bcPeriodTypes.setgxTv_SdtPeriodTypes_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow225( bcPeriodTypes) ;
         }
         else
         {
            RowToVars225( bcPeriodTypes, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcPeriodTypes.getgxTv_SdtPeriodTypes_Mode(), "") == 0 ) )
         {
            bcPeriodTypes.setgxTv_SdtPeriodTypes_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars225( bcPeriodTypes, 1) ;
      return  ;
   }

   public SdtPeriodTypes getPeriodTypes_BC( )
   {
      return bcPeriodTypes ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z387Period = "" ;
      A387Period = "" ;
      AV14LgnLog = "" ;
      AV11Sessio = httpContext.getWebSession();
      gxTv_SdtPeriodTypes_Periodtypescode_Z = "" ;
      gxTv_SdtPeriodTypes_Periodtypesdescription_Z = "" ;
      gxTv_SdtPeriodTypes_Periodtypesstatus_Z = "" ;
      GX_JID = 0 ;
      Z1116Perio = "" ;
      A1116Perio = "" ;
      Z1117Perio = "" ;
      A1117Perio = "" ;
      BC002A4_A387Period = new String[] {""} ;
      BC002A4_A1116Perio = new String[] {""} ;
      BC002A4_A1117Perio = new String[] {""} ;
      RcdFound225 = (short)(0) ;
      BC002A5_A387Period = new String[] {""} ;
      BC002A3_A387Period = new String[] {""} ;
      BC002A3_A1116Perio = new String[] {""} ;
      BC002A3_A1117Perio = new String[] {""} ;
      sMode225 = "" ;
      BC002A2_A387Period = new String[] {""} ;
      BC002A2_A1116Perio = new String[] {""} ;
      BC002A2_A1117Perio = new String[] {""} ;
      BC002A9_A23ISOCod = new String[] {""} ;
      BC002A9_A349CurCod = new String[] {""} ;
      BC002A9_A387Period = new String[] {""} ;
      BC002A9_A263PerID = new String[] {""} ;
      BC002A10_A387Period = new String[] {""} ;
      BC002A10_A1116Perio = new String[] {""} ;
      BC002A10_A1117Perio = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tperiodtypes_bc__default(),
         new Object[] {
             new Object[] {
            BC002A2_A387Period, BC002A2_A1116Perio, BC002A2_A1117Perio
            }
            , new Object[] {
            BC002A3_A387Period, BC002A3_A1116Perio, BC002A3_A1117Perio
            }
            , new Object[] {
            BC002A4_A387Period, BC002A4_A1116Perio, BC002A4_A1117Perio
            }
            , new Object[] {
            BC002A5_A387Period
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC002A9_A23ISOCod, BC002A9_A349CurCod, BC002A9_A387Period, BC002A9_A263PerID
            }
            , new Object[] {
            BC002A10_A387Period, BC002A10_A1116Perio, BC002A10_A1117Perio
            }
         }
      );
      /* Execute Start event if defined. */
      /* Execute user event: e122A2 */
      e122A2 ();
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound225 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String gxTv_SdtPeriodTypes_Periodtypesstatus_Z ;
   private String Z1117Perio ;
   private String A1117Perio ;
   private String sMode225 ;
   private String Z387Period ;
   private String A387Period ;
   private String AV14LgnLog ;
   private String gxTv_SdtPeriodTypes_Periodtypescode_Z ;
   private String gxTv_SdtPeriodTypes_Periodtypesdescription_Z ;
   private String Z1116Perio ;
   private String A1116Perio ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.WebSession AV11Sessio ;
   private SdtPeriodTypes bcPeriodTypes ;
   private IDataStoreProvider pr_default ;
   private String[] BC002A4_A387Period ;
   private String[] BC002A4_A1116Perio ;
   private String[] BC002A4_A1117Perio ;
   private String[] BC002A5_A387Period ;
   private String[] BC002A3_A387Period ;
   private String[] BC002A3_A1116Perio ;
   private String[] BC002A3_A1117Perio ;
   private String[] BC002A2_A387Period ;
   private String[] BC002A2_A1116Perio ;
   private String[] BC002A2_A1117Perio ;
   private String[] BC002A9_A23ISOCod ;
   private String[] BC002A9_A349CurCod ;
   private String[] BC002A9_A387Period ;
   private String[] BC002A9_A263PerID ;
   private String[] BC002A10_A387Period ;
   private String[] BC002A10_A1116Perio ;
   private String[] BC002A10_A1117Perio ;
}

final  class tperiodtypes_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC002A2", "SELECT [PeriodTypesCode], [PeriodTypesDescription], [PeriodTypesStatus] FROM [PERIODTYPES] WITH (UPDLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002A3", "SELECT [PeriodTypesCode], [PeriodTypesDescription], [PeriodTypesStatus] FROM [PERIODTYPES] WITH (NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002A4", "SELECT TM1.[PeriodTypesCode], TM1.[PeriodTypesDescription], TM1.[PeriodTypesStatus] FROM [PERIODTYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[PeriodTypesCode] = ? ORDER BY TM1.[PeriodTypesCode] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC002A5", "SELECT [PeriodTypesCode] FROM [PERIODTYPES] WITH (FASTFIRSTROW NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC002A6", "INSERT INTO [PERIODTYPES] ([PeriodTypesCode], [PeriodTypesDescription], [PeriodTypesStatus]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC002A7", "UPDATE [PERIODTYPES] SET [PeriodTypesDescription]=?, [PeriodTypesStatus]=?  WHERE [PeriodTypesCode] = ?", GX_NOMASK)
         ,new UpdateCursor("BC002A8", "DELETE FROM [PERIODTYPES]  WHERE [PeriodTypesCode] = ?", GX_NOMASK)
         ,new ForEachCursor("BC002A9", "SELECT TOP 1 [ISOCod], [CurCode], [PeriodTypesCode], [PerID] FROM [PERIODS] WITH (NOLOCK) WHERE [PeriodTypesCode] = ? ",true, GX_NOMASK, false, this,0,true )
         ,new ForEachCursor("BC002A10", "SELECT TM1.[PeriodTypesCode], TM1.[PeriodTypesDescription], TM1.[PeriodTypesStatus] FROM [PERIODTYPES] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[PeriodTypesCode] = ? ORDER BY TM1.[PeriodTypesCode] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               stmt.setVarchar(2, (String)parms[1], 30, false);
               stmt.setString(3, (String)parms[2], 1);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 30, false);
               stmt.setString(2, (String)parms[1], 1);
               stmt.setVarchar(3, (String)parms[2], 5, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
            case 8 :
               stmt.setVarchar(1, (String)parms[0], 5, false);
               break;
      }
   }

}


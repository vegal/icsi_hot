import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtICSI_CCConf_Level1Item extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtICSI_CCConf_Level1Item( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtICSI_CCConf_Level1Item.class));
   }

   public SdtICSI_CCConf_Level1Item( int remoteHandle ,
                                     ModelContext context )
   {
      super( context, "SdtICSI_CCConf_Level1Item");
      initialize( remoteHandle) ;
   }

   public SdtICSI_CCConf_Level1Item( int remoteHandle ,
                                     StructSdtICSI_CCConf_Level1Item struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public SdtICSI_CCConf_Level1Item( )
   {
      super( new ModelContext(SdtICSI_CCConf_Level1Item.class), "SdtICSI_CCConf_Level1Item");
      initialize( ) ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCError") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror = (int)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCErrorDsc") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Modified") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Modified = (short)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCError_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = (int)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCErrorDsc_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCErrorDsc_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "ICSI_CCConf.Level1Item" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ICSI_CCError", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror, 6, 0)));
      oWriter.writeElement("ICSI_CCErrorDsc", GXutil.rtrim( gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtICSI_CCConf_Level1Item_Mode));
      oWriter.writeElement("Modified", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Level1Item_Modified, 4, 0)));
      oWriter.writeElement("ICSI_CCError_Z", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z, 6, 0)));
      oWriter.writeElement("ICSI_CCErrorDsc_Z", GXutil.rtrim( gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z));
      oWriter.writeElement("ICSI_CCErrorDsc_N", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public int getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror( int value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror = 0 ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc( String value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(1) ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc = "" ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Level1Item_Mode( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Mode ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Mode( String value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Mode_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Mode = "" ;
      return  ;
   }

   public short getgxTv_SdtICSI_CCConf_Level1Item_Modified( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Modified ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Modified( short value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Modified = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Modified_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Modified = (short)(0) ;
      return  ;
   }

   public int getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z( int value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = 0 ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z( String value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N( )
   {
      return gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N( byte value )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror = 0 ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Mode = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Modified = (short)(0) ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z = 0 ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z = "" ;
      gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      return  ;
   }

   public SdtICSI_CCConf_Level1Item Clone( )
   {
      return (SdtICSI_CCConf_Level1Item)(clone()) ;
   }

   public void setStruct( StructSdtICSI_CCConf_Level1Item struct )
   {
      setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror(struct.getIcsi_ccerror());
      setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc(struct.getIcsi_ccerrordsc());
      setgxTv_SdtICSI_CCConf_Level1Item_Mode(struct.getMode());
      setgxTv_SdtICSI_CCConf_Level1Item_Modified(struct.getModified());
      setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z(struct.getIcsi_ccerror_Z());
      setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z(struct.getIcsi_ccerrordsc_Z());
      setgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N(struct.getIcsi_ccerrordsc_N());
   }

   public StructSdtICSI_CCConf_Level1Item getStruct( )
   {
      StructSdtICSI_CCConf_Level1Item struct = new StructSdtICSI_CCConf_Level1Item ();
      struct.setIcsi_ccerror(getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror());
      struct.setIcsi_ccerrordsc(getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc());
      struct.setMode(getgxTv_SdtICSI_CCConf_Level1Item_Mode());
      struct.setModified(getgxTv_SdtICSI_CCConf_Level1Item_Modified());
      struct.setIcsi_ccerror_Z(getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z());
      struct.setIcsi_ccerrordsc_Z(getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z());
      struct.setIcsi_ccerrordsc_N(getgxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N());
      return struct ;
   }

   protected byte gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_N ;
   protected short gxTv_SdtICSI_CCConf_Level1Item_Modified ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected int gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror ;
   protected int gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerror_Z ;
   protected String gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc ;
   protected String gxTv_SdtICSI_CCConf_Level1Item_Mode ;
   protected String gxTv_SdtICSI_CCConf_Level1Item_Icsi_ccerrordsc_Z ;
   protected String sTagName ;
   protected String GXt_char2 ;
}


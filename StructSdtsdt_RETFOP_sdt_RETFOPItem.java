
public final  class StructSdtsdt_RETFOP_sdt_RETFOPItem implements Cloneable, java.io.Serializable
{
   public StructSdtsdt_RETFOP_sdt_RETFOPItem( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq = (byte)(0) ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment = 0 ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal = 0 ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getFpac( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac ;
   }

   public void setFpac( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac = value ;
      return  ;
   }

   public String getFpam( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam ;
   }

   public void setFpam( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam = value ;
      return  ;
   }

   public String getAplc( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc ;
   }

   public void setAplc( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc = value ;
      return  ;
   }

   public String getCutp( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp ;
   }

   public void setCutp( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp = value ;
      return  ;
   }

   public String getExpc( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc ;
   }

   public void setExpc( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc = value ;
      return  ;
   }

   public String getFptp( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp ;
   }

   public void setFptp( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp = value ;
      return  ;
   }

   public String getExda( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda ;
   }

   public void setExda( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda = value ;
      return  ;
   }

   public String getCstf( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf ;
   }

   public void setCstf( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf = value ;
      return  ;
   }

   public String getCrcc( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc ;
   }

   public void setCrcc( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc = value ;
      return  ;
   }

   public String getAvcd( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd ;
   }

   public void setAvcd( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd = value ;
      return  ;
   }

   public String getSapp( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp ;
   }

   public void setSapp( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp = value ;
      return  ;
   }

   public String getFpti( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti ;
   }

   public void setFpti( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti = value ;
      return  ;
   }

   public String getAuta( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta ;
   }

   public void setAuta( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta = value ;
      return  ;
   }

   public String getIt08pos( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos ;
   }

   public void setIt08pos( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos = value ;
      return  ;
   }

   public byte getIt08seq( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq ;
   }

   public void setIt08seq( byte value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq = value ;
      return  ;
   }

   public double getFirstinstallment( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment ;
   }

   public void setFirstinstallment( double value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment = value ;
      return  ;
   }

   public double getFareoriginal( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal ;
   }

   public void setFareoriginal( double value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal = value ;
      return  ;
   }

   protected byte gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq ;
   protected double gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment ;
   protected double gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta ;
   protected String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos ;
}


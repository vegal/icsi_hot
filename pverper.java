/*
               File: VerPer
        Description: Verifica Periodo
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:26.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pverper extends GXProcedure
{
   public pverper( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pverper.class ), "" );
   }

   public pverper( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 ,
                        String[] aP5 ,
                        String[] aP6 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5, aP6);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 ,
                             String[] aP5 ,
                             String[] aP6 )
   {
      pverper.this.AV13ciacod = aP0[0];
      this.aP0 = aP0;
      pverper.this.AV62Period = aP1[0];
      this.aP1 = aP1;
      pverper.this.AV45Modo = aP2[0];
      this.aP2 = aP2;
      pverper.this.AV28ErrorM = aP3[0];
      this.aP3 = aP3;
      pverper.this.AV82VerNum = aP4[0];
      this.aP4 = aP4;
      pverper.this.AV78Type = aP5[0];
      this.aP5 = aP5;
      pverper.this.AV9Arquivo = aP6[0];
      this.aP6 = aP6;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      /* Using cursor P005G2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A989HntRec = P005G2_A989HntRec[0] ;
         n989HntRec = P005G2_n989HntRec[0] ;
         A994HntLin = P005G2_A994HntLin[0] ;
         n994HntLin = P005G2_n994HntLin[0] ;
         A997HntSeq = P005G2_A997HntSeq[0] ;
         A998HntSub = P005G2_A998HntSub[0] ;
         if ( ( GXutil.strcmp(A989HntRec, "BFH") == 0 ) )
         {
            AV13ciacod = GXutil.substring( A994HntLin, 17, 3) ;
         }
         if ( ( GXutil.strcmp(A989HntRec, "BCH") == 0 ) )
         {
            AV62Period = GXutil.substring( A994HntLin, 18, 2) + GXutil.substring( A994HntLin, 20, 2) + GXutil.substring( A994HntLin, 22, 2) ;
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
      GXv_char1[0] = AV13ciacod ;
      GXv_char2[0] = AV62Period ;
      GXv_char3[0] = AV45Modo ;
      GXv_char4[0] = AV28ErrorM ;
      GXv_char5[0] = AV82VerNum ;
      GXv_int6[0] = 0 ;
      GXv_char7[0] = AV78Type ;
      GXv_char8[0] = AV9Arquivo ;
      new ppeginf(remoteHandle, context).execute( GXv_char1, GXv_char2, GXv_char3, GXv_char4, GXv_char5, GXv_int6, GXv_char7, GXv_char8) ;
      pverper.this.AV13ciacod = GXv_char1[0] ;
      pverper.this.AV62Period = GXv_char2[0] ;
      pverper.this.AV45Modo = GXv_char3[0] ;
      pverper.this.AV28ErrorM = GXv_char4[0] ;
      pverper.this.AV82VerNum = GXv_char5[0] ;
      pverper.this.AV78Type = GXv_char7[0] ;
      pverper.this.AV9Arquivo = GXv_char8[0] ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pverper.this.AV13ciacod;
      this.aP1[0] = pverper.this.AV62Period;
      this.aP2[0] = pverper.this.AV45Modo;
      this.aP3[0] = pverper.this.AV28ErrorM;
      this.aP4[0] = pverper.this.AV82VerNum;
      this.aP5[0] = pverper.this.AV78Type;
      this.aP6[0] = pverper.this.AV9Arquivo;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      P005G2_A989HntRec = new String[] {""} ;
      P005G2_n989HntRec = new boolean[] {false} ;
      P005G2_A994HntLin = new String[] {""} ;
      P005G2_n994HntLin = new boolean[] {false} ;
      P005G2_A997HntSeq = new String[] {""} ;
      P005G2_A998HntSub = new byte[1] ;
      A989HntRec = "" ;
      n989HntRec = false ;
      A994HntLin = "" ;
      n994HntLin = false ;
      A997HntSeq = "" ;
      A998HntSub = (byte)(0) ;
      GXv_char1 = new String [1] ;
      GXv_char2 = new String [1] ;
      GXv_char3 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_char5 = new String [1] ;
      GXv_int6 = new int [1] ;
      GXv_char7 = new String [1] ;
      GXv_char8 = new String [1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pverper__default(),
         new Object[] {
             new Object[] {
            P005G2_A989HntRec, P005G2_n989HntRec, P005G2_A994HntLin, P005G2_n994HntLin, P005G2_A997HntSeq, P005G2_A998HntSub
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte A998HntSub ;
   private short Gx_err ;
   private int GXv_int6[] ;
   private String AV13ciacod ;
   private String AV62Period ;
   private String AV45Modo ;
   private String AV28ErrorM ;
   private String AV82VerNum ;
   private String AV78Type ;
   private String AV9Arquivo ;
   private String scmdbuf ;
   private String A989HntRec ;
   private String A997HntSeq ;
   private String GXv_char1[] ;
   private String GXv_char2[] ;
   private String GXv_char3[] ;
   private String GXv_char4[] ;
   private String GXv_char5[] ;
   private String GXv_char7[] ;
   private String GXv_char8[] ;
   private boolean n989HntRec ;
   private boolean n994HntLin ;
   private String A994HntLin ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
   private String[] aP5 ;
   private String[] aP6 ;
   private IDataStoreProvider pr_default ;
   private String[] P005G2_A989HntRec ;
   private boolean[] P005G2_n989HntRec ;
   private String[] P005G2_A994HntLin ;
   private boolean[] P005G2_n994HntLin ;
   private String[] P005G2_A997HntSeq ;
   private byte[] P005G2_A998HntSub ;
}

final  class pverper__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P005G2", "SELECT [HntRecId], [HntLine], [HntSeq], [HntSub] FROM [RETSPECTEMP] WITH (NOLOCK) ORDER BY [HntSeq] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 8) ;
               ((byte[]) buf[5])[0] = rslt.getByte(4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

}


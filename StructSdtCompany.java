
public final  class StructSdtCompany implements Cloneable, java.io.Serializable
{
   public StructSdtCompany( )
   {
      gxTv_SdtCompany_Companycod = "" ;
      gxTv_SdtCompany_Companydes = "" ;
      gxTv_SdtCompany_Mode = "" ;
      gxTv_SdtCompany_Companycod_Z = "" ;
      gxTv_SdtCompany_Companydes_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getCompanycod( )
   {
      return gxTv_SdtCompany_Companycod ;
   }

   public void setCompanycod( String value )
   {
      gxTv_SdtCompany_Companycod = value ;
      return  ;
   }

   public String getCompanydes( )
   {
      return gxTv_SdtCompany_Companydes ;
   }

   public void setCompanydes( String value )
   {
      gxTv_SdtCompany_Companydes = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtCompany_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtCompany_Mode = value ;
      return  ;
   }

   public String getCompanycod_Z( )
   {
      return gxTv_SdtCompany_Companycod_Z ;
   }

   public void setCompanycod_Z( String value )
   {
      gxTv_SdtCompany_Companycod_Z = value ;
      return  ;
   }

   public String getCompanydes_Z( )
   {
      return gxTv_SdtCompany_Companydes_Z ;
   }

   public void setCompanydes_Z( String value )
   {
      gxTv_SdtCompany_Companydes_Z = value ;
      return  ;
   }

   protected String gxTv_SdtCompany_Mode ;
   protected String gxTv_SdtCompany_Companycod ;
   protected String gxTv_SdtCompany_Companydes ;
   protected String gxTv_SdtCompany_Companycod_Z ;
   protected String gxTv_SdtCompany_Companydes_Z ;
}


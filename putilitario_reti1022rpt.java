/*
               File: UTILITARIO_RETi1022rpt
        Description: UTILITARIO_ RETi1022rpt
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:25.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;
import com.genexus.reports.*;

public final  class putilitario_reti1022rpt extends GXReport
{
   public putilitario_reti1022rpt( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( putilitario_reti1022rpt.class ), "" );
   }

   public putilitario_reti1022rpt( int remoteHandle ,
                                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 ,
                        java.util.Date[] aP5 ,
                        java.util.Date[] aP6 ,
                        String[] aP7 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5, aP6, aP7);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 ,
                             java.util.Date[] aP5 ,
                             java.util.Date[] aP6 ,
                             String[] aP7 )
   {
      putilitario_reti1022rpt.this.AV20lccbEm = aP0[0];
      this.aP0 = aP0;
      putilitario_reti1022rpt.this.AV21EmpNom = aP1[0];
      this.aP1 = aP1;
      putilitario_reti1022rpt.this.AV22FilePD = aP2[0];
      this.aP2 = aP2;
      putilitario_reti1022rpt.this.AV23FileTX = aP3[0];
      this.aP3 = aP3;
      putilitario_reti1022rpt.this.AV31DebugM = aP4[0];
      this.aP4 = aP4;
      putilitario_reti1022rpt.this.AV67DateFr = aP5[0];
      this.aP5 = aP5;
      putilitario_reti1022rpt.this.AV68DateTo = aP6[0];
      this.aP6 = aP6;
      putilitario_reti1022rpt.this.AV73Modo = aP7[0];
      this.aP7 = aP7;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      M_top = 0 ;
      M_bot = 4 ;
      P_lines = (int)(66-M_bot) ;
      getPrinter().GxClearAttris() ;
      add_metrics( ) ;
      lineHeight = 16 ;
      PrtOffset = 0 ;
      gxXPage = 96 ;
      gxYPage = 96 ;
      getPrinter().GxSetDocName(AV22FilePD) ;
      getPrinter().GxSetDocFormat("PDF") ;
      try
      {
         Gx_out = "FIL" ;
         if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 9, 16838, 11906, 0, 1, 1, 0, 1, 1) )
         {
            cleanup();
            return;
         }
         getPrinter().setModal(true) ;
         P_lines = (int)(gxYPage-(lineHeight*4)) ;
         Gx_line = (int)(P_lines+1) ;
         getPrinter().setPageLines(P_lines);
         getPrinter().setLineHeight(lineHeight);
         getPrinter().setM_top(M_top);
         getPrinter().setM_bot(M_bot);
         GXt_char1 = AV65CarFim ;
         GXv_svchar2[0] = GXt_char1 ;
         new pr2getparm(remoteHandle, context).execute( "UTILIZA_LINHA_FIM", "S= utiliza Char(20) / N= NewLine() ", "S", "N", GXv_svchar2) ;
         putilitario_reti1022rpt.this.GXt_char1 = GXv_svchar2[0] ;
         AV65CarFim = GXt_char1 ;
         AV32i = (short)(GXutil.strSearch( AV31DebugM, "PROCDATE=", 1)) ;
         if ( ( AV32i > 0 ) )
         {
            AV32i = (short)(AV32i+9) ;
            AV27sOutpu = GXutil.trim( GXutil.substring( AV31DebugM, AV32i, 10)) ;
         }
         else
         {
            GXt_char1 = AV27sOutpu ;
            GXv_svchar2[0] = GXt_char1 ;
            new pr2getparm(remoteHandle, context).execute( "ICSIDiaRep4122", "Dia Relat�rio 4122", "S", "[TODAY]", GXv_svchar2) ;
            putilitario_reti1022rpt.this.GXt_char1 = GXv_svchar2[0] ;
            AV27sOutpu = GXt_char1 ;
            AV27sOutpu = GXutil.trim( AV27sOutpu) ;
         }
         if ( ( GXutil.strcmp(AV27sOutpu, "[TODAY]") == 0 ) )
         {
            AV29lccbSu = GXutil.resetTime( Gx_date );
         }
         else
         {
            AV29lccbSu = GXutil.resetTime( localUtil.ctod( AV27sOutpu, 2) );
         }
         AV35Decisa = "?" ;
         AV36Decisa = "?" ;
         AV37Decisa = "?" ;
         AV38sDecis = "" ;
         AV39sDecis = "" ;
         AV40sDecis = "" ;
         AV89GXLvl5 = (byte)(0) ;
         /* Using cursor P00792 */
         pr_default.execute(0, new Object[] {AV20lccbEm});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1147lccbE = P00792_A1147lccbE[0] ;
            n1147lccbE = P00792_n1147lccbE[0] ;
            A1150lccbE = P00792_A1150lccbE[0] ;
            A1166lccbP = P00792_A1166lccbP[0] ;
            A1165lccbD = P00792_A1165lccbD[0] ;
            n1165lccbD = P00792_n1165lccbD[0] ;
            A1147lccbE = P00792_A1147lccbE[0] ;
            n1147lccbE = P00792_n1147lccbE[0] ;
            AV89GXLvl5 = (byte)(1) ;
            if ( ( GXutil.strcmp(A1166lccbP, "1") == 0 ) )
            {
               AV35Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "V") == 0 ) )
               {
                  AV38sDecis = "Ser� faturado como CC � vista" ;
               }
               else if ( ( GXutil.strcmp(A1165lccbD, "C") == 0 ) || ( GXutil.strcmp(A1165lccbD, "Y") == 0 ) )
               {
                  AV38sDecis = "Transformar para Cash (N�O USADO NO ICSI, CONTACTE A R2TECH !)" ;
               }
               else
               {
                  AV38sDecis = "Faturar fora do ICSI" ;
               }
            }
            else if ( ( GXutil.strcmp(A1166lccbP, "2") == 0 ) )
            {
               AV36Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "R") == 0 ) || ( GXutil.strcmp(A1165lccbD, "N") == 0 ) )
               {
                  AV39sDecis = "Processar com informa��es da RET, aceitando o valor da parcela dado pelo agente" ;
               }
               else
               {
                  AV39sDecis = "Faturar fora do ICSI" ;
               }
            }
            else if ( ( GXutil.strcmp(A1166lccbP, "3") == 0 ) )
            {
               AV37Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "R") == 0 ) || ( GXutil.strcmp(A1165lccbD, "Y") == 0 ) )
               {
                  AV40sDecis = "Processar com informa��es da RET, aceitando o valor da parcela dado pelo agente" ;
               }
               else
               {
                  AV40sDecis = "Faturar fora do ICSI" ;
               }
            }
            else if ( ( GXutil.strcmp(A1166lccbP, "4") == 0 ) )
            {
               AV78Decisa = A1165lccbD ;
               if ( ( GXutil.strcmp(A1165lccbD, "R") == 0 ) )
               {
                  AV77SDecis = "Processar com informa��es da RET, aceitando o valor da parcela dado pelo agente" ;
               }
               else
               {
                  AV77SDecis = "Rejeitar a transa��o" ;
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( ( AV89GXLvl5 == 0 ) )
         {
         }
         h790( false, 178) ;
         getPrinter().GxDrawRect(7, Gx_line+2, 1001, Gx_line+168, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV35Decisa, "X")), 31, Gx_line+24, 43, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV38sDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 67, Gx_line+24, 546, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV36Decisa, "X")), 31, Gx_line+59, 43, Gx_line+75, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV39sDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 67, Gx_line+59, 546, Gx_line+75, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV37Decisa, "X")), 31, Gx_line+93, 43, Gx_line+109, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV40sDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 67, Gx_line+93, 546, Gx_line+109, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV77SDecis, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 67, Gx_line+127, 546, Gx_line+143, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV78Decisa, "X")), 31, Gx_line+127, 43, Gx_line+143, 0+256) ;
         getPrinter().GxAttris("MS Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Problema 1: Falta da quantidade de presta��es e/ou valor da parcela", 16, Gx_line+8, 411, Gx_line+21, 0+256) ;
         getPrinter().GxDrawText("Problema 2: Falta do C�digo de Parcelamento", 16, Gx_line+43, 275, Gx_line+56, 0+256) ;
         getPrinter().GxDrawText("Problema 3: Quantidade e/ou valor de presta��o diferentes do plano", 17, Gx_line+77, 407, Gx_line+90, 0+256) ;
         getPrinter().GxDrawText("Problema 4: Plano informado n�o acomoda quantidade de parcelas", 17, Gx_line+111, 396, Gx_line+124, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+178) ;
         /* Execute user subroutine: S1196 */
         S1196 ();
         if ( returnInSub )
         {
         }
         /* Print footer for last page */
         ToSkip = (int)(P_lines+1) ;
         h790( true, 0) ;
         /* Close printer file */
         getPrinter().GxEndDocument() ;
         endPrinter();
      }
      catch ( ProcessInterruptedException e )
      {
      }
      cleanup();
   }

   public void S1196( ) throws ProcessInterruptedException
   {
      /* 'MAIN' Routine */
      if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
      {
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV23FileTX, "", "", (byte)(0), "226") ;
      }
      else
      {
         AV66xmlWri.openURL(AV23FileTX);
      }
      AV63Date = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      AV62DateCa = GXutil.trim( GXutil.str( GXutil.year( AV63Date), 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV63Date), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV63Date), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.hour( AV63Date), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.minute( AV63Date), 10, 0)), (short)(2), "0") ;
      AV43LinhaA = "C" + "000000000000" + GXutil.padr( GXutil.trim( AV62DateCa), (short)(12), " ") + " 4122" ;
      if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
      {
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(254)) ;
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      else
      {
         AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(226), " ") ;
         AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
         AV66xmlWri.writeRawText(AV43LinhaA);
      }
      AV30nSeq = 0 ;
      AV71DateFr = GXutil.resetTime( AV67DateFr );
      AV72DateTo = GXutil.resetTime( AV68DateTo );
      pr_default.dynParam(1, new Object[]{ new Object[]{
                                           AV73Modo ,
                                           A1299SCEDa ,
                                           AV71DateFr ,
                                           AV72DateTo ,
                                           A1309SCEFo ,
                                           A1298SCETk ,
                                           AV20lccbEm ,
                                           A1301SCEAi ,
                                           A1306SCEAP },
                                           new int[] {
                                           TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                           TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                           }
      });
      /* Using cursor P00793 */
      pr_default.execute(1, new Object[] {AV20lccbEm, AV20lccbEm});
      while ( (pr_default.getStatus(1) != 101) )
      {
         A1301SCEAi = P00793_A1301SCEAi[0] ;
         n1301SCEAi = P00793_n1301SCEAi[0] ;
         A1306SCEAP = P00793_A1306SCEAP[0] ;
         n1306SCEAP = P00793_n1306SCEAP[0] ;
         A1309SCEFo = P00793_A1309SCEFo[0] ;
         n1309SCEFo = P00793_n1309SCEFo[0] ;
         A1298SCETk = P00793_A1298SCETk[0] ;
         n1298SCETk = P00793_n1298SCETk[0] ;
         A1299SCEDa = P00793_A1299SCEDa[0] ;
         n1299SCEDa = P00793_n1299SCEDa[0] ;
         A1307SCEIa = P00793_A1307SCEIa[0] ;
         n1307SCEIa = P00793_n1307SCEIa[0] ;
         A1310SCETp = P00793_A1310SCETp[0] ;
         n1310SCETp = P00793_n1310SCETp[0] ;
         A1305SCETe = P00793_A1305SCETe[0] ;
         n1305SCETe = P00793_n1305SCETe[0] ;
         A1312SCEId = P00793_A1312SCEId[0] ;
         if ( ( GXutil.strcmp(A1298SCETk, "") != 0 ) )
         {
            AV46SCETkt = A1298SCETk ;
            AV70Encont = "N" ;
            /* Using cursor P00794 */
            pr_default.execute(2, new Object[] {AV46SCETkt});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1231lccbT = P00794_A1231lccbT[0] ;
               A1150lccbE = P00794_A1150lccbE[0] ;
               A1222lccbI = P00794_A1222lccbI[0] ;
               A1223lccbD = P00794_A1223lccbD[0] ;
               A1224lccbC = P00794_A1224lccbC[0] ;
               A1225lccbC = P00794_A1225lccbC[0] ;
               A1226lccbA = P00794_A1226lccbA[0] ;
               A1227lccbO = P00794_A1227lccbO[0] ;
               A1228lccbF = P00794_A1228lccbF[0] ;
               A1232lccbT = P00794_A1232lccbT[0] ;
               AV70Encont = "Y" ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( ( GXutil.strcmp(AV70Encont, "N") == 0 ) )
            {
               AV30nSeq = (int)(AV30nSeq+1) ;
               AV54nSeqCa = GXutil.trim( GXutil.str( AV30nSeq, 10, 0)) ;
               AV45Empres = AV20lccbEm ;
               AV44IATA = GXutil.substring( GXutil.trim( A1307SCEIa), 1, 7) ;
               AV59SCETpE = A1310SCETp ;
               AV60SCETex = A1305SCETe ;
               AV48lccbSa = 0 ;
               AV50lccbTi = 0 ;
               AV52lccbDa = GXutil.resetTime(A1299SCEDa) ;
               /* Using cursor P00795 */
               pr_default.execute(3, new Object[] {AV46SCETkt});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A966CODE = P00795_A966CODE[0] ;
                  A968NUM_BI = P00795_A968NUM_BI[0] ;
                  A874AIRPT_ = P00795_A874AIRPT_[0] ;
                  n874AIRPT_ = P00795_n874AIRPT_[0] ;
                  A875TAX_1 = P00795_A875TAX_1[0] ;
                  n875TAX_1 = P00795_n875TAX_1[0] ;
                  A970DATA = P00795_A970DATA[0] ;
                  A963ISOC = P00795_A963ISOC[0] ;
                  A964CiaCod = P00795_A964CiaCod[0] ;
                  A965PER_NA = P00795_A965PER_NA[0] ;
                  A967IATA = P00795_A967IATA[0] ;
                  A969TIPO_V = P00795_A969TIPO_V[0] ;
                  AV48lccbSa = A874AIRPT_ ;
                  AV50lccbTi = A875TAX_1 ;
                  AV52lccbDa = A970DATA ;
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               /* Using cursor P00796 */
               pr_default.execute(4, new Object[] {AV46SCETkt});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1150lccbE = P00796_A1150lccbE[0] ;
                  A1222lccbI = P00796_A1222lccbI[0] ;
                  A1223lccbD = P00796_A1223lccbD[0] ;
                  A1224lccbC = P00796_A1224lccbC[0] ;
                  A1225lccbC = P00796_A1225lccbC[0] ;
                  A1226lccbA = P00796_A1226lccbA[0] ;
                  A1227lccbO = P00796_A1227lccbO[0] ;
                  A1228lccbF = P00796_A1228lccbF[0] ;
                  A1231lccbT = P00796_A1231lccbT[0] ;
                  A1178lccbO = P00796_A1178lccbO[0] ;
                  n1178lccbO = P00796_n1178lccbO[0] ;
                  A1176lccbO = P00796_A1176lccbO[0] ;
                  n1176lccbO = P00796_n1176lccbO[0] ;
                  A1177lccbO = P00796_A1177lccbO[0] ;
                  n1177lccbO = P00796_n1177lccbO[0] ;
                  A1172lccbS = P00796_A1172lccbS[0] ;
                  n1172lccbS = P00796_n1172lccbS[0] ;
                  A1170lccbI = P00796_A1170lccbI[0] ;
                  n1170lccbI = P00796_n1170lccbI[0] ;
                  A1171lccbT = P00796_A1171lccbT[0] ;
                  n1171lccbT = P00796_n1171lccbT[0] ;
                  A1232lccbT = P00796_A1232lccbT[0] ;
                  A1178lccbO = P00796_A1178lccbO[0] ;
                  n1178lccbO = P00796_n1178lccbO[0] ;
                  A1176lccbO = P00796_A1176lccbO[0] ;
                  n1176lccbO = P00796_n1176lccbO[0] ;
                  A1177lccbO = P00796_A1177lccbO[0] ;
                  n1177lccbO = P00796_n1177lccbO[0] ;
                  A1172lccbS = P00796_A1172lccbS[0] ;
                  n1172lccbS = P00796_n1172lccbS[0] ;
                  A1170lccbI = P00796_A1170lccbI[0] ;
                  n1170lccbI = P00796_n1170lccbI[0] ;
                  A1171lccbT = P00796_A1171lccbT[0] ;
                  n1171lccbT = P00796_n1171lccbT[0] ;
                  AV80lccbOr = A1178lccbO ;
                  AV82lccbOr = A1176lccbO ;
                  AV83lccbOr = A1177lccbO ;
                  AV48lccbSa = A1172lccbS ;
                  AV84lccbIn = A1170lccbI ;
                  AV50lccbTi = A1171lccbT ;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               AV49lccbSa = GXutil.strReplace( GXutil.trim( GXutil.str( AV48lccbSa, 12, 2)), ".", "") ;
               AV51lccbTi = GXutil.strReplace( GXutil.trim( GXutil.str( AV50lccbTi, 12, 2)), ".", "") ;
               AV53lccbDa = GXutil.trim( GXutil.str( GXutil.year( AV52lccbDa), 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV52lccbDa), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV52lccbDa), 10, 0)), (short)(2), "0") ;
               AV58lccbIn = "0" ;
               AV43LinhaA = "6" + GXutil.padl( GXutil.trim( AV54nSeqCa), (short)(7), "0") + GXutil.padl( GXutil.trim( AV45Empres), (short)(3), "0") + GXutil.padl( GXutil.trim( AV44IATA), (short)(7), "0") + "0000000000" + GXutil.space( (short)(7)) + GXutil.padr( GXutil.trim( AV47lccbCu), (short)(3), " ") ;
               AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV49lccbSa), (short)(11), "0") + "0000000000000000000" + GXutil.padl( GXutil.trim( AV51lccbTi), (short)(11), "0") + GXutil.padl( GXutil.trim( AV46SCETkt), (short)(10), "0") + "0000" + GXutil.space( (short)(3)) + GXutil.space( (short)(8)) ;
               AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV53lccbDa), (short)(8), " ") + GXutil.padr( GXutil.trim( AV55lccbCC), (short)(2), " ") + GXutil.padl( GXutil.trim( AV49lccbSa), (short)(11), "0") + "00000000000" + GXutil.padl( GXutil.trim( AV58lccbIn), (short)(2), "0") ;
               AV43LinhaA = AV43LinhaA + "00000000000" + GXutil.padl( GXutil.trim( AV59SCETpE), (short)(3), "0") + GXutil.padr( GXutil.trim( AV60SCETex), (short)(40), " ") + GXutil.padr( GXutil.trim( AV61lccbCC), (short)(20), " ") + GXutil.space( (short)(6)) + GXutil.space( (short)(6)) + "DB" ;
               if ( ( GXutil.strcmp(A1309SCEFo, "W") == 0 ) )
               {
                  AV81lccbOr = (double)(AV82lccbOr+AV83lccbOr) ;
                  AV43LinhaA = AV43LinhaA + GXutil.trim( A1309SCEFo) ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV80lccbOr, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV81lccbOr, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV82lccbOr, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV79Indica = "Aviso:" ;
               }
               else if ( ( GXutil.strcmp(A1309SCEFo, "E") == 0 ) )
               {
                  AV94Lccbdo = (double)(AV84lccbIn+AV50lccbTi) ;
                  AV43LinhaA = AV43LinhaA + GXutil.trim( A1309SCEFo) ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV48lccbSa, "ZZZZZZZZZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV94Lccbdo, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV84lccbIn, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV79Indica = "Erro:" ;
               }
               if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
               {
                  AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(254)) ;
                  AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
               }
               else
               {
                  AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(226), " ") ;
                  AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
                  AV66xmlWri.writeRawText(AV43LinhaA);
               }
               AV33s1 = GXutil.substring( GXutil.trim( A1305SCETe), 1, 90) ;
               h790( false, 44) ;
               getPrinter().GxDrawLine(7, Gx_line+42, 1002, Gx_line+42, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1298SCETk, "XXXXXXXXXX")), 82, Gx_line+5, 131, Gx_line+19, 0+256) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1307SCEIa, "@!")), 82, Gx_line+26, 136, Gx_line+40, 0+256) ;
               getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("C�d. IATA", 8, Gx_line+26, 59, Gx_line+40, 0+256) ;
               getPrinter().GxDrawText("Documento", 8, Gx_line+5, 70, Gx_line+19, 0+256) ;
               getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV33s1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 300, Gx_line+3, 839, Gx_line+19, 0+256) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV34s2, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 300, Gx_line+24, 839, Gx_line+40, 0+256) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1310SCETp, "XXX")), 250, Gx_line+3, 288, Gx_line+19, 0+256) ;
               getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 255, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV79Indica, "XXXXXXX")), 142, Gx_line+3, 232, Gx_line+19, 0+256) ;
               Gx_OldLine = Gx_line ;
               Gx_line = (int)(Gx_line+44) ;
               if ( ( GXutil.strcmp(AV73Modo, "G") == 0 ) )
               {
                  A1309SCEFo = "1" ;
                  n1309SCEFo = false ;
               }
            }
            /* Using cursor P00797 */
            pr_default.execute(5, new Object[] {new Boolean(n1309SCEFo), A1309SCEFo, new Long(A1312SCEId)});
         }
         pr_default.readNext(1);
      }
      pr_default.close(1);
      AV95GXLvl2 = (byte)(0) ;
      pr_default.dynParam(6, new Object[]{ new Object[]{
                                           AV73Modo ,
                                           A1223lccbD ,
                                           AV67DateFr ,
                                           AV68DateTo ,
                                           A1191lccbS ,
                                           A1184lccbS ,
                                           AV20lccbEm ,
                                           A1150lccbE },
                                           new int[] {
                                           TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                           }
      });
      /* Using cursor P00798 */
      pr_default.execute(6, new Object[] {AV20lccbEm});
      while ( (pr_default.getStatus(6) != 101) )
      {
         A1227lccbO = P00798_A1227lccbO[0] ;
         A1228lccbF = P00798_A1228lccbF[0] ;
         A1191lccbS = P00798_A1191lccbS[0] ;
         n1191lccbS = P00798_n1191lccbS[0] ;
         A1223lccbD = P00798_A1223lccbD[0] ;
         A1184lccbS = P00798_A1184lccbS[0] ;
         n1184lccbS = P00798_n1184lccbS[0] ;
         A1150lccbE = P00798_A1150lccbE[0] ;
         A1181lccbC = P00798_A1181lccbC[0] ;
         n1181lccbC = P00798_n1181lccbC[0] ;
         A1172lccbS = P00798_A1172lccbS[0] ;
         n1172lccbS = P00798_n1172lccbS[0] ;
         A1171lccbT = P00798_A1171lccbT[0] ;
         n1171lccbT = P00798_n1171lccbT[0] ;
         A1224lccbC = P00798_A1224lccbC[0] ;
         A1226lccbA = P00798_A1226lccbA[0] ;
         A1168lccbI = P00798_A1168lccbI[0] ;
         n1168lccbI = P00798_n1168lccbI[0] ;
         A1178lccbO = P00798_A1178lccbO[0] ;
         n1178lccbO = P00798_n1178lccbO[0] ;
         A1175lccbO = P00798_A1175lccbO[0] ;
         n1175lccbO = P00798_n1175lccbO[0] ;
         A1176lccbO = P00798_A1176lccbO[0] ;
         n1176lccbO = P00798_n1176lccbO[0] ;
         A1177lccbO = P00798_A1177lccbO[0] ;
         n1177lccbO = P00798_n1177lccbO[0] ;
         A1225lccbC = P00798_A1225lccbC[0] ;
         A1231lccbT = P00798_A1231lccbT[0] ;
         A1222lccbI = P00798_A1222lccbI[0] ;
         A1232lccbT = P00798_A1232lccbT[0] ;
         A1191lccbS = P00798_A1191lccbS[0] ;
         n1191lccbS = P00798_n1191lccbS[0] ;
         A1184lccbS = P00798_A1184lccbS[0] ;
         n1184lccbS = P00798_n1184lccbS[0] ;
         A1181lccbC = P00798_A1181lccbC[0] ;
         n1181lccbC = P00798_n1181lccbC[0] ;
         A1172lccbS = P00798_A1172lccbS[0] ;
         n1172lccbS = P00798_n1172lccbS[0] ;
         A1171lccbT = P00798_A1171lccbT[0] ;
         n1171lccbT = P00798_n1171lccbT[0] ;
         A1168lccbI = P00798_A1168lccbI[0] ;
         n1168lccbI = P00798_n1168lccbI[0] ;
         A1178lccbO = P00798_A1178lccbO[0] ;
         n1178lccbO = P00798_n1178lccbO[0] ;
         A1175lccbO = P00798_A1175lccbO[0] ;
         n1175lccbO = P00798_n1175lccbO[0] ;
         A1176lccbO = P00798_A1176lccbO[0] ;
         n1176lccbO = P00798_n1176lccbO[0] ;
         A1177lccbO = P00798_A1177lccbO[0] ;
         n1177lccbO = P00798_n1177lccbO[0] ;
         if ( ( GXutil.strcmp(A1184lccbS, "NOSUB") == 0 ) )
         {
            AV95GXLvl2 = (byte)(1) ;
            AV47lccbCu = A1181lccbC ;
            AV48lccbSa = A1172lccbS ;
            AV50lccbTi = A1171lccbT ;
            AV52lccbDa = A1223lccbD ;
            AV55lccbCC = A1224lccbC ;
            AV56lccbAp = A1226lccbA ;
            AV57lccbIn = A1168lccbI ;
            AV80lccbOr = A1178lccbO ;
            AV81lccbOr = A1175lccbO ;
            AV82lccbOr = A1176lccbO ;
            AV83lccbOr = A1177lccbO ;
            AV74Atribu = A1225lccbC ;
            GXv_svchar2[0] = AV76Result ;
            new pcrypto(remoteHandle, context).execute( AV74Atribu, "D", GXv_svchar2) ;
            putilitario_reti1022rpt.this.AV76Result = GXv_svchar2[0] ;
            AV61lccbCC = AV76Result ;
            AV64lccbst = A1184lccbS ;
            AV46SCETkt = A1231lccbT ;
            AV69lccbIA = A1222lccbI ;
            if ( ( GXutil.strcmp(AV73Modo, "G") == 0 ) )
            {
               A1191lccbS = "1" ;
               n1191lccbS = false ;
            }
            /* Execute user subroutine: S129 */
            S129 ();
            if ( returnInSub )
            {
               pr_default.close(6);
               pr_default.close(6);
               getPrinter().GxEndPage() ;
               /* Close printer file */
               getPrinter().GxEndDocument() ;
               endPrinter();
               returnInSub = true;
               if (true) return;
            }
            /* Using cursor P00799 */
            pr_default.execute(7, new Object[] {new Boolean(n1191lccbS), A1191lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
         }
         pr_default.readNext(6);
      }
      pr_default.close(6);
      if ( ( AV95GXLvl2 == 0 ) )
      {
         h790( false, 18) ;
         getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Sem ocorr�ncias a registrar", 39, Gx_line+3, 193, Gx_line+17, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+18) ;
      }
      AV43LinhaA = "F" + GXutil.padl( GXutil.trim( AV54nSeqCa), (short)(7), "0") ;
      if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
      {
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(254)) ;
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      }
      else
      {
         AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(226), " ") ;
         AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
         AV66xmlWri.writeRawText(AV43LinhaA);
         AV66xmlWri.close();
      }
   }

   public void S129( ) throws ProcessInterruptedException
   {
      /* 'VERIFICASCEVENTS' Routine */
      AV96GXLvl2 = (byte)(0) ;
      /* Using cursor P007910 */
      pr_default.execute(8, new Object[] {AV20lccbEm, AV20lccbEm, AV46SCETkt});
      while ( (pr_default.getStatus(8) != 101) )
      {
         A1301SCEAi = P007910_A1301SCEAi[0] ;
         n1301SCEAi = P007910_n1301SCEAi[0] ;
         A1298SCETk = P007910_A1298SCETk[0] ;
         n1298SCETk = P007910_n1298SCETk[0] ;
         A1306SCEAP = P007910_A1306SCEAP[0] ;
         n1306SCEAP = P007910_n1306SCEAP[0] ;
         A1307SCEIa = P007910_A1307SCEIa[0] ;
         n1307SCEIa = P007910_n1307SCEIa[0] ;
         A1310SCETp = P007910_A1310SCETp[0] ;
         n1310SCETp = P007910_n1310SCETp[0] ;
         A1305SCETe = P007910_A1305SCETe[0] ;
         n1305SCETe = P007910_n1305SCETe[0] ;
         A1309SCEFo = P007910_A1309SCEFo[0] ;
         n1309SCEFo = P007910_n1309SCEFo[0] ;
         A1299SCEDa = P007910_A1299SCEDa[0] ;
         n1299SCEDa = P007910_n1299SCEDa[0] ;
         A1312SCEId = P007910_A1312SCEId[0] ;
         if ( ( GXutil.strcmp(A1298SCETk, AV46SCETkt) == 0 ) )
         {
            if ( ( GXutil.strcmp(A1306SCEAP, "ICSI") == 0 ) )
            {
               AV96GXLvl2 = (byte)(1) ;
               AV30nSeq = (int)(AV30nSeq+1) ;
               AV54nSeqCa = GXutil.trim( GXutil.str( AV30nSeq, 10, 0)) ;
               AV45Empres = A1301SCEAi ;
               AV44IATA = GXutil.substring( GXutil.trim( A1307SCEIa), 1, 7) ;
               AV46SCETkt = A1298SCETk ;
               AV59SCETpE = A1310SCETp ;
               AV60SCETex = GXutil.substring( GXutil.trim( A1305SCETe), 1, 40) ;
               AV49lccbSa = GXutil.strReplace( GXutil.trim( GXutil.str( AV48lccbSa, 12, 2)), ".", "") ;
               AV51lccbTi = GXutil.strReplace( GXutil.trim( GXutil.str( AV50lccbTi, 12, 2)), ".", "") ;
               AV53lccbDa = GXutil.trim( GXutil.str( GXutil.year( AV52lccbDa), 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV52lccbDa), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV52lccbDa), 10, 0)), (short)(2), "0") ;
               AV58lccbIn = GXutil.trim( GXutil.str( AV57lccbIn, 10, 0)) ;
               AV43LinhaA = "6" + GXutil.padl( GXutil.trim( AV54nSeqCa), (short)(7), "0") + GXutil.padl( GXutil.trim( AV45Empres), (short)(3), "0") + GXutil.padl( GXutil.trim( AV44IATA), (short)(7), "0") + "0000000000" + GXutil.space( (short)(7)) + GXutil.padr( GXutil.trim( AV47lccbCu), (short)(3), " ") ;
               AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV49lccbSa), (short)(11), "0") + "0000000000000000000" + GXutil.padl( GXutil.trim( AV51lccbTi), (short)(11), "0") + GXutil.padl( GXutil.trim( AV46SCETkt), (short)(10), "0") + "0000" + GXutil.space( (short)(3)) + GXutil.space( (short)(8)) ;
               AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV53lccbDa), (short)(8), " ") + GXutil.padr( GXutil.trim( AV55lccbCC), (short)(2), " ") + GXutil.padl( GXutil.trim( AV49lccbSa), (short)(11), "0") + "00000000000" + GXutil.padl( GXutil.trim( AV58lccbIn), (short)(2), "0") ;
               AV43LinhaA = AV43LinhaA + "00000000000" + GXutil.padl( GXutil.trim( AV59SCETpE), (short)(3), "0") + GXutil.padr( GXutil.trim( AV60SCETex), (short)(40), " ") + GXutil.padr( GXutil.trim( AV61lccbCC), (short)(20), " ") + GXutil.space( (short)(6)) + GXutil.space( (short)(6)) + "DB" ;
               if ( ( GXutil.strcmp(A1309SCEFo, "W") == 0 ) )
               {
                  AV81lccbOr = (double)(AV82lccbOr+AV83lccbOr) ;
                  AV43LinhaA = AV43LinhaA + GXutil.trim( A1309SCEFo) ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV80lccbOr, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV81lccbOr, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV82lccbOr, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV79Indica = "Aviso:" ;
               }
               else if ( ( GXutil.strcmp(A1309SCEFo, "E") == 0 ) )
               {
                  AV94Lccbdo = (double)(AV84lccbIn+AV50lccbTi) ;
                  AV43LinhaA = AV43LinhaA + GXutil.trim( A1309SCEFo) ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV48lccbSa, "ZZZZZZZZZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV94Lccbdo, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.strReplace( GXutil.trim( localUtil.format( AV84lccbIn, "ZZ,ZZZ,ZZZ,ZZ9.99")), ".", ""), (short)(11), "0") ;
                  AV79Indica = "Erro:" ;
               }
               if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
               {
                  AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(254)) ;
                  AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
               }
               else
               {
                  AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(226), " ") ;
                  AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
                  AV66xmlWri.writeRawText(AV43LinhaA);
               }
               AV33s1 = GXutil.left( A1305SCETe, 80) ;
               AV34s2 = GXutil.substring( A1305SCETe, 81, 80) ;
               h790( false, 44) ;
               getPrinter().GxDrawLine(7, Gx_line+42, 1002, Gx_line+42, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1298SCETk, "XXXXXXXXXX")), 82, Gx_line+5, 131, Gx_line+19, 0+256) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1307SCEIa, "@!")), 82, Gx_line+26, 136, Gx_line+40, 0+256) ;
               getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("C�d. IATA", 8, Gx_line+26, 59, Gx_line+40, 0+256) ;
               getPrinter().GxDrawText("Documento", 8, Gx_line+5, 70, Gx_line+19, 0+256) ;
               getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV33s1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 300, Gx_line+3, 839, Gx_line+19, 0+256) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV34s2, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 300, Gx_line+24, 839, Gx_line+40, 0+256) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1310SCETp, "XXX")), 250, Gx_line+3, 288, Gx_line+19, 0+256) ;
               getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 255, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV79Indica, "XXXXXXX")), 142, Gx_line+3, 232, Gx_line+19, 0+256) ;
               Gx_OldLine = Gx_line ;
               Gx_line = (int)(Gx_line+44) ;
               if ( ( GXutil.strcmp(AV73Modo, "G") == 0 ) )
               {
                  A1309SCEFo = "1" ;
                  n1309SCEFo = false ;
               }
               /* Using cursor P007911 */
               pr_default.execute(9, new Object[] {new Boolean(n1309SCEFo), A1309SCEFo, new Long(A1312SCEId)});
            }
         }
         pr_default.readNext(8);
      }
      pr_default.close(8);
      if ( ( AV96GXLvl2 == 0 ) )
      {
         AV30nSeq = (int)(AV30nSeq+1) ;
         AV54nSeqCa = GXutil.trim( GXutil.str( AV30nSeq, 10, 0)) ;
         AV45Empres = AV20lccbEm ;
         AV44IATA = GXutil.substring( GXutil.trim( AV69lccbIA), 1, 7) ;
         AV59SCETpE = "99" ;
         AV60SCETex = "ERRO TRANSACAO NAO SUBMETIDA" ;
         AV49lccbSa = GXutil.strReplace( GXutil.trim( GXutil.str( AV48lccbSa, 12, 2)), ".", "") ;
         AV51lccbTi = GXutil.strReplace( GXutil.trim( GXutil.str( AV50lccbTi, 12, 2)), ".", "") ;
         AV53lccbDa = GXutil.trim( GXutil.str( GXutil.year( AV52lccbDa), 10, 0)) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV52lccbDa), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV52lccbDa), 10, 0)), (short)(2), "0") ;
         AV58lccbIn = GXutil.trim( GXutil.str( AV57lccbIn, 10, 0)) ;
         AV43LinhaA = "6" + GXutil.padl( GXutil.trim( AV54nSeqCa), (short)(7), "0") + GXutil.padl( GXutil.trim( AV45Empres), (short)(3), "0") + GXutil.padl( GXutil.trim( AV44IATA), (short)(7), "0") + "0000000000" + GXutil.space( (short)(7)) + GXutil.padr( GXutil.trim( AV47lccbCu), (short)(3), " ") ;
         AV43LinhaA = AV43LinhaA + GXutil.padl( GXutil.trim( AV49lccbSa), (short)(11), "0") + "0000000000000000000" + GXutil.padl( GXutil.trim( AV51lccbTi), (short)(11), "0") + GXutil.padl( GXutil.trim( AV46SCETkt), (short)(10), "0") + "0000" + GXutil.space( (short)(3)) + GXutil.space( (short)(8)) ;
         AV43LinhaA = AV43LinhaA + GXutil.padr( GXutil.trim( AV53lccbDa), (short)(8), " ") + GXutil.padr( GXutil.trim( AV55lccbCC), (short)(2), " ") + GXutil.padr( GXutil.trim( AV56lccbAp), (short)(6), " ") + "0000000000000000" + GXutil.padl( GXutil.trim( AV58lccbIn), (short)(2), "0") ;
         AV43LinhaA = AV43LinhaA + "00000000000" + GXutil.padl( GXutil.trim( AV59SCETpE), (short)(3), "0") + GXutil.padr( GXutil.trim( AV60SCETex), (short)(40), " ") + GXutil.padr( GXutil.trim( AV61lccbCC), (short)(20), " ") + GXutil.space( (short)(6)) + GXutil.space( (short)(6)) + "DB" ;
         if ( ( GXutil.strcmp(AV65CarFim, "S") == 0 ) )
         {
            AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( GXutil.trim( AV43LinhaA), (short)(254)) ;
            AV24FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
         }
         else
         {
            AV43LinhaA = GXutil.padr( AV43LinhaA, (short)(226), " ") ;
            AV43LinhaA = AV43LinhaA + GXutil.newLine( ) ;
            AV66xmlWri.writeRawText(AV43LinhaA);
         }
         AV33s1 = "ERRO TRANSACAO NAO SUBMETIDA" ;
         h790( false, 44) ;
         getPrinter().GxDrawLine(7, Gx_line+42, 1002, Gx_line+42, 1, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1298SCETk, "XXXXXXXXXX")), 82, Gx_line+5, 131, Gx_line+19, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1307SCEIa, "@!")), 82, Gx_line+26, 136, Gx_line+40, 0+256) ;
         getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("C�d. IATA", 8, Gx_line+26, 59, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText("Documento", 8, Gx_line+5, 70, Gx_line+19, 0+256) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV33s1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 300, Gx_line+3, 839, Gx_line+19, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV34s2, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")), 300, Gx_line+24, 839, Gx_line+40, 0+256) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( A1310SCETp, "XXX")), 250, Gx_line+3, 288, Gx_line+19, 0+256) ;
         getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 255, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV79Indica, "XXXXXXX")), 142, Gx_line+3, 232, Gx_line+19, 0+256) ;
         Gx_OldLine = Gx_line ;
         Gx_line = (int)(Gx_line+44) ;
      }
   }

   public void h790( boolean bFoot ,
                     int Inc )
   {
      /* Skip the required number of lines */
      while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
      {
         if ( ( Gx_line + Inc >= P_lines ) )
         {
            if ( ( Gx_page > 0 ) )
            {
               /* Print footers */
               Gx_line = P_lines ;
               getPrinter().GxDrawLine(6, Gx_line+7, 1001, Gx_line+7, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 8, true, true, false, false, 0, 0, 0, 192, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("R2 Tecnologia", 911, Gx_line+10, 989, Gx_line+23, 0+256) ;
               Gx_OldLine = Gx_line ;
               Gx_line = (int)(Gx_line+24) ;
               if ( ! bFoot )
               {
                  getPrinter().GxEndPage() ;
               }
               if ( bFoot )
               {
                  return  ;
               }
            }
            ToSkip = 0 ;
            Gx_line = 0 ;
            Gx_page = (int)(Gx_page+1) ;
            /* Skip Margin Top Lines */
            Gx_line = (int)(Gx_line+(M_top*lineHeight)) ;
            /* Print headers */
            getPrinter().GxStartPage() ;
            getPrinter().setPage(Gx_page);
            getPrinter().GxDrawRect(6, Gx_line+3, 1001, Gx_line+73, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Arial", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(localUtil.format( Gx_date, "99/99/9999"), 838, Gx_line+28, 891, Gx_line+42, 0+256) ;
            getPrinter().GxDrawText(GXutil.ltrim( localUtil.format( Gx_page, "ZZZZZ9")), 838, Gx_line+8, 873, Gx_line+22, 2+256) ;
            getPrinter().GxAttris("Arial", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("P�GINA:", 779, Gx_line+8, 822, Gx_line+22, 0+256) ;
            getPrinter().GxDrawText("DATA:", 779, Gx_line+26, 809, Gx_line+40, 0+256) ;
            getPrinter().GxAttris("Arial", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("EMPRESA A�REA:", 14, Gx_line+53, 113, Gx_line+68, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("R2Tech Brasil", 14, Gx_line+6, 99, Gx_line+22, 0+256) ;
            getPrinter().GxAttris("Arial", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV20lccbEm, "@!")), 124, Gx_line+52, 162, Gx_line+68, 0+256) ;
            getPrinter().GxDrawText(GXutil.rtrim( localUtil.format( AV21EmpNom, "@!")), 187, Gx_line+52, 366, Gx_line+68, 0+256) ;
            getPrinter().GxAttris("MS Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("i1022", 14, Gx_line+27, 45, Gx_line+40, 0+256) ;
            getPrinter().GxAttris("MS Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("RELAT�RIO DE VENDAS A CR�DITO COM DISCREP�NCIAS ", 207, Gx_line+20, 717, Gx_line+40, 0+256) ;
            Gx_OldLine = Gx_line ;
            Gx_line = (int)(Gx_line+75) ;
            if (true) break;
         }
         else
         {
            PrtOffset = 0 ;
            Gx_line = (int)(Gx_line+1) ;
         }
         ToSkip = (int)(ToSkip-1) ;
      }
      getPrinter().setPage(Gx_page);
   }

   public void add_metrics( )
   {
      add_metrics0( ) ;
      add_metrics1( ) ;
      add_metrics2( ) ;
      add_metrics3( ) ;
   }

   public void add_metrics0( )
   {
      getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics1( )
   {
      getPrinter().setMetrics("MS Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 34, 47, 17, 34, 31, 62, 34, 34, 21, 64, 41, 21, 62, 47, 38, 47, 47, 17, 17, 31, 31, 22, 34, 62, 20, 62, 34, 21, 59, 47, 31, 41, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
   }

   public void add_metrics2( )
   {
      getPrinter().setMetrics("Arial", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 36, 48, 14, 36, 21, 64, 36, 36, 21, 64, 43, 21, 64, 48, 39, 48, 48, 14, 14, 21, 21, 22, 36, 64, 20, 64, 32, 21, 60, 48, 31, 43, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
   }

   public void add_metrics3( )
   {
      getPrinter().setMetrics("Arial", true, true, 58, 14, 72, 123,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 30, 35, 35, 55, 45, 14, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 21, 21, 37, 37, 37, 38, 61, 45, 45, 45, 45, 42, 38, 49, 45, 17, 35, 45, 38, 52, 45, 49, 42, 49, 45, 42, 38, 45, 42, 59, 42, 42, 38, 21, 18, 23, 37, 35, 21, 35, 38, 35, 38, 35, 21, 38, 38, 18, 18, 35, 18, 56, 38, 38, 38, 38, 25, 35, 21, 38, 35, 49, 35, 35, 32, 25, 17, 25, 37, 47, 35, 47, 18, 35, 32, 63, 35, 35, 21, 63, 42, 21, 63, 47, 38, 47, 47, 17, 18, 32, 32, 22, 35, 64, 21, 63, 35, 21, 59, 47, 32, 42, 18, 21, 36, 35, 35, 35, 17, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 21, 21, 36, 35, 21, 21, 21, 23, 35, 53, 53, 53, 38, 45, 45, 45, 45, 45, 45, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 35, 35, 35, 35, 35, 18, 18, 18, 18, 38, 38, 38, 38, 38, 38, 38, 35, 38, 38, 38, 38, 38, 35, 38, 35}) ;
   }

   protected int getOutputType( )
   {
      return OUTPUT_PDF;
   }

   protected void cleanup( )
   {
      this.aP0[0] = putilitario_reti1022rpt.this.AV20lccbEm;
      this.aP1[0] = putilitario_reti1022rpt.this.AV21EmpNom;
      this.aP2[0] = putilitario_reti1022rpt.this.AV22FilePD;
      this.aP3[0] = putilitario_reti1022rpt.this.AV23FileTX;
      this.aP4[0] = putilitario_reti1022rpt.this.AV31DebugM;
      this.aP5[0] = putilitario_reti1022rpt.this.AV67DateFr;
      this.aP6[0] = putilitario_reti1022rpt.this.AV68DateTo;
      this.aP7[0] = putilitario_reti1022rpt.this.AV73Modo;
      Application.commit(context, remoteHandle, "DEFAULT", "putilitario_reti1022rpt");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      M_top = 0 ;
      M_bot = 0 ;
      Gx_line = 0 ;
      ToSkip = 0 ;
      PrtOffset = 0 ;
      AV65CarFim = "" ;
      AV32i = (short)(0) ;
      AV27sOutpu = "" ;
      GXt_char1 = "" ;
      AV29lccbSu = GXutil.resetTime( GXutil.nullDate() );
      Gx_date = GXutil.nullDate() ;
      AV35Decisa = "" ;
      AV36Decisa = "" ;
      AV37Decisa = "" ;
      AV38sDecis = "" ;
      AV39sDecis = "" ;
      AV40sDecis = "" ;
      AV89GXLvl5 = (byte)(0) ;
      scmdbuf = "" ;
      P00792_A1147lccbE = new String[] {""} ;
      P00792_n1147lccbE = new boolean[] {false} ;
      P00792_A1150lccbE = new String[] {""} ;
      P00792_A1166lccbP = new String[] {""} ;
      P00792_A1165lccbD = new String[] {""} ;
      P00792_n1165lccbD = new boolean[] {false} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      A1166lccbP = "" ;
      A1165lccbD = "" ;
      n1165lccbD = false ;
      AV78Decisa = "" ;
      AV77SDecis = "" ;
      Gx_OldLine = 0 ;
      returnInSub = false ;
      A1298SCETk = "" ;
      A1310SCETp = "" ;
      A1307SCEIa = "" ;
      AV24FileNo = 0 ;
      AV66xmlWri = new com.genexus.xml.XMLWriter();
      AV63Date = GXutil.resetTime( GXutil.nullDate() );
      AV62DateCa = "" ;
      AV43LinhaA = "" ;
      AV30nSeq = 0 ;
      AV71DateFr = GXutil.resetTime( GXutil.nullDate() );
      AV72DateTo = GXutil.resetTime( GXutil.nullDate() );
      A1299SCEDa = GXutil.resetTime( GXutil.nullDate() );
      A1309SCEFo = "" ;
      A1301SCEAi = "" ;
      A1306SCEAP = "" ;
      P00793_A1301SCEAi = new String[] {""} ;
      P00793_n1301SCEAi = new boolean[] {false} ;
      P00793_A1306SCEAP = new String[] {""} ;
      P00793_n1306SCEAP = new boolean[] {false} ;
      P00793_A1309SCEFo = new String[] {""} ;
      P00793_n1309SCEFo = new boolean[] {false} ;
      P00793_A1298SCETk = new String[] {""} ;
      P00793_n1298SCETk = new boolean[] {false} ;
      P00793_A1299SCEDa = new java.util.Date[] {GXutil.nullDate()} ;
      P00793_n1299SCEDa = new boolean[] {false} ;
      P00793_A1307SCEIa = new String[] {""} ;
      P00793_n1307SCEIa = new boolean[] {false} ;
      P00793_A1310SCETp = new String[] {""} ;
      P00793_n1310SCETp = new boolean[] {false} ;
      P00793_A1305SCETe = new String[] {""} ;
      P00793_n1305SCETe = new boolean[] {false} ;
      P00793_A1312SCEId = new long[1] ;
      n1301SCEAi = false ;
      n1306SCEAP = false ;
      n1309SCEFo = false ;
      n1298SCETk = false ;
      n1299SCEDa = false ;
      n1307SCEIa = false ;
      n1310SCETp = false ;
      A1305SCETe = "" ;
      n1305SCETe = false ;
      A1312SCEId = 0 ;
      AV46SCETkt = "" ;
      AV70Encont = "" ;
      P00794_A1231lccbT = new String[] {""} ;
      P00794_A1150lccbE = new String[] {""} ;
      P00794_A1222lccbI = new String[] {""} ;
      P00794_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00794_A1224lccbC = new String[] {""} ;
      P00794_A1225lccbC = new String[] {""} ;
      P00794_A1226lccbA = new String[] {""} ;
      P00794_A1227lccbO = new String[] {""} ;
      P00794_A1228lccbF = new String[] {""} ;
      P00794_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1222lccbI = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1224lccbC = "" ;
      A1225lccbC = "" ;
      A1226lccbA = "" ;
      A1227lccbO = "" ;
      A1228lccbF = "" ;
      A1232lccbT = "" ;
      AV54nSeqCa = "" ;
      AV45Empres = "" ;
      AV44IATA = "" ;
      AV59SCETpE = "" ;
      AV60SCETex = "" ;
      AV48lccbSa = 0 ;
      AV50lccbTi = 0 ;
      AV52lccbDa = GXutil.nullDate() ;
      P00795_A966CODE = new String[] {""} ;
      P00795_A968NUM_BI = new String[] {""} ;
      P00795_A874AIRPT_ = new double[1] ;
      P00795_n874AIRPT_ = new boolean[] {false} ;
      P00795_A875TAX_1 = new double[1] ;
      P00795_n875TAX_1 = new boolean[] {false} ;
      P00795_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P00795_A963ISOC = new String[] {""} ;
      P00795_A964CiaCod = new String[] {""} ;
      P00795_A965PER_NA = new String[] {""} ;
      P00795_A967IATA = new String[] {""} ;
      P00795_A969TIPO_V = new String[] {""} ;
      A966CODE = "" ;
      A968NUM_BI = "" ;
      A874AIRPT_ = 0 ;
      n874AIRPT_ = false ;
      A875TAX_1 = 0 ;
      n875TAX_1 = false ;
      A970DATA = GXutil.nullDate() ;
      A963ISOC = "" ;
      A964CiaCod = "" ;
      A965PER_NA = "" ;
      A967IATA = "" ;
      A969TIPO_V = "" ;
      P00796_A1150lccbE = new String[] {""} ;
      P00796_A1222lccbI = new String[] {""} ;
      P00796_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00796_A1224lccbC = new String[] {""} ;
      P00796_A1225lccbC = new String[] {""} ;
      P00796_A1226lccbA = new String[] {""} ;
      P00796_A1227lccbO = new String[] {""} ;
      P00796_A1228lccbF = new String[] {""} ;
      P00796_A1231lccbT = new String[] {""} ;
      P00796_A1178lccbO = new double[1] ;
      P00796_n1178lccbO = new boolean[] {false} ;
      P00796_A1176lccbO = new double[1] ;
      P00796_n1176lccbO = new boolean[] {false} ;
      P00796_A1177lccbO = new double[1] ;
      P00796_n1177lccbO = new boolean[] {false} ;
      P00796_A1172lccbS = new double[1] ;
      P00796_n1172lccbS = new boolean[] {false} ;
      P00796_A1170lccbI = new double[1] ;
      P00796_n1170lccbI = new boolean[] {false} ;
      P00796_A1171lccbT = new double[1] ;
      P00796_n1171lccbT = new boolean[] {false} ;
      P00796_A1232lccbT = new String[] {""} ;
      A1178lccbO = 0 ;
      n1178lccbO = false ;
      A1176lccbO = 0 ;
      n1176lccbO = false ;
      A1177lccbO = 0 ;
      n1177lccbO = false ;
      A1172lccbS = 0 ;
      n1172lccbS = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      AV80lccbOr = 0 ;
      AV82lccbOr = 0 ;
      AV83lccbOr = 0 ;
      AV84lccbIn = 0 ;
      AV49lccbSa = "" ;
      AV51lccbTi = "" ;
      AV53lccbDa = "" ;
      AV58lccbIn = "" ;
      AV47lccbCu = "" ;
      AV55lccbCC = "" ;
      AV61lccbCC = "" ;
      AV81lccbOr = 0 ;
      AV79Indica = "" ;
      AV94Lccbdo = 0 ;
      AV33s1 = "" ;
      AV34s2 = "" ;
      AV95GXLvl2 = (byte)(0) ;
      A1191lccbS = "" ;
      A1184lccbS = "" ;
      P00798_A1227lccbO = new String[] {""} ;
      P00798_A1228lccbF = new String[] {""} ;
      P00798_A1191lccbS = new String[] {""} ;
      P00798_n1191lccbS = new boolean[] {false} ;
      P00798_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P00798_A1184lccbS = new String[] {""} ;
      P00798_n1184lccbS = new boolean[] {false} ;
      P00798_A1150lccbE = new String[] {""} ;
      P00798_A1181lccbC = new String[] {""} ;
      P00798_n1181lccbC = new boolean[] {false} ;
      P00798_A1172lccbS = new double[1] ;
      P00798_n1172lccbS = new boolean[] {false} ;
      P00798_A1171lccbT = new double[1] ;
      P00798_n1171lccbT = new boolean[] {false} ;
      P00798_A1224lccbC = new String[] {""} ;
      P00798_A1226lccbA = new String[] {""} ;
      P00798_A1168lccbI = new short[1] ;
      P00798_n1168lccbI = new boolean[] {false} ;
      P00798_A1178lccbO = new double[1] ;
      P00798_n1178lccbO = new boolean[] {false} ;
      P00798_A1175lccbO = new double[1] ;
      P00798_n1175lccbO = new boolean[] {false} ;
      P00798_A1176lccbO = new double[1] ;
      P00798_n1176lccbO = new boolean[] {false} ;
      P00798_A1177lccbO = new double[1] ;
      P00798_n1177lccbO = new boolean[] {false} ;
      P00798_A1225lccbC = new String[] {""} ;
      P00798_A1231lccbT = new String[] {""} ;
      P00798_A1222lccbI = new String[] {""} ;
      P00798_A1232lccbT = new String[] {""} ;
      n1191lccbS = false ;
      n1184lccbS = false ;
      A1181lccbC = "" ;
      n1181lccbC = false ;
      A1168lccbI = (short)(0) ;
      n1168lccbI = false ;
      A1175lccbO = 0 ;
      n1175lccbO = false ;
      AV56lccbAp = "" ;
      AV57lccbIn = (short)(0) ;
      AV74Atribu = "" ;
      AV76Result = "" ;
      GXv_svchar2 = new String [1] ;
      AV64lccbst = "" ;
      AV69lccbIA = "" ;
      AV96GXLvl2 = (byte)(0) ;
      P007910_A1301SCEAi = new String[] {""} ;
      P007910_n1301SCEAi = new boolean[] {false} ;
      P007910_A1298SCETk = new String[] {""} ;
      P007910_n1298SCETk = new boolean[] {false} ;
      P007910_A1306SCEAP = new String[] {""} ;
      P007910_n1306SCEAP = new boolean[] {false} ;
      P007910_A1307SCEIa = new String[] {""} ;
      P007910_n1307SCEIa = new boolean[] {false} ;
      P007910_A1310SCETp = new String[] {""} ;
      P007910_n1310SCETp = new boolean[] {false} ;
      P007910_A1305SCETe = new String[] {""} ;
      P007910_n1305SCETe = new boolean[] {false} ;
      P007910_A1309SCEFo = new String[] {""} ;
      P007910_n1309SCEFo = new boolean[] {false} ;
      P007910_A1299SCEDa = new java.util.Date[] {GXutil.nullDate()} ;
      P007910_n1299SCEDa = new boolean[] {false} ;
      P007910_A1312SCEId = new long[1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new putilitario_reti1022rpt__default(),
         new Object[] {
             new Object[] {
            P00792_A1147lccbE, P00792_n1147lccbE, P00792_A1150lccbE, P00792_A1166lccbP, P00792_A1165lccbD, P00792_n1165lccbD
            }
            , new Object[] {
            P00793_A1301SCEAi, P00793_n1301SCEAi, P00793_A1306SCEAP, P00793_n1306SCEAP, P00793_A1309SCEFo, P00793_n1309SCEFo, P00793_A1298SCETk, P00793_n1298SCETk, P00793_A1299SCEDa, P00793_n1299SCEDa,
            P00793_A1307SCEIa, P00793_n1307SCEIa, P00793_A1310SCETp, P00793_n1310SCETp, P00793_A1305SCETe, P00793_n1305SCETe, P00793_A1312SCEId
            }
            , new Object[] {
            P00794_A1231lccbT, P00794_A1150lccbE, P00794_A1222lccbI, P00794_A1223lccbD, P00794_A1224lccbC, P00794_A1225lccbC, P00794_A1226lccbA, P00794_A1227lccbO, P00794_A1228lccbF, P00794_A1232lccbT
            }
            , new Object[] {
            P00795_A966CODE, P00795_A968NUM_BI, P00795_A874AIRPT_, P00795_n874AIRPT_, P00795_A875TAX_1, P00795_n875TAX_1, P00795_A970DATA, P00795_A963ISOC, P00795_A964CiaCod, P00795_A965PER_NA,
            P00795_A967IATA, P00795_A969TIPO_V
            }
            , new Object[] {
            P00796_A1150lccbE, P00796_A1222lccbI, P00796_A1223lccbD, P00796_A1224lccbC, P00796_A1225lccbC, P00796_A1226lccbA, P00796_A1227lccbO, P00796_A1228lccbF, P00796_A1231lccbT, P00796_A1178lccbO,
            P00796_n1178lccbO, P00796_A1176lccbO, P00796_n1176lccbO, P00796_A1177lccbO, P00796_n1177lccbO, P00796_A1172lccbS, P00796_n1172lccbS, P00796_A1170lccbI, P00796_n1170lccbI, P00796_A1171lccbT,
            P00796_n1171lccbT, P00796_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            P00798_A1227lccbO, P00798_A1228lccbF, P00798_A1191lccbS, P00798_n1191lccbS, P00798_A1223lccbD, P00798_A1184lccbS, P00798_n1184lccbS, P00798_A1150lccbE, P00798_A1181lccbC, P00798_n1181lccbC,
            P00798_A1172lccbS, P00798_n1172lccbS, P00798_A1171lccbT, P00798_n1171lccbT, P00798_A1224lccbC, P00798_A1226lccbA, P00798_A1168lccbI, P00798_n1168lccbI, P00798_A1178lccbO, P00798_n1178lccbO,
            P00798_A1175lccbO, P00798_n1175lccbO, P00798_A1176lccbO, P00798_n1176lccbO, P00798_A1177lccbO, P00798_n1177lccbO, P00798_A1225lccbC, P00798_A1231lccbT, P00798_A1222lccbI, P00798_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            P007910_A1301SCEAi, P007910_n1301SCEAi, P007910_A1298SCETk, P007910_n1298SCETk, P007910_A1306SCEAP, P007910_n1306SCEAP, P007910_A1307SCEIa, P007910_n1307SCEIa, P007910_A1310SCETp, P007910_n1310SCETp,
            P007910_A1305SCETe, P007910_n1305SCETe, P007910_A1309SCEFo, P007910_n1309SCEFo, P007910_A1299SCEDa, P007910_n1299SCEDa, P007910_A1312SCEId
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_line = 0 ;
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV89GXLvl5 ;
   private byte AV95GXLvl2 ;
   private byte AV96GXLvl2 ;
   private short AV32i ;
   private short A1168lccbI ;
   private short AV57lccbIn ;
   private short Gx_err ;
   private int M_top ;
   private int M_bot ;
   private int Line ;
   private int ToSkip ;
   private int PrtOffset ;
   private int Gx_OldLine ;
   private int AV24FileNo ;
   private int AV30nSeq ;
   private long A1312SCEId ;
   private double AV48lccbSa ;
   private double AV50lccbTi ;
   private double A874AIRPT_ ;
   private double A875TAX_1 ;
   private double A1178lccbO ;
   private double A1176lccbO ;
   private double A1177lccbO ;
   private double A1172lccbS ;
   private double A1170lccbI ;
   private double A1171lccbT ;
   private double AV80lccbOr ;
   private double AV82lccbOr ;
   private double AV83lccbOr ;
   private double AV84lccbIn ;
   private double AV81lccbOr ;
   private double AV94Lccbdo ;
   private double A1175lccbO ;
   private String AV20lccbEm ;
   private String AV21EmpNom ;
   private String AV22FilePD ;
   private String AV23FileTX ;
   private String AV31DebugM ;
   private String AV73Modo ;
   private String AV65CarFim ;
   private String AV27sOutpu ;
   private String GXt_char1 ;
   private String AV35Decisa ;
   private String AV36Decisa ;
   private String AV37Decisa ;
   private String AV38sDecis ;
   private String AV39sDecis ;
   private String AV40sDecis ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String A1166lccbP ;
   private String A1165lccbD ;
   private String AV78Decisa ;
   private String AV77SDecis ;
   private String A1298SCETk ;
   private String A1310SCETp ;
   private String A1307SCEIa ;
   private String AV62DateCa ;
   private String AV43LinhaA ;
   private String A1309SCEFo ;
   private String A1301SCEAi ;
   private String A1306SCEAP ;
   private String A1305SCETe ;
   private String AV46SCETkt ;
   private String AV70Encont ;
   private String A1231lccbT ;
   private String A1222lccbI ;
   private String A1224lccbC ;
   private String A1225lccbC ;
   private String A1226lccbA ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private String A1232lccbT ;
   private String AV54nSeqCa ;
   private String AV45Empres ;
   private String AV44IATA ;
   private String AV59SCETpE ;
   private String AV60SCETex ;
   private String A966CODE ;
   private String A968NUM_BI ;
   private String A963ISOC ;
   private String A964CiaCod ;
   private String A965PER_NA ;
   private String A967IATA ;
   private String A969TIPO_V ;
   private String AV49lccbSa ;
   private String AV51lccbTi ;
   private String AV53lccbDa ;
   private String AV58lccbIn ;
   private String AV47lccbCu ;
   private String AV55lccbCC ;
   private String AV61lccbCC ;
   private String AV79Indica ;
   private String AV33s1 ;
   private String AV34s2 ;
   private String A1191lccbS ;
   private String A1184lccbS ;
   private String A1181lccbC ;
   private String AV56lccbAp ;
   private String AV74Atribu ;
   private String AV76Result ;
   private String AV64lccbst ;
   private String AV69lccbIA ;
   private java.util.Date AV29lccbSu ;
   private java.util.Date AV63Date ;
   private java.util.Date AV71DateFr ;
   private java.util.Date AV72DateTo ;
   private java.util.Date A1299SCEDa ;
   private java.util.Date AV67DateFr ;
   private java.util.Date AV68DateTo ;
   private java.util.Date Gx_date ;
   private java.util.Date A1223lccbD ;
   private java.util.Date AV52lccbDa ;
   private java.util.Date A970DATA ;
   private boolean n1147lccbE ;
   private boolean n1165lccbD ;
   private boolean returnInSub ;
   private boolean n1301SCEAi ;
   private boolean n1306SCEAP ;
   private boolean n1309SCEFo ;
   private boolean n1298SCETk ;
   private boolean n1299SCEDa ;
   private boolean n1307SCEIa ;
   private boolean n1310SCETp ;
   private boolean n1305SCETe ;
   private boolean n874AIRPT_ ;
   private boolean n875TAX_1 ;
   private boolean n1178lccbO ;
   private boolean n1176lccbO ;
   private boolean n1177lccbO ;
   private boolean n1172lccbS ;
   private boolean n1170lccbI ;
   private boolean n1171lccbT ;
   private boolean n1191lccbS ;
   private boolean n1184lccbS ;
   private boolean n1181lccbC ;
   private boolean n1168lccbI ;
   private boolean n1175lccbO ;
   private String GXv_svchar2[] ;
   private com.genexus.xml.XMLWriter AV66xmlWri ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
   private java.util.Date[] aP5 ;
   private java.util.Date[] aP6 ;
   private String[] aP7 ;
   private IDataStoreProvider pr_default ;
   private String[] P00792_A1147lccbE ;
   private boolean[] P00792_n1147lccbE ;
   private String[] P00792_A1150lccbE ;
   private String[] P00792_A1166lccbP ;
   private String[] P00792_A1165lccbD ;
   private boolean[] P00792_n1165lccbD ;
   private String[] P00793_A1301SCEAi ;
   private boolean[] P00793_n1301SCEAi ;
   private String[] P00793_A1306SCEAP ;
   private boolean[] P00793_n1306SCEAP ;
   private String[] P00793_A1309SCEFo ;
   private boolean[] P00793_n1309SCEFo ;
   private String[] P00793_A1298SCETk ;
   private boolean[] P00793_n1298SCETk ;
   private java.util.Date[] P00793_A1299SCEDa ;
   private boolean[] P00793_n1299SCEDa ;
   private String[] P00793_A1307SCEIa ;
   private boolean[] P00793_n1307SCEIa ;
   private String[] P00793_A1310SCETp ;
   private boolean[] P00793_n1310SCETp ;
   private String[] P00793_A1305SCETe ;
   private boolean[] P00793_n1305SCETe ;
   private long[] P00793_A1312SCEId ;
   private String[] P00794_A1231lccbT ;
   private String[] P00794_A1150lccbE ;
   private String[] P00794_A1222lccbI ;
   private java.util.Date[] P00794_A1223lccbD ;
   private String[] P00794_A1224lccbC ;
   private String[] P00794_A1225lccbC ;
   private String[] P00794_A1226lccbA ;
   private String[] P00794_A1227lccbO ;
   private String[] P00794_A1228lccbF ;
   private String[] P00794_A1232lccbT ;
   private String[] P00795_A966CODE ;
   private String[] P00795_A968NUM_BI ;
   private double[] P00795_A874AIRPT_ ;
   private boolean[] P00795_n874AIRPT_ ;
   private double[] P00795_A875TAX_1 ;
   private boolean[] P00795_n875TAX_1 ;
   private java.util.Date[] P00795_A970DATA ;
   private String[] P00795_A963ISOC ;
   private String[] P00795_A964CiaCod ;
   private String[] P00795_A965PER_NA ;
   private String[] P00795_A967IATA ;
   private String[] P00795_A969TIPO_V ;
   private String[] P00796_A1150lccbE ;
   private String[] P00796_A1222lccbI ;
   private java.util.Date[] P00796_A1223lccbD ;
   private String[] P00796_A1224lccbC ;
   private String[] P00796_A1225lccbC ;
   private String[] P00796_A1226lccbA ;
   private String[] P00796_A1227lccbO ;
   private String[] P00796_A1228lccbF ;
   private String[] P00796_A1231lccbT ;
   private double[] P00796_A1178lccbO ;
   private boolean[] P00796_n1178lccbO ;
   private double[] P00796_A1176lccbO ;
   private boolean[] P00796_n1176lccbO ;
   private double[] P00796_A1177lccbO ;
   private boolean[] P00796_n1177lccbO ;
   private double[] P00796_A1172lccbS ;
   private boolean[] P00796_n1172lccbS ;
   private double[] P00796_A1170lccbI ;
   private boolean[] P00796_n1170lccbI ;
   private double[] P00796_A1171lccbT ;
   private boolean[] P00796_n1171lccbT ;
   private String[] P00796_A1232lccbT ;
   private String[] P00798_A1227lccbO ;
   private String[] P00798_A1228lccbF ;
   private String[] P00798_A1191lccbS ;
   private boolean[] P00798_n1191lccbS ;
   private java.util.Date[] P00798_A1223lccbD ;
   private String[] P00798_A1184lccbS ;
   private boolean[] P00798_n1184lccbS ;
   private String[] P00798_A1150lccbE ;
   private String[] P00798_A1181lccbC ;
   private boolean[] P00798_n1181lccbC ;
   private double[] P00798_A1172lccbS ;
   private boolean[] P00798_n1172lccbS ;
   private double[] P00798_A1171lccbT ;
   private boolean[] P00798_n1171lccbT ;
   private String[] P00798_A1224lccbC ;
   private String[] P00798_A1226lccbA ;
   private short[] P00798_A1168lccbI ;
   private boolean[] P00798_n1168lccbI ;
   private double[] P00798_A1178lccbO ;
   private boolean[] P00798_n1178lccbO ;
   private double[] P00798_A1175lccbO ;
   private boolean[] P00798_n1175lccbO ;
   private double[] P00798_A1176lccbO ;
   private boolean[] P00798_n1176lccbO ;
   private double[] P00798_A1177lccbO ;
   private boolean[] P00798_n1177lccbO ;
   private String[] P00798_A1225lccbC ;
   private String[] P00798_A1231lccbT ;
   private String[] P00798_A1222lccbI ;
   private String[] P00798_A1232lccbT ;
   private String[] P007910_A1301SCEAi ;
   private boolean[] P007910_n1301SCEAi ;
   private String[] P007910_A1298SCETk ;
   private boolean[] P007910_n1298SCETk ;
   private String[] P007910_A1306SCEAP ;
   private boolean[] P007910_n1306SCEAP ;
   private String[] P007910_A1307SCEIa ;
   private boolean[] P007910_n1307SCEIa ;
   private String[] P007910_A1310SCETp ;
   private boolean[] P007910_n1310SCETp ;
   private String[] P007910_A1305SCETe ;
   private boolean[] P007910_n1305SCETe ;
   private String[] P007910_A1309SCEFo ;
   private boolean[] P007910_n1309SCEFo ;
   private java.util.Date[] P007910_A1299SCEDa ;
   private boolean[] P007910_n1299SCEDa ;
   private long[] P007910_A1312SCEId ;
}

final  class putilitario_reti1022rpt__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P00793( String AV73Modo ,
                                        java.util.Date A1299SCEDa ,
                                        java.util.Date AV71DateFr ,
                                        java.util.Date AV72DateTo ,
                                        String A1309SCEFo ,
                                        String A1298SCETk ,
                                        String AV20lccbEm ,
                                        String A1301SCEAi ,
                                        String A1306SCEAP )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT [SCEAirLine], [SCEAPP], [SCEFopID], [SCETkt], [SCEDate], [SCEIata], [SCETpEvento]," ;
      scmdbuf = scmdbuf + " [SCEText], [SCEId] FROM [SCEVENTS] WITH (UPDLOCK)" ;
      scmdbuf = scmdbuf + " WHERE ([SCEAirLine] = '" + GXutil.rtrim( GXutil.strReplace( AV20lccbEm, "'", "''")) + "')" ;
      scmdbuf = scmdbuf + " and ([SCEAPP] = 'ICSI')" ;
      scmdbuf = scmdbuf + " and ([SCEAirLine] = '" + GXutil.rtrim( GXutil.strReplace( AV20lccbEm, "'", "''")) + "' and [SCEAPP] = 'ICSI')" ;
      scmdbuf = scmdbuf + " and ([SCETkt] <> '')" ;
      sWhereString = "" ;
      if ( ( GXutil.strcmp(AV73Modo, "R") == 0 ) )
      {
         sWhereString = sWhereString + " and ([SCEDate] >= " + (AV71DateFr.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.ttoc( AV71DateFr, 10, 8, 0, 0, "-", ":", " ")+"',120)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      }
      if ( ( GXutil.strcmp(AV73Modo, "R") == 0 ) )
      {
         sWhereString = sWhereString + " and ([SCEDate] <= " + (AV72DateTo.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.ttoc( AV72DateTo, 10, 8, 0, 0, "-", ":", " ")+"',120)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      }
      if ( ( GXutil.strcmp(AV73Modo, "G") == 0 ) )
      {
         sWhereString = sWhereString + " and ([SCEFopID] = '' or [SCEFopID] = 'W' or [SCEFopID] = 'E')" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY [SCEAirLine], [SCEAPP], [SCEFopID], [SCEDate], [SCETkt]" ;
      return scmdbuf;
   }

   protected String conditional_P00798( String AV73Modo ,
                                        java.util.Date A1223lccbD ,
                                        java.util.Date AV67DateFr ,
                                        java.util.Date AV68DateTo ,
                                        String A1191lccbS ,
                                        String A1184lccbS ,
                                        String AV20lccbEm ,
                                        String A1150lccbE )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT T1.[lccbOpCode], T1.[lccbFPAC_PLP], T2.[lccbSubType], T1.[lccbDate], T2.[lccbStatus]," ;
      scmdbuf = scmdbuf + " T1.[lccbEmpCod], T2.[lccbCurrency], T2.[lccbSaleAmount], T2.[lccbTip], T1.[lccbCCard]," ;
      scmdbuf = scmdbuf + " T1.[lccbAppCode], T2.[lccbInstallments], T2.[lccbOrgSaleAmount], T2.[lccbOrgDownPayment]," ;
      scmdbuf = scmdbuf + " T2.[lccbOrgInstAmount], T2.[lccbOrgTip], T1.[lccbCCNum], T1.[lccbTDNR], T1.[lccbIATA]," ;
      scmdbuf = scmdbuf + " T1.[lccbTRNC] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (UPDLOCK)" ;
      scmdbuf = scmdbuf + " ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate]" ;
      scmdbuf = scmdbuf + " = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum]" ;
      scmdbuf = scmdbuf + " AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND" ;
      scmdbuf = scmdbuf + " T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP])" ;
      scmdbuf = scmdbuf + " WHERE (T1.[lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV20lccbEm, "'", "''")) + "')" ;
      scmdbuf = scmdbuf + " and (T2.[lccbStatus] = 'NOSUB')" ;
      sWhereString = "" ;
      if ( ( GXutil.strcmp(AV73Modo, "R") == 0 ) )
      {
         sWhereString = sWhereString + " and (T1.[lccbDate] >= " + (AV67DateFr.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.dtoc( AV67DateFr, 0, "-")+"',102)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      }
      if ( ( GXutil.strcmp(AV73Modo, "R") == 0 ) )
      {
         sWhereString = sWhereString + " and (T1.[lccbDate] <= " + (AV68DateTo.after(localUtil.ctod( "01/01/1753", 3)) ? "convert( DATETIME,'"+localUtil.dtoc( AV68DateTo, 0, "-")+"',102)" : "convert( DATETIME, '17530101', 112 )") + ")" ;
      }
      if ( ( GXutil.strcmp(AV73Modo, "G") == 0 ) )
      {
         sWhereString = sWhereString + " and (T2.[lccbSubType] = '')" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY T1.[lccbEmpCod]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 1 :
                  return conditional_P00793( (String)dynConstraints[0] , (java.util.Date)dynConstraints[1] , (java.util.Date)dynConstraints[2] , (java.util.Date)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
            case 6 :
                  return conditional_P00798( (String)dynConstraints[0] , (java.util.Date)dynConstraints[1] , (java.util.Date)dynConstraints[2] , (java.util.Date)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00792", "SELECT T2.[lccbEmpEnab], T1.[lccbEmpCod], T1.[lccbProblemID], T1.[lccbDecActID] FROM ([LCCBEMPRESAPROBLEMA] T1 WITH (NOLOCK) INNER JOIN [LCCBEMP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod]) WHERE (T1.[lccbEmpCod] = ?) AND (T2.[lccbEmpEnab] = '1') ORDER BY T1.[lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00793", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00794", "SELECT TOP 1 [lccbTDNR], [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbTDNR] = ? ORDER BY [lccbTDNR] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P00795", "SELECT [CODE], [NUM_BIL], [AIRPT_TAX], [TAX_1], [DATA], [ISOC], [CiaCod], [PER_NAME], [IATA], [TIPO_VEND] FROM [HOT] WITH (NOLOCK) WHERE ([CODE] <> 'RFND') AND ([NUM_BIL] = ?) ORDER BY [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00796", "SELECT T1.[lccbEmpCod], T1.[lccbIATA], T1.[lccbDate], T1.[lccbCCard], T1.[lccbCCNum], T1.[lccbAppCode], T1.[lccbOpCode], T1.[lccbFPAC_PLP], T1.[lccbTDNR], T2.[lccbOrgSaleAmount], T2.[lccbOrgInstAmount], T2.[lccbOrgTip], T2.[lccbSaleAmount], T2.[lccbInstAmount], T2.[lccbTip], T1.[lccbTRNC] FROM ([LCCBPLP2] T1 WITH (NOLOCK) INNER JOIN [LCCBPLP] T2 WITH (NOLOCK) ON T2.[lccbEmpCod] = T1.[lccbEmpCod] AND T2.[lccbIATA] = T1.[lccbIATA] AND T2.[lccbDate] = T1.[lccbDate] AND T2.[lccbCCard] = T1.[lccbCCard] AND T2.[lccbCCNum] = T1.[lccbCCNum] AND T2.[lccbAppCode] = T1.[lccbAppCode] AND T2.[lccbOpCode] = T1.[lccbOpCode] AND T2.[lccbFPAC_PLP] = T1.[lccbFPAC_PLP]) WHERE T1.[lccbTDNR] = ? ORDER BY T1.[lccbTDNR] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P00797", "UPDATE [SCEVENTS] SET [SCEFopID]=?  WHERE [SCEId] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P00798", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P00799", "UPDATE [LCCBPLP] SET [lccbSubType]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007910", "SELECT [SCEAirLine], [SCETkt], [SCEAPP], [SCEIata], [SCETpEvento], [SCEText], [SCEFopID], [SCEDate], [SCEId] FROM [SCEVENTS] WITH (UPDLOCK) WHERE ([SCEAirLine] = ?) AND (([SCEAirLine] = ?) AND ([SCETkt] = ?) AND ([SCEAPP] = 'ICSI')) ORDER BY [SCEAirLine], [SCEDate], [SCETkt] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007911", "UPDATE [SCEVENTS] SET [SCEFopID]=?  WHERE [SCEId] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 4) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 5) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(4, 10) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[8])[0] = rslt.getGXDateTime(5) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(6, 11) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(7, 3) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(8, 150) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((long[]) buf[16])[0] = rslt.getLong(9) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 7) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 44) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 1) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 19) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 4) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((double[]) buf[2])[0] = rslt.getDouble(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[6])[0] = rslt.getGXDate(5) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 2) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
               ((String[]) buf[11])[0] = rslt.getString(10, 20) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((double[]) buf[9])[0] = rslt.getDouble(10) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((double[]) buf[11])[0] = rslt.getDouble(11) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((double[]) buf[13])[0] = rslt.getDouble(12) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               ((double[]) buf[15])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[16])[0] = rslt.wasNull();
               ((double[]) buf[17])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[18])[0] = rslt.wasNull();
               ((double[]) buf[19])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[20])[0] = rslt.wasNull();
               ((String[]) buf[21])[0] = rslt.getString(16, 4) ;
               break;
            case 6 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 19) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[4])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 8) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getString(6, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 3) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((double[]) buf[10])[0] = rslt.getDouble(8) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((double[]) buf[12])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(10, 2) ;
               ((String[]) buf[15])[0] = rslt.getString(11, 20) ;
               ((short[]) buf[16])[0] = rslt.getShort(12) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(13) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((double[]) buf[22])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((double[]) buf[24])[0] = rslt.getDouble(16) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(17, 44) ;
               ((String[]) buf[27])[0] = rslt.getString(18, 10) ;
               ((String[]) buf[28])[0] = rslt.getString(19, 7) ;
               ((String[]) buf[29])[0] = rslt.getString(20, 4) ;
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(3, 4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(4, 11) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((String[]) buf[8])[0] = rslt.getString(5, 3) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(6, 150) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(7, 5) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[14])[0] = rslt.getGXDateTime(8) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((long[]) buf[16])[0] = rslt.getLong(9) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 3);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 10);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 10);
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 10);
               break;
            case 5 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 5);
               }
               stmt.setLong(2, ((Number) parms[2]).longValue());
               break;
            case 7 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 1);
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 7);
               stmt.setDate(4, (java.util.Date)parms[4]);
               stmt.setString(5, (String)parms[5], 2);
               stmt.setString(6, (String)parms[6], 44);
               stmt.setString(7, (String)parms[7], 20);
               stmt.setString(8, (String)parms[8], 1);
               stmt.setString(9, (String)parms[9], 19);
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 3);
               stmt.setString(3, (String)parms[2], 10);
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 5);
               }
               stmt.setLong(2, ((Number) parms[2]).longValue());
               break;
      }
   }

}


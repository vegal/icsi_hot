/*
               File: Wkp01
        Description: Stub for Wkp01
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:27.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class wwkp01 extends GXApplet
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      wwkp01 pgm = new wwkp01 (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void init( )
   {
      Application.setApplet(this);
      ApplicationContext.getInstance().setCurrentLocation( "" );
      ClientPreferences.resetPreferences();
      Application.init(GXcfg.class);
      ModelContext context = new ModelContext( wwkp01.class );
      remoteHandle = Application.getNewRemoteHandle(context);
      execute();
   }

   public  wwkp01( )
   {
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public wwkp01( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( wwkp01.class ), "" );
   }

   public wwkp01( int remoteHandle ,
                  ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new uwkp01(remoteHandle, context).execute(  );
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
}


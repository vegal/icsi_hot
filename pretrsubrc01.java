/*
               File: RETRSubRC01
        Description: Processamento retorno Redecar com reprocesso rejeitados
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:15.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pretrsubrc01 extends GXProcedure
{
   public pretrsubrc01( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretrsubrc01.class ), "" );
   }

   public pretrsubrc01( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 )
   {
      pretrsubrc01.this.AV11DebugM = aP0[0];
      this.aP0 = aP0;
      pretrsubrc01.this.AV100FileS = aP1[0];
      this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV85Versao = "00006" ;
      AV137msgEr = "" ;
      if ( ( GXutil.strSearch( AV11DebugM, "TESTMODE", 1) > 0 ) )
      {
         AV122TestM = (byte)(1) ;
      }
      else
      {
         AV122TestM = (byte)(0) ;
      }
      if ( ( GXutil.strSearch( AV11DebugM, "SAYALL", 1) > 0 ) )
      {
         AV124Verbo = (byte)(1) ;
      }
      else
      {
         AV124Verbo = (byte)(0) ;
      }
      AV9DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      AV10DataC = GXutil.substring( AV9DataB, 3, 6) ;
      AV28HoraA = GXutil.time( ) ;
      AV29HoraB = GXutil.substring( AV28HoraA, 1, 2) + GXutil.substring( AV28HoraA, 4, 2) + GXutil.substring( AV28HoraA, 7, 2) ;
      AV160Repro = 0 ;
      AV161RegRe = 0 ;
      /* Execute user subroutine: S1134 */
      S1134 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S1134( )
   {
      /* 'MAIN' Routine */
      AV85Versao = "00006" ;
      AV11DebugM = GXutil.trim( GXutil.upper( AV11DebugM)) ;
      GXt_char1 = AV14FileNa ;
      GXv_char2[0] = AV100FileS ;
      GXv_char3[0] = GXt_char1 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char2, GXv_char3) ;
      pretrsubrc01.this.AV100FileS = GXv_char2[0] ;
      pretrsubrc01.this.GXt_char1 = GXv_char3[0] ;
      AV14FileNa = GXt_char1 ;
      if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "Redecard Submission Feedback - Version "+AV85Versao );
         context.msgStatus( "  Running mode: ["+AV11DebugM+"] - Started at "+GXutil.time( ) );
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV100FileS), "") != 0 ) )
      {
         if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( "  Reading Redecard feedback file "+AV14FileNa );
         }
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfropen( AV100FileS, 250, "", "\"", "") ;
         /* Execute user subroutine: S121 */
         S121 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         if ( ( AV139ErroG != 99 ) )
         {
            if ( ( AV110Error <= 30 ) )
            {
               AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfropen( AV100FileS, 250, "", "\"", "") ;
               /* Execute user subroutine: S131 */
               S131 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV15FileNo = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
               if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
               {
                  context.msgStatus( "  Record Statistics:" );
                  AV104n = (int)(AV131cntCC+AV129cntCC) ;
                  context.msgStatus( "    Total CC      = "+GXutil.trim( GXutil.str( AV104n, 10, 0)) );
                  AV104n = (int)(AV132cntPL+AV130cntPL) ;
                  context.msgStatus( "    Total PLP     = "+GXutil.trim( GXutil.str( AV104n, 10, 0)) );
                  context.msgStatus( "  Breakdown:" );
                  context.msgStatus( "    Accepted      = "+GXutil.trim( GXutil.str( AV127cntAc, 10, 0)) );
                  context.msgStatus( "       Normal CC  = "+GXutil.trim( GXutil.str( AV131cntCC, 10, 0)) );
                  context.msgStatus( "       PLP        = "+GXutil.trim( GXutil.str( AV132cntPL, 10, 0)) );
                  context.msgStatus( "    Rejected      = "+GXutil.trim( GXutil.str( AV128cntRe, 10, 0)) );
                  context.msgStatus( "       Normal CC  = "+GXutil.trim( GXutil.str( AV129cntCC, 10, 0)) );
                  context.msgStatus( "       PLP        = "+GXutil.trim( GXutil.str( AV130cntPL, 10, 0)) );
                  context.msgStatus( "       Wrong type = "+GXutil.trim( GXutil.str( AV119cntWr, 10, 0)) );
                  context.msgStatus( "    Wrong status  = "+GXutil.trim( GXutil.str( AV116cntWr, 10, 0)) );
                  context.msgStatus( "    Not found     = "+GXutil.trim( GXutil.str( AV115cntNo, 10, 0)) );
                  context.msgStatus( " Resubmit         = "+GXutil.trim( GXutil.str( AV160Repro, 10, 0)) );
                  context.msgStatus( " Rejected         = "+GXutil.trim( GXutil.str( AV161RegRe, 10, 0)) );
               }
               AV162Repro = (int)(AV160Repro-AV161RegRe) ;
               AV137msgEr = "Processados normal " + GXutil.trim( GXutil.str( AV136TotRe, 10, 0)) + " registros " ;
            }
            else
            {
               AV137msgEr = "Ocorreram " + GXutil.str( AV110Error, 10, 0) + " erros" ;
            }
         }
      }
      else
      {
         if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( "  ERROR: No filename suplied!" );
         }
         else
         {
            GXutil.msg( this, "  ERROR: No filename suplied!" );
         }
      }
      if ( ( GXutil.strcmp(AV172Erro_, "S") == 0 ) )
      {
         /* Execute user subroutine: S141 */
         S141 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S121( )
   {
      /* 'CHECKFILE' Routine */
      AV110Error = (byte)(0) ;
      AV106LineC = 0 ;
      AV113OpenB = 0 ;
      AV107OpenB = 0 ;
      GX_I = 1 ;
      while ( ( GX_I <= 999 ) )
      {
         AV138Erros[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV139ErroG = (byte)(0) ;
      GX_I = 1 ;
      while ( ( GX_I <= 9999 ) )
      {
         AV144VetLo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 9999 ) )
      {
         AV145VetLo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV146Conta = (short)(0) ;
      AV172Erro_ = "N" ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char3[0] = AV102Linha ;
         GXt_int15 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char3, (short)(255)) ;
         AV102Linha = GXv_char3[0] ;
         AV15FileNo = GXt_int15 ;
         AV106LineC = (int)(AV106LineC+1) ;
         AV105LineC = GXutil.trim( GXutil.str( AV106LineC, 10, 0)) ;
         AV171linha = AV102Linha ;
         if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
         {
            Gx_msg = "  ERROR at line " + AV105LineC + ": " ;
         }
         AV103RecTy = GXutil.substring( AV102Linha, 1, 2) ;
         AV104n = GXutil.len( AV102Linha) ;
         if ( ( AV104n != 0 ) && ( AV104n != 250 ) )
         {
            Gx_msg = Gx_msg + ": Invalid line lenght" + GXutil.newLine( ) ;
            if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
            {
               context.msgStatus( Gx_msg );
            }
            AV110Error = (byte)(AV110Error+1) ;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV171linha, 1, 32), "0000000000000000000000590R2-IATA") != 0 ) && ( AV106LineC == 1 ) )
         {
            Gx_msg = Gx_msg + "Invalid Header " + GXutil.newLine( ) ;
            if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
            {
               context.msgStatus( Gx_msg );
            }
            AV110Error = (byte)(AV110Error+1) ;
            AV138Erros[AV110Error-1] = GXutil.str( AV110Error, 10, 0) + " - " + "Invalid Header " ;
            AV139ErroG = (byte)(99) ;
            AV172Erro_ = "S" ;
            if (true) break;
         }
         if ( ( GXutil.strcmp(AV103RecTy, "00") == 0 ) )
         {
            GX_I = 1 ;
            while ( ( GX_I <= 4 ) )
            {
               AV112Somas[GX_I-1] = 0 ;
               GX_I = (int)(GX_I+1) ;
            }
            AV146Conta = (short)(AV146Conta+1) ;
            AV109MovSt = GXutil.substring( AV102Linha, 245, 1) ;
            AV145VetLo[1-1] = AV109MovSt ;
            AV148Dia = (byte)(GXutil.val( GXutil.substring( AV102Linha, 69, 2), ".")) ;
            AV149Mes = (byte)(GXutil.val( GXutil.substring( AV102Linha, 67, 2), ".")) ;
            AV150Ano = (short)(GXutil.val( GXutil.substring( AV102Linha, 65, 2), ".")) ;
            AV143LccbS = localUtil.ymdtod( AV150Ano, AV149Mes, AV148Dia) ;
            AV154VarMo = GXutil.substring( AV102Linha, 61, 16) ;
            if ( ( GXutil.strcmp(AV109MovSt, "A") == 0 ) )
            {
               if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
               {
                  context.msgStatus( "  INFO: Header Status=ACCEPTED" );
               }
            }
            else if ( ( GXutil.strcmp(AV109MovSt, "R") == 0 ) )
            {
               if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
               {
                  context.msgStatus( "  INFO: Header Status=***REJECTED***" );
                  AV139ErroG = (byte)(98) ;
                  AV110Error = (byte)(AV110Error+1) ;
                  AV138Erros[AV110Error-1] = GXutil.str( AV110Error, 10, 0) + " - " + "  INFO: Header Status=***REJECTED***" ;
               }
            }
            else
            {
               Gx_msg = Gx_msg + "Invalid Header Status:" + AV109MovSt + GXutil.newLine( ) ;
               if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
               {
                  context.msgStatus( Gx_msg );
               }
               AV110Error = (byte)(AV110Error+1) ;
               AV138Erros[AV110Error-1] = GXutil.str( AV110Error, 10, 0) + " - " + "Invalid Header Status:" + AV109MovSt ;
               AV139ErroG = (byte)(99) ;
               if (true) break;
            }
            AV113OpenB = (int)(AV113OpenB+1) ;
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "01") == 0 ) )
         {
            if ( ( GXutil.strcmp(AV109MovSt, "R") == 0 ) )
            {
               AV141VarTe = GXutil.substring( AV102Linha, 3, 13) ;
               AV158LccbS = "TOSUB" ;
               AV152Grava = "REJECTED" ;
               context.msgStatus( "Chamando reprocessamento" );
               AV156TipoP = "I" ;
               /* Execute user subroutine: S151 */
               S151 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
         else
         {
            if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
            {
               context.msgStatus( "  Warning at line"+AV105LineC+": record type ["+AV103RecTy+"] unknown" );
            }
         }
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV171linha, 1, 15), "999999999999999") != 0 ) && ( AV106LineC > 0 ) )
      {
         Gx_msg = Gx_msg + "Invalid Header " + GXutil.newLine( ) ;
         if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( Gx_msg );
         }
         AV110Error = (byte)(AV110Error+1) ;
         AV138Erros[AV110Error-1] = GXutil.str( AV110Error, 10, 0) + " - " + "Invalid Trailer " ;
         AV139ErroG = (byte)(99) ;
         AV172Erro_ = "S" ;
      }
      if ( ( AV139ErroG == 99 ) )
      {
         Gx_msg = Gx_msg + "Serious error, archive total rejected" + GXutil.newLine( ) ;
         if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
         {
            context.msgStatus( Gx_msg );
         }
      }
      else
      {
      }
   }

   public void S131( )
   {
      /* 'LOADRECORDS' Routine */
      AV110Error = (byte)(0) ;
      AV106LineC = 0 ;
      AV113OpenB = 0 ;
      AV107OpenB = 0 ;
      AV134Conta = 0 ;
      while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
      {
         GXv_char3[0] = AV102Linha ;
         GXt_int15 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char3, (short)(255)) ;
         AV102Linha = GXv_char3[0] ;
         AV15FileNo = GXt_int15 ;
         AV106LineC = (int)(AV106LineC+1) ;
         AV105LineC = GXutil.trim( GXutil.str( AV106LineC, 10, 0)) ;
         AV103RecTy = GXutil.substring( AV102Linha, 1, 2) ;
         if ( ( GXutil.strcmp(AV103RecTy, "00") == 0 ) )
         {
            AV109MovSt = GXutil.substring( AV102Linha, 245, 1) ;
            AV154VarMo = GXutil.substring( AV102Linha, 61, 16) ;
            AV148Dia = (byte)(GXutil.val( GXutil.substring( AV102Linha, 126, 2), ".")) ;
            AV149Mes = (byte)(GXutil.val( GXutil.substring( AV102Linha, 128, 2), ".")) ;
            AV164Txt = "20" + GXutil.substring( AV102Linha, 130, 2) ;
            AV150Ano = (short)(GXutil.val( AV164Txt, ".")) ;
            AV163LccbR = localUtil.ymdtod( AV150Ano, AV149Mes, AV148Dia) ;
            AV148Dia = (byte)(GXutil.val( GXutil.substring( AV102Linha, 130, 2), ".")) ;
            AV149Mes = (byte)(GXutil.val( GXutil.substring( AV102Linha, 128, 2), ".")) ;
            AV164Txt = "20" + GXutil.substring( AV102Linha, 126, 2) ;
            AV150Ano = (short)(GXutil.val( AV164Txt, ".")) ;
            AV165LccbR = localUtil.ymdtod( AV150Ano, AV149Mes, AV148Dia) ;
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "01") == 0 ) )
         {
            if ( ( GXutil.strcmp(AV109MovSt, "A") == 0 ) )
            {
               AV114Refer = GXutil.substring( AV102Linha, 3, 13) ;
               AV147LccbC = GXutil.substring( AV102Linha, 6, 2) ;
               AV117trnTy = GXutil.substring( AV102Linha, 16, 3) ;
               if ( ( GXutil.strcmp(AV117trnTy, "101") == 0 ) )
               {
                  AV118RetSt = GXutil.substring( AV102Linha, 198, 2) ;
               }
               if ( ( GXutil.strcmp(AV117trnTy, "103") == 0 ) )
               {
                  AV118RetSt = GXutil.substring( AV102Linha, 204, 2) ;
                  AV163LccbR = AV165LccbR ;
               }
               if ( ( GXutil.val( AV118RetSt, ".") > 0 ) )
               {
                  AV123lccbS = "RETNOK" ;
                  GXt_char14 = AV121ErrDe ;
                  GXv_char3[0] = AV109MovSt ;
                  GXv_char2[0] = AV118RetSt ;
                  GXv_char16[0] = GXt_char14 ;
                  new pgeterrorrc(remoteHandle, context).execute( GXv_char3, GXv_char2, GXv_char16) ;
                  pretrsubrc01.this.AV109MovSt = GXv_char3[0] ;
                  pretrsubrc01.this.AV118RetSt = GXv_char2[0] ;
                  pretrsubrc01.this.GXt_char14 = GXv_char16[0] ;
                  AV121ErrDe = GXt_char14 ;
               }
               else
               {
                  AV123lccbS = "RETOK" ;
                  AV121ErrDe = "OK" ;
               }
               AV183GXLvl = (byte)(0) ;
               /* Using cursor P006E2 */
               pr_default.execute(0, new Object[] {AV114Refer});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A1228lccbF = P006E2_A1228lccbF[0] ;
                  A1227lccbO = P006E2_A1227lccbO[0] ;
                  A1226lccbA = P006E2_A1226lccbA[0] ;
                  A1225lccbC = P006E2_A1225lccbC[0] ;
                  A1224lccbC = P006E2_A1224lccbC[0] ;
                  A1223lccbD = P006E2_A1223lccbD[0] ;
                  A1222lccbI = P006E2_A1222lccbI[0] ;
                  A1150lccbE = P006E2_A1150lccbE[0] ;
                  A1193lccbS = P006E2_A1193lccbS[0] ;
                  n1193lccbS = P006E2_n1193lccbS[0] ;
                  A1490Distr = P006E2_A1490Distr[0] ;
                  n1490Distr = P006E2_n1490Distr[0] ;
                  A1184lccbS = P006E2_A1184lccbS[0] ;
                  n1184lccbS = P006E2_n1184lccbS[0] ;
                  A1194lccbS = P006E2_A1194lccbS[0] ;
                  n1194lccbS = P006E2_n1194lccbS[0] ;
                  A1206LccbR = P006E2_A1206LccbR[0] ;
                  n1206LccbR = P006E2_n1206LccbR[0] ;
                  A1205lccbR = P006E2_A1205lccbR[0] ;
                  n1205lccbR = P006E2_n1205lccbR[0] ;
                  A1203lccbR = P006E2_A1203lccbR[0] ;
                  n1203lccbR = P006E2_n1203lccbR[0] ;
                  A1196lccbR = P006E2_A1196lccbR[0] ;
                  n1196lccbR = P006E2_n1196lccbR[0] ;
                  A1201lccbR = P006E2_A1201lccbR[0] ;
                  n1201lccbR = P006E2_n1201lccbR[0] ;
                  A1199lccbR = P006E2_A1199lccbR[0] ;
                  n1199lccbR = P006E2_n1199lccbR[0] ;
                  A1197lccbR = P006E2_A1197lccbR[0] ;
                  n1197lccbR = P006E2_n1197lccbR[0] ;
                  A1200lccbR = P006E2_A1200lccbR[0] ;
                  n1200lccbR = P006E2_n1200lccbR[0] ;
                  A1198lccbR = P006E2_A1198lccbR[0] ;
                  n1198lccbR = P006E2_n1198lccbR[0] ;
                  A1185lccbB = P006E2_A1185lccbB[0] ;
                  n1185lccbB = P006E2_n1185lccbB[0] ;
                  if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "R") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(A1193lccbS, AV114Refer) == 0 ) )
                     {
                        AV183GXLvl = (byte)(1) ;
                        if ( ( GXutil.strcmp(A1184lccbS, "PROCPLP") == 0 ) )
                        {
                           if ( ( GXutil.strcmp(AV117trnTy, "101") == 0 ) )
                           {
                              AV166NumLo = GXutil.substring( AV102Linha, 115, 5) ;
                              if ( ( GXutil.val( AV118RetSt, ".") <= 0 ) )
                              {
                                 AV131cntCC = (int)(AV131cntCC+1) ;
                              }
                              else
                              {
                                 AV129cntCC = (int)(AV129cntCC+1) ;
                              }
                           }
                           else if ( ( GXutil.strcmp(AV117trnTy, "103") == 0 ) )
                           {
                              if ( ( GXutil.val( AV118RetSt, ".") <= 0 ) )
                              {
                                 AV132cntPL = (int)(AV132cntPL+1) ;
                              }
                              else
                              {
                                 AV130cntPL = (int)(AV130cntPL+1) ;
                              }
                              AV166NumLo = GXutil.substring( AV102Linha, 145, 5) ;
                           }
                           else
                           {
                              if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
                              {
                                 context.msgStatus( "  WARNING: Reference number ["+AV114Refer+"] with Wrong Type: "+A1184lccbS );
                              }
                              AV118RetSt = "ZZ" ;
                              AV119cntWr = (int)(AV119cntWr+1) ;
                           }
                           if ( ( GXutil.val( AV118RetSt, ".") <= 0 ) )
                           {
                              if ( ( AV124Verbo == 1 ) )
                              {
                                 context.msgStatus( "  OK: "+AV114Refer );
                              }
                              AV127cntAc = (int)(AV127cntAc+1) ;
                              AV136TotRe = (int)(AV136TotRe+1) ;
                           }
                           else
                           {
                              if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
                              {
                                 context.msgStatus( "    "+AV121ErrDe );
                              }
                              AV128cntRe = (int)(AV128cntRe+1) ;
                              AV136TotRe = (int)(AV136TotRe+1) ;
                           }
                           if ( ( AV122TestM == 0 ) )
                           {
                              A1184lccbS = GXutil.substring( AV123lccbS, 1, 8) ;
                              n1184lccbS = false ;
                              A1194lccbS = AV166NumLo ;
                              n1194lccbS = false ;
                              if ( ( GXutil.strcmp(AV123lccbS, "RETOK") == 0 ) )
                              {
                                 A1206LccbR = AV163LccbR ;
                                 n1206LccbR = false ;
                              }
                              /*
                                 INSERT RECORD ON TABLE LCCBPLP1

                              */
                              A1229lccbS = GXutil.now(true, false) ;
                              A1186lccbS = GXutil.substring( AV123lccbS, 1, 8) ;
                              n1186lccbS = false ;
                              A1187lccbS = "Redecard - Retorno realizado - " + AV121ErrDe ;
                              n1187lccbS = false ;
                              /* Using cursor P006E3 */
                              pr_default.execute(1, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                              if ( (pr_default.getStatus(1) == 1) )
                              {
                                 Gx_err = (short)(1) ;
                                 Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                              }
                              else
                              {
                                 Gx_err = (short)(0) ;
                                 Gx_emsg = "" ;
                              }
                              /* End Insert */
                           }
                           A1205lccbR = GXutil.trim( GXutil.substring( AV102Linha, 45, 6)) ;
                           n1205lccbR = false ;
                           A1203lccbR = (long)(GXutil.val( GXutil.trim( GXutil.substring( AV102Linha, 51, 8)), ".")) ;
                           n1203lccbR = false ;
                           A1196lccbR = Gx_date ;
                           n1196lccbR = false ;
                           A1201lccbR = (int)(GXutil.val( AV118RetSt, ".")) ;
                           n1201lccbR = false ;
                           A1199lccbR = GXutil.substring( AV14FileNa, 1, 20) ;
                           n1199lccbR = false ;
                           A1197lccbR = GXutil.now(true, false) ;
                           n1197lccbR = false ;
                           A1200lccbR = GXutil.substring( AV114Refer, 1, 20) ;
                           n1200lccbR = false ;
                           A1198lccbR = "F" ;
                           n1198lccbR = false ;
                        }
                        else
                        {
                           if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
                           {
                              context.msgStatus( "  WARNING: Reference number ["+AV114Refer+"] at Wrong Status: "+A1184lccbS );
                           }
                           AV116cntWr = (int)(AV116cntWr+1) ;
                        }
                        /* Using cursor P006E4 */
                        pr_default.execute(2, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1194lccbS), A1194lccbS, new Boolean(n1206LccbR), A1206LccbR, new Boolean(n1205lccbR), A1205lccbR, new Boolean(n1203lccbR), new Long(A1203lccbR), new Boolean(n1196lccbR), A1196lccbR, new Boolean(n1201lccbR), new Integer(A1201lccbR), new Boolean(n1199lccbR), A1199lccbR, new Boolean(n1197lccbR), A1197lccbR, new Boolean(n1200lccbR), A1200lccbR, new Boolean(n1198lccbR), A1198lccbR, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                     }
                  }
                  pr_default.readNext(0);
               }
               pr_default.close(0);
               if ( ( AV183GXLvl == 0 ) )
               {
                  if ( ( GXutil.strSearch( AV11DebugM, "NOBATCH", 1) == 0 ) )
                  {
                     context.msgStatus( "  WARNING: Reference number not found: ["+AV114Refer+"]" );
                  }
                  AV115cntNo = (int)(AV115cntNo+1) ;
               }
            }
         }
         else if ( ( GXutil.strcmp(AV103RecTy, "30") == 0 ) )
         {
            if ( ( GXutil.strcmp(AV109MovSt, "A") == 0 ) )
            {
               AV167LccbR = GXutil.substring( AV102Linha, 19, 8) ;
               /* Optimized UPDATE. */
               /* Using cursor P006E5 */
               pr_default.execute(3, new Object[] {new Boolean(n1204lccbR), AV167LccbR, AV166NumLo});
               /* End optimized UPDATE. */
            }
         }
         else
         {
         }
      }
   }

   public void S151( )
   {
      /* 'PROCREJECT' Routine */
      AV147LccbC = "CA" ;
      AV140I = (short)(1) ;
      while ( ( AV140I <= 2 ) )
      {
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV156TipoP ,
                                              A1193lccbS ,
                                              AV141VarTe ,
                                              A1490Distr ,
                                              A1184lccbS ,
                                              AV158LccbS ,
                                              A1227lccbO },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         /* Using cursor P006E6 */
         pr_default.execute(4, new Object[] {AV158LccbS});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1227lccbO = P006E6_A1227lccbO[0] ;
            A1184lccbS = P006E6_A1184lccbS[0] ;
            n1184lccbS = P006E6_n1184lccbS[0] ;
            A1228lccbF = P006E6_A1228lccbF[0] ;
            A1226lccbA = P006E6_A1226lccbA[0] ;
            A1225lccbC = P006E6_A1225lccbC[0] ;
            A1224lccbC = P006E6_A1224lccbC[0] ;
            A1223lccbD = P006E6_A1223lccbD[0] ;
            A1222lccbI = P006E6_A1222lccbI[0] ;
            A1150lccbE = P006E6_A1150lccbE[0] ;
            A1193lccbS = P006E6_A1193lccbS[0] ;
            n1193lccbS = P006E6_n1193lccbS[0] ;
            A1490Distr = P006E6_A1490Distr[0] ;
            n1490Distr = P006E6_n1490Distr[0] ;
            A1196lccbR = P006E6_A1196lccbR[0] ;
            n1196lccbR = P006E6_n1196lccbR[0] ;
            A1201lccbR = P006E6_A1201lccbR[0] ;
            n1201lccbR = P006E6_n1201lccbR[0] ;
            A1199lccbR = P006E6_A1199lccbR[0] ;
            n1199lccbR = P006E6_n1199lccbR[0] ;
            A1197lccbR = P006E6_A1197lccbR[0] ;
            n1197lccbR = P006E6_n1197lccbR[0] ;
            A1200lccbR = P006E6_A1200lccbR[0] ;
            n1200lccbR = P006E6_n1200lccbR[0] ;
            A1198lccbR = P006E6_A1198lccbR[0] ;
            n1198lccbR = P006E6_n1198lccbR[0] ;
            A1185lccbB = P006E6_A1185lccbB[0] ;
            n1185lccbB = P006E6_n1185lccbB[0] ;
            if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "R") == 0 ) )
            {
               if ( ( GXutil.strcmp(A1184lccbS, AV158LccbS) == 0 ) )
               {
                  if ( ( GXutil.strcmp(AV156TipoP, "I") == 0 ) )
                  {
                     AV157Descr = "Redecard - register rejected" ;
                  }
                  else
                  {
                     AV157Descr = "Redecard - resubmeter" ;
                  }
                  A1184lccbS = AV152Grava ;
                  n1184lccbS = false ;
                  /*
                     INSERT RECORD ON TABLE LCCBPLP1

                  */
                  A1229lccbS = GXutil.now(true, false) ;
                  A1186lccbS = GXutil.substring( AV152Grava, 1, 8) ;
                  n1186lccbS = false ;
                  A1187lccbS = GXutil.substring( AV157Descr, 1, 120) ;
                  n1187lccbS = false ;
                  /* Using cursor P006E7 */
                  pr_default.execute(5, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                  if ( (pr_default.getStatus(5) == 1) )
                  {
                     Gx_err = (short)(1) ;
                     Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                  }
                  else
                  {
                     Gx_err = (short)(0) ;
                     Gx_emsg = "" ;
                  }
                  /* End Insert */
                  if ( ( GXutil.strcmp(AV156TipoP, "T") == 0 ) )
                  {
                     AV160Repro = (int)(AV160Repro+1) ;
                  }
                  else
                  {
                     AV161RegRe = (int)(AV161RegRe+1) ;
                  }
                  A1196lccbR = Gx_date ;
                  n1196lccbR = false ;
                  A1201lccbR = (int)(GXutil.val( AV118RetSt, ".")) ;
                  n1201lccbR = false ;
                  A1199lccbR = GXutil.substring( AV14FileNa, 1, 20) ;
                  n1199lccbR = false ;
                  A1197lccbR = GXutil.now(true, false) ;
                  n1197lccbR = false ;
                  A1200lccbR = GXutil.substring( A1193lccbS, 1, 20) ;
                  n1200lccbR = false ;
                  A1198lccbR = "F" ;
                  n1198lccbR = false ;
                  /* Using cursor P006E8 */
                  pr_default.execute(6, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1196lccbR), A1196lccbR, new Boolean(n1201lccbR), new Integer(A1201lccbR), new Boolean(n1199lccbR), A1199lccbR, new Boolean(n1197lccbR), A1197lccbR, new Boolean(n1200lccbR), A1200lccbR, new Boolean(n1198lccbR), A1198lccbR, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
               }
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         AV147LccbC = "DC" ;
         AV140I = (short)(AV140I+1) ;
      }
   }

   public void S141( )
   {
      /* 'ENVIAREMAIL' Routine */
      GXt_char14 = AV178To ;
      GXv_char16[0] = GXt_char14 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_TO_ICSI", "S", "", "", GXv_char16) ;
      pretrsubrc01.this.GXt_char14 = GXv_char16[0] ;
      AV178To = GXt_char14 ;
      GXt_char14 = AV176CC ;
      GXv_char16[0] = GXt_char14 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_CC_ICSI", "S", "", "", GXv_char16) ;
      pretrsubrc01.this.GXt_char14 = GXv_char16[0] ;
      AV176CC = GXt_char14 ;
      AV174BCC = "" ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV173Anexo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GXt_char14 = AV177Subje ;
      GXv_char16[0] = GXt_char14 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_RET_INVALIDO", "S", "TIESS ICSI - RETORNO INVALIDO", "", GXv_char16) ;
      pretrsubrc01.this.GXt_char14 = GXv_char16[0] ;
      AV177Subje = GXt_char14 ;
      GXt_char14 = AV175Body ;
      GXv_char16[0] = GXt_char14 ;
      new pr2getparm(remoteHandle, context).execute( "EMAIL_RET_BODY", "S", "<br>The return file [FILE] related to [ADM] is uncompleted.", "", GXv_char16) ;
      pretrsubrc01.this.GXt_char14 = GXv_char16[0] ;
      AV175Body = GXt_char14 ;
      AV175Body = GXutil.strReplace( AV175Body, "[FILE]", AV14FileNa) ;
      AV175Body = GXutil.strReplace( AV175Body, "[ADM]", "RC") ;
      context.msgStatus( "The return file "+GXutil.trim( AV14FileNa)+" related to RC is uncompleted " );
      new penviaemail(remoteHandle, context).execute( AV177Subje, AV175Body, AV178To, AV176CC, AV174BCC, AV173Anexo) ;
   }

   protected void cleanup( )
   {
      this.aP0[0] = pretrsubrc01.this.AV11DebugM;
      this.aP1[0] = pretrsubrc01.this.AV100FileS;
      Application.commit(context, remoteHandle, "DEFAULT", "pretrsubrc01");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV85Versao = "" ;
      AV137msgEr = "" ;
      AV122TestM = (byte)(0) ;
      AV124Verbo = (byte)(0) ;
      AV9DataB = "" ;
      Gx_date = GXutil.nullDate() ;
      AV10DataC = "" ;
      AV28HoraA = "" ;
      AV29HoraB = "" ;
      AV160Repro = 0 ;
      AV161RegRe = 0 ;
      returnInSub = false ;
      AV14FileNa = "" ;
      AV15FileNo = 0 ;
      AV139ErroG = (byte)(0) ;
      AV110Error = (byte)(0) ;
      AV104n = 0 ;
      AV131cntCC = 0 ;
      AV129cntCC = 0 ;
      AV132cntPL = 0 ;
      AV130cntPL = 0 ;
      GXt_char4 = "" ;
      AV127cntAc = 0 ;
      GXt_char1 = "" ;
      GXt_char5 = "" ;
      GXt_char6 = "" ;
      AV128cntRe = 0 ;
      GXt_char7 = "" ;
      GXt_char8 = "" ;
      GXt_char9 = "" ;
      AV119cntWr = 0 ;
      GXt_char10 = "" ;
      AV116cntWr = 0 ;
      GXt_char11 = "" ;
      AV115cntNo = 0 ;
      GXt_char12 = "" ;
      GXt_char13 = "" ;
      AV162Repro = 0 ;
      AV136TotRe = 0 ;
      AV172Erro_ = "" ;
      AV106LineC = 0 ;
      AV113OpenB = 0 ;
      AV107OpenB = 0 ;
      GX_I = 0 ;
      AV138Erros = new String [999] ;
      GX_I = 1 ;
      while ( ( GX_I <= 999 ) )
      {
         AV138Erros[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV144VetLo = new String [9999] ;
      GX_I = 1 ;
      while ( ( GX_I <= 9999 ) )
      {
         AV144VetLo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV145VetLo = new String [9999] ;
      GX_I = 1 ;
      while ( ( GX_I <= 9999 ) )
      {
         AV145VetLo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV146Conta = (short)(0) ;
      AV102Linha = "" ;
      AV105LineC = "" ;
      AV171linha = "" ;
      Gx_msg = "" ;
      AV103RecTy = "" ;
      AV112Somas = new long [4] ;
      AV109MovSt = "" ;
      AV148Dia = (byte)(0) ;
      AV149Mes = (byte)(0) ;
      AV150Ano = (short)(0) ;
      AV143LccbS = GXutil.nullDate() ;
      AV154VarMo = "" ;
      AV141VarTe = "" ;
      AV158LccbS = "" ;
      AV152Grava = "" ;
      AV156TipoP = "" ;
      AV134Conta = 0 ;
      GXt_int15 = (short)(0) ;
      AV164Txt = "" ;
      AV163LccbR = GXutil.nullDate() ;
      AV165LccbR = GXutil.nullDate() ;
      AV114Refer = "" ;
      AV147LccbC = "" ;
      AV117trnTy = "" ;
      AV118RetSt = "" ;
      AV123lccbS = "" ;
      AV121ErrDe = "" ;
      GXv_char3 = new String [1] ;
      GXv_char2 = new String [1] ;
      AV183GXLvl = (byte)(0) ;
      scmdbuf = "" ;
      P006E2_A1228lccbF = new String[] {""} ;
      P006E2_A1227lccbO = new String[] {""} ;
      P006E2_A1226lccbA = new String[] {""} ;
      P006E2_A1225lccbC = new String[] {""} ;
      P006E2_A1224lccbC = new String[] {""} ;
      P006E2_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006E2_A1222lccbI = new String[] {""} ;
      P006E2_A1150lccbE = new String[] {""} ;
      P006E2_A1193lccbS = new String[] {""} ;
      P006E2_n1193lccbS = new boolean[] {false} ;
      P006E2_A1490Distr = new String[] {""} ;
      P006E2_n1490Distr = new boolean[] {false} ;
      P006E2_A1184lccbS = new String[] {""} ;
      P006E2_n1184lccbS = new boolean[] {false} ;
      P006E2_A1194lccbS = new String[] {""} ;
      P006E2_n1194lccbS = new boolean[] {false} ;
      P006E2_A1206LccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006E2_n1206LccbR = new boolean[] {false} ;
      P006E2_A1205lccbR = new String[] {""} ;
      P006E2_n1205lccbR = new boolean[] {false} ;
      P006E2_A1203lccbR = new long[1] ;
      P006E2_n1203lccbR = new boolean[] {false} ;
      P006E2_A1196lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006E2_n1196lccbR = new boolean[] {false} ;
      P006E2_A1201lccbR = new int[1] ;
      P006E2_n1201lccbR = new boolean[] {false} ;
      P006E2_A1199lccbR = new String[] {""} ;
      P006E2_n1199lccbR = new boolean[] {false} ;
      P006E2_A1197lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006E2_n1197lccbR = new boolean[] {false} ;
      P006E2_A1200lccbR = new String[] {""} ;
      P006E2_n1200lccbR = new boolean[] {false} ;
      P006E2_A1198lccbR = new String[] {""} ;
      P006E2_n1198lccbR = new boolean[] {false} ;
      P006E2_A1185lccbB = new String[] {""} ;
      P006E2_n1185lccbB = new boolean[] {false} ;
      A1228lccbF = "" ;
      A1227lccbO = "" ;
      A1226lccbA = "" ;
      A1225lccbC = "" ;
      A1224lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1150lccbE = "" ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1490Distr = "" ;
      n1490Distr = false ;
      A1184lccbS = "" ;
      n1184lccbS = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      A1206LccbR = GXutil.nullDate() ;
      n1206LccbR = false ;
      A1205lccbR = "" ;
      n1205lccbR = false ;
      A1203lccbR = 0 ;
      n1203lccbR = false ;
      A1196lccbR = GXutil.nullDate() ;
      n1196lccbR = false ;
      A1201lccbR = 0 ;
      n1201lccbR = false ;
      A1199lccbR = "" ;
      n1199lccbR = false ;
      A1197lccbR = GXutil.resetTime( GXutil.nullDate() );
      n1197lccbR = false ;
      A1200lccbR = "" ;
      n1200lccbR = false ;
      A1198lccbR = "" ;
      n1198lccbR = false ;
      A1185lccbB = "" ;
      n1185lccbB = false ;
      AV166NumLo = "" ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV167LccbR = "" ;
      n1204lccbR = false ;
      A1204lccbR = "" ;
      AV140I = (short)(0) ;
      P006E6_A1227lccbO = new String[] {""} ;
      P006E6_A1184lccbS = new String[] {""} ;
      P006E6_n1184lccbS = new boolean[] {false} ;
      P006E6_A1228lccbF = new String[] {""} ;
      P006E6_A1226lccbA = new String[] {""} ;
      P006E6_A1225lccbC = new String[] {""} ;
      P006E6_A1224lccbC = new String[] {""} ;
      P006E6_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P006E6_A1222lccbI = new String[] {""} ;
      P006E6_A1150lccbE = new String[] {""} ;
      P006E6_A1193lccbS = new String[] {""} ;
      P006E6_n1193lccbS = new boolean[] {false} ;
      P006E6_A1490Distr = new String[] {""} ;
      P006E6_n1490Distr = new boolean[] {false} ;
      P006E6_A1196lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006E6_n1196lccbR = new boolean[] {false} ;
      P006E6_A1201lccbR = new int[1] ;
      P006E6_n1201lccbR = new boolean[] {false} ;
      P006E6_A1199lccbR = new String[] {""} ;
      P006E6_n1199lccbR = new boolean[] {false} ;
      P006E6_A1197lccbR = new java.util.Date[] {GXutil.nullDate()} ;
      P006E6_n1197lccbR = new boolean[] {false} ;
      P006E6_A1200lccbR = new String[] {""} ;
      P006E6_n1200lccbR = new boolean[] {false} ;
      P006E6_A1198lccbR = new String[] {""} ;
      P006E6_n1198lccbR = new boolean[] {false} ;
      P006E6_A1185lccbB = new String[] {""} ;
      P006E6_n1185lccbB = new boolean[] {false} ;
      AV157Descr = "" ;
      AV178To = "" ;
      AV176CC = "" ;
      AV174BCC = "" ;
      AV173Anexo = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV173Anexo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV177Subje = "" ;
      AV175Body = "" ;
      GXv_char16 = new String [1] ;
      GXt_char14 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretrsubrc01__default(),
         new Object[] {
             new Object[] {
            P006E2_A1228lccbF, P006E2_A1227lccbO, P006E2_A1226lccbA, P006E2_A1225lccbC, P006E2_A1224lccbC, P006E2_A1223lccbD, P006E2_A1222lccbI, P006E2_A1150lccbE, P006E2_A1193lccbS, P006E2_n1193lccbS,
            P006E2_A1490Distr, P006E2_n1490Distr, P006E2_A1184lccbS, P006E2_n1184lccbS, P006E2_A1194lccbS, P006E2_n1194lccbS, P006E2_A1206LccbR, P006E2_n1206LccbR, P006E2_A1205lccbR, P006E2_n1205lccbR,
            P006E2_A1203lccbR, P006E2_n1203lccbR, P006E2_A1196lccbR, P006E2_n1196lccbR, P006E2_A1201lccbR, P006E2_n1201lccbR, P006E2_A1199lccbR, P006E2_n1199lccbR, P006E2_A1197lccbR, P006E2_n1197lccbR,
            P006E2_A1200lccbR, P006E2_n1200lccbR, P006E2_A1198lccbR, P006E2_n1198lccbR, P006E2_A1185lccbB, P006E2_n1185lccbB
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P006E6_A1227lccbO, P006E6_A1184lccbS, P006E6_n1184lccbS, P006E6_A1228lccbF, P006E6_A1226lccbA, P006E6_A1225lccbC, P006E6_A1224lccbC, P006E6_A1223lccbD, P006E6_A1222lccbI, P006E6_A1150lccbE,
            P006E6_A1193lccbS, P006E6_n1193lccbS, P006E6_A1490Distr, P006E6_n1490Distr, P006E6_A1196lccbR, P006E6_n1196lccbR, P006E6_A1201lccbR, P006E6_n1201lccbR, P006E6_A1199lccbR, P006E6_n1199lccbR,
            P006E6_A1197lccbR, P006E6_n1197lccbR, P006E6_A1200lccbR, P006E6_n1200lccbR, P006E6_A1198lccbR, P006E6_n1198lccbR, P006E6_A1185lccbB, P006E6_n1185lccbB
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV122TestM ;
   private byte AV124Verbo ;
   private byte AV139ErroG ;
   private byte AV110Error ;
   private byte AV148Dia ;
   private byte AV149Mes ;
   private byte AV183GXLvl ;
   private short AV146Conta ;
   private short AV150Ano ;
   private short GXt_int15 ;
   private short A1230lccbS ;
   private short Gx_err ;
   private short AV140I ;
   private int AV160Repro ;
   private int AV161RegRe ;
   private int AV104n ;
   private int AV131cntCC ;
   private int AV129cntCC ;
   private int AV132cntPL ;
   private int AV130cntPL ;
   private int AV127cntAc ;
   private int AV128cntRe ;
   private int AV119cntWr ;
   private int AV116cntWr ;
   private int AV115cntNo ;
   private int AV162Repro ;
   private int AV136TotRe ;
   private int AV106LineC ;
   private int AV113OpenB ;
   private int AV107OpenB ;
   private int GX_I ;
   private int AV134Conta ;
   private int A1201lccbR ;
   private int GX_INS236 ;
   private long AV15FileNo ;
   private long AV112Somas[] ;
   private long A1203lccbR ;
   private String AV11DebugM ;
   private String AV100FileS ;
   private String AV85Versao ;
   private String AV137msgEr ;
   private String AV9DataB ;
   private String AV10DataC ;
   private String AV28HoraA ;
   private String AV29HoraB ;
   private String AV14FileNa ;
   private String GXt_char4 ;
   private String GXt_char1 ;
   private String GXt_char5 ;
   private String GXt_char6 ;
   private String GXt_char7 ;
   private String GXt_char8 ;
   private String GXt_char9 ;
   private String GXt_char10 ;
   private String GXt_char11 ;
   private String GXt_char12 ;
   private String GXt_char13 ;
   private String AV172Erro_ ;
   private String AV138Erros[] ;
   private String AV144VetLo[] ;
   private String AV145VetLo[] ;
   private String AV102Linha ;
   private String AV105LineC ;
   private String AV171linha ;
   private String Gx_msg ;
   private String AV103RecTy ;
   private String AV109MovSt ;
   private String AV154VarMo ;
   private String AV141VarTe ;
   private String AV158LccbS ;
   private String AV152Grava ;
   private String AV156TipoP ;
   private String AV164Txt ;
   private String AV114Refer ;
   private String AV147LccbC ;
   private String AV117trnTy ;
   private String AV118RetSt ;
   private String AV123lccbS ;
   private String AV121ErrDe ;
   private String GXv_char3[] ;
   private String GXv_char2[] ;
   private String scmdbuf ;
   private String A1228lccbF ;
   private String A1227lccbO ;
   private String A1226lccbA ;
   private String A1225lccbC ;
   private String A1224lccbC ;
   private String A1222lccbI ;
   private String A1150lccbE ;
   private String A1193lccbS ;
   private String A1490Distr ;
   private String A1184lccbS ;
   private String A1194lccbS ;
   private String A1205lccbR ;
   private String A1199lccbR ;
   private String A1200lccbR ;
   private String A1198lccbR ;
   private String A1185lccbB ;
   private String AV166NumLo ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String AV167LccbR ;
   private String A1204lccbR ;
   private String GXv_char16[] ;
   private String GXt_char14 ;
   private java.util.Date A1197lccbR ;
   private java.util.Date A1229lccbS ;
   private java.util.Date Gx_date ;
   private java.util.Date AV143LccbS ;
   private java.util.Date AV163LccbR ;
   private java.util.Date AV165LccbR ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1206LccbR ;
   private java.util.Date A1196lccbR ;
   private boolean returnInSub ;
   private boolean n1193lccbS ;
   private boolean n1490Distr ;
   private boolean n1184lccbS ;
   private boolean n1194lccbS ;
   private boolean n1206LccbR ;
   private boolean n1205lccbR ;
   private boolean n1203lccbR ;
   private boolean n1196lccbR ;
   private boolean n1201lccbR ;
   private boolean n1199lccbR ;
   private boolean n1197lccbR ;
   private boolean n1200lccbR ;
   private boolean n1198lccbR ;
   private boolean n1185lccbB ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n1204lccbR ;
   private String A1187lccbS ;
   private String AV157Descr ;
   private String AV178To ;
   private String AV176CC ;
   private String AV174BCC ;
   private String AV173Anexo[] ;
   private String AV177Subje ;
   private String AV175Body ;
   private String[] aP0 ;
   private String[] aP1 ;
   private IDataStoreProvider pr_default ;
   private String[] P006E2_A1228lccbF ;
   private String[] P006E2_A1227lccbO ;
   private String[] P006E2_A1226lccbA ;
   private String[] P006E2_A1225lccbC ;
   private String[] P006E2_A1224lccbC ;
   private java.util.Date[] P006E2_A1223lccbD ;
   private String[] P006E2_A1222lccbI ;
   private String[] P006E2_A1150lccbE ;
   private String[] P006E2_A1193lccbS ;
   private boolean[] P006E2_n1193lccbS ;
   private String[] P006E2_A1490Distr ;
   private boolean[] P006E2_n1490Distr ;
   private String[] P006E2_A1184lccbS ;
   private boolean[] P006E2_n1184lccbS ;
   private String[] P006E2_A1194lccbS ;
   private boolean[] P006E2_n1194lccbS ;
   private java.util.Date[] P006E2_A1206LccbR ;
   private boolean[] P006E2_n1206LccbR ;
   private String[] P006E2_A1205lccbR ;
   private boolean[] P006E2_n1205lccbR ;
   private long[] P006E2_A1203lccbR ;
   private boolean[] P006E2_n1203lccbR ;
   private java.util.Date[] P006E2_A1196lccbR ;
   private boolean[] P006E2_n1196lccbR ;
   private int[] P006E2_A1201lccbR ;
   private boolean[] P006E2_n1201lccbR ;
   private String[] P006E2_A1199lccbR ;
   private boolean[] P006E2_n1199lccbR ;
   private java.util.Date[] P006E2_A1197lccbR ;
   private boolean[] P006E2_n1197lccbR ;
   private String[] P006E2_A1200lccbR ;
   private boolean[] P006E2_n1200lccbR ;
   private String[] P006E2_A1198lccbR ;
   private boolean[] P006E2_n1198lccbR ;
   private String[] P006E2_A1185lccbB ;
   private boolean[] P006E2_n1185lccbB ;
   private String[] P006E6_A1227lccbO ;
   private String[] P006E6_A1184lccbS ;
   private boolean[] P006E6_n1184lccbS ;
   private String[] P006E6_A1228lccbF ;
   private String[] P006E6_A1226lccbA ;
   private String[] P006E6_A1225lccbC ;
   private String[] P006E6_A1224lccbC ;
   private java.util.Date[] P006E6_A1223lccbD ;
   private String[] P006E6_A1222lccbI ;
   private String[] P006E6_A1150lccbE ;
   private String[] P006E6_A1193lccbS ;
   private boolean[] P006E6_n1193lccbS ;
   private String[] P006E6_A1490Distr ;
   private boolean[] P006E6_n1490Distr ;
   private java.util.Date[] P006E6_A1196lccbR ;
   private boolean[] P006E6_n1196lccbR ;
   private int[] P006E6_A1201lccbR ;
   private boolean[] P006E6_n1201lccbR ;
   private String[] P006E6_A1199lccbR ;
   private boolean[] P006E6_n1199lccbR ;
   private java.util.Date[] P006E6_A1197lccbR ;
   private boolean[] P006E6_n1197lccbR ;
   private String[] P006E6_A1200lccbR ;
   private boolean[] P006E6_n1200lccbR ;
   private String[] P006E6_A1198lccbR ;
   private boolean[] P006E6_n1198lccbR ;
   private String[] P006E6_A1185lccbB ;
   private boolean[] P006E6_n1185lccbB ;
}

final  class pretrsubrc01__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P006E6( String AV156TipoP ,
                                        String A1193lccbS ,
                                        String AV141VarTe ,
                                        String A1490Distr ,
                                        String A1184lccbS ,
                                        String AV158LccbS ,
                                        String A1227lccbO )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT [lccbOpCode], [lccbStatus], [lccbFPAC_PLP], [lccbAppCode], [lccbCCNum], [lccbCCard]," ;
      scmdbuf = scmdbuf + " [lccbDate], [lccbIATA], [lccbEmpCod], [lccbSubTrn], [DistribuicaoTransacoesTipo]," ;
      scmdbuf = scmdbuf + " [lccbRSubDate], [lccbRSubError], [lccbRSubFile], [lccbRSubTime], [lccbRSubTrn]," ;
      scmdbuf = scmdbuf + " [lccbRSubType], [lccbBatchNum] FROM [LCCBPLP] WITH (NOLOCK)" ;
      scmdbuf = scmdbuf + " WHERE ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and (SUBSTRING([DistribuicaoTransacoesTipo], 1, 1) = 'R')" ;
      scmdbuf = scmdbuf + " and ([lccbStatus] = '" + GXutil.rtrim( GXutil.strReplace( AV158LccbS, "'", "''")) + "')" ;
      sWhereString = "" ;
      if ( ( GXutil.strcmp(AV156TipoP, "I") == 0 ) )
      {
         sWhereString = sWhereString + " and ([lccbSubTrn] = '" + GXutil.rtrim( GXutil.strReplace( AV141VarTe, "'", "''")) + "')" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY [lccbOpCode], [DistribuicaoTransacoesTipo], [lccbStatus], [lccbBatchNum], [lccbSubTrn]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 4 :
                  return conditional_P006E6( (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P006E2", "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbAppCode], [lccbCCNum], [lccbCCard], [lccbDate], [lccbIATA], [lccbEmpCod], [lccbSubTrn], [DistribuicaoTransacoesTipo], [lccbStatus], [lccbSubRO], [LccbRCredDate], [lccbRSubAppCode], [lccbRSubCCCF], [lccbRSubDate], [lccbRSubError], [lccbRSubFile], [lccbRSubTime], [lccbRSubTrn], [lccbRSubType], [lccbBatchNum] FROM [LCCBPLP] WITH (UPDLOCK) WHERE ([lccbOpCode] = 'S') AND (([lccbOpCode] = 'S') AND (SUBSTRING([DistribuicaoTransacoesTipo], 1, 1) = 'R') AND ([lccbSubTrn] = ?)) ORDER BY [lccbOpCode], [DistribuicaoTransacoesTipo], [lccbBatchNum], [lccbSubTrn] ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006E3", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006E4", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbSubRO]=?, [LccbRCredDate]=?, [lccbRSubAppCode]=?, [lccbRSubCCCF]=?, [lccbRSubDate]=?, [lccbRSubError]=?, [lccbRSubFile]=?, [lccbRSubTime]=?, [lccbRSubTrn]=?, [lccbRSubType]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006E5", "UPDATE [LCCBPLP] SET [lccbRSubRO]=?  WHERE ([lccbOpCode] = 'S') AND (SUBSTRING([DistribuicaoTransacoesTipo], 1, 1) = 'R') AND ([lccbSubRO] = ?) AND ([lccbStatus] = 'RETOK')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P006E6", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P006E7", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P006E8", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbRSubDate]=?, [lccbRSubError]=?, [lccbRSubFile]=?, [lccbRSubTime]=?, [lccbRSubTrn]=?, [lccbRSubType]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 44) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 2) ;
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(6) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 7) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 3) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 20) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 4) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 8) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 10) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[16])[0] = rslt.getGXDate(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 10) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((long[]) buf[20])[0] = rslt.getLong(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[22])[0] = rslt.getGXDate(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((int[]) buf[24])[0] = rslt.getInt(17) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(18, 20) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[28])[0] = rslt.getGXDateTime(19) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 1) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((String[]) buf[34])[0] = rslt.getString(22, 20) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 8) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getString(3, 19) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[6])[0] = rslt.getString(6, 2) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(7) ;
               ((String[]) buf[8])[0] = rslt.getString(8, 7) ;
               ((String[]) buf[9])[0] = rslt.getString(9, 3) ;
               ((String[]) buf[10])[0] = rslt.getString(10, 20) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 4) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[14])[0] = rslt.getGXDate(12) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((int[]) buf[16])[0] = rslt.getInt(13) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((String[]) buf[18])[0] = rslt.getString(14, 20) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[20])[0] = rslt.getGXDateTime(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((String[]) buf[22])[0] = rslt.getString(16, 20) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(17, 1) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((String[]) buf[26])[0] = rslt.getString(18, 20) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 13);
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 2 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[3], 10);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(3, (java.util.Date)parms[5]);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 10);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(5, ((Number) parms[9]).longValue());
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(6, (java.util.Date)parms[11]);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(7, ((Number) parms[13]).intValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 20);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(9, (java.util.Date)parms[17], false);
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[19], 20);
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[21], 1);
               }
               stmt.setString(12, (String)parms[22], 3);
               stmt.setString(13, (String)parms[23], 7);
               stmt.setDate(14, (java.util.Date)parms[24]);
               stmt.setString(15, (String)parms[25], 2);
               stmt.setString(16, (String)parms[26], 44);
               stmt.setString(17, (String)parms[27], 20);
               stmt.setString(18, (String)parms[28], 1);
               stmt.setString(19, (String)parms[29], 19);
               break;
            case 3 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 10);
               }
               stmt.setString(2, (String)parms[2], 5);
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(2, (java.util.Date)parms[3]);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(3, ((Number) parms[5]).intValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(4, (String)parms[7], 20);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(5, (java.util.Date)parms[9], false);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 1);
               }
               stmt.setString(8, (String)parms[14], 3);
               stmt.setString(9, (String)parms[15], 7);
               stmt.setDate(10, (java.util.Date)parms[16]);
               stmt.setString(11, (String)parms[17], 2);
               stmt.setString(12, (String)parms[18], 44);
               stmt.setString(13, (String)parms[19], 20);
               stmt.setString(14, (String)parms[20], 1);
               stmt.setString(15, (String)parms[21], 19);
               break;
      }
   }

}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtIT0C_IT0CItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtIT0C_IT0CItem( )
   {
      this(  new ModelContext(SdtIT0C_IT0CItem.class));
   }

   public SdtIT0C_IT0CItem( ModelContext context )
   {
      super( context, "SdtIT0C_IT0CItem");
   }

   public SdtIT0C_IT0CItem( StructSdtIT0C_IT0CItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PLID_IT0C") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0C_IT0CItem_Plid_it0c = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PLTX_IT0C") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtIT0C_IT0CItem_Pltx_it0c = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "IT0C.IT0CItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("PLID_IT0C", GXutil.rtrim( gxTv_SdtIT0C_IT0CItem_Plid_it0c));
      oWriter.writeElement("PLTX_IT0C", GXutil.rtrim( gxTv_SdtIT0C_IT0CItem_Pltx_it0c));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtIT0C_IT0CItem_Plid_it0c( )
   {
      return gxTv_SdtIT0C_IT0CItem_Plid_it0c ;
   }

   public void setgxTv_SdtIT0C_IT0CItem_Plid_it0c( String value )
   {
      gxTv_SdtIT0C_IT0CItem_Plid_it0c = value ;
      return  ;
   }

   public void setgxTv_SdtIT0C_IT0CItem_Plid_it0c_SetNull( )
   {
      gxTv_SdtIT0C_IT0CItem_Plid_it0c = "" ;
      return  ;
   }

   public String getgxTv_SdtIT0C_IT0CItem_Pltx_it0c( )
   {
      return gxTv_SdtIT0C_IT0CItem_Pltx_it0c ;
   }

   public void setgxTv_SdtIT0C_IT0CItem_Pltx_it0c( String value )
   {
      gxTv_SdtIT0C_IT0CItem_Pltx_it0c = value ;
      return  ;
   }

   public void setgxTv_SdtIT0C_IT0CItem_Pltx_it0c_SetNull( )
   {
      gxTv_SdtIT0C_IT0CItem_Pltx_it0c = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtIT0C_IT0CItem_Plid_it0c = "" ;
      gxTv_SdtIT0C_IT0CItem_Pltx_it0c = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char2 = "" ;
      return  ;
   }

   public SdtIT0C_IT0CItem Clone( )
   {
      return (SdtIT0C_IT0CItem)(clone()) ;
   }

   public void setStruct( StructSdtIT0C_IT0CItem struct )
   {
      setgxTv_SdtIT0C_IT0CItem_Plid_it0c(struct.getPlid_it0c());
      setgxTv_SdtIT0C_IT0CItem_Pltx_it0c(struct.getPltx_it0c());
   }

   public StructSdtIT0C_IT0CItem getStruct( )
   {
      StructSdtIT0C_IT0CItem struct = new StructSdtIT0C_IT0CItem ();
      struct.setPlid_it0c(getgxTv_SdtIT0C_IT0CItem_Plid_it0c());
      struct.setPltx_it0c(getgxTv_SdtIT0C_IT0CItem_Pltx_it0c());
      return struct ;
   }

   private short nOutParmCount ;
   private short readOk ;
   private String sTagName ;
   private String GXt_char2 ;
   private String gxTv_SdtIT0C_IT0CItem_Plid_it0c ;
   private String gxTv_SdtIT0C_IT0CItem_Pltx_it0c ;
}


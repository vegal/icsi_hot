
public final  class StructSdtUserProfiles implements Cloneable, java.io.Serializable
{
   public StructSdtUserProfiles( )
   {
      gxTv_SdtUserProfiles_Ustcode = "" ;
      gxTv_SdtUserProfiles_Ustdescription = "" ;
      gxTv_SdtUserProfiles_Functions = new java.util.Vector();
      gxTv_SdtUserProfiles_Mode = "" ;
      gxTv_SdtUserProfiles_Ustcode_Z = "" ;
      gxTv_SdtUserProfiles_Ustdescription_Z = "" ;
      gxTv_SdtUserProfiles_Ustdescription_N = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getUstcode( )
   {
      return gxTv_SdtUserProfiles_Ustcode ;
   }

   public void setUstcode( String value )
   {
      gxTv_SdtUserProfiles_Ustcode = value ;
      return  ;
   }

   public String getUstdescription( )
   {
      return gxTv_SdtUserProfiles_Ustdescription ;
   }

   public void setUstdescription( String value )
   {
      gxTv_SdtUserProfiles_Ustdescription = value ;
      return  ;
   }

   public java.util.Vector getFunctions( )
   {
      return gxTv_SdtUserProfiles_Functions ;
   }

   public void setFunctions( java.util.Vector value )
   {
      gxTv_SdtUserProfiles_Functions = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtUserProfiles_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtUserProfiles_Mode = value ;
      return  ;
   }

   public String getUstcode_Z( )
   {
      return gxTv_SdtUserProfiles_Ustcode_Z ;
   }

   public void setUstcode_Z( String value )
   {
      gxTv_SdtUserProfiles_Ustcode_Z = value ;
      return  ;
   }

   public String getUstdescription_Z( )
   {
      return gxTv_SdtUserProfiles_Ustdescription_Z ;
   }

   public void setUstdescription_Z( String value )
   {
      gxTv_SdtUserProfiles_Ustdescription_Z = value ;
      return  ;
   }

   public byte getUstdescription_N( )
   {
      return gxTv_SdtUserProfiles_Ustdescription_N ;
   }

   public void setUstdescription_N( byte value )
   {
      gxTv_SdtUserProfiles_Ustdescription_N = value ;
      return  ;
   }

   protected byte gxTv_SdtUserProfiles_Ustdescription_N ;
   protected String gxTv_SdtUserProfiles_Mode ;
   protected String gxTv_SdtUserProfiles_Ustcode ;
   protected String gxTv_SdtUserProfiles_Ustdescription ;
   protected String gxTv_SdtUserProfiles_Ustcode_Z ;
   protected String gxTv_SdtUserProfiles_Ustdescription_Z ;
   protected java.util.Vector gxTv_SdtUserProfiles_Functions ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class Sdtsdt_RETFOP_sdt_RETFOPItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public Sdtsdt_RETFOP_sdt_RETFOPItem( )
   {
      this(  new ModelContext(Sdtsdt_RETFOP_sdt_RETFOPItem.class));
   }

   public Sdtsdt_RETFOP_sdt_RETFOPItem( ModelContext context )
   {
      super( context, "Sdtsdt_RETFOP_sdt_RETFOPItem");
   }

   public Sdtsdt_RETFOP_sdt_RETFOPItem( StructSdtsdt_RETFOP_sdt_RETFOPItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "FPAC") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "FPAM") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "APLC") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CUTP") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EXPC") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "FPTP") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "EXDA") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CSTF") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CRCC") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AVCD") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "SAPP") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "FPTI") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "AUTA") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "IT08pos") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "IT08Seq") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "FirstInstallment") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment = GXutil.val( oReader.getValue(), "") ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "FareOriginal") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal = GXutil.val( oReader.getValue(), "") ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "sdt_RETFOP.sdt_RETFOPItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("FPAC", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac));
      oWriter.writeElement("FPAM", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam));
      oWriter.writeElement("APLC", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc));
      oWriter.writeElement("CUTP", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp));
      oWriter.writeElement("EXPC", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc));
      oWriter.writeElement("FPTP", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp));
      oWriter.writeElement("EXDA", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda));
      oWriter.writeElement("CSTF", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf));
      oWriter.writeElement("CRCC", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc));
      oWriter.writeElement("AVCD", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd));
      oWriter.writeElement("SAPP", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp));
      oWriter.writeElement("FPTI", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti));
      oWriter.writeElement("AUTA", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta));
      oWriter.writeElement("IT08pos", GXutil.rtrim( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos));
      oWriter.writeElement("IT08Seq", GXutil.trim( GXutil.str( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq, 1, 0)));
      oWriter.writeElement("FirstInstallment", GXutil.trim( GXutil.str( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment, 12, 2)));
      oWriter.writeElement("FareOriginal", GXutil.trim( GXutil.str( gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal, 12, 2)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta = "" ;
      return  ;
   }

   public String getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos( String value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos = "" ;
      return  ;
   }

   public byte getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq( byte value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq = (byte)(0) ;
      return  ;
   }

   public double getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment( double value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment = 0 ;
      return  ;
   }

   public double getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal( )
   {
      return gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal( double value )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal = value ;
      return  ;
   }

   public void setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal_SetNull( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal = 0 ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos = "" ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq = (byte)(0) ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment = 0 ;
      gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal = 0 ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public Sdtsdt_RETFOP_sdt_RETFOPItem Clone( )
   {
      return (Sdtsdt_RETFOP_sdt_RETFOPItem)(clone()) ;
   }

   public void setStruct( StructSdtsdt_RETFOP_sdt_RETFOPItem struct )
   {
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac(struct.getFpac());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam(struct.getFpam());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc(struct.getAplc());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp(struct.getCutp());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc(struct.getExpc());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp(struct.getFptp());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda(struct.getExda());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf(struct.getCstf());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc(struct.getCrcc());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd(struct.getAvcd());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp(struct.getSapp());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti(struct.getFpti());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta(struct.getAuta());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos(struct.getIt08pos());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq(struct.getIt08seq());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment(struct.getFirstinstallment());
      setgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal(struct.getFareoriginal());
   }

   public StructSdtsdt_RETFOP_sdt_RETFOPItem getStruct( )
   {
      StructSdtsdt_RETFOP_sdt_RETFOPItem struct = new StructSdtsdt_RETFOP_sdt_RETFOPItem ();
      struct.setFpac(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac());
      struct.setFpam(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam());
      struct.setAplc(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc());
      struct.setCutp(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp());
      struct.setExpc(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc());
      struct.setFptp(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp());
      struct.setExda(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda());
      struct.setCstf(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf());
      struct.setCrcc(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc());
      struct.setAvcd(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd());
      struct.setSapp(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp());
      struct.setFpti(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti());
      struct.setAuta(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta());
      struct.setIt08pos(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos());
      struct.setIt08seq(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq());
      struct.setFirstinstallment(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment());
      struct.setFareoriginal(getgxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal());
      return struct ;
   }

   private byte gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08seq ;
   private short nOutParmCount ;
   private short readOk ;
   private double gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Firstinstallment ;
   private double gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fareoriginal ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpac ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpam ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Aplc ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cutp ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Expc ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fptp ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Exda ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Cstf ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Crcc ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Avcd ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Sapp ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Fpti ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_Auta ;
   private String gxTv_Sdtsdt_RETFOP_sdt_RETFOPItem_It08pos ;
   private String sTagName ;
   private String GXt_char1 ;
}


/*
               File: UTILITARIO_RETi1022
        Description: UTILITARIO_ RETi1022
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: October 14, 2011 17:51:33.69
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class autilitario_reti1022 extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      autilitario_reti1022 pgm = new autilitario_reti1022 (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {
      String[] aP0 = new String[] {""};
      java.util.Date[] aP1 = new java.util.Date[] {GXutil.nullDate()};
      java.util.Date[] aP2 = new java.util.Date[] {GXutil.nullDate()};

      try
      {
         aP0[0] = (String) args[0];
         aP1[0] = (java.util.Date) localUtil.ctod( args[1], 2);
         aP2[0] = (java.util.Date) localUtil.ctod( args[2], 2);
      }
      catch ( ArrayIndexOutOfBoundsException e )
      {
      }

      execute(aP0, aP1, aP2);
   }

   public autilitario_reti1022( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( autilitario_reti1022.class ), "" );
   }

   public autilitario_reti1022( int remoteHandle ,
                                ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        java.util.Date[] aP1 ,
                        java.util.Date[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             java.util.Date[] aP1 ,
                             java.util.Date[] aP2 )
   {
      autilitario_reti1022.this.AV8DebugMo = aP0[0];
      this.aP0 = aP0;
      autilitario_reti1022.this.AV19DateFr = aP1[0];
      this.aP1 = aP1;
      autilitario_reti1022.this.AV20DateTo = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV9Versao = "00018" ;
      AV8DebugMo = GXutil.trim( GXutil.upper( AV8DebugMo)) ;
      if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "i1022/i1023 - Version "+AV9Versao );
         context.msgStatus( "  Running mode: ["+AV8DebugMo+"] - Started at "+GXutil.time( ) );
      }
      /* Execute user subroutine: S1123 */
      S1123 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) == 0 ) )
      {
         context.msgStatus( "  Ended at "+GXutil.time( ) );
      }
      else
      {
         GXutil.msg( this, "T�rmino do processo" );
      }
      cleanup();
   }

   public void S1123( )
   {
      /* 'MAIN' Routine */
      GXt_char2 = AV10Path ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_4122", "Caminho do R4122", "F", "C:\\Temp\\ICSI\\R4122", GXv_svchar3) ;
      autilitario_reti1022.this.GXt_char2 = GXv_svchar3[0] ;
      AV10Path = GXt_char2 ;
      GXt_char2 = AV11Path1 ;
      GXv_svchar3[0] = GXt_char2 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_4122_BSPLink", "Caminho do R4122", "F", "C:\\Temp\\ICSI\\R4122\\BSPLink", GXv_svchar3) ;
      autilitario_reti1022.this.GXt_char2 = GXv_svchar3[0] ;
      AV11Path1 = GXt_char2 ;
      AV12DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) ;
      AV12DataB = AV12DataB + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) ;
      AV12DataB = AV12DataB + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
      /* Using cursor P00772 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1147lccbE = P00772_A1147lccbE[0] ;
         n1147lccbE = P00772_n1147lccbE[0] ;
         A1150lccbE = P00772_A1150lccbE[0] ;
         AV15lccbEm = GXutil.trim( A1150lccbE) ;
         /* Execute user subroutine: S122 */
         S122 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
         if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) > 0 ) )
         {
            context.msgStatus( "    Creating Reports for "+AV15lccbEm+"("+AV16EmpNom+")..." );
         }
         GXt_char2 = AV13PathPD ;
         GXv_svchar3[0] = GXt_char2 ;
         new pdigitoverificador(remoteHandle, context).execute( AV15lccbEm, GXv_svchar3) ;
         autilitario_reti1022.this.GXt_char2 = GXv_svchar3[0] ;
         AV13PathPD = AV10Path + "BRrr" + AV15lccbEm + GXutil.trim( GXt_char2) + "_" + GXutil.trim( AV12DataB) + "_i1022" + ".pdf" ;
         GXt_char2 = AV14PathTX ;
         GXv_svchar3[0] = GXt_char2 ;
         new pdigitoverificador(remoteHandle, context).execute( AV15lccbEm, GXv_svchar3) ;
         autilitario_reti1022.this.GXt_char2 = GXv_svchar3[0] ;
         AV14PathTX = AV10Path + "BRrr" + AV15lccbEm + GXutil.trim( GXt_char2) + "_" + GXutil.trim( AV12DataB) + "_i1023" + ".txt" ;
         if ( ( GXutil.strSearch( AV8DebugMo, "NOBATCH", 1) > 0 ) )
         {
            context.msgStatus( "    "+AV13PathPD );
            context.msgStatus( "    "+AV14PathTX );
         }
         GXv_svchar3[0] = AV15lccbEm ;
         GXv_char4[0] = AV16EmpNom ;
         GXv_char5[0] = AV13PathPD ;
         GXv_char6[0] = AV14PathTX ;
         GXv_char7[0] = AV8DebugMo ;
         GXv_date8[0] = AV19DateFr ;
         GXv_date9[0] = AV20DateTo ;
         new rutilitario_reti1022rpt(remoteHandle, context).execute( GXv_svchar3, GXv_char4, GXv_char5, GXv_char6, GXv_char7, GXv_date8, GXv_date9) ;
         autilitario_reti1022.this.AV15lccbEm = GXv_svchar3[0] ;
         autilitario_reti1022.this.AV16EmpNom = GXv_char4[0] ;
         autilitario_reti1022.this.AV13PathPD = GXv_char5[0] ;
         autilitario_reti1022.this.AV14PathTX = GXv_char6[0] ;
         autilitario_reti1022.this.AV8DebugMo = GXv_char7[0] ;
         autilitario_reti1022.this.AV19DateFr = GXv_date8[0] ;
         autilitario_reti1022.this.AV20DateTo = GXv_date9[0] ;
         if ( ( GXutil.strSearch( AV8DebugMo, "NOMOVE", 1) == 0 ) )
         {
            AV17Comman = "cmd /c move " + AV13PathPD + " " + AV11Path1 ;
            AV18Ret = GXutil.shell( AV17Comman, 1) ;
            AV17Comman = "cmd /c move " + AV14PathTX + " " + AV11Path1 ;
            AV18Ret = GXutil.shell( AV17Comman, 1) ;
         }
         pr_default.readNext(0);
      }
      pr_default.close(0);
   }

   public void S122( )
   {
      /* 'GETEMPNOM' Routine */
      AV25GXLvl6 = (byte)(0) ;
      /* Using cursor P00773 */
      pr_default.execute(1, new Object[] {AV15lccbEm});
      while ( (pr_default.getStatus(1) != 101) )
      {
         A1233EmpCo = P00773_A1233EmpCo[0] ;
         A1234EmpNo = P00773_A1234EmpNo[0] ;
         n1234EmpNo = P00773_n1234EmpNo[0] ;
         AV25GXLvl6 = (byte)(1) ;
         AV16EmpNom = GXutil.trim( A1234EmpNo) ;
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(1);
      if ( ( AV25GXLvl6 == 0 ) )
      {
         AV16EmpNom = "???" ;
      }
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(putilitario_reti1022.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = autilitario_reti1022.this.AV8DebugMo;
      this.aP1[0] = autilitario_reti1022.this.AV19DateFr;
      this.aP2[0] = autilitario_reti1022.this.AV20DateTo;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV9Versao = "" ;
      returnInSub = false ;
      AV10Path = "" ;
      AV11Path1 = "" ;
      AV12DataB = "" ;
      Gx_date = GXutil.nullDate() ;
      scmdbuf = "" ;
      P00772_A1147lccbE = new String[] {""} ;
      P00772_n1147lccbE = new boolean[] {false} ;
      P00772_A1150lccbE = new String[] {""} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      AV15lccbEm = "" ;
      AV16EmpNom = "" ;
      AV13PathPD = "" ;
      AV14PathTX = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      GXv_svchar3 = new String [1] ;
      GXv_char4 = new String [1] ;
      GXv_char5 = new String [1] ;
      GXv_char6 = new String [1] ;
      GXv_char7 = new String [1] ;
      GXv_date8 = new java.util.Date [1] ;
      GXv_date9 = new java.util.Date [1] ;
      AV17Comman = "" ;
      AV18Ret = 0 ;
      AV25GXLvl6 = (byte)(0) ;
      P00773_A1233EmpCo = new String[] {""} ;
      P00773_A1234EmpNo = new String[] {""} ;
      P00773_n1234EmpNo = new boolean[] {false} ;
      A1233EmpCo = "" ;
      A1234EmpNo = "" ;
      n1234EmpNo = false ;
      pr_default = new DataStoreProvider(context, remoteHandle, new autilitario_reti1022__default(),
         new Object[] {
             new Object[] {
            P00772_A1147lccbE, P00772_n1147lccbE, P00772_A1150lccbE
            }
            , new Object[] {
            P00773_A1233EmpCo, P00773_A1234EmpNo, P00773_n1234EmpNo
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV25GXLvl6 ;
   private short Gx_err ;
   private double AV18Ret ;
   private String AV8DebugMo ;
   private String AV9Versao ;
   private String AV10Path ;
   private String AV11Path1 ;
   private String AV12DataB ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String AV15lccbEm ;
   private String AV16EmpNom ;
   private String AV13PathPD ;
   private String AV14PathTX ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXv_char4[] ;
   private String GXv_char5[] ;
   private String GXv_char6[] ;
   private String GXv_char7[] ;
   private String AV17Comman ;
   private String A1233EmpCo ;
   private String A1234EmpNo ;
   private java.util.Date AV19DateFr ;
   private java.util.Date AV20DateTo ;
   private java.util.Date Gx_date ;
   private java.util.Date GXv_date8[] ;
   private java.util.Date GXv_date9[] ;
   private boolean returnInSub ;
   private boolean n1147lccbE ;
   private boolean n1234EmpNo ;
   private String GXv_svchar3[] ;
   private String[] aP0 ;
   private java.util.Date[] aP1 ;
   private java.util.Date[] aP2 ;
   private IDataStoreProvider pr_default ;
   private String[] P00772_A1147lccbE ;
   private boolean[] P00772_n1147lccbE ;
   private String[] P00772_A1150lccbE ;
   private String[] P00773_A1233EmpCo ;
   private String[] P00773_A1234EmpNo ;
   private boolean[] P00773_n1234EmpNo ;
}

final  class autilitario_reti1022__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00772", "SELECT [lccbEmpEnab], [lccbEmpCod] FROM [LCCBEMP] WITH (NOLOCK) WHERE [lccbEmpEnab] = '1' ORDER BY [lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P00773", "SELECT [EmpCod], [EmpNom] FROM [EMPRESAS] WITH (NOLOCK) WHERE [EmpCod] = ? ORDER BY [EmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               break;
      }
   }

}


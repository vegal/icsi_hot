
public final  class StructSdtAirlines_Contacts implements Cloneable, java.io.Serializable
{
   public StructSdtAirlines_Contacts( )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttseq = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Airlinecttname = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttphone = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttemail = "" ;
      gxTv_SdtAirlines_Contacts_Contacttypesdescription = "" ;
      gxTv_SdtAirlines_Contacts_Mode = "" ;
      gxTv_SdtAirlines_Contacts_Modified = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Contacttypescode_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = (short)(0) ;
      gxTv_SdtAirlines_Contacts_Airlinecttname_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = "" ;
      gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = "" ;
      gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getContacttypescode( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypescode ;
   }

   public void setContacttypescode( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode = value ;
      return  ;
   }

   public short getAirlinecttseq( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttseq ;
   }

   public void setAirlinecttseq( short value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttseq = value ;
      return  ;
   }

   public String getAirlinecttname( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttname ;
   }

   public void setAirlinecttname( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttname = value ;
      return  ;
   }

   public String getAirlinecttphone( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttphone ;
   }

   public void setAirlinecttphone( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttphone = value ;
      return  ;
   }

   public String getAirlinecttemail( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttemail ;
   }

   public void setAirlinecttemail( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttemail = value ;
      return  ;
   }

   public String getContacttypesdescription( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypesdescription ;
   }

   public void setContacttypesdescription( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypesdescription = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtAirlines_Contacts_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtAirlines_Contacts_Mode = value ;
      return  ;
   }

   public short getModified( )
   {
      return gxTv_SdtAirlines_Contacts_Modified ;
   }

   public void setModified( short value )
   {
      gxTv_SdtAirlines_Contacts_Modified = value ;
      return  ;
   }

   public String getContacttypescode_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypescode_Z ;
   }

   public void setContacttypescode_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypescode_Z = value ;
      return  ;
   }

   public short getAirlinecttseq_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttseq_Z ;
   }

   public void setAirlinecttseq_Z( short value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttseq_Z = value ;
      return  ;
   }

   public String getAirlinecttname_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttname_Z ;
   }

   public void setAirlinecttname_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttname_Z = value ;
      return  ;
   }

   public String getAirlinecttphone_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttphone_Z ;
   }

   public void setAirlinecttphone_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttphone_Z = value ;
      return  ;
   }

   public String getAirlinecttemail_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Airlinecttemail_Z ;
   }

   public void setAirlinecttemail_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Airlinecttemail_Z = value ;
      return  ;
   }

   public String getContacttypesdescription_Z( )
   {
      return gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z ;
   }

   public void setContacttypesdescription_Z( String value )
   {
      gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z = value ;
      return  ;
   }

   protected short gxTv_SdtAirlines_Contacts_Airlinecttseq ;
   protected short gxTv_SdtAirlines_Contacts_Modified ;
   protected short gxTv_SdtAirlines_Contacts_Airlinecttseq_Z ;
   protected String gxTv_SdtAirlines_Contacts_Mode ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypescode ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttname ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttphone ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttemail ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypesdescription ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypescode_Z ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttname_Z ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttphone_Z ;
   protected String gxTv_SdtAirlines_Contacts_Airlinecttemail_Z ;
   protected String gxTv_SdtAirlines_Contacts_Contacttypesdescription_Z ;
}


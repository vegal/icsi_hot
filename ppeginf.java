/*
               File: PegInf
        Description: Peg Inf
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:7.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class ppeginf extends GXProcedure
{
   public ppeginf( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( ppeginf.class ), "" );
   }

   public ppeginf( int remoteHandle ,
                   ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 ,
                        String[] aP4 ,
                        int[] aP5 ,
                        String[] aP6 ,
                        String[] aP7 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4, aP5, aP6, aP7);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 ,
                             String[] aP4 ,
                             int[] aP5 ,
                             String[] aP6 ,
                             String[] aP7 )
   {
      ppeginf.this.AV201ciaco = aP0[0];
      this.aP0 = aP0;
      ppeginf.this.AV20Period = aP1[0];
      this.aP1 = aP1;
      ppeginf.this.AV281Modo = aP2[0];
      this.aP2 = aP2;
      ppeginf.this.AV282Error = aP3[0];
      this.aP3 = aP3;
      ppeginf.this.AV290VerNu = aP4[0];
      this.aP4 = aP4;
      ppeginf.this.AV291First = aP5[0];
      this.aP5 = aP5;
      ppeginf.this.AV348Type = aP6[0];
      this.aP6 = aP6;
      ppeginf.this.AV378Path = aP7[0];
      this.aP7 = aP7;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV389Short ;
      GXv_char2[0] = AV378Path ;
      GXv_char3[0] = GXt_char1 ;
      new pr2shortname(remoteHandle, context).execute( GXv_char2, GXv_char3) ;
      ppeginf.this.AV378Path = GXv_char2[0] ;
      ppeginf.this.GXt_char1 = GXv_char3[0] ;
      AV389Short = GXt_char1 ;
      GXt_char1 = AV310ENCCC ;
      GXv_char3[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ENCRYPT_CC", "Encrypt Credit Card Information (Y/N) ?", "S", "Y", GXv_char3) ;
      ppeginf.this.GXt_char1 = GXv_char3[0] ;
      AV310ENCCC = GXt_char1 ;
      GXt_char1 = AV302RFDCR ;
      GXv_char3[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "CC_AMOUNT_POS", "Credit Card Amount when it is Refund (Y = pos 36 / N = pos 101)", "S", "N", GXv_char3) ;
      ppeginf.this.GXt_char1 = GXv_char3[0] ;
      AV302RFDCR = GXt_char1 ;
      GXt_char1 = AV288ZumOn ;
      GXv_char3[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ZUMONOFF", "Do Conjunction Tickets have ZUM (Y/N) ?", "S", "N", GXv_char3) ;
      ppeginf.this.GXt_char1 = GXv_char3[0] ;
      AV288ZumOn = GXt_char1 ;
      GXt_char1 = AV286LOADH ;
      GXv_char3[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "LOADHOT5", "Load HOT5 (Y/N) ?", "S", "Y", GXv_char3) ;
      ppeginf.this.GXt_char1 = GXv_char3[0] ;
      AV286LOADH = GXt_char1 ;
      GXt_char1 = AV325PGAMO ;
      GXv_char3[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGAMOUNTPOS101", "Amount position (Y = pos 101 / N = pos 36", "S", "Y", GXv_char3) ;
      ppeginf.this.GXt_char1 = GXv_char3[0] ;
      AV325PGAMO = GXt_char1 ;
      AV311AccTa = "AY,CA,SQ,US" ;
      AV365RFND2 = "CA" ;
      AV345PERDI = "N" ;
      /* Execute user subroutine: S381624 */
      S381624 ();
      if ( returnInSub )
      {
         returnInSub = true;
         cleanup();
         if (true) return;
      }
      AV189Level[1-1] = "PERIOD" ;
      AV189Level[2-1] = "AGENT NNNNNNN" ;
      AV189Level[3-1] = "" ;
      AV189Level[4-1] = "" ;
      AV189Level[5-1] = "" ;
      AV189Level[6-1] = "" ;
      AV189Level[7-1] = "" ;
      AV189Level[8-1] = "" ;
      AV189Level[9-1] = "" ;
      AV189Level[10-1] = "" ;
      AV190LevEv[1-1] = "GROSS AMOUNT" ;
      AV190LevEv[2-1] = "IVA" ;
      AV190LevEv[3-1] = "COMMISSIONS" ;
      AV190LevEv[4-1] = "NET" ;
      AV190LevEv[5-1] = "CREDIT TOTAL" ;
      AV190LevEv[6-1] = "LOCAL TAX" ;
      AV190LevEv[7-1] = "OTHER TAX" ;
      AV190LevEv[8-1] = "COMM + IVA" ;
      AV190LevEv[9-1] = "" ;
      AV190LevEv[10-1] = "" ;
      AV190LevEv[11-1] = "" ;
      AV190LevEv[12-1] = "" ;
      AV190LevEv[13-1] = "" ;
      AV190LevEv[14-1] = "" ;
      AV190LevEv[15-1] = "" ;
      AV320AcctS[1-1][1-1] = "P_GROSSAMO" ;
      AV320AcctS[2-1][1-1] = "A_GROSSAMO" ;
      AV320AcctS[1-1][2-1] = "P_IVA" ;
      AV320AcctS[2-1][2-1] = "A_IVA" ;
      AV320AcctS[1-1][3-1] = "P_COMMTOT" ;
      AV320AcctS[2-1][3-1] = "A_COMMTOT" ;
      AV320AcctS[1-1][4-1] = "P_NET" ;
      AV320AcctS[2-1][4-1] = "A_NET" ;
      AV320AcctS[1-1][5-1] = "P_CREDTOT" ;
      AV320AcctS[2-1][5-1] = "A_CREDTOT" ;
      AV320AcctS[1-1][6-1] = "P_LOCALTAX" ;
      AV320AcctS[2-1][6-1] = "A_LOCALTAX" ;
      AV320AcctS[1-1][7-1] = "P_OTHERTAX" ;
      AV320AcctS[2-1][7-1] = "A_OTHERTAX" ;
      AV320AcctS[1-1][8-1] = "P_COMMIVA" ;
      AV320AcctS[2-1][8-1] = "A_COMMIVA" ;
      AV320AcctS[1-1][9-1] = "P_RFDGROSS" ;
      AV320AcctS[2-1][9-1] = "A_RFDGROSS" ;
      AV320AcctS[1-1][10-1] = "P_RFDIVA" ;
      AV320AcctS[2-1][10-1] = "A_RFDIVA" ;
      AV320AcctS[1-1][11-1] = "P_RFDCOMMT" ;
      AV320AcctS[2-1][11-1] = "A_RFDCOMMT" ;
      AV320AcctS[1-1][12-1] = "P_RFDNET" ;
      AV320AcctS[2-1][12-1] = "A_RFDNET" ;
      AV320AcctS[1-1][13-1] = "P_RFDCREDT" ;
      AV320AcctS[2-1][13-1] = "A_RFDCREDT" ;
      AV320AcctS[1-1][14-1] = "P_RFDLOCTX" ;
      AV320AcctS[2-1][14-1] = "A_RFDLOCTX" ;
      AV320AcctS[1-1][15-1] = "P_RFDOTTAX" ;
      AV320AcctS[2-1][15-1] = "A_RFDOTTAX" ;
      AV320AcctS[1-1][16-1] = "P_RFDCOIVA" ;
      AV320AcctS[2-1][16-1] = "A_RFDCOIVA" ;
      AV320AcctS[1-1][17-1] = "P_ADMGROSS" ;
      AV320AcctS[2-1][17-1] = "A_ADMGROSS" ;
      AV320AcctS[1-1][18-1] = "P_ADMIVA" ;
      AV320AcctS[2-1][18-1] = "A_ADMIVA" ;
      AV320AcctS[1-1][19-1] = "P_ADMCOMMT" ;
      AV320AcctS[2-1][19-1] = "A_ADMCOMMT" ;
      AV320AcctS[1-1][20-1] = "P_ADMNET" ;
      AV320AcctS[2-1][20-1] = "A_ADMNET" ;
      AV320AcctS[1-1][21-1] = "P_ADMCREDT" ;
      AV320AcctS[2-1][21-1] = "A_ADMCREDT" ;
      AV320AcctS[1-1][22-1] = "P_ADMLOCTX" ;
      AV320AcctS[2-1][22-1] = "A_ADMLOCTX" ;
      AV320AcctS[1-1][23-1] = "P_ADMOTTAX" ;
      AV320AcctS[2-1][23-1] = "A_ADMOTTAX" ;
      AV320AcctS[1-1][24-1] = "P_ADMCOIVA" ;
      AV320AcctS[2-1][24-1] = "A_ADMCOIVA" ;
      AV320AcctS[1-1][25-1] = "P_ACMGROSS" ;
      AV320AcctS[2-1][25-1] = "A_ACMGROSS" ;
      AV320AcctS[1-1][26-1] = "P_ACMIVA" ;
      AV320AcctS[2-1][26-1] = "A_ACMIVA" ;
      AV320AcctS[1-1][27-1] = "P_ACMCOMMT" ;
      AV320AcctS[2-1][27-1] = "A_ACMCOMMT" ;
      AV320AcctS[1-1][28-1] = "P_ACMNET" ;
      AV320AcctS[2-1][28-1] = "A_ACMNET" ;
      AV320AcctS[1-1][29-1] = "P_ACMCREDT" ;
      AV320AcctS[2-1][29-1] = "A_ACMCREDT" ;
      AV320AcctS[1-1][30-1] = "P_ACMLOCTX" ;
      AV320AcctS[2-1][30-1] = "A_ACMLOCTX" ;
      AV320AcctS[1-1][31-1] = "P_ACMOTTAX" ;
      AV320AcctS[2-1][31-1] = "A_ACMOTTAX" ;
      AV320AcctS[1-1][32-1] = "P_ACMCOIVA" ;
      AV320AcctS[2-1][32-1] = "A_ACMCOIVA" ;
      AV320AcctS[1-1][33-1] = "P_RFDLTXCN" ;
      AV320AcctS[2-1][33-1] = "A_RFDLTXCN" ;
      AV320AcctS[1-1][34-1] = "P_RFDOTXCN" ;
      AV320AcctS[2-1][34-1] = "A_RFDOTXCN" ;
      AV320AcctS[1-1][35-1] = "P_USCCRED" ;
      AV320AcctS[2-1][35-1] = "A_USCCRED" ;
      AV320AcctS[1-1][36-1] = "P_OLLGROSS" ;
      AV320AcctS[2-1][36-1] = "A_OLLGROSS" ;
      AV320AcctS[1-1][37-1] = "P_OLLTADS" ;
      AV320AcctS[2-1][37-1] = "A_OLLTADS" ;
      AV320AcctS[1-1][38-1] = "P_OLLGR" ;
      AV320AcctS[2-1][38-1] = "A_OLLGR" ;
      AV320AcctS[1-1][39-1] = "P_OLLDRATE" ;
      AV320AcctS[2-1][39-1] = "A_OLLDRATE" ;
      AV320AcctS[1-1][40-1] = "P_OLLCRATE" ;
      AV320AcctS[2-1][40-1] = "A_OLLCRATE" ;
      AV320AcctS[1-1][41-1] = "P_OLLCOMMB" ;
      AV320AcctS[2-1][41-1] = "A_OLLCOMMB" ;
      AV320AcctS[1-1][42-1] = "P_OLLCOMMO" ;
      AV320AcctS[2-1][42-1] = "A_OLLCOMMO" ;
      AV320AcctS[1-1][43-1] = "P_OLLCOMMD" ;
      AV320AcctS[2-1][43-1] = "A_OLLCOMMD" ;
      AV320AcctS[1-1][44-1] = "P_ADMGRNET" ;
      AV320AcctS[2-1][44-1] = "A_ADMGRNET" ;
      AV320AcctS[1-1][45-1] = "P_OLLTADG" ;
      AV320AcctS[2-1][45-1] = "A_OLLTADG" ;
      AV320AcctS[1-1][46-1] = "P_OLLDRAG" ;
      AV320AcctS[2-1][46-1] = "A_OLLDRAG" ;
      AV320AcctS[1-1][47-1] = "P_OLLCRAG" ;
      AV320AcctS[2-1][47-1] = "A_OLLCRAG" ;
      AV320AcctS[1-1][48-1] = "P_ADMGRG" ;
      AV320AcctS[2-1][48-1] = "A_ADMGRG" ;
      AV320AcctS[1-1][49-1] = "P_ADMGRCOM" ;
      AV320AcctS[2-1][49-1] = "A_ADMGRCOM" ;
      AV320AcctS[1-1][50-1] = "P_RFDPENCA" ;
      AV320AcctS[2-1][50-1] = "A_RFDPENCA" ;
      AV320AcctS[1-1][51-1] = "P_RFDPENCC" ;
      AV320AcctS[2-1][51-1] = "A_RFDPENCC" ;
      AV320AcctS[1-1][52-1] = "P_ADM2GROS" ;
      AV320AcctS[2-1][52-1] = "A_ADM2GROS" ;
      AV320AcctS[1-1][53-1] = "P_ADM2NET" ;
      AV320AcctS[2-1][53-1] = "A_ADM2NET" ;
      AV320AcctS[1-1][54-1] = "P_ADM2COMM" ;
      AV320AcctS[2-1][54-1] = "A_ADM2COMM" ;
      AV320AcctS[1-1][55-1] = "P_ADM2CIVA" ;
      AV320AcctS[2-1][55-1] = "A_ADM2CIVA" ;
      AV320AcctS[1-1][56-1] = "P_ADM2LTAX" ;
      AV320AcctS[2-1][56-1] = "A_ADM2LTAX" ;
      AV320AcctS[1-1][57-1] = "P_ADM2OTAX" ;
      AV320AcctS[2-1][57-1] = "A_ADM2OTAX" ;
      AV320AcctS[1-1][58-1] = "P_ADMLTAX" ;
      AV320AcctS[2-1][58-1] = "A_ADMLTAX" ;
      AV320AcctS[1-1][59-1] = "P_ADMOTAX" ;
      AV320AcctS[2-1][59-1] = "A_ADMOTAX" ;
      AV320AcctS[1-1][60-1] = "P_ALLNET" ;
      AV320AcctS[2-1][60-1] = "A_ALLNET" ;
      AV320AcctS[1-1][61-1] = "P_ACM2NET" ;
      AV320AcctS[2-1][61-1] = "A_ACM2NET" ;
      AV320AcctS[1-1][62-1] = "P_ACM2GROS" ;
      AV320AcctS[2-1][62-1] = "A_ACM2GROS" ;
      AV320AcctS[1-1][63-1] = "P_ACM2COMM" ;
      AV320AcctS[2-1][63-1] = "A_ACM2COMM" ;
      AV320AcctS[1-1][64-1] = "P_ACM2CIVA" ;
      AV320AcctS[2-1][64-1] = "A_ACM2CIVA" ;
      AV320AcctS[1-1][65-1] = "P_ACM2LTAX" ;
      AV320AcctS[2-1][65-1] = "A_ACM2LTAX" ;
      AV320AcctS[1-1][66-1] = "P_ACM2OTAX" ;
      AV320AcctS[2-1][66-1] = "A_ACM2OTAX" ;
      AV320AcctS[1-1][67-1] = "P_ACMLTAX" ;
      AV320AcctS[2-1][67-1] = "A_ACMLTAX" ;
      AV320AcctS[1-1][68-1] = "P_ACMOTAX" ;
      AV320AcctS[2-1][68-1] = "A_ACMOTAX" ;
      AV320AcctS[1-1][69-1] = "P_ACM2IVA" ;
      AV320AcctS[2-1][69-1] = "A_ACM2IVA" ;
      AV320AcctS[1-1][70-1] = "P_ADM2IVA" ;
      AV320AcctS[2-1][70-1] = "A_ADM2IVA" ;
      AV320AcctS[1-1][71-1] = "P_OLLTADD" ;
      AV320AcctS[2-1][71-1] = "A_OLLTADD" ;
      AV320AcctS[1-1][72-1] = "P_OLLTADC" ;
      AV320AcctS[2-1][72-1] = "A_OLLTADC" ;
      AV320AcctS[1-1][73-1] = "P_ACMNETT" ;
      AV320AcctS[2-1][73-1] = "A_ACMNETT" ;
      AV320AcctS[1-1][74-1] = "P_ACMNETL" ;
      AV320AcctS[2-1][74-1] = "A_ACMNETL" ;
      AV320AcctS[1-1][75-1] = "P_IVACOMM" ;
      AV320AcctS[2-1][75-1] = "A_IVACOMM" ;
      AV320AcctS[1-1][76-1] = "P_REIVACOMM" ;
      AV320AcctS[2-1][76-1] = "A_REIVACOMM" ;
      AV320AcctS[1-1][77-1] = "P_REFUENTE" ;
      AV320AcctS[2-1][77-1] = "A_REFUENTE" ;
      AV320AcctS[1-1][78-1] = "P_REICACOMM" ;
      AV320AcctS[2-1][78-1] = "A_REICACOMM" ;
      AV320AcctS[1-1][79-1] = "P_SPCRGROS" ;
      AV320AcctS[2-1][79-1] = "A_SPCRGROS" ;
      AV320AcctS[1-1][80-1] = "P_SPCROSER" ;
      AV320AcctS[2-1][80-1] = "A_SPCROSER" ;
      AV320AcctS[1-1][81-1] = "P_SPCRIVA" ;
      AV320AcctS[2-1][81-1] = "A_SPCRIVA" ;
      AV320AcctS[1-1][82-1] = "P_SPCRRET" ;
      AV320AcctS[2-1][82-1] = "A_SPCRRET" ;
      AV320AcctS[1-1][83-1] = "P_SPDRGROS" ;
      AV320AcctS[2-1][83-1] = "A_SPDRGROS" ;
      AV320AcctS[1-1][84-1] = "P_SPDROSER" ;
      AV320AcctS[2-1][84-1] = "A_SPDROSER" ;
      AV320AcctS[1-1][85-1] = "P_SPDRIVA" ;
      AV320AcctS[2-1][85-1] = "A_SPDRIVA" ;
      AV320AcctS[1-1][86-1] = "P_SPDRRET" ;
      AV320AcctS[2-1][86-1] = "A_SPDRRET" ;
      AV193J = (short)(87) ;
      AV312Tax = "" ;
      AV311AccTa = GXutil.trim( AV311AccTa) + "," ;
      GX_I = 1 ;
      while ( ( GX_I <= 11 ) )
      {
         AV314TaxPo[GX_I-1] = (byte)(0) ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 11 ) )
      {
         AV315TaxCo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV317Cmpny = (short)(0) ;
      AV313G = 1 ;
      while ( ( AV313G <= GXutil.len( AV311AccTa) ) )
      {
         if ( ( AV317Cmpny <= 10 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV311AccTa, (int)(AV313G), 1), ",") != 0 ) )
            {
               if ( ( GXutil.len( AV312Tax) <= 1 ) )
               {
                  AV312Tax = AV312Tax + GXutil.substring( AV311AccTa, (int)(AV313G), 1) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(AV312Tax, "") != 0 ) )
               {
                  AV317Cmpny = (short)(AV317Cmpny+1) ;
                  AV315TaxCo[AV317Cmpny-1] = AV312Tax ;
                  AV314TaxPo[AV317Cmpny-1] = (byte)(AV193J) ;
                  AV320AcctS[1-1][AV193J-1] = "P_TAXCA_" + AV312Tax ;
                  AV320AcctS[2-1][AV193J-1] = "A_TAXCA_" + AV312Tax ;
                  AV193J = (short)(AV193J+1) ;
                  AV320AcctS[1-1][AV193J-1] = "P_TAXCC_" + AV312Tax ;
                  AV320AcctS[2-1][AV193J-1] = "A_TAXCC_" + AV312Tax ;
                  AV193J = (short)(AV193J+1) ;
                  AV320AcctS[1-1][AV193J-1] = "P_TXTOT_" + AV312Tax ;
                  AV320AcctS[2-1][AV193J-1] = "A_TXTOT_" + AV312Tax ;
                  AV193J = (short)(AV193J+1) ;
                  AV312Tax = "" ;
               }
            }
         }
         else
         {
            if (true) break;
         }
         AV313G = (long)(AV313G+1) ;
      }
      AV314TaxPo[11-1] = (byte)(AV193J) ;
      AV320AcctS[1-1][AV193J-1] = "P_TAXCA_99" ;
      AV320AcctS[2-1][AV193J-1] = "A_TAXCA_99" ;
      AV193J = (short)(AV193J+1) ;
      AV320AcctS[1-1][AV193J-1] = "P_TAXCC_99" ;
      AV320AcctS[2-1][AV193J-1] = "A_TAXCC_99" ;
      AV193J = (short)(AV193J+1) ;
      AV320AcctS[1-1][AV193J-1] = "P_TXTOT_99" ;
      AV320AcctS[2-1][AV193J-1] = "A_TXTOT_99" ;
      AV219QtdSc = (byte)(10) ;
      AV220QtdSc = (byte)(AV193J) ;
      AV192I = (byte)(3) ;
      while ( ( AV192I <= AV219QtdSc ) )
      {
         AV320AcctS[AV192I-1][1-1] = "C_GROSSAMO" ;
         AV320AcctS[AV192I-1][2-1] = "C_USCGROSS" ;
         AV192I = (byte)(AV192I+1) ;
      }
      AV192I = (byte)(1) ;
      while ( ( AV192I <= AV219QtdSc ) )
      {
         AV193J = (short)(1) ;
         while ( ( AV193J <= AV220QtdSc ) )
         {
            AV196LevEv[AV192I-1][AV193J-1] = 0 ;
            AV199LevEv[AV192I-1][AV193J-1] = 0 ;
            AV193J = (short)(AV193J+1) ;
         }
         AV192I = (byte)(AV192I+1) ;
      }
      AV23Achou = "N" ;
      AV8Linha = "" ;
      AV92Primei = "S" ;
      AV94Cont = 0 ;
      AV95Cont2 = 0 ;
      AV96Cont3 = 0 ;
      AV97Con4 = 0 ;
      AV101ValNu = 0 ;
      AV127Passo = "N" ;
      AV131SeqIt = 1 ;
      AV132SeqPg = 1 ;
      AV172PagSe = 1 ;
      AV104TLinA = " " ;
      AV150TotCa = 0 ;
      AV151TotCC = 0 ;
      AV153ContP = (short)(1) ;
      AV186Linha = "" ;
      AV215Carta = "" ;
      AV270msg1 = "" ;
      AV184TaxIn = (byte)(1) ;
      /* Execute user subroutine: S421838 */
      S421838 ();
      if ( returnInSub )
      {
      }
      cleanup();
   }

   public void S11341( )
   {
      /* 'FINAL' Routine */
      if ( ( GXutil.strcmp(AV281Modo, "A") != 0 ) )
      {
         context.msgStatus( "Terminado !!!!" );
      }
   }

   public void S12347( )
   {
      /* 'VERPROCESSADOS' Routine */
      AV155CProc = (short)(1) ;
      while ( ( AV155CProc <= AV153ContP ) )
      {
         AV155CProc = (short)(AV155CProc+1) ;
      }
   }

   public void S13354( )
   {
      /* 'CONVAL' Routine */
      /* Execute user subroutine: S141 */
      S141 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV64Valcon = 0 ;
      AV65Val2 = "0" ;
      AV66Sinal = (byte)(1) ;
      if ( ( GXutil.strcmp(AV63Simbol, "{") == 0 ) )
      {
         AV65Val2 = "0" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "A") == 0 ) )
      {
         AV65Val2 = "1" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "B") == 0 ) )
      {
         AV65Val2 = "2" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "C") == 0 ) )
      {
         AV65Val2 = "3" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "D") == 0 ) )
      {
         AV65Val2 = "4" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "E") == 0 ) )
      {
         AV65Val2 = "5" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "F") == 0 ) )
      {
         AV65Val2 = "6" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "G") == 0 ) )
      {
         AV65Val2 = "7" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "H") == 0 ) )
      {
         AV65Val2 = "8" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "I") == 0 ) )
      {
         AV65Val2 = "9" ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "}") == 0 ) )
      {
         AV65Val2 = "0" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "J") == 0 ) )
      {
         AV65Val2 = "1" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "K") == 0 ) )
      {
         AV65Val2 = "2" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "L") == 0 ) )
      {
         AV65Val2 = "3" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "M") == 0 ) )
      {
         AV65Val2 = "4" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "N") == 0 ) )
      {
         AV65Val2 = "5" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "O") == 0 ) )
      {
         AV65Val2 = "6" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "P") == 0 ) )
      {
         AV65Val2 = "7" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "Q") == 0 ) )
      {
         AV65Val2 = "8" ;
         AV66Sinal = (byte)(-1) ;
      }
      else if ( ( GXutil.strcmp(AV63Simbol, "R") == 0 ) )
      {
         AV65Val2 = "9" ;
         AV66Sinal = (byte)(-1) ;
      }
      AV68Valx = GXutil.concat( AV62Val1, AV65Val2, "") ;
      if ( ( AV212Divis > 0 ) )
      {
         AV69ValxDe = (double)((GXutil.val( AV68Valx, ".")*AV66Sinal)/ (double) (AV212Divis)) ;
      }
      else
      {
         AV69ValxDe = (double)(((GXutil.val( AV68Valx, ".")*AV66Sinal)/ (double) (100))) ;
      }
   }

   public void S15423( )
   {
      /* 'GRAVAITI' Routine */
      AV149Cit = 1 ;
      if ( ( AV131SeqIt <= 1 ) )
      {
         AV159ContU = 1 ;
      }
      else
      {
         AV159ContU = (int)(AV131SeqIt-1) ;
      }
      AV158Ultim = AV156Tipo_[AV159ContU-1] ;
      while ( ( AV149Cit < AV131SeqIt ) )
      {
         /*
            INSERT RECORD ON TABLE HOT1

         */
         A963ISOC = AV373ISOC ;
         A965PER_NA = GXutil.substring( AV135PerMT[AV149Cit-1], 1, 10) ;
         A966CODE = GXutil.trim( AV246Trans) ;
         A967IATA = AV136IataM[AV149Cit-1] ;
         if ( ( GXutil.strcmp(GXutil.substring( AV148Num_M[AV149Cit-1], 11, 4), "/ZUM") == 0 ) )
         {
            A968NUM_BI = GXutil.substring( AV148Num_M[AV149Cit-1], 1, 14) ;
         }
         else
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 11, 4), "/ZUM") == 0 ) )
            {
               if ( ( GXutil.strcmp(AV288ZumOn, "Y") == 0 ) )
               {
                  AV231TempB = (AV148Num_M[AV149Cit-1] + "/ZUM") ;
               }
               A968NUM_BI = GXutil.substring( AV231TempB, 1, 14) ;
            }
            else
            {
               AV231TempB = GXutil.substring( AV148Num_M[AV149Cit-1], 1, 14) ;
               A968NUM_BI = GXutil.substring( AV231TempB, 1, 14) ;
            }
         }
         A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
         A970DATA = AV157DataM[AV149Cit-1] ;
         A971ISeq = AV139ISeqM[AV149Cit-1] ;
         A922Num_Bi = AV140NumBP[AV149Cit-1] ;
         n922Num_Bi = false ;
         A923I_From = GXutil.substring( AV141I_Fro[AV149Cit-1], 1, 20) ;
         n923I_From = false ;
         A924I_To = GXutil.substring( AV142I_ToM[AV149Cit-1], 1, 30) ;
         n924I_To = false ;
         A925Carrie = GXutil.substring( AV143Carri[AV149Cit-1], 1, 30) ;
         n925Carrie = false ;
         A926Tariff = GXutil.substring( AV144Tarri[AV149Cit-1], 1, 30) ;
         n926Tariff = false ;
         A928Claxss = GXutil.substring( AV145Class[AV149Cit-1], 1, 2) ;
         n928Claxss = false ;
         A927Flight = GXutil.substring( AV146Fligh[AV149Cit-1], 1, 15) ;
         n927Flight = false ;
         A929Fli_Da = GXutil.substring( AV147Fli_M[AV149Cit-1], 1, 5) ;
         n929Fli_Da = false ;
         A930Fli_Da = AV271Fli_M[AV149Cit-1] ;
         n930Fli_Da = false ;
         A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
         /* Using cursor P005F2 */
         pr_default.execute(0, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Integer(A971ISeq), new Boolean(n922Num_Bi), new Long(A922Num_Bi), new Boolean(n923I_From), A923I_From, new Boolean(n924I_To), A924I_To, new Boolean(n925Carrie), A925Carrie, new Boolean(n926Tariff), A926Tariff, new Boolean(n927Flight), A927Flight, new Boolean(n928Claxss), A928Claxss, new Boolean(n929Fli_Da), A929Fli_Da, new Boolean(n930Fli_Da), A930Fli_Da});
         if ( (pr_default.getStatus(0) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         AV149Cit = (int)(AV149Cit+1) ;
      }
   }

   public void S16478( )
   {
      /* 'GRAVAPAG' Routine */
      AV160CPag = 1 ;
      while ( ( AV160CPag < AV172PagSe ) )
      {
         /*
            INSERT RECORD ON TABLE HOT2

         */
         A963ISOC = AV373ISOC ;
         A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
         A965PER_NA = GXutil.substring( AV161Per_P[AV160CPag-1], 1, 10) ;
         A966CODE = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
         A967IATA = GXutil.substring( AV163IataP[AV160CPag-1], 1, 15) ;
         A968NUM_BI = GXutil.substring( AV164NumPa[AV160CPag-1], 1, 14) ;
         A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
         A970DATA = AV165DataP[AV160CPag-1] ;
         A972SeqPag = AV166SeqMt[AV160CPag-1] ;
         A936PGTIP_ = GXutil.substring( AV173PgTip[AV160CPag-1], 1, 2) ;
         n936PGTIP_ = false ;
         A933PGCC_T = GXutil.substring( AV168PgCCT[AV160CPag-1], 1, 2) ;
         n933PGCC_T = false ;
         A934PGCCCF = GXutil.substring( AV169Pgccc[AV160CPag-1], 1, 30) ;
         n934PGCCCF = false ;
         if ( ( GXutil.strcmp(AV310ENCCC, "N") == 0 ) )
         {
            A937PGINFO = GXutil.substring( AV170PgInf[AV160CPag-1], 1, 32) ;
            n937PGINFO = false ;
            A1073PGCCN = "" ;
            n1073PGCCN = false ;
            A1074PGCCM = "" ;
            n1074PGCCM = false ;
         }
         else
         {
            AV309PGInf = AV170PgInf[AV160CPag-1] ;
            GXv_char3[0] = AV309PGInf ;
            GXv_char2[0] = AV307PGCCN ;
            GXv_char4[0] = AV308PGCCM ;
            new pcartaocripmask(remoteHandle, context).execute( GXv_char3, GXv_char2, GXv_char4) ;
            ppeginf.this.AV309PGInf = GXv_char3[0] ;
            ppeginf.this.AV307PGCCN = GXv_char2[0] ;
            ppeginf.this.AV308PGCCM = GXv_char4[0] ;
            A937PGINFO = GXutil.substring( AV309PGInf, 1, 32) ;
            n937PGINFO = false ;
            A1073PGCCN = GXutil.substring( AV307PGCCN, 1, 32) ;
            n1073PGCCN = false ;
            A1074PGCCM = AV308PGCCM ;
            n1074PGCCM = false ;
         }
         A935PGAMOU = AV171PgAmo[AV160CPag-1] ;
         n935PGAMOU = false ;
         A939PGMOED = GXutil.substring( AV206PgMoe, 1, 3) ;
         n939PGMOED = false ;
         A941EXDA = GXutil.substring( AV304EXDA, 1, 4) ;
         n941EXDA = false ;
         A940APLC = GXutil.substring( AV305APLC, 1, 6) ;
         n940APLC = false ;
         /* Using cursor P005F3 */
         pr_default.execute(1, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Integer(A972SeqPag), new Boolean(n933PGCC_T), A933PGCC_T, new Boolean(n934PGCCCF), A934PGCCCF, new Boolean(n935PGAMOU), new Double(A935PGAMOU), new Boolean(n936PGTIP_), A936PGTIP_, new Boolean(n937PGINFO), A937PGINFO, new Boolean(n939PGMOED), A939PGMOED, new Boolean(n940APLC), A940APLC, new Boolean(n941EXDA), A941EXDA, new Boolean(n1073PGCCN), A1073PGCCN, new Boolean(n1074PGCCM), A1074PGCCM});
         if ( (pr_default.getStatus(1) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         AV160CPag = (int)(AV160CPag+1) ;
      }
   }

   public void S17527( )
   {
      /* 'GRAVATAX' Routine */
      AV184TaxIn = (byte)(1) ;
      while ( ( GXutil.strcmp(AV183Tax_C[AV184TaxIn-1], "") != 0 ) )
      {
         /* Execute user subroutine: S181 */
         S181 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV319Found = "" ;
         AV316Cmpny = (short)(1) ;
         while ( ( AV316Cmpny <= AV317Cmpny ) )
         {
            if ( ( GXutil.strcmp(AV315TaxCo[AV316Cmpny-1], AV183Tax_C[AV184TaxIn-1]) == 0 ) )
            {
               /* Execute user subroutine: S191 */
               S191 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               AV319Found = "S" ;
            }
            AV316Cmpny = (short)(AV316Cmpny+1) ;
         }
         if ( ( GXutil.strcmp(AV319Found, "S") != 0 ) )
         {
            AV318Level = AV314TaxPo[11-1] ;
            if ( ( GXutil.strcmp(AV79Pay_ty, "CA") == 0 ) )
            {
               if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
               {
                  AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV318Level = (short)(AV318Level+2) ;
                  AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               }
               else
               {
                  AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV318Level = (short)(AV318Level+2) ;
                  AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               }
            }
            else
            {
               AV318Level = AV314TaxPo[11-1] ;
               if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
               {
                  AV318Level = (short)(AV318Level+1) ;
                  AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV318Level = (short)(AV318Level+1) ;
                  AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               }
               else
               {
                  AV318Level = (short)(AV318Level+1) ;
                  AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV318Level = (short)(AV318Level+1) ;
                  AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               }
            }
         }
         AV184TaxIn = (byte)(AV184TaxIn+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV185Tax_a[GX_I-1] = 0 ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 500 ) )
      {
         AV183Tax_C[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV184TaxIn = (byte)(1) ;
   }

   public void S191( )
   {
      /* 'ACUTAXTIPO' Routine */
      AV318Level = AV314TaxPo[AV316Cmpny-1] ;
      if ( ( GXutil.strcmp(AV79Pay_ty, "CA") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV318Level = (short)(AV318Level+2) ;
            AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
         else
         {
            AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV318Level = (short)(AV318Level+2) ;
            AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
      }
      else
      {
         AV318Level = AV314TaxPo[AV316Cmpny-1] ;
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV318Level = (short)(AV318Level+1) ;
            AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV318Level = (short)(AV318Level+1) ;
            AV196LevEv[1-1][AV318Level-1] = (double)(AV196LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV196LevEv[2-1][AV318Level-1] = (double)(AV196LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
         else
         {
            AV318Level = (short)(AV318Level+1) ;
            AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV318Level = (short)(AV318Level+1) ;
            AV199LevEv[1-1][AV318Level-1] = (double)(AV199LevEv[1-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV199LevEv[2-1][AV318Level-1] = (double)(AV199LevEv[2-1][AV318Level-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
      }
   }

   public void S20625( )
   {
      /* 'ACUSALEVE' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         AV196LevEv[1-1][60-1] = (double)(AV196LevEv[1-1][60-1]+AV42Due_Ag) ;
      }
      else
      {
         AV199LevEv[1-1][60-1] = (double)(AV199LevEv[1-1][60-1]+AV42Due_Ag) ;
      }
      if ( ( ( GXutil.len( GXutil.trim( AV40Tkt)) < 9 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV226ADM_p) != 0 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV227ACM_p) != 0 ) && ( GXutil.strcmp(AV238Trans, "D") != 0 ) ) || ( GXutil.strcmp(AV238Trans, "R") == 0 ) )
      {
         /* Execute user subroutine: S211 */
         S211 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( ( GXutil.len( GXutil.trim( AV40Tkt)) < 9 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV226ADM_p) == 0 ) ) || ( GXutil.strcmp(AV238Trans, "D") == 0 ) )
      {
         /* Execute user subroutine: S221 */
         S221 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( ( GXutil.len( GXutil.trim( AV40Tkt)) < 10 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV227ACM_p) == 0 ) ) || ( GXutil.strcmp(AV238Trans, "C") == 0 ) )
      {
         /* Execute user subroutine: S231 */
         S231 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( GXutil.strcmp(AV246Trans, "SPCR") == 0 ) )
      {
         /* Execute user subroutine: S241 */
         S241 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( GXutil.strcmp(AV246Trans, "SPDR") == 0 ) )
      {
         /* Execute user subroutine: S251 */
         S251 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         /* Execute user subroutine: S261 */
         S261 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S211( )
   {
      /* 'ACURFD' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         AV196LevEv[1-1][9-1] = (double)(AV196LevEv[1-1][9-1]+AV41Tkt_Am) ;
         AV196LevEv[2-1][9-1] = (double)(AV196LevEv[2-1][9-1]+AV41Tkt_Am) ;
         AV196LevEv[1-1][10-1] = (double)(AV196LevEv[1-1][10-1]+AV211IVAAM) ;
         AV196LevEv[2-1][10-1] = (double)(AV196LevEv[2-1][10-1]+AV211IVAAM) ;
         AV196LevEv[1-1][11-1] = (double)(AV196LevEv[1-1][11-1]+(AV76Lit_co+AV77Over_c)) ;
         AV196LevEv[2-1][11-1] = (double)(AV196LevEv[2-1][11-1]+(AV76Lit_co+AV77Over_c)) ;
         AV196LevEv[1-1][12-1] = (double)(AV196LevEv[1-1][12-1]+AV42Due_Ag) ;
         AV196LevEv[2-1][12-1] = (double)(AV196LevEv[2-1][12-1]+AV42Due_Ag) ;
         if ( ( AV108CC_Am != 0 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV79Pay_ty, 1, 2), "CA") == 0 ) )
            {
               if ( ( GXutil.strcmp(AV365RFND2, "CA") == 0 ) )
               {
                  AV196LevEv[1-1][13-1] = (double)(AV196LevEv[1-1][13-1]+AV108CC_Am) ;
                  AV196LevEv[2-1][13-1] = (double)(AV196LevEv[2-1][13-1]+AV108CC_Am) ;
               }
               else
               {
                  AV196LevEv[1-1][13-1] = (double)(AV196LevEv[1-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
                  AV196LevEv[2-1][13-1] = (double)(AV196LevEv[2-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
               }
            }
            else
            {
               AV196LevEv[1-1][13-1] = (double)(AV196LevEv[1-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
               AV196LevEv[2-1][13-1] = (double)(AV196LevEv[2-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
            }
         }
         AV196LevEv[1-1][16-1] = (double)(AV196LevEv[1-1][16-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         AV196LevEv[2-1][16-1] = (double)(AV196LevEv[2-1][16-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
      }
      else
      {
         AV199LevEv[1-1][9-1] = (double)(AV199LevEv[1-1][9-1]+AV41Tkt_Am) ;
         AV199LevEv[2-1][9-1] = (double)(AV199LevEv[2-1][9-1]+AV41Tkt_Am) ;
         AV199LevEv[1-1][10-1] = (double)(AV199LevEv[1-1][10-1]+AV211IVAAM) ;
         AV199LevEv[2-1][10-1] = (double)(AV199LevEv[2-1][10-1]+AV211IVAAM) ;
         AV199LevEv[1-1][11-1] = (double)(AV199LevEv[1-1][11-1]+(AV76Lit_co+AV77Over_c)) ;
         AV199LevEv[2-1][11-1] = (double)(AV199LevEv[2-1][11-1]+(AV76Lit_co+AV77Over_c)) ;
         AV199LevEv[1-1][12-1] = (double)(AV199LevEv[1-1][12-1]+AV42Due_Ag) ;
         AV199LevEv[2-1][12-1] = (double)(AV199LevEv[2-1][12-1]+AV42Due_Ag) ;
         if ( ( AV108CC_Am != 0 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV79Pay_ty, 1, 2), "CA") == 0 ) )
            {
               if ( ( GXutil.strcmp(AV365RFND2, "CA") == 0 ) )
               {
                  AV199LevEv[1-1][13-1] = (double)(AV199LevEv[1-1][13-1]+AV108CC_Am) ;
                  AV199LevEv[2-1][13-1] = (double)(AV199LevEv[2-1][13-1]+AV108CC_Am) ;
               }
               else
               {
                  AV199LevEv[1-1][13-1] = (double)(AV199LevEv[1-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
                  AV199LevEv[2-1][13-1] = (double)(AV199LevEv[2-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
               }
            }
            else
            {
               AV199LevEv[1-1][13-1] = (double)(AV199LevEv[1-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
               AV199LevEv[2-1][13-1] = (double)(AV199LevEv[2-1][13-1]+(AV108CC_Am-AV38Tax_Cp)) ;
            }
         }
         AV199LevEv[1-1][16-1] = (double)(AV199LevEv[1-1][16-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         AV199LevEv[2-1][16-1] = (double)(AV199LevEv[2-1][16-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
      }
   }

   public void S221( )
   {
      /* 'ACUADM' Routine */
      /* Execute user subroutine: S271 */
      S271 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV225BSPIa), "") != 0 ) && ( GXutil.strcmp(GXutil.trim( AV225BSPIa), GXutil.trim( AV22iataC)) == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][39-1] = (double)(AV196LevEv[1-1][39-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][39-1] = (double)(AV196LevEv[2-1][39-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][46-1] = (double)(AV196LevEv[1-1][46-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][46-1] = (double)(AV196LevEv[2-1][46-1]+AV42Due_Ag) ;
         }
         else
         {
            AV199LevEv[1-1][39-1] = (double)(AV199LevEv[1-1][39-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][39-1] = (double)(AV199LevEv[2-1][39-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][46-1] = (double)(AV199LevEv[1-1][46-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][46-1] = (double)(AV199LevEv[2-1][46-1]+AV42Due_Ag) ;
         }
      }
      else if ( ( GXutil.strcmp(AV252TipoM, "T") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][37-1] = (double)(AV196LevEv[1-1][37-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][37-1] = (double)(AV196LevEv[2-1][37-1]+AV42Due_Ag) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, AV295TADDL), AV296TADDN) == 0 ) )
            {
               AV196LevEv[1-1][71-1] = (double)(AV196LevEv[1-1][71-1]+AV42Due_Ag) ;
               AV196LevEv[2-1][71-1] = (double)(AV196LevEv[2-1][71-1]+AV42Due_Ag) ;
            }
            AV196LevEv[1-1][45-1] = (double)(AV196LevEv[1-1][45-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][45-1] = (double)(AV196LevEv[2-1][45-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][53-1] = (double)(AV196LevEv[1-1][53-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][53-1] = (double)(AV196LevEv[2-1][53-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][52-1] = (double)(AV196LevEv[1-1][52-1]+AV41Tkt_Am) ;
            AV196LevEv[2-1][52-1] = (double)(AV196LevEv[2-1][52-1]+AV41Tkt_Am) ;
            AV196LevEv[1-1][54-1] = (double)(AV196LevEv[1-1][54-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[2-1][54-1] = (double)(AV196LevEv[2-1][54-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[1-1][55-1] = (double)(AV196LevEv[1-1][55-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV196LevEv[2-1][55-1] = (double)(AV196LevEv[2-1][55-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV196LevEv[1-1][70-1] = (double)(AV196LevEv[1-1][70-1]+AV211IVAAM) ;
            AV196LevEv[2-1][70-1] = (double)(AV196LevEv[2-1][70-1]+AV211IVAAM) ;
         }
         else
         {
            AV199LevEv[1-1][37-1] = (double)(AV199LevEv[1-1][37-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][37-1] = (double)(AV199LevEv[2-1][37-1]+AV42Due_Ag) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, AV295TADDL), AV296TADDN) == 0 ) )
            {
               AV199LevEv[1-1][71-1] = (double)(AV199LevEv[1-1][71-1]+AV42Due_Ag) ;
               AV199LevEv[2-1][71-1] = (double)(AV199LevEv[2-1][71-1]+AV42Due_Ag) ;
            }
            AV199LevEv[1-1][45-1] = (double)(AV199LevEv[1-1][45-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][45-1] = (double)(AV199LevEv[2-1][45-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][53-1] = (double)(AV199LevEv[1-1][53-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][53-1] = (double)(AV199LevEv[2-1][53-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][52-1] = (double)(AV199LevEv[1-1][52-1]+AV41Tkt_Am) ;
            AV199LevEv[2-1][52-1] = (double)(AV199LevEv[2-1][52-1]+AV41Tkt_Am) ;
            AV199LevEv[1-1][54-1] = (double)(AV199LevEv[1-1][54-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[2-1][54-1] = (double)(AV199LevEv[2-1][54-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[1-1][55-1] = (double)(AV199LevEv[1-1][55-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV199LevEv[2-1][55-1] = (double)(AV199LevEv[2-1][55-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV199LevEv[1-1][70-1] = (double)(AV199LevEv[1-1][70-1]+AV211IVAAM) ;
            AV199LevEv[2-1][70-1] = (double)(AV199LevEv[2-1][70-1]+AV211IVAAM) ;
         }
      }
      else if ( ( ( AV248GRlen > 0 ) && ( GXutil.strcmp(GXutil.substring( AV247GRNum, 1, (int)(AV248GRlen)), GXutil.substring( AV40Tkt, 1, (int)(AV248GRlen))) == 0 ) ) || ( ( GXutil.strcmp(AV252TipoM, "G") == 0 ) ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][44-1] = (double)(AV196LevEv[1-1][44-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][44-1] = (double)(AV196LevEv[2-1][44-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][48-1] = (double)(AV196LevEv[1-1][48-1]+AV41Tkt_Am) ;
            AV196LevEv[2-1][48-1] = (double)(AV196LevEv[2-1][48-1]+AV41Tkt_Am) ;
            AV196LevEv[1-1][49-1] = (double)(AV196LevEv[1-1][49-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[2-1][49-1] = (double)(AV196LevEv[2-1][49-1]+(AV76Lit_co+AV77Over_c)) ;
         }
         else
         {
            AV199LevEv[1-1][44-1] = (double)(AV199LevEv[1-1][44-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][44-1] = (double)(AV199LevEv[2-1][44-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][48-1] = (double)(AV199LevEv[1-1][48-1]+AV41Tkt_Am) ;
            AV199LevEv[2-1][48-1] = (double)(AV199LevEv[2-1][48-1]+AV41Tkt_Am) ;
            AV199LevEv[1-1][49-1] = (double)(AV199LevEv[1-1][49-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[2-1][49-1] = (double)(AV199LevEv[2-1][49-1]+(AV76Lit_co+AV77Over_c)) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][17-1] = (double)(AV196LevEv[1-1][17-1]+AV41Tkt_Am) ;
            AV196LevEv[2-1][17-1] = (double)(AV196LevEv[2-1][17-1]+AV41Tkt_Am) ;
            AV196LevEv[1-1][18-1] = (double)(AV196LevEv[1-1][18-1]+AV211IVAAM) ;
            AV196LevEv[2-1][18-1] = (double)(AV196LevEv[2-1][18-1]+AV211IVAAM) ;
            AV196LevEv[1-1][19-1] = (double)(AV196LevEv[1-1][19-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[2-1][19-1] = (double)(AV196LevEv[2-1][19-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[1-1][20-1] = (double)(AV196LevEv[1-1][20-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][20-1] = (double)(AV196LevEv[2-1][20-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][21-1] = (double)(AV196LevEv[1-1][21-1]+AV108CC_Am) ;
            AV196LevEv[2-1][21-1] = (double)(AV196LevEv[2-1][21-1]+AV108CC_Am) ;
            AV196LevEv[1-1][24-1] = (double)(AV196LevEv[1-1][24-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV196LevEv[2-1][24-1] = (double)(AV196LevEv[2-1][24-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         }
         else
         {
            AV199LevEv[1-1][17-1] = (double)(AV199LevEv[1-1][17-1]+AV41Tkt_Am) ;
            AV199LevEv[2-1][17-1] = (double)(AV199LevEv[2-1][17-1]+AV41Tkt_Am) ;
            AV199LevEv[1-1][18-1] = (double)(AV199LevEv[1-1][18-1]+AV211IVAAM) ;
            AV199LevEv[2-1][18-1] = (double)(AV199LevEv[2-1][18-1]+AV211IVAAM) ;
            AV199LevEv[1-1][19-1] = (double)(AV199LevEv[1-1][19-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[2-1][19-1] = (double)(AV199LevEv[2-1][19-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[1-1][20-1] = (double)(AV199LevEv[1-1][20-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][20-1] = (double)(AV199LevEv[2-1][20-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][21-1] = (double)(AV199LevEv[1-1][21-1]+AV108CC_Am) ;
            AV199LevEv[2-1][21-1] = (double)(AV199LevEv[2-1][21-1]+AV108CC_Am) ;
            AV199LevEv[1-1][24-1] = (double)(AV199LevEv[1-1][24-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV199LevEv[2-1][24-1] = (double)(AV199LevEv[2-1][24-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         }
      }
   }

   public void S231( )
   {
      /* 'ACUACM' Routine */
      /* Execute user subroutine: S271 */
      S271 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV225BSPIa), "") != 0 ) && ( GXutil.strcmp(GXutil.trim( AV225BSPIa), GXutil.trim( AV22iataC)) == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][40-1] = (double)(AV196LevEv[1-1][40-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][40-1] = (double)(AV196LevEv[2-1][40-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][47-1] = (double)(AV196LevEv[1-1][47-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][47-1] = (double)(AV196LevEv[2-1][47-1]+AV42Due_Ag) ;
         }
         else
         {
            AV199LevEv[1-1][40-1] = (double)(AV199LevEv[1-1][40-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][40-1] = (double)(AV199LevEv[2-1][40-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][47-1] = (double)(AV199LevEv[1-1][47-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][47-1] = (double)(AV199LevEv[2-1][47-1]+AV42Due_Ag) ;
         }
      }
      else if ( ( GXutil.strcmp(AV252TipoM, "T") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, AV297TADCL), AV298TADCN) == 0 ) )
            {
               AV196LevEv[1-1][72-1] = (double)(AV196LevEv[1-1][72-1]+AV42Due_Ag) ;
               AV196LevEv[2-1][72-1] = (double)(AV196LevEv[2-1][72-1]+AV42Due_Ag) ;
            }
            AV196LevEv[1-1][61-1] = (double)(AV196LevEv[1-1][61-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][61-1] = (double)(AV196LevEv[2-1][61-1]+AV42Due_Ag) ;
            AV196LevEv[1-1][62-1] = (double)(AV196LevEv[1-1][62-1]+AV41Tkt_Am) ;
            AV196LevEv[2-1][62-1] = (double)(AV196LevEv[2-1][62-1]+AV41Tkt_Am) ;
            AV196LevEv[1-1][63-1] = (double)(AV196LevEv[1-1][63-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[2-1][63-1] = (double)(AV196LevEv[2-1][63-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[1-1][64-1] = (double)(AV196LevEv[1-1][64-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV196LevEv[2-1][64-1] = (double)(AV196LevEv[2-1][64-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV196LevEv[1-1][69-1] = (double)(AV196LevEv[1-1][69-1]+AV211IVAAM) ;
            AV196LevEv[2-1][69-1] = (double)(AV196LevEv[2-1][69-1]+AV211IVAAM) ;
         }
         else
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, AV297TADCL), AV298TADCN) == 0 ) )
            {
               AV199LevEv[1-1][72-1] = (double)(AV199LevEv[1-1][72-1]+AV42Due_Ag) ;
               AV199LevEv[2-1][72-1] = (double)(AV199LevEv[2-1][72-1]+AV42Due_Ag) ;
            }
            AV199LevEv[1-1][61-1] = (double)(AV199LevEv[1-1][61-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][61-1] = (double)(AV199LevEv[2-1][61-1]+AV42Due_Ag) ;
            AV199LevEv[1-1][62-1] = (double)(AV199LevEv[1-1][62-1]+AV41Tkt_Am) ;
            AV199LevEv[2-1][62-1] = (double)(AV199LevEv[2-1][62-1]+AV41Tkt_Am) ;
            AV199LevEv[1-1][63-1] = (double)(AV199LevEv[1-1][63-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[2-1][63-1] = (double)(AV199LevEv[2-1][63-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[1-1][64-1] = (double)(AV199LevEv[1-1][64-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV199LevEv[2-1][64-1] = (double)(AV199LevEv[2-1][64-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV199LevEv[1-1][70-1] = (double)(AV199LevEv[1-1][70-1]+AV211IVAAM) ;
            AV199LevEv[2-1][70-1] = (double)(AV199LevEv[2-1][70-1]+AV211IVAAM) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            AV196LevEv[1-1][25-1] = (double)(AV196LevEv[1-1][25-1]+AV41Tkt_Am) ;
            AV196LevEv[2-1][25-1] = (double)(AV196LevEv[2-1][25-1]+AV41Tkt_Am) ;
            AV196LevEv[1-1][26-1] = (double)(AV196LevEv[1-1][26-1]+AV211IVAAM) ;
            AV196LevEv[2-1][26-1] = (double)(AV196LevEv[2-1][26-1]+AV211IVAAM) ;
            AV196LevEv[1-1][27-1] = (double)(AV196LevEv[1-1][27-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[2-1][27-1] = (double)(AV196LevEv[2-1][27-1]+(AV76Lit_co+AV77Over_c)) ;
            AV196LevEv[1-1][28-1] = (double)(AV196LevEv[1-1][28-1]+AV42Due_Ag) ;
            AV196LevEv[2-1][28-1] = (double)(AV196LevEv[2-1][28-1]+AV42Due_Ag) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, AV297TADCL), AV298TADCN) == 0 ) )
            {
               AV196LevEv[1-1][73-1] = (double)(AV196LevEv[1-1][73-1]+AV42Due_Ag) ;
               AV196LevEv[2-1][73-1] = (double)(AV196LevEv[2-1][73-1]+AV42Due_Ag) ;
            }
            else
            {
               AV196LevEv[1-1][74-1] = (double)(AV196LevEv[1-1][74-1]+AV42Due_Ag) ;
               AV196LevEv[2-1][74-1] = (double)(AV196LevEv[2-1][74-1]+AV42Due_Ag) ;
            }
            AV196LevEv[1-1][29-1] = (double)(AV196LevEv[1-1][29-1]+AV108CC_Am) ;
            AV196LevEv[2-1][29-1] = (double)(AV196LevEv[2-1][29-1]+AV108CC_Am) ;
            AV196LevEv[1-1][32-1] = (double)(AV196LevEv[1-1][32-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV196LevEv[2-1][32-1] = (double)(AV196LevEv[2-1][32-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         }
         else
         {
            AV199LevEv[1-1][25-1] = (double)(AV199LevEv[1-1][25-1]+AV41Tkt_Am) ;
            AV199LevEv[2-1][25-1] = (double)(AV199LevEv[2-1][25-1]+AV41Tkt_Am) ;
            AV199LevEv[1-1][26-1] = (double)(AV199LevEv[1-1][26-1]+AV211IVAAM) ;
            AV199LevEv[2-1][26-1] = (double)(AV199LevEv[2-1][26-1]+AV211IVAAM) ;
            AV199LevEv[1-1][27-1] = (double)(AV199LevEv[1-1][27-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[2-1][27-1] = (double)(AV199LevEv[2-1][27-1]+(AV76Lit_co+AV77Over_c)) ;
            AV199LevEv[1-1][28-1] = (double)(AV199LevEv[1-1][28-1]+AV42Due_Ag) ;
            AV199LevEv[2-1][28-1] = (double)(AV199LevEv[2-1][28-1]+AV42Due_Ag) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, AV297TADCL), AV298TADCN) == 0 ) )
            {
               AV199LevEv[1-1][73-1] = (double)(AV199LevEv[1-1][73-1]+AV42Due_Ag) ;
               AV199LevEv[2-1][73-1] = (double)(AV199LevEv[2-1][73-1]+AV42Due_Ag) ;
            }
            else
            {
               AV199LevEv[1-1][74-1] = (double)(AV199LevEv[1-1][74-1]+AV42Due_Ag) ;
               AV199LevEv[2-1][74-1] = (double)(AV199LevEv[2-1][74-1]+AV42Due_Ag) ;
            }
            AV199LevEv[1-1][29-1] = (double)(AV199LevEv[1-1][29-1]+AV108CC_Am) ;
            AV199LevEv[2-1][29-1] = (double)(AV199LevEv[2-1][29-1]+AV108CC_Am) ;
            AV199LevEv[1-1][32-1] = (double)(AV199LevEv[1-1][32-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
            AV199LevEv[2-1][32-1] = (double)(AV199LevEv[2-1][32-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         }
      }
   }

   public void S261( )
   {
      /* 'ACUTKT' Routine */
      AV218C_Pos = (byte)(0) ;
      if ( ( GXutil.strcmp(AV215Carta, "") != 0 ) && ( AV108CC_Am > 0 ) )
      {
         AV216CC_No = "CREDIT CARD " + AV215Carta ;
         AV217c = (byte)(3) ;
         AV218C_Pos = (byte)(0) ;
         while ( ( AV217c <= ( AV219QtdSc - 1 ) ) )
         {
            if ( ( GXutil.strcmp(AV189Level[AV217c-1], "") == 0 ) )
            {
               AV189Level[AV217c-1] = AV216CC_No ;
               AV218C_Pos = AV217c ;
               AV217c = AV219QtdSc ;
            }
            else if ( ( GXutil.strcmp(AV189Level[AV217c-1], AV216CC_No) == 0 ) )
            {
               AV218C_Pos = AV217c ;
               AV217c = AV219QtdSc ;
            }
            else
            {
               AV217c = (byte)(AV217c+1) ;
               if ( ( AV217c == AV219QtdSc ) )
               {
                  AV282Error = "More than 8 CC found." ;
                  AV189Level[AV219QtdSc-1] = "CREDIT CARD XX" ;
                  AV218C_Pos = AV219QtdSc ;
               }
            }
         }
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         AV196LevEv[1-1][1-1] = (double)(AV196LevEv[1-1][1-1]+AV41Tkt_Am) ;
         AV196LevEv[2-1][1-1] = (double)(AV196LevEv[2-1][1-1]+AV41Tkt_Am) ;
         AV196LevEv[1-1][2-1] = (double)(AV196LevEv[1-1][2-1]+AV211IVAAM) ;
         AV196LevEv[2-1][2-1] = (double)(AV196LevEv[2-1][2-1]+AV211IVAAM) ;
         AV196LevEv[1-1][3-1] = (double)(AV196LevEv[1-1][3-1]+(AV76Lit_co+AV77Over_c)) ;
         AV196LevEv[2-1][3-1] = (double)(AV196LevEv[2-1][3-1]+(AV76Lit_co+AV77Over_c)) ;
         AV196LevEv[1-1][41-1] = (double)(AV196LevEv[1-1][41-1]+AV76Lit_co) ;
         AV196LevEv[2-1][41-1] = (double)(AV196LevEv[2-1][41-1]+AV76Lit_co) ;
         AV196LevEv[1-1][42-1] = (double)(AV196LevEv[1-1][42-1]+((AV77Over_c+AV115over_))) ;
         AV196LevEv[2-1][42-1] = (double)(AV196LevEv[2-1][42-1]+((AV77Over_c+AV115over_))) ;
         AV196LevEv[1-1][43-1] = (double)(AV196LevEv[1-1][43-1]+AV115over_) ;
         AV196LevEv[2-1][43-1] = (double)(AV196LevEv[2-1][43-1]+AV115over_) ;
         AV196LevEv[1-1][4-1] = (double)(AV196LevEv[1-1][4-1]+AV42Due_Ag) ;
         AV196LevEv[2-1][4-1] = (double)(AV196LevEv[2-1][4-1]+AV42Due_Ag) ;
         if ( ( GXutil.strcmp(AV215Carta, "TP") == 0 ) )
         {
            AV196LevEv[1-1][35-1] = (double)(AV196LevEv[1-1][35-1]+AV108CC_Am) ;
            AV196LevEv[2-1][35-1] = (double)(AV196LevEv[2-1][35-1]+AV108CC_Am) ;
         }
         else
         {
            if ( ( GXutil.strcmp(AV215Carta, "GR") == 0 ) )
            {
               AV196LevEv[1-1][38-1] = (double)(AV196LevEv[1-1][38-1]+AV108CC_Am) ;
               AV196LevEv[2-1][38-1] = (double)(AV196LevEv[2-1][38-1]+AV108CC_Am) ;
            }
            else
            {
               AV196LevEv[1-1][5-1] = (double)(AV196LevEv[1-1][5-1]+AV108CC_Am) ;
               AV196LevEv[2-1][5-1] = (double)(AV196LevEv[2-1][5-1]+AV108CC_Am) ;
            }
         }
         AV196LevEv[1-1][8-1] = (double)(AV196LevEv[1-1][8-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         AV196LevEv[2-1][8-1] = (double)(AV196LevEv[2-1][8-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         if ( ( AV218C_Pos > 0 ) )
         {
            if ( ( GXutil.strcmp(AV215Carta, "TP") == 0 ) )
            {
               AV196LevEv[AV218C_Pos-1][2-1] = (double)(AV196LevEv[AV218C_Pos-1][2-1]+AV108CC_Am) ;
            }
            else
            {
               AV196LevEv[AV218C_Pos-1][1-1] = (double)(AV196LevEv[AV218C_Pos-1][1-1]+AV108CC_Am) ;
            }
         }
      }
      else
      {
         AV199LevEv[1-1][1-1] = (double)(AV199LevEv[1-1][1-1]+AV41Tkt_Am) ;
         AV199LevEv[2-1][1-1] = (double)(AV199LevEv[2-1][1-1]+AV41Tkt_Am) ;
         AV199LevEv[1-1][2-1] = (double)(AV199LevEv[1-1][2-1]+AV211IVAAM) ;
         AV199LevEv[2-1][2-1] = (double)(AV199LevEv[2-1][2-1]+AV211IVAAM) ;
         AV199LevEv[1-1][3-1] = (double)(AV199LevEv[1-1][3-1]+(AV76Lit_co+AV77Over_c)) ;
         AV199LevEv[2-1][3-1] = (double)(AV199LevEv[2-1][3-1]+(AV76Lit_co+AV77Over_c)) ;
         AV199LevEv[1-1][41-1] = (double)(AV199LevEv[1-1][41-1]+AV76Lit_co) ;
         AV199LevEv[2-1][41-1] = (double)(AV199LevEv[2-1][41-1]+AV76Lit_co) ;
         AV199LevEv[1-1][42-1] = (double)(AV199LevEv[1-1][42-1]+((AV77Over_c+AV115over_))) ;
         AV199LevEv[2-1][42-1] = (double)(AV199LevEv[2-1][42-1]+((AV77Over_c+AV115over_))) ;
         AV199LevEv[1-1][43-1] = (double)(AV199LevEv[1-1][43-1]+AV115over_) ;
         AV199LevEv[2-1][43-1] = (double)(AV199LevEv[2-1][43-1]+AV115over_) ;
         AV199LevEv[1-1][4-1] = (double)(AV199LevEv[1-1][4-1]+AV42Due_Ag) ;
         AV199LevEv[2-1][4-1] = (double)(AV199LevEv[2-1][4-1]+AV42Due_Ag) ;
         if ( ( GXutil.strcmp(AV215Carta, "TP") == 0 ) )
         {
            AV199LevEv[1-1][35-1] = (double)(AV199LevEv[1-1][35-1]+AV108CC_Am) ;
            AV199LevEv[2-1][35-1] = (double)(AV199LevEv[2-1][35-1]+AV108CC_Am) ;
         }
         else
         {
            if ( ( GXutil.strcmp(AV215Carta, "GR") == 0 ) )
            {
               AV199LevEv[1-1][38-1] = (double)(AV199LevEv[1-1][38-1]+AV108CC_Am) ;
               AV199LevEv[2-1][38-1] = (double)(AV199LevEv[2-1][38-1]+AV108CC_Am) ;
            }
            else
            {
               AV199LevEv[1-1][5-1] = (double)(AV199LevEv[1-1][5-1]+AV108CC_Am) ;
               AV199LevEv[2-1][5-1] = (double)(AV199LevEv[2-1][5-1]+AV108CC_Am) ;
            }
         }
         AV199LevEv[1-1][8-1] = (double)(AV199LevEv[1-1][8-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         AV199LevEv[2-1][8-1] = (double)(AV199LevEv[2-1][8-1]+(AV76Lit_co+AV77Over_c+AV211IVAAM)) ;
         if ( ( AV218C_Pos > 0 ) )
         {
            if ( ( GXutil.strcmp(AV215Carta, "TP") == 0 ) )
            {
               AV199LevEv[AV218C_Pos-1][2-1] = (double)(AV199LevEv[AV218C_Pos-1][2-1]+AV108CC_Am) ;
            }
            else
            {
               AV199LevEv[AV218C_Pos-1][1-1] = (double)(AV199LevEv[AV218C_Pos-1][1-1]+AV108CC_Am) ;
            }
         }
      }
      AV215Carta = "" ;
   }

   public void S181( )
   {
      /* 'ACUTAXEVE' Routine */
      if ( ( ( GXutil.len( GXutil.trim( AV40Tkt)) < 9 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV226ADM_p) != 0 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV227ACM_p) != 0 ) && ( GXutil.strcmp(AV238Trans, "D") != 0 ) ) || ( GXutil.strcmp(AV238Trans, "R") == 0 ) )
      {
         /* Execute user subroutine: S281 */
         S281 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( ( GXutil.len( GXutil.trim( AV40Tkt)) < 9 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV226ADM_p) == 0 ) ) || ( GXutil.strcmp(AV238Trans, "D") == 0 ) )
      {
         /* Execute user subroutine: S291 */
         S291 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else if ( ( ( GXutil.len( GXutil.trim( AV40Tkt)) < 9 ) && ( GXutil.strcmp(GXutil.substring( AV40Tkt, 1, 2), AV227ACM_p) == 0 ) ) || ( GXutil.strcmp(AV238Trans, "C") == 0 ) )
      {
         /* Execute user subroutine: S301 */
         S301 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
      else
      {
         /* Execute user subroutine: S311 */
         S311 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S281( )
   {
      /* 'ACUTAXRFD' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
         {
            if ( ( AV185Tax_a[AV184TaxIn-1] < 0 ) )
            {
               AV196LevEv[1-1][14-1] = (double)(AV196LevEv[1-1][14-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][14-1] = (double)(AV196LevEv[2-1][14-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][33-1] = (double)(AV196LevEv[1-1][33-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][33-1] = (double)(AV196LevEv[2-1][33-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( AV185Tax_a[AV184TaxIn-1] < 0 ) )
            {
               AV196LevEv[1-1][15-1] = (double)(AV196LevEv[1-1][15-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][15-1] = (double)(AV196LevEv[2-1][15-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][34-1] = (double)(AV196LevEv[1-1][34-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][34-1] = (double)(AV196LevEv[2-1][34-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               if ( ( AV108CC_Am == 0 ) )
               {
                  AV196LevEv[1-1][50-1] = (double)(AV196LevEv[1-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV196LevEv[2-1][50-1] = (double)(AV196LevEv[2-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               }
               else
               {
                  if ( ( GXutil.strcmp(GXutil.substring( AV79Pay_ty, 1, 2), "CA") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(AV365RFND2, "CA") == 0 ) )
                     {
                        AV196LevEv[1-1][50-1] = (double)(AV196LevEv[1-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                        AV196LevEv[2-1][50-1] = (double)(AV196LevEv[2-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                     }
                     else
                     {
                        AV196LevEv[1-1][51-1] = (double)(AV196LevEv[1-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                        AV196LevEv[2-1][51-1] = (double)(AV196LevEv[2-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                     }
                  }
                  else
                  {
                     AV196LevEv[1-1][51-1] = (double)(AV196LevEv[1-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                     AV196LevEv[2-1][51-1] = (double)(AV196LevEv[2-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  }
               }
            }
         }
      }
      else
      {
         if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
         {
            if ( ( AV185Tax_a[AV184TaxIn-1] < 0 ) )
            {
               AV199LevEv[1-1][14-1] = (double)(AV199LevEv[1-1][14-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][14-1] = (double)(AV199LevEv[2-1][14-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][33-1] = (double)(AV199LevEv[1-1][33-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][33-1] = (double)(AV199LevEv[2-1][33-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( AV185Tax_a[AV184TaxIn-1] < 0 ) )
            {
               AV199LevEv[1-1][15-1] = (double)(AV199LevEv[1-1][15-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][15-1] = (double)(AV199LevEv[2-1][15-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][34-1] = (double)(AV199LevEv[1-1][34-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][34-1] = (double)(AV199LevEv[2-1][34-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               if ( ( AV108CC_Am == 0 ) )
               {
                  AV199LevEv[1-1][50-1] = (double)(AV199LevEv[1-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  AV199LevEv[2-1][50-1] = (double)(AV199LevEv[2-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               }
               else
               {
                  if ( ( GXutil.strcmp(GXutil.substring( AV79Pay_ty, 1, 2), "CA") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(AV365RFND2, "CA") == 0 ) )
                     {
                        AV199LevEv[1-1][50-1] = (double)(AV199LevEv[1-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                        AV199LevEv[2-1][50-1] = (double)(AV199LevEv[2-1][50-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                     }
                     else
                     {
                        AV199LevEv[1-1][51-1] = (double)(AV199LevEv[1-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                        AV199LevEv[2-1][51-1] = (double)(AV199LevEv[2-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                     }
                  }
                  else
                  {
                     AV199LevEv[1-1][51-1] = (double)(AV199LevEv[1-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                     AV199LevEv[2-1][51-1] = (double)(AV199LevEv[2-1][51-1]+(AV185Tax_a[AV184TaxIn-1])) ;
                  }
               }
            }
         }
      }
   }

   public void S291( )
   {
      /* 'ACUTAXADM' Routine */
      /* Execute user subroutine: S271 */
      S271 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV225BSPIa), "") != 0 ) && ( GXutil.strcmp(GXutil.trim( AV225BSPIa), GXutil.trim( AV22iataC)) == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV196LevEv[1-1][22-1] = (double)(AV196LevEv[1-1][22-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][22-1] = (double)(AV196LevEv[2-1][22-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][23-1] = (double)(AV196LevEv[1-1][23-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][23-1] = (double)(AV196LevEv[2-1][23-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV199LevEv[1-1][22-1] = (double)(AV199LevEv[1-1][22-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][22-1] = (double)(AV199LevEv[2-1][22-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][23-1] = (double)(AV199LevEv[1-1][23-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][23-1] = (double)(AV199LevEv[2-1][23-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
      }
      else if ( ( GXutil.strcmp(AV252TipoM, "T") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV196LevEv[1-1][56-1] = (double)(AV196LevEv[1-1][56-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][56-1] = (double)(AV196LevEv[2-1][56-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][57-1] = (double)(AV196LevEv[1-1][57-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][57-1] = (double)(AV196LevEv[2-1][57-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV199LevEv[1-1][56-1] = (double)(AV199LevEv[1-1][56-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][56-1] = (double)(AV199LevEv[2-1][56-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][57-1] = (double)(AV199LevEv[1-1][57-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][57-1] = (double)(AV199LevEv[2-1][57-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
      }
      else
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV196LevEv[1-1][58-1] = (double)(AV196LevEv[1-1][58-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][58-1] = (double)(AV196LevEv[2-1][58-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][59-1] = (double)(AV196LevEv[1-1][59-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][59-1] = (double)(AV196LevEv[2-1][59-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV199LevEv[1-1][58-1] = (double)(AV199LevEv[1-1][58-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][58-1] = (double)(AV199LevEv[2-1][58-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][59-1] = (double)(AV199LevEv[1-1][59-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][59-1] = (double)(AV199LevEv[2-1][59-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
      }
   }

   public void S301( )
   {
      /* 'ACUTAXACM' Routine */
      /* Execute user subroutine: S271 */
      S271 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( GXutil.strcmp(GXutil.trim( AV225BSPIa), "") != 0 ) && ( GXutil.strcmp(GXutil.trim( AV225BSPIa), GXutil.trim( AV22iataC)) == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV196LevEv[1-1][30-1] = (double)(AV196LevEv[1-1][30-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][30-1] = (double)(AV196LevEv[2-1][30-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][31-1] = (double)(AV196LevEv[1-1][31-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][31-1] = (double)(AV196LevEv[2-1][31-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV199LevEv[1-1][30-1] = (double)(AV199LevEv[1-1][30-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][30-1] = (double)(AV199LevEv[2-1][30-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][31-1] = (double)(AV199LevEv[1-1][31-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][31-1] = (double)(AV199LevEv[2-1][31-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
      }
      else if ( ( GXutil.strcmp(AV252TipoM, "T") == 0 ) )
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV196LevEv[1-1][65-1] = (double)(AV196LevEv[1-1][65-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][65-1] = (double)(AV196LevEv[2-1][65-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][66-1] = (double)(AV196LevEv[1-1][66-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][67-1] = (double)(AV196LevEv[2-1][67-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV199LevEv[1-1][65-1] = (double)(AV199LevEv[1-1][65-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][65-1] = (double)(AV199LevEv[2-1][65-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][66-1] = (double)(AV199LevEv[1-1][66-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][66-1] = (double)(AV199LevEv[2-1][66-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
      }
      else
      {
         if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV196LevEv[1-1][67-1] = (double)(AV196LevEv[1-1][67-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][67-1] = (double)(AV196LevEv[2-1][67-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV196LevEv[1-1][68-1] = (double)(AV196LevEv[1-1][68-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV196LevEv[2-1][68-1] = (double)(AV196LevEv[2-1][68-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
         else
         {
            if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
            {
               AV199LevEv[1-1][67-1] = (double)(AV199LevEv[1-1][67-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][67-1] = (double)(AV199LevEv[2-1][67-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
            else
            {
               AV199LevEv[1-1][68-1] = (double)(AV199LevEv[1-1][68-1]+(AV185Tax_a[AV184TaxIn-1])) ;
               AV199LevEv[2-1][68-1] = (double)(AV199LevEv[2-1][68-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            }
         }
      }
   }

   public void S311( )
   {
      /* 'ACUTAXTKT' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
         {
            AV196LevEv[1-1][6-1] = (double)(AV196LevEv[1-1][6-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV196LevEv[2-1][6-1] = (double)(AV196LevEv[2-1][6-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
         else
         {
            AV196LevEv[1-1][7-1] = (double)(AV196LevEv[1-1][7-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV196LevEv[2-1][7-1] = (double)(AV196LevEv[2-1][7-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(AV214LocTa, GXutil.substring( AV183Tax_C[AV184TaxIn-1], 1, 2)) == 0 ) )
         {
            AV199LevEv[1-1][6-1] = (double)(AV199LevEv[1-1][6-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV199LevEv[2-1][6-1] = (double)(AV199LevEv[2-1][6-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
         else
         {
            AV199LevEv[1-1][7-1] = (double)(AV199LevEv[1-1][7-1]+(AV185Tax_a[AV184TaxIn-1])) ;
            AV199LevEv[2-1][7-1] = (double)(AV199LevEv[2-1][7-1]+(AV185Tax_a[AV184TaxIn-1])) ;
         }
      }
   }

   public void S321270( )
   {
      /* 'GRVSALEVE' Routine */
      AV203SalCo = AV189Level[AV192I-1] ;
      AV202ASCod = AV320AcctS[AV192I-1][AV193J-1] ;
      AV204SalAm = AV196LevEv[AV192I-1][AV193J-1] ;
      AV205SalAm = AV199LevEv[AV192I-1][AV193J-1] ;
      if ( ( GXutil.strcmp(AV287LOADE, "N") != 0 ) && ( ( AV204SalAm != 0 ) || ( AV205SalAm != 0 ) ) )
      {
      }
   }

   public void S331298( )
   {
      /* 'LIMPALEV2' Routine */
      AV213Curre = AV192I ;
      AV192I = (byte)(2) ;
      AV193J = (short)(1) ;
      while ( ( AV193J <= AV220QtdSc ) )
      {
         AV196LevEv[AV192I-1][AV193J-1] = 0 ;
         AV199LevEv[AV192I-1][AV193J-1] = 0 ;
         AV193J = (short)(AV193J+1) ;
      }
      AV192I = (byte)(AV213Curre) ;
   }

   public void S341312( )
   {
      /* 'LIMPAVARS' Routine */
      AV29Tax_1 = 0 ;
      AV28Tax_2 = 0 ;
      AV30Tax_Br = 0 ;
      AV31Tax_De = 1 ;
      AV32Tax_Xt = 0 ;
      AV33Tax_Us = 0 ;
      AV34Tax_It = 0 ;
      AV35Tax_Ot = 0 ;
      AV36Tax_Ch = 0 ;
      AV37Tax_Xx = 0 ;
      AV38Tax_Cp = 0 ;
      AV116Tax2A = 0 ;
      AV109Trans = AV39Trans_ ;
      AV39Trans_ = GXutil.substring( AV8Linha, 20, 6) ;
      AV238Trans = "" ;
      AV246Trans = "" ;
      AV40Tkt = "" ;
      AV174Tkt2 = 0 ;
      AV112TktPa = "" ;
      AV41Tkt_Am = 0 ;
      AV126TktFi = "" ;
      AV129TktFi = 0 ;
      AV42Due_Ag = 0 ;
      AV43Issue_ = "" ;
      AV118Grupo = " " ;
      AV132SeqPg = 1 ;
      AV250STAT = " " ;
      AV303wSTAT = " " ;
      AV172PagSe = 1 ;
      AV127Passo = "N" ;
      AV133FezAt = "N" ;
      AV104TLinA = " " ;
      AV299HOT_F = "" ;
      AV245RPSI = "" ;
      AV110VendC = " " ;
      AV107Cash_ = 0 ;
      AV108CC_Am = 0 ;
      AV175Rate_ = 0 ;
      AV176Exc_R = 0 ;
      AV131SeqIt = 1 ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV135PerMT[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV138CodeM[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV136IataM[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV148Num_M[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV156Tipo_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV157DataM[GX_I-1] = GXutil.nullDate() ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV139ISeqM[GX_I-1] = 0 ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV140NumBP[GX_I-1] = 0 ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV141I_Fro[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV142I_ToM[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV143Carri[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV144Tarri[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV145Class[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV146Fligh[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV147Fli_M[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV271Fli_M[GX_I-1] = GXutil.nullDate() ;
         GX_I = (int)(GX_I+1) ;
      }
      AV211IVAAM = 0 ;
      AV77Over_c = 0 ;
      AV115over_ = 0 ;
      AV76Lit_co = 0 ;
      AV276FCMI = "" ;
      AV277INLS = "" ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV278FRCA[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV279FRCAS = (byte)(0) ;
      AV289CPUI = "" ;
      AV300IndHO = (byte)(0) ;
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV265Sub_N[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV266Sub_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV267Sub_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV268Sub_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV301EFRT = 0 ;
   }

   public void S351388( )
   {
      /* 'GRAVAHOT' Routine */
      AV354statu = "IGNORED" ;
      /*
         INSERT RECORD ON TABLE HOT

      */
      A963ISOC = AV373ISOC ;
      A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
      A965PER_NA = GXutil.substring( AV20Period, 1, 10) ;
      AV338Per_N = AV20Period ;
      A966CODE = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
      A918CODE2 = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
      n918CODE2 = false ;
      AV335CODEA = GXutil.trim( AV246Trans) ;
      A967IATA = GXutil.substring( AV22iataC, 1, 15) ;
      AV336IATAA = AV22iataC ;
      A968NUM_BI = GXutil.substring( AV40Tkt, 1, 14) ;
      AV334Num_B = GXutil.substring( AV40Tkt, 1, 14) ;
      A900MOEDA = GXutil.substring( AV206PgMoe, 1, 5) ;
      n900MOEDA = false ;
      A869ID_AGE = GXutil.substring( AV26Id_Age, 1, 4) ;
      n869ID_AGE = false ;
      A970DATA = AV55Datbks ;
      AV339DataA = AV55Datbks ;
      A877TOUR_C = GXutil.strReplace( AV389Short, ".txt", "") ;
      n877TOUR_C = false ;
      A872LIT_CO = AV76Lit_co ;
      n872LIT_CO = false ;
      A873OVER_C = AV77Over_c ;
      n873OVER_C = false ;
      A888OVER_I = AV115over_ ;
      n888OVER_I = false ;
      A890ADM_AC = GXutil.substring( AV238Trans, 1, 1) ;
      n890ADM_AC = false ;
      A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
      AV337Tipo_ = AV79Pay_ty ;
      A870VALOR_ = AV41Tkt_Am ;
      n870VALOR_ = false ;
      A878PAX_NA = GXutil.substring( AV83Pax, 1, 50) ;
      n878PAX_NA = false ;
      AV83Pax = " " ;
      AV102Var1 = (double)(AV76Lit_co*100) ;
      AV102Var1 = (double)(AV102Var1*-1) ;
      if ( ( AV41Tkt_Am != 0 ) )
      {
         AV100Var = (double)(AV102Var1/ (double) ((AV41Tkt_Am))) ;
      }
      else
      {
         AV100Var = 0 ;
      }
      if ( ( AV31Tax_De == 1 ) )
      {
         if ( ( GXutil.strcmp(AV201ciaco, "954") == 0 ) && ( GXutil.strcmp(GXutil.trim( AV246Trans), "CANN") == 0 ) )
         {
            AV31Tax_De = 1 ;
         }
      }
      A871COMMIS = (double)(DecimalUtil.decToDouble(GXutil.roundDecimal( DecimalUtil.doubleToDec(AV100Var), 0))) ;
      n871COMMIS = false ;
      AV100Var = 0 ;
      A894PLP_DO = ((GXutil.strcmp(AV245RPSI, "")==0) ? "WSSS" : GXutil.substring( AV245RPSI, 1, 7)) ;
      n894PLP_DO = false ;
      A897NUMBER = AV376PNR ;
      n897NUMBER = false ;
      A874AIRPT_ = AV108CC_Am ;
      n874AIRPT_ = false ;
      A875TAX_1 = AV29Tax_1 ;
      n875TAX_1 = false ;
      A876TAX_2 = AV28Tax_2 ;
      n876TAX_2 = false ;
      A879TAX_BR = AV30Tax_Br ;
      n879TAX_BR = false ;
      A880TAX_DE = AV31Tax_De ;
      n880TAX_DE = false ;
      A881TAX_XT = AV32Tax_Xt ;
      n881TAX_XT = false ;
      A882TAX_US = AV33Tax_Us ;
      n882TAX_US = false ;
      if ( ( GXutil.strcmp(AV321NewIv, "Y") == 0 ) )
      {
         A883TAX_IT = AV34Tax_It ;
         n883TAX_IT = false ;
      }
      else
      {
         A883TAX_IT = AV175Rate_ ;
         n883TAX_IT = false ;
      }
      A884TAX_OT = AV176Exc_R ;
      n884TAX_OT = false ;
      A885TAX_CH = AV36Tax_Ch ;
      n885TAX_CH = false ;
      A886TAX_XX = AV37Tax_Xx ;
      n886TAX_XX = false ;
      A887TAX_CP = AV38Tax_Cp ;
      n887TAX_CP = false ;
      A889TRANS_ = "P" ;
      n889TRANS_ = false ;
      A891CC_TYP = GXutil.substring( AV80Cc_Typ, 1, 2) ;
      n891CC_TYP = false ;
      A909HOT_FO = GXutil.substring( AV299HOT_F, 1, 1) ;
      n909HOT_FO = false ;
      AV87Numero = GXutil.substring( AV61Tour_C, 6, 10) ;
      A893DUE_AG = AV42Due_Ag ;
      n893DUE_AG = false ;
      A898ISSUE_ = GXutil.substring( AV43Issue_, 1, 100) ;
      n898ISSUE_ = false ;
      A904IVAAmo = AV211IVAAM ;
      n904IVAAmo = false ;
      if ( ( AV41Tkt_Am == 0 ) && ( AV108CC_Am == 0 ) && ( AV29Tax_1 == 0 ) && ( AV28Tax_2 == 0 ) && ( AV42Due_Ag == 0 ) )
      {
         A899PAYMT_ = "" ;
         n899PAYMT_ = false ;
      }
      else
      {
         A899PAYMT_ = GXutil.substring( AV182paymt, 1, 100) ;
         n899PAYMT_ = false ;
      }
      A899PAYMT_ = AV182paymt ;
      n899PAYMT_ = false ;
      A902NUM_BI = (long)(GXutil.val( GXutil.substring( AV40Tkt, 1, 10), ".")) ;
      n902NUM_BI = false ;
      AV181NUM_B = AV210Num_b ;
      if ( ( GXutil.len( AV186Linha) > 165 ) )
      {
         AV186Linha = "" ;
      }
      AV186Linha = AV186Linha + GXutil.padr( AV40Tkt, (short)(10), " ") + " " ;
      A905FCMI = GXutil.substring( AV276FCMI, 1, 1) ;
      n905FCMI = false ;
      A906INLS = GXutil.substring( AV277INLS, 1, 4) ;
      n906INLS = false ;
      A907CPUI = GXutil.substring( AV289CPUI, 1, 4) ;
      n907CPUI = false ;
      A908Loader = GXutil.substring( AV290VerNu, 1, 6) ;
      n908Loader = false ;
      A910EFRT = AV301EFRT ;
      n910EFRT = false ;
      if ( ( GXutil.strcmp(AV404Trans, "N") == 0 ) )
      {
         A913TCNR = "X" ;
         n913TCNR = false ;
      }
      AV354statu = "INSERTED" ;
      /* Using cursor P005F4 */
      pr_default.execute(2, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Boolean(n869ID_AGE), A869ID_AGE, new Boolean(n870VALOR_), new Double(A870VALOR_), new Boolean(n871COMMIS), new Double(A871COMMIS), new Boolean(n872LIT_CO), new Double(A872LIT_CO), new Boolean(n873OVER_C), new Double(A873OVER_C), new Boolean(n874AIRPT_), new Double(A874AIRPT_), new Boolean(n875TAX_1), new Double(A875TAX_1), new Boolean(n876TAX_2), new Double(A876TAX_2), new Boolean(n877TOUR_C), A877TOUR_C, new Boolean(n878PAX_NA), A878PAX_NA, new Boolean(n879TAX_BR), new Double(A879TAX_BR), new Boolean(n880TAX_DE), new Double(A880TAX_DE), new Boolean(n881TAX_XT), new Double(A881TAX_XT), new Boolean(n882TAX_US), new Double(A882TAX_US), new Boolean(n883TAX_IT), new Double(A883TAX_IT), new Boolean(n884TAX_OT), new Double(A884TAX_OT), new Boolean(n885TAX_CH), new Double(A885TAX_CH), new Boolean(n886TAX_XX), new Double(A886TAX_XX), new Boolean(n887TAX_CP), new Double(A887TAX_CP), new Boolean(n888OVER_I), new Double(A888OVER_I), new Boolean(n889TRANS_), A889TRANS_, new Boolean(n890ADM_AC), A890ADM_AC, new Boolean(n891CC_TYP), A891CC_TYP, new Boolean(n893DUE_AG), new Double(A893DUE_AG), new Boolean(n894PLP_DO), A894PLP_DO, new Boolean(n897NUMBER), A897NUMBER, new Boolean(n898ISSUE_), A898ISSUE_, new Boolean(n899PAYMT_), A899PAYMT_, new Boolean(n900MOEDA), A900MOEDA, new Boolean(n902NUM_BI), new Long(A902NUM_BI), new Boolean(n904IVAAmo), new Double(A904IVAAmo), new Boolean(n905FCMI), A905FCMI, new Boolean(n906INLS), A906INLS, new Boolean(n907CPUI), A907CPUI, new Boolean(n908Loader), A908Loader, new Boolean(n909HOT_FO), A909HOT_FO, new Boolean(n910EFRT), new Double(A910EFRT), new Boolean(n913TCNR), A913TCNR, new Boolean(n918CODE2), A918CODE2});
      if ( (pr_default.getStatus(2) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         AV354statu = "IGNORED" ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      /*
         INSERT RECORD ON TABLE HOT6

      */
      A963ISOC = AV373ISOC ;
      A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
      A965PER_NA = GXutil.substring( AV20Period, 1, 10) ;
      A966CODE = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
      A967IATA = GXutil.substring( AV22iataC, 1, 15) ;
      A968NUM_BI = GXutil.substring( AV40Tkt, 1, 14) ;
      A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
      A970DATA = AV55Datbks ;
      A978IFCode = "" ;
      A952IFStat = "" ;
      n952IFStat = false ;
      A953IFDate = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
      n953IFDate = false ;
      A954IFDeta = "Add by SABREWJ " + GXutil.trim( AV378Path) ;
      n954IFDeta = false ;
      /* Using cursor P005F5 */
      pr_default.execute(3, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A978IFCode, new Boolean(n952IFStat), A952IFStat, new Boolean(n953IFDate), A953IFDate, new Boolean(n954IFDeta), A954IFDeta});
      if ( (pr_default.getStatus(3) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      AV357Total = (int)(AV357Total+1) ;
      if ( ( AV279FRCAS > 0 ) && ( GXutil.strcmp(AV286LOADH, "N") != 0 ) )
      {
         AV279FRCAS = (byte)(1) ;
         while ( ( GXutil.strcmp(AV278FRCA[AV279FRCAS-1], "") != 0 ) && ( AV279FRCAS <= 5 ) )
         {
            /*
               INSERT RECORD ON TABLE HOT5

            */
            A963ISOC = AV373ISOC ;
            A965PER_NA = GXutil.substring( AV20Period, 1, 10) ;
            A966CODE = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
            A967IATA = GXutil.substring( AV22iataC, 1, 15) ;
            A968NUM_BI = GXutil.substring( AV40Tkt, 1, 14) ;
            A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
            A970DATA = AV55Datbks ;
            A977FRCASe = AV279FRCAS ;
            A951FRCA = GXutil.substring( AV278FRCA[AV279FRCAS-1], 1, 87) ;
            n951FRCA = false ;
            A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
            /* Using cursor P005F6 */
            pr_default.execute(4, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, new Byte(A977FRCASe), new Boolean(n951FRCA), A951FRCA});
            if ( (pr_default.getStatus(4) == 1) )
            {
               Gx_err = (short)(1) ;
               Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            }
            else
            {
               Gx_err = (short)(0) ;
               Gx_emsg = "" ;
            }
            /* End Insert */
            AV279FRCAS = (byte)(AV279FRCAS+1) ;
         }
      }
      if ( ( AV300IndHO > 0 ) )
      {
         AV300IndHO = (byte)(1) ;
         while ( ( AV300IndHO <= 30 ) )
         {
            if ( ( GXutil.strcmp(AV265Sub_N[AV300IndHO-1], "") != 0 ) )
            {
               /* Execute user subroutine: S361 */
               S361 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            AV300IndHO = (byte)(AV300IndHO+1) ;
         }
      }
   }

   public void S371558( )
   {
      /* 'GRAVAHOT3' Routine */
      if ( ( AV69ValxDe != 0 ) )
      {
         /*
            INSERT RECORD ON TABLE HOT3

         */
         A963ISOC = AV373ISOC ;
         A965PER_NA = GXutil.substring( AV20Period, 1, 10) ;
         A966CODE = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
         A967IATA = GXutil.substring( AV22iataC, 1, 15) ;
         A968NUM_BI = GXutil.substring( AV40Tkt, 1, 14) ;
         A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
         A970DATA = AV55Datbks ;
         A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
         A973Tax_Co = AV73Pais ;
         A942Tax_Am = AV69ValxDe ;
         n942Tax_Am = false ;
         /* Using cursor P005F7 */
         pr_default.execute(5, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A973Tax_Co, new Byte(A974Tax_Wh), A975Tax_Pd, new Boolean(n942Tax_Am), new Double(A942Tax_Am)});
         if ( (pr_default.getStatus(5) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
            /* Optimized UPDATE. */
            /* Using cursor P005F8 */
            pr_default.execute(6, new Object[] {new Double(AV69ValxDe), A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A973Tax_Co, new Byte(A974Tax_Wh), A975Tax_Pd});
            /* End optimized UPDATE. */
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
      }
   }

   public void S361( )
   {
      /* 'GRAVAHOT4' Routine */
      AV306H4Num = (long)(GXutil.val( GXutil.substring( AV265Sub_N[AV300IndHO-1], 4, 10), ".")) ;
      /*
         INSERT RECORD ON TABLE HOT4

      */
      A963ISOC = AV373ISOC ;
      A965PER_NA = GXutil.substring( AV20Period, 1, 10) ;
      A966CODE = GXutil.substring( GXutil.trim( AV246Trans), 1, 15) ;
      A967IATA = AV22iataC ;
      A968NUM_BI = GXutil.substring( AV40Tkt, 1, 14) ;
      A969TIPO_V = GXutil.substring( AV79Pay_ty, 1, 2) ;
      A970DATA = AV55Datbks ;
      A976Sub_Nu = GXutil.substring( AV265Sub_N[AV300IndHO-1], 1, 14) ;
      A948Sub_De = GXutil.substring( AV266Sub_D[AV300IndHO-1], 1, 40) ;
      n948Sub_De = false ;
      A949Sub_De = GXutil.substring( AV267Sub_D[AV300IndHO-1], 1, 40) ;
      n949Sub_De = false ;
      A950Sub_De = GXutil.substring( AV268Sub_D[AV300IndHO-1], 1, 40) ;
      n950Sub_De = false ;
      A964CiaCod = GXutil.substring( AV201ciaco, 1, 5) ;
      /* Using cursor P005F9 */
      pr_default.execute(7, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu, new Boolean(n948Sub_De), A948Sub_De, new Boolean(n949Sub_De), A949Sub_De, new Boolean(n950Sub_De), A950Sub_De});
      if ( (pr_default.getStatus(7) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         /* Using cursor P005F10 */
         pr_default.execute(8, new Object[] {A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A963ISOC = P005F10_A963ISOC[0] ;
            A964CiaCod = P005F10_A964CiaCod[0] ;
            A965PER_NA = P005F10_A965PER_NA[0] ;
            A966CODE = P005F10_A966CODE[0] ;
            A967IATA = P005F10_A967IATA[0] ;
            A968NUM_BI = P005F10_A968NUM_BI[0] ;
            A969TIPO_V = P005F10_A969TIPO_V[0] ;
            A970DATA = P005F10_A970DATA[0] ;
            A976Sub_Nu = P005F10_A976Sub_Nu[0] ;
            A948Sub_De = P005F10_A948Sub_De[0] ;
            n948Sub_De = P005F10_n948Sub_De[0] ;
            A949Sub_De = P005F10_A949Sub_De[0] ;
            n949Sub_De = P005F10_n949Sub_De[0] ;
            A950Sub_De = P005F10_A950Sub_De[0] ;
            n950Sub_De = P005F10_n950Sub_De[0] ;
            A948Sub_De = GXutil.substring( AV266Sub_D[AV300IndHO-1], 1, 40) ;
            n948Sub_De = false ;
            A949Sub_De = GXutil.substring( AV267Sub_D[AV300IndHO-1], 1, 40) ;
            n949Sub_De = false ;
            A950Sub_De = GXutil.substring( AV268Sub_D[AV300IndHO-1], 1, 40) ;
            n950Sub_De = false ;
            /* Using cursor P005F11 */
            pr_default.execute(9, new Object[] {new Boolean(n948Sub_De), A948Sub_De, new Boolean(n949Sub_De), A949Sub_De, new Boolean(n950Sub_De), A950Sub_De, A963ISOC, A964CiaCod, A965PER_NA, A966CODE, A967IATA, A968NUM_BI, A969TIPO_V, A970DATA, A976Sub_Nu});
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
   }

   public void S141( )
   {
      /* 'DIVIDER' Routine */
      AV212Divis = 0 ;
      if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 133, 1), "0") == 0 ) )
      {
         AV212Divis = 1 ;
      }
      else if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 133, 1), "1") == 0 ) )
      {
         AV212Divis = 10 ;
      }
      else if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 133, 1), "2") == 0 ) )
      {
         AV212Divis = 100 ;
      }
      else if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 133, 1), "3") == 0 ) )
      {
         AV212Divis = 1000 ;
      }
   }

   public void S381624( )
   {
      /* 'LEPARAMETROS' Routine */
      AV269Barra = "Reading Parameters" ;
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV207CiaLo = "" ;
      /* Using cursor P005F12 */
      pr_default.execute(10, new Object[] {AV201ciaco});
      while ( (pr_default.getStatus(10) != 101) )
      {
         A349CurCod = P005F12_A349CurCod[0] ;
         A23ISOCod = P005F12_A23ISOCod[0] ;
         AV207CiaLo = A349CurCod ;
         pr_default.readNext(10);
      }
      pr_default.close(10);
      AV212Divis = 100 ;
      AV209YearP = "0" ;
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV221Acm_P = "08" ;
      AV227ACM_p = GXutil.substring( AV221Acm_P, 2, 1) + GXutil.substring( AV221Acm_P, 1, 1) ;
      AV222Adm_P = "06" ;
      AV226ADM_p = GXutil.substring( AV222Adm_P, 2, 1) + GXutil.substring( AV222Adm_P, 1, 1) ;
      AV293ACMPr = (byte)(2) ;
      AV294ADMPr = (byte)(2) ;
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV224TADSL = 0 ;
      AV223TADSN = "" ;
      AV295TADDL = (byte)(6) ;
      AV296TADDN = "060018" ;
      AV297TADCL = (byte)(6) ;
      AV298TADCN = "080018" ;
      AV248GRlen = 0 ;
      AV247GRNum = "" ;
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV225BSPIa = "" ;
      AV251MemoD = "N" ;
      AV239AtuSa = 1 ;
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
   }

   public void S271( )
   {
      /* 'VERNUMMEMO' Routine */
      AV263Lista = AV253TADSL ;
      /* Execute user subroutine: S401 */
      S401 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( AV262FlagA != 0 ) )
      {
         AV252TipoM = "T" ;
      }
      else
      {
         AV263Lista = AV254GRLis ;
         /* Execute user subroutine: S401 */
         S401 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ( AV262FlagA != 0 ) )
         {
            AV252TipoM = "G" ;
         }
         else
         {
            AV252TipoM = "N" ;
         }
      }
   }

   public void S401( )
   {
      /* 'VERLISTA' Routine */
      AV262FlagA = (short)(0) ;
      AV263Lista = GXutil.strReplace( AV263Lista, " ", "") ;
      AV255ListL = (short)(GXutil.len( GXutil.trim( AV263Lista))) ;
      AV259Palav = "" ;
      AV256Pos = (short)(1) ;
      while ( ( AV256Pos <= AV255ListL ) )
      {
         AV257letra = GXutil.substring( AV263Lista, AV256Pos, 1) ;
         if ( ( GXutil.strcmp(AV257letra, ",") == 0 ) || ( GXutil.strcmp(AV257letra, ";") == 0 ) )
         {
            AV258Check = GXutil.trim( AV259Palav) ;
            AV260LenCh = (short)(GXutil.len( AV258Check)) ;
            AV261mNum_ = GXutil.trim( GXutil.substring( AV40Tkt, 1, AV260LenCh)) ;
            if ( ( GXutil.strcmp(AV261mNum_, AV258Check) == 0 ) )
            {
               AV262FlagA = (short)(AV262FlagA+1) ;
            }
            AV259Palav = "" ;
         }
         else
         {
            AV259Palav = AV259Palav + AV257letra ;
         }
         AV256Pos = (short)(AV256Pos+1) ;
      }
      AV258Check = GXutil.trim( AV259Palav) ;
      AV260LenCh = (short)(GXutil.len( AV258Check)) ;
      AV261mNum_ = GXutil.trim( GXutil.substring( AV40Tkt, 1, AV260LenCh)) ;
      if ( ( GXutil.strcmp(AV261mNum_, AV258Check) == 0 ) )
      {
         AV262FlagA = (short)(AV262FlagA+1) ;
      }
   }

   public void S391( )
   {
      /* 'BARRASTAT' Routine */
      if ( ( GXutil.strcmp(AV281Modo, "A") != 0 ) )
      {
         /* Execute user subroutine: S411 */
         S411 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }
   }

   public void S411( )
   {
      /* 'BARRAMODOA' Routine */
      if ( ( GXutil.len( AV270msg1) <= 158 ) && ( GXutil.strcmp(AV348Type, "J") != 0 ) )
      {
      }
      else
      {
         AV270msg1 = "" ;
      }
      context.msgStatus( AV269Barra+AV270msg1 );
   }

   public void S421838( )
   {
      /* 'PROCESSAHOT' Routine */
      AV347Proce = "Y" ;
      AV404Trans = "Y" ;
      /* Using cursor P005F13 */
      pr_default.execute(11, new Object[] {new Integer(AV291First)});
      while ( (pr_default.getStatus(11) != 101) )
      {
         A997HntSeq = P005F13_A997HntSeq[0] ;
         A994HntLin = P005F13_A994HntLin[0] ;
         n994HntLin = P005F13_n994HntLin[0] ;
         A998HntSub = P005F13_A998HntSub[0] ;
         AV8Linha = A994HntLin ;
         /* Execute user subroutine: S391 */
         S391 ();
         if ( returnInSub )
         {
            pr_default.close(11);
            returnInSub = true;
            if (true) return;
         }
         AV9TLine = GXutil.substring( AV8Linha, 1, 3) ;
         AV21TSubLi = GXutil.substring( AV8Linha, 12, 2) ;
         if ( ( GXutil.strcmp(AV9TLine, "BFH") == 0 ) )
         {
            AV10DtFH = GXutil.substring( AV8Linha, 29, 6) ;
            AV11TmFH = GXutil.substring( AV8Linha, 35, 4) ;
            AV373ISOC = ((GXutil.strcmp(GXutil.substring( AV8Linha, 39, 2), "")==0) ? "BR" : GXutil.substring( AV8Linha, 39, 2)) ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BCH") == 0 ) )
         {
            AV379DATAC = GXutil.substring( AV8Linha, 20, 2) + "/" + GXutil.substring( AV8Linha, 22, 2) + "/" + GXutil.substring( AV8Linha, 18, 2) ;
            AV380DATA_ = localUtil.ctod( AV379DATAC, 2) ;
            AV394Data_ = localUtil.ctod( AV379DATAC, 2) ;
            if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 24, 1), "D") != 0 ) )
            {
            }
            AV12yy = GXutil.substring( AV8Linha, 18, 2) ;
            AV24y1 = GXutil.substring( AV12yy, 1, 1) ;
            AV25y2 = GXutil.substring( AV12yy, 2, 1) ;
            if ( ( GXutil.strcmp(AV24y1, "0") == 0 ) )
            {
               AV12yy = GXutil.concat( AV209YearP, AV25y2, "") ;
            }
            AV14mm = GXutil.substring( AV8Linha, 20, 2) ;
            AV15dd = GXutil.substring( AV8Linha, 22, 2) ;
            AV17Data1 = GXutil.concat( AV14mm, AV15dd, "/") ;
            AV17Data1 = GXutil.concat( AV17Data1, AV12yy, "/") ;
            AV16Per_Da = localUtil.ctod( AV17Data1, 2) ;
            AV18Data_I = GXutil.dadd(AV16Per_Da,-(30)) ;
            AV19ddval = (byte)(GXutil.val( AV15dd, ".")) ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BOH") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "03") == 0 ) )
         {
            AV85Posic = (int)(GXutil.val( GXutil.substring( AV8Linha, 4, 8), ".")) ;
            AV88Posian = AV85Posic ;
            if ( ( AV85Posic != 3 ) )
            {
               if ( ( GXutil.strcmp(AV201ciaco, "AACL") != 0 ) )
               {
                  AV240CurrC = GXutil.trim( AV246Trans) ;
                  AV443GXLvl = (byte)(0) ;
                  /* Using cursor P005F14 */
                  pr_default.execute(12, new Object[] {new Long(AV174Tkt2), AV20Period, AV240CurrC});
                  while ( (pr_default.getStatus(12) != 101) )
                  {
                     A966CODE = P005F14_A966CODE[0] ;
                     A965PER_NA = P005F14_A965PER_NA[0] ;
                     A902NUM_BI = P005F14_A902NUM_BI[0] ;
                     n902NUM_BI = P005F14_n902NUM_BI[0] ;
                     A874AIRPT_ = P005F14_A874AIRPT_[0] ;
                     n874AIRPT_ = P005F14_n874AIRPT_[0] ;
                     A963ISOC = P005F14_A963ISOC[0] ;
                     A964CiaCod = P005F14_A964CiaCod[0] ;
                     A967IATA = P005F14_A967IATA[0] ;
                     A968NUM_BI = P005F14_A968NUM_BI[0] ;
                     A969TIPO_V = P005F14_A969TIPO_V[0] ;
                     A970DATA = P005F14_A970DATA[0] ;
                     AV443GXLvl = (byte)(1) ;
                     pr_default.readNext(12);
                  }
                  pr_default.close(12);
                  if ( ( AV443GXLvl == 0 ) )
                  {
                     /* Execute user subroutine: S351388 */
                     S351388 ();
                     if ( returnInSub )
                     {
                        pr_default.close(11);
                        returnInSub = true;
                        if (true) return;
                     }
                  }
               }
               else
               {
                  AV240CurrC = GXutil.trim( AV246Trans) ;
                  AV444GXLvl = (byte)(0) ;
                  /* Using cursor P005F15 */
                  pr_default.execute(13, new Object[] {new Long(AV174Tkt2), AV240CurrC, AV20Period});
                  while ( (pr_default.getStatus(13) != 101) )
                  {
                     A965PER_NA = P005F15_A965PER_NA[0] ;
                     A966CODE = P005F15_A966CODE[0] ;
                     A902NUM_BI = P005F15_A902NUM_BI[0] ;
                     n902NUM_BI = P005F15_n902NUM_BI[0] ;
                     A874AIRPT_ = P005F15_A874AIRPT_[0] ;
                     n874AIRPT_ = P005F15_n874AIRPT_[0] ;
                     A963ISOC = P005F15_A963ISOC[0] ;
                     A964CiaCod = P005F15_A964CiaCod[0] ;
                     A967IATA = P005F15_A967IATA[0] ;
                     A968NUM_BI = P005F15_A968NUM_BI[0] ;
                     A969TIPO_V = P005F15_A969TIPO_V[0] ;
                     A970DATA = P005F15_A970DATA[0] ;
                     AV444GXLvl = (byte)(1) ;
                     pr_default.readNext(13);
                  }
                  pr_default.close(13);
                  if ( ( AV444GXLvl == 0 ) )
                  {
                     /* Execute user subroutine: S351388 */
                     S351388 ();
                     if ( returnInSub )
                     {
                        pr_default.close(11);
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  AV240CurrC = "" ;
               }
               if ( ( AV239AtuSa == 1 ) )
               {
                  /* Execute user subroutine: S20625 */
                  S20625 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               /* Execute user subroutine: S17527 */
               S17527 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: S15423 */
               S15423 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: S16478 */
               S16478 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               AV192I = (byte)(2) ;
               AV193J = (short)(1) ;
               while ( ( AV193J <= AV220QtdSc ) )
               {
                  if ( ( AV239AtuSa == 1 ) )
                  {
                     /* Execute user subroutine: S321270 */
                     S321270 ();
                     if ( returnInSub )
                     {
                        pr_default.close(11);
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  AV193J = (short)(AV193J+1) ;
               }
               /* Execute user subroutine: S331298 */
               S331298 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: S341312 */
               S341312 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
            }
            AV22iataC = GXutil.substring( AV8Linha, 14, 7) ;
            AV189Level[2-1] = AV22iataC ;
            if ( ( GXutil.strcmp(AV287LOADE, "N") != 0 ) )
            {
            }
            AV26Id_Age = GXutil.substring( AV8Linha, 28, 4) ;
            AV244AgIat = GXutil.trim( GXutil.substring( AV22iataC, 1, 7)) ;
            AV445GXLvl = (byte)(0) ;
            /* Using cursor P005F16 */
            pr_default.execute(14, new Object[] {AV244AgIat});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A1313CadIA = P005F16_A1313CadIA[0] ;
               A1314CadNo = P005F16_A1314CadNo[0] ;
               n1314CadNo = P005F16_n1314CadNo[0] ;
               AV445GXLvl = (byte)(1) ;
               AV22iataC = A1313CadIA ;
               AV26Id_Age = A1314CadNo ;
               AV188agnom = A1314CadNo ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(14);
            if ( ( AV445GXLvl == 0 ) )
            {
               /*
                  INSERT RECORD ON TABLE CADAGE

               */
               A1313CadIA = GXutil.substring( AV22iataC, 1, 7) ;
               A1314CadNo = GXutil.substring( AV22iataC, 1, 50) ;
               n1314CadNo = false ;
               /* Using cursor P005F17 */
               pr_default.execute(15, new Object[] {A1313CadIA, new Boolean(n1314CadNo), A1314CadNo});
               if ( (pr_default.getStatus(15) == 1) )
               {
                  Gx_err = (short)(1) ;
                  Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
               }
               else
               {
                  Gx_err = (short)(0) ;
                  Gx_emsg = "" ;
               }
               /* End Insert */
            }
            AV186Linha = "" ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKT") == 0 ) )
         {
            AV404Trans = "Y" ;
            AV113NovoT = 1 ;
            AV85Posic = (int)(GXutil.val( GXutil.substring( AV8Linha, 4, 8), ".")) ;
            if ( ( AV85Posic != 4 ) && ( AV88Posian != AV85Posic - 1 ) )
            {
               if ( ( GXutil.strcmp(AV201ciaco, "AACL") != 0 ) )
               {
                  AV240CurrC = GXutil.trim( AV246Trans) ;
                  AV446GXLvl = (byte)(0) ;
                  /* Using cursor P005F18 */
                  pr_default.execute(16, new Object[] {new Long(AV174Tkt2), AV20Period, AV240CurrC});
                  while ( (pr_default.getStatus(16) != 101) )
                  {
                     A966CODE = P005F18_A966CODE[0] ;
                     A965PER_NA = P005F18_A965PER_NA[0] ;
                     A902NUM_BI = P005F18_A902NUM_BI[0] ;
                     n902NUM_BI = P005F18_n902NUM_BI[0] ;
                     A874AIRPT_ = P005F18_A874AIRPT_[0] ;
                     n874AIRPT_ = P005F18_n874AIRPT_[0] ;
                     A963ISOC = P005F18_A963ISOC[0] ;
                     A964CiaCod = P005F18_A964CiaCod[0] ;
                     A967IATA = P005F18_A967IATA[0] ;
                     A968NUM_BI = P005F18_A968NUM_BI[0] ;
                     A969TIPO_V = P005F18_A969TIPO_V[0] ;
                     A970DATA = P005F18_A970DATA[0] ;
                     AV446GXLvl = (byte)(1) ;
                     pr_default.readNext(16);
                  }
                  pr_default.close(16);
                  if ( ( AV446GXLvl == 0 ) )
                  {
                     /* Execute user subroutine: S351388 */
                     S351388 ();
                     if ( returnInSub )
                     {
                        pr_default.close(11);
                        returnInSub = true;
                        if (true) return;
                     }
                  }
               }
               else
               {
                  AV240CurrC = GXutil.trim( AV246Trans) ;
                  AV447GXLvl = (byte)(0) ;
                  /* Using cursor P005F19 */
                  pr_default.execute(17, new Object[] {new Long(AV174Tkt2), AV240CurrC, AV20Period});
                  while ( (pr_default.getStatus(17) != 101) )
                  {
                     A965PER_NA = P005F19_A965PER_NA[0] ;
                     A966CODE = P005F19_A966CODE[0] ;
                     A902NUM_BI = P005F19_A902NUM_BI[0] ;
                     n902NUM_BI = P005F19_n902NUM_BI[0] ;
                     A874AIRPT_ = P005F19_A874AIRPT_[0] ;
                     n874AIRPT_ = P005F19_n874AIRPT_[0] ;
                     A963ISOC = P005F19_A963ISOC[0] ;
                     A964CiaCod = P005F19_A964CiaCod[0] ;
                     A967IATA = P005F19_A967IATA[0] ;
                     A968NUM_BI = P005F19_A968NUM_BI[0] ;
                     A969TIPO_V = P005F19_A969TIPO_V[0] ;
                     A970DATA = P005F19_A970DATA[0] ;
                     AV447GXLvl = (byte)(1) ;
                     pr_default.readNext(17);
                  }
                  pr_default.close(17);
                  if ( ( AV447GXLvl == 0 ) )
                  {
                     /* Execute user subroutine: S351388 */
                     S351388 ();
                     if ( returnInSub )
                     {
                        pr_default.close(11);
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  AV240CurrC = "" ;
               }
            }
            if ( ( AV239AtuSa == 1 ) )
            {
               /* Execute user subroutine: S20625 */
               S20625 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Execute user subroutine: S17527 */
            S17527 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: S15423 */
            S15423 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: S16478 */
            S16478 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: S341312 */
            S341312 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            AV299HOT_F = GXutil.substring( AV8Linha, 49, 1) ;
            AV245RPSI = GXutil.trim( GXutil.substring( AV8Linha, 101, 4)) ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKS") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "24") == 0 ) )
         {
            AV376PNR = GXutil.substring( AV8Linha, 115, 13) ;
            AV43Issue_ = GXutil.substring( AV8Linha, 97, 6) ;
            AV407Partn = GXutil.substring( AV8Linha, 65, 7) ;
            AV406Partn = AV407Partn ;
            AV289CPUI = GXutil.substring( AV8Linha, 58, 4) ;
            AV53yybks = GXutil.substring( AV8Linha, 14, 2) ;
            AV54mmbks = GXutil.substring( AV8Linha, 16, 2) ;
            AV52ddbks = GXutil.substring( AV8Linha, 18, 2) ;
            AV56Data2 = GXutil.concat( AV54mmbks, AV52ddbks, "/") ;
            AV56Data2 = GXutil.concat( AV56Data2, AV53yybks, "/") ;
            AV55Datbks = localUtil.ctod( AV56Data2, 2) ;
            AV347Proce = "Y" ;
            if ( ( GXutil.strcmp(AV347Proce, "Y") == 0 ) )
            {
               if ( ( GXutil.strcmp(AV40Tkt, "") == 0 ) || ( GXutil.strcmp(GXutil.trim( AV40Tkt), "") == 0 ) )
               {
                  AV57Part = GXutil.substring( AV8Linha, 29, 2) ;
                  if ( ( ( GXutil.strcmp(AV57Part, GXutil.trim( AV222Adm_P)) == 0 ) || ( GXutil.strcmp(AV57Part, GXutil.trim( AV221Acm_P)) == 0 ) ) && ( GXutil.strcmp(AV251MemoD, "N") == 0 ) )
                  {
                     AV58Par1 = GXutil.substring( AV8Linha, 30, 1) ;
                     AV59Par2 = GXutil.substring( AV8Linha, 29, 1) ;
                     AV60Par3 = GXutil.substring( AV8Linha, 34, 5) ;
                     AV40Tkt = GXutil.concat( AV58Par1, AV60Par3, AV59Par2) ;
                  }
                  else if ( ( GXutil.strcmp(AV57Part, "00") == 0 ) && ( GXutil.strcmp(AV251MemoD, "N") == 0 ) )
                  {
                     AV40Tkt = GXutil.substring( AV8Linha, 31, 8) ;
                  }
                  else
                  {
                     AV40Tkt = GXutil.substring( AV8Linha, 29, 10) ;
                     AV129TktFi = (long)(GXutil.val( AV40Tkt, ".")) ;
                  }
               }
               else
               {
                  if ( ( GXutil.strcmp(AV288ZumOn, "Y") == 0 ) )
                  {
                     AV40Tkt = GXutil.trim( AV40Tkt) + "/ZUM" ;
                  }
                  AV126TktFi = GXutil.substring( AV8Linha, 29, 10) ;
                  AV129TktFi = (long)(GXutil.val( AV126TktFi, ".")) ;
                  AV127Passo = "S" ;
               }
               AV246Trans = GXutil.substring( AV8Linha, 93, 4) ;
               if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 93, 2), "RF") == 0 ) )
               {
                  AV238Trans = "R" ;
               }
               else if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 93, 2), "AD") == 0 ) )
               {
                  AV238Trans = "D" ;
               }
               else if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 93, 2), "AC") == 0 ) )
               {
                  AV238Trans = "C" ;
               }
               else
               {
                  AV238Trans = "T" ;
               }
               AV174Tkt2 = (long)(GXutil.val( GXutil.substring( AV40Tkt, 1, 10), ".")) ;
               if ( ( AV113NovoT == 1 ) )
               {
                  AV112TktPa = AV40Tkt ;
                  AV113NovoT = 0 ;
               }
               else
               {
                  AV110VendC = "CJ" ;
               }
               AV61Tour_C = GXutil.substring( AV8Linha, 78, 15) ;
               if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV61Tour_C))==0)) )
               {
                  AV61Tour_C = GXutil.substring( AV8Linha, 78, 15) ;
               }
               else
               {
                  AV61Tour_C = "" ;
               }
               AV277INLS = GXutil.substring( AV8Linha, 111, 4) ;
            }
            /* Using cursor P005F20 */
            pr_default.execute(18, new Object[] {AV373ISOC, AV201ciaco, AV20Period, AV246Trans, AV22iataC, AV40Tkt, AV79Pay_ty, AV55Datbks});
            while ( (pr_default.getStatus(18) != 101) )
            {
               A970DATA = P005F20_A970DATA[0] ;
               A969TIPO_V = P005F20_A969TIPO_V[0] ;
               A968NUM_BI = P005F20_A968NUM_BI[0] ;
               A967IATA = P005F20_A967IATA[0] ;
               A966CODE = P005F20_A966CODE[0] ;
               A965PER_NA = P005F20_A965PER_NA[0] ;
               A964CiaCod = P005F20_A964CiaCod[0] ;
               A963ISOC = P005F20_A963ISOC[0] ;
               A942Tax_Am = P005F20_A942Tax_Am[0] ;
               n942Tax_Am = P005F20_n942Tax_Am[0] ;
               A973Tax_Co = P005F20_A973Tax_Co[0] ;
               A974Tax_Wh = P005F20_A974Tax_Wh[0] ;
               A975Tax_Pd = P005F20_A975Tax_Pd[0] ;
               pr_default.readNext(18);
            }
            pr_default.close(18);
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKI") == 0 ) )
         {
            AV93TransI = GXutil.substring( AV8Linha, 20, 6) ;
            AV44I_from = "ERR" ;
            AV45I_to = "ERR" ;
            AV46I_car = "ERR" ;
            AV47I_tar = "ERR" ;
            AV48I_seq = (byte)(0) ;
            AV49I_tkt = "ERR" ;
            AV50Flight = "ERR" ;
            AV51Fli_Da = "ERR" ;
            AV44I_from = GXutil.substring( AV8Linha, 61, 3) ;
            AV45I_to = GXutil.substring( AV8Linha, 66, 3) ;
            AV46I_car = GXutil.substring( AV8Linha, 71, 2) ;
            AV47I_tar = GXutil.substring( AV8Linha, 103, 15) ;
            AV49I_tkt = GXutil.substring( AV8Linha, 29, 10) ;
            AV50Flight = GXutil.concat( "00000", GXutil.trim( GXutil.substring( AV8Linha, 76, 5)), "") ;
            AV51Fli_Da = GXutil.substring( AV8Linha, 83, 5) ;
            AV273Dia = (byte)(GXutil.val( GXutil.substring( AV51Fli_Da, 1, 2), ".")) ;
            AV274mes = (byte)(GXutil.strSearch( "JANFEVMARABRMAIJUNJULAGOSETOUTNOVDEZ", GXutil.substring( AV51Fli_Da, 3, 3), 1)) ;
            if ( ( AV274mes == 0 ) )
            {
               AV274mes = (byte)(GXutil.strSearch( "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC", GXutil.substring( AV51Fli_Da, 3, 3), 1)) ;
            }
            if ( ( AV274mes == 0 ) )
            {
               AV272Fli_D = GXutil.nullDate() ;
            }
            else
            {
               AV274mes = (byte)((AV274mes+2)/ (double) (3)) ;
               if ( ( GXutil.month( AV55Datbks) > AV274mes ) )
               {
                  AV275Ano4 = (short)(GXutil.year( AV55Datbks)+1) ;
               }
               else
               {
                  AV275Ano4 = (short)(GXutil.year( AV55Datbks)) ;
               }
               AV272Fli_D = localUtil.ymdtod( AV275Ano4, AV274mes, AV273Dia) ;
            }
            AV125Class = GXutil.substring( AV8Linha, 81, 2) ;
            AV135PerMT[AV131SeqIt-1] = AV20Period ;
            AV138CodeM[AV131SeqIt-1] = AV22iataC ;
            AV136IataM[AV131SeqIt-1] = AV22iataC ;
            AV148Num_M[AV131SeqIt-1] = AV40Tkt ;
            AV157DataM[AV131SeqIt-1] = AV55Datbks ;
            AV139ISeqM[AV131SeqIt-1] = AV131SeqIt ;
            AV140NumBP[AV131SeqIt-1] = AV129TktFi ;
            AV141I_Fro[AV131SeqIt-1] = AV44I_from ;
            AV142I_ToM[AV131SeqIt-1] = AV45I_to ;
            AV143Carri[AV131SeqIt-1] = AV46I_car ;
            AV144Tarri[AV131SeqIt-1] = AV47I_tar ;
            AV145Class[AV131SeqIt-1] = AV125Class ;
            AV146Fligh[AV131SeqIt-1] = AV50Flight ;
            AV147Fli_M[AV131SeqIt-1] = AV51Fli_Da ;
            AV271Fli_M[AV131SeqIt-1] = AV272Fli_D ;
            AV131SeqIt = (int)(AV131SeqIt+1) ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKS") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "30") == 0 ) )
         {
            AV62Val1 = GXutil.substring( AV8Linha, 42, 10) ;
            AV206PgMoe = GXutil.substring( AV8Linha, 130, 4) ;
            AV63Simbol = GXutil.substring( AV8Linha, 52, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            if ( ( AV69ValxDe != 0 ) )
            {
               AV41Tkt_Am = AV69ValxDe ;
               AV101ValNu = 0 ;
            }
            AV62Val1 = GXutil.substring( AV8Linha, 103, 10) ;
            AV63Simbol = GXutil.substring( AV8Linha, 113, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            if ( ( AV69ValxDe != 0 ) )
            {
               AV28Tax_2 = AV69ValxDe ;
            }
            AV73Pais = GXutil.substring( AV8Linha, 65, 2) ;
            AV89PaisX = GXutil.substring( AV8Linha, 65, 1) ;
            AV62Val1 = GXutil.substring( AV8Linha, 73, 10) ;
            AV63Simbol = GXutil.substring( AV8Linha, 83, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: S371558 */
            S371558 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            if ( ( AV184TaxIn < 1 ) )
            {
               AV184TaxIn = (byte)(1) ;
            }
            if ( ( AV69ValxDe != 0 ) )
            {
               AV183Tax_C[AV184TaxIn-1] = GXutil.substring( AV8Linha, 65, 2) ;
               AV185Tax_a[AV184TaxIn-1] = AV69ValxDe ;
               AV184TaxIn = (byte)(AV184TaxIn+1) ;
               AV29Tax_1 = (double)(AV29Tax_1+AV69ValxDe) ;
               if ( ( GXutil.strcmp(AV89PaisX, "X") == 0 ) )
               {
                  AV32Tax_Xt = (double)(AV32Tax_Xt+AV69ValxDe) ;
               }
               else
               {
                  if ( ( GXutil.strcmp(AV73Pais, "BR") == 0 ) )
                  {
                     AV30Tax_Br = (double)(AV30Tax_Br+AV69ValxDe) ;
                  }
                  else if ( ( GXutil.strcmp(AV73Pais, "US") == 0 ) )
                  {
                     AV33Tax_Us = (double)(AV33Tax_Us+AV69ValxDe) ;
                  }
                  else if ( ( GXutil.strcmp(AV73Pais, "CH") == 0 ) )
                  {
                     AV36Tax_Ch = (double)(AV36Tax_Ch+AV69ValxDe) ;
                  }
                  else if ( ( GXutil.strcmp(AV73Pais, "CP") == 0 ) && ( GXutil.strcmp(AV238Trans, "T") != 0 ) )
                  {
                     AV38Tax_Cp = (double)(AV38Tax_Cp+AV69ValxDe) ;
                  }
                  else
                  {
                     AV37Tax_Xx = (double)(AV37Tax_Xx+AV69ValxDe) ;
                  }
               }
            }
            AV74Pais2 = GXutil.substring( AV8Linha, 84, 2) ;
            AV90Pais2X = GXutil.substring( AV8Linha, 84, 1) ;
            AV62Val1 = GXutil.substring( AV8Linha, 92, 10) ;
            AV63Simbol = GXutil.substring( AV8Linha, 102, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            if ( ( AV69ValxDe != 0 ) )
            {
               AV183Tax_C[AV184TaxIn-1] = GXutil.substring( AV8Linha, 84, 2) ;
               AV185Tax_a[AV184TaxIn-1] = AV69ValxDe ;
               AV184TaxIn = (byte)(AV184TaxIn+1) ;
               AV29Tax_1 = (double)(AV29Tax_1+AV69ValxDe) ;
               if ( ( GXutil.strcmp(AV90Pais2X, "X") == 0 ) )
               {
                  AV32Tax_Xt = (double)(AV32Tax_Xt+AV69ValxDe) ;
               }
               else
               {
                  if ( ( GXutil.strcmp(AV74Pais2, "BR") == 0 ) )
                  {
                  }
                  else if ( ( GXutil.strcmp(AV74Pais2, "DE") == 0 ) )
                  {
                  }
                  else if ( ( GXutil.strcmp(AV74Pais2, "US") == 0 ) )
                  {
                     AV33Tax_Us = (double)(AV33Tax_Us+AV69ValxDe) ;
                  }
                  else if ( ( GXutil.strcmp(AV74Pais2, "CH") == 0 ) )
                  {
                  }
                  else if ( ( GXutil.strcmp(AV74Pais2, "CP") == 0 ) && ( GXutil.strcmp(AV238Trans, "T") != 0 ) )
                  {
                     AV38Tax_Cp = (double)(AV38Tax_Cp+AV69ValxDe) ;
                  }
                  else
                  {
                     AV37Tax_Xx = (double)(AV37Tax_Xx+AV69ValxDe) ;
                  }
               }
            }
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKS") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "39") == 0 ) )
         {
            AV250STAT = GXutil.substring( AV8Linha, 42, 1) ;
            if ( ( GXutil.strcmp(AV250STAT, "I") == 0 ) )
            {
               AV31Tax_De = 0 ;
               AV303wSTAT = AV250STAT ;
            }
            if ( ( GXutil.strcmp(AV303wSTAT, "I") != 0 ) )
            {
               if ( ( GXutil.strcmp(GXutil.trim( AV250STAT), "") == 0 ) )
               {
                  AV31Tax_De = 1 ;
               }
            }
            AV75C_Rate = (double)(GXutil.val( GXutil.substring( AV8Linha, 56, 10), ".")/ (double) (100)) ;
            AV175Rate_ = (double)(GXutil.val( GXutil.substring( AV8Linha, 51, 5), ".")/ (double) (100)) ;
            AV62Val1 = GXutil.substring( AV8Linha, 56, 10) ;
            AV63Simbol = GXutil.substring( AV8Linha, 66, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            AV76Lit_co = AV69ValxDe ;
            AV62Val1 = GXutil.substring( AV8Linha, 78, 10) ;
            AV63Simbol = GXutil.substring( AV8Linha, 88, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            AV77Over_c = AV69ValxDe ;
            AV301EFRT = (double)(GXutil.val( GXutil.substring( AV8Linha, 89, 5), ".")/ (double) (100)) ;
            AV62Val1 = GXutil.substring( AV8Linha, 116, 8) ;
            AV63Simbol = GXutil.substring( AV8Linha, 124, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            AV115over_ = AV69ValxDe ;
            AV99Header = GXutil.concat( AV99Header, " BKS39", "") ;
            if ( ( GXutil.strcmp(AV382TCND, "M") == 0 ) )
            {
               AV395nAmou = (double)(AV395nAmou+(AV76Lit_co+AV77Over_c)) ;
            }
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKS") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "42") == 0 ) )
         {
            AV211IVAAM = 0 ;
            AV62Val1 = GXutil.substring( AV8Linha, 50, 10) ;
            AV63Simbol = GXutil.substring( AV8Linha, 60, 1) ;
            /* Execute user subroutine: S13354 */
            S13354 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            AV211IVAAM = AV69ValxDe ;
            if ( ( GXutil.strcmp(AV321NewIv, "Y") == 0 ) )
            {
               AV322HotTa = (short)(GXutil.strSearch( AV8Linha, "IVA", 1)) ;
               AV323IniVa = (byte)(0) ;
               AV69ValxDe = 0 ;
               AV324IniSi = (byte)(0) ;
               AV323IniVa = (byte)(0) ;
               if ( ( AV322HotTa > 0 ) )
               {
                  if ( ( AV322HotTa < 50 ) )
                  {
                     AV323IniVa = (byte)(50) ;
                     AV324IniSi = (byte)(60) ;
                  }
                  else if ( ( AV322HotTa < 69 ) )
                  {
                     AV323IniVa = (byte)(69) ;
                     AV324IniSi = (byte)(79) ;
                  }
                  else if ( ( AV322HotTa < 88 ) )
                  {
                     AV323IniVa = (byte)(88) ;
                     AV324IniSi = (byte)(98) ;
                  }
                  else if ( ( AV322HotTa < 107 ) )
                  {
                     AV323IniVa = (byte)(107) ;
                     AV324IniSi = (byte)(117) ;
                  }
               }
               AV30Tax_Br = 0 ;
               if ( ( AV323IniVa > 0 ) )
               {
                  AV62Val1 = GXutil.substring( AV8Linha, AV323IniVa, 10) ;
                  AV63Simbol = GXutil.substring( AV8Linha, AV324IniSi, 1) ;
                  /* Execute user subroutine: S13354 */
                  S13354 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV30Tax_Br = AV69ValxDe ;
                  AV196LevEv[1-1][75-1] = (double)(AV196LevEv[1-1][75-1]+AV30Tax_Br) ;
                  AV211IVAAM = AV69ValxDe ;
               }
               else
               {
                  AV30Tax_Br = 0 ;
                  AV211IVAAM = 0 ;
               }
               AV322HotTa = (short)(GXutil.strSearch( AV8Linha, "RIVA", 1)) ;
               AV323IniVa = (byte)(0) ;
               AV69ValxDe = 0 ;
               AV324IniSi = (byte)(0) ;
               AV323IniVa = (byte)(0) ;
               if ( ( AV322HotTa > 0 ) )
               {
                  if ( ( AV322HotTa < 50 ) )
                  {
                     AV323IniVa = (byte)(50) ;
                     AV324IniSi = (byte)(60) ;
                  }
                  else if ( ( AV322HotTa < 69 ) )
                  {
                     AV323IniVa = (byte)(69) ;
                     AV324IniSi = (byte)(79) ;
                  }
                  else if ( ( AV322HotTa < 88 ) )
                  {
                     AV323IniVa = (byte)(88) ;
                     AV324IniSi = (byte)(98) ;
                  }
                  else if ( ( AV322HotTa < 107 ) )
                  {
                     AV323IniVa = (byte)(107) ;
                     AV324IniSi = (byte)(117) ;
                  }
               }
               AV36Tax_Ch = 0 ;
               if ( ( AV323IniVa > 0 ) )
               {
                  AV62Val1 = GXutil.substring( AV8Linha, AV323IniVa, 10) ;
                  AV63Simbol = GXutil.substring( AV8Linha, AV324IniSi, 1) ;
                  /* Execute user subroutine: S13354 */
                  S13354 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV36Tax_Ch = AV69ValxDe ;
                  AV196LevEv[1-1][75-1] = (double)(AV196LevEv[1-1][75-1]+AV36Tax_Ch) ;
                  AV196LevEv[1-1][76-1] = (double)(AV196LevEv[1-1][76-1]+AV36Tax_Ch) ;
               }
               else
               {
                  AV62Val1 = "0000000000" ;
                  AV63Simbol = "{" ;
                  AV36Tax_Ch = 0 ;
               }
               AV322HotTa = (short)(GXutil.strSearch( AV8Linha, "RET", 1)) ;
               AV323IniVa = (byte)(0) ;
               AV69ValxDe = 0 ;
               AV324IniSi = (byte)(0) ;
               AV323IniVa = (byte)(0) ;
               if ( ( AV322HotTa > 0 ) )
               {
                  if ( ( AV322HotTa < 50 ) )
                  {
                     AV323IniVa = (byte)(50) ;
                     AV324IniSi = (byte)(60) ;
                  }
                  else if ( ( AV322HotTa < 69 ) )
                  {
                     AV323IniVa = (byte)(69) ;
                     AV324IniSi = (byte)(79) ;
                  }
                  else if ( ( AV322HotTa < 88 ) )
                  {
                     AV323IniVa = (byte)(88) ;
                     AV324IniSi = (byte)(98) ;
                  }
                  else if ( ( AV322HotTa < 107 ) )
                  {
                     AV323IniVa = (byte)(107) ;
                     AV324IniSi = (byte)(117) ;
                  }
               }
               AV34Tax_It = 0 ;
               if ( ( AV323IniVa > 0 ) )
               {
                  AV62Val1 = GXutil.substring( AV8Linha, AV323IniVa, 10) ;
                  AV63Simbol = GXutil.substring( AV8Linha, AV324IniSi, 1) ;
                  /* Execute user subroutine: S13354 */
                  S13354 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV34Tax_It = AV69ValxDe ;
                  AV196LevEv[1-1][75-1] = (double)(AV196LevEv[1-1][75-1]+AV34Tax_It) ;
                  AV196LevEv[1-1][77-1] = (double)(AV196LevEv[1-1][77-1]+AV34Tax_It) ;
               }
               else
               {
                  AV34Tax_It = 0 ;
               }
               AV322HotTa = (short)(GXutil.strSearch( AV8Linha, "ICA", 1)) ;
               AV323IniVa = (byte)(0) ;
               AV69ValxDe = 0 ;
               AV324IniSi = (byte)(0) ;
               AV323IniVa = (byte)(0) ;
               if ( ( AV322HotTa > 0 ) )
               {
                  if ( ( AV322HotTa < 50 ) )
                  {
                     AV323IniVa = (byte)(50) ;
                     AV324IniSi = (byte)(60) ;
                  }
                  else if ( ( AV322HotTa < 69 ) )
                  {
                     AV323IniVa = (byte)(69) ;
                     AV324IniSi = (byte)(79) ;
                  }
                  else if ( ( AV322HotTa < 88 ) )
                  {
                     AV323IniVa = (byte)(88) ;
                     AV324IniSi = (byte)(98) ;
                  }
                  else if ( ( AV322HotTa < 107 ) )
                  {
                     AV323IniVa = (byte)(107) ;
                     AV324IniSi = (byte)(117) ;
                  }
               }
               AV31Tax_De = 0 ;
               if ( ( AV323IniVa > 0 ) )
               {
                  AV62Val1 = GXutil.substring( AV8Linha, AV323IniVa, 10) ;
                  AV63Simbol = GXutil.substring( AV8Linha, AV324IniSi, 1) ;
                  /* Execute user subroutine: S13354 */
                  S13354 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV31Tax_De = AV69ValxDe ;
                  AV196LevEv[1-1][75-1] = (double)(AV196LevEv[1-1][75-1]+AV31Tax_De) ;
               }
               else
               {
                  AV31Tax_De = 0 ;
               }
            }
            if ( ( GXutil.strcmp(AV382TCND, "M") == 0 ) )
            {
               AV395nAmou = (double)(AV395nAmou+AV211IVAAM) ;
            }
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKS") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "45") == 0 ) )
         {
            AV300IndHO = (byte)(AV300IndHO+1) ;
            if ( ( AV300IndHO <= 30 ) )
            {
               AV265Sub_N[AV300IndHO-1] = GXutil.substring( AV8Linha, 26, 15) ;
               AV266Sub_D[AV300IndHO-1] = "Waiver Code--> " + GXutil.substring( AV8Linha, 42, 14) ;
               AV267Sub_D[AV300IndHO-1] = "Related Ticket/Doc. identifier--> " + GXutil.substring( AV8Linha, 108, 4) ;
               AV268Sub_D[AV300IndHO-1] = "Related Doc.Issue Date(YYMMDD)--> " + GXutil.substring( AV8Linha, 128, 6) ;
            }
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKS") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "46") == 0 ) )
         {
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKP") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "84") == 0 ) )
         {
            AV182paymt = GXutil.substring( AV8Linha, 26, 4) + GXutil.trim( GXutil.substring( AV8Linha, 47, 16)) ;
            AV105Trans = GXutil.substring( AV8Linha, 20, 6) ;
            AV79Pay_ty = GXutil.substring( AV8Linha, 26, 2) ;
            AV80Cc_Typ = GXutil.substring( AV8Linha, 28, 2) ;
            if ( ( GXutil.strcmp(AV80Cc_Typ, "") == 0 ) )
            {
               AV80Cc_Typ = AV79Pay_ty ;
            }
            AV78FomPay = GXutil.val( GXutil.substring( AV8Linha, 36, 10), ".") ;
            if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 30, 2), "GR") == 0 ) )
            {
               AV80Cc_Typ = "GR" ;
               AV215Carta = "GR" ;
            }
            if ( ( GXutil.strcmp(AV79Pay_ty, "CC") == 0 ) || ( GXutil.strcmp(AV79Pay_ty, "TC") == 0 ) )
            {
               if ( ( GXutil.strcmp(GXutil.trim( AV80Cc_Typ), "") == 0 ) )
               {
                  AV80Cc_Typ = GXutil.substring( AV8Linha, 81, 2) ;
               }
               if ( ( ( GXutil.strcmp(AV80Cc_Typ, "CA") == 0 ) || ( GXutil.strcmp(AV80Cc_Typ, "IK") == 0 ) ) )
               {
                  AV215Carta = "MC" ;
               }
               else
               {
                  AV215Carta = AV80Cc_Typ ;
               }
               AV269Barra = GXutil.trim( AV22iataC) + "-Credit Card " + AV215Carta ;
               AV304EXDA = GXutil.substring( AV8Linha, 66, 4) ;
               AV305APLC = GXutil.substring( AV8Linha, 75, 6) ;
            }
            else
            {
               AV304EXDA = "" ;
               AV305APLC = "" ;
            }
            AV81Cccf = GXutil.substring( AV8Linha, 81, 11) ;
            AV114Aux = GXutil.substring( AV8Linha, 26, 2) ;
            if ( ( GXutil.strcmp(AV114Aux, "CA") == 0 ) )
            {
               AV62Val1 = "0000000000" ;
               AV63Simbol = "{" ;
               /* Execute user subroutine: S13354 */
               S13354 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               AV42Due_Ag = AV69ValxDe ;
               AV121Total = AV69ValxDe ;
               AV122PayCA = "CA" ;
               AV123CC_Ty = "" ;
               AV124CccfC = "" ;
               AV62Val1 = GXutil.substring( AV8Linha, 36, 10) ;
               AV63Simbol = GXutil.substring( AV8Linha, 46, 1) ;
               /* Execute user subroutine: S13354 */
               S13354 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               AV118Grupo = "" ;
               AV150TotCa = (double)(AV150TotCa+AV69ValxDe) ;
               AV269Barra = GXutil.trim( AV201ciaco) + "-" + GXutil.trim( AV22iataC) + "-CASH  " ;
            }
            else
            {
               AV325PGAMO = "Y" ;
               if ( ( GXutil.strcmp(AV325PGAMO, "N") == 0 ) )
               {
                  AV62Val1 = GXutil.substring( AV8Linha, 36, 10) ;
                  AV63Simbol = GXutil.substring( AV8Linha, 46, 1) ;
               }
               else
               {
                  AV62Val1 = GXutil.substring( AV8Linha, 101, 10) ;
                  AV63Simbol = GXutil.substring( AV8Linha, 111, 1) ;
               }
               /* Execute user subroutine: S13354 */
               S13354 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
               if ( ( GXutil.strcmp(AV238Trans, "T") != 0 ) )
               {
                  if ( ( GXutil.strcmp(AV238Trans, "R") == 0 ) )
                  {
                     if ( ( GXutil.strcmp(AV302RFDCR, "Y") == 0 ) )
                     {
                        AV108CC_Am = (double)(AV108CC_Am+AV69ValxDe) ;
                     }
                     else
                     {
                        AV62Val1 = GXutil.substring( AV8Linha, 36, 10) ;
                        AV63Simbol = GXutil.substring( AV8Linha, 46, 1) ;
                        /* Execute user subroutine: S13354 */
                        S13354 ();
                        if ( returnInSub )
                        {
                           pr_default.close(11);
                           returnInSub = true;
                           if (true) return;
                        }
                        AV108CC_Am = AV69ValxDe ;
                     }
                  }
                  else
                  {
                     AV108CC_Am = AV28Tax_2 ;
                  }
               }
               else
               {
                  AV108CC_Am = (double)(AV108CC_Am+AV69ValxDe) ;
               }
               AV119Gru1 = GXutil.substring( AV8Linha, 47, 19) ;
               AV120Gru2 = GXutil.substring( AV8Linha, 75, 6) ;
               if ( ( GXutil.strcmp(GXutil.trim( AV119Gru1), "") != 0 ) )
               {
                  AV118Grupo = GXutil.trim( AV119Gru1) + "*" + GXutil.trim( AV120Gru2) ;
               }
               AV121Total = AV69ValxDe ;
               AV151TotCC = (double)(AV151TotCC+AV108CC_Am) ;
            }
            AV114Aux = "" ;
            AV161Per_P[AV172PagSe-1] = AV20Period ;
            AV162CodeP[AV172PagSe-1] = AV22iataC ;
            AV163IataP[AV172PagSe-1] = AV22iataC ;
            AV164NumPa[AV172PagSe-1] = AV40Tkt ;
            AV165DataP[AV172PagSe-1] = AV55Datbks ;
            AV166SeqMt[AV172PagSe-1] = AV132SeqPg ;
            if ( ( GXutil.strcmp(AV122PayCA, "CA") == 0 ) )
            {
               AV173PgTip[AV172PagSe-1] = AV122PayCA ;
               AV168PgCCT[AV172PagSe-1] = AV80Cc_Typ ;
               AV169Pgccc[AV172PagSe-1] = "" ;
            }
            else
            {
               AV173PgTip[AV172PagSe-1] = AV79Pay_ty ;
               AV168PgCCT[AV172PagSe-1] = AV80Cc_Typ ;
               AV169Pgccc[AV172PagSe-1] = AV81Cccf ;
            }
            AV170PgInf[AV172PagSe-1] = AV118Grupo ;
            AV171PgAmo[AV172PagSe-1] = AV121Total ;
            AV172PagSe = (int)(AV172PagSe+1) ;
            AV132SeqPg = (int)(AV132SeqPg+1) ;
            AV121Total = 0 ;
            AV122PayCA = "" ;
            if ( ( GXutil.strcmp(AV382TCND, "W") == 0 ) )
            {
               AV396nAmou = (double)(AV396nAmou+AV69ValxDe) ;
            }
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BAR") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "64") == 0 ) )
         {
            AV228TmpUS = GXutil.substring( AV8Linha, 42, 11) ;
            AV229TmpLo = GXutil.substring( AV8Linha, 54, 11) ;
            AV230X = (byte)(1) ;
            AV177FareU = "" ;
            while ( ( AV230X <= 11 ) )
            {
               if ( ( GXutil.strSearch( ".0123456789", GXutil.substring( AV228TmpUS, AV230X, 1), 1) > 0 ) )
               {
                  AV177FareU = AV177FareU + GXutil.substring( AV228TmpUS, AV230X, 1) ;
               }
               AV230X = (byte)(AV230X+1) ;
            }
            AV230X = (byte)(1) ;
            AV178FareB = "" ;
            while ( ( AV230X <= 11 ) )
            {
               if ( ( GXutil.strSearch( ".0123456789", GXutil.substring( AV229TmpLo, AV230X, 1), 1) > 0 ) )
               {
                  AV178FareB = AV178FareB + GXutil.substring( AV229TmpLo, AV230X, 1) ;
               }
               AV230X = (byte)(AV230X+1) ;
            }
            AV179Valor = GXutil.val( AV177FareU, ".") ;
            AV180Valor = GXutil.val( AV178FareB, ".") ;
            AV176Exc_R = 1 ;
            if ( ( AV179Valor > 0 ) && ( AV180Valor > 0 ) )
            {
               AV176Exc_R = (double)(AV179Valor/ (double) (AV180Valor)) ;
            }
            AV276FCMI = GXutil.substring( AV8Linha, 117, 1) ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BAR") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "65") == 0 ) )
         {
            AV83Pax = GXutil.substring( AV8Linha, 42, 49) ;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BFT") == 0 ) && ( GXutil.strcmp(AV347Proce, "Y") == 0 ) )
         {
            AV240CurrC = GXutil.trim( AV246Trans) ;
            AV449GXLvl = (byte)(0) ;
            /* Using cursor P005F21 */
            pr_default.execute(19, new Object[] {new Long(AV174Tkt2), AV20Period, AV240CurrC});
            while ( (pr_default.getStatus(19) != 101) )
            {
               A966CODE = P005F21_A966CODE[0] ;
               A965PER_NA = P005F21_A965PER_NA[0] ;
               A902NUM_BI = P005F21_A902NUM_BI[0] ;
               n902NUM_BI = P005F21_n902NUM_BI[0] ;
               A874AIRPT_ = P005F21_A874AIRPT_[0] ;
               n874AIRPT_ = P005F21_n874AIRPT_[0] ;
               A963ISOC = P005F21_A963ISOC[0] ;
               A964CiaCod = P005F21_A964CiaCod[0] ;
               A967IATA = P005F21_A967IATA[0] ;
               A968NUM_BI = P005F21_A968NUM_BI[0] ;
               A969TIPO_V = P005F21_A969TIPO_V[0] ;
               A970DATA = P005F21_A970DATA[0] ;
               AV449GXLvl = (byte)(1) ;
               pr_default.readNext(19);
            }
            pr_default.close(19);
            if ( ( AV449GXLvl == 0 ) )
            {
               /* Execute user subroutine: S351388 */
               S351388 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
            }
            if ( ( AV239AtuSa == 1 ) )
            {
               /* Execute user subroutine: S20625 */
               S20625 ();
               if ( returnInSub )
               {
                  pr_default.close(11);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Execute user subroutine: S17527 */
            S17527 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: S15423 */
            S15423 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: S16478 */
            S16478 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            AV192I = (byte)(2) ;
            AV193J = (short)(1) ;
            while ( ( AV193J <= AV220QtdSc ) )
            {
               if ( ( AV239AtuSa == 1 ) )
               {
                  /* Execute user subroutine: S321270 */
                  S321270 ();
                  if ( returnInSub )
                  {
                     pr_default.close(11);
                     returnInSub = true;
                     if (true) return;
                  }
               }
               AV193J = (short)(AV193J+1) ;
            }
            /* Execute user subroutine: S341312 */
            S341312 ();
            if ( returnInSub )
            {
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
         }
         else if ( ( GXutil.strcmp(AV9TLine, "BKF") == 0 ) && ( GXutil.strcmp(AV21TSubLi, "81") == 0 ) )
         {
            AV279FRCAS = (byte)(AV279FRCAS+1) ;
            if ( ( AV279FRCAS <= 5 ) )
            {
               AV278FRCA[AV279FRCAS-1] = GXutil.substring( AV8Linha, 45, 87) ;
            }
         }
         else
         {
            if ( ( GXutil.strSearch( "BAR,BCH,BCT,BFH,BFT,BKF,BKI,BKP,BKS,BKT,BMP,BOH,BOT,-fi", AV9TLine, 1) == 0 ) )
            {
               AV282Error = "Prefix " + AV9TLine + "-line " + GXutil.trim( GXutil.str( AV85Posic, 10, 0)) ;
               pr_default.close(11);
               returnInSub = true;
               if (true) return;
            }
         }
         AV341ACMG_ = AV327ACMG_ ;
         AV342ACMG_ = AV328ACMG_ ;
         if ( ( GXutil.strcmp(GXutil.left( AV335CODEA, 3), "ACM") == 0 ) || ( GXutil.strcmp(GXutil.left( AV335CODEA, 3), "ADM") == 0 ) )
         {
            if ( ( GXutil.strcmp(AV326USE_A, "Y") == 0 ) )
            {
               if ( ( GXutil.strcmp(AV341ACMG_, "") != 0 ) || ( GXutil.strcmp(AV342ACMG_, "") != 0 ) )
               {
                  if ( ( GXutil.strcmp(GXutil.right( GXutil.trim( AV341ACMG_), 1), ",") != 0 ) )
                  {
                     AV341ACMG_ = GXutil.trim( AV341ACMG_) + "," ;
                  }
                  if ( ( GXutil.strcmp(GXutil.right( GXutil.trim( AV342ACMG_), 1), ",") != 0 ) )
                  {
                     AV342ACMG_ = GXutil.trim( AV342ACMG_) + "," ;
                  }
                  AV331Num_T = (byte)(1) ;
                  while ( ( AV331Num_T <= 20 ) )
                  {
                     if ( ( GXutil.len( AV341ACMG_) == 0 ) )
                     {
                        if (true) break;
                     }
                     AV333Posic = (short)(GXutil.strSearch( AV341ACMG_, ",", 1)) ;
                     AV332Tam = (short)(AV333Posic-1) ;
                     AV329Vetor[AV331Num_T-1] = GXutil.substring( AV341ACMG_, 1, AV332Tam) ;
                     AV341ACMG_ = GXutil.strReplace( AV341ACMG_, AV329Vetor[AV331Num_T-1]+",", "") ;
                     AV331Num_T = (byte)(AV331Num_T+1) ;
                  }
                  AV340Num_T = (byte)(1) ;
                  while ( ( AV340Num_T <= 20 ) )
                  {
                     if ( ( GXutil.len( AV342ACMG_) == 0 ) )
                     {
                        if (true) break;
                     }
                     AV333Posic = (short)(GXutil.strSearch( AV342ACMG_, ",", 1)) ;
                     AV332Tam = (short)(AV333Posic-1) ;
                     AV330Vetor[AV340Num_T-1] = GXutil.substring( AV342ACMG_, 1, AV332Tam) ;
                     AV342ACMG_ = GXutil.strReplace( AV342ACMG_, AV330Vetor[AV340Num_T-1]+",", "") ;
                     AV340Num_T = (byte)(AV340Num_T+1) ;
                  }
                  AV332Tam = (short)(1) ;
                  while ( ( AV332Tam <= AV340Num_T - 1 ) )
                  {
                     if ( ( GXutil.strcmp(AV330Vetor[AV332Tam-1], GXutil.left( AV334Num_B, 3)) == 0 ) )
                     {
                        AV333Posic = (short)(1) ;
                        while ( ( AV333Posic <= AV331Num_T - 1 ) )
                        {
                           if ( ( GXutil.strcmp(AV329Vetor[AV333Posic-1], AV336IATAA) == 0 ) )
                           {
                              AV333Posic = (short)(-1) ;
                              if (true) break;
                           }
                           AV333Posic = (short)(AV333Posic+1) ;
                        }
                        if ( ( AV333Posic != -1 ) )
                        {
                           AV333Posic = (short)(-2) ;
                        }
                        if (true) break;
                     }
                     AV332Tam = (short)(AV332Tam+1) ;
                  }
                  if ( ( AV333Posic == -2 ) )
                  {
                  }
                  if ( ( AV333Posic > 0 ) )
                  {
                  }
               }
            }
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 1, 3), "BCH") == 0 ) && ( AV292LineN > 3 ) )
         {
            AV344PerDa = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( AV8Linha, 25, 2), ".")), (int)(GXutil.val( GXutil.substring( AV8Linha, 27, 2), ".")), (int)(GXutil.val( GXutil.substring( AV8Linha, 29, 2), "."))) ;
            /* Using cursor P005F22 */
            pr_default.execute(20, new Object[] {AV344PerDa, AV344PerDa});
            while ( (pr_default.getStatus(20) != 101) )
            {
               A1107PerDa = P005F22_A1107PerDa[0] ;
               A1106PerDa = P005F22_A1106PerDa[0] ;
               A263PerID = P005F22_A263PerID[0] ;
               A23ISOCod = P005F22_A23ISOCod[0] ;
               A349CurCod = P005F22_A349CurCod[0] ;
               A387Period = P005F22_A387Period[0] ;
               if ( ( GXutil.strcmp(AV20Period, A263PerID) == 0 ) )
               {
               }
               else
               {
                  AV20Period = A263PerID ;
                  if ( ( GXutil.strcmp(GXutil.substring( AV8Linha, 24, 1), "D") != 0 ) )
                  {
                  }
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(20);
            }
            pr_default.close(20);
         }
         pr_default.readNext(11);
      }
      pr_default.close(11);
      /* Execute user subroutine: S12347 */
      S12347 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      /* Execute user subroutine: S11341 */
      S11341 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV269Barra = "Saving Summaries" ;
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      if ( ( AV192I <= 10 ) )
      {
         if ( ( GXutil.strcmp(AV287LOADE, "N") != 0 ) )
         {
         }
      }
      else
      {
         AV282Error = "Index " + GXutil.trim( GXutil.str( AV192I, 10, 0)) + " reached" ;
      }
      AV192I = (byte)(1) ;
      AV193J = (short)(1) ;
      while ( ( AV193J <= AV220QtdSc ) )
      {
         if ( ( AV239AtuSa == 1 ) )
         {
            /* Execute user subroutine: S321270 */
            S321270 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         AV193J = (short)(AV193J+1) ;
      }
      /* Execute user subroutine: S391 */
      S391 ();
      if ( returnInSub )
      {
         returnInSub = true;
         if (true) return;
      }
      AV192I = (byte)(3) ;
      while ( ( AV192I <= AV219QtdSc ) )
      {
         if ( ( GXutil.strcmp(AV189Level[AV192I-1], "") != 0 ) )
         {
            if ( ( GXutil.strcmp(AV287LOADE, "N") != 0 ) )
            {
            }
            AV193J = (short)(1) ;
            while ( ( AV193J <= AV220QtdSc ) )
            {
               /* Execute user subroutine: S391 */
               S391 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( ( AV239AtuSa == 1 ) )
               {
                  /* Execute user subroutine: S321270 */
                  S321270 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               AV193J = (short)(AV193J+1) ;
            }
         }
         AV192I = (byte)(AV192I+1) ;
      }
      AV355DataF = GXutil.serverNow( context, remoteHandle, "DEFAULT") ;
   }

   public void S241( )
   {
      /* 'ACUSPCR' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         AV196LevEv[1-1][79-1] = (double)(AV196LevEv[1-1][79-1]+AV41Tkt_Am) ;
         AV196LevEv[2-1][79-1] = (double)(AV196LevEv[2-1][79-1]+AV41Tkt_Am) ;
         AV196LevEv[1-1][80-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[1-1][80-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV196LevEv[2-1][80-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[2-1][80-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV196LevEv[1-1][81-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[1-1][81-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV196LevEv[2-1][81-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[2-1][81-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV196LevEv[1-1][82-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[1-1][82-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
         AV196LevEv[2-1][82-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[2-1][82-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
      }
      else
      {
         AV199LevEv[1-1][79-1] = (double)(AV199LevEv[1-1][79-1]+AV41Tkt_Am) ;
         AV199LevEv[2-1][79-1] = (double)(AV199LevEv[2-1][79-1]+AV41Tkt_Am) ;
         AV199LevEv[1-1][80-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[1-1][80-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV199LevEv[2-1][80-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[2-1][80-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV199LevEv[1-1][81-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[1-1][81-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV199LevEv[2-1][81-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[2-1][81-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV199LevEv[1-1][82-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[1-1][82-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
         AV199LevEv[2-1][82-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[2-1][82-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
      }
   }

   public void S251( )
   {
      /* 'ACUSPDR' Routine */
      if ( ( GXutil.strcmp(GXutil.trim( AV206PgMoe), GXutil.trim( AV207CiaLo)) == 0 ) )
      {
         AV196LevEv[1-1][83-1] = (double)(AV196LevEv[1-1][83-1]+AV41Tkt_Am) ;
         AV196LevEv[2-1][83-1] = (double)(AV196LevEv[2-1][83-1]+AV41Tkt_Am) ;
         AV196LevEv[1-1][84-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[1-1][84-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV196LevEv[2-1][84-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[2-1][84-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV196LevEv[1-1][85-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[1-1][85-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV196LevEv[2-1][85-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[2-1][85-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV196LevEv[1-1][86-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[1-1][86-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
         AV196LevEv[2-1][86-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV196LevEv[2-1][86-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
      }
      else
      {
         AV199LevEv[1-1][83-1] = (double)(AV199LevEv[1-1][83-1]+AV41Tkt_Am) ;
         AV199LevEv[2-1][83-1] = (double)(AV199LevEv[2-1][83-1]+AV41Tkt_Am) ;
         AV199LevEv[1-1][84-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[1-1][84-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV199LevEv[2-1][84-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[2-1][84-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa)), 2))))) ;
         AV199LevEv[1-1][85-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[1-1][85-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV199LevEv[2-1][85-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[2-1][85-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec(((AV41Tkt_Am/ (double) (AV370GovTa))/ (double) (AV369GovTa))*(AV367GovTa/ (double) (100))), 2))))) ;
         AV199LevEv[1-1][86-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[1-1][86-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
         AV199LevEv[2-1][86-1] = (double)(DecimalUtil.decToDouble(DecimalUtil.doubleToDec(AV199LevEv[2-1][86-1]).add((GXutil.roundDecimal( DecimalUtil.doubleToDec((AV41Tkt_Am/ (double) (AV370GovTa))*(AV368GovTa/ (double) (100))), 2))))) ;
      }
   }

   protected void cleanup( )
   {
      this.aP0[0] = ppeginf.this.AV201ciaco;
      this.aP1[0] = ppeginf.this.AV20Period;
      this.aP2[0] = ppeginf.this.AV281Modo;
      this.aP3[0] = ppeginf.this.AV282Error;
      this.aP4[0] = ppeginf.this.AV290VerNu;
      this.aP5[0] = ppeginf.this.AV291First;
      this.aP6[0] = ppeginf.this.AV348Type;
      this.aP7[0] = ppeginf.this.AV378Path;
      Application.commit(context, remoteHandle, "DEFAULT", "ppeginf");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV389Short = "" ;
      AV310ENCCC = "" ;
      AV302RFDCR = "" ;
      AV288ZumOn = "" ;
      AV286LOADH = "" ;
      AV325PGAMO = "" ;
      AV311AccTa = "" ;
      AV365RFND2 = "" ;
      AV345PERDI = "" ;
      returnInSub = false ;
      AV189Level = new String [10] ;
      GX_I = 1 ;
      while ( ( GX_I <= 10 ) )
      {
         AV189Level[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV190LevEv = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV190LevEv[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV320AcctS = new String [37][156] ;
      GX_I = 1 ;
      while ( ( GX_I <= 37 ) )
      {
         GX_J = 1 ;
         while ( ( GX_J <= 156 ) )
         {
            AV320AcctS[GX_I-1][GX_J-1] = "" ;
            GX_J = (int)(GX_J+1) ;
         }
         GX_I = (int)(GX_I+1) ;
      }
      AV193J = (short)(0) ;
      AV312Tax = "" ;
      GX_I = 0 ;
      AV314TaxPo = new byte [11] ;
      AV315TaxCo = new String [11] ;
      GX_I = 1 ;
      while ( ( GX_I <= 11 ) )
      {
         AV315TaxCo[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV317Cmpny = (short)(0) ;
      AV313G = 0 ;
      AV219QtdSc = (byte)(0) ;
      AV220QtdSc = (byte)(0) ;
      AV192I = (byte)(0) ;
      AV196LevEv = new double [37][120] ;
      AV199LevEv = new double [37][120] ;
      AV23Achou = "" ;
      AV8Linha = "" ;
      AV92Primei = "" ;
      AV94Cont = 0 ;
      AV95Cont2 = 0 ;
      AV96Cont3 = 0 ;
      AV97Con4 = 0 ;
      AV101ValNu = 0 ;
      AV127Passo = "" ;
      AV131SeqIt = 0 ;
      AV132SeqPg = 0 ;
      AV172PagSe = 0 ;
      AV104TLinA = "" ;
      AV150TotCa = 0 ;
      AV151TotCC = 0 ;
      AV153ContP = (short)(0) ;
      AV186Linha = "" ;
      AV215Carta = "" ;
      AV270msg1 = "" ;
      AV184TaxIn = (byte)(0) ;
      AV155CProc = (short)(0) ;
      AV64Valcon = 0 ;
      AV65Val2 = "" ;
      AV66Sinal = (byte)(0) ;
      AV63Simbol = "" ;
      AV68Valx = "" ;
      AV62Val1 = "" ;
      AV212Divis = 0 ;
      AV69ValxDe = 0 ;
      AV149Cit = 0 ;
      AV159ContU = 0 ;
      AV158Ultim = "" ;
      AV156Tipo_ = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV156Tipo_[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GX_INS213 = 0 ;
      A963ISOC = "" ;
      AV373ISOC = "" ;
      A965PER_NA = "" ;
      AV135PerMT = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV135PerMT[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      A966CODE = "" ;
      AV246Trans = "" ;
      A967IATA = "" ;
      AV136IataM = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV136IataM[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV148Num_M = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV148Num_M[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      A968NUM_BI = "" ;
      AV40Tkt = "" ;
      AV231TempB = "" ;
      A969TIPO_V = "" ;
      AV79Pay_ty = "" ;
      A970DATA = GXutil.nullDate() ;
      AV157DataM = new java.util.Date [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV157DataM[GX_I-1] = GXutil.nullDate() ;
         GX_I = (int)(GX_I+1) ;
      }
      A971ISeq = 0 ;
      AV139ISeqM = new int [50] ;
      A922Num_Bi = 0 ;
      AV140NumBP = new long [50] ;
      n922Num_Bi = false ;
      A923I_From = "" ;
      AV141I_Fro = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV141I_Fro[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n923I_From = false ;
      A924I_To = "" ;
      AV142I_ToM = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV142I_ToM[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n924I_To = false ;
      A925Carrie = "" ;
      AV143Carri = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV143Carri[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n925Carrie = false ;
      A926Tariff = "" ;
      AV144Tarri = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV144Tarri[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n926Tariff = false ;
      A928Claxss = "" ;
      AV145Class = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV145Class[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n928Claxss = false ;
      A927Flight = "" ;
      AV146Fligh = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV146Fligh[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n927Flight = false ;
      A929Fli_Da = "" ;
      AV147Fli_M = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV147Fli_M[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n929Fli_Da = false ;
      A930Fli_Da = GXutil.nullDate() ;
      AV271Fli_M = new java.util.Date [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV271Fli_M[GX_I-1] = GXutil.nullDate() ;
         GX_I = (int)(GX_I+1) ;
      }
      n930Fli_Da = false ;
      A964CiaCod = "" ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV160CPag = 0 ;
      GX_INS214 = 0 ;
      AV161Per_P = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV161Per_P[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV163IataP = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV163IataP[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV164NumPa = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV164NumPa[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV165DataP = new java.util.Date [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV165DataP[GX_I-1] = GXutil.nullDate() ;
         GX_I = (int)(GX_I+1) ;
      }
      A972SeqPag = 0 ;
      AV166SeqMt = new int [50] ;
      A936PGTIP_ = "" ;
      AV173PgTip = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV173PgTip[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n936PGTIP_ = false ;
      A933PGCC_T = "" ;
      AV168PgCCT = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV168PgCCT[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n933PGCC_T = false ;
      A934PGCCCF = "" ;
      AV169Pgccc = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV169Pgccc[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n934PGCCCF = false ;
      A937PGINFO = "" ;
      AV170PgInf = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV170PgInf[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      n937PGINFO = false ;
      A1073PGCCN = "" ;
      n1073PGCCN = false ;
      A1074PGCCM = "" ;
      n1074PGCCM = false ;
      AV309PGInf = "" ;
      GXv_char3 = new String [1] ;
      AV307PGCCN = "" ;
      GXv_char2 = new String [1] ;
      AV308PGCCM = "" ;
      GXv_char4 = new String [1] ;
      A935PGAMOU = 0 ;
      AV171PgAmo = new double [50] ;
      n935PGAMOU = false ;
      A939PGMOED = "" ;
      AV206PgMoe = "" ;
      n939PGMOED = false ;
      A941EXDA = "" ;
      AV304EXDA = "" ;
      n941EXDA = false ;
      A940APLC = "" ;
      AV305APLC = "" ;
      n940APLC = false ;
      AV183Tax_C = new String [500] ;
      GX_I = 1 ;
      while ( ( GX_I <= 500 ) )
      {
         AV183Tax_C[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV319Found = "" ;
      AV316Cmpny = (short)(0) ;
      AV318Level = (short)(0) ;
      AV207CiaLo = "" ;
      AV185Tax_a = new double [50] ;
      AV42Due_Ag = 0 ;
      AV226ADM_p = "" ;
      AV227ACM_p = "" ;
      AV238Trans = "" ;
      AV41Tkt_Am = 0 ;
      AV211IVAAM = 0 ;
      AV76Lit_co = 0 ;
      AV77Over_c = 0 ;
      AV108CC_Am = 0 ;
      AV38Tax_Cp = 0 ;
      AV225BSPIa = "" ;
      AV22iataC = "" ;
      AV252TipoM = "" ;
      AV295TADDL = (byte)(0) ;
      AV296TADDN = "" ;
      AV248GRlen = 0 ;
      AV247GRNum = "" ;
      AV297TADCL = (byte)(0) ;
      AV298TADCN = "" ;
      AV218C_Pos = (byte)(0) ;
      AV216CC_No = "" ;
      AV217c = (byte)(0) ;
      AV115over_ = 0 ;
      AV214LocTa = "" ;
      AV203SalCo = "" ;
      AV202ASCod = "" ;
      AV204SalAm = 0 ;
      AV205SalAm = 0 ;
      AV287LOADE = "" ;
      AV213Curre = 0 ;
      AV29Tax_1 = 0 ;
      AV28Tax_2 = 0 ;
      AV30Tax_Br = 0 ;
      AV31Tax_De = 0 ;
      AV32Tax_Xt = 0 ;
      AV33Tax_Us = 0 ;
      AV34Tax_It = 0 ;
      AV35Tax_Ot = 0 ;
      AV36Tax_Ch = 0 ;
      AV37Tax_Xx = 0 ;
      AV116Tax2A = 0 ;
      AV109Trans = "" ;
      AV39Trans_ = "" ;
      AV174Tkt2 = 0 ;
      AV112TktPa = "" ;
      AV126TktFi = "" ;
      AV129TktFi = 0 ;
      AV43Issue_ = "" ;
      AV118Grupo = "" ;
      AV250STAT = "" ;
      AV303wSTAT = "" ;
      AV133FezAt = "" ;
      AV299HOT_F = "" ;
      AV245RPSI = "" ;
      AV110VendC = "" ;
      AV107Cash_ = 0 ;
      AV175Rate_ = 0 ;
      AV176Exc_R = 0 ;
      AV138CodeM = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV138CodeM[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV276FCMI = "" ;
      AV277INLS = "" ;
      AV278FRCA = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV278FRCA[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV279FRCAS = (byte)(0) ;
      AV289CPUI = "" ;
      AV300IndHO = (byte)(0) ;
      AV265Sub_N = new String [30] ;
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV265Sub_N[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV266Sub_D = new String [30] ;
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV266Sub_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV267Sub_D = new String [30] ;
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV267Sub_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV268Sub_D = new String [30] ;
      GX_I = 1 ;
      while ( ( GX_I <= 30 ) )
      {
         AV268Sub_D[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV301EFRT = 0 ;
      AV354statu = "" ;
      GX_INS212 = 0 ;
      AV338Per_N = "" ;
      A918CODE2 = "" ;
      n918CODE2 = false ;
      AV335CODEA = "" ;
      AV336IATAA = "" ;
      AV334Num_B = "" ;
      A900MOEDA = "" ;
      n900MOEDA = false ;
      A869ID_AGE = "" ;
      AV26Id_Age = "" ;
      n869ID_AGE = false ;
      AV55Datbks = GXutil.nullDate() ;
      AV339DataA = GXutil.nullDate() ;
      A877TOUR_C = "" ;
      n877TOUR_C = false ;
      A872LIT_CO = 0 ;
      n872LIT_CO = false ;
      A873OVER_C = 0 ;
      n873OVER_C = false ;
      A888OVER_I = 0 ;
      n888OVER_I = false ;
      A890ADM_AC = "" ;
      n890ADM_AC = false ;
      AV337Tipo_ = "" ;
      A870VALOR_ = 0 ;
      n870VALOR_ = false ;
      A878PAX_NA = "" ;
      AV83Pax = "" ;
      n878PAX_NA = false ;
      AV102Var1 = 0 ;
      AV100Var = 0 ;
      A871COMMIS = 0 ;
      n871COMMIS = false ;
      A894PLP_DO = "" ;
      n894PLP_DO = false ;
      A897NUMBER = "" ;
      AV376PNR = "" ;
      n897NUMBER = false ;
      A874AIRPT_ = 0 ;
      n874AIRPT_ = false ;
      A875TAX_1 = 0 ;
      n875TAX_1 = false ;
      A876TAX_2 = 0 ;
      n876TAX_2 = false ;
      A879TAX_BR = 0 ;
      n879TAX_BR = false ;
      A880TAX_DE = 0 ;
      n880TAX_DE = false ;
      A881TAX_XT = 0 ;
      n881TAX_XT = false ;
      A882TAX_US = 0 ;
      n882TAX_US = false ;
      AV321NewIv = "" ;
      A883TAX_IT = 0 ;
      n883TAX_IT = false ;
      A884TAX_OT = 0 ;
      n884TAX_OT = false ;
      A885TAX_CH = 0 ;
      n885TAX_CH = false ;
      A886TAX_XX = 0 ;
      n886TAX_XX = false ;
      A887TAX_CP = 0 ;
      n887TAX_CP = false ;
      A889TRANS_ = "" ;
      n889TRANS_ = false ;
      A891CC_TYP = "" ;
      AV80Cc_Typ = "" ;
      n891CC_TYP = false ;
      A909HOT_FO = "" ;
      n909HOT_FO = false ;
      AV87Numero = "" ;
      AV61Tour_C = "" ;
      A893DUE_AG = 0 ;
      n893DUE_AG = false ;
      A898ISSUE_ = "" ;
      n898ISSUE_ = false ;
      A904IVAAmo = 0 ;
      n904IVAAmo = false ;
      A899PAYMT_ = "" ;
      n899PAYMT_ = false ;
      AV182paymt = "" ;
      A902NUM_BI = 0 ;
      n902NUM_BI = false ;
      AV181NUM_B = 0 ;
      AV210Num_b = 0 ;
      A905FCMI = "" ;
      n905FCMI = false ;
      A906INLS = "" ;
      n906INLS = false ;
      A907CPUI = "" ;
      n907CPUI = false ;
      A908Loader = "" ;
      n908Loader = false ;
      A910EFRT = 0 ;
      n910EFRT = false ;
      AV404Trans = "" ;
      A913TCNR = "" ;
      n913TCNR = false ;
      GX_INS218 = 0 ;
      A978IFCode = "" ;
      A952IFStat = "" ;
      n952IFStat = false ;
      A953IFDate = GXutil.resetTime( GXutil.nullDate() );
      n953IFDate = false ;
      A954IFDeta = "" ;
      n954IFDeta = false ;
      AV357Total = 0 ;
      GX_INS217 = 0 ;
      A977FRCASe = (byte)(0) ;
      A951FRCA = "" ;
      n951FRCA = false ;
      GX_INS215 = 0 ;
      A973Tax_Co = "" ;
      AV73Pais = "" ;
      A942Tax_Am = 0 ;
      n942Tax_Am = false ;
      A974Tax_Wh = (byte)(0) ;
      A975Tax_Pd = "" ;
      AV306H4Num = 0 ;
      GX_INS216 = 0 ;
      A976Sub_Nu = "" ;
      A948Sub_De = "" ;
      n948Sub_De = false ;
      A949Sub_De = "" ;
      n949Sub_De = false ;
      A950Sub_De = "" ;
      n950Sub_De = false ;
      scmdbuf = "" ;
      P005F10_A963ISOC = new String[] {""} ;
      P005F10_A964CiaCod = new String[] {""} ;
      P005F10_A965PER_NA = new String[] {""} ;
      P005F10_A966CODE = new String[] {""} ;
      P005F10_A967IATA = new String[] {""} ;
      P005F10_A968NUM_BI = new String[] {""} ;
      P005F10_A969TIPO_V = new String[] {""} ;
      P005F10_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005F10_A976Sub_Nu = new String[] {""} ;
      P005F10_A948Sub_De = new String[] {""} ;
      P005F10_n948Sub_De = new boolean[] {false} ;
      P005F10_A949Sub_De = new String[] {""} ;
      P005F10_n949Sub_De = new boolean[] {false} ;
      P005F10_A950Sub_De = new String[] {""} ;
      P005F10_n950Sub_De = new boolean[] {false} ;
      AV269Barra = "" ;
      P005F12_A349CurCod = new String[] {""} ;
      P005F12_A23ISOCod = new String[] {""} ;
      A349CurCod = "" ;
      A23ISOCod = "" ;
      AV209YearP = "" ;
      AV221Acm_P = "" ;
      AV222Adm_P = "" ;
      AV293ACMPr = (byte)(0) ;
      AV294ADMPr = (byte)(0) ;
      AV224TADSL = 0 ;
      AV223TADSN = "" ;
      AV251MemoD = "" ;
      AV239AtuSa = 0 ;
      AV263Lista = "" ;
      AV253TADSL = "" ;
      AV262FlagA = (short)(0) ;
      AV254GRLis = "" ;
      AV255ListL = (short)(0) ;
      AV259Palav = "" ;
      AV256Pos = (short)(0) ;
      AV257letra = "" ;
      AV258Check = "" ;
      AV260LenCh = (short)(0) ;
      AV261mNum_ = "" ;
      AV347Proce = "" ;
      P005F13_A997HntSeq = new String[] {""} ;
      P005F13_A994HntLin = new String[] {""} ;
      P005F13_n994HntLin = new boolean[] {false} ;
      P005F13_A998HntSub = new byte[1] ;
      A997HntSeq = "" ;
      A994HntLin = "" ;
      n994HntLin = false ;
      A998HntSub = (byte)(0) ;
      AV9TLine = "" ;
      AV21TSubLi = "" ;
      AV10DtFH = "" ;
      AV11TmFH = "" ;
      AV379DATAC = "" ;
      AV380DATA_ = GXutil.nullDate() ;
      AV394Data_ = GXutil.nullDate() ;
      AV12yy = "" ;
      AV24y1 = "" ;
      AV25y2 = "" ;
      AV14mm = "" ;
      AV15dd = "" ;
      AV17Data1 = "" ;
      AV16Per_Da = GXutil.nullDate() ;
      AV18Data_I = GXutil.nullDate() ;
      AV19ddval = (byte)(0) ;
      AV85Posic = 0 ;
      AV88Posian = 0 ;
      AV240CurrC = "" ;
      AV443GXLvl = (byte)(0) ;
      P005F14_A966CODE = new String[] {""} ;
      P005F14_A965PER_NA = new String[] {""} ;
      P005F14_A902NUM_BI = new long[1] ;
      P005F14_n902NUM_BI = new boolean[] {false} ;
      P005F14_A874AIRPT_ = new double[1] ;
      P005F14_n874AIRPT_ = new boolean[] {false} ;
      P005F14_A963ISOC = new String[] {""} ;
      P005F14_A964CiaCod = new String[] {""} ;
      P005F14_A967IATA = new String[] {""} ;
      P005F14_A968NUM_BI = new String[] {""} ;
      P005F14_A969TIPO_V = new String[] {""} ;
      P005F14_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      AV444GXLvl = (byte)(0) ;
      P005F15_A965PER_NA = new String[] {""} ;
      P005F15_A966CODE = new String[] {""} ;
      P005F15_A902NUM_BI = new long[1] ;
      P005F15_n902NUM_BI = new boolean[] {false} ;
      P005F15_A874AIRPT_ = new double[1] ;
      P005F15_n874AIRPT_ = new boolean[] {false} ;
      P005F15_A963ISOC = new String[] {""} ;
      P005F15_A964CiaCod = new String[] {""} ;
      P005F15_A967IATA = new String[] {""} ;
      P005F15_A968NUM_BI = new String[] {""} ;
      P005F15_A969TIPO_V = new String[] {""} ;
      P005F15_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      AV244AgIat = "" ;
      AV445GXLvl = (byte)(0) ;
      P005F16_A1313CadIA = new String[] {""} ;
      P005F16_A1314CadNo = new String[] {""} ;
      P005F16_n1314CadNo = new boolean[] {false} ;
      A1313CadIA = "" ;
      A1314CadNo = "" ;
      n1314CadNo = false ;
      AV188agnom = "" ;
      GX_INS251 = 0 ;
      AV113NovoT = 0 ;
      AV446GXLvl = (byte)(0) ;
      P005F18_A966CODE = new String[] {""} ;
      P005F18_A965PER_NA = new String[] {""} ;
      P005F18_A902NUM_BI = new long[1] ;
      P005F18_n902NUM_BI = new boolean[] {false} ;
      P005F18_A874AIRPT_ = new double[1] ;
      P005F18_n874AIRPT_ = new boolean[] {false} ;
      P005F18_A963ISOC = new String[] {""} ;
      P005F18_A964CiaCod = new String[] {""} ;
      P005F18_A967IATA = new String[] {""} ;
      P005F18_A968NUM_BI = new String[] {""} ;
      P005F18_A969TIPO_V = new String[] {""} ;
      P005F18_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      AV447GXLvl = (byte)(0) ;
      P005F19_A965PER_NA = new String[] {""} ;
      P005F19_A966CODE = new String[] {""} ;
      P005F19_A902NUM_BI = new long[1] ;
      P005F19_n902NUM_BI = new boolean[] {false} ;
      P005F19_A874AIRPT_ = new double[1] ;
      P005F19_n874AIRPT_ = new boolean[] {false} ;
      P005F19_A963ISOC = new String[] {""} ;
      P005F19_A964CiaCod = new String[] {""} ;
      P005F19_A967IATA = new String[] {""} ;
      P005F19_A968NUM_BI = new String[] {""} ;
      P005F19_A969TIPO_V = new String[] {""} ;
      P005F19_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      AV407Partn = "" ;
      AV406Partn = "" ;
      AV53yybks = "" ;
      AV54mmbks = "" ;
      AV52ddbks = "" ;
      AV56Data2 = "" ;
      AV57Part = "" ;
      AV58Par1 = "" ;
      AV59Par2 = "" ;
      AV60Par3 = "" ;
      P005F20_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      P005F20_A969TIPO_V = new String[] {""} ;
      P005F20_A968NUM_BI = new String[] {""} ;
      P005F20_A967IATA = new String[] {""} ;
      P005F20_A966CODE = new String[] {""} ;
      P005F20_A965PER_NA = new String[] {""} ;
      P005F20_A964CiaCod = new String[] {""} ;
      P005F20_A963ISOC = new String[] {""} ;
      P005F20_A942Tax_Am = new double[1] ;
      P005F20_n942Tax_Am = new boolean[] {false} ;
      P005F20_A973Tax_Co = new String[] {""} ;
      P005F20_A974Tax_Wh = new byte[1] ;
      P005F20_A975Tax_Pd = new String[] {""} ;
      AV93TransI = "" ;
      AV44I_from = "" ;
      AV45I_to = "" ;
      AV46I_car = "" ;
      AV47I_tar = "" ;
      AV48I_seq = (byte)(0) ;
      AV49I_tkt = "" ;
      AV50Flight = "" ;
      AV51Fli_Da = "" ;
      AV273Dia = (byte)(0) ;
      AV274mes = (byte)(0) ;
      AV272Fli_D = GXutil.nullDate() ;
      AV275Ano4 = (short)(0) ;
      AV125Class = "" ;
      AV89PaisX = "" ;
      AV74Pais2 = "" ;
      AV90Pais2X = "" ;
      AV75C_Rate = 0 ;
      AV99Header = "" ;
      AV382TCND = "" ;
      AV395nAmou = 0 ;
      AV322HotTa = (short)(0) ;
      AV323IniVa = (byte)(0) ;
      AV324IniSi = (byte)(0) ;
      AV105Trans = "" ;
      AV78FomPay = 0 ;
      AV81Cccf = "" ;
      AV114Aux = "" ;
      AV121Total = 0 ;
      AV122PayCA = "" ;
      AV123CC_Ty = "" ;
      AV124CccfC = "" ;
      AV119Gru1 = "" ;
      AV120Gru2 = "" ;
      AV162CodeP = new String [50] ;
      GX_I = 1 ;
      while ( ( GX_I <= 50 ) )
      {
         AV162CodeP[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV396nAmou = 0 ;
      AV228TmpUS = "" ;
      AV229TmpLo = "" ;
      AV230X = (byte)(0) ;
      AV177FareU = "" ;
      AV178FareB = "" ;
      AV179Valor = 0 ;
      AV180Valor = 0 ;
      AV449GXLvl = (byte)(0) ;
      P005F21_A966CODE = new String[] {""} ;
      P005F21_A965PER_NA = new String[] {""} ;
      P005F21_A902NUM_BI = new long[1] ;
      P005F21_n902NUM_BI = new boolean[] {false} ;
      P005F21_A874AIRPT_ = new double[1] ;
      P005F21_n874AIRPT_ = new boolean[] {false} ;
      P005F21_A963ISOC = new String[] {""} ;
      P005F21_A964CiaCod = new String[] {""} ;
      P005F21_A967IATA = new String[] {""} ;
      P005F21_A968NUM_BI = new String[] {""} ;
      P005F21_A969TIPO_V = new String[] {""} ;
      P005F21_A970DATA = new java.util.Date[] {GXutil.nullDate()} ;
      AV341ACMG_ = "" ;
      AV327ACMG_ = "" ;
      AV342ACMG_ = "" ;
      AV328ACMG_ = "" ;
      AV326USE_A = "" ;
      AV331Num_T = (byte)(0) ;
      AV333Posic = (short)(0) ;
      AV332Tam = (short)(0) ;
      AV329Vetor = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV329Vetor[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV340Num_T = (byte)(0) ;
      AV330Vetor = new String [20] ;
      GX_I = 1 ;
      while ( ( GX_I <= 20 ) )
      {
         AV330Vetor[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      GXt_char1 = "" ;
      AV292LineN = 0 ;
      AV344PerDa = GXutil.nullDate() ;
      P005F22_A1107PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      P005F22_A1106PerDa = new java.util.Date[] {GXutil.nullDate()} ;
      P005F22_A263PerID = new String[] {""} ;
      P005F22_A23ISOCod = new String[] {""} ;
      P005F22_A349CurCod = new String[] {""} ;
      P005F22_A387Period = new String[] {""} ;
      A1107PerDa = GXutil.nullDate() ;
      A1106PerDa = GXutil.nullDate() ;
      A263PerID = "" ;
      A387Period = "" ;
      AV355DataF = GXutil.resetTime( GXutil.nullDate() );
      AV370GovTa = 0 ;
      AV369GovTa = 0 ;
      AV367GovTa = 0 ;
      AV368GovTa = 0 ;
      GX_J = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new ppeginf__default(),
         new Object[] {
             new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P005F10_A963ISOC, P005F10_A964CiaCod, P005F10_A965PER_NA, P005F10_A966CODE, P005F10_A967IATA, P005F10_A968NUM_BI, P005F10_A969TIPO_V, P005F10_A970DATA, P005F10_A976Sub_Nu, P005F10_A948Sub_De,
            P005F10_n948Sub_De, P005F10_A949Sub_De, P005F10_n949Sub_De, P005F10_A950Sub_De, P005F10_n950Sub_De
            }
            , new Object[] {
            }
            , new Object[] {
            P005F12_A349CurCod, P005F12_A23ISOCod
            }
            , new Object[] {
            P005F13_A997HntSeq, P005F13_A994HntLin, P005F13_n994HntLin, P005F13_A998HntSub
            }
            , new Object[] {
            P005F14_A966CODE, P005F14_A965PER_NA, P005F14_A902NUM_BI, P005F14_n902NUM_BI, P005F14_A874AIRPT_, P005F14_n874AIRPT_, P005F14_A963ISOC, P005F14_A964CiaCod, P005F14_A967IATA, P005F14_A968NUM_BI,
            P005F14_A969TIPO_V, P005F14_A970DATA
            }
            , new Object[] {
            P005F15_A965PER_NA, P005F15_A966CODE, P005F15_A902NUM_BI, P005F15_n902NUM_BI, P005F15_A874AIRPT_, P005F15_n874AIRPT_, P005F15_A963ISOC, P005F15_A964CiaCod, P005F15_A967IATA, P005F15_A968NUM_BI,
            P005F15_A969TIPO_V, P005F15_A970DATA
            }
            , new Object[] {
            P005F16_A1313CadIA, P005F16_A1314CadNo, P005F16_n1314CadNo
            }
            , new Object[] {
            }
            , new Object[] {
            P005F18_A966CODE, P005F18_A965PER_NA, P005F18_A902NUM_BI, P005F18_n902NUM_BI, P005F18_A874AIRPT_, P005F18_n874AIRPT_, P005F18_A963ISOC, P005F18_A964CiaCod, P005F18_A967IATA, P005F18_A968NUM_BI,
            P005F18_A969TIPO_V, P005F18_A970DATA
            }
            , new Object[] {
            P005F19_A965PER_NA, P005F19_A966CODE, P005F19_A902NUM_BI, P005F19_n902NUM_BI, P005F19_A874AIRPT_, P005F19_n874AIRPT_, P005F19_A963ISOC, P005F19_A964CiaCod, P005F19_A967IATA, P005F19_A968NUM_BI,
            P005F19_A969TIPO_V, P005F19_A970DATA
            }
            , new Object[] {
            P005F20_A970DATA, P005F20_A969TIPO_V, P005F20_A968NUM_BI, P005F20_A967IATA, P005F20_A966CODE, P005F20_A965PER_NA, P005F20_A964CiaCod, P005F20_A963ISOC, P005F20_A942Tax_Am, P005F20_n942Tax_Am,
            P005F20_A973Tax_Co, P005F20_A974Tax_Wh, P005F20_A975Tax_Pd
            }
            , new Object[] {
            P005F21_A966CODE, P005F21_A965PER_NA, P005F21_A902NUM_BI, P005F21_n902NUM_BI, P005F21_A874AIRPT_, P005F21_n874AIRPT_, P005F21_A963ISOC, P005F21_A964CiaCod, P005F21_A967IATA, P005F21_A968NUM_BI,
            P005F21_A969TIPO_V, P005F21_A970DATA
            }
            , new Object[] {
            P005F22_A1107PerDa, P005F22_A1106PerDa, P005F22_A263PerID, P005F22_A23ISOCod, P005F22_A349CurCod, P005F22_A387Period
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV314TaxPo[] ;
   private byte AV219QtdSc ;
   private byte AV220QtdSc ;
   private byte AV192I ;
   private byte AV184TaxIn ;
   private byte AV66Sinal ;
   private byte AV295TADDL ;
   private byte AV297TADCL ;
   private byte AV218C_Pos ;
   private byte AV217c ;
   private byte AV279FRCAS ;
   private byte AV300IndHO ;
   private byte A977FRCASe ;
   private byte A974Tax_Wh ;
   private byte AV293ACMPr ;
   private byte AV294ADMPr ;
   private byte A998HntSub ;
   private byte AV19ddval ;
   private byte AV443GXLvl ;
   private byte AV444GXLvl ;
   private byte AV445GXLvl ;
   private byte AV446GXLvl ;
   private byte AV447GXLvl ;
   private byte AV48I_seq ;
   private byte AV273Dia ;
   private byte AV274mes ;
   private byte AV323IniVa ;
   private byte AV324IniSi ;
   private byte AV230X ;
   private byte AV449GXLvl ;
   private byte AV331Num_T ;
   private byte AV340Num_T ;
   private short AV193J ;
   private short AV317Cmpny ;
   private short AV153ContP ;
   private short AV155CProc ;
   private short Gx_err ;
   private short AV316Cmpny ;
   private short AV318Level ;
   private short AV262FlagA ;
   private short AV255ListL ;
   private short AV256Pos ;
   private short AV260LenCh ;
   private short AV275Ano4 ;
   private short AV322HotTa ;
   private short AV333Posic ;
   private short AV332Tam ;
   private int AV291First ;
   private int GX_I ;
   private int AV131SeqIt ;
   private int AV132SeqPg ;
   private int AV172PagSe ;
   private int AV212Divis ;
   private int AV149Cit ;
   private int AV159ContU ;
   private int GX_INS213 ;
   private int A971ISeq ;
   private int AV139ISeqM[] ;
   private int AV160CPag ;
   private int GX_INS214 ;
   private int A972SeqPag ;
   private int AV166SeqMt[] ;
   private int GX_INS212 ;
   private int GX_INS218 ;
   private int AV357Total ;
   private int GX_INS217 ;
   private int GX_INS215 ;
   private int GX_INS216 ;
   private int AV85Posic ;
   private int AV88Posian ;
   private int GX_INS251 ;
   private int AV113NovoT ;
   private int AV292LineN ;
   private int GX_J ;
   private long AV313G ;
   private long AV94Cont ;
   private long AV95Cont2 ;
   private long AV96Cont3 ;
   private long AV97Con4 ;
   private long AV101ValNu ;
   private long A922Num_Bi ;
   private long AV140NumBP[] ;
   private long AV174Tkt2 ;
   private long AV129TktFi ;
   private long A902NUM_BI ;
   private long AV181NUM_B ;
   private long AV210Num_b ;
   private long AV306H4Num ;
   private double AV196LevEv[][] ;
   private double AV199LevEv[][] ;
   private double AV150TotCa ;
   private double AV151TotCC ;
   private double AV64Valcon ;
   private double AV69ValxDe ;
   private double A935PGAMOU ;
   private double AV171PgAmo[] ;
   private double AV185Tax_a[] ;
   private double AV42Due_Ag ;
   private double AV41Tkt_Am ;
   private double AV211IVAAM ;
   private double AV76Lit_co ;
   private double AV77Over_c ;
   private double AV108CC_Am ;
   private double AV38Tax_Cp ;
   private double AV248GRlen ;
   private double AV115over_ ;
   private double AV204SalAm ;
   private double AV205SalAm ;
   private double AV213Curre ;
   private double AV29Tax_1 ;
   private double AV28Tax_2 ;
   private double AV30Tax_Br ;
   private double AV31Tax_De ;
   private double AV32Tax_Xt ;
   private double AV33Tax_Us ;
   private double AV34Tax_It ;
   private double AV35Tax_Ot ;
   private double AV36Tax_Ch ;
   private double AV37Tax_Xx ;
   private double AV116Tax2A ;
   private double AV107Cash_ ;
   private double AV175Rate_ ;
   private double AV176Exc_R ;
   private double AV301EFRT ;
   private double A872LIT_CO ;
   private double A873OVER_C ;
   private double A888OVER_I ;
   private double A870VALOR_ ;
   private double AV102Var1 ;
   private double AV100Var ;
   private double A871COMMIS ;
   private double A874AIRPT_ ;
   private double A875TAX_1 ;
   private double A876TAX_2 ;
   private double A879TAX_BR ;
   private double A880TAX_DE ;
   private double A881TAX_XT ;
   private double A882TAX_US ;
   private double A883TAX_IT ;
   private double A884TAX_OT ;
   private double A885TAX_CH ;
   private double A886TAX_XX ;
   private double A887TAX_CP ;
   private double A893DUE_AG ;
   private double A904IVAAmo ;
   private double A910EFRT ;
   private double A942Tax_Am ;
   private double AV224TADSL ;
   private double AV239AtuSa ;
   private double AV75C_Rate ;
   private double AV395nAmou ;
   private double AV78FomPay ;
   private double AV121Total ;
   private double AV396nAmou ;
   private double AV179Valor ;
   private double AV180Valor ;
   private double AV370GovTa ;
   private double AV369GovTa ;
   private double AV367GovTa ;
   private double AV368GovTa ;
   private String AV201ciaco ;
   private String AV20Period ;
   private String AV281Modo ;
   private String AV282Error ;
   private String AV290VerNu ;
   private String AV348Type ;
   private String AV378Path ;
   private String AV389Short ;
   private String AV310ENCCC ;
   private String AV302RFDCR ;
   private String AV288ZumOn ;
   private String AV286LOADH ;
   private String AV325PGAMO ;
   private String AV311AccTa ;
   private String AV365RFND2 ;
   private String AV345PERDI ;
   private String AV189Level[] ;
   private String AV190LevEv[] ;
   private String AV320AcctS[][] ;
   private String AV312Tax ;
   private String AV315TaxCo[] ;
   private String AV23Achou ;
   private String AV92Primei ;
   private String AV127Passo ;
   private String AV104TLinA ;
   private String AV186Linha ;
   private String AV215Carta ;
   private String AV270msg1 ;
   private String AV65Val2 ;
   private String AV63Simbol ;
   private String AV68Valx ;
   private String AV62Val1 ;
   private String AV158Ultim ;
   private String AV156Tipo_[] ;
   private String A963ISOC ;
   private String AV373ISOC ;
   private String A965PER_NA ;
   private String AV135PerMT[] ;
   private String A966CODE ;
   private String AV246Trans ;
   private String A967IATA ;
   private String AV136IataM[] ;
   private String AV148Num_M[] ;
   private String A968NUM_BI ;
   private String AV40Tkt ;
   private String AV231TempB ;
   private String A969TIPO_V ;
   private String AV79Pay_ty ;
   private String AV141I_Fro[] ;
   private String AV142I_ToM[] ;
   private String AV143Carri[] ;
   private String AV144Tarri[] ;
   private String A928Claxss ;
   private String AV145Class[] ;
   private String AV146Fligh[] ;
   private String A929Fli_Da ;
   private String AV147Fli_M[] ;
   private String A964CiaCod ;
   private String Gx_emsg ;
   private String AV161Per_P[] ;
   private String AV163IataP[] ;
   private String AV164NumPa[] ;
   private String A936PGTIP_ ;
   private String AV173PgTip[] ;
   private String A933PGCC_T ;
   private String AV168PgCCT[] ;
   private String AV169Pgccc[] ;
   private String A1074PGCCM ;
   private String GXv_char3[] ;
   private String GXv_char2[] ;
   private String AV308PGCCM ;
   private String GXv_char4[] ;
   private String A941EXDA ;
   private String AV304EXDA ;
   private String A940APLC ;
   private String AV305APLC ;
   private String AV183Tax_C[] ;
   private String AV319Found ;
   private String AV226ADM_p ;
   private String AV227ACM_p ;
   private String AV238Trans ;
   private String AV225BSPIa ;
   private String AV22iataC ;
   private String AV252TipoM ;
   private String AV296TADDN ;
   private String AV247GRNum ;
   private String AV298TADCN ;
   private String AV216CC_No ;
   private String AV214LocTa ;
   private String AV203SalCo ;
   private String AV202ASCod ;
   private String AV287LOADE ;
   private String AV109Trans ;
   private String AV112TktPa ;
   private String AV126TktFi ;
   private String AV250STAT ;
   private String AV303wSTAT ;
   private String AV133FezAt ;
   private String AV299HOT_F ;
   private String AV245RPSI ;
   private String AV110VendC ;
   private String AV138CodeM[] ;
   private String AV276FCMI ;
   private String AV277INLS ;
   private String AV278FRCA[] ;
   private String AV289CPUI ;
   private String AV265Sub_N[] ;
   private String AV354statu ;
   private String AV338Per_N ;
   private String A918CODE2 ;
   private String AV335CODEA ;
   private String AV336IATAA ;
   private String AV334Num_B ;
   private String A900MOEDA ;
   private String A869ID_AGE ;
   private String AV26Id_Age ;
   private String A890ADM_AC ;
   private String AV337Tipo_ ;
   private String AV83Pax ;
   private String AV376PNR ;
   private String AV321NewIv ;
   private String A891CC_TYP ;
   private String AV80Cc_Typ ;
   private String A909HOT_FO ;
   private String AV87Numero ;
   private String A905FCMI ;
   private String A906INLS ;
   private String A907CPUI ;
   private String A908Loader ;
   private String AV404Trans ;
   private String A913TCNR ;
   private String A978IFCode ;
   private String A952IFStat ;
   private String A951FRCA ;
   private String A973Tax_Co ;
   private String AV73Pais ;
   private String A975Tax_Pd ;
   private String A976Sub_Nu ;
   private String scmdbuf ;
   private String AV269Barra ;
   private String AV209YearP ;
   private String AV221Acm_P ;
   private String AV222Adm_P ;
   private String AV223TADSN ;
   private String AV251MemoD ;
   private String AV263Lista ;
   private String AV253TADSL ;
   private String AV254GRLis ;
   private String AV259Palav ;
   private String AV257letra ;
   private String AV258Check ;
   private String AV261mNum_ ;
   private String AV347Proce ;
   private String A997HntSeq ;
   private String AV9TLine ;
   private String AV21TSubLi ;
   private String AV10DtFH ;
   private String AV11TmFH ;
   private String AV379DATAC ;
   private String AV12yy ;
   private String AV24y1 ;
   private String AV25y2 ;
   private String AV14mm ;
   private String AV15dd ;
   private String AV17Data1 ;
   private String AV240CurrC ;
   private String AV244AgIat ;
   private String A1313CadIA ;
   private String A1314CadNo ;
   private String AV188agnom ;
   private String AV407Partn ;
   private String AV406Partn ;
   private String AV53yybks ;
   private String AV54mmbks ;
   private String AV52ddbks ;
   private String AV56Data2 ;
   private String AV57Part ;
   private String AV58Par1 ;
   private String AV59Par2 ;
   private String AV60Par3 ;
   private String AV93TransI ;
   private String AV44I_from ;
   private String AV45I_to ;
   private String AV46I_car ;
   private String AV47I_tar ;
   private String AV49I_tkt ;
   private String AV50Flight ;
   private String AV51Fli_Da ;
   private String AV125Class ;
   private String AV89PaisX ;
   private String AV74Pais2 ;
   private String AV90Pais2X ;
   private String AV99Header ;
   private String AV382TCND ;
   private String AV105Trans ;
   private String AV81Cccf ;
   private String AV114Aux ;
   private String AV122PayCA ;
   private String AV123CC_Ty ;
   private String AV124CccfC ;
   private String AV119Gru1 ;
   private String AV120Gru2 ;
   private String AV162CodeP[] ;
   private String AV228TmpUS ;
   private String AV229TmpLo ;
   private String AV177FareU ;
   private String AV178FareB ;
   private String AV341ACMG_ ;
   private String AV327ACMG_ ;
   private String AV342ACMG_ ;
   private String AV328ACMG_ ;
   private String AV326USE_A ;
   private String AV329Vetor[] ;
   private String AV330Vetor[] ;
   private String GXt_char1 ;
   private java.util.Date A953IFDate ;
   private java.util.Date AV355DataF ;
   private java.util.Date A970DATA ;
   private java.util.Date AV157DataM[] ;
   private java.util.Date A930Fli_Da ;
   private java.util.Date AV271Fli_M[] ;
   private java.util.Date AV165DataP[] ;
   private java.util.Date AV55Datbks ;
   private java.util.Date AV339DataA ;
   private java.util.Date AV380DATA_ ;
   private java.util.Date AV394Data_ ;
   private java.util.Date AV16Per_Da ;
   private java.util.Date AV18Data_I ;
   private java.util.Date AV272Fli_D ;
   private java.util.Date AV344PerDa ;
   private java.util.Date A1107PerDa ;
   private java.util.Date A1106PerDa ;
   private boolean returnInSub ;
   private boolean n922Num_Bi ;
   private boolean n923I_From ;
   private boolean n924I_To ;
   private boolean n925Carrie ;
   private boolean n926Tariff ;
   private boolean n928Claxss ;
   private boolean n927Flight ;
   private boolean n929Fli_Da ;
   private boolean n930Fli_Da ;
   private boolean n936PGTIP_ ;
   private boolean n933PGCC_T ;
   private boolean n934PGCCCF ;
   private boolean n937PGINFO ;
   private boolean n1073PGCCN ;
   private boolean n1074PGCCM ;
   private boolean n935PGAMOU ;
   private boolean n939PGMOED ;
   private boolean n941EXDA ;
   private boolean n940APLC ;
   private boolean n918CODE2 ;
   private boolean n900MOEDA ;
   private boolean n869ID_AGE ;
   private boolean n877TOUR_C ;
   private boolean n872LIT_CO ;
   private boolean n873OVER_C ;
   private boolean n888OVER_I ;
   private boolean n890ADM_AC ;
   private boolean n870VALOR_ ;
   private boolean n878PAX_NA ;
   private boolean n871COMMIS ;
   private boolean n894PLP_DO ;
   private boolean n897NUMBER ;
   private boolean n874AIRPT_ ;
   private boolean n875TAX_1 ;
   private boolean n876TAX_2 ;
   private boolean n879TAX_BR ;
   private boolean n880TAX_DE ;
   private boolean n881TAX_XT ;
   private boolean n882TAX_US ;
   private boolean n883TAX_IT ;
   private boolean n884TAX_OT ;
   private boolean n885TAX_CH ;
   private boolean n886TAX_XX ;
   private boolean n887TAX_CP ;
   private boolean n889TRANS_ ;
   private boolean n891CC_TYP ;
   private boolean n909HOT_FO ;
   private boolean n893DUE_AG ;
   private boolean n898ISSUE_ ;
   private boolean n904IVAAmo ;
   private boolean n899PAYMT_ ;
   private boolean n902NUM_BI ;
   private boolean n905FCMI ;
   private boolean n906INLS ;
   private boolean n907CPUI ;
   private boolean n908Loader ;
   private boolean n910EFRT ;
   private boolean n913TCNR ;
   private boolean n952IFStat ;
   private boolean n953IFDate ;
   private boolean n954IFDeta ;
   private boolean n951FRCA ;
   private boolean n942Tax_Am ;
   private boolean n948Sub_De ;
   private boolean n949Sub_De ;
   private boolean n950Sub_De ;
   private boolean n994HntLin ;
   private boolean n1314CadNo ;
   private String AV8Linha ;
   private String A954IFDeta ;
   private String A994HntLin ;
   private String A923I_From ;
   private String A924I_To ;
   private String A925Carrie ;
   private String A926Tariff ;
   private String A927Flight ;
   private String A934PGCCCF ;
   private String A937PGINFO ;
   private String AV170PgInf[] ;
   private String A1073PGCCN ;
   private String AV309PGInf ;
   private String AV307PGCCN ;
   private String A939PGMOED ;
   private String AV206PgMoe ;
   private String AV207CiaLo ;
   private String AV39Trans_ ;
   private String AV43Issue_ ;
   private String AV118Grupo ;
   private String AV266Sub_D[] ;
   private String AV267Sub_D[] ;
   private String AV268Sub_D[] ;
   private String A877TOUR_C ;
   private String A878PAX_NA ;
   private String A894PLP_DO ;
   private String A897NUMBER ;
   private String A889TRANS_ ;
   private String AV61Tour_C ;
   private String A898ISSUE_ ;
   private String A899PAYMT_ ;
   private String AV182paymt ;
   private String A948Sub_De ;
   private String A949Sub_De ;
   private String A950Sub_De ;
   private String A349CurCod ;
   private String A23ISOCod ;
   private String A263PerID ;
   private String A387Period ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
   private String[] aP4 ;
   private int[] aP5 ;
   private String[] aP6 ;
   private String[] aP7 ;
   private IDataStoreProvider pr_default ;
   private String[] P005F10_A963ISOC ;
   private String[] P005F10_A964CiaCod ;
   private String[] P005F10_A965PER_NA ;
   private String[] P005F10_A966CODE ;
   private String[] P005F10_A967IATA ;
   private String[] P005F10_A968NUM_BI ;
   private String[] P005F10_A969TIPO_V ;
   private java.util.Date[] P005F10_A970DATA ;
   private String[] P005F10_A976Sub_Nu ;
   private String[] P005F10_A948Sub_De ;
   private boolean[] P005F10_n948Sub_De ;
   private String[] P005F10_A949Sub_De ;
   private boolean[] P005F10_n949Sub_De ;
   private String[] P005F10_A950Sub_De ;
   private boolean[] P005F10_n950Sub_De ;
   private String[] P005F12_A349CurCod ;
   private String[] P005F12_A23ISOCod ;
   private String[] P005F13_A997HntSeq ;
   private String[] P005F13_A994HntLin ;
   private boolean[] P005F13_n994HntLin ;
   private byte[] P005F13_A998HntSub ;
   private String[] P005F14_A966CODE ;
   private String[] P005F14_A965PER_NA ;
   private long[] P005F14_A902NUM_BI ;
   private boolean[] P005F14_n902NUM_BI ;
   private double[] P005F14_A874AIRPT_ ;
   private boolean[] P005F14_n874AIRPT_ ;
   private String[] P005F14_A963ISOC ;
   private String[] P005F14_A964CiaCod ;
   private String[] P005F14_A967IATA ;
   private String[] P005F14_A968NUM_BI ;
   private String[] P005F14_A969TIPO_V ;
   private java.util.Date[] P005F14_A970DATA ;
   private String[] P005F15_A965PER_NA ;
   private String[] P005F15_A966CODE ;
   private long[] P005F15_A902NUM_BI ;
   private boolean[] P005F15_n902NUM_BI ;
   private double[] P005F15_A874AIRPT_ ;
   private boolean[] P005F15_n874AIRPT_ ;
   private String[] P005F15_A963ISOC ;
   private String[] P005F15_A964CiaCod ;
   private String[] P005F15_A967IATA ;
   private String[] P005F15_A968NUM_BI ;
   private String[] P005F15_A969TIPO_V ;
   private java.util.Date[] P005F15_A970DATA ;
   private String[] P005F16_A1313CadIA ;
   private String[] P005F16_A1314CadNo ;
   private boolean[] P005F16_n1314CadNo ;
   private String[] P005F18_A966CODE ;
   private String[] P005F18_A965PER_NA ;
   private long[] P005F18_A902NUM_BI ;
   private boolean[] P005F18_n902NUM_BI ;
   private double[] P005F18_A874AIRPT_ ;
   private boolean[] P005F18_n874AIRPT_ ;
   private String[] P005F18_A963ISOC ;
   private String[] P005F18_A964CiaCod ;
   private String[] P005F18_A967IATA ;
   private String[] P005F18_A968NUM_BI ;
   private String[] P005F18_A969TIPO_V ;
   private java.util.Date[] P005F18_A970DATA ;
   private String[] P005F19_A965PER_NA ;
   private String[] P005F19_A966CODE ;
   private long[] P005F19_A902NUM_BI ;
   private boolean[] P005F19_n902NUM_BI ;
   private double[] P005F19_A874AIRPT_ ;
   private boolean[] P005F19_n874AIRPT_ ;
   private String[] P005F19_A963ISOC ;
   private String[] P005F19_A964CiaCod ;
   private String[] P005F19_A967IATA ;
   private String[] P005F19_A968NUM_BI ;
   private String[] P005F19_A969TIPO_V ;
   private java.util.Date[] P005F19_A970DATA ;
   private java.util.Date[] P005F20_A970DATA ;
   private String[] P005F20_A969TIPO_V ;
   private String[] P005F20_A968NUM_BI ;
   private String[] P005F20_A967IATA ;
   private String[] P005F20_A966CODE ;
   private String[] P005F20_A965PER_NA ;
   private String[] P005F20_A964CiaCod ;
   private String[] P005F20_A963ISOC ;
   private double[] P005F20_A942Tax_Am ;
   private boolean[] P005F20_n942Tax_Am ;
   private String[] P005F20_A973Tax_Co ;
   private byte[] P005F20_A974Tax_Wh ;
   private String[] P005F20_A975Tax_Pd ;
   private String[] P005F21_A966CODE ;
   private String[] P005F21_A965PER_NA ;
   private long[] P005F21_A902NUM_BI ;
   private boolean[] P005F21_n902NUM_BI ;
   private double[] P005F21_A874AIRPT_ ;
   private boolean[] P005F21_n874AIRPT_ ;
   private String[] P005F21_A963ISOC ;
   private String[] P005F21_A964CiaCod ;
   private String[] P005F21_A967IATA ;
   private String[] P005F21_A968NUM_BI ;
   private String[] P005F21_A969TIPO_V ;
   private java.util.Date[] P005F21_A970DATA ;
   private java.util.Date[] P005F22_A1107PerDa ;
   private java.util.Date[] P005F22_A1106PerDa ;
   private String[] P005F22_A263PerID ;
   private String[] P005F22_A23ISOCod ;
   private String[] P005F22_A349CurCod ;
   private String[] P005F22_A387Period ;
}

final  class ppeginf__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new UpdateCursor("P005F2", "INSERT INTO [HOT1] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [ISeq], [Num_BilP], [I_From], [I_To], [Carrier], [Tariff], [Flight], [Claxss], [Fli_DateC], [Fli_DateD], [Fli_Time], [STPO]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F3", "INSERT INTO [HOT2] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [SeqPag], [PGCC_TYPE], [PGCCCF], [PGAMOUNT], [PGTIP_VEND], [PGINFO], [PGMOEDA], [APLC], [EXDA], [PGCCNUMBER], [PGCCMASKED]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F4", "INSERT INTO [HOT] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [ID_AGENCY], [VALOR_USD], [COMMISSION], [LIT_COMM], [OVER_COMM], [AIRPT_TAX], [TAX_1], [TAX_2], [TOUR_CODE], [PAX_NAME], [TAX_BR], [TAX_DE], [TAX_XT], [TAX_US], [TAX_IT], [TAX_OT], [TAX_CH], [TAX_XX], [TAX_CP], [OVER_INCE], [TRANS_NO], [ADM_ACM], [CC_TYPE], [DUE_AGT_AA], [PLP_DOC], [NUMBER], [ISSUE_INFO], [PAYMT_INFO], [MOEDA], [NUM_BIL2], [IVAAmou], [FCMI], [INLS], [CPUI], [LoaderVers], [HOT_FORM], [EFRT], [TCNR], [CODE2], [CCCF], [DESC_PERCT], [RA_NUMBER], [CONJ_TO], [VOIDTKT], [END_DATE], [BEG_DATE], [TCND], [ESAC], [NAME_RET], [DATA_HEADER], [SETT_DATE], [HOTREMARK]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0), '', convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '', '', '', convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F5", "INSERT INTO [HOT6] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [IFCode], [IFStatus], [IFDate], [IFDetail]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F6", "INSERT INTO [HOT5] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [FRCASeq], [FRCA]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F7", "INSERT INTO [HOT3] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Tax_Cod], [Tax_Wh], [Tax_Pd], [Tax_Amt], [Tax_Hot]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F8", "UPDATE [HOT3] SET [Tax_Amt]=[Tax_Amt] + ?  WHERE [ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [Tax_Cod] = ? and [Tax_Wh] = ? and [Tax_Pd] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P005F9", "INSERT INTO [HOT4] ([ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Sub_NumTkt], [Sub_Desc1], [Sub_Desc2], [Sub_Desc3], [Sub_tptkt], [Sub_Amt1], [Sub_Amt2], [Sub_Amt3]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), convert(int, 0), convert(int, 0))", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005F10", "SELECT [ISOC], [CiaCod], [PER_NAME], [CODE], [IATA], [NUM_BIL], [TIPO_VEND], [DATA], [Sub_NumTkt], [Sub_Desc1], [Sub_Desc2], [Sub_Desc3] FROM [HOT4] WITH (UPDLOCK) WHERE ([ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [Sub_NumTkt] = ?) AND ([ISOC] = ? and [CiaCod] = ? and [PER_NAME] = ? and [CODE] = ? and [IATA] = ? and [NUM_BIL] = ? and [TIPO_VEND] = ? and [DATA] = ? and [Sub_NumTkt] = ?) ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P005F11", "UPDATE [HOT4] SET [Sub_Desc1]=?, [Sub_Desc2]=?, [Sub_Desc3]=?  WHERE [ISOC] = ? AND [CiaCod] = ? AND [PER_NAME] = ? AND [CODE] = ? AND [IATA] = ? AND [NUM_BIL] = ? AND [TIPO_VEND] = ? AND [DATA] = ? AND [Sub_NumTkt] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005F12", "SELECT [CurCode], [ISOCod] FROM [COUNTRYCURRENCIES] WITH (NOLOCK) WHERE ([ISOCod] = ?) AND ([CurCode] <> 'USD') ORDER BY [ISOCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F13", "SELECT [HntSeq], [HntLine], [HntSub] FROM [RETSPECTEMP] WITH (NOLOCK) WHERE CONVERT( DECIMAL(28,14), [HntSeq]) >= ? ORDER BY [HntSeq], [HntSub] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F14", "SELECT [CODE], [PER_NAME], [NUM_BIL2], [AIRPT_TAX], [ISOC], [CiaCod], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([PER_NAME] = ?) AND ([CODE] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F15", "SELECT [PER_NAME], [CODE], [NUM_BIL2], [AIRPT_TAX], [ISOC], [CiaCod], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([CODE] = ?) AND ([PER_NAME] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F16", "SELECT [CadIATA], [CadNome] FROM [CADAGE] WITH (NOLOCK) WHERE [CadIATA] = ? ORDER BY [CadIATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P005F17", "INSERT INTO [CADAGE] ([CadIATA], [CadNome], [CadDigIATA], [TipAgenteHO], [TipAgenteCnpj], [CadHeadOffice], [CadApelido], [CadData], [CadCNPJ], [CadEndereco], [CadBairro], [CadCEP], [CadEmail], [CadWWW], [CadCidSigla], [CadEstado], [CadFaxDDD], [CadTelefone], [CadTelDDD], [CadFax], [CadCredito], [CadBanco], [CadBanAge], [CadBanConta], [CadAccCode], [CadStation], [CadCCM], [CadSabreID], [CadIRRTipo], [CadCommis], [CadPal], [CadDac], [CadVendor], [CadFatEnv], [CadColID], [BcoCod], [BcoNom], [BcoArmazId], [BcoArmazDs], [CadISS], [CadBspLinkTipUsu], [CadTipEmisAut], [CadEspIss], [CadTpAviso], [CadAssociate]) VALUES (?, ?, '', '', '', '', '', convert( DATETIME, '17530101', 112 ), '', '', '', '', '', '', '', '', '', '', '', '', convert(int, 0), '', '', '', '', '', '', '', '', '', '', '', '', '', '', convert(int, 0), '', '', '', '', '', '', convert(int, 0), '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P005F18", "SELECT [CODE], [PER_NAME], [NUM_BIL2], [AIRPT_TAX], [ISOC], [CiaCod], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([PER_NAME] = ?) AND ([CODE] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F19", "SELECT [PER_NAME], [CODE], [NUM_BIL2], [AIRPT_TAX], [ISOC], [CiaCod], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([CODE] = ?) AND ([PER_NAME] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F20", "SELECT [DATA], [TIPO_VEND], [NUM_BIL], [IATA], [CODE], [PER_NAME], [CiaCod], [ISOC], [Tax_Amt], [Tax_Cod], [Tax_Wh], [Tax_Pd] FROM [HOT3] WITH (NOLOCK) WHERE ([ISOC] = ?) AND ([CiaCod] = SUBSTRING(?, 1, 5)) AND ([PER_NAME] = SUBSTRING(?, 1, 10)) AND ([CODE] = SUBSTRING(RTRIM(LTRIM(?)), 1, 15)) AND ([IATA] = SUBSTRING(?, 1, 15)) AND ([NUM_BIL] = SUBSTRING(?, 1, 14)) AND ([TIPO_VEND] = SUBSTRING(?, 1, 2)) AND ([DATA] = ?) ORDER BY [ISOC] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F21", "SELECT [CODE], [PER_NAME], [NUM_BIL2], [AIRPT_TAX], [ISOC], [CiaCod], [IATA], [NUM_BIL], [TIPO_VEND], [DATA] FROM [HOT] WITH (NOLOCK) WHERE ([NUM_BIL2] = ?) AND ([PER_NAME] = ?) AND ([CODE] = ?) ORDER BY [NUM_BIL2] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P005F22", "SELECT TOP 1 [PerDatEnd], [PerDatIni], [PerID], [ISOCod], [CurCode], [PeriodTypesCode] FROM [PERIODS] WITH (NOLOCK) WHERE ([PerDatIni] >= ?) AND ([PerDatEnd] <= ?) ORDER BY [ISOCod], [CurCode], [PeriodTypesCode], [PerID] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 8 :
               ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((java.util.Date[]) buf[7])[0] = rslt.getGXDate(8) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 20) ;
               ((String[]) buf[9])[0] = rslt.getVarchar(10) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
               ((boolean[]) buf[12])[0] = rslt.wasNull();
               ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
               ((boolean[]) buf[14])[0] = rslt.wasNull();
               break;
            case 10 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getString(1, 8) ;
               ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((byte[]) buf[3])[0] = rslt.getByte(3) ;
               break;
            case 12 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((long[]) buf[2])[0] = rslt.getLong(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               break;
            case 13 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((long[]) buf[2])[0] = rslt.getLong(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               break;
            case 14 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 45) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((long[]) buf[2])[0] = rslt.getLong(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               break;
            case 17 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((long[]) buf[2])[0] = rslt.getLong(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               break;
            case 18 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 2) ;
               ((double[]) buf[8])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((String[]) buf[10])[0] = rslt.getString(10, 10) ;
               ((byte[]) buf[11])[0] = rslt.getByte(11) ;
               ((String[]) buf[12])[0] = rslt.getString(12, 10) ;
               break;
            case 19 :
               ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
               ((long[]) buf[2])[0] = rslt.getLong(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((double[]) buf[4])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
               ((String[]) buf[7])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[8])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[9])[0] = rslt.getString(8, 20) ;
               ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
               ((java.util.Date[]) buf[11])[0] = rslt.getGXDate(10) ;
               break;
            case 20 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setInt(9, ((Number) parms[8]).intValue());
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(10, ((Number) parms[10]).longValue());
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 20);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[14], 20);
               }
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(13, (String)parms[16], 20);
               }
               if ( ((Boolean) parms[17]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(14, (String)parms[18], 30);
               }
               if ( ((Boolean) parms[19]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(15, (String)parms[20], 15);
               }
               if ( ((Boolean) parms[21]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(16, (String)parms[22], 2);
               }
               if ( ((Boolean) parms[23]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(17, (String)parms[24], 5);
               }
               if ( ((Boolean) parms[25]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(18, (java.util.Date)parms[26]);
               }
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setInt(9, ((Number) parms[8]).intValue());
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[10], 2);
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 30);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[14]).doubleValue());
               }
               if ( ((Boolean) parms[15]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(13, (String)parms[16], 2);
               }
               if ( ((Boolean) parms[17]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(14, (String)parms[18], 32);
               }
               if ( ((Boolean) parms[19]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(15, (String)parms[20], 3);
               }
               if ( ((Boolean) parms[21]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(16, (String)parms[22], 6);
               }
               if ( ((Boolean) parms[23]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(17, (String)parms[24], 4);
               }
               if ( ((Boolean) parms[25]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(18, (String)parms[26], 1024);
               }
               if ( ((Boolean) parms[27]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(19, (String)parms[28], 32);
               }
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[9], 4);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(10, ((Number) parms[11]).doubleValue());
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(11, ((Number) parms[13]).doubleValue());
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[15]).doubleValue());
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 13 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(13, ((Number) parms[17]).doubleValue());
               }
               if ( ((Boolean) parms[18]).booleanValue() )
               {
                  stmt.setNull( 14 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(14, ((Number) parms[19]).doubleValue());
               }
               if ( ((Boolean) parms[20]).booleanValue() )
               {
                  stmt.setNull( 15 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(15, ((Number) parms[21]).doubleValue());
               }
               if ( ((Boolean) parms[22]).booleanValue() )
               {
                  stmt.setNull( 16 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(16, ((Number) parms[23]).doubleValue());
               }
               if ( ((Boolean) parms[24]).booleanValue() )
               {
                  stmt.setNull( 17 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(17, (String)parms[25], 100);
               }
               if ( ((Boolean) parms[26]).booleanValue() )
               {
                  stmt.setNull( 18 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(18, (String)parms[27], 50);
               }
               if ( ((Boolean) parms[28]).booleanValue() )
               {
                  stmt.setNull( 19 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(19, ((Number) parms[29]).doubleValue());
               }
               if ( ((Boolean) parms[30]).booleanValue() )
               {
                  stmt.setNull( 20 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(20, ((Number) parms[31]).doubleValue());
               }
               if ( ((Boolean) parms[32]).booleanValue() )
               {
                  stmt.setNull( 21 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(21, ((Number) parms[33]).doubleValue());
               }
               if ( ((Boolean) parms[34]).booleanValue() )
               {
                  stmt.setNull( 22 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(22, ((Number) parms[35]).doubleValue());
               }
               if ( ((Boolean) parms[36]).booleanValue() )
               {
                  stmt.setNull( 23 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(23, ((Number) parms[37]).doubleValue());
               }
               if ( ((Boolean) parms[38]).booleanValue() )
               {
                  stmt.setNull( 24 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(24, ((Number) parms[39]).doubleValue());
               }
               if ( ((Boolean) parms[40]).booleanValue() )
               {
                  stmt.setNull( 25 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(25, ((Number) parms[41]).doubleValue());
               }
               if ( ((Boolean) parms[42]).booleanValue() )
               {
                  stmt.setNull( 26 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(26, ((Number) parms[43]).doubleValue());
               }
               if ( ((Boolean) parms[44]).booleanValue() )
               {
                  stmt.setNull( 27 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(27, ((Number) parms[45]).doubleValue());
               }
               if ( ((Boolean) parms[46]).booleanValue() )
               {
                  stmt.setNull( 28 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(28, ((Number) parms[47]).doubleValue());
               }
               if ( ((Boolean) parms[48]).booleanValue() )
               {
                  stmt.setNull( 29 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(29, (String)parms[49], 20);
               }
               if ( ((Boolean) parms[50]).booleanValue() )
               {
                  stmt.setNull( 30 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(30, (String)parms[51], 1);
               }
               if ( ((Boolean) parms[52]).booleanValue() )
               {
                  stmt.setNull( 31 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(31, (String)parms[53], 2);
               }
               if ( ((Boolean) parms[54]).booleanValue() )
               {
                  stmt.setNull( 32 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(32, ((Number) parms[55]).doubleValue());
               }
               if ( ((Boolean) parms[56]).booleanValue() )
               {
                  stmt.setNull( 33 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(33, (String)parms[57], 20);
               }
               if ( ((Boolean) parms[58]).booleanValue() )
               {
                  stmt.setNull( 34 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(34, (String)parms[59], 20);
               }
               if ( ((Boolean) parms[60]).booleanValue() )
               {
                  stmt.setNull( 35 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(35, (String)parms[61], 100);
               }
               if ( ((Boolean) parms[62]).booleanValue() )
               {
                  stmt.setNull( 36 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(36, (String)parms[63], 100);
               }
               if ( ((Boolean) parms[64]).booleanValue() )
               {
                  stmt.setNull( 37 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(37, (String)parms[65], 5);
               }
               if ( ((Boolean) parms[66]).booleanValue() )
               {
                  stmt.setNull( 38 , Types.NUMERIC );
               }
               else
               {
                  stmt.setLong(38, ((Number) parms[67]).longValue());
               }
               if ( ((Boolean) parms[68]).booleanValue() )
               {
                  stmt.setNull( 39 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(39, ((Number) parms[69]).doubleValue());
               }
               if ( ((Boolean) parms[70]).booleanValue() )
               {
                  stmt.setNull( 40 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(40, (String)parms[71], 1);
               }
               if ( ((Boolean) parms[72]).booleanValue() )
               {
                  stmt.setNull( 41 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(41, (String)parms[73], 4);
               }
               if ( ((Boolean) parms[74]).booleanValue() )
               {
                  stmt.setNull( 42 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(42, (String)parms[75], 4);
               }
               if ( ((Boolean) parms[76]).booleanValue() )
               {
                  stmt.setNull( 43 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(43, (String)parms[77], 10);
               }
               if ( ((Boolean) parms[78]).booleanValue() )
               {
                  stmt.setNull( 44 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(44, (String)parms[79], 1);
               }
               if ( ((Boolean) parms[80]).booleanValue() )
               {
                  stmt.setNull( 45 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(45, ((Number) parms[81]).doubleValue());
               }
               if ( ((Boolean) parms[82]).booleanValue() )
               {
                  stmt.setNull( 46 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(46, (String)parms[83], 15);
               }
               if ( ((Boolean) parms[84]).booleanValue() )
               {
                  stmt.setNull( 47 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(47, (String)parms[85], 20);
               }
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 20);
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[10], 3);
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(11, (java.util.Date)parms[12], false);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.LONGVARCHAR );
               }
               else
               {
                  stmt.setLongVarchar(12, (String)parms[14]);
               }
               break;
            case 4 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setByte(9, ((Number) parms[8]).byteValue());
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(10, (String)parms[10], 87);
               }
               break;
            case 5 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 10);
               stmt.setByte(10, ((Number) parms[9]).byteValue());
               stmt.setString(11, (String)parms[10], 10);
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.NUMERIC );
               }
               else
               {
                  stmt.setDouble(12, ((Number) parms[12]).doubleValue());
               }
               break;
            case 6 :
               stmt.setDouble(1, ((Number) parms[0]).doubleValue());
               stmt.setString(2, (String)parms[1], 2);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setString(8, (String)parms[7], 20);
               stmt.setDate(9, (java.util.Date)parms[8]);
               stmt.setString(10, (String)parms[9], 10);
               stmt.setByte(11, ((Number) parms[10]).byteValue());
               stmt.setString(12, (String)parms[11], 10);
               break;
            case 7 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 20);
               if ( ((Boolean) parms[9]).booleanValue() )
               {
                  stmt.setNull( 10 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(10, (String)parms[10], 40);
               }
               if ( ((Boolean) parms[11]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(11, (String)parms[12], 40);
               }
               if ( ((Boolean) parms[13]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[14], 40);
               }
               break;
            case 8 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 20);
               stmt.setString(5, (String)parms[4], 20);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 20);
               stmt.setDate(8, (java.util.Date)parms[7]);
               stmt.setString(9, (String)parms[8], 20);
               stmt.setString(10, (String)parms[9], 2);
               stmt.setString(11, (String)parms[10], 20);
               stmt.setString(12, (String)parms[11], 20);
               stmt.setString(13, (String)parms[12], 20);
               stmt.setString(14, (String)parms[13], 20);
               stmt.setString(15, (String)parms[14], 20);
               stmt.setString(16, (String)parms[15], 20);
               stmt.setDate(17, (java.util.Date)parms[16]);
               stmt.setString(18, (String)parms[17], 20);
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 40);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 40);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 40);
               }
               stmt.setString(4, (String)parms[6], 2);
               stmt.setString(5, (String)parms[7], 20);
               stmt.setString(6, (String)parms[8], 20);
               stmt.setString(7, (String)parms[9], 20);
               stmt.setString(8, (String)parms[10], 20);
               stmt.setString(9, (String)parms[11], 20);
               stmt.setString(10, (String)parms[12], 20);
               stmt.setDate(11, (java.util.Date)parms[13]);
               stmt.setString(12, (String)parms[14], 20);
               break;
            case 10 :
               stmt.setString(1, (String)parms[0], 5);
               break;
            case 11 :
               stmt.setInt(1, ((Number) parms[0]).intValue());
               break;
            case 12 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 15);
               break;
            case 13 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 15);
               stmt.setString(3, (String)parms[2], 20);
               break;
            case 14 :
               stmt.setString(1, (String)parms[0], 10);
               break;
            case 15 :
               stmt.setString(1, (String)parms[0], 11);
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(2, (String)parms[2], 45);
               }
               break;
            case 16 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 15);
               break;
            case 17 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 15);
               stmt.setString(3, (String)parms[2], 20);
               break;
            case 18 :
               stmt.setString(1, (String)parms[0], 2);
               stmt.setString(2, (String)parms[1], 5);
               stmt.setString(3, (String)parms[2], 20);
               stmt.setString(4, (String)parms[3], 4);
               stmt.setString(5, (String)parms[4], 8);
               stmt.setString(6, (String)parms[5], 14);
               stmt.setString(7, (String)parms[6], 2);
               stmt.setDate(8, (java.util.Date)parms[7]);
               break;
            case 19 :
               stmt.setLong(1, ((Number) parms[0]).longValue());
               stmt.setString(2, (String)parms[1], 20);
               stmt.setString(3, (String)parms[2], 15);
               break;
            case 20 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               stmt.setDate(2, (java.util.Date)parms[1]);
               break;
      }
   }

}


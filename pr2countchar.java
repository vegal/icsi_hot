/*
               File: R2CountChar
        Description: Contar um caracter dentro de uma string
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:1.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2countchar extends GXProcedure
{
   public pr2countchar( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2countchar.class ), "" );
   }

   public pr2countchar( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        int[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             int[] aP2 )
   {
      pr2countchar.this.AV8s = aP0[0];
      this.aP0 = aP0;
      pr2countchar.this.AV9target = aP1[0];
      this.aP1 = aP1;
      pr2countchar.this.AV10nOutpu = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV10nOutpu = 0 ;
      AV11i = 1 ;
      while ( ( AV11i <= GXutil.len( AV8s) ) )
      {
         if ( ( GXutil.strcmp(GXutil.substring( AV8s, AV11i, 1), AV9target) == 0 ) )
         {
            AV10nOutpu = (int)(AV10nOutpu+1) ;
         }
         AV11i = (int)(AV11i+1) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2countchar.this.AV8s;
      this.aP1[0] = pr2countchar.this.AV9target;
      this.aP2[0] = pr2countchar.this.AV10nOutpu;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV11i = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private int AV10nOutpu ;
   private int AV11i ;
   private String AV8s ;
   private String AV9target ;
   private String[] aP0 ;
   private String[] aP1 ;
   private int[] aP2 ;
}


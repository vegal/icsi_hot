import com.genexus.*;
import com.genexus.ui.*;

public final  class subtairlines43 extends GXSubfileElement
{
   private String AirLineCurCode ;
   private String AirLIneDataBank ;
   private String AirLineCurTrans ;
   private byte VAlterBank ;
   private String ZAirLineCurTrans ;
   public String getAirLineCurCode( )
   {
      return AirLineCurCode ;
   }

   public void setAirLineCurCode( String value )
   {
      AirLineCurCode = value;
   }

   public String getAirLIneDataBank( )
   {
      return AirLIneDataBank ;
   }

   public void setAirLIneDataBank( String value )
   {
      AirLIneDataBank = value;
   }

   public String getAirLineCurTrans( )
   {
      return AirLineCurTrans ;
   }

   public void setAirLineCurTrans( String value )
   {
      AirLineCurTrans = value;
   }

   public byte getVAlterBank( )
   {
      return VAlterBank ;
   }

   public void setVAlterBank( byte value )
   {
      VAlterBank = value;
   }

   public String getZAirLineCurTrans( )
   {
      return ZAirLineCurTrans ;
   }

   public void setZAirLineCurTrans( String value )
   {
      ZAirLineCurTrans = value;
   }

   public void clear( )
   {
      AirLineCurCode = "" ;
      AirLIneDataBank = "" ;
      AirLineCurTrans = "" ;
      VAlterBank = 0 ;
      ZAirLineCurTrans = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getAirLineCurCode().compareTo(((subtairlines43) element).getAirLineCurCode()) ;
            case 1 :
               return  getAirLIneDataBank().compareTo(((subtairlines43) element).getAirLIneDataBank()) ;
            case 2 :
               return  getAirLineCurTrans().compareTo(((subtairlines43) element).getAirLineCurTrans()) ;
      }
      return 0;
   }

   public int isChanged( )
   {
      return (!userModified && (!inserted
      && ( GXutil.strcmp(ZAirLineCurTrans,AirLineCurTrans) == 0)
      ))?0:1;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getAirLineCurCode(), "") == 0 ) && ( GXutil.strcmp(getAirLIneDataBank(), "") == 0 ) && ( GXutil.strcmp(getAirLineCurTrans(), "") == 0 ) && ( getVAlterBank() == 0 ) || insertedNotUserModified() )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getAirLineCurCode() );
            break;
         case 1 :
            cell.setValue( getAirLIneDataBank() );
            break;
         case 2 :
            cell.setValue( getAirLineCurTrans() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCurCode()) == 0) );
         case 1 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLIneDataBank()) == 0) );
         case 2 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCurTrans()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setAirLineCurCode(value.getStringValue());
               break;
            case 1 :
               setAirLIneDataBank(value.getStringValue());
               break;
            case 2 :
               setAirLineCurTrans(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setAirLineCurCode(((subtairlines43) element).getAirLineCurCode());
               return;
            case 1 :
               setAirLIneDataBank(((subtairlines43) element).getAirLIneDataBank());
               return;
            case 2 :
               setAirLineCurTrans(((subtairlines43) element).getAirLineCurTrans());
               return;
      }
   }

}


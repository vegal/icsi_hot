/*
               File: GXcfg
        Description: Relat�rio submiss�es aceitas - i1009/i1010 - 1089
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: June 19, 2020 16:8:53.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.uifactory.*;
import java.sql.*;

public final  class GXcfg
{
   public static int strcmp( String Left ,
                             String Right )
   {
      return GXutil.rtrim(Left).compareTo(GXutil.rtrim(Right));
   }

}


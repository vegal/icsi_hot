/*
               File: getWeekOfMonth
        Description: Pega o n�mero da semana de um determinado dia
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pgetweekofmonth extends GXProcedure
{
   public pgetweekofmonth( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pgetweekofmonth.class ), "" );
   }

   public pgetweekofmonth( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( java.util.Date aP0 ,
                        byte[] aP1 )
   {
      execute_int(aP0, aP1);
   }

   private void execute_int( java.util.Date aP0 ,
                             byte[] aP1 )
   {
      pgetweekofmonth.this.AV9inDate = aP0;
      pgetweekofmonth.this.aP1 = aP1;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV10finalD = GXutil.trim( GXutil.str( GXutil.month( AV9inDate), 10, 0)) + "/01/" + GXutil.trim( GXutil.str( GXutil.year( AV9inDate), 10, 0)) ;
      AV11finalD = localUtil.ctod( AV10finalD, 2) ;
      AV8Week = (byte)(1) ;
      while ( (( AV11finalD.before( AV9inDate ) ) || ( AV11finalD.equals( AV9inDate ) )) )
      {
         if ( ( GXutil.dow( AV11finalD) == 1 ) && ( GXutil.day( AV11finalD) != 1 ) )
         {
            AV8Week = (byte)(AV8Week+1) ;
         }
         AV11finalD = GXutil.dadd(AV11finalD,+(1)) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP1[0] = pgetweekofmonth.this.AV8Week;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV8Week = (byte)(0) ;
      AV10finalD = "" ;
      AV11finalD = GXutil.nullDate() ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV8Week ;
   private short Gx_err ;
   private String AV10finalD ;
   private java.util.Date AV9inDate ;
   private java.util.Date AV11finalD ;
   private byte[] aP1 ;
}


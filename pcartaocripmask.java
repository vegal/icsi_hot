/*
               File: CartaoCripMask
        Description: Cartao Crip Mask
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:5.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pcartaocripmask extends GXProcedure
{
   public pcartaocripmask( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pcartaocripmask.class ), "" );
   }

   public pcartaocripmask( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      pcartaocripmask.this.AV8PGInfo = aP0[0];
      this.aP0 = aP0;
      pcartaocripmask.this.AV16PGCCNU = aP1[0];
      this.aP1 = aP1;
      pcartaocripmask.this.AV17PGCCMA = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV16PGCCNU = "" ;
      AV17PGCCMA = "" ;
      if ( ( GXutil.strcmp(GXutil.trim( AV8PGInfo), "") != 0 ) )
      {
         AV18Cartao = "N" ;
         AV12A = (short)(GXutil.strSearch( AV8PGInfo, "*", 12)) ;
         if ( ( AV12A == 0 ) )
         {
            if ( ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "4") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "5") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "6") == 0 ) )
            {
               AV9Cartao = GXutil.substring( AV8PGInfo, 1, 16) ;
               AV18Cartao = "Y" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 2), "37") == 0 ) )
            {
               AV9Cartao = GXutil.substring( AV8PGInfo, 1, 15) ;
               AV18Cartao = "Y" ;
            }
            else if ( ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 2), "36") == 0 ) )
            {
               AV9Cartao = GXutil.substring( AV8PGInfo, 1, 14) ;
               AV18Cartao = "Y" ;
            }
            else
            {
               /* Execute user subroutine: S1165 */
               S1165 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            AV12A = (short)(AV12A-1) ;
            AV9Cartao = GXutil.substring( AV8PGInfo, 1, AV12A) ;
            AV9Cartao = GXutil.trim( AV9Cartao) ;
            if ( ( GXutil.len( AV9Cartao) < 13 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "1") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "2") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "7") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "8") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "9") == 0 ) || ( GXutil.strcmp(GXutil.substring( AV8PGInfo, 1, 1), "0") == 0 ) )
            {
               /* Execute user subroutine: S1165 */
               S1165 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
            }
            else
            {
               AV18Cartao = "Y" ;
            }
         }
         if ( ( GXutil.strcmp(AV18Cartao, "Y") == 0 ) )
         {
            GXt_char1 = AV17PGCCMA ;
            GXv_char2[0] = AV9Cartao ;
            GXv_char3[0] = GXt_char1 ;
            new pmask(remoteHandle, context).execute( GXv_char2, GXv_char3) ;
            pcartaocripmask.this.AV9Cartao = GXv_char2[0] ;
            pcartaocripmask.this.GXt_char1 = GXv_char3[0] ;
            AV17PGCCMA = GXt_char1 ;
            if ( ( GXutil.strcmp(GXutil.trim( AV17PGCCMA), "") != 0 ) )
            {
               GXt_char1 = AV16PGCCNU ;
               GXv_char3[0] = GXt_char1 ;
               new pcrypto(remoteHandle, context).execute( AV9Cartao, "E", GXv_char3) ;
               pcartaocripmask.this.GXt_char1 = GXv_char3[0] ;
               AV16PGCCNU = GXt_char1 ;
               AV12A = (short)(GXutil.strSearch( AV17PGCCMA, "*", 1)) ;
               AV12A = (short)(AV12A-1) ;
               AV13Cartao = GXutil.substring( AV17PGCCMA, 1, AV12A) ;
               AV11B = (short)(GXutil.strSearch( AV9Cartao, AV13Cartao, 1)) ;
               AV9Cartao = GXutil.trim( GXutil.substring( AV9Cartao, AV11B, 32)) ;
               AV8PGInfo = GXutil.strReplace( AV8PGInfo, AV9Cartao, AV17PGCCMA) ;
            }
            else
            {
               AV16PGCCNU = "" ;
               AV17PGCCMA = "" ;
               AV9Cartao = "" ;
            }
         }
         else
         {
            AV16PGCCNU = "" ;
            AV17PGCCMA = "" ;
            AV9Cartao = "" ;
         }
      }
      cleanup();
   }

   public void S1165( )
   {
      /* 'CARTAO_NUM' Routine */
      AV9Cartao = "" ;
      AV12A = (short)(0) ;
      AV11B = (short)(0) ;
      AV10I = (short)(1) ;
      while ( ( AV10I <= GXutil.len( AV8PGInfo) ) )
      {
         if ( ( GXutil.strcmp(GXutil.substring( AV8PGInfo, AV10I, 1), " ") == 0 ) )
         {
            AV12A = (short)(AV12A+1) ;
            if ( ( GXutil.len( AV9Cartao) > 12 ) && ( GXutil.strSearch( "0123456789", GXutil.substring( AV9Cartao, 1, 1), 1) != 0 ) )
            {
               if (true) break;
            }
            else
            {
               AV9Cartao = "" ;
            }
         }
         if ( ( AV12A == 2 ) )
         {
            if (true) break;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV8PGInfo, AV10I, 1), "/") == 0 ) )
         {
            AV11B = (short)(AV11B+1) ;
         }
         if ( ( AV11B == 2 ) )
         {
            if (true) break;
         }
         if ( ( GXutil.strcmp(GXutil.substring( AV8PGInfo, AV10I, 1), "*") == 0 ) )
         {
            if ( ( AV10I <= 5 ) )
            {
               AV9Cartao = "" ;
            }
            else
            {
               if (true) break;
            }
         }
         if ( ( GXutil.strSearch( "0123456789", GXutil.substring( AV8PGInfo, AV10I, 1), 1) != 0 ) )
         {
            AV9Cartao = AV9Cartao + GXutil.substring( AV8PGInfo, AV10I, 1) ;
         }
         if ( ( ( GXutil.strcmp(GXutil.substring( GXutil.trim( AV9Cartao), 1, 2), "37") == 0 ) && ( GXutil.len( GXutil.trim( AV9Cartao)) == 15 ) ) || ( ( GXutil.strcmp(GXutil.substring( GXutil.trim( AV9Cartao), 1, 2), "36") == 0 ) && ( GXutil.len( GXutil.trim( AV9Cartao)) == 14 ) ) || ( ( GXutil.strcmp(GXutil.substring( GXutil.trim( AV9Cartao), 1, 1), "4") == 0 ) && ( GXutil.len( GXutil.trim( AV9Cartao)) == 16 ) ) || ( ( GXutil.strcmp(GXutil.substring( GXutil.trim( AV9Cartao), 1, 1), "5") == 0 ) && ( GXutil.len( GXutil.trim( AV9Cartao)) == 16 ) ) || ( ( GXutil.strcmp(GXutil.substring( GXutil.trim( AV9Cartao), 1, 1), "6") == 0 ) && ( GXutil.len( GXutil.trim( AV9Cartao)) == 16 ) ) )
         {
            AV18Cartao = "Y" ;
            if (true) break;
         }
         if ( ( GXutil.len( AV9Cartao) == 1 ) && ( GXutil.strcmp(GXutil.substring( AV9Cartao, 1, 1), "0") == 0 ) )
         {
            AV9Cartao = "" ;
         }
         AV10I = (short)(AV10I+1) ;
      }
      if ( ( GXutil.strcmp(AV18Cartao, "N") == 0 ) && ( GXutil.len( AV9Cartao) > 12 ) )
      {
         AV18Cartao = "Y" ;
      }
   }

   protected void cleanup( )
   {
      this.aP0[0] = pcartaocripmask.this.AV8PGInfo;
      this.aP1[0] = pcartaocripmask.this.AV16PGCCNU;
      this.aP2[0] = pcartaocripmask.this.AV17PGCCMA;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV18Cartao = "" ;
      AV12A = (short)(0) ;
      AV9Cartao = "" ;
      returnInSub = false ;
      GXv_char2 = new String [1] ;
      GXt_char1 = "" ;
      GXv_char3 = new String [1] ;
      AV13Cartao = "" ;
      AV11B = (short)(0) ;
      AV10I = (short)(0) ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short AV12A ;
   private short AV11B ;
   private short AV10I ;
   private short Gx_err ;
   private String AV17PGCCMA ;
   private String AV18Cartao ;
   private String AV9Cartao ;
   private String GXv_char2[] ;
   private String GXt_char1 ;
   private String GXv_char3[] ;
   private String AV13Cartao ;
   private boolean returnInSub ;
   private String AV8PGInfo ;
   private String AV16PGCCNU ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
}


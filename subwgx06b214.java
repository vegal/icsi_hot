import com.genexus.*;
import com.genexus.ui.*;

public final  class subwgx06b214 extends GXSubfileElement
{
   private String AirLineCurCode ;
   private String AirLineCurTrans ;
   private String ISOCod ;
   private String ISOCodDescription ;
   private String AirLineCode ;
   public String getAirLineCurCode( )
   {
      return AirLineCurCode ;
   }

   public void setAirLineCurCode( String value )
   {
      AirLineCurCode = value;
   }

   public String getAirLineCurTrans( )
   {
      return AirLineCurTrans ;
   }

   public void setAirLineCurTrans( String value )
   {
      AirLineCurTrans = value;
   }

   public String getISOCod( )
   {
      return ISOCod ;
   }

   public void setISOCod( String value )
   {
      ISOCod = value;
   }

   public String getISOCodDescription( )
   {
      return ISOCodDescription ;
   }

   public void setISOCodDescription( String value )
   {
      ISOCodDescription = value;
   }

   public String getAirLineCode( )
   {
      return AirLineCode ;
   }

   public void setAirLineCode( String value )
   {
      AirLineCode = value;
   }

   public void clear( )
   {
      AirLineCurCode = "" ;
      AirLineCurTrans = "" ;
      ISOCod = "" ;
      AirLineCode = "" ;
   }

   public int compareTo( GXSubfileElement element ,
                         int column )
   {
      switch (column)
      {
            case 0 :
               return  getAirLineCurCode().compareTo(((subwgx06b214) element).getAirLineCurCode()) ;
            case 1 :
               return  getAirLineCurTrans().compareTo(((subwgx06b214) element).getAirLineCurTrans()) ;
      }
      return 0;
   }

   public boolean isEmpty( )
   {
      return ( ( GXutil.strcmp(getAirLineCurCode(), "") == 0 ) && ( GXutil.strcmp(getAirLineCurTrans(), "") == 0 ) && ( GXutil.strcmp(getISOCod(), "") == 0 ) && ( GXutil.strcmp(getAirLineCode(), "") == 0 ) )  ;
   }

   public void setColumn( GXComponent cell ,
                          int field )
   {
      switch (field) {
         case 0 :
            cell.setValue( getAirLineCurCode() );
            break;
         case 1 :
            cell.setValue( getAirLineCurTrans() );
            break;
      }
   }

   public String getColumn( int field )
   {
      switch (field) {
      }
      return "";
   }

   public boolean isFieldChanged( GXComponent cell ,
                                  int field )
   {
      switch (field) {
         case 0 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCurCode()) == 0) );
         case 1 :
            return ( ( GXutil.strcmp(((GUIObjectString) cell).getValue(),getAirLineCurTrans()) == 0) );
      }
      return false;
   }

   public void setField( int i ,
                         GXComponent value )
   {
      switch (i)
      {
            case 0 :
               setAirLineCurCode(value.getStringValue());
               break;
            case 1 :
               setAirLineCurTrans(value.getStringValue());
               break;
      }
   }

   public void setField( int i ,
                         GXSubfileElement element )
   {
      switch (i)
      {
            case 0 :
               setAirLineCurCode(((subwgx06b214) element).getAirLineCurCode());
               return;
            case 1 :
               setAirLineCurTrans(((subwgx06b214) element).getAirLineCurTrans());
               return;
      }
   }

}


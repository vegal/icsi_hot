
public final  class StructSdtIT0A_IT0AItem implements Cloneable, java.io.Serializable
{
   public StructSdtIT0A_IT0AItem( )
   {
      gxTv_SdtIT0A_IT0AItem_Rfid_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Bera_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Icdn_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Cdgt_it0a = (byte)(0) ;
      gxTv_SdtIT0A_IT0AItem_Amil_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Rfic_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpoc_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpeq_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpev_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mpsc_it0a = "" ;
      gxTv_SdtIT0A_IT0AItem_Mptx_it0a = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getRfid_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Rfid_it0a ;
   }

   public void setRfid_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Rfid_it0a = value ;
      return  ;
   }

   public String getBera_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Bera_it0a ;
   }

   public void setBera_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Bera_it0a = value ;
      return  ;
   }

   public String getIcdn_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Icdn_it0a ;
   }

   public void setIcdn_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Icdn_it0a = value ;
      return  ;
   }

   public byte getCdgt_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Cdgt_it0a ;
   }

   public void setCdgt_it0a( byte value )
   {
      gxTv_SdtIT0A_IT0AItem_Cdgt_it0a = value ;
      return  ;
   }

   public String getAmil_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Amil_it0a ;
   }

   public void setAmil_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Amil_it0a = value ;
      return  ;
   }

   public String getRfic_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Rfic_it0a ;
   }

   public void setRfic_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Rfic_it0a = value ;
      return  ;
   }

   public String getMpoc_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpoc_it0a ;
   }

   public void setMpoc_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpoc_it0a = value ;
      return  ;
   }

   public String getMpeq_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpeq_it0a ;
   }

   public void setMpeq_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpeq_it0a = value ;
      return  ;
   }

   public String getMpev_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpev_it0a ;
   }

   public void setMpev_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpev_it0a = value ;
      return  ;
   }

   public String getMpsc_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mpsc_it0a ;
   }

   public void setMpsc_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mpsc_it0a = value ;
      return  ;
   }

   public String getMptx_it0a( )
   {
      return gxTv_SdtIT0A_IT0AItem_Mptx_it0a ;
   }

   public void setMptx_it0a( String value )
   {
      gxTv_SdtIT0A_IT0AItem_Mptx_it0a = value ;
      return  ;
   }

   protected byte gxTv_SdtIT0A_IT0AItem_Cdgt_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Rfid_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Bera_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Icdn_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Amil_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Rfic_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Mpoc_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Mpeq_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Mpev_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Mpsc_it0a ;
   protected String gxTv_SdtIT0A_IT0AItem_Mptx_it0a ;
}


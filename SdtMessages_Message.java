import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtMessages_Message extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtMessages_Message( )
   {
      this(  new ModelContext(SdtMessages_Message.class));
   }

   public SdtMessages_Message( ModelContext context )
   {
      super( context, "SdtMessages_Message");
   }

   public SdtMessages_Message( StructSdtMessages_Message struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Id") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "Genexus") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessages_Message_Id = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Type") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "Genexus") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessages_Message_Type = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Description") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "Genexus") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtMessages_Message_Description = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Messages.Message" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "Genexus") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "Genexus") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "Genexus");
      }
      oWriter.writeElement("Id", GXutil.rtrim( gxTv_SdtMessages_Message_Id));
      oWriter.writeElement("Type", GXutil.trim( GXutil.str( gxTv_SdtMessages_Message_Type, 2, 0)));
      oWriter.writeElement("Description", GXutil.rtrim( gxTv_SdtMessages_Message_Description));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtMessages_Message_Id( )
   {
      return gxTv_SdtMessages_Message_Id ;
   }

   public void setgxTv_SdtMessages_Message_Id( String value )
   {
      gxTv_SdtMessages_Message_Id = value ;
      return  ;
   }

   public void setgxTv_SdtMessages_Message_Id_SetNull( )
   {
      gxTv_SdtMessages_Message_Id = "" ;
      return  ;
   }

   public byte getgxTv_SdtMessages_Message_Type( )
   {
      return gxTv_SdtMessages_Message_Type ;
   }

   public void setgxTv_SdtMessages_Message_Type( byte value )
   {
      gxTv_SdtMessages_Message_Type = value ;
      return  ;
   }

   public void setgxTv_SdtMessages_Message_Type_SetNull( )
   {
      gxTv_SdtMessages_Message_Type = (byte)(0) ;
      return  ;
   }

   public String getgxTv_SdtMessages_Message_Description( )
   {
      return gxTv_SdtMessages_Message_Description ;
   }

   public void setgxTv_SdtMessages_Message_Description( String value )
   {
      gxTv_SdtMessages_Message_Description = value ;
      return  ;
   }

   public void setgxTv_SdtMessages_Message_Description_SetNull( )
   {
      gxTv_SdtMessages_Message_Description = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtMessages_Message_Id = "" ;
      gxTv_SdtMessages_Message_Type = (byte)(0) ;
      gxTv_SdtMessages_Message_Description = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtMessages_Message Clone( )
   {
      return (SdtMessages_Message)(clone()) ;
   }

   public void setStruct( StructSdtMessages_Message struct )
   {
      setgxTv_SdtMessages_Message_Id(struct.getId());
      setgxTv_SdtMessages_Message_Type(struct.getType());
      setgxTv_SdtMessages_Message_Description(struct.getDescription());
   }

   public StructSdtMessages_Message getStruct( )
   {
      StructSdtMessages_Message struct = new StructSdtMessages_Message ();
      struct.setId(getgxTv_SdtMessages_Message_Id());
      struct.setType(getgxTv_SdtMessages_Message_Type());
      struct.setDescription(getgxTv_SdtMessages_Message_Description());
      return struct ;
   }

   protected byte gxTv_SdtMessages_Message_Type ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtMessages_Message_Id ;
   protected String gxTv_SdtMessages_Message_Description ;
}


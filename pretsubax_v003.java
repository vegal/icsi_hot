/*
               File: RETSubAX_V003
        Description: Gera arquivo de submiss�o AMEX vers�o 003.
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:18.75
       Program type: Main program
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;

public final  class pretsubax_v003 extends GXProcedure
{
   public pretsubax_v003( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pretsubax_v003.class ), "" );
   }

   public pretsubax_v003( int remoteHandle ,
                          ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 )
   {
      execute_int(aP0);
   }

   private void execute_int( String[] aP0 )
   {
      pretsubax_v003.this.AV10DebugM = aP0[0];
      this.aP0 = aP0;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      GXt_char1 = AV222Airli ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_AIRLINE_AX", "Airline que deve ser utilizada para submiss�o TAM", "S", "571", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV222Airli = GXt_char1 ;
      GXt_char1 = AV223Airli ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_AIRLINE_TAM", "Airline TAM", "S", "957", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV223Airli = GXt_char1 ;
      GXt_char1 = AV263UsaNo ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "USA_NOVA_BAND_HC", "Par�metro que indica se no arquivo de submiss�o Redecard a nova bandeira HC deve ser utilizada", "S", "Y", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV263UsaNo = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV264Bande ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER", "C�digo do cart�o Hipercard", "S", "HC", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV264Bande = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV265NovaB ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_HIPER_NEW", "C�digo do cart�o Hipercard para arquivos output", "S", "HP", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV265NovaB = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV266Bande ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO", "C�digo do cart�o ELO", "S", "EL", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV266Bande = GXutil.trim( GXt_char1) ;
      GXt_char1 = AV267NovaB ;
      GXv_svchar2[0] = GXt_char1 ;
      new pr2getparm(remoteHandle, context).execute( "PGCCTYPE_ELO_NEW", "C�digo do cart�o ELO para arquivos output", "S", "EL", GXv_svchar2) ;
      pretsubax_v003.this.GXt_char1 = GXv_svchar2[0] ;
      AV267NovaB = GXutil.trim( GXt_char1) ;
      AV84Versao = "Layout Maio/2015 TASK 48466" ;
      AV10DebugM = GXutil.trim( GXutil.upper( AV10DebugM)) ;
      context.msgStatus( "Amex- Submission - "+AV84Versao );
      context.msgStatus( "Running mode: ["+AV10DebugM+"] - Started at "+GXutil.time( ) );
      context.msgStatus( "Creating Amex Submission File" );
      if ( ( GXutil.strSearch( AV10DebugM, "SEPPIPE", 1) > 0 ) )
      {
         AV64Sep = "|" ;
      }
      else
      {
         AV64Sep = "" ;
      }
      if ( ( GXutil.strSearch( AV10DebugM, "TESTMODE", 1) > 0 ) )
      {
         AV108TestM = (byte)(1) ;
         AV198RetSu = "P" ;
      }
      else
      {
         AV108TestM = (byte)(0) ;
         AV198RetSu = "D" ;
      }
      /* Using cursor P007L2 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A1147lccbE = P007L2_A1147lccbE[0] ;
         n1147lccbE = P007L2_n1147lccbE[0] ;
         A1150lccbE = P007L2_A1150lccbE[0] ;
         AV144lccbE = A1150lccbE ;
         AV194TipoM = (byte)(0) ;
         AV195TotRe = 0 ;
         AV196TotRe = 0 ;
         while ( ( AV194TipoM <= 1 ) )
         {
            AV8DataB = GXutil.trim( GXutil.str( GXutil.year( Gx_date), 10, 0)) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.month( Gx_date)+100, 10, 0)), 2, 3) + GXutil.substring( GXutil.trim( GXutil.str( GXutil.day( Gx_date)+100, 10, 0)), 2, 3) ;
            AV8DataB = GXutil.ltrim( GXutil.rtrim( AV8DataB)) ;
            AV213ArqDa = GXutil.now(true, false) ;
            AV27HoraA = GXutil.time( ) ;
            AV28HoraB = GXutil.substring( AV27HoraA, 1, 2) + GXutil.substring( AV27HoraA, 4, 2) + GXutil.substring( AV27HoraA, 7, 2) ;
            AV28HoraB = GXutil.ltrim( GXutil.rtrim( AV28HoraB)) ;
            AV159NumRe = 0 ;
            if ( ( AV194TipoM == 0 ) )
            {
               AV216InicL = "S" ;
               AV215TotLi = 0 ;
               AV197Trans = "001" ;
               AV212W = (short)(AV212W+1) ;
               AV232Total = 0 ;
               AV235SeqLi = 0 ;
               /* Execute user subroutine: S1179 */
               S1179 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
            }
            else
            {
               AV216InicL = "S" ;
               AV197Trans = "002" ;
               AV212W = (short)(AV212W+1) ;
               AV215TotLi = 0 ;
               AV232Total = 0 ;
               AV235SeqLi = 0 ;
               /* Execute user subroutine: S1179 */
               S1179 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
            }
            AV194TipoM = (byte)(AV194TipoM+1) ;
            if ( ( AV194TipoM > 1 ) )
            {
               if (true) break;
            }
         }
         context.msgStatus( "Empresa: "+GXutil.trim( AV144lccbE) );
         context.msgStatus( "Total Regs a vista = "+GXutil.str( AV195TotRe, 6, 0) );
         context.msgStatus( "Total Regs a prazo = "+GXutil.str( AV196TotRe, 6, 0) );
         pr_default.readNext(0);
      }
      pr_default.close(0);
      context.msgStatus( "Submiss�o conclu�da com sucesso" );
      cleanup();
   }

   public void S1179( )
   {
      /* 'MAIN' Routine */
      AV140Short = "AMEX_" + GXutil.trim( AV8DataB) + AV28HoraB + "SUBAX" ;
      if ( ( AV108TestM == 1 ) )
      {
         AV140Short = "Prv" + AV140Short ;
      }
      AV256Audit = context.getWorkstationId( remoteHandle) + " - " + context.getUserId( "LocalHost", remoteHandle, "DEFAULT") + " In�cio gera��o arquivo " + GXutil.trim( AV140Short) ;
      AV253Audit = "Localhost" ;
      AV254Audit = "TIES_ICSI" ;
      AV255Audit = "PCI" ;
      GXv_svchar2[0] = AV253Audit ;
      GXv_svchar5[0] = AV254Audit ;
      GXv_svchar6[0] = AV255Audit ;
      GXv_svchar7[0] = AV256Audit ;
      new pnewaudit(remoteHandle, context).execute( GXv_svchar2, GXv_svchar5, GXv_svchar6, GXv_svchar7) ;
      pretsubax_v003.this.AV253Audit = GXv_svchar2[0] ;
      pretsubax_v003.this.AV254Audit = GXv_svchar5[0] ;
      pretsubax_v003.this.AV255Audit = GXv_svchar6[0] ;
      pretsubax_v003.this.AV256Audit = GXv_svchar7[0] ;
      GXt_char4 = AV13FileNa ;
      GXv_svchar7[0] = GXt_char4 ;
      new pr2getparm(remoteHandle, context).execute( "ICSI_SUB_AX", "Caminho dos arquivos de submiss�o Amex", "F", "C:\\TEMP\\ICSI\\AX", GXv_svchar7) ;
      pretsubax_v003.this.GXt_char4 = GXv_svchar7[0] ;
      AV13FileNa = GXt_char4 ;
      AV13FileNa = AV13FileNa + GXutil.trim( AV140Short) + GXutil.trim( AV197Trans) + ".txt" ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwopen( AV13FileNa, AV64Sep, "", (byte)(0), "") ;
      context.msgStatus( "Creating file "+AV13FileNa );
      AV190ValTo = 0 ;
      AV191ValTo = 0 ;
      AV192ValTo = 0 ;
      AV193ValTo = 0 ;
      AV241Heade = "Y" ;
      pr_default.dynParam(1, new Object[]{ new Object[]{
                                           AV197Trans ,
                                           new Short(A1168lccbI) ,
                                           A1490Distr ,
                                           new Double(A1172lccbS) ,
                                           A1184lccbS ,
                                           A1150lccbE ,
                                           AV144lccbE ,
                                           A1227lccbO },
                                           new int[] {
                                           TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DOUBLE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                           TypeConstants.STRING, TypeConstants.STRING
                                           }
      });
      /* Using cursor P007L3 */
      pr_default.execute(1, new Object[] {AV144lccbE});
      while ( (pr_default.getStatus(1) != 101) )
      {
         A1228lccbF = P007L3_A1228lccbF[0] ;
         A1227lccbO = P007L3_A1227lccbO[0] ;
         A1225lccbC = P007L3_A1225lccbC[0] ;
         A1223lccbD = P007L3_A1223lccbD[0] ;
         A1222lccbI = P007L3_A1222lccbI[0] ;
         A1150lccbE = P007L3_A1150lccbE[0] ;
         A1226lccbA = P007L3_A1226lccbA[0] ;
         A1224lccbC = P007L3_A1224lccbC[0] ;
         A1172lccbS = P007L3_A1172lccbS[0] ;
         n1172lccbS = P007L3_n1172lccbS[0] ;
         A1168lccbI = P007L3_A1168lccbI[0] ;
         n1168lccbI = P007L3_n1168lccbI[0] ;
         A1490Distr = P007L3_A1490Distr[0] ;
         n1490Distr = P007L3_n1490Distr[0] ;
         A1184lccbS = P007L3_A1184lccbS[0] ;
         n1184lccbS = P007L3_n1184lccbS[0] ;
         A1179lccbV = P007L3_A1179lccbV[0] ;
         n1179lccbV = P007L3_n1179lccbV[0] ;
         A1171lccbT = P007L3_A1171lccbT[0] ;
         n1171lccbT = P007L3_n1171lccbT[0] ;
         A1170lccbI = P007L3_A1170lccbI[0] ;
         n1170lccbI = P007L3_n1170lccbI[0] ;
         A1189lccbS = P007L3_A1189lccbS[0] ;
         n1189lccbS = P007L3_n1189lccbS[0] ;
         A1192lccbS = P007L3_A1192lccbS[0] ;
         n1192lccbS = P007L3_n1192lccbS[0] ;
         A1190lccbS = P007L3_A1190lccbS[0] ;
         n1190lccbS = P007L3_n1190lccbS[0] ;
         A1191lccbS = P007L3_A1191lccbS[0] ;
         n1191lccbS = P007L3_n1191lccbS[0] ;
         A1193lccbS = P007L3_A1193lccbS[0] ;
         n1193lccbS = P007L3_n1193lccbS[0] ;
         A1185lccbB = P007L3_A1185lccbB[0] ;
         n1185lccbB = P007L3_n1185lccbB[0] ;
         A1195lccbS = P007L3_A1195lccbS[0] ;
         n1195lccbS = P007L3_n1195lccbS[0] ;
         A1194lccbS = P007L3_A1194lccbS[0] ;
         n1194lccbS = P007L3_n1194lccbS[0] ;
         if ( ( GXutil.strcmp(GXutil.substring( A1490Distr, 1, 1), "A") == 0 ) )
         {
            if ( ( A1172lccbS > 0.00 ) )
            {
               if ( ( GXutil.strcmp(A1184lccbS, "TOSUB") == 0 ) )
               {
                  if ( ( GXutil.strcmp(A1150lccbE, AV144lccbE) == 0 ) )
                  {
                     AV224lccbE = A1150lccbE ;
                     AV145lccbD = A1223lccbD ;
                     AV146lccbC = A1224lccbC ;
                     if ( ( GXutil.strcmp(AV263UsaNo, "Y") == 0 ) )
                     {
                        if ( ( GXutil.strcmp(A1224lccbC, AV264Bande) == 0 ) )
                        {
                           AV146lccbC = AV265NovaB ;
                        }
                        else if ( ( GXutil.strcmp(A1224lccbC, AV266Bande) == 0 ) )
                        {
                           AV146lccbC = AV267NovaB ;
                        }
                        else
                        {
                           AV146lccbC = A1224lccbC ;
                        }
                     }
                     if ( ( GXutil.strcmp(AV241Heade, "Y") == 0 ) )
                     {
                        /* Execute user subroutine: S123 */
                        S123 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           returnInSub = true;
                           if (true) return;
                        }
                        AV241Heade = "N" ;
                     }
                     AV250Atrib = A1225lccbC ;
                     GXv_svchar7[0] = AV252Resul ;
                     new pcrypto(remoteHandle, context).execute( AV250Atrib, "D", GXv_svchar7) ;
                     pretsubax_v003.this.AV252Resul = GXv_svchar7[0] ;
                     AV137lccbC = AV252Resul ;
                     AV136lccbA = A1226lccbA ;
                     AV248sInpu = AV136lccbA ;
                     AV249sOutp = "" ;
                     AV245j = (byte)(1) ;
                     while ( ( AV245j <= GXutil.len( AV248sInpu) ) )
                     {
                        AV247c = GXutil.substring( AV248sInpu, AV245j, 1) ;
                        if ( ( GXutil.strSearch( "0123456789", AV247c, 1) > 0 ) )
                        {
                           AV249sOutp = AV249sOutp + AV247c ;
                        }
                        else
                        {
                           AV249sOutp = AV249sOutp + "0" ;
                        }
                        AV245j = (byte)(AV245j+1) ;
                     }
                     AV136lccbA = AV249sOutp ;
                     AV147lccbO = A1227lccbO ;
                     AV148lccbF = A1228lccbF ;
                     AV182TotRe = (int)(AV182TotRe+1) ;
                     AV159NumRe = (int)(AV159NumRe+1) ;
                     AV235SeqLi = (int)(AV235SeqLi+1) ;
                     AV234lccbS = GXutil.trim( AV146lccbC) + GXutil.padl( GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0)), (short)(6), "0") + GXutil.padl( GXutil.trim( GXutil.str( AV235SeqLi, 10, 0)), (short)(5), "0") ;
                     AV229DataA = AV145lccbD ;
                     AV230lccbD = GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV229DataA), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV229DataA), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV229DataA), 10, 0)), (short)(2), "0") ;
                     AV261DataV = GXutil.substring( A1179lccbV, 3, 2) + GXutil.substring( A1179lccbV, 1, 2) ;
                     if ( ( GXutil.strcmp(AV197Trans, "001") == 0 ) )
                     {
                        AV238QtdeP = "00" ;
                        AV239lccbT = "0" ;
                        AV240lccbD = "0" ;
                        AV227lccbS = GXutil.str( A1172lccbS, 14, 2) ;
                        AV232Total = (double)(AV232Total+A1172lccbS) ;
                     }
                     else
                     {
                        AV238QtdeP = GXutil.padl( GXutil.trim( GXutil.str( A1168lccbI, 10, 0)), (short)(2), "0") ;
                        AV239lccbT = GXutil.str( A1171lccbT, 14, 2) ;
                        AV240lccbD = "0" ;
                        AV244VlrPr = (double)(A1170lccbI+A1171lccbT) ;
                        AV227lccbS = GXutil.str( AV244VlrPr, 14, 2) ;
                        AV232Total = (double)(AV232Total+AV244VlrPr) ;
                     }
                     AV239lccbT = GXutil.strReplace( GXutil.trim( AV239lccbT), ".", "") ;
                     AV239lccbT = GXutil.strReplace( GXutil.trim( AV239lccbT), ",", "") ;
                     AV227lccbS = GXutil.strReplace( GXutil.trim( AV227lccbS), ".", "") ;
                     AV227lccbS = GXutil.strReplace( GXutil.trim( AV227lccbS), ",", "") ;
                     AV240lccbD = GXutil.strReplace( GXutil.trim( AV240lccbD), ".", "") ;
                     AV240lccbD = GXutil.strReplace( GXutil.trim( AV240lccbD), ",", "") ;
                     AV243CodAu = GXutil.trim( AV136lccbA) ;
                     AV262CodAu = "" ;
                     AV276K = (byte)(1) ;
                     while ( ( AV276K <= 6 ) )
                     {
                        if ( ( GXutil.strSearch( "1234567890", GXutil.substring( AV243CodAu, AV276K, 1), 1) == 0 ) )
                        {
                           AV262CodAu = AV262CodAu + "0" ;
                        }
                        else
                        {
                           AV262CodAu = AV262CodAu + GXutil.substring( AV243CodAu, AV276K, 1) ;
                        }
                        AV276K = (byte)(AV276K+1) ;
                     }
                     AV243CodAu = AV262CodAu ;
                     AV228Texto = "D" + GXutil.padl( GXutil.trim( AV242CodEs), (short)(10), "0") + "0" + GXutil.padl( GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0)), (short)(6), "0") + GXutil.padl( GXutil.trim( AV136lccbA), (short)(7), "0") + GXutil.padr( GXutil.trim( AV230lccbD), (short)(6), " ") ;
                     AV228Texto = AV228Texto + "1" + GXutil.padl( GXutil.trim( AV137lccbC), (short)(17), "0") + "D" + GXutil.padl( GXutil.trim( AV227lccbS), (short)(17), "0") + GXutil.trim( AV238QtdeP) + "00R" + GXutil.padl( GXutil.trim( AV240lccbD), (short)(17), "0") ;
                     AV228Texto = AV228Texto + GXutil.padl( GXutil.trim( AV239lccbT), (short)(17), "0") + GXutil.padl( GXutil.trim( AV243CodAu), (short)(6), "0") + GXutil.padl( GXutil.trim( AV261DataV), (short)(4), "0") + GXutil.padr( GXutil.trim( AV234lccbS), (short)(21), " ") ;
                     AV141Refer = GXutil.trim( AV146lccbC) + AV160Txt + AV165Batch ;
                     AV175FlgTk = "S" ;
                     AV176Conta = (byte)(1) ;
                     /* Using cursor P007L4 */
                     pr_default.execute(2, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                     while ( (pr_default.getStatus(2) != 101) )
                     {
                        A1231lccbT = P007L4_A1231lccbT[0] ;
                        A1207lccbP = P007L4_A1207lccbP[0] ;
                        n1207lccbP = P007L4_n1207lccbP[0] ;
                        A1232lccbT = P007L4_A1232lccbT[0] ;
                        if ( ( GXutil.strcmp(AV175FlgTk, "S") != 0 ) )
                        {
                           AV159NumRe = (int)(AV159NumRe+1) ;
                           AV176Conta = (byte)(AV176Conta+1) ;
                        }
                        else
                        {
                           AV175FlgTk = "N" ;
                        }
                        AV118lccbT[AV176Conta-1] = A1231lccbT ;
                        AV177DbIat[AV176Conta-1] = (int)(GXutil.val( AV130lccbI, ".")) ;
                        AV178DbCCc[AV176Conta-1] = (long)(GXutil.val( A1231lccbT, ".")) ;
                        AV179DbPas[AV176Conta-1] = GXutil.substring( A1207lccbP, 1, 40) ;
                        AV218DbLcc[AV176Conta-1] = A1226lccbA ;
                        AV181DbNum[AV176Conta-1] = (short)(AV159NumRe) ;
                        if ( ( AV108TestM == 0 ) )
                        {
                           /*
                              INSERT RECORD ON TABLE LCCBPLP1

                           */
                           A1229lccbS = GXutil.now(true, false) ;
                           A1186lccbS = "PROCPLP" ;
                           n1186lccbS = false ;
                           A1187lccbS = "Amex - Submiss�o realizada" ;
                           n1187lccbS = false ;
                           /* Using cursor P007L5 */
                           pr_default.execute(3, new Object[] {A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF, A1229lccbS, new Short(A1230lccbS), new Boolean(n1186lccbS), A1186lccbS, new Boolean(n1187lccbS), A1187lccbS});
                           if ( (pr_default.getStatus(3) == 1) )
                           {
                              Gx_err = (short)(1) ;
                              Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
                           }
                           else
                           {
                              Gx_err = (short)(0) ;
                              Gx_emsg = "" ;
                           }
                           /* End Insert */
                        }
                        pr_default.readNext(2);
                     }
                     pr_default.close(2);
                     AV169Num11 = (long)(GXutil.val( AV118lccbT[1-1], ".")) ;
                     AV231Bilhe = GXutil.trim( A1150lccbE) + GXutil.trim( GXutil.str( AV169Num11, 10, 0)) ;
                     AV228Texto = AV228Texto + GXutil.padr( GXutil.trim( AV231Bilhe), (short)(13), "0") ;
                     AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV228Texto, (short)(150)) ;
                     AV215TotLi = (int)(AV215TotLi+1) ;
                     AV270Total = (int)(AV270Total+1) ;
                     AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
                     AV190ValTo = (long)(AV190ValTo+AV214LccbS) ;
                     AV191ValTo = (long)(AV191ValTo+AV143lccbD) ;
                     AV192ValTo = (long)(AV192ValTo+AV134lccbT) ;
                     AV193ValTo = (long)(AV193ValTo+AV133lccbI) ;
                     AV278Lccbs = GXutil.trim( AV140Short) + GXutil.trim( AV197Trans) ;
                     AV278Lccbs = GXutil.right( AV278Lccbs, 20) ;
                     if ( ( AV108TestM == 0 ) )
                     {
                        A1184lccbS = "PROCPLP" ;
                        n1184lccbS = false ;
                        A1189lccbS = Gx_date ;
                        n1189lccbS = false ;
                        A1192lccbS = GXutil.trim( AV278Lccbs) ;
                        n1192lccbS = false ;
                        A1190lccbS = GXutil.now(true, false) ;
                        n1190lccbS = false ;
                        A1191lccbS = "F" ;
                        n1191lccbS = false ;
                        A1193lccbS = AV234lccbS ;
                        n1193lccbS = false ;
                        A1185lccbB = GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0)) ;
                        n1185lccbB = false ;
                        A1195lccbS = AV242CodEs ;
                        n1195lccbS = false ;
                        A1194lccbS = GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0)) ;
                        n1194lccbS = false ;
                     }
                     /* Using cursor P007L6 */
                     pr_default.execute(4, new Object[] {new Boolean(n1184lccbS), A1184lccbS, new Boolean(n1189lccbS), A1189lccbS, new Boolean(n1192lccbS), A1192lccbS, new Boolean(n1190lccbS), A1190lccbS, new Boolean(n1191lccbS), A1191lccbS, new Boolean(n1193lccbS), A1193lccbS, new Boolean(n1185lccbB), A1185lccbB, new Boolean(n1195lccbS), A1195lccbS, new Boolean(n1194lccbS), A1194lccbS, A1150lccbE, A1222lccbI, A1223lccbD, A1224lccbC, A1225lccbC, A1226lccbA, A1227lccbO, A1228lccbF});
                  }
               }
            }
         }
         pr_default.readNext(1);
      }
      pr_default.close(1);
      if ( ( AV215TotLi > 0 ) )
      {
         AV270Total = (int)(AV270Total+1) ;
         AV159NumRe = (int)(AV159NumRe+1) ;
         AV233Total = GXutil.trim( GXutil.str( AV232Total, 14, 2)) ;
         AV233Total = GXutil.strReplace( GXutil.trim( AV233Total), ".", "") ;
         AV233Total = GXutil.strReplace( GXutil.trim( AV233Total), ",", "") ;
         AV228Texto = "T" + GXutil.padl( GXutil.trim( GXutil.str( AV270Total, 10, 0)), (short)(5), "0") + GXutil.padl( GXutil.trim( AV233Total), (short)(17), "0") + GXutil.space( (short)(31)) + GXutil.space( (short)(95)) ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV228Texto, (short)(150)) ;
         AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      }
      AV157tSale = (double)(AV157tSale/ (double) (100)) ;
      AV158tSale = (double)(AV158tSale/ (double) (100)) ;
      AV195TotRe = AV215TotLi ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwclose( ) ;
      GXt_char4 = AV268Proce ;
      GXv_svchar7[0] = GXt_char4 ;
      new pr2getparm(remoteHandle, context).execute( "PROCESSA_SCHECK", "Verifica se processa o sanity check", "S", "N", GXv_svchar7) ;
      pretsubax_v003.this.GXt_char4 = GXv_svchar7[0] ;
      AV268Proce = GXt_char4 ;
      if ( ( GXutil.strcmp(AV268Proce, "Y") == 0 ) )
      {
         context.msgStatus( "Sanity Check " );
         GXv_svchar7[0] = AV269msgEr ;
         new psanitycheckax(remoteHandle, context).execute( AV13FileNa, AV144lccbE, GXv_svchar7) ;
         pretsubax_v003.this.AV269msgEr = GXv_svchar7[0] ;
         if ( ((GXutil.strcmp("", GXutil.rtrim( AV269msgEr))==0)) )
         {
            GXt_char4 = AV257PathC ;
            GXv_svchar7[0] = GXt_char4 ;
            new pr2getparm(remoteHandle, context).execute( "PATHCOPSUBAX", "Caminho C�pia Submiss�o AX", "F", "C:\\R2Tech\\Sistemas\\ICSI\\TIVIT\\AX", GXv_svchar7) ;
            pretsubax_v003.this.GXt_char4 = GXv_svchar7[0] ;
            AV257PathC = GXt_char4 ;
            AV258Diret.setSource( GXutil.trim( AV257PathC) );
            if ( AV258Diret.exists() )
            {
               AV260File.setSource( GXutil.trim( AV13FileNa) );
               GXt_char4 = AV140Short ;
               GXv_svchar7[0] = AV13FileNa ;
               GXv_svchar6[0] = GXt_char4 ;
               new pr2shortname(remoteHandle, context).execute( GXv_svchar7, GXv_svchar6) ;
               pretsubax_v003.this.AV13FileNa = GXv_svchar7[0] ;
               pretsubax_v003.this.GXt_char4 = GXv_svchar6[0] ;
               AV140Short = GXt_char4 ;
               AV259Filen = GXutil.trim( AV257PathC) + "\\" + GXutil.trim( AV140Short) ;
               AV260File.copy(AV259Filen);
            }
         }
         else
         {
            context.msgStatus( AV269msgEr );
         }
      }
   }

   public void S123( )
   {
      /* 'RECORDHA' Routine */
      AV115Total = 0 ;
      AV116Total = 0 ;
      AV153SeqFi = 0 ;
      AV154cntCC = 0 ;
      AV155cntPL = 0 ;
      AV157tSale = 0 ;
      AV158tSale = 0 ;
      AV215TotLi = 0 ;
      AV279GXLvl = (byte)(0) ;
      /* Using cursor P007L7 */
      pr_default.execute(5);
      while ( (pr_default.getStatus(5) != 101) )
      {
         A1488ICSI_ = P007L7_A1488ICSI_[0] ;
         A1487ICSI_ = P007L7_A1487ICSI_[0] ;
         A1480ICSI_ = P007L7_A1480ICSI_[0] ;
         n1480ICSI_ = P007L7_n1480ICSI_[0] ;
         A1481ICSI_ = P007L7_A1481ICSI_[0] ;
         n1481ICSI_ = P007L7_n1481ICSI_[0] ;
         A1485ICSI_ = P007L7_A1485ICSI_[0] ;
         n1485ICSI_ = P007L7_n1485ICSI_[0] ;
         A1479ICSI_ = P007L7_A1479ICSI_[0] ;
         n1479ICSI_ = P007L7_n1479ICSI_[0] ;
         A1478ICSI_ = P007L7_A1478ICSI_[0] ;
         n1478ICSI_ = P007L7_n1478ICSI_[0] ;
         AV279GXLvl = (byte)(1) ;
         AV150ICSI_ = A1480ICSI_ ;
         AV151ICSI_ = A1481ICSI_ ;
         AV139ICSI_ = GXutil.trim( A1485ICSI_) ;
         if ( ( AV108TestM == 0 ) )
         {
            A1479ICSI_ = (int)(A1479ICSI_+1) ;
            n1479ICSI_ = false ;
         }
         AV32ICSI_C = A1479ICSI_ ;
         AV30ICSI_C = A1478ICSI_ ;
         AV163ICSI_ = AV32ICSI_C ;
         context.msgStatus( "Info: File sequence="+GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0))+", Batch="+GXutil.trim( GXutil.str( AV30ICSI_C, 10, 0)) );
         /* Using cursor P007L8 */
         pr_default.execute(6, new Object[] {new Boolean(n1479ICSI_), new Integer(A1479ICSI_), A1487ICSI_, A1488ICSI_});
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(5);
      if ( ( AV279GXLvl == 0 ) )
      {
         AV139ICSI_ = "R2" ;
         AV32ICSI_C = 1 ;
         AV30ICSI_C = 1 ;
         /*
            INSERT RECORD ON TABLE ICSI_CCINFO

         */
         A1487ICSI_ = "000" ;
         A1488ICSI_ = "AX" ;
         A1485ICSI_ = AV139ICSI_ ;
         n1485ICSI_ = false ;
         A1479ICSI_ = AV32ICSI_C ;
         n1479ICSI_ = false ;
         A1478ICSI_ = AV30ICSI_C ;
         n1478ICSI_ = false ;
         A1480ICSI_ = "9080158454" ;
         n1480ICSI_ = false ;
         A1481ICSI_ = "9080158496" ;
         n1481ICSI_ = false ;
         /* Using cursor P007L9 */
         pr_default.execute(7, new Object[] {A1487ICSI_, A1488ICSI_, new Boolean(n1478ICSI_), new Integer(A1478ICSI_), new Boolean(n1479ICSI_), new Integer(A1479ICSI_), new Boolean(n1480ICSI_), A1480ICSI_, new Boolean(n1481ICSI_), A1481ICSI_, new Boolean(n1485ICSI_), A1485ICSI_});
         if ( (pr_default.getStatus(7) == 1) )
         {
            Gx_err = (short)(1) ;
            Gx_emsg = localUtil.getMessages().getMessage("noupdate") ;
         }
         else
         {
            Gx_err = (short)(0) ;
            Gx_emsg = "" ;
         }
         /* End Insert */
         context.msgStatus( "ERROR: AMEX not configured" );
      }
      AV159NumRe = 1 ;
      AV229DataA = GXutil.resetTime(GXutil.serverNow( context, remoteHandle, "DEFAULT")) ;
      AV226DataA = GXutil.substring( GXutil.trim( GXutil.str( GXutil.year( AV229DataA), 10, 0)), 3, 2) + GXutil.padl( GXutil.trim( GXutil.str( GXutil.month( AV229DataA), 10, 0)), (short)(2), "0") + GXutil.padl( GXutil.trim( GXutil.str( GXutil.day( AV229DataA), 10, 0)), (short)(2), "0") ;
      if ( ( GXutil.strcmp(AV197Trans, "001") == 0 ) )
      {
         AV242CodEs = GXutil.padl( GXutil.trim( AV150ICSI_), (short)(10), "0") ;
      }
      else
      {
         AV242CodEs = GXutil.padl( GXutil.trim( AV151ICSI_), (short)(10), "0") ;
      }
      AV236Dados = GXutil.trim( AV146lccbC) + GXutil.padl( GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0)), (short)(6), "0") ;
      AV237TipoV = ((GXutil.strcmp(AV197Trans, "001")==0) ? "1" : "0") ;
      AV228Texto = "H" + GXutil.trim( AV242CodEs) + GXutil.padl( AV226DataA, (short)(6), " ") + "3" + GXutil.padl( GXutil.trim( GXutil.str( AV32ICSI_C, 10, 0)), (short)(6), "0") + GXutil.trim( AV236Dados) + GXutil.space( (short)(23)) + GXutil.trim( AV237TipoV) + GXutil.space( (short)(94)) ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwptxt( AV228Texto, (short)(150)) ;
      AV270Total = 1 ;
      AV14FileNo = context.getSessionInstances().getDelimitedFiles().dfwnext( ) ;
      GXt_char4 = AV111s ;
      GXv_int8[0] = AV32ICSI_C ;
      GXv_int9[0] = (byte)(5) ;
      GXv_int10[0] = (byte)(0) ;
      GXv_svchar7[0] = GXt_char4 ;
      new pr2strzero(remoteHandle, context).execute( GXv_int8, GXv_int9, GXv_int10, GXv_svchar7) ;
      pretsubax_v003.this.AV32ICSI_C = (int)((int)(GXv_int8[0])) ;
      pretsubax_v003.this.GXt_char4 = GXv_svchar7[0] ;
      AV111s = GXt_char4 ;
      AV215TotLi = (int)(AV215TotLi+1) ;
      AV187ArqNu = AV111s ;
   }
/*
   public static Object refClasses( )
   {
      GXutil.refClasses(pretsubax_v003.class);
      return new GXcfg();
   }
*/
   protected void cleanup( )
   {
      this.aP0[0] = pretsubax_v003.this.AV10DebugM;
      Application.commit(context, remoteHandle, "DEFAULT", "pretsubax_v003");
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV222Airli = "" ;
      AV223Airli = "" ;
      AV263UsaNo = "" ;
      AV264Bande = "" ;
      AV265NovaB = "" ;
      AV266Bande = "" ;
      AV267NovaB = "" ;
      AV84Versao = "" ;
      AV64Sep = "" ;
      AV108TestM = (byte)(0) ;
      AV198RetSu = "" ;
      scmdbuf = "" ;
      P007L2_A1147lccbE = new String[] {""} ;
      P007L2_n1147lccbE = new boolean[] {false} ;
      P007L2_A1150lccbE = new String[] {""} ;
      A1147lccbE = "" ;
      n1147lccbE = false ;
      A1150lccbE = "" ;
      AV144lccbE = "" ;
      AV194TipoM = (byte)(0) ;
      AV195TotRe = 0 ;
      AV196TotRe = 0 ;
      AV8DataB = "" ;
      Gx_date = GXutil.nullDate() ;
      AV213ArqDa = GXutil.resetTime( GXutil.nullDate() );
      AV27HoraA = "" ;
      AV28HoraB = "" ;
      AV159NumRe = 0 ;
      AV216InicL = "" ;
      AV215TotLi = 0 ;
      AV197Trans = "" ;
      AV212W = (short)(0) ;
      AV232Total = 0 ;
      AV235SeqLi = 0 ;
      returnInSub = false ;
      GXt_char3 = "" ;
      GXt_char1 = "" ;
      AV140Short = "" ;
      AV256Audit = "" ;
      AV253Audit = "" ;
      AV254Audit = "" ;
      AV255Audit = "" ;
      GXv_svchar2 = new String [1] ;
      GXv_svchar5 = new String [1] ;
      AV13FileNa = "" ;
      AV14FileNo = 0 ;
      AV190ValTo = 0 ;
      AV191ValTo = 0 ;
      AV192ValTo = 0 ;
      AV193ValTo = 0 ;
      AV241Heade = "" ;
      A1168lccbI = (short)(0) ;
      A1490Distr = "" ;
      A1172lccbS = 0 ;
      A1184lccbS = "" ;
      A1227lccbO = "" ;
      P007L3_A1228lccbF = new String[] {""} ;
      P007L3_A1227lccbO = new String[] {""} ;
      P007L3_A1225lccbC = new String[] {""} ;
      P007L3_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007L3_A1222lccbI = new String[] {""} ;
      P007L3_A1150lccbE = new String[] {""} ;
      P007L3_A1226lccbA = new String[] {""} ;
      P007L3_A1224lccbC = new String[] {""} ;
      P007L3_A1172lccbS = new double[1] ;
      P007L3_n1172lccbS = new boolean[] {false} ;
      P007L3_A1168lccbI = new short[1] ;
      P007L3_n1168lccbI = new boolean[] {false} ;
      P007L3_A1490Distr = new String[] {""} ;
      P007L3_n1490Distr = new boolean[] {false} ;
      P007L3_A1184lccbS = new String[] {""} ;
      P007L3_n1184lccbS = new boolean[] {false} ;
      P007L3_A1179lccbV = new String[] {""} ;
      P007L3_n1179lccbV = new boolean[] {false} ;
      P007L3_A1171lccbT = new double[1] ;
      P007L3_n1171lccbT = new boolean[] {false} ;
      P007L3_A1170lccbI = new double[1] ;
      P007L3_n1170lccbI = new boolean[] {false} ;
      P007L3_A1189lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P007L3_n1189lccbS = new boolean[] {false} ;
      P007L3_A1192lccbS = new String[] {""} ;
      P007L3_n1192lccbS = new boolean[] {false} ;
      P007L3_A1190lccbS = new java.util.Date[] {GXutil.nullDate()} ;
      P007L3_n1190lccbS = new boolean[] {false} ;
      P007L3_A1191lccbS = new String[] {""} ;
      P007L3_n1191lccbS = new boolean[] {false} ;
      P007L3_A1193lccbS = new String[] {""} ;
      P007L3_n1193lccbS = new boolean[] {false} ;
      P007L3_A1185lccbB = new String[] {""} ;
      P007L3_n1185lccbB = new boolean[] {false} ;
      P007L3_A1195lccbS = new String[] {""} ;
      P007L3_n1195lccbS = new boolean[] {false} ;
      P007L3_A1194lccbS = new String[] {""} ;
      P007L3_n1194lccbS = new boolean[] {false} ;
      A1228lccbF = "" ;
      A1225lccbC = "" ;
      A1223lccbD = GXutil.nullDate() ;
      A1222lccbI = "" ;
      A1226lccbA = "" ;
      A1224lccbC = "" ;
      n1172lccbS = false ;
      n1168lccbI = false ;
      n1490Distr = false ;
      n1184lccbS = false ;
      A1179lccbV = "" ;
      n1179lccbV = false ;
      A1171lccbT = 0 ;
      n1171lccbT = false ;
      A1170lccbI = 0 ;
      n1170lccbI = false ;
      A1189lccbS = GXutil.nullDate() ;
      n1189lccbS = false ;
      A1192lccbS = "" ;
      n1192lccbS = false ;
      A1190lccbS = GXutil.resetTime( GXutil.nullDate() );
      n1190lccbS = false ;
      A1191lccbS = "" ;
      n1191lccbS = false ;
      A1193lccbS = "" ;
      n1193lccbS = false ;
      A1185lccbB = "" ;
      n1185lccbB = false ;
      A1195lccbS = "" ;
      n1195lccbS = false ;
      A1194lccbS = "" ;
      n1194lccbS = false ;
      AV224lccbE = "" ;
      AV145lccbD = GXutil.nullDate() ;
      AV146lccbC = "" ;
      AV250Atrib = "" ;
      AV252Resul = "" ;
      AV137lccbC = "" ;
      AV136lccbA = "" ;
      AV248sInpu = "" ;
      AV249sOutp = "" ;
      AV245j = (byte)(0) ;
      AV247c = "" ;
      AV147lccbO = "" ;
      AV148lccbF = "" ;
      AV182TotRe = 0 ;
      AV234lccbS = "" ;
      AV32ICSI_C = 0 ;
      AV229DataA = GXutil.nullDate() ;
      AV230lccbD = "" ;
      AV261DataV = "" ;
      AV238QtdeP = "" ;
      AV239lccbT = "" ;
      AV240lccbD = "" ;
      AV227lccbS = "" ;
      AV244VlrPr = 0 ;
      AV243CodAu = "" ;
      AV262CodAu = "" ;
      AV276K = (byte)(0) ;
      AV228Texto = "" ;
      AV242CodEs = "" ;
      AV141Refer = "" ;
      AV160Txt = "" ;
      AV165Batch = "" ;
      AV175FlgTk = "" ;
      AV176Conta = (byte)(0) ;
      P007L4_A1150lccbE = new String[] {""} ;
      P007L4_A1222lccbI = new String[] {""} ;
      P007L4_A1223lccbD = new java.util.Date[] {GXutil.nullDate()} ;
      P007L4_A1224lccbC = new String[] {""} ;
      P007L4_A1225lccbC = new String[] {""} ;
      P007L4_A1226lccbA = new String[] {""} ;
      P007L4_A1227lccbO = new String[] {""} ;
      P007L4_A1228lccbF = new String[] {""} ;
      P007L4_A1231lccbT = new String[] {""} ;
      P007L4_A1207lccbP = new String[] {""} ;
      P007L4_n1207lccbP = new boolean[] {false} ;
      P007L4_A1232lccbT = new String[] {""} ;
      A1231lccbT = "" ;
      A1207lccbP = "" ;
      n1207lccbP = false ;
      A1232lccbT = "" ;
      AV118lccbT = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV118lccbT[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV177DbIat = new int [99] ;
      AV130lccbI = "" ;
      AV178DbCCc = new long [99] ;
      AV179DbPas = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV179DbPas[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV218DbLcc = new String [99] ;
      GX_I = 1 ;
      while ( ( GX_I <= 99 ) )
      {
         AV218DbLcc[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV181DbNum = new short [99] ;
      GX_INS236 = 0 ;
      A1229lccbS = GXutil.resetTime( GXutil.nullDate() );
      A1186lccbS = "" ;
      n1186lccbS = false ;
      A1187lccbS = "" ;
      n1187lccbS = false ;
      A1230lccbS = (short)(0) ;
      Gx_err = (short)(0) ;
      Gx_emsg = "" ;
      AV169Num11 = 0 ;
      AV231Bilhe = "" ;
      AV270Total = 0 ;
      AV214LccbS = 0 ;
      AV143lccbD = 0 ;
      AV134lccbT = 0 ;
      AV133lccbI = 0 ;
      AV278Lccbs = "" ;
      AV233Total = "" ;
      AV157tSale = 0 ;
      AV158tSale = 0 ;
      AV268Proce = "" ;
      AV269msgEr = "" ;
      AV257PathC = "" ;
      AV258Diret = new com.genexus.util.GXDirectory();
      AV260File = new com.genexus.util.GXFile();
      GXv_svchar6 = new String [1] ;
      AV259Filen = "" ;
      AV115Total = 0 ;
      AV116Total = 0 ;
      AV153SeqFi = 0 ;
      AV154cntCC = 0 ;
      AV155cntPL = 0 ;
      AV279GXLvl = (byte)(0) ;
      P007L7_A1488ICSI_ = new String[] {""} ;
      P007L7_A1487ICSI_ = new String[] {""} ;
      P007L7_A1480ICSI_ = new String[] {""} ;
      P007L7_n1480ICSI_ = new boolean[] {false} ;
      P007L7_A1481ICSI_ = new String[] {""} ;
      P007L7_n1481ICSI_ = new boolean[] {false} ;
      P007L7_A1485ICSI_ = new String[] {""} ;
      P007L7_n1485ICSI_ = new boolean[] {false} ;
      P007L7_A1479ICSI_ = new int[1] ;
      P007L7_n1479ICSI_ = new boolean[] {false} ;
      P007L7_A1478ICSI_ = new int[1] ;
      P007L7_n1478ICSI_ = new boolean[] {false} ;
      A1488ICSI_ = "" ;
      A1487ICSI_ = "" ;
      A1480ICSI_ = "" ;
      n1480ICSI_ = false ;
      A1481ICSI_ = "" ;
      n1481ICSI_ = false ;
      A1485ICSI_ = "" ;
      n1485ICSI_ = false ;
      A1479ICSI_ = 0 ;
      n1479ICSI_ = false ;
      A1478ICSI_ = 0 ;
      n1478ICSI_ = false ;
      AV150ICSI_ = "" ;
      AV151ICSI_ = "" ;
      AV139ICSI_ = "" ;
      AV30ICSI_C = 0 ;
      AV163ICSI_ = 0 ;
      GX_INS273 = 0 ;
      AV226DataA = "" ;
      AV236Dados = "" ;
      AV237TipoV = "" ;
      AV111s = "" ;
      GXt_char4 = "" ;
      GXv_int8 = new double [1] ;
      GXv_int9 = new byte [1] ;
      GXv_int10 = new byte [1] ;
      GXv_svchar7 = new String [1] ;
      AV187ArqNu = "" ;
      GX_I = 0 ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pretsubax_v003__default(),
         new Object[] {
             new Object[] {
            P007L2_A1147lccbE, P007L2_n1147lccbE, P007L2_A1150lccbE
            }
            , new Object[] {
            P007L3_A1228lccbF, P007L3_A1227lccbO, P007L3_A1225lccbC, P007L3_A1223lccbD, P007L3_A1222lccbI, P007L3_A1150lccbE, P007L3_A1226lccbA, P007L3_A1224lccbC, P007L3_A1172lccbS, P007L3_n1172lccbS,
            P007L3_A1168lccbI, P007L3_n1168lccbI, P007L3_A1490Distr, P007L3_n1490Distr, P007L3_A1184lccbS, P007L3_n1184lccbS, P007L3_A1179lccbV, P007L3_n1179lccbV, P007L3_A1171lccbT, P007L3_n1171lccbT,
            P007L3_A1170lccbI, P007L3_n1170lccbI, P007L3_A1189lccbS, P007L3_n1189lccbS, P007L3_A1192lccbS, P007L3_n1192lccbS, P007L3_A1190lccbS, P007L3_n1190lccbS, P007L3_A1191lccbS, P007L3_n1191lccbS,
            P007L3_A1193lccbS, P007L3_n1193lccbS, P007L3_A1185lccbB, P007L3_n1185lccbB, P007L3_A1195lccbS, P007L3_n1195lccbS, P007L3_A1194lccbS, P007L3_n1194lccbS
            }
            , new Object[] {
            P007L4_A1150lccbE, P007L4_A1222lccbI, P007L4_A1223lccbD, P007L4_A1224lccbC, P007L4_A1225lccbC, P007L4_A1226lccbA, P007L4_A1227lccbO, P007L4_A1228lccbF, P007L4_A1231lccbT, P007L4_A1207lccbP,
            P007L4_n1207lccbP, P007L4_A1232lccbT
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            P007L7_A1488ICSI_, P007L7_A1487ICSI_, P007L7_A1480ICSI_, P007L7_n1480ICSI_, P007L7_A1481ICSI_, P007L7_n1481ICSI_, P007L7_A1485ICSI_, P007L7_n1485ICSI_, P007L7_A1479ICSI_, P007L7_n1479ICSI_,
            P007L7_A1478ICSI_, P007L7_n1478ICSI_
            }
            , new Object[] {
            }
            , new Object[] {
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV108TestM ;
   private byte AV194TipoM ;
   private byte AV245j ;
   private byte AV276K ;
   private byte AV176Conta ;
   private byte AV279GXLvl ;
   private byte GXv_int9[] ;
   private byte GXv_int10[] ;
   private short AV212W ;
   private short A1168lccbI ;
   private short AV181DbNum[] ;
   private short A1230lccbS ;
   private short Gx_err ;
   private int AV195TotRe ;
   private int AV196TotRe ;
   private int AV159NumRe ;
   private int AV215TotLi ;
   private int AV235SeqLi ;
   private int AV182TotRe ;
   private int AV32ICSI_C ;
   private int AV177DbIat[] ;
   private int GX_INS236 ;
   private int AV270Total ;
   private int AV153SeqFi ;
   private int AV154cntCC ;
   private int AV155cntPL ;
   private int A1479ICSI_ ;
   private int A1478ICSI_ ;
   private int AV30ICSI_C ;
   private int AV163ICSI_ ;
   private int GX_INS273 ;
   private int GX_I ;
   private long AV14FileNo ;
   private long AV190ValTo ;
   private long AV191ValTo ;
   private long AV192ValTo ;
   private long AV193ValTo ;
   private long AV178DbCCc[] ;
   private long AV169Num11 ;
   private long AV214LccbS ;
   private long AV143lccbD ;
   private long AV134lccbT ;
   private long AV133lccbI ;
   private long AV115Total ;
   private long AV116Total ;
   private double AV232Total ;
   private double A1172lccbS ;
   private double A1171lccbT ;
   private double A1170lccbI ;
   private double AV244VlrPr ;
   private double AV157tSale ;
   private double AV158tSale ;
   private double GXv_int8[] ;
   private String AV10DebugM ;
   private String AV222Airli ;
   private String AV223Airli ;
   private String AV263UsaNo ;
   private String AV84Versao ;
   private String AV64Sep ;
   private String AV198RetSu ;
   private String scmdbuf ;
   private String A1147lccbE ;
   private String A1150lccbE ;
   private String AV144lccbE ;
   private String AV8DataB ;
   private String AV27HoraA ;
   private String AV28HoraB ;
   private String AV216InicL ;
   private String AV197Trans ;
   private String GXt_char3 ;
   private String GXt_char1 ;
   private String AV140Short ;
   private String AV13FileNa ;
   private String AV241Heade ;
   private String A1490Distr ;
   private String A1184lccbS ;
   private String A1227lccbO ;
   private String A1228lccbF ;
   private String A1225lccbC ;
   private String A1222lccbI ;
   private String A1226lccbA ;
   private String A1224lccbC ;
   private String A1179lccbV ;
   private String A1192lccbS ;
   private String A1191lccbS ;
   private String A1193lccbS ;
   private String A1185lccbB ;
   private String A1195lccbS ;
   private String A1194lccbS ;
   private String AV224lccbE ;
   private String AV146lccbC ;
   private String AV250Atrib ;
   private String AV252Resul ;
   private String AV137lccbC ;
   private String AV136lccbA ;
   private String AV248sInpu ;
   private String AV249sOutp ;
   private String AV247c ;
   private String AV147lccbO ;
   private String AV148lccbF ;
   private String AV234lccbS ;
   private String AV230lccbD ;
   private String AV261DataV ;
   private String AV238QtdeP ;
   private String AV239lccbT ;
   private String AV240lccbD ;
   private String AV227lccbS ;
   private String AV243CodAu ;
   private String AV262CodAu ;
   private String AV228Texto ;
   private String AV242CodEs ;
   private String AV141Refer ;
   private String AV160Txt ;
   private String AV165Batch ;
   private String AV175FlgTk ;
   private String A1231lccbT ;
   private String A1207lccbP ;
   private String A1232lccbT ;
   private String AV118lccbT[] ;
   private String AV130lccbI ;
   private String AV179DbPas[] ;
   private String AV218DbLcc[] ;
   private String A1186lccbS ;
   private String Gx_emsg ;
   private String AV231Bilhe ;
   private String AV278Lccbs ;
   private String AV233Total ;
   private String AV257PathC ;
   private String AV259Filen ;
   private String A1488ICSI_ ;
   private String A1487ICSI_ ;
   private String A1480ICSI_ ;
   private String A1481ICSI_ ;
   private String A1485ICSI_ ;
   private String AV150ICSI_ ;
   private String AV151ICSI_ ;
   private String AV139ICSI_ ;
   private String AV226DataA ;
   private String AV236Dados ;
   private String AV237TipoV ;
   private String AV111s ;
   private String GXt_char4 ;
   private String AV187ArqNu ;
   private java.util.Date AV213ArqDa ;
   private java.util.Date A1190lccbS ;
   private java.util.Date A1229lccbS ;
   private java.util.Date Gx_date ;
   private java.util.Date A1223lccbD ;
   private java.util.Date A1189lccbS ;
   private java.util.Date AV145lccbD ;
   private java.util.Date AV229DataA ;
   private boolean n1147lccbE ;
   private boolean returnInSub ;
   private boolean n1172lccbS ;
   private boolean n1168lccbI ;
   private boolean n1490Distr ;
   private boolean n1184lccbS ;
   private boolean n1179lccbV ;
   private boolean n1171lccbT ;
   private boolean n1170lccbI ;
   private boolean n1189lccbS ;
   private boolean n1192lccbS ;
   private boolean n1190lccbS ;
   private boolean n1191lccbS ;
   private boolean n1193lccbS ;
   private boolean n1185lccbB ;
   private boolean n1195lccbS ;
   private boolean n1194lccbS ;
   private boolean n1207lccbP ;
   private boolean n1186lccbS ;
   private boolean n1187lccbS ;
   private boolean n1480ICSI_ ;
   private boolean n1481ICSI_ ;
   private boolean n1485ICSI_ ;
   private boolean n1479ICSI_ ;
   private boolean n1478ICSI_ ;
   private String AV264Bande ;
   private String AV265NovaB ;
   private String AV266Bande ;
   private String AV267NovaB ;
   private String AV256Audit ;
   private String AV253Audit ;
   private String AV254Audit ;
   private String AV255Audit ;
   private String GXv_svchar2[] ;
   private String GXv_svchar5[] ;
   private String A1187lccbS ;
   private String AV268Proce ;
   private String AV269msgEr ;
   private String GXv_svchar6[] ;
   private String GXv_svchar7[] ;
   private com.genexus.util.GXFile AV260File ;
   private com.genexus.util.GXDirectory AV258Diret ;
   private String[] aP0 ;
   private IDataStoreProvider pr_default ;
   private String[] P007L2_A1147lccbE ;
   private boolean[] P007L2_n1147lccbE ;
   private String[] P007L2_A1150lccbE ;
   private String[] P007L3_A1228lccbF ;
   private String[] P007L3_A1227lccbO ;
   private String[] P007L3_A1225lccbC ;
   private java.util.Date[] P007L3_A1223lccbD ;
   private String[] P007L3_A1222lccbI ;
   private String[] P007L3_A1150lccbE ;
   private String[] P007L3_A1226lccbA ;
   private String[] P007L3_A1224lccbC ;
   private double[] P007L3_A1172lccbS ;
   private boolean[] P007L3_n1172lccbS ;
   private short[] P007L3_A1168lccbI ;
   private boolean[] P007L3_n1168lccbI ;
   private String[] P007L3_A1490Distr ;
   private boolean[] P007L3_n1490Distr ;
   private String[] P007L3_A1184lccbS ;
   private boolean[] P007L3_n1184lccbS ;
   private String[] P007L3_A1179lccbV ;
   private boolean[] P007L3_n1179lccbV ;
   private double[] P007L3_A1171lccbT ;
   private boolean[] P007L3_n1171lccbT ;
   private double[] P007L3_A1170lccbI ;
   private boolean[] P007L3_n1170lccbI ;
   private java.util.Date[] P007L3_A1189lccbS ;
   private boolean[] P007L3_n1189lccbS ;
   private String[] P007L3_A1192lccbS ;
   private boolean[] P007L3_n1192lccbS ;
   private java.util.Date[] P007L3_A1190lccbS ;
   private boolean[] P007L3_n1190lccbS ;
   private String[] P007L3_A1191lccbS ;
   private boolean[] P007L3_n1191lccbS ;
   private String[] P007L3_A1193lccbS ;
   private boolean[] P007L3_n1193lccbS ;
   private String[] P007L3_A1185lccbB ;
   private boolean[] P007L3_n1185lccbB ;
   private String[] P007L3_A1195lccbS ;
   private boolean[] P007L3_n1195lccbS ;
   private String[] P007L3_A1194lccbS ;
   private boolean[] P007L3_n1194lccbS ;
   private String[] P007L4_A1150lccbE ;
   private String[] P007L4_A1222lccbI ;
   private java.util.Date[] P007L4_A1223lccbD ;
   private String[] P007L4_A1224lccbC ;
   private String[] P007L4_A1225lccbC ;
   private String[] P007L4_A1226lccbA ;
   private String[] P007L4_A1227lccbO ;
   private String[] P007L4_A1228lccbF ;
   private String[] P007L4_A1231lccbT ;
   private String[] P007L4_A1207lccbP ;
   private boolean[] P007L4_n1207lccbP ;
   private String[] P007L4_A1232lccbT ;
   private String[] P007L7_A1488ICSI_ ;
   private String[] P007L7_A1487ICSI_ ;
   private String[] P007L7_A1480ICSI_ ;
   private boolean[] P007L7_n1480ICSI_ ;
   private String[] P007L7_A1481ICSI_ ;
   private boolean[] P007L7_n1481ICSI_ ;
   private String[] P007L7_A1485ICSI_ ;
   private boolean[] P007L7_n1485ICSI_ ;
   private int[] P007L7_A1479ICSI_ ;
   private boolean[] P007L7_n1479ICSI_ ;
   private int[] P007L7_A1478ICSI_ ;
   private boolean[] P007L7_n1478ICSI_ ;
}

final  class pretsubax_v003__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   protected String conditional_P007L3( String AV197Trans ,
                                        short A1168lccbI ,
                                        String A1490Distr ,
                                        double A1172lccbS ,
                                        String A1184lccbS ,
                                        String A1150lccbE ,
                                        String AV144lccbE ,
                                        String A1227lccbO )
   {
      String sWhereString ;
      String scmdbuf ;
      scmdbuf = "SELECT [lccbFPAC_PLP], [lccbOpCode], [lccbCCNum], [lccbDate], [lccbIATA], [lccbEmpCod]," ;
      scmdbuf = scmdbuf + " [lccbAppCode], [lccbCCard], [lccbSaleAmount], [lccbInstallments], [DistribuicaoTransacoesTipo]," ;
      scmdbuf = scmdbuf + " [lccbStatus], [lccbValidDate], [lccbTip], [lccbInstAmount], [lccbSubDate], [lccbSubFile]," ;
      scmdbuf = scmdbuf + " [lccbSubTime], [lccbSubType], [lccbSubTrn], [lccbBatchNum], [lccbSubPOS], [lccbSubRO]" ;
      scmdbuf = scmdbuf + " FROM [LCCBPLP] WITH (NOLOCK)" ;
      scmdbuf = scmdbuf + " WHERE ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and ([lccbOpCode] = 'S')" ;
      scmdbuf = scmdbuf + " and (SUBSTRING([DistribuicaoTransacoesTipo], 1, 1) = 'A')" ;
      scmdbuf = scmdbuf + " and ([lccbSaleAmount] > 0.00)" ;
      scmdbuf = scmdbuf + " and ([lccbStatus] = 'TOSUB')" ;
      scmdbuf = scmdbuf + " and ([lccbEmpCod] = '" + GXutil.rtrim( GXutil.strReplace( AV144lccbE, "'", "''")) + "')" ;
      sWhereString = "" ;
      if ( ( GXutil.strcmp(AV197Trans, "001") == 0 ) )
      {
         sWhereString = sWhereString + " and ([lccbInstallments] <= 1)" ;
      }
      if ( ( GXutil.strcmp(AV197Trans, "002") == 0 ) )
      {
         sWhereString = sWhereString + " and ([lccbInstallments] > 1)" ;
      }
      scmdbuf = scmdbuf + sWhereString ;
      scmdbuf = scmdbuf + " ORDER BY [lccbOpCode], [lccbCCard], [lccbStatus]" ;
      return scmdbuf;
   }

   public String getDynamicStatement( int cursor ,
                                      Object [] dynConstraints )
   {
      switch ( cursor )
      {
            case 1 :
                  return conditional_P007L3( (String)dynConstraints[0] , ((Number) dynConstraints[1]).shortValue() , (String)dynConstraints[2] , ((Number) dynConstraints[3]).doubleValue() , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] );
      }
      return super.getDynamicStatement(cursor, dynConstraints);
   }

   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P007L2", "SELECT [lccbEmpEnab], [lccbEmpCod] FROM [LCCBEMP] WITH (NOLOCK) WHERE [lccbEmpEnab] = '1' ORDER BY [lccbEmpCod] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007L3", "scmdbuf",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new ForEachCursor("P007L4", "SELECT [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbTDNR], [lccbPaxName], [lccbTRNC] FROM [LCCBPLP2] WITH (NOLOCK) WHERE [lccbEmpCod] = ? and [lccbIATA] = ? and [lccbDate] = ? and [lccbCCard] = ? and [lccbCCNum] = ? and [lccbAppCode] = ? and [lccbOpCode] = ? and [lccbFPAC_PLP] = ? ORDER BY [lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,false )
         ,new UpdateCursor("P007L5", "INSERT INTO [LCCBPLP1] ([lccbEmpCod], [lccbIATA], [lccbDate], [lccbCCard], [lccbCCNum], [lccbAppCode], [lccbOpCode], [lccbFPAC_PLP], [lccbSubStDate], [lccbSubStSeq], [lccbSubStStatus], [lccbSubStRemark], [lccbSubStType], [lccbCCNumEnc1]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', '')", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007L6", "UPDATE [LCCBPLP] SET [lccbStatus]=?, [lccbSubDate]=?, [lccbSubFile]=?, [lccbSubTime]=?, [lccbSubType]=?, [lccbSubTrn]=?, [lccbBatchNum]=?, [lccbSubPOS]=?, [lccbSubRO]=?  WHERE [lccbEmpCod] = ? AND [lccbIATA] = ? AND [lccbDate] = ? AND [lccbCCard] = ? AND [lccbCCNum] = ? AND [lccbAppCode] = ? AND [lccbOpCode] = ? AND [lccbFPAC_PLP] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new ForEachCursor("P007L7", "SELECT [ICSI_CCCod], [ICSI_EmpCod], [ICSI_CCPOS1], [ICSI_CCPOS2], [ICSI_CCNome], [ICSI_CCSeqFile], [ICSI_CCLote] FROM [ICSI_CCINFO] WITH (UPDLOCK) WHERE ([ICSI_EmpCod] = '000' AND [ICSI_CCCod] = 'AX') AND ([ICSI_EmpCod] = '000' and [ICSI_CCCod] = 'AX') ",true, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new UpdateCursor("P007L8", "UPDATE [ICSI_CCINFO] SET [ICSI_CCSeqFile]=?  WHERE [ICSI_EmpCod] = ? AND [ICSI_CCCod] = ?", GX_NOMASK + GX_MASKLOOPLOCK)
         ,new UpdateCursor("P007L9", "INSERT INTO [ICSI_CCINFO] ([ICSI_EmpCod], [ICSI_CCCod], [ICSI_CCLote], [ICSI_CCSeqFile], [ICSI_CCPOS1], [ICSI_CCPOS2], [ICSI_CCNome], [ICSI_CCPOS3], [ICSI_SplitPLP], [ICSI_CCEstab]) VALUES (?, ?, ?, ?, ?, ?, ?, '', convert(int, 0), '')", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 19) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 44) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 7) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 3) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 20) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 2) ;
               ((double[]) buf[8])[0] = rslt.getDouble(9) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((short[]) buf[10])[0] = rslt.getShort(10) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               ((String[]) buf[12])[0] = rslt.getString(11, 4) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               ((String[]) buf[14])[0] = rslt.getString(12, 8) ;
               ((boolean[]) buf[15])[0] = rslt.wasNull();
               ((String[]) buf[16])[0] = rslt.getString(13, 4) ;
               ((boolean[]) buf[17])[0] = rslt.wasNull();
               ((double[]) buf[18])[0] = rslt.getDouble(14) ;
               ((boolean[]) buf[19])[0] = rslt.wasNull();
               ((double[]) buf[20])[0] = rslt.getDouble(15) ;
               ((boolean[]) buf[21])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[22])[0] = rslt.getGXDate(16) ;
               ((boolean[]) buf[23])[0] = rslt.wasNull();
               ((String[]) buf[24])[0] = rslt.getString(17, 20) ;
               ((boolean[]) buf[25])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[26])[0] = rslt.getGXDateTime(18) ;
               ((boolean[]) buf[27])[0] = rslt.wasNull();
               ((String[]) buf[28])[0] = rslt.getString(19, 1) ;
               ((boolean[]) buf[29])[0] = rslt.wasNull();
               ((String[]) buf[30])[0] = rslt.getString(20, 20) ;
               ((boolean[]) buf[31])[0] = rslt.wasNull();
               ((String[]) buf[32])[0] = rslt.getString(21, 20) ;
               ((boolean[]) buf[33])[0] = rslt.wasNull();
               ((String[]) buf[34])[0] = rslt.getString(22, 20) ;
               ((boolean[]) buf[35])[0] = rslt.wasNull();
               ((String[]) buf[36])[0] = rslt.getString(23, 10) ;
               ((boolean[]) buf[37])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 7) ;
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(3) ;
               ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
               ((String[]) buf[4])[0] = rslt.getString(5, 44) ;
               ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
               ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
               ((String[]) buf[7])[0] = rslt.getString(8, 19) ;
               ((String[]) buf[8])[0] = rslt.getString(9, 10) ;
               ((String[]) buf[9])[0] = rslt.getString(10, 50) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((String[]) buf[11])[0] = rslt.getString(11, 4) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((String[]) buf[6])[0] = rslt.getString(5, 30) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((int[]) buf[8])[0] = rslt.getInt(6) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               ((int[]) buf[10])[0] = rslt.getInt(7) ;
               ((boolean[]) buf[11])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 2 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 7);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 2);
               stmt.setString(5, (String)parms[4], 44);
               stmt.setString(6, (String)parms[5], 20);
               stmt.setString(7, (String)parms[6], 1);
               stmt.setString(8, (String)parms[7], 19);
               stmt.setDateTime(9, (java.util.Date)parms[8], false);
               stmt.setShort(10, ((Number) parms[9]).shortValue());
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 11 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(11, (String)parms[11], 8);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 12 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(12, (String)parms[13], 120);
               }
               break;
            case 4 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(1, (String)parms[1], 8);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(2, (java.util.Date)parms[3]);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(3, (String)parms[5], 20);
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDateTime(4, (java.util.Date)parms[7], false);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[9], 1);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[11], 20);
               }
               if ( ((Boolean) parms[12]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[13], 20);
               }
               if ( ((Boolean) parms[14]).booleanValue() )
               {
                  stmt.setNull( 8 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(8, (String)parms[15], 20);
               }
               if ( ((Boolean) parms[16]).booleanValue() )
               {
                  stmt.setNull( 9 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(9, (String)parms[17], 10);
               }
               stmt.setString(10, (String)parms[18], 3);
               stmt.setString(11, (String)parms[19], 7);
               stmt.setDate(12, (java.util.Date)parms[20]);
               stmt.setString(13, (String)parms[21], 2);
               stmt.setString(14, (String)parms[22], 44);
               stmt.setString(15, (String)parms[23], 20);
               stmt.setString(16, (String)parms[24], 1);
               stmt.setString(17, (String)parms[25], 19);
               break;
            case 6 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(1, ((Number) parms[1]).intValue());
               }
               stmt.setString(2, (String)parms[2], 3);
               stmt.setString(3, (String)parms[3], 10);
               break;
            case 7 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 10);
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(3, ((Number) parms[3]).intValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setInt(4, ((Number) parms[5]).intValue());
               }
               if ( ((Boolean) parms[6]).booleanValue() )
               {
                  stmt.setNull( 5 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(5, (String)parms[7], 20);
               }
               if ( ((Boolean) parms[8]).booleanValue() )
               {
                  stmt.setNull( 6 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(6, (String)parms[9], 20);
               }
               if ( ((Boolean) parms[10]).booleanValue() )
               {
                  stmt.setNull( 7 , Types.VARCHAR );
               }
               else
               {
                  stmt.setString(7, (String)parms[11], 30);
               }
               break;
      }
   }

}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtSdtProcessingFile_SdtProcessingFileItem extends GXXMLSerializable implements Cloneable, java.io.Serializable
{
   public SdtSdtProcessingFile_SdtProcessingFileItem( )
   {
      this(  new ModelContext(SdtSdtProcessingFile_SdtProcessingFileItem.class));
   }

   public SdtSdtProcessingFile_SdtProcessingFileItem( ModelContext context )
   {
      super( context, "SdtSdtProcessingFile_SdtProcessingFileItem");
   }

   public SdtSdtProcessingFile_SdtProcessingFileItem( StructSdtSdtProcessingFile_SdtProcessingFileItem struct )
   {
      this();
      setStruct(struct);
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "LogLinha") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "LogStatus") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "LogDescricao") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "SdtProcessingFile.SdtProcessingFileItem" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("LogLinha", GXutil.rtrim( gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha));
      oWriter.writeElement("LogStatus", GXutil.rtrim( gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus));
      oWriter.writeElement("LogDescricao", GXutil.rtrim( gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha( )
   {
      return gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha ;
   }

   public void setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha( String value )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha = value ;
      return  ;
   }

   public void setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha_SetNull( )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha = "" ;
      return  ;
   }

   public String getgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus( )
   {
      return gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus ;
   }

   public void setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus( String value )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus = value ;
      return  ;
   }

   public void setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus_SetNull( )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus = "" ;
      return  ;
   }

   public String getgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao( )
   {
      return gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao ;
   }

   public void setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao( String value )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao = value ;
      return  ;
   }

   public void setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao_SetNull( )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao = "" ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha = "" ;
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus = "" ;
      gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao = "" ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtSdtProcessingFile_SdtProcessingFileItem Clone( )
   {
      return (SdtSdtProcessingFile_SdtProcessingFileItem)(clone()) ;
   }

   public void setStruct( StructSdtSdtProcessingFile_SdtProcessingFileItem struct )
   {
      setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha(struct.getLoglinha());
      setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus(struct.getLogstatus());
      setgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao(struct.getLogdescricao());
   }

   public StructSdtSdtProcessingFile_SdtProcessingFileItem getStruct( )
   {
      StructSdtSdtProcessingFile_SdtProcessingFileItem struct = new StructSdtSdtProcessingFile_SdtProcessingFileItem ();
      struct.setLoglinha(getgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha());
      struct.setLogstatus(getgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus());
      struct.setLogdescricao(getgxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao());
      return struct ;
   }

   private short nOutParmCount ;
   private short readOk ;
   private String gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Loglinha ;
   private String gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logstatus ;
   private String gxTv_SdtSdtProcessingFile_SdtProcessingFileItem_Logdescricao ;
   private String sTagName ;
   private String GXt_char1 ;
}



public final  class StructSdtIT0D_IT0DItem implements Cloneable, java.io.Serializable
{
   public StructSdtIT0D_IT0DItem( )
   {
      gxTv_SdtIT0D_IT0DItem_Plid_it0d = "" ;
      gxTv_SdtIT0D_IT0DItem_Pltx_it0d = "" ;
      gxTv_SdtIT0D_IT0DItem_Cpnr_it0d = (byte)(0) ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getPlid_it0d( )
   {
      return gxTv_SdtIT0D_IT0DItem_Plid_it0d ;
   }

   public void setPlid_it0d( String value )
   {
      gxTv_SdtIT0D_IT0DItem_Plid_it0d = value ;
      return  ;
   }

   public String getPltx_it0d( )
   {
      return gxTv_SdtIT0D_IT0DItem_Pltx_it0d ;
   }

   public void setPltx_it0d( String value )
   {
      gxTv_SdtIT0D_IT0DItem_Pltx_it0d = value ;
      return  ;
   }

   public byte getCpnr_it0d( )
   {
      return gxTv_SdtIT0D_IT0DItem_Cpnr_it0d ;
   }

   public void setCpnr_it0d( byte value )
   {
      gxTv_SdtIT0D_IT0DItem_Cpnr_it0d = value ;
      return  ;
   }

   protected byte gxTv_SdtIT0D_IT0DItem_Cpnr_it0d ;
   protected String gxTv_SdtIT0D_IT0DItem_Plid_it0d ;
   protected String gxTv_SdtIT0D_IT0DItem_Pltx_it0d ;
}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtICSI_CCConf extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtICSI_CCConf( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtICSI_CCConf.class));
   }

   public SdtICSI_CCConf( int remoteHandle ,
                          ModelContext context )
   {
      super( context, "SdtICSI_CCConf");
      initialize( remoteHandle) ;
   }

   public SdtICSI_CCConf( int remoteHandle ,
                          StructSdtICSI_CCConf struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV1237ICSI_CCConfCod )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV1237ICSI_CCConfCod});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfCod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfcod = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfName") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfname = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfEnab") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfenab = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Level1") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtICSI_CCConf_Level1.readxml(oReader, "Level1") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfCod_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfName_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfEnab_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfName_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ICSI_CCConfEnab_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "ICSI_CCConf" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ICSI_CCConfCod", GXutil.rtrim( gxTv_SdtICSI_CCConf_Icsi_ccconfcod));
      oWriter.writeElement("ICSI_CCConfName", GXutil.rtrim( gxTv_SdtICSI_CCConf_Icsi_ccconfname));
      oWriter.writeElement("ICSI_CCConfEnab", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Icsi_ccconfenab, 1, 0)));
      gxTv_SdtICSI_CCConf_Level1.writexml(oWriter, "Level1", "IataICSI");
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtICSI_CCConf_Mode));
      oWriter.writeElement("ICSI_CCConfCod_Z", GXutil.rtrim( gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z));
      oWriter.writeElement("ICSI_CCConfName_Z", GXutil.rtrim( gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z));
      oWriter.writeElement("ICSI_CCConfEnab_Z", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z, 1, 0)));
      oWriter.writeElement("ICSI_CCConfName_N", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Icsi_ccconfname_N, 1, 0)));
      oWriter.writeElement("ICSI_CCConfEnab_N", GXutil.trim( GXutil.str( gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Icsi_ccconfcod( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfcod ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfcod( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfcod_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod = "" ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Icsi_ccconfname( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfname ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfname( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfname_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(1) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname = "" ;
      return  ;
   }

   public byte getgxTv_SdtICSI_CCConf_Icsi_ccconfenab( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfenab ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfenab( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(1) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab = (byte)(0) ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtICSI_CCConf_Level1( )
   {
      return gxTv_SdtICSI_CCConf_Level1 ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1( GxSilentTrnGridCollection value )
   {
      gxTv_SdtICSI_CCConf_Level1 = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Level1_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Level1 = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Mode( )
   {
      return gxTv_SdtICSI_CCConf_Mode ;
   }

   public void setgxTv_SdtICSI_CCConf_Mode( String value )
   {
      gxTv_SdtICSI_CCConf_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Mode_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z( String value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtICSI_CCConf_Icsi_ccconfname_N( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfname_N ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfname_N( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfname_N_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N( )
   {
      return gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N( byte value )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = value ;
      return  ;
   }

   public void setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N_SetNull( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      ticsi_ccconf_bc obj ;
      obj = new ticsi_ccconf_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Level1 = new GxSilentTrnGridCollection(SdtICSI_CCConf_Level1Item.class, "ICSI_CCConf.Level1Item", "IataICSI");
      gxTv_SdtICSI_CCConf_Mode = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z = "" ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfname_N = (byte)(0) ;
      gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtICSI_CCConf Clone( )
   {
      SdtICSI_CCConf sdt ;
      ticsi_ccconf_bc obj ;
      sdt = (SdtICSI_CCConf)(clone()) ;
      obj = (ticsi_ccconf_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtICSI_CCConf struct )
   {
      setgxTv_SdtICSI_CCConf_Icsi_ccconfcod(struct.getIcsi_ccconfcod());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfname(struct.getIcsi_ccconfname());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfenab(struct.getIcsi_ccconfenab());
      setgxTv_SdtICSI_CCConf_Level1(new GxSilentTrnGridCollection(SdtICSI_CCConf_Level1Item.class, "ICSI_CCConf.Level1Item", "IataICSI", struct.getLevel1()));
      setgxTv_SdtICSI_CCConf_Mode(struct.getMode());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z(struct.getIcsi_ccconfcod_Z());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z(struct.getIcsi_ccconfname_Z());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z(struct.getIcsi_ccconfenab_Z());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfname_N(struct.getIcsi_ccconfname_N());
      setgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N(struct.getIcsi_ccconfenab_N());
   }

   public StructSdtICSI_CCConf getStruct( )
   {
      StructSdtICSI_CCConf struct = new StructSdtICSI_CCConf ();
      struct.setIcsi_ccconfcod(getgxTv_SdtICSI_CCConf_Icsi_ccconfcod());
      struct.setIcsi_ccconfname(getgxTv_SdtICSI_CCConf_Icsi_ccconfname());
      struct.setIcsi_ccconfenab(getgxTv_SdtICSI_CCConf_Icsi_ccconfenab());
      struct.setLevel1(getgxTv_SdtICSI_CCConf_Level1().getStruct());
      struct.setMode(getgxTv_SdtICSI_CCConf_Mode());
      struct.setIcsi_ccconfcod_Z(getgxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z());
      struct.setIcsi_ccconfname_Z(getgxTv_SdtICSI_CCConf_Icsi_ccconfname_Z());
      struct.setIcsi_ccconfenab_Z(getgxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z());
      struct.setIcsi_ccconfname_N(getgxTv_SdtICSI_CCConf_Icsi_ccconfname_N());
      struct.setIcsi_ccconfenab_N(getgxTv_SdtICSI_CCConf_Icsi_ccconfenab_N());
      return struct ;
   }

   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab ;
   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab_Z ;
   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfname_N ;
   protected byte gxTv_SdtICSI_CCConf_Icsi_ccconfenab_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfcod ;
   protected String gxTv_SdtICSI_CCConf_Mode ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfcod_Z ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfname ;
   protected String gxTv_SdtICSI_CCConf_Icsi_ccconfname_Z ;
   protected GxSilentTrnGridCollection gxTv_SdtICSI_CCConf_Level1 ;
}


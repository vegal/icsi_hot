
public final  class StructSdtUserFunctions implements Cloneable, java.io.Serializable
{
   public StructSdtUserFunctions( )
   {
      gxTv_SdtUserFunctions_Uspcode = "" ;
      gxTv_SdtUserFunctions_Uspdescription = "" ;
      gxTv_SdtUserFunctions_Mode = "" ;
      gxTv_SdtUserFunctions_Uspcode_Z = "" ;
      gxTv_SdtUserFunctions_Uspdescription_Z = "" ;
   }

   public Object clone()
   {
      Object cloned = null;
      try
      {
         cloned = super.clone();
      }catch (CloneNotSupportedException e){ ; }
      return cloned;
   }

   public String getUspcode( )
   {
      return gxTv_SdtUserFunctions_Uspcode ;
   }

   public void setUspcode( String value )
   {
      gxTv_SdtUserFunctions_Uspcode = value ;
      return  ;
   }

   public String getUspdescription( )
   {
      return gxTv_SdtUserFunctions_Uspdescription ;
   }

   public void setUspdescription( String value )
   {
      gxTv_SdtUserFunctions_Uspdescription = value ;
      return  ;
   }

   public String getMode( )
   {
      return gxTv_SdtUserFunctions_Mode ;
   }

   public void setMode( String value )
   {
      gxTv_SdtUserFunctions_Mode = value ;
      return  ;
   }

   public String getUspcode_Z( )
   {
      return gxTv_SdtUserFunctions_Uspcode_Z ;
   }

   public void setUspcode_Z( String value )
   {
      gxTv_SdtUserFunctions_Uspcode_Z = value ;
      return  ;
   }

   public String getUspdescription_Z( )
   {
      return gxTv_SdtUserFunctions_Uspdescription_Z ;
   }

   public void setUspdescription_Z( String value )
   {
      gxTv_SdtUserFunctions_Uspdescription_Z = value ;
      return  ;
   }

   protected String gxTv_SdtUserFunctions_Mode ;
   protected String gxTv_SdtUserFunctions_Uspcode ;
   protected String gxTv_SdtUserFunctions_Uspdescription ;
   protected String gxTv_SdtUserFunctions_Uspcode_Z ;
   protected String gxTv_SdtUserFunctions_Uspdescription_Z ;
}


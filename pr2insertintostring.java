/*
               File: R2InsertIntoString
        Description: Insere uma string dentro de outra
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:1.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pr2insertintostring extends GXProcedure
{
   public pr2insertintostring( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pr2insertintostring.class ), "" );
   }

   public pr2insertintostring( int remoteHandle ,
                               ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        short[] aP1 ,
                        String[] aP2 ,
                        String[] aP3 )
   {
      execute_int(aP0, aP1, aP2, aP3);
   }

   private void execute_int( String[] aP0 ,
                             short[] aP1 ,
                             String[] aP2 ,
                             String[] aP3 )
   {
      pr2insertintostring.this.AV8sInput = aP0[0];
      this.aP0 = aP0;
      pr2insertintostring.this.AV13PosIni = aP1[0];
      this.aP1 = aP1;
      pr2insertintostring.this.AV14sInser = aP2[0];
      this.aP2 = aP2;
      pr2insertintostring.this.AV9sOutput = aP3[0];
      this.aP3 = aP3;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( AV13PosIni <= 0 ) || ( AV13PosIni > GXutil.len( AV8sInput) ) )
      {
         AV9sOutput = AV8sInput ;
      }
      else
      {
         AV10i = (short)(AV13PosIni-1) ;
         AV9sOutput = GXutil.substring( AV8sInput, 1, AV10i) ;
         AV9sOutput = AV9sOutput + AV14sInser ;
         AV10i = (short)(AV13PosIni+GXutil.len( AV14sInser)) ;
         AV12k = (short)(GXutil.len( AV8sInput)-AV10i+1) ;
         AV9sOutput = AV9sOutput + GXutil.substring( AV8sInput, AV10i, AV12k) ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pr2insertintostring.this.AV8sInput;
      this.aP1[0] = pr2insertintostring.this.AV13PosIni;
      this.aP2[0] = pr2insertintostring.this.AV14sInser;
      this.aP3[0] = pr2insertintostring.this.AV9sOutput;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV10i = (short)(0) ;
      AV12k = (short)(0) ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short AV13PosIni ;
   private short AV10i ;
   private short AV12k ;
   private short Gx_err ;
   private String AV8sInput ;
   private String AV14sInser ;
   private String AV9sOutput ;
   private String[] aP0 ;
   private short[] aP1 ;
   private String[] aP2 ;
   private String[] aP3 ;
}


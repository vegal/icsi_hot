/*
               File: EncryptPassword
        Description: Encrypt Password
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:44:53.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pencryptpassword extends GXProcedure
{
   public pencryptpassword( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pencryptpassword.class ), "" );
   }

   public pencryptpassword( int remoteHandle ,
                            ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String[] aP0 ,
                        String[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             String[] aP1 ,
                             String[] aP2 )
   {
      pencryptpassword.this.AV8lgnLogi = aP0[0];
      this.aP0 = aP0;
      pencryptpassword.this.AV10Passwo = aP1[0];
      this.aP1 = aP1;
      pencryptpassword.this.AV9Encrypt = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV11ToEncr = GXutil.trim( AV8lgnLogi) + GXutil.trim( AV10Passwo) ;
      AV9Encrypt = com.genexus.util.Encryption.encrypt64( AV11ToEncr, "1a00f902f2df1b38968877e8514bcb56") ;
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = pencryptpassword.this.AV8lgnLogi;
      this.aP1[0] = pencryptpassword.this.AV10Passwo;
      this.aP2[0] = pencryptpassword.this.AV9Encrypt;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV11ToEncr = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String AV10Passwo ;
   private String AV9Encrypt ;
   private String AV11ToEncr ;
   private String AV8lgnLogi ;
   private String[] aP0 ;
   private String[] aP1 ;
   private String[] aP2 ;
}


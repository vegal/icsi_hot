/*
               File: tconfig_bc
        Description: System Configurations
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 12, 2015 10:45:1.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;

public final  class tconfig_bc extends GXWebPanel implements IGxSilentTrn
{
   public tconfig_bc( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public tconfig_bc( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( tconfig_bc.class ));
   }

   public tconfig_bc( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void inittrn( )
   {
   }

   public void disable_std_buttons( )
   {
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("confdelete"), 0);
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("mustconfirm"), 0);
         }
      }
   }

   public void afterTrn( )
   {
      if ( ( trnEnded == 1 ) )
      {
         trnEnded = 0 ;
         if ( ( httpContext.nUserReturn == 1 ) )
         {
            return  ;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            Z19ConfigI = A19ConfigI ;
            SetMode( "UPD") ;
         }
      }
   }

   public void confirm_030( )
   {
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            onDeleteControls033( ) ;
         }
         else
         {
            checkExtendedTable033( ) ;
            if ( ( AnyError == 0 ) )
            {
            }
            closeExtendedTableCursors033( ) ;
         }
      }
      if ( ( AnyError == 0 ) )
      {
         IsConfirmed = (short)(1) ;
      }
      if ( ( AnyError == 0 ) )
      {
         confirmValues030( ) ;
      }
   }

   public void zm033( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         Z17ConfigV = A17ConfigV ;
         Z18ConfigD = A18ConfigD ;
      }
      if ( ( GX_JID == -1 ) )
      {
         Z19ConfigI = A19ConfigI ;
         Z17ConfigV = A17ConfigV ;
         Z18ConfigD = A18ConfigD ;
      }
   }

   public void standaloneNotModal( )
   {
   }

   public void standaloneModal( )
   {
   }

   public void load033( )
   {
      /* Using cursor BC00034 */
      pr_default.execute(2, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(2) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A17ConfigV = BC00034_A17ConfigV[0] ;
         A18ConfigD = BC00034_A18ConfigD[0] ;
         zm033( -1) ;
      }
      pr_default.close(2);
      onLoadActions033( ) ;
   }

   public void onLoadActions033( )
   {
   }

   public void checkExtendedTable033( )
   {
      standaloneModal( ) ;
   }

   public void closeExtendedTableCursors033( )
   {
   }

   public void enableDisable( )
   {
   }

   public void getKey033( )
   {
      /* Using cursor BC00035 */
      pr_default.execute(3, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound3 = (short)(1) ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
      }
      pr_default.close(3);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor BC00033 */
      pr_default.execute(1, new Object[] {A19ConfigI});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm033( 1) ;
         RcdFound3 = (short)(1) ;
         A19ConfigI = BC00033_A19ConfigI[0] ;
         A17ConfigV = BC00033_A17ConfigV[0] ;
         A18ConfigD = BC00033_A18ConfigD[0] ;
         Z19ConfigI = A19ConfigI ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         load033( ) ;
         Gx_mode = sMode3 ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
         initializeNonKey033( ) ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         standaloneModal( ) ;
         Gx_mode = sMode3 ;
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey033( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
      }
      getByPrimaryKey( ) ;
   }

   public void insert_check( )
   {
      confirm_030( ) ;
      IsConfirmed = (short)(0) ;
   }

   public void update_check( )
   {
      insert_check( ) ;
   }

   public void delete_check( )
   {
      insert_check( ) ;
   }

   public void checkOptimisticConcurrency033( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         /* Using cursor BC00032 */
         pr_default.execute(0, new Object[] {A19ConfigI});
         if ( ! (pr_default.getStatus(0) != 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("lock", new Object[] {"CONFIG"}), "RecordIsLocked", 1);
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z17ConfigV, BC00032_A17ConfigV[0]) != 0 ) || ( GXutil.strcmp(Z18ConfigD, BC00032_A18ConfigD[0]) != 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("waschg", new Object[] {"CONFIG"}), "RecordWasChanged", 1);
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert033( )
   {
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         zm033( 0) ;
         checkOptimisticConcurrency033( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm033( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeInsert033( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00036 */
                  pr_default.execute(4, new Object[] {A19ConfigI, A17ConfigV, A18ConfigD});
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
                     AnyError = (short)(1) ;
                  }
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucadded"), 0);
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load033( ) ;
         }
         endLevel033( ) ;
      }
      closeExtendedTableCursors033( ) ;
   }

   public void update033( )
   {
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkExtendedTable033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency033( ) ;
         if ( ( AnyError == 0 ) )
         {
            afterConfirm033( ) ;
            if ( ( AnyError == 0 ) )
            {
               beforeUpdate033( ) ;
               if ( ( AnyError == 0 ) )
               {
                  /* Using cursor BC00037 */
                  pr_default.execute(5, new Object[] {A17ConfigV, A18ConfigD, A19ConfigI});
                  deferredUpdate033( ) ;
                  if ( ( AnyError == 0 ) )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( ( AnyError == 0 ) )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucupdated"), 0);
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel033( ) ;
      }
      closeExtendedTableCursors033( ) ;
   }

   public void deferredUpdate033( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      beforeValidate033( ) ;
      if ( ( AnyError == 0 ) )
      {
         checkOptimisticConcurrency033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         onDeleteControls033( ) ;
         /* No cascading delete specified. */
         afterConfirm033( ) ;
         if ( ( AnyError == 0 ) )
         {
            beforeDelete033( ) ;
            if ( ( AnyError == 0 ) )
            {
               /* Using cursor BC00038 */
               pr_default.execute(6, new Object[] {A19ConfigI});
               if ( ( AnyError == 0 ) )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( ( AnyError == 0 ) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("sucdeleted"), 0);
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("unexp"), 1);
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode3 = Gx_mode ;
      Gx_mode = "DLT" ;
      endLevel033( ) ;
      Gx_mode = sMode3 ;
   }

   public void onDeleteControls033( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel033( )
   {
      if ( ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
      {
         pr_default.close(0);
      }
      if ( ( AnyError == 0 ) )
      {
         beforeComplete033( ) ;
      }
      if ( ( AnyError == 0 ) )
      {
         if ( ( AnyError == 0 ) )
         {
            confirmValues030( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
      }
      IsModified = (short)(0) ;
      if ( ( AnyError != 0 ) )
      {
         httpContext.wjLoc = "" ;
      }
   }

   public void scanStart033( )
   {
      /* Using cursor BC00039 */
      pr_default.execute(7, new Object[] {A19ConfigI});
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A19ConfigI = BC00039_A19ConfigI[0] ;
         A17ConfigV = BC00039_A17ConfigV[0] ;
         A18ConfigD = BC00039_A18ConfigD[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext033( )
   {
      pr_default.readNext(7);
      RcdFound3 = (short)(0) ;
      scanLoad033( ) ;
   }

   public void scanLoad033( )
   {
      sMode3 = Gx_mode ;
      Gx_mode = "DSP" ;
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A19ConfigI = BC00039_A19ConfigI[0] ;
         A17ConfigV = BC00039_A17ConfigV[0] ;
         A18ConfigD = BC00039_A18ConfigD[0] ;
      }
      Gx_mode = sMode3 ;
   }

   public void scanEnd033( )
   {
      pr_default.close(7);
   }

   public void afterConfirm033( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert033( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate033( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete033( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete033( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate033( )
   {
      /* Before Validate Rules */
   }

   public void addRow033( )
   {
      VarsToRow3( bcConfig) ;
   }

   public void sendRow033( )
   {
   }

   public void readRow033( )
   {
      RowToVars3( bcConfig, 0) ;
   }

   public void confirmValues030( )
   {
   }

   public void initializeNonKey033( )
   {
      A17ConfigV = "" ;
      A18ConfigD = "" ;
   }

   public void initAll033( )
   {
      A19ConfigI = "" ;
      initializeNonKey033( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void VarsToRow3( SdtConfig obj3 )
   {
      obj3.setgxTv_SdtConfig_Mode( Gx_mode );
      obj3.setgxTv_SdtConfig_Configvalue( A17ConfigV );
      obj3.setgxTv_SdtConfig_Configdes( A18ConfigD );
      obj3.setgxTv_SdtConfig_Configid( A19ConfigI );
      obj3.setgxTv_SdtConfig_Configid_Z( Z19ConfigI );
      obj3.setgxTv_SdtConfig_Configvalue_Z( Z17ConfigV );
      obj3.setgxTv_SdtConfig_Configdes_Z( Z18ConfigD );
      obj3.setgxTv_SdtConfig_Mode( Gx_mode );
      return  ;
   }

   public void RowToVars3( SdtConfig obj3 ,
                           int forceLoad )
   {
      Gx_mode = obj3.getgxTv_SdtConfig_Mode() ;
      A17ConfigV = obj3.getgxTv_SdtConfig_Configvalue() ;
      A18ConfigD = obj3.getgxTv_SdtConfig_Configdes() ;
      A19ConfigI = obj3.getgxTv_SdtConfig_Configid() ;
      Z19ConfigI = obj3.getgxTv_SdtConfig_Configid_Z() ;
      Z17ConfigV = obj3.getgxTv_SdtConfig_Configvalue_Z() ;
      Z18ConfigD = obj3.getgxTv_SdtConfig_Configdes_Z() ;
      Gx_mode = obj3.getgxTv_SdtConfig_Mode() ;
      return  ;
   }

   public void LoadKey( Object[] obj )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      A19ConfigI = (String)obj[0] ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      initializeNonKey033( ) ;
      scanStart033( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         Gx_mode = "INS" ;
      }
      else
      {
         Gx_mode = "UPD" ;
         Z19ConfigI = A19ConfigI ;
      }
      onLoadActions033( ) ;
      zm033( 0) ;
      addRow033( ) ;
      scanEnd033( ) ;
      if ( ( RcdFound3 == 0 ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("keynfound"), "PrimaryKeyNotFound", 1);
         AnyError = (short)(1) ;
      }
      httpContext.GX_msglist = BackMsgLst ;
   }

   public void Save( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      IsConfirmed = (short)(1) ;
      RowToVars3( bcConfig, 0) ;
      nKeyPressed = (byte)(1) ;
      getKey033( ) ;
      if ( ( RcdFound3 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
         {
            A19ConfigI = Z19ConfigI ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete( ) ;
            afterTrn( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            /* Update record */
            update033( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "CandidateKeyNotFound", 1);
            AnyError = (short)(1) ;
         }
         else
         {
            if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert033( ) ;
               }
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
                  AnyError = (short)(1) ;
               }
               else
               {
                  Gx_mode = "INS" ;
                  /* Insert record */
                  insert033( ) ;
               }
            }
         }
      }
      afterTrn( ) ;
      VarsToRow3( bcConfig) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public void Check( )
   {
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      AnyError = (short)(0) ;
      httpContext.GX_msglist.removeAllItems();
      RowToVars3( bcConfig, 0) ;
      nKeyPressed = (byte)(3) ;
      IsConfirmed = (short)(0) ;
      getKey033( ) ;
      if ( ( RcdFound3 == 1 ) )
      {
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("noupdate"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
         {
            A19ConfigI = Z19ConfigI ;
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("getbeforeupd"), "DuplicatePrimaryKey", 1);
            AnyError = (short)(1) ;
         }
         else if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 ) )
         {
            delete_check( ) ;
         }
         else
         {
            Gx_mode = "UPD" ;
            update_check( ) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(A19ConfigI, Z19ConfigI) != 0 ) )
         {
            Gx_mode = "INS" ;
            insert_check( ) ;
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("recdeleted"), 1);
               AnyError = (short)(1) ;
            }
            else
            {
               Gx_mode = "INS" ;
               insert_check( ) ;
            }
         }
      }
      Application.rollback(context, remoteHandle, "DEFAULT", "tconfig_bc");
      VarsToRow3( bcConfig) ;
      httpContext.GX_msglist = BackMsgLst ;
      return  ;
   }

   public int Errors( )
   {
      if ( ( AnyError == 0 ) )
      {
         return 0 ;
      }
      return 1 ;
   }

   public com.genexus.internet.MsgList GetMessages( )
   {
      return LclMsgLst ;
   }

   public String GetMode( )
   {
      Gx_mode = bcConfig.getgxTv_SdtConfig_Mode() ;
      return Gx_mode ;
   }

   public void SetMode( String lMode )
   {
      Gx_mode = lMode ;
      bcConfig.setgxTv_SdtConfig_Mode( Gx_mode );
      return  ;
   }

   public void SetSDT( SdtConfig sdt ,
                       byte sdtToBc )
   {
      if ( ( sdt != bcConfig ) )
      {
         bcConfig = sdt ;
         if ( ( GXutil.strcmp(bcConfig.getgxTv_SdtConfig_Mode(), "") == 0 ) )
         {
            bcConfig.setgxTv_SdtConfig_Mode( "INS" );
         }
         if ( ( sdtToBc == 1 ) )
         {
            VarsToRow3( bcConfig) ;
         }
         else
         {
            RowToVars3( bcConfig, 1) ;
         }
      }
      else
      {
         if ( ( GXutil.strcmp(bcConfig.getgxTv_SdtConfig_Mode(), "") == 0 ) )
         {
            bcConfig.setgxTv_SdtConfig_Mode( "INS" );
         }
      }
      return  ;
   }

   public void ReloadFromSDT( )
   {
      RowToVars3( bcConfig, 1) ;
      return  ;
   }

   public SdtConfig getConfig_BC( )
   {
      return bcConfig ;
   }


   public void webExecute( )
   {
   }

   protected void createObjects( )
   {
   }

   protected void Process( )
   {
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      IsConfirmed = (short)(0) ;
      IsModified = (short)(0) ;
      AnyError = (short)(0) ;
      trnEnded = 0 ;
      nKeyPressed = (byte)(0) ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Gx_mode = "" ;
      Z19ConfigI = "" ;
      A19ConfigI = "" ;
      gxTv_SdtConfig_Configid_Z = "" ;
      gxTv_SdtConfig_Configvalue_Z = "" ;
      gxTv_SdtConfig_Configdes_Z = "" ;
      GX_JID = 0 ;
      Z17ConfigV = "" ;
      A17ConfigV = "" ;
      Z18ConfigD = "" ;
      A18ConfigD = "" ;
      BC00034_A19ConfigI = new String[] {""} ;
      BC00034_A17ConfigV = new String[] {""} ;
      BC00034_A18ConfigD = new String[] {""} ;
      RcdFound3 = (short)(0) ;
      BC00035_A19ConfigI = new String[] {""} ;
      BC00033_A19ConfigI = new String[] {""} ;
      BC00033_A17ConfigV = new String[] {""} ;
      BC00033_A18ConfigD = new String[] {""} ;
      sMode3 = "" ;
      BC00032_A19ConfigI = new String[] {""} ;
      BC00032_A17ConfigV = new String[] {""} ;
      BC00032_A18ConfigD = new String[] {""} ;
      BC00039_A19ConfigI = new String[] {""} ;
      BC00039_A17ConfigV = new String[] {""} ;
      BC00039_A18ConfigD = new String[] {""} ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new tconfig_bc__default(),
         new Object[] {
             new Object[] {
            BC00032_A19ConfigI, BC00032_A17ConfigV, BC00032_A18ConfigD
            }
            , new Object[] {
            BC00033_A19ConfigI, BC00033_A17ConfigV, BC00033_A18ConfigD
            }
            , new Object[] {
            BC00034_A19ConfigI, BC00034_A17ConfigV, BC00034_A18ConfigD
            }
            , new Object[] {
            BC00035_A19ConfigI
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            BC00039_A19ConfigI, BC00039_A17ConfigV, BC00039_A18ConfigD
            }
         }
      );
      /* Execute Start event if defined. */
   }

   private byte nKeyPressed ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short RcdFound3 ;
   private int trnEnded ;
   private int GX_JID ;
   private String scmdbuf ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String Gx_mode ;
   private String sMode3 ;
   private String Z19ConfigI ;
   private String A19ConfigI ;
   private String gxTv_SdtConfig_Configid_Z ;
   private String gxTv_SdtConfig_Configvalue_Z ;
   private String gxTv_SdtConfig_Configdes_Z ;
   private String Z17ConfigV ;
   private String A17ConfigV ;
   private String Z18ConfigD ;
   private String A18ConfigD ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private SdtConfig bcConfig ;
   private IDataStoreProvider pr_default ;
   private String[] BC00034_A19ConfigI ;
   private String[] BC00034_A17ConfigV ;
   private String[] BC00034_A18ConfigD ;
   private String[] BC00035_A19ConfigI ;
   private String[] BC00033_A19ConfigI ;
   private String[] BC00033_A17ConfigV ;
   private String[] BC00033_A18ConfigD ;
   private String[] BC00032_A19ConfigI ;
   private String[] BC00032_A17ConfigV ;
   private String[] BC00032_A18ConfigD ;
   private String[] BC00039_A19ConfigI ;
   private String[] BC00039_A17ConfigV ;
   private String[] BC00039_A18ConfigD ;
}

final  class tconfig_bc__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("BC00032", "SELECT [ConfigID], [ConfigValue], [ConfigDes] FROM [CONFIG] WITH (UPDLOCK) WHERE [ConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00033", "SELECT [ConfigID], [ConfigValue], [ConfigDes] FROM [CONFIG] WITH (NOLOCK) WHERE [ConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00034", "SELECT TM1.[ConfigID], TM1.[ConfigValue], TM1.[ConfigDes] FROM [CONFIG] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ConfigID] = ? ORDER BY TM1.[ConfigID] ",true, GX_NOMASK, false, this,0,false )
         ,new ForEachCursor("BC00035", "SELECT [ConfigID] FROM [CONFIG] WITH (FASTFIRSTROW NOLOCK) WHERE [ConfigID] = ? ",true, GX_NOMASK, false, this,0,false )
         ,new UpdateCursor("BC00036", "INSERT INTO [CONFIG] ([ConfigID], [ConfigValue], [ConfigDes]) VALUES (?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("BC00037", "UPDATE [CONFIG] SET [ConfigValue]=?, [ConfigDes]=?  WHERE [ConfigID] = ?", GX_NOMASK)
         ,new UpdateCursor("BC00038", "DELETE FROM [CONFIG]  WHERE [ConfigID] = ?", GX_NOMASK)
         ,new ForEachCursor("BC00039", "SELECT TM1.[ConfigID], TM1.[ConfigValue], TM1.[ConfigDes] FROM [CONFIG] TM1 WITH (FASTFIRSTROW NOLOCK) WHERE TM1.[ConfigID] = ? ORDER BY TM1.[ConfigID] ",true, GX_NOMASK, false, this,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               break;
            case 7 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 1 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 2 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 3 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 4 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               stmt.setVarchar(2, (String)parms[1], 255, false);
               stmt.setVarchar(3, (String)parms[2], 100, false);
               break;
            case 5 :
               stmt.setVarchar(1, (String)parms[0], 255, false);
               stmt.setVarchar(2, (String)parms[1], 100, false);
               stmt.setVarchar(3, (String)parms[2], 20, false);
               break;
            case 6 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
            case 7 :
               stmt.setVarchar(1, (String)parms[0], 20, false);
               break;
      }
   }

}


import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtPeriods extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtPeriods( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtPeriods.class));
   }

   public SdtPeriods( int remoteHandle ,
                      ModelContext context )
   {
      super( context, "SdtPeriods");
      initialize( remoteHandle) ;
   }

   public SdtPeriods( int remoteHandle ,
                      StructSdtPeriods struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV23ISOCod ,
                     String AV349CurCode ,
                     String AV387PeriodTypesCode ,
                     String AV263PerID )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV23ISOCod,AV349CurCode,AV387PeriodTypesCode,AV263PerID});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Isocod = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Curcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Periodtypescode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerID") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perid = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Isodes = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Curdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDescr") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perdescr = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatIni") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatini = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatini = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatEnd") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatend = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatend = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDisIni") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdisini = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdisini = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDisFin") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdisfin = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdisfin = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDueIni") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdueini = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdueini = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDueEnd") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdueend = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdueend = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerClose") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perclose = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatIns") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatins = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtPeriods_Perdatins = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatUpd") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatupd = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtPeriods_Perdatupd = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerIDAdm") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Peridadm = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISOCod_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Isocod_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Curcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PeriodTypesCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Periodtypescode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerID_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perid_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Isodes_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Curdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDescr_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perdescr_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatIni_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatini_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatini_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatEnd_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatend_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatend_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDisIni_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdisini_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdisini_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDisFin_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdisfin_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdisfin_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDueIni_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdueini_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdueini_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDueEnd_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatdueend_Z = GXutil.nullDate() ;
            }
            else
            {
               gxTv_SdtPeriods_Perdatdueend_Z = localUtil.ymdtod( (int)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (int)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerClose_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perclose_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatIns_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatins_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtPeriods_Perdatins_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatUpd_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( GXutil.strcmp(oReader.getValue(), "0000-00-00T00:00:00") == 0 ) )
            {
               gxTv_SdtPeriods_Perdatupd_Z = GXutil.resetTime( GXutil.nullDate() );
            }
            else
            {
               gxTv_SdtPeriods_Perdatupd_Z = localUtil.ymdhmsToT( (short)(GXutil.val( GXutil.substring( oReader.getValue(), 1, 4), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 6, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 9, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 12, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 15, 2), ".")), (byte)(GXutil.val( GXutil.substring( oReader.getValue(), 18, 2), "."))) ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerIDAdm_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Peridadm_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ISODes_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Isodes_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "CurDescription_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Curdescription_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDisIni_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perdatdisini_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "PerDatDisFin_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtPeriods_Perdatdisfin_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "Periods" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ISOCod", GXutil.rtrim( gxTv_SdtPeriods_Isocod));
      oWriter.writeElement("CurCode", GXutil.rtrim( gxTv_SdtPeriods_Curcode));
      oWriter.writeElement("PeriodTypesCode", GXutil.rtrim( gxTv_SdtPeriods_Periodtypescode));
      oWriter.writeElement("PerID", GXutil.rtrim( gxTv_SdtPeriods_Perid));
      oWriter.writeElement("ISODes", GXutil.rtrim( gxTv_SdtPeriods_Isodes));
      oWriter.writeElement("CurDescription", GXutil.rtrim( gxTv_SdtPeriods_Curdescription));
      oWriter.writeElement("PerDescr", GXutil.rtrim( gxTv_SdtPeriods_Perdescr));
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatini)) )
      {
         oWriter.writeElement("PerDatIni", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatIni", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatend)) )
      {
         oWriter.writeElement("PerDatEnd", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatend), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatend), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatend), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatEnd", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdisini)) )
      {
         oWriter.writeElement("PerDatDisIni", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdisini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdisini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdisini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDisIni", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdisfin)) )
      {
         oWriter.writeElement("PerDatDisFin", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdisfin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdisfin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdisfin), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDisFin", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdueini)) )
      {
         oWriter.writeElement("PerDatDueIni", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdueini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdueini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdueini), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDueIni", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdueend)) )
      {
         oWriter.writeElement("PerDatDueEnd", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdueend), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdueend), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdueend), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDueEnd", sDateCnv);
      }
      oWriter.writeElement("PerClose", GXutil.rtrim( gxTv_SdtPeriods_Perclose));
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatins)) )
      {
         oWriter.writeElement("PerDatIns", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtPeriods_Perdatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtPeriods_Perdatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtPeriods_Perdatins), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatIns", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatupd)) )
      {
         oWriter.writeElement("PerDatUpd", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtPeriods_Perdatupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtPeriods_Perdatupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtPeriods_Perdatupd), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatUpd", sDateCnv);
      }
      oWriter.writeElement("PerIDAdm", GXutil.rtrim( gxTv_SdtPeriods_Peridadm));
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtPeriods_Mode));
      oWriter.writeElement("ISOCod_Z", GXutil.rtrim( gxTv_SdtPeriods_Isocod_Z));
      oWriter.writeElement("CurCode_Z", GXutil.rtrim( gxTv_SdtPeriods_Curcode_Z));
      oWriter.writeElement("PeriodTypesCode_Z", GXutil.rtrim( gxTv_SdtPeriods_Periodtypescode_Z));
      oWriter.writeElement("PerID_Z", GXutil.rtrim( gxTv_SdtPeriods_Perid_Z));
      oWriter.writeElement("ISODes_Z", GXutil.rtrim( gxTv_SdtPeriods_Isodes_Z));
      oWriter.writeElement("CurDescription_Z", GXutil.rtrim( gxTv_SdtPeriods_Curdescription_Z));
      oWriter.writeElement("PerDescr_Z", GXutil.rtrim( gxTv_SdtPeriods_Perdescr_Z));
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatini_Z)) )
      {
         oWriter.writeElement("PerDatIni_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatIni_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatend_Z)) )
      {
         oWriter.writeElement("PerDatEnd_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatend_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatend_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatend_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatEnd_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdisini_Z)) )
      {
         oWriter.writeElement("PerDatDisIni_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdisini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdisini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdisini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDisIni_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdisfin_Z)) )
      {
         oWriter.writeElement("PerDatDisFin_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdisfin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdisfin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdisfin_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDisFin_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdueini_Z)) )
      {
         oWriter.writeElement("PerDatDueIni_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdueini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdueini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdueini_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDueIni_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatdueend_Z)) )
      {
         oWriter.writeElement("PerDatDueEnd_Z", "0000-00-00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatdueend_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatdueend_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatdueend_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatDueEnd_Z", sDateCnv);
      }
      oWriter.writeElement("PerClose_Z", GXutil.rtrim( gxTv_SdtPeriods_Perclose_Z));
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatins_Z)) )
      {
         oWriter.writeElement("PerDatIns_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtPeriods_Perdatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtPeriods_Perdatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtPeriods_Perdatins_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatIns_Z", sDateCnv);
      }
      if ( (GXutil.nullDate().equals(gxTv_SdtPeriods_Perdatupd_Z)) )
      {
         oWriter.writeElement("PerDatUpd_Z", "0000-00-00T00:00:00");
      }
      else
      {
         sDateCnv = "" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.year( gxTv_SdtPeriods_Perdatupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "0000", 1, 4-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.month( gxTv_SdtPeriods_Perdatupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "-" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.day( gxTv_SdtPeriods_Perdatupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + "T" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.hour( gxTv_SdtPeriods_Perdatupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.minute( gxTv_SdtPeriods_Perdatupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         sDateCnv = sDateCnv + ":" ;
         sNumToPad = GXutil.trim( GXutil.str( GXutil.second( gxTv_SdtPeriods_Perdatupd_Z), 10, 0)) ;
         sDateCnv = sDateCnv + GXutil.substring( "00", 1, 2-GXutil.len( sNumToPad)) + sNumToPad ;
         oWriter.writeElement("PerDatUpd_Z", sDateCnv);
      }
      oWriter.writeElement("PerIDAdm_Z", GXutil.rtrim( gxTv_SdtPeriods_Peridadm_Z));
      oWriter.writeElement("ISODes_N", GXutil.trim( GXutil.str( gxTv_SdtPeriods_Isodes_N, 1, 0)));
      oWriter.writeElement("CurDescription_N", GXutil.trim( GXutil.str( gxTv_SdtPeriods_Curdescription_N, 1, 0)));
      oWriter.writeElement("PerDatDisIni_N", GXutil.trim( GXutil.str( gxTv_SdtPeriods_Perdatdisini_N, 1, 0)));
      oWriter.writeElement("PerDatDisFin_N", GXutil.trim( GXutil.str( gxTv_SdtPeriods_Perdatdisfin_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtPeriods_Isocod( )
   {
      return gxTv_SdtPeriods_Isocod ;
   }

   public void setgxTv_SdtPeriods_Isocod( String value )
   {
      gxTv_SdtPeriods_Isocod = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Isocod_SetNull( )
   {
      gxTv_SdtPeriods_Isocod = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Curcode( )
   {
      return gxTv_SdtPeriods_Curcode ;
   }

   public void setgxTv_SdtPeriods_Curcode( String value )
   {
      gxTv_SdtPeriods_Curcode = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Curcode_SetNull( )
   {
      gxTv_SdtPeriods_Curcode = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Periodtypescode( )
   {
      return gxTv_SdtPeriods_Periodtypescode ;
   }

   public void setgxTv_SdtPeriods_Periodtypescode( String value )
   {
      gxTv_SdtPeriods_Periodtypescode = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Periodtypescode_SetNull( )
   {
      gxTv_SdtPeriods_Periodtypescode = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Perid( )
   {
      return gxTv_SdtPeriods_Perid ;
   }

   public void setgxTv_SdtPeriods_Perid( String value )
   {
      gxTv_SdtPeriods_Perid = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perid_SetNull( )
   {
      gxTv_SdtPeriods_Perid = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Isodes( )
   {
      return gxTv_SdtPeriods_Isodes ;
   }

   public void setgxTv_SdtPeriods_Isodes( String value )
   {
      gxTv_SdtPeriods_Isodes_N = (byte)(0) ;
      gxTv_SdtPeriods_Isodes = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Isodes_SetNull( )
   {
      gxTv_SdtPeriods_Isodes_N = (byte)(1) ;
      gxTv_SdtPeriods_Isodes = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Curdescription( )
   {
      return gxTv_SdtPeriods_Curdescription ;
   }

   public void setgxTv_SdtPeriods_Curdescription( String value )
   {
      gxTv_SdtPeriods_Curdescription_N = (byte)(0) ;
      gxTv_SdtPeriods_Curdescription = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Curdescription_SetNull( )
   {
      gxTv_SdtPeriods_Curdescription_N = (byte)(1) ;
      gxTv_SdtPeriods_Curdescription = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Perdescr( )
   {
      return gxTv_SdtPeriods_Perdescr ;
   }

   public void setgxTv_SdtPeriods_Perdescr( String value )
   {
      gxTv_SdtPeriods_Perdescr = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdescr_SetNull( )
   {
      gxTv_SdtPeriods_Perdescr = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatini( )
   {
      return gxTv_SdtPeriods_Perdatini ;
   }

   public void setgxTv_SdtPeriods_Perdatini( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatini = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatini_SetNull( )
   {
      gxTv_SdtPeriods_Perdatini = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatend( )
   {
      return gxTv_SdtPeriods_Perdatend ;
   }

   public void setgxTv_SdtPeriods_Perdatend( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatend = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatend_SetNull( )
   {
      gxTv_SdtPeriods_Perdatend = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdisini( )
   {
      return gxTv_SdtPeriods_Perdatdisini ;
   }

   public void setgxTv_SdtPeriods_Perdatdisini( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisini_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisini = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdisini_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdisini_N = (byte)(1) ;
      gxTv_SdtPeriods_Perdatdisini = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdisfin( )
   {
      return gxTv_SdtPeriods_Perdatdisfin ;
   }

   public void setgxTv_SdtPeriods_Perdatdisfin( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisfin_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisfin = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdisfin_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdisfin_N = (byte)(1) ;
      gxTv_SdtPeriods_Perdatdisfin = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdueini( )
   {
      return gxTv_SdtPeriods_Perdatdueini ;
   }

   public void setgxTv_SdtPeriods_Perdatdueini( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueini = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdueini_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdueini = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdueend( )
   {
      return gxTv_SdtPeriods_Perdatdueend ;
   }

   public void setgxTv_SdtPeriods_Perdatdueend( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueend = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdueend_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdueend = GXutil.nullDate() ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Perclose( )
   {
      return gxTv_SdtPeriods_Perclose ;
   }

   public void setgxTv_SdtPeriods_Perclose( String value )
   {
      gxTv_SdtPeriods_Perclose = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perclose_SetNull( )
   {
      gxTv_SdtPeriods_Perclose = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatins( )
   {
      return gxTv_SdtPeriods_Perdatins ;
   }

   public void setgxTv_SdtPeriods_Perdatins( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatins = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatins_SetNull( )
   {
      gxTv_SdtPeriods_Perdatins = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatupd( )
   {
      return gxTv_SdtPeriods_Perdatupd ;
   }

   public void setgxTv_SdtPeriods_Perdatupd( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatupd = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatupd_SetNull( )
   {
      gxTv_SdtPeriods_Perdatupd = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public String getgxTv_SdtPeriods_Peridadm( )
   {
      return gxTv_SdtPeriods_Peridadm ;
   }

   public void setgxTv_SdtPeriods_Peridadm( String value )
   {
      gxTv_SdtPeriods_Peridadm = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Peridadm_SetNull( )
   {
      gxTv_SdtPeriods_Peridadm = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Mode( )
   {
      return gxTv_SdtPeriods_Mode ;
   }

   public void setgxTv_SdtPeriods_Mode( String value )
   {
      gxTv_SdtPeriods_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Mode_SetNull( )
   {
      gxTv_SdtPeriods_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Isocod_Z( )
   {
      return gxTv_SdtPeriods_Isocod_Z ;
   }

   public void setgxTv_SdtPeriods_Isocod_Z( String value )
   {
      gxTv_SdtPeriods_Isocod_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Isocod_Z_SetNull( )
   {
      gxTv_SdtPeriods_Isocod_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Curcode_Z( )
   {
      return gxTv_SdtPeriods_Curcode_Z ;
   }

   public void setgxTv_SdtPeriods_Curcode_Z( String value )
   {
      gxTv_SdtPeriods_Curcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Curcode_Z_SetNull( )
   {
      gxTv_SdtPeriods_Curcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Periodtypescode_Z( )
   {
      return gxTv_SdtPeriods_Periodtypescode_Z ;
   }

   public void setgxTv_SdtPeriods_Periodtypescode_Z( String value )
   {
      gxTv_SdtPeriods_Periodtypescode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Periodtypescode_Z_SetNull( )
   {
      gxTv_SdtPeriods_Periodtypescode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Perid_Z( )
   {
      return gxTv_SdtPeriods_Perid_Z ;
   }

   public void setgxTv_SdtPeriods_Perid_Z( String value )
   {
      gxTv_SdtPeriods_Perid_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perid_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perid_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Isodes_Z( )
   {
      return gxTv_SdtPeriods_Isodes_Z ;
   }

   public void setgxTv_SdtPeriods_Isodes_Z( String value )
   {
      gxTv_SdtPeriods_Isodes_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Isodes_Z_SetNull( )
   {
      gxTv_SdtPeriods_Isodes_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Curdescription_Z( )
   {
      return gxTv_SdtPeriods_Curdescription_Z ;
   }

   public void setgxTv_SdtPeriods_Curdescription_Z( String value )
   {
      gxTv_SdtPeriods_Curdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Curdescription_Z_SetNull( )
   {
      gxTv_SdtPeriods_Curdescription_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Perdescr_Z( )
   {
      return gxTv_SdtPeriods_Perdescr_Z ;
   }

   public void setgxTv_SdtPeriods_Perdescr_Z( String value )
   {
      gxTv_SdtPeriods_Perdescr_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdescr_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdescr_Z = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatini_Z( )
   {
      return gxTv_SdtPeriods_Perdatini_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatini_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatini_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatini_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatini_Z = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatend_Z( )
   {
      return gxTv_SdtPeriods_Perdatend_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatend_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatend_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatend_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatend_Z = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdisini_Z( )
   {
      return gxTv_SdtPeriods_Perdatdisini_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatdisini_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisini_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdisini_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdisini_Z = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdisfin_Z( )
   {
      return gxTv_SdtPeriods_Perdatdisfin_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatdisfin_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdisfin_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdisfin_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdisfin_Z = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdueini_Z( )
   {
      return gxTv_SdtPeriods_Perdatdueini_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatdueini_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueini_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdueini_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdueini_Z = GXutil.nullDate() ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatdueend_Z( )
   {
      return gxTv_SdtPeriods_Perdatdueend_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatdueend_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatdueend_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdueend_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdueend_Z = GXutil.nullDate() ;
      return  ;
   }

   public String getgxTv_SdtPeriods_Perclose_Z( )
   {
      return gxTv_SdtPeriods_Perclose_Z ;
   }

   public void setgxTv_SdtPeriods_Perclose_Z( String value )
   {
      gxTv_SdtPeriods_Perclose_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perclose_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perclose_Z = "" ;
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatins_Z( )
   {
      return gxTv_SdtPeriods_Perdatins_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatins_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatins_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatins_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatins_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public java.util.Date getgxTv_SdtPeriods_Perdatupd_Z( )
   {
      return gxTv_SdtPeriods_Perdatupd_Z ;
   }

   public void setgxTv_SdtPeriods_Perdatupd_Z( java.util.Date value )
   {
      gxTv_SdtPeriods_Perdatupd_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatupd_Z_SetNull( )
   {
      gxTv_SdtPeriods_Perdatupd_Z = GXutil.resetTime( GXutil.nullDate() );
      return  ;
   }

   public String getgxTv_SdtPeriods_Peridadm_Z( )
   {
      return gxTv_SdtPeriods_Peridadm_Z ;
   }

   public void setgxTv_SdtPeriods_Peridadm_Z( String value )
   {
      gxTv_SdtPeriods_Peridadm_Z = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Peridadm_Z_SetNull( )
   {
      gxTv_SdtPeriods_Peridadm_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtPeriods_Isodes_N( )
   {
      return gxTv_SdtPeriods_Isodes_N ;
   }

   public void setgxTv_SdtPeriods_Isodes_N( byte value )
   {
      gxTv_SdtPeriods_Isodes_N = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Isodes_N_SetNull( )
   {
      gxTv_SdtPeriods_Isodes_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtPeriods_Curdescription_N( )
   {
      return gxTv_SdtPeriods_Curdescription_N ;
   }

   public void setgxTv_SdtPeriods_Curdescription_N( byte value )
   {
      gxTv_SdtPeriods_Curdescription_N = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Curdescription_N_SetNull( )
   {
      gxTv_SdtPeriods_Curdescription_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtPeriods_Perdatdisini_N( )
   {
      return gxTv_SdtPeriods_Perdatdisini_N ;
   }

   public void setgxTv_SdtPeriods_Perdatdisini_N( byte value )
   {
      gxTv_SdtPeriods_Perdatdisini_N = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdisini_N_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdisini_N = (byte)(0) ;
      return  ;
   }

   public byte getgxTv_SdtPeriods_Perdatdisfin_N( )
   {
      return gxTv_SdtPeriods_Perdatdisfin_N ;
   }

   public void setgxTv_SdtPeriods_Perdatdisfin_N( byte value )
   {
      gxTv_SdtPeriods_Perdatdisfin_N = value ;
      return  ;
   }

   public void setgxTv_SdtPeriods_Perdatdisfin_N_SetNull( )
   {
      gxTv_SdtPeriods_Perdatdisfin_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tperiods_bc obj ;
      obj = new tperiods_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtPeriods_Isocod = "" ;
      gxTv_SdtPeriods_Curcode = "" ;
      gxTv_SdtPeriods_Periodtypescode = "" ;
      gxTv_SdtPeriods_Perid = "" ;
      gxTv_SdtPeriods_Isodes = "" ;
      gxTv_SdtPeriods_Curdescription = "" ;
      gxTv_SdtPeriods_Perdescr = "" ;
      gxTv_SdtPeriods_Perdatini = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatend = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdisini = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdisfin = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdueini = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdueend = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perclose = "" ;
      gxTv_SdtPeriods_Perdatins = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtPeriods_Perdatupd = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtPeriods_Peridadm = "" ;
      gxTv_SdtPeriods_Mode = "" ;
      gxTv_SdtPeriods_Isocod_Z = "" ;
      gxTv_SdtPeriods_Curcode_Z = "" ;
      gxTv_SdtPeriods_Periodtypescode_Z = "" ;
      gxTv_SdtPeriods_Perid_Z = "" ;
      gxTv_SdtPeriods_Isodes_Z = "" ;
      gxTv_SdtPeriods_Curdescription_Z = "" ;
      gxTv_SdtPeriods_Perdescr_Z = "" ;
      gxTv_SdtPeriods_Perdatini_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatend_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdisini_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdisfin_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdueini_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perdatdueend_Z = GXutil.nullDate() ;
      gxTv_SdtPeriods_Perclose_Z = "" ;
      gxTv_SdtPeriods_Perdatins_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtPeriods_Perdatupd_Z = GXutil.resetTime( GXutil.nullDate() );
      gxTv_SdtPeriods_Peridadm_Z = "" ;
      gxTv_SdtPeriods_Isodes_N = (byte)(0) ;
      gxTv_SdtPeriods_Curdescription_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisini_N = (byte)(0) ;
      gxTv_SdtPeriods_Perdatdisfin_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      sDateCnv = "" ;
      sNumToPad = "" ;
      return  ;
   }

   public SdtPeriods Clone( )
   {
      SdtPeriods sdt ;
      tperiods_bc obj ;
      sdt = (SdtPeriods)(clone()) ;
      obj = (tperiods_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtPeriods struct )
   {
      setgxTv_SdtPeriods_Isocod(struct.getIsocod());
      setgxTv_SdtPeriods_Curcode(struct.getCurcode());
      setgxTv_SdtPeriods_Periodtypescode(struct.getPeriodtypescode());
      setgxTv_SdtPeriods_Perid(struct.getPerid());
      setgxTv_SdtPeriods_Isodes(struct.getIsodes());
      setgxTv_SdtPeriods_Curdescription(struct.getCurdescription());
      setgxTv_SdtPeriods_Perdescr(struct.getPerdescr());
      setgxTv_SdtPeriods_Perdatini(struct.getPerdatini());
      setgxTv_SdtPeriods_Perdatend(struct.getPerdatend());
      setgxTv_SdtPeriods_Perdatdisini(struct.getPerdatdisini());
      setgxTv_SdtPeriods_Perdatdisfin(struct.getPerdatdisfin());
      setgxTv_SdtPeriods_Perdatdueini(struct.getPerdatdueini());
      setgxTv_SdtPeriods_Perdatdueend(struct.getPerdatdueend());
      setgxTv_SdtPeriods_Perclose(struct.getPerclose());
      setgxTv_SdtPeriods_Perdatins(struct.getPerdatins());
      setgxTv_SdtPeriods_Perdatupd(struct.getPerdatupd());
      setgxTv_SdtPeriods_Peridadm(struct.getPeridadm());
      setgxTv_SdtPeriods_Mode(struct.getMode());
      setgxTv_SdtPeriods_Isocod_Z(struct.getIsocod_Z());
      setgxTv_SdtPeriods_Curcode_Z(struct.getCurcode_Z());
      setgxTv_SdtPeriods_Periodtypescode_Z(struct.getPeriodtypescode_Z());
      setgxTv_SdtPeriods_Perid_Z(struct.getPerid_Z());
      setgxTv_SdtPeriods_Isodes_Z(struct.getIsodes_Z());
      setgxTv_SdtPeriods_Curdescription_Z(struct.getCurdescription_Z());
      setgxTv_SdtPeriods_Perdescr_Z(struct.getPerdescr_Z());
      setgxTv_SdtPeriods_Perdatini_Z(struct.getPerdatini_Z());
      setgxTv_SdtPeriods_Perdatend_Z(struct.getPerdatend_Z());
      setgxTv_SdtPeriods_Perdatdisini_Z(struct.getPerdatdisini_Z());
      setgxTv_SdtPeriods_Perdatdisfin_Z(struct.getPerdatdisfin_Z());
      setgxTv_SdtPeriods_Perdatdueini_Z(struct.getPerdatdueini_Z());
      setgxTv_SdtPeriods_Perdatdueend_Z(struct.getPerdatdueend_Z());
      setgxTv_SdtPeriods_Perclose_Z(struct.getPerclose_Z());
      setgxTv_SdtPeriods_Perdatins_Z(struct.getPerdatins_Z());
      setgxTv_SdtPeriods_Perdatupd_Z(struct.getPerdatupd_Z());
      setgxTv_SdtPeriods_Peridadm_Z(struct.getPeridadm_Z());
      setgxTv_SdtPeriods_Isodes_N(struct.getIsodes_N());
      setgxTv_SdtPeriods_Curdescription_N(struct.getCurdescription_N());
      setgxTv_SdtPeriods_Perdatdisini_N(struct.getPerdatdisini_N());
      setgxTv_SdtPeriods_Perdatdisfin_N(struct.getPerdatdisfin_N());
   }

   public StructSdtPeriods getStruct( )
   {
      StructSdtPeriods struct = new StructSdtPeriods ();
      struct.setIsocod(getgxTv_SdtPeriods_Isocod());
      struct.setCurcode(getgxTv_SdtPeriods_Curcode());
      struct.setPeriodtypescode(getgxTv_SdtPeriods_Periodtypescode());
      struct.setPerid(getgxTv_SdtPeriods_Perid());
      struct.setIsodes(getgxTv_SdtPeriods_Isodes());
      struct.setCurdescription(getgxTv_SdtPeriods_Curdescription());
      struct.setPerdescr(getgxTv_SdtPeriods_Perdescr());
      struct.setPerdatini(getgxTv_SdtPeriods_Perdatini());
      struct.setPerdatend(getgxTv_SdtPeriods_Perdatend());
      struct.setPerdatdisini(getgxTv_SdtPeriods_Perdatdisini());
      struct.setPerdatdisfin(getgxTv_SdtPeriods_Perdatdisfin());
      struct.setPerdatdueini(getgxTv_SdtPeriods_Perdatdueini());
      struct.setPerdatdueend(getgxTv_SdtPeriods_Perdatdueend());
      struct.setPerclose(getgxTv_SdtPeriods_Perclose());
      struct.setPerdatins(getgxTv_SdtPeriods_Perdatins());
      struct.setPerdatupd(getgxTv_SdtPeriods_Perdatupd());
      struct.setPeridadm(getgxTv_SdtPeriods_Peridadm());
      struct.setMode(getgxTv_SdtPeriods_Mode());
      struct.setIsocod_Z(getgxTv_SdtPeriods_Isocod_Z());
      struct.setCurcode_Z(getgxTv_SdtPeriods_Curcode_Z());
      struct.setPeriodtypescode_Z(getgxTv_SdtPeriods_Periodtypescode_Z());
      struct.setPerid_Z(getgxTv_SdtPeriods_Perid_Z());
      struct.setIsodes_Z(getgxTv_SdtPeriods_Isodes_Z());
      struct.setCurdescription_Z(getgxTv_SdtPeriods_Curdescription_Z());
      struct.setPerdescr_Z(getgxTv_SdtPeriods_Perdescr_Z());
      struct.setPerdatini_Z(getgxTv_SdtPeriods_Perdatini_Z());
      struct.setPerdatend_Z(getgxTv_SdtPeriods_Perdatend_Z());
      struct.setPerdatdisini_Z(getgxTv_SdtPeriods_Perdatdisini_Z());
      struct.setPerdatdisfin_Z(getgxTv_SdtPeriods_Perdatdisfin_Z());
      struct.setPerdatdueini_Z(getgxTv_SdtPeriods_Perdatdueini_Z());
      struct.setPerdatdueend_Z(getgxTv_SdtPeriods_Perdatdueend_Z());
      struct.setPerclose_Z(getgxTv_SdtPeriods_Perclose_Z());
      struct.setPerdatins_Z(getgxTv_SdtPeriods_Perdatins_Z());
      struct.setPerdatupd_Z(getgxTv_SdtPeriods_Perdatupd_Z());
      struct.setPeridadm_Z(getgxTv_SdtPeriods_Peridadm_Z());
      struct.setIsodes_N(getgxTv_SdtPeriods_Isodes_N());
      struct.setCurdescription_N(getgxTv_SdtPeriods_Curdescription_N());
      struct.setPerdatdisini_N(getgxTv_SdtPeriods_Perdatdisini_N());
      struct.setPerdatdisfin_N(getgxTv_SdtPeriods_Perdatdisfin_N());
      return struct ;
   }

   protected byte gxTv_SdtPeriods_Isodes_N ;
   protected byte gxTv_SdtPeriods_Curdescription_N ;
   protected byte gxTv_SdtPeriods_Perdatdisini_N ;
   protected byte gxTv_SdtPeriods_Perdatdisfin_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtPeriods_Perclose ;
   protected String gxTv_SdtPeriods_Mode ;
   protected String gxTv_SdtPeriods_Perclose_Z ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String sDateCnv ;
   protected String sNumToPad ;
   protected java.util.Date gxTv_SdtPeriods_Perdatins ;
   protected java.util.Date gxTv_SdtPeriods_Perdatupd ;
   protected java.util.Date gxTv_SdtPeriods_Perdatins_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatupd_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatini ;
   protected java.util.Date gxTv_SdtPeriods_Perdatend ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisini ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisfin ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueini ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueend ;
   protected java.util.Date gxTv_SdtPeriods_Perdatini_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatend_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisini_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdisfin_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueini_Z ;
   protected java.util.Date gxTv_SdtPeriods_Perdatdueend_Z ;
   protected String gxTv_SdtPeriods_Isocod ;
   protected String gxTv_SdtPeriods_Curcode ;
   protected String gxTv_SdtPeriods_Periodtypescode ;
   protected String gxTv_SdtPeriods_Perid ;
   protected String gxTv_SdtPeriods_Isodes ;
   protected String gxTv_SdtPeriods_Curdescription ;
   protected String gxTv_SdtPeriods_Perdescr ;
   protected String gxTv_SdtPeriods_Peridadm ;
   protected String gxTv_SdtPeriods_Isocod_Z ;
   protected String gxTv_SdtPeriods_Curcode_Z ;
   protected String gxTv_SdtPeriods_Periodtypescode_Z ;
   protected String gxTv_SdtPeriods_Perid_Z ;
   protected String gxTv_SdtPeriods_Isodes_Z ;
   protected String gxTv_SdtPeriods_Curdescription_Z ;
   protected String gxTv_SdtPeriods_Perdescr_Z ;
   protected String gxTv_SdtPeriods_Peridadm_Z ;
}


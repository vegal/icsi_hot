/*
               File: sanitychecki1012_23
        Description: sanitycheck report i1012 e i1023
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:26:23.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class psanitychecki1012_23 extends GXProcedure
{
   public psanitychecki1012_23( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( psanitychecki1012_23.class ), "" );
   }

   public psanitychecki1012_23( int remoteHandle ,
                                ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String[] aP2 )
   {
      psanitychecki1012_23.this.AV24FileNa = aP0;
      psanitychecki1012_23.this.AV48RptTip = aP1;
      psanitychecki1012_23.this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      AV26Caminh = AV24FileNa ;
      AV18File.setSource( AV26Caminh );
      if ( AV18File.exists() )
      {
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfropen( AV26Caminh, 255, "%", "\"", "") ;
         AV28RegCou = 0 ;
         AV10ErroHe = "Header Not Found." ;
         AV11ErroTr = "Trailer Not Found." ;
         while ( ( context.getSessionInstances().getDelimitedFiles().dfrnext( ) == 0 ) )
         {
            GXv_char1[0] = AV9linha ;
            GXt_int2 = context.getSessionInstances().getDelimitedFiles().dfrgtxt( GXv_char1, (short)(255)) ;
            AV9linha = GXv_char1[0] ;
            AV27RetVal = GXt_int2 ;
            AV28RegCou = (long)(AV28RegCou+1) ;
            if ( ( AV28RegCou == 1 ) )
            {
               /* Execute user subroutine: S1183 */
               S1183 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               AV10ErroHe = AV15Erro ;
            }
         }
         if ( ! ((GXutil.strcmp("", GXutil.rtrim( AV9linha))==0)) )
         {
            /* Execute user subroutine: S12143 */
            S12143 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
            AV11ErroTr = AV15Erro ;
         }
         AV27RetVal = context.getSessionInstances().getDelimitedFiles().dfrclose( ) ;
         AV25msgErr = "" ;
         if ( ! ( ((GXutil.strcmp("", GXutil.rtrim( AV10ErroHe))==0)) && ((GXutil.strcmp("", GXutil.rtrim( AV11ErroTr))==0)) ) )
         {
            AV25msgErr = AV10ErroHe + "=ErrHeader / ErrTrailer=" + AV11ErroTr ;
            GXt_svchar3 = AV29ShortN ;
            GXv_char1[0] = AV26Caminh ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2shortname(remoteHandle, context).execute( GXv_char1, GXv_char4) ;
            psanitychecki1012_23.this.AV26Caminh = GXv_char1[0] ;
            psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
            AV29ShortN = GXt_svchar3 ;
            GXt_svchar3 = AV12BasePa ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getpath(remoteHandle, context).execute( AV26Caminh, GXv_char4) ;
            psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
            AV12BasePa = GXt_svchar3 ;
            AV30SubFol = "Error\\" ;
            AV12BasePa = AV12BasePa + AV30SubFol ;
            AV19NewFil = GXutil.trim( AV12BasePa) + GXutil.trim( AV29ShortN) ;
            AV18File.setSource( AV26Caminh );
            AV18File.rename(GXutil.trim( AV19NewFil));
            AV14ShortN = GXutil.trim( AV29ShortN) + ".err" ;
            AV31Arquiv = GXutil.trim( AV12BasePa) + GXutil.trim( AV14ShortN) ;
            AV13LOGFil.openURL(AV31Arquiv);
            AV10ErroHe = AV10ErroHe + GXutil.newLine( ) ;
            AV11ErroTr = AV11ErroTr + GXutil.newLine( ) ;
            AV13LOGFil.writeRawText(AV10ErroHe);
            AV13LOGFil.writeRawText(AV11ErroTr);
            AV13LOGFil.close();
            if ( ( GXutil.strcmp(AV48RptTip, "i1012") == 0 ) )
            {
               GXt_svchar3 = AV23Subjec ;
               GXv_char4[0] = GXt_svchar3 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_SUBJ_12", "Mensagem no subject do e-mail do tipo Sanity i1012", "S", "Problem file i1012 generation", GXv_char4) ;
               psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
               AV23Subjec = GXt_svchar3 ;
               GXt_svchar3 = AV21Body ;
               GXv_char4[0] = GXt_svchar3 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_BODY_12", "Mensagem no corpo do e-mail do tipo Sanity i1012", "S", "<br>The file [FILE] type i1012 was rejected in the Sanity Check.<br><br>Please check the file.<br><br>Thanks", GXv_char4) ;
               psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
               AV21Body = GXt_svchar3 ;
            }
            else
            {
               GXt_svchar3 = AV23Subjec ;
               GXv_char4[0] = GXt_svchar3 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_SUBJ_23", "Mensagem no subject do e-mail do tipo Sanity i1023", "S", "Problem file i1023 generation", GXv_char4) ;
               psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
               AV23Subjec = GXt_svchar3 ;
               GXt_svchar3 = AV21Body ;
               GXv_char4[0] = GXt_svchar3 ;
               new pr2getparm(remoteHandle, context).execute( "EMAIL_SANITY_BODY_23", "Mensagem no corpo do e-mail do tipo Sanity i1023", "S", "<br>The file [FILE] type i1023 was rejected in the Sanity Check.<br><br>Please check the file.<br><br>Thanks", GXv_char4) ;
               psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
               AV21Body = GXt_svchar3 ;
            }
            AV21Body = GXutil.strReplace( AV21Body, "[FILE]", AV29ShortN) ;
            GXt_svchar3 = AV40To ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_TO", "", "S", "", GXv_char4) ;
            psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
            AV40To = GXutil.trim( GXt_svchar3) ;
            GXt_svchar3 = AV22CC ;
            GXv_char4[0] = GXt_svchar3 ;
            new pr2getparm(remoteHandle, context).execute( "EMAIL_CC", "", "S", "", GXv_char4) ;
            psanitychecki1012_23.this.GXt_svchar3 = GXv_char4[0] ;
            AV22CC = GXutil.trim( GXt_svchar3) ;
            AV20BCC = "" ;
            GX_I = 1 ;
            while ( ( GX_I <= 5 ) )
            {
               AV8Anexos[GX_I-1] = "" ;
               GX_I = (int)(GX_I+1) ;
            }
            new penviaemail(remoteHandle, context).execute( AV23Subjec, AV21Body, AV40To, AV22CC, AV20BCC, AV8Anexos) ;
         }
      }
      cleanup();
   }

   public void S1183( )
   {
      /* 'CHECKHEADER' Routine */
      AV15Erro = "" ;
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 1), "C") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Identifier Not equal C /" ;
      }
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 2, 3), "957") != 0 ) )
      {
         AV15Erro = AV15Erro + "Header Company Not equal 957 / " ;
      }
      AV32yy = GXutil.substring( AV9linha, 5, 2) ;
      AV33mm = GXutil.substring( AV9linha, 7, 2) ;
      AV34dd = GXutil.substring( AV9linha, 9, 2) ;
      AV41val = (int)(GXutil.val( AV32yy, ".")) ;
      if ( ( AV41val > 40 ) )
      {
         AV35Nyy = (int)(GXutil.val( "19"+GXutil.trim( AV32yy), ".")) ;
      }
      else
      {
         AV35Nyy = (int)(GXutil.val( "20"+GXutil.trim( AV32yy), ".")) ;
      }
      AV36Nmm = (int)(GXutil.val( AV33mm, ".")) ;
      AV37Ndd = (int)(GXutil.val( AV34dd, ".")) ;
      AV49DataSu = localUtil.ymdtod( AV35Nyy, AV36Nmm, AV37Ndd) ;
      if ( (GXutil.nullDate().equals(AV49DataSu)) )
      {
         AV15Erro = AV15Erro + "Header Submission Date Not Valid / " ;
      }
      AV38Sequen = GXutil.substring( AV9linha, 11, 3) ;
      if ( ( GXutil.val( AV38Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Sequencial Not Valid / " ;
      }
      AV50yyyy = GXutil.substring( AV9linha, 14, 4) ;
      AV33mm = GXutil.substring( AV9linha, 18, 2) ;
      AV34dd = GXutil.substring( AV9linha, 20, 2) ;
      AV42hh = GXutil.substring( AV9linha, 22, 2) ;
      AV43mi = GXutil.substring( AV9linha, 24, 2) ;
      AV35Nyy = (int)(GXutil.val( GXutil.trim( AV50yyyy), ".")) ;
      AV36Nmm = (int)(GXutil.val( AV33mm, ".")) ;
      AV37Ndd = (int)(GXutil.val( AV34dd, ".")) ;
      AV45Nhh = (int)(GXutil.val( AV42hh, ".")) ;
      AV46Nmi = (int)(GXutil.val( AV43mi, ".")) ;
      AV47Nss = (int)(GXutil.val( AV44ss, ".")) ;
      AV16DataGe = localUtil.ymdhmsToT( (short)(AV35Nyy), (byte)(AV36Nmm), (byte)(AV37Ndd), (byte)(AV45Nhh), (byte)(AV46Nmi), (byte)(AV47Nss)) ;
      if ( (GXutil.nullDate().equals(AV16DataGe)) )
      {
         AV15Erro = AV15Erro + "Header Processing Date Not Valid / " ;
      }
      AV39Sequen = GXutil.substring( AV9linha, 26, 5) ;
      if ( ( GXutil.val( AV39Sequen, ".") <= 0 ) )
      {
         AV15Erro = AV15Erro + "Header Invoice Not Valid / " ;
      }
   }

   public void S12143( )
   {
      /* 'CHECKTRAILER' Routine */
      AV15Erro = "" ;
      if ( ( GXutil.strcmp(GXutil.substring( AV9linha, 1, 1), "F") != 0 ) )
      {
         AV15Erro = AV15Erro + "Trailer Identifier Not equal F " ;
      }
      AV51RegCou = (long)(AV28RegCou-2) ;
      AV52TotalR = (long)(GXutil.val( GXutil.substring( AV9linha, 2, 7), ".")) ;
      if ( ( AV52TotalR != AV51RegCou ) )
      {
         AV15Erro = AV15Erro + "Trailer Total Lines Not OK: " + GXutil.trim( GXutil.str( AV52TotalR, 10, 0)) + " <> " + GXutil.trim( GXutil.str( AV51RegCou, 10, 0)) + " / " ;
      }
   }

   protected void cleanup( )
   {
      this.aP2[0] = psanitychecki1012_23.this.AV25msgErr;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV25msgErr = "" ;
      AV26Caminh = "" ;
      AV18File = new com.genexus.util.GXFile();
      AV27RetVal = 0 ;
      AV28RegCou = 0 ;
      AV10ErroHe = "" ;
      AV11ErroTr = "" ;
      AV9linha = "" ;
      GXt_int2 = (short)(0) ;
      returnInSub = false ;
      AV15Erro = "" ;
      AV29ShortN = "" ;
      GXv_char1 = new String [1] ;
      AV12BasePa = "" ;
      AV30SubFol = "" ;
      AV19NewFil = "" ;
      AV14ShortN = "" ;
      AV31Arquiv = "" ;
      AV13LOGFil = new com.genexus.xml.XMLWriter();
      AV23Subjec = "" ;
      AV21Body = "" ;
      AV40To = "" ;
      AV22CC = "" ;
      GXv_char4 = new String [1] ;
      AV20BCC = "" ;
      GX_I = 0 ;
      AV8Anexos = new String [5] ;
      GX_I = 1 ;
      while ( ( GX_I <= 5 ) )
      {
         AV8Anexos[GX_I-1] = "" ;
         GX_I = (int)(GX_I+1) ;
      }
      AV32yy = "" ;
      AV33mm = "" ;
      AV34dd = "" ;
      AV41val = 0 ;
      AV35Nyy = 0 ;
      GXt_svchar3 = "" ;
      AV36Nmm = 0 ;
      AV37Ndd = 0 ;
      AV49DataSu = GXutil.nullDate() ;
      AV38Sequen = "" ;
      AV50yyyy = "" ;
      AV42hh = "" ;
      AV43mi = "" ;
      AV45Nhh = 0 ;
      AV46Nmi = 0 ;
      AV47Nss = 0 ;
      AV44ss = "" ;
      AV16DataGe = GXutil.resetTime( GXutil.nullDate() );
      AV39Sequen = "" ;
      AV51RegCou = 0 ;
      AV52TotalR = 0 ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short GXt_int2 ;
   private short Gx_err ;
   private int GX_I ;
   private int AV41val ;
   private int AV35Nyy ;
   private int AV36Nmm ;
   private int AV37Ndd ;
   private int AV45Nhh ;
   private int AV46Nmi ;
   private int AV47Nss ;
   private long AV27RetVal ;
   private long AV28RegCou ;
   private long AV51RegCou ;
   private long AV52TotalR ;
   private String GXv_char1[] ;
   private String AV12BasePa ;
   private String GXv_char4[] ;
   private String AV32yy ;
   private String AV33mm ;
   private String AV34dd ;
   private String AV50yyyy ;
   private String AV42hh ;
   private String AV43mi ;
   private String AV44ss ;
   private java.util.Date AV16DataGe ;
   private java.util.Date AV49DataSu ;
   private boolean returnInSub ;
   private String AV24FileNa ;
   private String AV48RptTip ;
   private String AV25msgErr ;
   private String AV26Caminh ;
   private String AV10ErroHe ;
   private String AV11ErroTr ;
   private String AV9linha ;
   private String AV15Erro ;
   private String AV29ShortN ;
   private String AV30SubFol ;
   private String AV19NewFil ;
   private String AV14ShortN ;
   private String AV31Arquiv ;
   private String AV23Subjec ;
   private String AV21Body ;
   private String AV40To ;
   private String AV22CC ;
   private String AV20BCC ;
   private String AV8Anexos[] ;
   private String GXt_svchar3 ;
   private String AV38Sequen ;
   private String AV39Sequen ;
   private com.genexus.xml.XMLWriter AV13LOGFil ;
   private com.genexus.util.GXFile AV18File ;
   private String[] aP2 ;
}


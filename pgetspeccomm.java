/*
               File: getSpecComm
        Description: Obt�m Comiss�o Espec�fica do SACI
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: May 25, 2020 17:25:58.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pgetspeccomm extends GXProcedure
{
   public pgetspeccomm( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pgetspeccomm.class ), "" );
   }

   public pgetspeccomm( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( String aP0 ,
                        String aP1 ,
                        String aP2 ,
                        String aP3 ,
                        String[] aP4 )
   {
      execute_int(aP0, aP1, aP2, aP3, aP4);
   }

   private void execute_int( String aP0 ,
                             String aP1 ,
                             String aP2 ,
                             String aP3 ,
                             String[] aP4 )
   {
      pgetspeccomm.this.AV16TACN = aP0;
      pgetspeccomm.this.AV15STAT = aP1;
      pgetspeccomm.this.AV14AGTN = aP2;
      pgetspeccomm.this.AV10CORT = aP3;
      pgetspeccomm.this.aP4 = aP4;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      if ( ( GXutil.val( AV10CORT, ".") == 0 ) )
      {
         AV11new_CO = AV10CORT ;
      }
      else
      {
         AV12xSTAT = GXutil.upper( GXutil.left( AV15STAT, 1)) ;
         if ( ( GXutil.strcmp(AV12xSTAT, "N") == 0 ) )
         {
            AV12xSTAT = "D" ;
         }
         else if ( ( GXutil.strcmp(AV12xSTAT, "S") == 0 ) )
         {
            AV12xSTAT = "I" ;
         }
         else
         {
            if ( ( GXutil.strcmp(AV12xSTAT, "D") != 0 ) && ( GXutil.strcmp(AV12xSTAT, "I") != 0 ) )
            {
               AV12xSTAT = "D" ;
            }
         }
         AV13xTACN = GXutil.left( AV16TACN, 3) ;
         AV8xAGTN = GXutil.left( AV14AGTN, 7) ;
         if ( ( GXutil.strcmp(AV12xSTAT, "I") == 0 ) && ( GXutil.strcmp(GXutil.left( AV8xAGTN, 3), "570") == 0 ) )
         {
            AV9ComValo = 0 ;
            /* Execute user subroutine: S1174 */
            S1174 ();
            if ( returnInSub )
            {
               returnInSub = true;
               cleanup();
               if (true) return;
            }
         }
         else
         {
            AV20GXLvl3 = (byte)(0) ;
            /* Using cursor P005Q2 */
            pr_default.execute(0, new Object[] {AV8xAGTN, AV13xTACN, Gx_date, AV12xSTAT});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1388ComVa = P005Q2_A1388ComVa[0] ;
               n1388ComVa = P005Q2_n1388ComVa[0] ;
               A1401ComDa = P005Q2_A1401ComDa[0] ;
               A1400ComTi = P005Q2_A1400ComTi[0] ;
               A1233EmpCo = P005Q2_A1233EmpCo[0] ;
               A1313CadIA = P005Q2_A1313CadIA[0] ;
               AV20GXLvl3 = (byte)(1) ;
               AV9ComValo = A1388ComVa ;
               /* Execute user subroutine: S1174 */
               S1174 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( ( AV20GXLvl3 == 0 ) )
            {
               /* Execute user subroutine: S1290 */
               S1290 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  cleanup();
                  if (true) return;
               }
               if ( ( GXutil.strcmp(AV17CadEst, "") == 0 ) )
               {
                  AV22GXLvl5 = (byte)(0) ;
                  /* Using cursor P005Q3 */
                  pr_default.execute(1, new Object[] {AV13xTACN, AV12xSTAT, Gx_date});
                  while ( (pr_default.getStatus(1) != 101) )
                  {
                     A1271CoEmp = P005Q3_A1271CoEmp[0] ;
                     A1270CoEmp = P005Q3_A1270CoEmp[0] ;
                     A1233EmpCo = P005Q3_A1233EmpCo[0] ;
                     A1263CoEmp = P005Q3_A1263CoEmp[0] ;
                     n1263CoEmp = P005Q3_n1263CoEmp[0] ;
                     AV22GXLvl5 = (byte)(1) ;
                     AV9ComValo = A1263CoEmp ;
                     /* Execute user subroutine: S1174 */
                     S1174 ();
                     if ( returnInSub )
                     {
                        pr_default.close(1);
                        returnInSub = true;
                        cleanup();
                        if (true) return;
                     }
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(1);
                  }
                  pr_default.close(1);
                  if ( ( AV22GXLvl5 == 0 ) )
                  {
                     AV11new_CO = AV10CORT ;
                  }
               }
            }
         }
      }
      cleanup();
   }

   public void S1174( )
   {
      /* 'MAKECORT' Routine */
      if ( ( AV9ComValo > 100 ) )
      {
         AV9ComValo = 0 ;
      }
      AV11new_CO = GXutil.str( AV9ComValo*100, 5, 0) ;
      AV11new_CO = GXutil.strReplace( AV11new_CO, " ", "0") ;
      AV11new_CO = GXutil.left( AV11new_CO, 5) ;
   }

   public void S1290( )
   {
      /* 'GETUFCOMM' Routine */
      AV23GXLvl1 = (byte)(0) ;
      /* Using cursor P005Q4 */
      pr_default.execute(2, new Object[] {AV8xAGTN});
      while ( (pr_default.getStatus(2) != 101) )
      {
         A1313CadIA = P005Q4_A1313CadIA[0] ;
         A1344CadEs = P005Q4_A1344CadEs[0] ;
         n1344CadEs = P005Q4_n1344CadEs[0] ;
         AV23GXLvl1 = (byte)(1) ;
         AV17CadEst = A1344CadEs ;
         AV24GXLvl1 = (byte)(0) ;
         /* Using cursor P005Q5 */
         pr_default.execute(3, new Object[] {AV13xTACN, AV17CadEst, Gx_date, AV12xSTAT});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1233EmpCo = P005Q5_A1233EmpCo[0] ;
            A1272UfCod = P005Q5_A1272UfCod[0] ;
            A1275UfCom = P005Q5_A1275UfCom[0] ;
            A1264UfCom = P005Q5_A1264UfCom[0] ;
            n1264UfCom = P005Q5_n1264UfCom[0] ;
            A1274UfCom = P005Q5_A1274UfCom[0] ;
            AV24GXLvl1 = (byte)(1) ;
            AV9ComValo = A1264UfCom ;
            /* Execute user subroutine: S1174 */
            S1174 ();
            if ( returnInSub )
            {
               pr_default.close(3);
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( ( AV24GXLvl1 == 0 ) )
         {
            AV17CadEst = "" ;
         }
         /* Exiting from a For First loop. */
         if (true) break;
      }
      pr_default.close(2);
      if ( ( AV23GXLvl1 == 0 ) )
      {
         AV17CadEst = "" ;
      }
   }

   protected void cleanup( )
   {
      this.aP4[0] = pgetspeccomm.this.AV11new_CO;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      AV11new_CO = "" ;
      AV12xSTAT = "" ;
      AV13xTACN = "" ;
      AV8xAGTN = "" ;
      AV9ComValo = 0 ;
      returnInSub = false ;
      AV20GXLvl3 = (byte)(0) ;
      scmdbuf = "" ;
      Gx_date = GXutil.nullDate() ;
      P005Q2_A1388ComVa = new double[1] ;
      P005Q2_n1388ComVa = new boolean[] {false} ;
      P005Q2_A1401ComDa = new java.util.Date[] {GXutil.nullDate()} ;
      P005Q2_A1400ComTi = new String[] {""} ;
      P005Q2_A1233EmpCo = new String[] {""} ;
      P005Q2_A1313CadIA = new String[] {""} ;
      A1388ComVa = 0 ;
      n1388ComVa = false ;
      A1401ComDa = GXutil.nullDate() ;
      A1400ComTi = "" ;
      A1233EmpCo = "" ;
      A1313CadIA = "" ;
      AV17CadEst = "" ;
      AV22GXLvl5 = (byte)(0) ;
      P005Q3_A1271CoEmp = new String[] {""} ;
      P005Q3_A1270CoEmp = new java.util.Date[] {GXutil.nullDate()} ;
      P005Q3_A1233EmpCo = new String[] {""} ;
      P005Q3_A1263CoEmp = new double[1] ;
      P005Q3_n1263CoEmp = new boolean[] {false} ;
      A1271CoEmp = "" ;
      A1270CoEmp = GXutil.nullDate() ;
      A1263CoEmp = 0 ;
      n1263CoEmp = false ;
      AV23GXLvl1 = (byte)(0) ;
      P005Q4_A1313CadIA = new String[] {""} ;
      P005Q4_A1344CadEs = new String[] {""} ;
      P005Q4_n1344CadEs = new boolean[] {false} ;
      A1344CadEs = "" ;
      n1344CadEs = false ;
      AV24GXLvl1 = (byte)(0) ;
      P005Q5_A1233EmpCo = new String[] {""} ;
      P005Q5_A1272UfCod = new String[] {""} ;
      P005Q5_A1275UfCom = new String[] {""} ;
      P005Q5_A1264UfCom = new double[1] ;
      P005Q5_n1264UfCom = new boolean[] {false} ;
      P005Q5_A1274UfCom = new java.util.Date[] {GXutil.nullDate()} ;
      A1272UfCod = "" ;
      A1275UfCom = "" ;
      A1264UfCom = 0 ;
      n1264UfCom = false ;
      A1274UfCom = GXutil.nullDate() ;
      pr_default = new DataStoreProvider(context, remoteHandle, new pgetspeccomm__default(),
         new Object[] {
             new Object[] {
            P005Q2_A1388ComVa, P005Q2_n1388ComVa, P005Q2_A1401ComDa, P005Q2_A1400ComTi, P005Q2_A1233EmpCo, P005Q2_A1313CadIA
            }
            , new Object[] {
            P005Q3_A1271CoEmp, P005Q3_A1270CoEmp, P005Q3_A1233EmpCo, P005Q3_A1263CoEmp, P005Q3_n1263CoEmp
            }
            , new Object[] {
            P005Q4_A1313CadIA, P005Q4_A1344CadEs, P005Q4_n1344CadEs
            }
            , new Object[] {
            P005Q5_A1233EmpCo, P005Q5_A1272UfCod, P005Q5_A1275UfCom, P005Q5_A1264UfCom, P005Q5_n1264UfCom, P005Q5_A1274UfCom
            }
         }
      );
      Gx_date = GXutil.today( ) ;
      /* GeneXus formulas. */
      Gx_date = GXutil.today( ) ;
      Gx_err = (short)(0) ;
   }

   private byte AV20GXLvl3 ;
   private byte AV22GXLvl5 ;
   private byte AV23GXLvl1 ;
   private byte AV24GXLvl1 ;
   private short Gx_err ;
   private double AV9ComValo ;
   private double A1388ComVa ;
   private double A1263CoEmp ;
   private double A1264UfCom ;
   private String AV16TACN ;
   private String AV15STAT ;
   private String AV14AGTN ;
   private String AV10CORT ;
   private String AV11new_CO ;
   private String AV12xSTAT ;
   private String AV13xTACN ;
   private String AV8xAGTN ;
   private String scmdbuf ;
   private String A1400ComTi ;
   private String A1233EmpCo ;
   private String A1313CadIA ;
   private String AV17CadEst ;
   private String A1271CoEmp ;
   private String A1344CadEs ;
   private String A1272UfCod ;
   private String A1275UfCom ;
   private java.util.Date Gx_date ;
   private java.util.Date A1401ComDa ;
   private java.util.Date A1270CoEmp ;
   private java.util.Date A1274UfCom ;
   private boolean returnInSub ;
   private boolean n1388ComVa ;
   private boolean n1263CoEmp ;
   private boolean n1344CadEs ;
   private boolean n1264UfCom ;
   private String[] aP4 ;
   private IDataStoreProvider pr_default ;
   private double[] P005Q2_A1388ComVa ;
   private boolean[] P005Q2_n1388ComVa ;
   private java.util.Date[] P005Q2_A1401ComDa ;
   private String[] P005Q2_A1400ComTi ;
   private String[] P005Q2_A1233EmpCo ;
   private String[] P005Q2_A1313CadIA ;
   private String[] P005Q3_A1271CoEmp ;
   private java.util.Date[] P005Q3_A1270CoEmp ;
   private String[] P005Q3_A1233EmpCo ;
   private double[] P005Q3_A1263CoEmp ;
   private boolean[] P005Q3_n1263CoEmp ;
   private String[] P005Q4_A1313CadIA ;
   private String[] P005Q4_A1344CadEs ;
   private boolean[] P005Q4_n1344CadEs ;
   private String[] P005Q5_A1233EmpCo ;
   private String[] P005Q5_A1272UfCod ;
   private String[] P005Q5_A1275UfCom ;
   private double[] P005Q5_A1264UfCom ;
   private boolean[] P005Q5_n1264UfCom ;
   private java.util.Date[] P005Q5_A1274UfCom ;
}

final  class pgetspeccomm__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P005Q2", "SELECT TOP 1 [ComValor], [ComData], [ComTipTrafic], [EmpCod], [CadIATA] FROM [COMMESP] WITH (NOLOCK) WHERE ([CadIATA] = ? and [EmpCod] = ? and [ComData] <= ?) AND ([ComValor] >= 0) AND ([ComTipTrafic] = ?) ORDER BY [CadIATA], [EmpCod], [ComData] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005Q3", "SELECT TOP 1 [CoEmpTiptraf], [CoEmpData], [EmpCod], [CoEmpValor] FROM [EMPRESAS1] WITH (NOLOCK) WHERE [EmpCod] = ? and [CoEmpTiptraf] = ? and [CoEmpData] <= ? ORDER BY [EmpCod], [CoEmpTiptraf], [CoEmpData] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005Q4", "SELECT [CadIATA], [CadEstado] FROM [CADAGE] WITH (NOLOCK) WHERE [CadIATA] = ? ORDER BY [CadIATA] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
         ,new ForEachCursor("P005Q5", "SELECT TOP 1 [EmpCod], [UfCod], [UfComTipTraf], [UfComValor], [UfComData] FROM [EMPRESAS2] WITH (NOLOCK) WHERE ([EmpCod] = ? and [UfCod] = ? and [UfComData] <= ?) AND ([UfComValor] >= 0) AND ([UfComTipTraf] = ?) ORDER BY [EmpCod], [UfCod], [UfComData] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,0,true )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((double[]) buf[0])[0] = rslt.getDouble(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[2])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
               ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
               ((String[]) buf[5])[0] = rslt.getString(5, 11) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
               ((java.util.Date[]) buf[1])[0] = rslt.getGXDate(2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
               ((double[]) buf[3])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 11) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               break;
            case 3 :
               ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
               ((double[]) buf[3])[0] = rslt.getDouble(4) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[5])[0] = rslt.getGXDate(5) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 7);
               stmt.setString(2, (String)parms[1], 3);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 3);
               break;
            case 1 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 3);
               stmt.setDate(3, (java.util.Date)parms[2]);
               break;
            case 2 :
               stmt.setString(1, (String)parms[0], 7);
               break;
            case 3 :
               stmt.setString(1, (String)parms[0], 3);
               stmt.setString(2, (String)parms[1], 2);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setString(4, (String)parms[3], 3);
               break;
      }
   }

}


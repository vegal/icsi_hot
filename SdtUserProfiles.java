import com.genexus.*;
import com.genexus.xml.*;

public final  class SdtUserProfiles extends GxSilentTrnSdt implements Cloneable, java.io.Serializable
{
   public SdtUserProfiles( int remoteHandle )
   {
      this( remoteHandle,  new ModelContext(SdtUserProfiles.class));
   }

   public SdtUserProfiles( int remoteHandle ,
                           ModelContext context )
   {
      super( context, "SdtUserProfiles");
      initialize( remoteHandle) ;
   }

   public SdtUserProfiles( int remoteHandle ,
                           StructSdtUserProfiles struct )
   {
      this(remoteHandle);
      setStruct(struct);
   }

   public void Load( String AV64ustCode )
   {
      IGxSilentTrn obj ;
      obj = getTransaction() ;
      obj.LoadKey(new Object[] {AV64ustCode});
      return  ;
   }

   public GxObjectCollection GetMessages( )
   {
      short item ;
      item = (short)(1) ;
      GxObjectCollection msgs ;
      msgs = new GxObjectCollection(SdtMessages_Message.class, "Messages.Message", "Genexus") ;
      com.genexus.internet.MsgList msgList ;
      SdtMessages_Message m1 ;
      IGxSilentTrn trn ;
      trn = getTransaction() ;
      msgList = trn.GetMessages() ;
      while ( ( item <= msgList.getItemCount() ) )
      {
         m1 = new SdtMessages_Message() ;
         m1.setgxTv_SdtMessages_Message_Id( msgList.getItemValue(item) );
         m1.setgxTv_SdtMessages_Message_Description( msgList.getItemText(item) );
         m1.setgxTv_SdtMessages_Message_Type( (byte)(msgList.getItemType(item)) );
         msgs.add(m1, 0);
         item = (short)(item+1) ;
      }
      return msgs ;
   }

   public short readxml( com.genexus.xml.XMLReader oReader ,
                         String sName )
   {
      short GXSoapError ;
      GXSoapError = (short)(1) ;
      sTagName = oReader.getName() ;
      GXSoapError = oReader.read() ;
      nOutParmCount = (short)(0) ;
      while ( ( ( GXutil.strcmp(oReader.getName(), sTagName) != 0 ) || ( oReader.getNodeType() == 1 ) ) && ( GXSoapError > 0 ) )
      {
         readOk = (short)(0) ;
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustCode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Ustcode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustDescription") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Ustdescription = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Functions") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            if ( ( oReader.getIsSimple() == 0 ) )
            {
               GXSoapError = gxTv_SdtUserProfiles_Functions.readxml(oReader, "Functions") ;
            }
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "Mode") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Mode = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustCode_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Ustcode_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustDescription_Z") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Ustdescription_Z = oReader.getValue() ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         if ( ( GXutil.strcmp(oReader.getLocalName(), "ustDescription_N") == 0 ) && ( ( GXutil.strcmp(oReader.getNamespaceURI(), "IataICSI") == 0 ) || ( GXutil.strcmp(oReader.getNamespaceURI(), "") == 0 ) ) )
         {
            gxTv_SdtUserProfiles_Ustdescription_N = (byte)(GXutil.lval( oReader.getValue())) ;
            if ( ( GXSoapError > 0 ) )
            {
               readOk = (short)(1) ;
            }
         }
         GXSoapError = oReader.read() ;
         nOutParmCount = (short)(nOutParmCount+1) ;
         if ( ( readOk == 0 ) )
         {
            context.globals.sSOAPErrMsg = context.globals.sSOAPErrMsg + "Error reading " + sTagName + GXutil.newLine( ) ;
            GXSoapError = (short)(nOutParmCount*-1) ;
         }
      }
      return GXSoapError ;
   }

   public void writexml( com.genexus.xml.XMLWriter oWriter ,
                         String sName ,
                         String sNameSpace )
   {
      short GXSoapError ;
      if ( ((GXutil.strcmp("", GXutil.rtrim( sName))==0)) )
      {
         sName = "UserProfiles" ;
      }
      if ( ! ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) && ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeStartElement(sName+"ns:"+sName);
         oWriter.writeAttribute("xmlns:"+sName+"ns", sNameSpace);
      }
      else
      {
         oWriter.writeStartElement(sName);
      }
      if ( ((GXutil.strcmp("", GXutil.rtrim( sNameSpace))==0)) || ( GXutil.strcmp(sNameSpace, "IataICSI") != 0 ) )
      {
         oWriter.writeAttribute("xmlns", "IataICSI");
      }
      oWriter.writeElement("ustCode", GXutil.rtrim( gxTv_SdtUserProfiles_Ustcode));
      oWriter.writeElement("ustDescription", GXutil.rtrim( gxTv_SdtUserProfiles_Ustdescription));
      gxTv_SdtUserProfiles_Functions.writexml(oWriter, "Functions", "IataICSI");
      oWriter.writeElement("Mode", GXutil.rtrim( gxTv_SdtUserProfiles_Mode));
      oWriter.writeElement("ustCode_Z", GXutil.rtrim( gxTv_SdtUserProfiles_Ustcode_Z));
      oWriter.writeElement("ustDescription_Z", GXutil.rtrim( gxTv_SdtUserProfiles_Ustdescription_Z));
      oWriter.writeElement("ustDescription_N", GXutil.trim( GXutil.str( gxTv_SdtUserProfiles_Ustdescription_N, 1, 0)));
      oWriter.writeEndElement();
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Ustcode( )
   {
      return gxTv_SdtUserProfiles_Ustcode ;
   }

   public void setgxTv_SdtUserProfiles_Ustcode( String value )
   {
      gxTv_SdtUserProfiles_Ustcode = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Ustcode_SetNull( )
   {
      gxTv_SdtUserProfiles_Ustcode = "" ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Ustdescription( )
   {
      return gxTv_SdtUserProfiles_Ustdescription ;
   }

   public void setgxTv_SdtUserProfiles_Ustdescription( String value )
   {
      gxTv_SdtUserProfiles_Ustdescription_N = (byte)(0) ;
      gxTv_SdtUserProfiles_Ustdescription = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Ustdescription_SetNull( )
   {
      gxTv_SdtUserProfiles_Ustdescription_N = (byte)(1) ;
      gxTv_SdtUserProfiles_Ustdescription = "" ;
      return  ;
   }

   public GxSilentTrnGridCollection getgxTv_SdtUserProfiles_Functions( )
   {
      return gxTv_SdtUserProfiles_Functions ;
   }

   public void setgxTv_SdtUserProfiles_Functions( GxSilentTrnGridCollection value )
   {
      gxTv_SdtUserProfiles_Functions = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Functions_SetNull( )
   {
      gxTv_SdtUserProfiles_Functions = new GxSilentTrnGridCollection() ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Mode( )
   {
      return gxTv_SdtUserProfiles_Mode ;
   }

   public void setgxTv_SdtUserProfiles_Mode( String value )
   {
      gxTv_SdtUserProfiles_Mode = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Mode_SetNull( )
   {
      gxTv_SdtUserProfiles_Mode = "" ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Ustcode_Z( )
   {
      return gxTv_SdtUserProfiles_Ustcode_Z ;
   }

   public void setgxTv_SdtUserProfiles_Ustcode_Z( String value )
   {
      gxTv_SdtUserProfiles_Ustcode_Z = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Ustcode_Z_SetNull( )
   {
      gxTv_SdtUserProfiles_Ustcode_Z = "" ;
      return  ;
   }

   public String getgxTv_SdtUserProfiles_Ustdescription_Z( )
   {
      return gxTv_SdtUserProfiles_Ustdescription_Z ;
   }

   public void setgxTv_SdtUserProfiles_Ustdescription_Z( String value )
   {
      gxTv_SdtUserProfiles_Ustdescription_Z = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Ustdescription_Z_SetNull( )
   {
      gxTv_SdtUserProfiles_Ustdescription_Z = "" ;
      return  ;
   }

   public byte getgxTv_SdtUserProfiles_Ustdescription_N( )
   {
      return gxTv_SdtUserProfiles_Ustdescription_N ;
   }

   public void setgxTv_SdtUserProfiles_Ustdescription_N( byte value )
   {
      gxTv_SdtUserProfiles_Ustdescription_N = value ;
      return  ;
   }

   public void setgxTv_SdtUserProfiles_Ustdescription_N_SetNull( )
   {
      gxTv_SdtUserProfiles_Ustdescription_N = (byte)(0) ;
      return  ;
   }

   public void initialize( int remoteHandle )
   {
      initialize( ) ;
      tuserprofiles_bc obj ;
      obj = new tuserprofiles_bc( remoteHandle, context) ;
      obj.initialize();
      obj.SetSDT(this, (byte)(1));
      setTransaction( obj) ;
      obj.SetMode("INS");
      return  ;
   }

   public void initialize( )
   {
      gxTv_SdtUserProfiles_Ustcode = "" ;
      gxTv_SdtUserProfiles_Ustdescription = "" ;
      gxTv_SdtUserProfiles_Functions = new GxSilentTrnGridCollection(SdtUserProfiles_Level1Item.class, "UserProfiles.Level1Item", "IataICSI");
      gxTv_SdtUserProfiles_Mode = "" ;
      gxTv_SdtUserProfiles_Ustcode_Z = "" ;
      gxTv_SdtUserProfiles_Ustdescription_Z = "" ;
      gxTv_SdtUserProfiles_Ustdescription_N = (byte)(0) ;
      sTagName = "" ;
      nOutParmCount = (short)(0) ;
      readOk = (short)(0) ;
      GXt_char1 = "" ;
      return  ;
   }

   public SdtUserProfiles Clone( )
   {
      SdtUserProfiles sdt ;
      tuserprofiles_bc obj ;
      sdt = (SdtUserProfiles)(clone()) ;
      obj = (tuserprofiles_bc)(sdt.getTransaction()) ;
      obj.SetSDT(sdt, (byte)(0));
      return sdt ;
   }

   public void setStruct( StructSdtUserProfiles struct )
   {
      setgxTv_SdtUserProfiles_Ustcode(struct.getUstcode());
      setgxTv_SdtUserProfiles_Ustdescription(struct.getUstdescription());
      setgxTv_SdtUserProfiles_Functions(new GxSilentTrnGridCollection(SdtUserProfiles_Level1Item.class, "UserProfiles.Level1Item", "IataICSI", struct.getFunctions()));
      setgxTv_SdtUserProfiles_Mode(struct.getMode());
      setgxTv_SdtUserProfiles_Ustcode_Z(struct.getUstcode_Z());
      setgxTv_SdtUserProfiles_Ustdescription_Z(struct.getUstdescription_Z());
      setgxTv_SdtUserProfiles_Ustdescription_N(struct.getUstdescription_N());
   }

   public StructSdtUserProfiles getStruct( )
   {
      StructSdtUserProfiles struct = new StructSdtUserProfiles ();
      struct.setUstcode(getgxTv_SdtUserProfiles_Ustcode());
      struct.setUstdescription(getgxTv_SdtUserProfiles_Ustdescription());
      struct.setFunctions(getgxTv_SdtUserProfiles_Functions().getStruct());
      struct.setMode(getgxTv_SdtUserProfiles_Mode());
      struct.setUstcode_Z(getgxTv_SdtUserProfiles_Ustcode_Z());
      struct.setUstdescription_Z(getgxTv_SdtUserProfiles_Ustdescription_Z());
      struct.setUstdescription_N(getgxTv_SdtUserProfiles_Ustdescription_N());
      return struct ;
   }

   protected byte gxTv_SdtUserProfiles_Ustdescription_N ;
   protected short nOutParmCount ;
   protected short readOk ;
   protected String gxTv_SdtUserProfiles_Mode ;
   protected String sTagName ;
   protected String GXt_char1 ;
   protected String gxTv_SdtUserProfiles_Ustcode ;
   protected String gxTv_SdtUserProfiles_Ustdescription ;
   protected String gxTv_SdtUserProfiles_Ustcode_Z ;
   protected String gxTv_SdtUserProfiles_Ustdescription_Z ;
   protected GxSilentTrnGridCollection gxTv_SdtUserProfiles_Functions ;
}


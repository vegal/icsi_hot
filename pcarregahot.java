/*
               File: CarregaHot
        Description: Stub for CarregaHot
             Author: GeneXus Java Generator version 9_0_6-019
       Generated on: April 19, 2020 13:56:5.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;

public final  class pcarregahot extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      pcarregahot pgm = new pcarregahot (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public pcarregahot( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( pcarregahot.class ), "" );
   }

   public pcarregahot( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      new acarregahot(remoteHandle, context).execute(  );
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
}

